/**
 * @license Copyright (c) 2014-2023, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import {ClassicEditor} from '@ckeditor/ckeditor5-editor-classic';
import {Alignment} from '@ckeditor/ckeditor5-alignment';
import {Bold, Italic, Subscript, Superscript, Underline} from '@ckeditor/ckeditor5-basic-styles';
import {BlockQuote} from '@ckeditor/ckeditor5-block-quote';
import {Essentials} from '@ckeditor/ckeditor5-essentials';
import {FindAndReplace} from '@ckeditor/ckeditor5-find-and-replace';
import {FontBackgroundColor, FontColor, FontFamily, FontSize} from '@ckeditor/ckeditor5-font';
import {Heading} from '@ckeditor/ckeditor5-heading';
import {GeneralHtmlSupport} from '@ckeditor/ckeditor5-html-support';
import {Image, ImageInsert, ImageResize, ImageUpload} from '@ckeditor/ckeditor5-image';
import {Indent} from '@ckeditor/ckeditor5-indent';
import {Link} from '@ckeditor/ckeditor5-link';
import {List} from '@ckeditor/ckeditor5-list';
import {MediaEmbed} from '@ckeditor/ckeditor5-media-embed';
import {Paragraph} from '@ckeditor/ckeditor5-paragraph';
import {PasteFromOffice} from '@ckeditor/ckeditor5-paste-from-office';
import {
  SpecialCharacters,
  SpecialCharactersArrows,
  SpecialCharactersCurrency,
  SpecialCharactersText
} from '@ckeditor/ckeditor5-special-characters';
import {Table, TableCellProperties, TableColumnResize, TableToolbar} from '@ckeditor/ckeditor5-table';
import {TextTransformation} from '@ckeditor/ckeditor5-typing';
import {Base64UploadAdapter} from '@ckeditor/ckeditor5-upload';
declare class Editor extends ClassicEditor {
  static builtinPlugins: (typeof Alignment | typeof Base64UploadAdapter | typeof BlockQuote | typeof Bold | typeof Essentials | typeof FindAndReplace | typeof FontBackgroundColor | typeof FontColor | typeof FontFamily | typeof FontSize | typeof GeneralHtmlSupport | typeof Heading | typeof Image | typeof ImageInsert | typeof ImageResize | typeof ImageUpload | typeof Indent | typeof Italic | typeof Link | typeof List | typeof MediaEmbed | typeof Paragraph | typeof PasteFromOffice | typeof SpecialCharacters | typeof SpecialCharactersArrows | typeof SpecialCharactersCurrency | typeof SpecialCharactersText | typeof Subscript | typeof Superscript | typeof Table | typeof TableCellProperties | typeof TableColumnResize | typeof TableToolbar | typeof TextTransformation | typeof Underline)[];
  static defaultConfig: {
    toolbar: {
      items: string[];
      shouldNotGroupWhenFull: boolean;
    };
    language: string;
    table: {
      contentToolbar: string[];
    };
  };
}
export default Editor;
