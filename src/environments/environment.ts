// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  app: {
    realm: 'BPM',
    relationShip: 'bpm-service',
  },
  refund_url: 'https://bpm-api-test.mbageas.org/api/v1',
  employee_url: 'https://bpm-employee-test.mbageas.org/api/v1',
  task_url: 'https://bpm-task-test.mbageas.org/api/v1',
  category_url: 'https://bpm-category-test.mbageas.org/api/v1',
  collection_url: 'https://bpm-collection-test.mbageas.org/api/v1',
  bpm_claim: 'https://bpm-claim-test.mbageas.org/api/v1',
  claim_url: 'https://claim-service-test.mbageas.org/api/v1',
  payment_url: 'https://bpm-payment-test.mbageas.org/api/v1'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
