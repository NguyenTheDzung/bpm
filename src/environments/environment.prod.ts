export const environment = {
  production: true,
  app: {
    issuer: 'https://keycloak-internal.mbageas.life/auth/',
    realm: 'BPM',
    client: 'bpm-frontend',
    relationShip: 'bpm-service',
  },
  refund_url: 'https://bpm-refund-prod.mbageas.org/api/v1',
  employee_url: 'https://bpm-employee-prod.mbageas.org/api/v1',
  task_url: 'https://bpm-task-prod.mbageas.org/api/v1',
  category_url: 'https://bpm-category-prod.mbageas.org/api/v1',
  collection_url: 'https://bpm-collection-prod.mbageas.org/api/v1',
  claim_url: 'https://claim-service-prod.mbageas.org/api/v1',
};
