export const environment = {
  production: true,
  app: {
    issuer: 'https://keycloak-uat.mbageas.org/auth/',
    realm: 'bpm',
    client: 'bpm-frontend',
    relationShip: 'bpm-service',
  },
  refund_url: 'https://bpm-api-test.mbageas.org/api/v1',
  employee_url: 'https://bpm-employee-test.mbageas.org/api/v1',
  task_url: 'https://bpm-task-test.mbageas.org/api/v1',
  category_url: 'https://bpm-category-test.mbageas.org/api/v1',
  collection_url: 'https://bpm-collection-test.mbageas.org/api/v1',
  bpm_claim: 'https://bpm-claim-test.mbageas.org/api/v1',
  claim_url: 'https://claim-service-test.mbageas.org/api/v1',
  payment_url: 'https://bpm-payment-test.mbageas.org/api/v1'
};
