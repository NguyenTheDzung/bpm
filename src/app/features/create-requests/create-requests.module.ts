import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';
import { CreateRequestsRoutingModule } from './create-requests-routing.module';
import { pages } from './pages';

@NgModule({
  declarations: [...pages, ...components],
  imports: [CreateRequestsRoutingModule, SharedModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  exports: [],
})
export class CreateRequestsModule {}
