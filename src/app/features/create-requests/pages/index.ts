import { RequestRefundComponent } from './request-refund/request-refund.component';
import { RequestCollectionComponent } from './request-collection/request-collection.component';
import { RequestClaimComponent } from '@create-requests/pages/request-claim/request-claim.component';

export const pages = [RequestRefundComponent, RequestCollectionComponent, RequestClaimComponent];

export * from './request-refund/request-refund.component';
export * from './request-collection/request-collection.component';
export * from './request-claim/request-claim.component';
