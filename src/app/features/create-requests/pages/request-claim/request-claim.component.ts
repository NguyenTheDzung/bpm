import { Component, Injector } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { StateRefund } from '../../models/refund.model';
import { CollectionDetailRequestModel } from '@detail-request/shared/models/collection-detail.model';
import { Roles } from '@cores/utils/constants';
import { CreateClaimService } from '@create-requests/service/create-claim.service';

@Component({
  selector: 'app-request-claim',
  templateUrl: './request-claim.component.html',
  styleUrls: ['./request-claim.component.scss'],
})
export class RequestClaimComponent extends BaseActionComponent {
  override form = this.fb.group({
    requestType: null,
  });
  itemType: string = '';
  stateData: StateRefund | undefined;
  itemRequest: CollectionDetailRequestModel | undefined;
  isCSS = false;

  constructor(inject: Injector, private claimService: CreateClaimService) {
    super(inject, claimService);
    this.isCSS = this.authService.getUserRoles().includes(Roles.OP_COL_CSSS);
    const stateUrl: any = this.router.getCurrentNavigation()?.extras?.state;
    if (stateUrl) {
      this.itemRequest = stateUrl?.request;
      this.itemType = stateUrl?.type || '';
    }
  }

  onChangeType() {
    this.itemRequest = undefined;
  }

  show() {
    return this.itemType && this.itemType !== this.requestType.TYPE06;
  }
}
