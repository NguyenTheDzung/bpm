import { Component, Injector, OnInit } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import * as _ from 'lodash';
import { StateRefund } from '../../models/refund.model';
import { RefundService } from '../../service/refund.service';

@Component({
  selector: 'app-request-refund',
  templateUrl: './request-refund.component.html',
  styleUrls: ['./request-refund.component.scss'],
})
export class RequestRefundComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    requestType: null,
  });
  itemType: string = '';
  stateData: StateRefund | undefined;
  itemRequest: any;

  constructor(inject: Injector, private refundService: RefundService) {
    super(inject, refundService);
    const stateUrl: any = this.router.getCurrentNavigation()?.extras?.state;
    if (stateUrl) {
      this.itemRequest = stateUrl?.request;
      this.itemType = stateUrl?.type || '';
    }
  }

  ngOnInit(): void {
    this.loadingService.start();
    const requestId = localStorage?.getItem('requestId');
    this.refundService.getState().subscribe((data) => {
      this.stateData = data;
      this.loadingService.complete();
    });
    if (!_.isEmpty(requestId) && !this.itemRequest) {
      this.getUpdatePendingRequest(requestId!);
    }
  }

  getUpdatePendingRequest(requestId: string) {
    this.loadingService.start();
    localStorage.removeItem('requestId');
    this.refundService.getUpdatePendingRequest(requestId).subscribe({
      next: (res) => {
        this.itemRequest = res?.data?.data;
        this.itemRequest!.ccCode = res.data?.data?.icCode;
        this.itemRequest!.policyNumber = res.data?.data?.requestId;
        this.itemType = this.requestType.TYPE06;
        this.loadingService.complete();
      },
    });
  }

  onChangeType() {
    this.itemRequest = undefined;
  }
}
