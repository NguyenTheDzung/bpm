import { FileRefund } from "../models/refund.model";
import { FileCollection } from "../models/collection.model";

//type của chứng từ nộp phí
export const ZCOL_PR = 'ZCOL_PR';
export const ZCOL_AR = 'ZCOL_AR';
//loại chứng từ phí
export const TypeFee = {
  other: 'D',
};
export const typeFileRefund: FileRefund[] = [
  {
    required: false,
    type: 'ZSAP_CA',
    label: 'createRequest.requestRefund.cancellationRequestForm',
    file: null,
    content: null,
    mimeType: null,
    extension: null,
  },
  {
    required: false,
    type: 'ZSAP_FRID',
    label: 'createRequest.requestRefund.idFrontside',
    file: null,
    content: null,
    mimeType: null,
    extension: null,
  },
  {
    required: false,
    type: 'ZSAP_BCKID',
    label: 'createRequest.requestRefund.idBackside',
    file: null,
    content: null,
    mimeType: null,
    extension: null,
  },
  {
    required: false,
    type: 'ZSAP_OTHR',
    label: 'createRequest.requestRefund.others',
    file: null,
    content: null,
    mimeType: null,
    extension: null,
  },
  {
    required: false,
    type: 'ZSAP_PR',
    label: 'createRequest.requestRefund.premiumReceipt',
    file: null,
    content: null,
    mimeType: null,
    extension: null,
  },
];

export const typeFileCollection: FileCollection[] = [
  {
    required: false,
    type: null,
    label: 'createRequest.requestCollection.paymentAdjustmentRequestForm',
    file: null,
    content: null,
    mimeType: null,
    extension: null,
  },
];

export const RequestType = {
  // "Hủy HSYCBH và hoàn phí BH",
  TYPE01: '01',
  // "Hủy HSYCBH và chuyển phí sang HSYCBH mới",
  TYPE02: '02',
  // "Hủy HĐBH trong 21 ngày và hoàn phí BH",
  TYPE03: '03',
  // "Hủy HĐBH trong 21 ngày và chuyển phí sang HSYCBH
  TYPE04: '04',
  // "Hủy HSYCBH theo quyết định của phòng thẩm định",
  TYPE05: '05',
  // "Nộp bổ sung thông tin theo yêu cầu",
  TYPE06: '06',
  // "Yêu cầu điều chỉnh phí của bảo hiểm",
  TYPE07: '07',
  // "Yêu cầu hoàn phí",
  TYPE08: '08',
  // "Nộp chứng từ nộp phí
  TYPE09: '09',
  // "Khôi phục hợp đồng tự động",
  TYPE10: '10',
  // Yêu cầu hỗ trợ chi phí nằm viện nội trú / phẫu thuật
  TYPE11: '11',
  // Yêu cầu claim | yêu cầu điều tra
  TYPE12: '12',
  // Yêu cầu claim | Yêu cầu tái thẩm định
  TYPE13: '13'
};

export const Refund05ProcessOption = {
  // Hủy chuyển: Nhập số hồ sơ mới cần chuyển sang
  transfer: '2',
  // Hủy hoàn: Chọn payment lot để thực hiện payment run vào hoàn tiền
  refund: '3',
};

//Phản hồi QM
export const SEND_QM_SELECT = {
  isGreedy: 'request.detailRequest.bp.isGreedy',
  isNotGreedy: 'request.detailRequest.bp.isNotGreedy',
};
