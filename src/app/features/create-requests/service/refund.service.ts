import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { environment } from '@env';
import { forkJoin, map, Observable, of } from 'rxjs';
import { StateRefund } from '../models/refund.model';
import { RefundStepApproveModel } from '@detail-request/shared/models/refund-step-approve.model';
import { RequestType } from '../utils/constants';
import * as _ from 'lodash';
import { SAP_FILE_TYPE_NBU } from '@cores/utils/constants';
import { CommonCategoryService } from '@cores/services/common-category.service';

@Injectable({
  providedIn: 'root',
})
export class RefundService extends BaseService {
  override state: StateRefund | undefined;

  constructor(http: HttpClient, private commonService: CommonCategoryService) {
    super(http, `${environment.refund_url}/request`);
  }

  override getState(): Observable<StateRefund> {
    if (_.isEmpty(this.state)) {
      return forkJoin({
        listFileNBU: this.commonService.getCommonCategory(SAP_FILE_TYPE_NBU),
      }).pipe(
        map(
          (data) =>
            (this.state = {
              ...this.state,
              ...data,
            })
        )
      );
    } else {
      return of(this.state!);
    }
  }

  createPending(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/request-completion`, data);
  }

  getInfoFromSAP(policyNumber: string, type: string): Observable<any> {
    let param = [RequestType.TYPE01, RequestType.TYPE02, RequestType.TYPE05].includes(type)
      ? 'applicationNumber'
      : 'policyNumber';
    return this.http.get(`${environment.refund_url}/sap/combined-data?${param}=${policyNumber}`);
  }

  getUpdatePendingRequest(id: any): Observable<any> {
    return this.http.get(`${environment.refund_url}/request/get-update-pending-request?id=${id}`);
  }

  processStepApprove(data: RefundStepApproveModel): Observable<any> {
    return this.http.put(`${this.baseUrl}/approve-payment-request`, data);
  }
}
