import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { environment } from '@env';
import { Observable } from 'rxjs';
import { StateRefund } from '../models/refund.model';
import { RefundStepApproveModel } from '@detail-request/shared/models/refund-step-approve.model';

@Injectable({
  providedIn: 'root',
})
export class CollectionService extends BaseService {
  override state: StateRefund | undefined;

  constructor(http: HttpClient) {
    super(http, `${environment.collection_url}/collection`);
  }

  getInfoFromSAP(policyNumber: any): Observable<any> {
    return this.http.get(`${environment.collection_url}/collection/combined-data?policyNumber=${policyNumber}`);
  }

  getInfoIntegration(params: any): Observable<any> {
    return this.http.post<any>(`${environment.collection_url}/integration/get-fpl9`, params);
  }

  createApiCollection(data: any): Observable<any> {
    return this.http.post(this.baseURL, data);
  }

  getUpdatePendingRequest(id: any): Observable<any> {
    return this.http.get(`${environment.refund_url}/request/get-update-pending-request?id=${id}`);
  }

  processStepApprove(data: RefundStepApproveModel) {
    return this.http.put(`${this.baseUrl}/refund-request/${data.requestCode}/approve`, data);
  }

  createPending(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/pending-attachment`, data);
  }

  findRequestPending(requestId: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/pending-attachment?requestId=${requestId}`);
  }

  getSapAttachment(arcDocId: string): Observable<any> {
    return this.http.get(`${environment.collection_url}/integration/get-sap-attachment?arcDocId=${arcDocId}`);
  }
}
