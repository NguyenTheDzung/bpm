import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { map, Observable } from 'rxjs';
import { CreateClaim, InfoPolicyHolder, PolicyInfoBP } from '@create-requests/models/claim.model';
import { IResponseModel } from '@cores/models/response.model';
import { CommonClaimCategory } from "@cores/models/common-claim.model";
import { ClaimCommonType } from "@cores/utils/constants";
import { CategoryService } from "@common-category/services/category.service";
import { CommonCategoryService } from "@cores/services/common-category.service";

@Injectable({
  providedIn: 'root',
})
export class CreateClaimService extends BaseService {
  constructor(
    http: HttpClient,
    private categoryService: CommonCategoryService
  ) {
    super(http, `${environment.claim_url}/claim`);
  }

  override create(data: CreateClaim): Observable<any> {
    return this.http.post(`${this.baseUrl}/create`, data);
  }

  getListPolicyByPartner(policyHolder: string): Observable<PolicyInfoBP> {
    return this.http
      .get<IResponseModel<PolicyInfoBP>>(`${this.baseUrl}/get-list-policy`, {
        params: {
          policyNumber: policyHolder,
        },
      })
      .pipe(map((res) => res.data));
  }

  getInfoCustomer(policyHolder: string): Observable<InfoPolicyHolder> {
    return this.http
      .get<IResponseModel<InfoPolicyHolder>>(`${this.baseUrl}/get-info?policyHolder=${policyHolder}`)
      .pipe(map((res) => res.data));
  }

  getDiseases(): Observable<CommonClaimCategory[]> {
    return this.categoryService.getCommonClaimByCode(ClaimCommonType.SICKNESS)
  }

  getFacility(): Observable<CommonClaimCategory[]> {
    return this.categoryService.getCommonClaimByCode(ClaimCommonType.FACILITY)
  }

  getBenefit(): Observable<CommonClaimCategory[]> {
    return this.categoryService.getCommonClaimByCode(ClaimCommonType.BENEFIT, {params: {type: "002"}})
  }

  getResourceDoc(): Observable<CommonClaimCategory[]> {
    return this.categoryService.getCommonClaimByCode(ClaimCommonType.RESOURCE_DOC)
  }

  getFatca(): Observable<CommonClaimCategory[]> {
    return this.http.get<IResponseModel<CommonClaimCategory[]>>(`${this.baseUrl}/fatca`).pipe(map((res) => res.data));
  }
}
