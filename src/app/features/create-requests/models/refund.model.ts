import { CommonModel } from '@common-category/models/common-category.model';

export interface StateRefund {
  listData?: CommonModel[];
  listAttachmentType?: CommonModel[];
  listFileNBU?: CommonModel[];
}

export interface FileRefund {
  label: string;
  type: string;
  file: File[] | null;
  required: boolean;
  content: string[] | null;
  mimeType: string | null;
  extension: string | null;
}

export interface RequestModel {
  requestId?: string;
  policyNumber?: string;
  policyHolderName?: string;
  ccCode?: string;
  icName?: string;
  additionalAttachments?: string[];
}
