import { FileAttachment } from '@cores/models/file-attachment.model';
import { ExtractFormControl } from '@cores/models/form.model';
import { CommonClaimCategory } from "@cores/models/common-claim.model";

export interface CreateClaim {
  policyNumber: string;
  holder: Holder;
  insured: LADetail;
  benefitDetail: CommonClaimCategory[];
  disease: CommonClaimCategory[];
  facility: CommonClaimCategory;
  identification?: Identification;
  bankInfo?: BankInfo;
  createDate: string;
  submitFirstRequest: string;
  startDateEvent: string;
  endDateEvent: string;
  paymentMethod: string;
  phone: string;
  email: string;
  requestFileDTOS: FileClaim[];
  resourceAttachmentType: string;
  fatca: Fatca[];
  frmSystem: string;
}

export interface FileClaim extends FileAttachment {
  objectType: string;
}

export interface FileClaimAdditional {
  objectType: string;
  objectId: string;
  fileTypes: string;
  fileName: string;
  content: string;
  typeFile: string;
  extension: string;
  mimeType: string;
  size: number;
  lastModified: number;
}

export interface Holder {
  code: string;
  name: string;
}

export interface BankInfo {
  branch?: string;
  name: string;
  code: string;
  accountName: string;
  accountNumber: string;
}

export interface Identification {
  type: string;
  idNumber: string;
  issuedDate: string;
  issuedPlace: string;
  branch?: string;
  name?: string;
  code?: string;
}

type IOmitFormClaim = 'fatca' | 'requestFileDTOS' | 'insured' | 'benefitDetail' | 'disease' | 'facility' | 'frmSystem';

export type IClaimForm = Omit<CreateClaim, IOmitFormClaim> & {
  benefitCodes: string[];
  diseaseCodes: string[];
  facilityCode: string;
  bpInsuredCode: string;
};

export interface ClaimForm extends ExtractFormControl<IClaimForm> {}

export interface Insured {
  bpIdInsured: string;
  fullName: string;
  policy: string;
  policyList: string[];
  pmId: string;
}

export interface PolicyInfoBP {
  fullName: string;
  pbNumber: string;
  phone: string;
  email: string;
  bankName: string;
  bankBranch: string;
  bankCardNum: string;
  bankAccount: string;
  cashType: string;
  cashNum: string;
  cashDate: string;
  cashAddress: string;
  laDetails: LADetail[];
  bankInfos: BankPolicy[];
}

export interface LADetail {
  policyNumber: string;
  fullName: string;
  idNumber: string;
  bpIdInsured: string;
  pmId: string;
  bpNumber: string;
  code: string;
  policyNumbers: LAPolicy[];
}

export interface LAPolicy {
  pmId: string;
  policyNumber: string;
}

export interface Fatca {
  code: string;
  answer: boolean;
}

export interface InfoPolicyHolder {
  fullName: string;
  type: number;
  email?: string;
  phone?: string;
  bankDTOList: BankPolicy[];
}

export interface BankPolicy {
  account: string;
  accountName: string;
  branch: string;
  code: string;
  name: string;
  displayName?: string;
}

export interface FileTypeClaim {
  label: string;
  type: string;
  file: File[] | null;
  content: string[] | null;
  required: boolean;
  maxSize: number;
  acceptType: string;
  multiLanguage: any;
}

export interface ClaimPending {
  requestId: string;
  fromSystem: string;
  dateUpdate: string;
  executorUpdate: string;
  updateInfo: {
    files: FileClaimAdditional[];
    sourceDoc: string;
    note: string;
  };
}
