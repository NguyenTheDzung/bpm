export interface FileCollection {
  label: string;
  type: string | null;
  file: File[] | null;
  required: boolean;
  content: string[] | null;
  mimeType: string | null;
  extension: string | null;
}

//khôi phục hợp đồng
export interface ContractRestoreModel {
  documentNo?: string;
  amount?: number;
  postingDate: string | Date;
  netDueDate?: string | Date;
  usageText?: string;
  additionalInfo?: string;
  changedBy?: string;
  changedOn?: string | Date;
  time?: string | Date;
}
