import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FunctionCode } from '@cores/utils/enums';
import { RequestClaimComponent, RequestCollectionComponent, RequestRefundComponent } from './pages';
import { FunctionGuardService } from '@cores/services/function-guard.service';

const routes: Routes = [
  {
    path: 'refund',
    component: RequestRefundComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.RefundRequest,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.RefundRequest}`,
    },
  },
  {
    path: 'collection',
    component: RequestCollectionComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.CollectionRequest,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.CollectionRequest}`,
    },
  },
  {
    path: 'claim',
    component: RequestClaimComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.CollectionClaim,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.CollectionClaim}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateRequestsRoutingModule {}
