import { Component, Injector, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Validators } from '@angular/forms';
import { createErrorMessage, getValueDateTimeLocal, validateAllFormFields } from '@cores/utils/functions';
import { MAX_FILE_SIZE, Roles } from '@cores/utils/constants';
import { BaseActionComponent } from '@shared/components';
import * as _ from 'lodash';
import { isEmpty } from 'lodash';
import { FileUpload } from 'primeng/fileupload';
import { TypeFee, typeFileCollection, ZCOL_PR } from '../../utils/constants';
import { CommonModel } from '@common-category/models/common-category.model';
import { RequestService } from 'src/app/features/requests/services/request.service';
import { FileCollection } from '@create-requests/models/collection.model';
import { CollectionService } from '@create-requests/service/collection.service';
import { CustomValidators } from '@cores/utils/custom-validators';
import { CollectionDetailRequestModel } from '@detail-request/shared/models/collection-detail.model';

@Component({
  selector: 'app-submit-pay-license',
  templateUrl: './submit-pay-license.component.html',
  styleUrls: ['./submit-pay-license.component.scss'],
})
//dùng chung cho 2 luồng HĐBH
export class SubmitPayLicenseComponent extends BaseActionComponent implements OnInit, OnChanges {
  @ViewChildren('fileInput') fileInputs: QueryList<FileUpload> | undefined;
  @Input() itemType: string = '';
  @Input() itemRequest?: CollectionDetailRequestModel;
  files: FileCollection[] = typeFileCollection;
  feeType!: CommonModel[];
  maxFileSize: number = 0;
  fileName: string = '';
  dateNotFuture = new Date();
  override form = this.fb.group({
    ticketNumber: ['', Validators.pattern('(CAS)([0-9]){6}')],
    policyNumber: ['', [Validators.required, Validators.minLength(12), Validators.maxLength(12)]],
    documentType: ['', Validators.required],
    policyHolderName: [{ value: '', disabled: true }, Validators.required],
    policyHolderPhone: [{ value: '', disabled: true }, [Validators.required, Validators.maxLength(12)]],
    numOfPremiums: [null, [Validators.required, Validators.maxLength(12)]],
    paymentReceiveDate: ['', Validators.required],
    documentTypeOther: null,
    email: ['', [Validators.required, CustomValidators.email]],
    requestId: null,
  });
  typeFee = TypeFee;
  isRoleCSSS = false;

  constructor(inject: Injector, private colService: CollectionService, private requestService: RequestService) {
    super(inject, colService);
    this.files[0].required = true;
  }

  ngOnInit(): void {
    this.isRoleCSSS = this.authService.getUserRoles()?.includes(Roles.OP_COL_CSSS);
    this.loadingService.start();
    this.requestService.getState().subscribe({
      next: (state) => {
        this.feeType = state.listFeeVoucherType ?? [];
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['itemType']) {
      this.form.reset();
      this.clearFile();
    }
    if (changes['itemRequest'] && this.itemRequest) {
      const data = {
        requestId: this.itemRequest?.step1?.requestId,
        policyHolderName: this.itemRequest?.step1?.policyHolderName,
        policyHolderPhone: this.itemRequest?.step1?.phoneNumber,
        email: this.itemRequest?.step1?.email,
        numOfPremiums: +this.itemRequest?.step1?.numberOfPremiums! || null,
        paymentReceiveDate: this.itemRequest?.step1?.paymentReceiveDate,
        documentType: this.itemRequest?.step1?.documentType,
        ticketNumber: this.itemRequest?.step1?.ticketNumber,
        documentTypeOther: this.itemRequest?.step1?.documentTypeOther,
      };
      this.form.patchValue(data);
      this.form.disable();
    }
  }

  findPolicyInfo() {
    const data = this.getDataForm();
    if (
      isEmpty(data?.policyNumber) ||
      this.loadingService.loading ||
      data.policyNumber?.length < 12 ||
      data.policyNumber?.length > 12
    ) {
      return;
    }
    this.loadingService.start();
    this.colService.getInfoFromSAP(data.policyNumber).subscribe({
      next: (res) => {
        this.loadingService.complete();
        const dataForm = {
          policyNumber: data.policyNumber || '',
          policyHolderName: res?.data?.policyInfoResponseToBpmDTO?.holderDTO?.fullName,
          policyHolderPhone: res?.data?.policyInfoResponseToBpmDTO?.holderDTO?.mobileResDTO?.item[0]?.telNumber,
          email: res?.data?.bpInfoOfAgent[0]?.emailResDTO[0]?.smtpAddr || '',
        };
        this.form.patchValue(dataForm);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  override save() {
    validateAllFormFields(this.form);
    const data: any = {};
    data.requestFileDTOs = this.mapFiles();
    if (this.form?.status !== 'VALID' && !(this.itemRequest && this.itemType === this.requestType.TYPE06)) {
      return;
    }
    if (this.maxFileSize > MAX_FILE_SIZE) {
      return this.messageService.error('MESSAGE.TOTAL_MAX_FILE_SIZE_UPLOAD');
    }
    if (data.requestFileDTOs.length === 0) {
      return this.messageService.error('MESSAGE.FILE_NULL');
    }

    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        if (this.itemRequest && this.itemType === this.requestType.TYPE06) {
          return this.createPending(data);
        } else {
          this.create(data);
        }
      }
    });
  }

  mapFiles() {
    const requestFileDTOs: any[] = [];
    this.maxFileSize = 0;
    this.files.forEach((item) => {
      if (item.file) {
        item.file.forEach((file, i) => {
          this.maxFileSize += file.size;
          requestFileDTOs.push({
            name: file.name,
            type: ZCOL_PR,
            extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
            content: item.content![i],
            mimeType: file.type,
          });
        });
      }
    });
    return requestFileDTOs;
  }

  override create(data: any) {
    const valueForm = _.clone(this.getDataForm());
    valueForm.paymentReceiveDate = getValueDateTimeLocal(valueForm.paymentReceiveDate);
    const body = {
      ...data,
      policyNumber: valueForm.policyNumber,
      requestType: this.itemType,
      feeInformationRequestCreateDTO: valueForm,
    };
    this.loadingService.start();
    this.colService.createApiCollection(body).subscribe({
      next: () => {
        this.form.reset();
        this.clearFile();
        this.files.forEach((item) => {
          item.file = null;
          item.content = null;
          item.mimeType = null;
        });
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  createPending(data: any) {
    data.requestType = this.requestType.TYPE09;
    data.requestId = this.itemRequest?.step1?.requestId;
    this.loadingService.start();
    this.colService.createPending(data).subscribe({
      next: () => {
        this.form.reset();
        this.clearFile();
        this.files.forEach((item) => {
          item.file = null;
          item.content = null;
          item.mimeType = null;
        });
        localStorage.removeItem('requestId');
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  uploadHandler(event: FileUpload, index: number) {
    this.files[index].content = [];
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.files[index].content?.push(
          (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1)
        );
      };
    });
    this.files[index].file = event?.files;
  }

  clearFile() {
    this.fileInputs?.forEach((item: FileUpload) => {
      item.clear();
    });
  }
}
