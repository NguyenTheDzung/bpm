import { Component, Injector, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { Validators } from '@angular/forms';
import { createErrorMessage, getValueDateTimeLocal, validateAllFormFields } from '@cores/utils/functions';
import { MAX_FILE_SIZE } from '@cores/utils/constants';
import { BaseActionComponent } from '@shared/components';
import * as _ from 'lodash';
import { isEmpty } from 'lodash';
import { FileUpload } from 'primeng/fileupload';
import { FileRefund, RequestModel, StateRefund } from '../../models/refund.model';
import { RefundService } from '../../service/refund.service';
import { RequestType } from '../../utils/constants';
import { CustomValidators } from '@cores/utils/custom-validators';
import { Observable } from 'rxjs';
import { CommonModel } from '@common-category/models/common-category.model';

@Component({
  selector: 'app-form-refund',
  templateUrl: './form-refund.component.html',
  styleUrls: ['./form-refund.component.scss'],
})
export class FormRefundComponent extends BaseActionComponent implements OnInit, OnChanges {
  @ViewChildren('fileInput')
  fileInputs: QueryList<FileUpload> | undefined;
  @Input() itemType: string = '';
  @Input() itemRequest!: RequestModel;
  @Input() filesRefund: CommonModel[] = [];
  maxDate = new Date();
  endDate = new Date();
  maxFileSize: number = 0;
  dateNotFuture = new Date();
  ackDate: string = '';
  files: FileRefund[] = [];
  override state: StateRefund | undefined;
  override form = this.fb.group({
    requestId: null,
    email: ['', [Validators.required, CustomValidators.email]],
    ccCode: [{ value: '', disabled: true }],
    icName: [{ value: '', disabled: true }],
    policyNumber: [''],
    requestSigningDate: ['', Validators.required],
    requestType: ['', Validators.required],
    policyHolderName: [{ value: '', disabled: true }],
  });

  constructor(inject: Injector, private refundService: RefundService) {
    super(inject, refundService);
    this.translateService.onLangChange.subscribe(() => {
      this.files?.forEach((item) => {
        item.label = this.filesRefund.find((o) => o.code === item.type)?.multiLanguage[
          this.translateService.currentLang
        ];
      });
    });
  }

  ngOnInit(): void {
    this.form.get('requestSigningDate')?.valueChanges.subscribe((value) => {
      if (
        value &&
        new Date(value).valueOf() > this.maxDate.valueOf() &&
        (this.itemType === RequestType.TYPE03 || this.itemType === RequestType.TYPE04)
      ) {
        this.messageService.warn('MESSAGE.GREATER_THAN_21_DAY');
        return;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['itemType']) {
      this.files = this.mapListFile();
      if (this.itemType === RequestType.TYPE05) {
        this.files = [];
        this.form.get('requestSigningDate')?.setValidators(null);
        this.form.get('email')?.enable();
      } else if (this.itemType === RequestType.TYPE06) {
        this.form.get('requestSigningDate')?.setValidators(null);
        this.form.get('email')?.disable();
      } else if ([RequestType.TYPE03, RequestType.TYPE04].includes(this.itemType)) {
        this.form.get('policyNumber')?.setValidators([Validators.required, Validators.minLength(12)]);
      } else {
        this.form.get('policyNumber')?.setValidators(Validators.required);
        this.form.get('requestSigningDate')?.setValidators(Validators.required);
        this.form.get('email')?.enable();
      }
      this.form.updateValueAndValidity();
      this.form.reset();
      this.maxFileSize = 0;
      this.clearFile();
    }
    if (changes['itemRequest']?.currentValue) {
      this.form.patchValue(this.itemRequest);
      this.createObj();
    }
  }

  createObj() {
    this.files = this.mapListFile().filter((x) => this.itemRequest?.additionalAttachments?.includes(x.type));
    this.files.forEach((x) => {
      x.required = true;
    });
  }

  mapListFile() {
    this.filesRefund = _.orderBy(this.filesRefund, 'orderNum');
    return (
      this.filesRefund?.map((item) => {
        return <FileRefund>{
          required: item.value.toLowerCase() === 'required',
          type: item.code,
          label: item.multiLanguage[this.translateService.currentLang],
          file: null,
          content: null,
          mimeType: null,
          extension: null,
        };
      }) || []
    );
  }

  findPolicyInfo() {
    const data = this.getDataForm();
    if (
      isEmpty(data?.policyNumber) ||
      this.loadingService.loading ||
      (data.policyNumber?.length < 12 && [RequestType.TYPE04, RequestType.TYPE03].includes(this.itemType))
    ) {
      return;
    }
    this.loadingService.start();
    this.refundService.getInfoFromSAP(data.policyNumber, this.itemType).subscribe({
      next: (res) => {
        this.loadingService.complete();
        this.ackDate = res?.data?.policyInfoResponseToBpmDTO?.zzAckDT;
        this.maxDate = new Date(res?.data?.policyInfoResponseToBpmDTO?.zzAckDT);
        this.maxDate.setDate(this.maxDate.getDate() + 21);
        const dataForm = {
          email: res?.data?.bpInfoOfAgent[0]?.emailResDTO[0]?.smtpAddr || '',
          icName: res?.data.bpInfoOfAgent[0]?.fullName,
          ccCode: res?.data?.bpInfoOfAgent[0]?.partnerId,
          policyHolderName: res?.data?.policyInfoResponseToBpmDTO?.holderResDTO?.fullName,
          requestSigningDate: null,
          requestType: this.itemType,
        };
        this.form.patchValue(dataForm);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  override save() {
    const data = this.getDataForm();
    let totalSize = 0;
    data.requestFileDTOs = [];
    this.files.forEach((item) => {
      if (item.file) {
        item.file.forEach((file, i) => {
          totalSize += file.size;
          data.requestFileDTOs.push({
            name: file.name,
            type: item.type,
            extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
            content: item.content![i],
            mimeType: file.type,
          });
        });
      }
    });
    data.requestSigningDate = getValueDateTimeLocal(data.requestSigningDate);
    if (totalSize > MAX_FILE_SIZE) {
      return this.messageService.error('MESSAGE.TOTAL_MAX_FILE_SIZE_UPLOAD');
    }
    if (this.form?.status === 'VALID') {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          this.create(data);
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  override create(data: any) {
    this.loadingService.start();
    let api: Observable<any>;
    if (this.itemType === RequestType.TYPE06) {
      api = this.refundService.createPending(data);
    } else {
      api = this.serviceBase.create(data);
    }
    api.subscribe({
      next: () => {
        this.form.reset();
        this.clearFile();
        this.files.forEach((item) => {
          item.file = null;
          item.content = null;
          item.mimeType = null;
        });
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  getData() {
    if (this.itemType === RequestType.TYPE06) {
      this.findUpdatePendingRequest();
    } else {
      this.findPolicyInfo();
    }
  }

  findUpdatePendingRequest() {
    const data = this.getDataForm();
    if (_.isEmpty(data?.policyNumber) || this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    this.refundService.getUpdatePendingRequest(data.policyNumber).subscribe({
      next: (res) => {
        this.itemRequest = res?.data?.data;
        const dataForm = {
          requestId: res?.data?.data?.requestId,
          policyNumber: res?.data?.data?.requestId,
          ccCode: res?.data?.data?.icCode,
          icName: res?.data?.data?.icName,
          policyHolderName: res?.data?.data?.policyHolderName,
          requestType: this.itemType,
        };
        this.form.patchValue(dataForm);
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  uploadHandler(event: FileUpload, index: number) {
    this.files[index].content = [];
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.files[index].content?.push(
          (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1)
        );
      };
    });
    this.files[index].file = event?.files;
  }

  clearFile() {
    this.fileInputs?.forEach((item: FileUpload) => {
      item.clear();
    });
  }

  getLabelPolicyNumber() {
    switch (this.itemType) {
      case RequestType.TYPE01:
      case RequestType.TYPE02:
      case RequestType.TYPE05:
        return 'applicationNumber';
      case RequestType.TYPE06:
        return 'requestId';
      default:
        return 'policyNumber';
    }
  }
}
