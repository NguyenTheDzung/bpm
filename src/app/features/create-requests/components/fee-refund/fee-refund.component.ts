import {RequestService} from '@requests/services/request.service';
import {FileCollection} from './../../models/collection.model';
import {Component, Injector, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren} from '@angular/core';
import {Validators} from '@angular/forms';
import {
  createErrorMessage,
  getValueDateTimeLocal,
  updateValidity,
  validateAllFormFields,
} from '@cores/utils/functions';
import {IdentificationType, MAX_FILE_SIZE, Pattern, regexNameFormat} from '@cores/utils/constants';
import {BaseActionComponent} from '@shared/components';
import {FileUpload} from 'primeng/fileupload';
import {typeFileCollection} from '../../utils/constants';
import {Bank} from '@detail-request/shared/models/detail-request.model';
import {CollectionService} from '@create-requests/service/collection.service';
import {PaymentMethodCode} from '@cores/utils/enums';
import {CommonModel} from '@common-category/models/common-category.model';
import {CollectionDetailRequestModel} from '@detail-request/shared/models/collection-detail.model';
import {CustomValidators} from '@cores/utils/custom-validators';

@Component({
  selector: 'app-fee-refund',
  templateUrl: './fee-refund.component.html',
  styleUrls: ['./fee-refund.component.scss'],
})
export class FeeRefundComponent extends BaseActionComponent implements OnInit, OnChanges {
  @ViewChildren('fileInput')
  fileInputs: QueryList<FileUpload> | undefined;
  @Input() itemType: string = '';
  @Input() itemRequest?: CollectionDetailRequestModel;
  files: FileCollection[] = typeFileCollection;
  maxDate = new Date();
  endDate = new Date();
  maxFileSize: number = 0;
  paymentMethod = PaymentMethodCode;

  paymentMethodList: CommonModel[] = [];
  identificationTypeList: CommonModel[] = [];
  genderList: CommonModel[] = [];
  bankDropList: Bank[] = [];
  isSubmit = false;
  today = new Date();

  override state = {
    bankDropList: <Bank[]>[],
    requestId: '',
  };

  override form = this.fb.group({
    requestId: null,
    paymentMethod: [null, Validators.required],
    customerName: [null, [Validators.required, Validators.pattern(regexNameFormat)]],
    identificationType: [null, Validators.required],
    identificationNumber: [null, Validators.required],
    identificationExpired: [null, Validators.required],
    refundAmount: [null, Validators.required],
    accountNumber: [null, Validators.required],
    bankBranch: [null, Validators.required],
    paymentReceiveDate: [null, Validators.required],
    bankCode: [null, Validators.required],
    gender: [null, Validators.required],
    placeOfIssue: [null, Validators.required],
    email: [null, [Validators.required, CustomValidators.email]],
  });

  constructor(inject: Injector, private requestService: RequestService, private collectionService: CollectionService) {
    super(inject, requestService);
    this.files[0].required = true;
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.requestService.getState().subscribe({
      next: (state) => {
        this.bankDropList = state.listBank || [];
        this.paymentMethodList = state.listPaymentMethod || [];
        this.identificationTypeList = state.listIdentifyType || [];
        this.genderList = state.listGender || [];
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
    this.form.get('paymentMethod')?.valueChanges.subscribe((value) => {
      if (value === PaymentMethodCode.Banking) {
        updateValidity(this.form.get('bankBranch'), null);
        updateValidity(this.form.get('accountNumber'), Validators.required);
      } else {
        updateValidity(this.form.get('bankBranch'), Validators.required);
        updateValidity(this.form.get('accountNumber'), null);
      }
    });
    this.form.get('identificationType')?.valueChanges.subscribe((value) => {
      if (value === IdentificationType.CCCD || value === IdentificationType.CMND) {
        updateValidity(this.form.get('identificationNumber'), [
          Validators.required,
          Validators.pattern(Pattern.IDENTIFY_CARD),
        ]);
      } else if (value === IdentificationType.PASSPORT) {
        updateValidity(this.form.get('identificationNumber'), [
          Validators.required,
          Validators.pattern(Pattern.PASSPORT),
        ]);
      } else if (value === IdentificationType.GPLX) {
        updateValidity(this.form.get('identificationNumber'), [
          Validators.required,
          Validators.pattern(Pattern.DRIVING_LICENSE),
        ]);
      } else {
        updateValidity(this.form.get('identificationNumber'), Validators.required);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['itemType']) {
      this.form.reset();
      this.clearFile();
      this.isSubmit = false;
    }
    if (changes['itemRequest'] && this.itemRequest) {
      const data = {
        requestId: this.itemRequest?.requestId,
        paymentMethod: this.itemRequest?.paymentMethod,
        customerName: this.itemRequest?.customerName,
        identificationType: this.itemRequest?.idType,
        identificationNumber: this.itemRequest?.idNo,
        identificationExpired: this.itemRequest?.dateOfIssue,
        refundAmount: +this.itemRequest.refundAmount!,
        accountNumber: this.itemRequest?.beneficiaryAccount,
        bankBranch: this.itemRequest?.branch,
        paymentReceiveDate: this.itemRequest?.paymentDate,
        bankCode: this.itemRequest?.bankCode,
        gender: this.itemRequest?.gender,
        placeOfIssue: this.itemRequest?.placeOfIssue,
        email: this.itemRequest?.email,
      };
      this.form.patchValue(data);
      this.form.disable();
    }
  }

  override save() {
    if (this.itemRequest) {
      return this.createPending();
    }
    if (this.endDate > this.maxDate || this.maxFileSize > MAX_FILE_SIZE) {
      return;
    }
    const data = {
      requestType: this.requestType.TYPE08,
      refundRequestCreateDTO: this.getDataForm(),
    };
    data.refundRequestCreateDTO.identificationExpired = getValueDateTimeLocal(
      data.refundRequestCreateDTO.identificationExpired
    );
    data.refundRequestCreateDTO.paymentReceiveDate = getValueDateTimeLocal(
      data.refundRequestCreateDTO.paymentReceiveDate
    );
    if (data.refundRequestCreateDTO.paymentMethod === PaymentMethodCode.Banking?.toUpperCase()) {
      delete data.refundRequestCreateDTO.bankBranch;
    } else {
      delete data.refundRequestCreateDTO.accountNumber;
    }
    data.refundRequestCreateDTO.bankName = this.bankDropList?.find(
      (item: any) => item.citadCode === data.refundRequestCreateDTO.bankCode
    )?.name;
    if (this.form?.status === 'VALID') {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          this.create(data);
        }
      });
    } else {
      validateAllFormFields(this.form!);
    }
  }

  override create(data: any) {
    data.requestFileDTOs = this.mapFiles();
    if (this.maxFileSize > MAX_FILE_SIZE) {
      return this.messageService.error('MESSAGE.TOTAL_MAX_FILE_SIZE_UPLOAD');
    }
    if (data.requestFileDTOs.length > 0) {
      this.loadingService.start();
      this.collectionService.createApiCollection(data).subscribe({
        next: () => {
          this.form.reset();
          this.clearFile();
          this.files.forEach((item) => {
            item.file = null;
            item.content = null;
            item.mimeType = null;
            item.extension = null;
          });
          this.messageService.success('MESSAGE.SUCCESS');
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(`MESSAGE.${err?.error?.messageKey ? err.error?.messageKey : 'E_INTERNAL_SERVER'}`);
          this.loadingService.complete();
        },
      });
    } else {
      this.messageService.error('MESSAGE.FILE_NULL');
    }
  }

  createPending() {
    const data = {
      requestFileDTOs: this.mapFiles(),
      requestId: this.itemRequest?.requestId,
      requestType: this.requestType.TYPE08,
    };
    if (data.requestFileDTOs.length === 0) {
      return this.messageService.error('MESSAGE.FILE_NULL');
    }
    if (this.maxFileSize > MAX_FILE_SIZE) {
      return this.messageService.error('MESSAGE.TOTAL_MAX_FILE_SIZE_UPLOAD');
    }
    this.loadingService.start();
    this.collectionService.createPending(data).subscribe({
      next: () => {
        this.isSubmit = true;
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  mapFiles() {
    const requestFileDTOs: any[] = [];
    this.maxFileSize = 0;
    this.files.forEach((item) => {
      if (item.file) {
        item.file.forEach((file, i) => {
          this.maxFileSize += file.size;
          requestFileDTOs.push({
            name: file.name,
            type: item.type,
            extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
            content: item.content![i],
            mimeType: file.type,
          });
        });
      }
    });
    return requestFileDTOs;
  }

  uploadHandler(event: FileUpload, index: number) {
    this.files[index].content = [];
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.files[index].content?.push(
          (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1)
        );
      };
    });
    this.files[index].file = event?.files;
  }

  clearFile() {
    this.fileInputs?.forEach((item: FileUpload) => {
      item.clear();
    });
  }
}
