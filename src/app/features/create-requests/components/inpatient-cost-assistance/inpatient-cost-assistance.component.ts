import { BaseComponent } from '@shared/components';
import { Component, Injector, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  BankInfo,
  BankPolicy,
  ClaimForm,
  CreateClaim,
  FileTypeClaim,
  Identification,
  LADetail,
} from '@create-requests/models/claim.model';
import { CreateClaimService } from '@create-requests/service/create-claim.service';
import { combineLatest } from 'rxjs';
import { FILE_TYPE_CLAIM_01, MAX_FILE_SIZE, SAP_IDENTIFICATION_TYPE_CODE } from '@cores/utils/constants';
import { CommonModel } from '@common-category/models/common-category.model';
import { RequestService } from '@requests/services/request.service';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import { filter, orderBy } from 'lodash';
import { CustomValidators } from '@cores/utils/custom-validators';
import { FileAttachment } from '@cores/models/file-attachment.model';
import { environment } from '@env';
import { Bank } from '@requests/models/request.model';
import { IErrorResponse } from '@cores/models/response.model';
import * as moment from 'moment';
import { FileUpload } from 'primeng/fileupload';
import { CommonClaimCategory } from '@cores/models/common-claim.model';

@Component({
  selector: 'inpatient-cost-assistance',
  templateUrl: './inpatient-cost-assistance.component.html',
  styleUrls: ['./inpatient-cost-assistance.component.scss'],
})
export class InpatientCostAssistanceComponent extends BaseComponent implements OnInit {
  listIdentityType: CommonModel[] = [];
  listResourceAttachmentType: CommonClaimCategory[] = [];
  listBenefit: CommonClaimCategory[] = [];
  listFacility: CommonClaimCategory[] = [];
  listDiseases: CommonClaimCategory[] = [];
  listFatca: CommonClaimCategory[] = [];
  listBank: Bank[] = [];
  listBankPolicy: BankPolicy[] = [];
  listInsured: LADetail[] = [];
  files: FileAttachment[] = [];
  fileTypes: FileTypeClaim[] = [];
  acceptTypeFile = '.pdf,image/*,.xml';
  formClaim: FormGroup = new FormGroup({
    policyNumber: new FormControl(null, [Validators.required, Validators.maxLength(12)]),
    holder: new FormGroup({
      name: new FormControl({ value: null, disabled: true }, Validators.required),
      code: new FormControl({ value: null, disabled: true }, Validators.required),
    }),
    benefitCodes: new FormControl(null, Validators.required),
    diseaseCodes: new FormControl(null, Validators.required),
    bpInsuredCode: new FormControl(null, Validators.required),
    facilityCode: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, CustomValidators.email]),
    phone: new FormControl({ value: null, disabled: true }),
    createDate: new FormControl(new Date(), Validators.required),
    submitFirstRequest: new FormControl(new Date(), Validators.required),
    startDateEvent: new FormControl(null, Validators.required),
    endDateEvent: new FormControl(null, Validators.required),
    paymentMethod: new FormControl(null, Validators.required),
    resourceAttachmentType: new FormControl(null, Validators.required),
    bankInfo: new FormGroup({
      name: new FormControl({ value: null, disabled: true }),
      code: new FormControl({ value: null, disabled: false }),
      branch: new FormControl({ value: null, disabled: true }),
      accountName: new FormControl({ value: null, disabled: true }),
      accountNumber: new FormControl({ value: null, disabled: true }),
    }),
    identification: new FormGroup({
      name: new FormControl(null),
      code: new FormControl(null),
      branch: new FormControl(null),
      type: new FormControl({ value: null, disabled: true }),
      idNumber: new FormControl({ value: null, disabled: true }),
      issuedDate: new FormControl({ value: null, disabled: true }),
      issuedPlace: new FormControl({ value: null, disabled: true }),
    }),
  });
  @ViewChildren(FileUpload) fileUploads?: QueryList<FileUpload>;
  readonly paymentMethod = {
    bank: 'BANKING',
    cash: 'CASH',
  };

  constructor(injector: Injector, private service: CreateClaimService, private requestService: RequestService) {
    super(injector);

    this.formClaim.get('paymentMethod')?.valueChanges.subscribe(
      (valueChanges : 'BANKING' | 'CASH' | null) => {
        if(!valueChanges) return
        // Nếu là banking thì sẽ thực hiện bỏ require trên form code của identification
        // nếu là cash thì thực hiện thêm require
        switch ( valueChanges ){
          case "BANKING":
            this.formClaim.get('identification')?.get('code')?.hasValidator(Validators.required) &&
            this.formClaim.get('identification')?.get('code')?.removeValidators(Validators.required)
            break;
          case "CASH":
            !this.formClaim.get('identification')?.get('code')?.hasValidator(Validators.required) &&
            this.formClaim.get('identification')?.get('code')?.addValidators(Validators.required)
            break;
        }
        this.formClaim.get('identification')?.get('code')?.updateValueAndValidity()
      }
    )
  }

  ngOnInit() {
    this.loadingService.start();
    combineLatest([
      this.service.getFatca(),
      this.service.getDiseases(),
      this.service.getFacility(),
      this.service.getBenefit(),
      this.service.getResourceDoc(),
      this.requestService.getBankCode(),
      this.commonService.getCommonByCodes([FILE_TYPE_CLAIM_01, SAP_IDENTIFICATION_TYPE_CODE]),
    ]).subscribe({
      next: ([fatca, diseases, facility, benefit, resourceDoc, banks, categories]) => {
        this.listFatca = fatca;
        this.listIdentityType = filter(categories, (item) => item.commonCategoryCode === SAP_IDENTIFICATION_TYPE_CODE);
        this.listResourceAttachmentType = resourceDoc;
        this.listBenefit = benefit;
        this.listDiseases = diseases;
        this.listFacility = facility;
        this.listBank = banks;
        const fileTypes = orderBy(
          filter(categories, (item) => item.commonCategoryCode === FILE_TYPE_CLAIM_01),
          (item) => item.orderNum
        );
        fileTypes?.forEach((item) => {
          const required = item.value === 'required';
          const validators = [CustomValidators.file({ maxSize: MAX_FILE_SIZE, acceptType: this.acceptTypeFile })];
          if (required) {
            validators.push(Validators.required);
          }
          this.fileTypes.push({
            label: item.name!,
            type: item.code!,
            file: null,
            content: null,
            maxSize: MAX_FILE_SIZE,
            acceptType: this.acceptTypeFile,
            multiLanguage: item.multiLanguage,
            required,
          });
          this.formClaim.addControl(item.code!, new FormControl(null, validators));
        });
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
    this.controls.identification?.controls?.code?.valueChanges.subscribe((value) => {
      if (value) {
        const bank = this.listBank.find((item) => item.citadCode === value)!;
        this.controls.identification?.controls?.name?.setValue(bank?.name);
      }
    });
    this.controls.bankInfo?.controls?.code?.valueChanges.subscribe((value) => {
      const itemBank = this.listBankPolicy.find((item) => item.code === value)!;
      const data: BankInfo = {
        name: itemBank?.name,
        code: itemBank?.code,
        accountName: itemBank?.accountName,
        accountNumber: itemBank?.account,
        branch: itemBank?.branch,
      };
      this.controls?.bankInfo?.patchValue(data, { emitEvent: false, onlySelf: true });
    });
  }

  save() {
    if (this.formClaim.invalid || this.loadingService.loading) {
       this.formClaim.markAllAsTouched();
       return
    }
    let data: CreateClaim = this.data;
    data.startDateEvent = getValueDateTimeLocal(data.startDateEvent)!;
    data.endDateEvent = getValueDateTimeLocal(data.endDateEvent)!;
    data.createDate = getValueDateTimeLocal(data.createDate)!;
    data.submitFirstRequest = getValueDateTimeLocal(data.submitFirstRequest)!;
    data.fatca = this.listFatca?.map((item) => {
      return { code: item.code, answer: false };
    });
    data.requestFileDTOS = [];
    data.frmSystem = environment.app.realm;
    this.fileTypes?.forEach((item) => {
      data.requestFileDTOS = [...data.requestFileDTOS, ...(this.formClaim.get(item.type)!.value ?? [])];
    });
    data.benefitDetail = this.listBenefit.filter((item) => this.controls.benefitCodes.value?.includes(item.code));
    data.disease = this.listDiseases.filter((item) => this.controls.diseaseCodes.value?.includes(item.code));
    data.facility = this.listFacility.find((item) => item.code === this.controls.facilityCode.value)!;
    data.insured = this.listInsured.find((item) => item.code === this.controls.bpInsuredCode.value)!;
    if (data.paymentMethod === this.paymentMethod.bank) {
      delete data.identification;
    } else {
      delete data.bankInfo;
    }

    this.loadingService.start();
    this.service.create(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.formClaim.reset();
        this.controls.createDate.setValue(getValueDateTimeLocal(new Date()));
        this.controls.submitFirstRequest.setValue(getValueDateTimeLocal(new Date()));
        this.fileUploads?.forEach((item) => item.clear());
        this.loadingService.complete();
      },
      error: (e: IErrorResponse) => {
        if (e?.error?.messageKey == 'SUBMIT_FIRST_REQUEST_EXIST') {
          const messageError = this.translateService.instant('MESSAGE.SUBMIT_FIRST_REQUEST_EXIST', {
            startEventInsuranceDate: moment(this.formClaim.get('startDateEvent')?.value).format('DD/MM/YYYY'),
          });
          this.messageService.error(messageError);
          this.loadingService.complete();
          return;
        }
        this.messageService.error(createErrorMessage(e));
        this.loadingService.complete();
      },
    });
  }

  getInfo(value: string) {
    if (value && !this.controls.policyNumber.errors) {
      this.formClaim.controls['bankInfo'].reset();
      this.loadingService.start();
      this.service.getListPolicyByPartner(value).subscribe({
        next: (policyInfoBp) => {
          this.controls.holder.controls.code.setValue(policyInfoBp.pbNumber);
          this.controls.holder.controls.name.setValue(policyInfoBp.fullName);
          const identification: Identification = {
            type: policyInfoBp.cashType,
            issuedPlace: policyInfoBp.cashAddress,
            issuedDate: policyInfoBp.cashDate,
            idNumber: policyInfoBp.cashNum,
          };
          this.controls.identification?.patchValue(identification);
          this.listInsured =
            policyInfoBp.laDetails?.map((item) => {
              return { ...item, code: `${item.bpNumber} - ${item.fullName}` };
            }) ?? [];
          this.listBankPolicy =
            policyInfoBp.bankInfos?.map((item) => {
              return { ...item, displayName: `${item.code}-${item.name}` };
            }) ?? [];
          this.controls.email.setValue(policyInfoBp?.email ?? null);
          this.controls.phone.setValue(policyInfoBp?.phone ?? null);
          this.loadingService.complete();
        },
        error: (e) => {
          this.formClaim.reset({
            policyNumber: this.formClaim.controls['policyNumber'].value,
            createDate: this.formClaim.controls['createDate'].value,
            submitFirstRequest: this.formClaim.controls['submitFirstRequest'].value,
          });
          this.messageService.error(createErrorMessage(e));
          this.loadingService.complete();
        },
      });
    }
  }

  uploadHandler(event: { files: File[] }, typeFile: string) {
    this.formClaim.get(typeFile)?.setValue([]);
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const filesOfType: FileAttachment[] = this.formClaim.get(typeFile)?.value ?? [];
        filesOfType.push({
          name: file.name,
          size: file.size,
          lastModified: file.lastModified,
          mimeType: file.type,
          extension: `.${file.name.split('.').pop()!}`,
          content: (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1),
          type: typeFile,
        });
        this.formClaim.get(typeFile)?.setValue(filesOfType);
        this.formClaim.get(typeFile)?.markAsTouched();
        this.formClaim.get(typeFile)?.markAsDirty();
      };
    });
  }

  removeFile(file: File, typeFile: string) {
    let filesOfType: FileAttachment[] = this.formClaim.get(typeFile)?.value ?? [];
    filesOfType = filesOfType.filter(
      (item) => item.name !== file.name && item.size !== file.size && item.lastModified !== file.lastModified
    );
    this.formClaim.get(typeFile)?.setValue(filesOfType);
  }

  get controlBenefit() {
    return this.controls.benefitCodes as FormControl;
  }

  get data(): CreateClaim {
    let item = this.formClaim.getRawValue();
    this.fileTypes?.forEach((file) => {
      delete item[file.type];
    });
    return item;
  }

  get controls(): ClaimForm {
    return <ClaimForm>this.formClaim.controls;
  }
}
