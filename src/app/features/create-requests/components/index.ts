import { ContractRestoreComponent } from './contract-restore/contract-restore.component';
import { FeeRefundComponent } from './fee-refund/fee-refund.component';
import { ConfigInsuranceFeeComponent } from './config-insurance-fee/config-insurance-fee.component';
import { FormRefundComponent } from './form-refund/form-refund.component';
import { SubmitPayLicenseComponent } from './submit-pay-license/submit-pay-license.component';
import { InpatientCostAssistanceComponent } from '@create-requests/components/inpatient-cost-assistance/inpatient-cost-assistance.component';
import { ClaimAdditionalAttachmentComponent } from '@create-requests/components/claim-additional-attachment/claim-additional-attachment.component';

export const components = [
  FormRefundComponent,
  ConfigInsuranceFeeComponent,
  FeeRefundComponent,
  SubmitPayLicenseComponent,
  ContractRestoreComponent,
  InpatientCostAssistanceComponent,
  ClaimAdditionalAttachmentComponent,
];

export * from './form-refund/form-refund.component';
export * from './config-insurance-fee/config-insurance-fee.component';
export * from './fee-refund/fee-refund.component';
export * from './submit-pay-license/submit-pay-license.component';
export * from './contract-restore/contract-restore.component';
export * from './inpatient-cost-assistance/inpatient-cost-assistance.component';
export * from './claim-additional-attachment/claim-additional-attachment.component';
