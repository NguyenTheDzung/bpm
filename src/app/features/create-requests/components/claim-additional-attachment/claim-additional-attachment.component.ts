import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { ClaimPending, FileClaimAdditional, FileTypeClaim } from '@create-requests/models/claim.model';
import { forkJoin } from 'rxjs';
import { CreateClaimService } from '@create-requests/service/create-claim.service';
import { FILE_TYPE_CLAIM_01, MAX_FILE_SIZE } from '@cores/utils/constants';
import { cloneDeep, isEmpty, orderBy } from 'lodash';
import { CustomValidators } from '@cores/utils/custom-validators';
import { FormControl } from '@angular/forms';
import { CommonModel } from '@common-category/models/common-category.model';
import { ClaimService } from '../../../claims/services/claim.service';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import { DataTable } from '@cores/models/data-table.model';
import { PaginatorModel } from '@cores/models/paginator.model';
import { environment } from '@env';
import { HistoryClaimService } from '@history/services/history-claim.service';
import { HistoryClaim, ParamSearch } from '@history/models/history.model';
import { RequestType } from '@create-requests/utils/constants';
import { CommonClaimCategory } from '@cores/models/common-claim.model';

@Component({
  selector: 'claim-additional-attachment',
  templateUrl: './claim-additional-attachment.component.html',
  styleUrls: ['./claim-additional-attachment.component.scss'],
})
export class ClaimAdditionalAttachmentComponent extends BaseComponent implements OnInit {
  formClaim = this.fb.group({
    claimId: null,
    requestId: null,
    requestType: null,
    policyNumber: null,
    bp: null,
    bpName: null,
    laFullName: null, // bpLA + laName
    sourceDoc: null,
    sourceDocOrigin: null,
    createdDate: null,
    submitFirstRequest: null,
    phone: null,
    email: null,
    note: null,
  });
  fileTypes: FileTypeClaim[] = [];
  acceptTypeFile = '.pdf,image/*,.xml';
  formSearch = this.fb.group({
    claimId: null,
    typeRequest: null,
    policyNumber: null,
    la: null,
    bpLA: null,
  });
  listResourceDoc: CommonClaimCategory[] = [];
  listRequestType: CommonModel[] = [];
  listStatus: CommonModel[] = [];
  dataTable: DataTable<HistoryClaim> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  selectedItem: HistoryClaim | null = null;

  constructor(
    injector: Injector,
    private createClaimService: CreateClaimService,
    private claimService: ClaimService,
    private historyService: HistoryClaimService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.formClaim.disable();
    this.formClaim.get('note')?.enable();
    this.formClaim.get('sourceDoc')?.enable();
    forkJoin([
      this.createClaimService.getResourceDoc(),
      this.commonService.getCommonCategory(FILE_TYPE_CLAIM_01),
      this.claimService.getCategories(),
    ]).subscribe({
      next: ([resourceDoc, listFileType, categories]) => {
        this.listRequestType = categories.listRequestType?.filter((item) => [RequestType.TYPE11].includes(item.value));
        this.listStatus = categories.listStatusClaim;
        this.listResourceDoc = resourceDoc;
        const fileTypes = orderBy(listFileType, (item) => item.orderNum);
        fileTypes?.forEach((item) => {
          const required = item.value === 'required';
          const validators = [CustomValidators.file({ maxSize: MAX_FILE_SIZE, acceptType: this.acceptTypeFile })];
          this.fileTypes.push({
            label: item.name!,
            type: item.code!,
            file: null,
            content: null,
            maxSize: MAX_FILE_SIZE,
            acceptType: this.acceptTypeFile,
            multiLanguage: item.multiLanguage,
            required,
          });
          this.formClaim.addControl(item.code!, new FormControl(null, validators));
        });
      },
    });
  }

  search(firstPage?: boolean) {
    let dataForm = this.formSearch.getRawValue(),
      isHasValue = false;
    for (const [key, value] of Object.entries(dataForm)) {
      if (!isEmpty(value)) {
        isHasValue = true;
        break;
      }
    }
    if (!isHasValue) {
      return this.messageService.error('Vui lòng nhập các trường thông tin để thực hiện tìm kiếm');
    }
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params: ParamSearch = {
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
      ...this.formSearch.getRawValue(),
    };
    this.historyService.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (firstPage && this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  onReset() {
    this.formSearch.reset();
    this.dataTable = {
      content: [],
      currentPage: 0,
      size: 10,
      totalElements: 0,
      totalPages: 0,
      first: 0,
      numberOfElements: 0,
    };
    // this.search(true);
  }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.search();
  }

  uploadHandler(event: { files: File[] }, typeFile: string) {
    this.formClaim.get(typeFile)?.setValue([]);
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const filesOfType: FileClaimAdditional[] = this.formClaim.get(typeFile)?.value ?? [];
        filesOfType.push({
          fileName: file.name,
          size: file.size,
          lastModified: file.lastModified,
          mimeType: file.type,
          typeFile: `.${file.name.split('.').pop()!}`,
          extension: `.${file.name.split('.').pop()!}`,
          content: (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1),
          fileTypes: typeFile,
          objectId: '',
          objectType: 'CLAIM',
        });
        this.formClaim.get(typeFile)?.setValue(filesOfType);
        this.formClaim.get(typeFile)?.markAsTouched();
        this.formClaim.get(typeFile)?.markAsDirty();
      };
    });
  }

  removeFile(file: File, typeFile: string) {
    let filesOfType: FileClaimAdditional[] = this.formClaim.get(typeFile)?.value ?? [];
    filesOfType = filesOfType.filter(
      (item) => item.fileName !== file.name && item.size !== file.size && item.lastModified !== file.lastModified
    );
    this.formClaim.get(typeFile)?.setValue(filesOfType);
  }

  save() {
    if (this.loadingService.loading || !this.selectedItem || this.formClaim.invalid) {
      this.formClaim.markAllAsTouched();
      return;
    }
    let files: FileClaimAdditional[] = [];
    this.fileTypes.forEach((item) => {
      if (this.formClaim.get(item.type)?.value) {
        files = [...files, ...(this.formClaim.get(item.type)!.value ?? [])];
      }
    });
    const data: ClaimPending = {
      requestId: this.selectedItem.requestId,
      fromSystem: environment.app.realm,
      dateUpdate: getValueDateTimeLocal(new Date())!,
      executorUpdate: this.currUser.username,
      updateInfo: {
        note: this.formClaim.get('note')?.value,
        sourceDoc: this.formClaim.get('sourceDoc')?.value,
        files,
      },
    };
    this.loadingService.start();
    this.claimService.submitPendingAttachment(data).subscribe({
      next: () => {
        this.selectedItem = null;
        this.onReset();
        this.formClaim.reset();
        this.loadingService.complete();
        this.messageService.success('MESSAGE.SUCCESS');
      },
      error: (e) => {
        this.messageService.error(createErrorMessage(e));
        this.loadingService.complete();
      },
    });
  }

  doSelectItem(item: HistoryClaim) {
    this.selectedItem = item;
    const data: any = cloneDeep(item);
    data.sourceDocOrigin = cloneDeep(data.sourceDoc);
    data.laFullName = `${data.bpLA ?? ''} - ${data.bpLAName ?? ''}`;
    data.sourceDoc = null;
    this.formClaim.patchValue(data);
  }

  checkRequiredSourceDoc() {
    return !!this.fileTypes.find((item) => !isEmpty(this.formClaim.get(item.type)?.value));
  }
}
