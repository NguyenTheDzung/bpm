import { Component, Injector, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  cleanDataForm,
  createErrorMessage,
  getValueDateTimeLocal,
  validateAllFormFields,
} from '@cores/utils/functions';
import { BaseActionComponent } from '@shared/components';
import * as _ from 'lodash';
import { CollectionService } from '@create-requests/service/collection.service';
import { ContractRestoreModel } from '@create-requests/models/collection.model';
import { RequestService } from 'src/app/features/requests/services/request.service';
import { CommonModel } from '@common-category/models/common-category.model';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-contract-restore',
  templateUrl: './contract-restore.component.html',
  styleUrls: ['./contract-restore.component.scss'],
})
export class ContractRestoreComponent extends BaseActionComponent {
  integration: ContractRestoreModel[] = [];
  selectedItem: ContractRestoreModel[] = [];
  listPeriodicFeePayment!: CommonModel[];
  override form = this.fb.group({
    policyNumber: ['', [Validators.required, Validators.minLength(12), Validators.maxLength(12)]],
    mainProduct: [{ value: '', disabled: true }],
    reason: ['', [Validators.maxLength(1000), Validators.required]],
    policyHolderName: [{ value: '', disabled: true }],
    effectiveDate: [{ value: '', disabled: true }],
    paidDate: [{ value: '', disabled: true }],
    periodicFeePayment: [{ value: '', disabled: true }],
    suggestedContent: ['', [Validators.maxLength(1000), Validators.required]],
    fpl9dto: null,
    feeAttachmentRequestId: null,
  });
  @Input() itemType: string = '';

  constructor(inject: Injector, private collectionService: CollectionService, private requestService: RequestService) {
    super(inject, collectionService);

    if (!_.isEmpty(this.state?.policyNumber)) {
      this.form.controls['policyNumber'].setValue(this.state?.policyNumber);
      this.findPolicyInfo();
    } else {
      this.requestService.getState().subscribe({
        next: (state) => {
          this.listPeriodicFeePayment = state.listPeriodicFeePayment || [];
        },
      });
    }
  }

  findPolicyInfo() {
    const data = cleanDataForm(this.form);
    if (
      _.isEmpty(data?.policyNumber) ||
      this.loadingService.loading ||
      data.policyNumber?.length < 12 ||
      data.policyNumber?.length > 12
    ) {
      return;
    }
    const info = {
      paymentLot: 'X',
      receivables: '',
      downPayment: '',
      chronology: '',
      page: 0,
      pageSize: 99999,
      policyNumber: data.policyNumber,
    };
    this.loadingService.start();
    forkJoin([
      this.collectionService.getInfoIntegration(info),
      this.collectionService.getInfoFromSAP(data.policyNumber),
      this.requestService.getState(),
    ]).subscribe({
      next: ([dataTb, res, state]) => {
        this.listPeriodicFeePayment = state.listPeriodicFeePayment || [];
        let periodicFeePayment = this.listPeriodicFeePayment?.find(
          (item) => +item.value === +res.data?.policyInfoResponseToBpmDTO?.payfrqCd
        )?.code;

        let dataForm = {
          policyHolderName: res?.data?.policyInfoResponseToBpmDTO?.holderDTO?.fullName || '',

          effectiveDate: res.data?.policyInfoResponseToBpmDTO?.recordTS
            ? new Date(res.data?.policyInfoResponseToBpmDTO?.polbegDT)
            : null,
          paidDate: res.data?.policyInfoResponseToBpmDTO.paidDate
            ? new Date(res.data?.policyInfoResponseToBpmDTO.paidDate)
            : null,
          policyNumber: data.policyNumber,
          periodicFeePayment: periodicFeePayment,
          mainProduct: res.data?.policyInfoResponseToBpmDTO?.longText,
        };
        this.integration = dataTb?.data?.paymentLot || [];
        this.form.patchValue(dataForm);
        this.ref.detectChanges();
        this.loadingService.complete();
      },
      error: (e) => {
        this.integration = [];
        this.messageService.error(createErrorMessage(e));
        this.loadingService.complete();
      },
    });
  }

  override save() {
    const data = cleanDataForm(this.form);
    let arr = [];
    if (this.selectedItem?.length > 0) {
      for (let i of this.selectedItem) {
        arr.push(i);
      }
    }
    data.fpl9dto = arr;
    data.effectiveDate = getValueDateTimeLocal(data.effectiveDate);
    data.paidDate = getValueDateTimeLocal(data.paidDate);
    data.feeAttachmentRequestId = this.state?.requestId || null;
    const mapValue = {
      policyNumber: data.policyNumber,
      requestType: this.itemType || '10',
      reinstatementPolicyRequestCreateDTO: data,
    };
    if (_.isEmpty(this.selectedItem)) {
      this.messageService.warn('MESSAGE.ITEMLOT_REQUIRED');
      return;
    }
    if (this.form?.status === 'INVALID') {
      validateAllFormFields(this.form!);
      return;
    } else {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          this.create(mapValue);
        }
      });
    }
  }

  override create(data: any) {
    this.loadingService.start();
    this.collectionService.createApiCollection(data).subscribe({
      next: () => {
        this.form.reset();
        this.integration = [];
        this.selectedItem = [];
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
        this.refDialog.close(true);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }
}
