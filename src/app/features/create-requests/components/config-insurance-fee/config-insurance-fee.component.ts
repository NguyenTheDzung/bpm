import {FileCollection} from './../../models/collection.model';
import {Component, Injector, Input, OnChanges, QueryList, SimpleChanges, ViewChildren} from '@angular/core';
import {Validators} from '@angular/forms';
import {createErrorMessage, getValueDateTimeLocal, validateAllFormFields} from '@cores/utils/functions';
import {MAX_FILE_SIZE} from '@cores/utils/constants';
import {BaseActionComponent} from '@shared/components';
import * as _ from 'lodash';
import {isEmpty} from 'lodash';
import {FileUpload} from 'primeng/fileupload';
import {StateRefund} from '../../models/refund.model';
import {typeFileCollection, ZCOL_AR} from '../../utils/constants';
import {CollectionService} from '@create-requests/service/collection.service';
import {CustomValidators} from '@cores/utils/custom-validators';
import {CollectionDetailRequestModel} from '@detail-request/shared/models/collection-detail.model';

@Component({
  selector: 'app-config-insurance-fee',
  templateUrl: './config-insurance-fee.component.html',
  styleUrls: ['./config-insurance-fee.component.scss'],
})
export class ConfigInsuranceFeeComponent extends BaseActionComponent implements OnChanges {
  @ViewChildren('fileInput') fileInputs: QueryList<FileUpload> | undefined;
  @Input() itemType: string = '';
  @Input() itemRequest?: CollectionDetailRequestModel;
  files: FileCollection[] = typeFileCollection;
  maxFileSize: number = 0;
  override state: StateRefund | undefined;
  override form = this.fb.group({
    onAttachmentPolicyNumber: [null],
    feeAdjustPolicyNumber: [null, Validators.required],
    onAttachmentAmount: [null, Validators.required],
    policyHolderName: [null],
    adjustmentAmount: [null, Validators.required],
    signDate: [null, Validators.required],
    crmTicket: [null],
    icEmail: ['', [Validators.required, CustomValidators.email]],
    requestId: null,
  });

  constructor(inject: Injector, private colService: CollectionService) {
    super(inject, colService);
    this.files[0].required = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['itemType']) {
      this.form.reset();
      this.clearFile();
    }
    if (changes['itemRequest'] && this.itemRequest) {
      const data = {
        requestId: this.itemRequest?.step1?.requestId,
        onAttachmentPolicyNumber: this.itemRequest?.step1?.onAttachmentPolicyNumber,
        feeAdjustPolicyNumber: this.itemRequest?.step1?.feeAdjustPolicyNumber,
        onAttachmentAmount: this.itemRequest?.step1?.onAttachmentAmount,
        policyHolderName: this.itemRequest?.step1?.policyHolderName,
        adjustmentAmount: this.itemRequest?.step1?.adjustmentAmount,
        signDate: this.itemRequest?.step1?.signDate,
        crmTicket: this.itemRequest?.step1?.crmTicket,
        icEmail: this.itemRequest?.step1?.icEmail,
      };
      this.form.patchValue(data);
      this.form.disable();
    }
  }

  checkPolicyNumber() {
    const data = this.getDataForm();
    if (
      isEmpty(data?.onAttachmentPolicyNumber) ||
      this.loadingService.loading ||
      data.onAttachmentPolicyNumber?.length < 12 ||
      data.onAttachmentPolicyNumber?.length > 12
    ) {
      return;
    }
    if (data?.onAttachmentPolicyNumber == data?.feeAdjustPolicyNumber) {
      this.messageService.error('MESSAGE.POLICY_EQUAL')
    }
    this.loadingService.start();
    this.colService.getInfoFromSAP(data.onAttachmentPolicyNumber).subscribe({
      next: () => {
        this.loadingService.complete();
      },
      error: (err) => {
        if (err.status == '404') {
          this.loadingService.complete();
          return this.messageService.error('MESSAGE.ON_ATTACHMENT_POLICY_NOT_EXIST');
        }
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  findDataPolicy() {
    const data = this.getDataForm();
    if (
      isEmpty(data?.feeAdjustPolicyNumber) ||
      this.loadingService.loading ||
      data.feeAdjustPolicyNumber?.length < 12 ||
      data.feeAdjustPolicyNumber?.length > 12
    ) {
      return;
    }
    if (data?.feeAdjustPolicyNumber == data?.onAttachmentPolicyNumber) {
      this.messageService.error('MESSAGE.POLICY_EQUAL')
    }
    this.loadingService.start();
    this.colService.getInfoFromSAP(data.feeAdjustPolicyNumber).subscribe({
      next: (res) => {
        this.loadingService.complete();
        const dataForm = {
          policyHolderName: res?.data?.policyInfoResponseToBpmDTO?.holderDTO?.fullName,
          icEmail: res?.data?.bpInfoOfAgent[0]?.emailResDTO[0]?.smtpAddr
        };
        this.form.patchValue(dataForm);
      },
      error: (err) => {
        if (err.status == '404') {
          this.loadingService.complete();
          return this.messageService.error('MESSAGE.FEE_ADJUST_POLICY_NOT_EXIST');
        }
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  override save() {
    validateAllFormFields(this.form!);
    const data = this.getDataForm();
    data.requestFileDTOs = this.handleFileSize();
    if (this.form?.invalid) {
      return;
    }
    if (this.maxFileSize > MAX_FILE_SIZE) {
      return this.messageService.error('MESSAGE.TOTAL_MAX_FILE_SIZE_UPLOAD');
    }
    if (data.requestFileDTOs.length === 0) {
      return this.messageService.error('MESSAGE.FILE_NULL');
    }
    if (data?.feeAdjustPolicyNumber == data?.onAttachmentPolicyNumber) {
      return this.messageService.error('MESSAGE.POLICY_EQUAL')
    }
    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        if (this.itemRequest && this.itemType === this.requestType.TYPE06) {
          return this.submitPending(data);
        } else {
          this.create(data);
        }
      }
    });
  }

  override create(data: any) {
    const valueForm = _.clone(this.getDataForm());
    const body = {
      requestType: this.requestType.TYPE07,
      insuranceAdjustmentRequestCreateDTO: {
        ...valueForm,
        signDate: getValueDateTimeLocal(valueForm.signDate),
      },
      requestFileDTOs: data.requestFileDTOs,
    };
    this.loadingService.start();
    this.colService.create(body).subscribe({
      next: () => {
        this.form.reset();
        this.clearFile();
        this.files.forEach((item) => {
          item.file = null;
          item.content = null;
          item.mimeType = null;
        });
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  submitPending(data: any) {
    data.requestType = this.requestType.TYPE07;
    data.requestId = this.itemRequest?.step1?.requestId;
    this.loadingService.start();
    this.colService.createPending(data).subscribe({
      next: () => {
        this.form.reset();
        this.clearFile();
        this.files.forEach((item) => {
          item.file = null;
          item.content = null;
          item.mimeType = null;
        });
        localStorage.removeItem('requestId');
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  uploadHandler(event: FileUpload, index: number) {
    this.files[index].content = [];
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.files[index].content?.push(
          (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1)
        );
      };
    });
    this.files[index].file = event?.files;
  }

  handleFileSize() {
    const requestFileDTOs: any[] = [];
    this.maxFileSize = 0;
    this.files.forEach((item) => {
      if (item.file) {
        item.file.forEach((file, i) => {
          this.maxFileSize += file.size;
          requestFileDTOs.push({
            name: file.name,
            type: ZCOL_AR,
            extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
            content: item.content![i],
            mimeType: file.type,
          });
        });
      }
    });
    return requestFileDTOs;
  }

  clearFile() {
    this.fileInputs?.forEach((item: FileUpload) => {
      item.clear();
    });
  }
}
