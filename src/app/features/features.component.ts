import { AfterContentInit, ChangeDetectorRef, Component, OnDestroy } from "@angular/core";
import { LoadingService } from "@cores/services/loading.service";
import { TranslateService } from "@ngx-translate/core";
import { MessageService, PrimeNGConfig } from "primeng/api";
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { debounceTime, delay, distinctUntilChanged, Observable, Subscription, tap } from "rxjs";
import { NotificationMessageService } from "@cores/services/message.service";
import { ConfirmDialogComponent } from "@shared/components";
import { SessionKey } from "@cores/utils/enums";
import { SessionService } from "@cores/services/session.service";
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from "@angular/router";
import { AuthService } from "@cores/services/auth.service";
import { CacheService } from "@cores/services/cache.service";

@Component({
  selector: "app-main",
  template: `
    <div [class]="classNameLayout">
      <app-menu (staticMenu)="onStaticMenu($event)"></app-menu>
      <div [ngClass]="{ 'layout-main': true, 'layout-overflow-hidden': loading$ | async }">
        <app-topbar></app-topbar>
        <app-breadcrumb></app-breadcrumb>
        <div class="layout-content">
          <app-loading [ngStyle]="{ display: (loading$ | async) ? 'block' : 'none' }"></app-loading>
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
    <p-toast></p-toast>
  `,
  providers: [MessageService, DialogService]
})
export class FeaturesComponent implements OnDestroy, AfterContentInit {
  subscription: Subscription[] = [];
  classNameLayout = "layout-wrapper layout-menu-light";
  loading$!: Observable<boolean>;

  constructor(
    private service: NotificationMessageService,
    private messageService: MessageService,
    private dialogService: DialogService,
    private loadingService: LoadingService,
    private ref: ChangeDetectorRef,
    private router: Router,
    private translate: TranslateService,
    private sessionService: SessionService,
    private authService: AuthService,
    private primeNGConfig: PrimeNGConfig,
    private cacheService: CacheService
  ) {
    this.subscription.push(
      this.sessionService.onStorageChange(SessionKey.UserProfile).subscribe(
        event => {
          if (!event.newValue) {
            this.router.navigateByUrl("/login");
            this.authService.userProfile = null;
            this.authService.setUserRoles([]);
          }
        }
      )
    );


    this.subscription.push(
      this.service.subjectMessage.subscribe((notify) => {
        this.messageService.add(notify);
      })
    );
    this.subscription.push(
      this.service.subjectDialog.subscribe((data) => {
        this.showConfirm(data.key, data.isNote);
      })
    );
    this.updatePrimengConfig();
    this.translate.onLangChange.subscribe(() => this.updatePrimengConfig());

    /**
     * Loading mỗi khi đổi page
     * do có nhiều module nặng và thực hiện bundle bằng lazy load
     * khiến cho thời gian chờ khi chuyển trang lâu
     * nên thực hiện loading mỗi khi
     * chuyển trang để tránh người dùng hiểu nhầm và thực hiện các hành động khác
     */
    this.subscription.push(
      this.router.events.pipe(
        tap((event) => {
          if(event instanceof NavigationStart){
            this.loadingService.start()
          }
        }),
        debounceTime(1000),
        tap((event) => {
          if (
            event instanceof NavigationEnd ||
            event instanceof NavigationCancel ||
            event instanceof NavigationError
          ) {
            this.loadingService.complete()
          }
        }),
      ).subscribe()
    )
  }

  updatePrimengConfig() {
    this.translate.get('primeng').subscribe(res => this.primeNGConfig.setTranslation(res));
  }

  showConfirm(key: string, isNote?: any) {
    if (key === "confirmReject" || key === "confirm") {
      const option: DynamicDialogConfig = {
        header: this.translate.instant(isNote ? "MESSAGE.messageReject" : "MESSAGE.confirm"),
        width: isNote ? "600px" : "400px",
        baseZIndex: 10000,
        data: isNote
      };
      const ref: DynamicDialogRef = this.dialogService.open(ConfirmDialogComponent, option);
      ref.onClose.subscribe((data) => {
        this.service.subjectDialog.next({ key: data?.confirm ? "accept" : "reject", data: data?.note });
      });
    }
  }

  onStaticMenu(isLock: boolean) {
    this.classNameLayout = isLock
      ? "layout-wrapper layout-menu-light layout-wrapper-static"
      : "layout-wrapper layout-menu-light";
  }

  ngOnDestroy(): void {
    this.dialogService.dialogComponentRefMap.forEach((dialogRef) => {
      dialogRef.destroy();
    });
    this.subscription.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  ngAfterContentInit() {
    this.loading$ = this.loadingService.loading$.pipe(delay(0), distinctUntilChanged());
  }
}
