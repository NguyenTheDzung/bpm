import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'common-category',
    loadChildren: () =>
      import('../configuration/common-category/common-category.module').then((m) => m.CommonCategoryModule),
  },
  {
    path: 'role',
    loadChildren: () => import('./role-category/role-category.module').then((m) => m.RoleCategoryModule),
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then((m) => m.UserModule),
  },
  {
    path: 'function',
    loadChildren: () => import('./function/function.module').then((m) => m.FunctionModule),
  },
  {
    path: 'manipulation',
    loadChildren: () => import('./manipulation/manipulation.module').then((m) => m.ManipulationModule),
  },
  {
    path: 'permission',
    loadChildren: () => import('./permission/permission.module').then((m) => m.PermissionModule),
  },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then((m) => m.NotificationModule),
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationRoutingModule {}
