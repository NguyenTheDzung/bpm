import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { CommonModel } from '@common-category/models/common-category.model';
import { cleanDataForm, validateAllFormFields } from '@cores/utils/functions';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import * as _ from 'lodash';
import { PermissionModel } from '../../models/permission.model';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-permission-action',
  templateUrl: './permission-action.component.html',
  styleUrls: ['./permission-action.component.scss'],
})
export class PermissionActionComponent extends BaseActionComponent implements OnInit {
  listDecentralization: CommonModel[] = [];
  listScopeByResource: PermissionModel[] = [];
  override form = this.fb.group({
    id: null,
    name: [null, [Validators.required, Validators.maxLength(256)]],
    description: null,
    resource: [null, Validators.required],
    policies: [null, Validators.required],
    type: [null, Validators.required],
    scopes: null,
  });

  constructor(injector: Injector, private requestService: PermissionService) {
    super(injector, requestService);
    this.listDecentralization = [
      { name: 'Scope', code: 'scope' },
      { name: 'Resource', code: 'resource' },
    ];
  }

  //call api lấy  scope trong resource
  getScopeByResource(resource: string, initData?: boolean) {
    if (resource) {
      this.requestService.getScopeByResource(resource).subscribe({
        next: (res) => {
          this.listScopeByResource = res.data;
          if (!initData) {
            this.form.get('scopes')?.setValue([]);
          }
        },
      });
    }
  }

  onChangeScopeByResource() {
    this.getScopeByResource(this.form.get('resource')?.value);
  }

  ngOnInit(): void {
    if (!_.isEmpty(this.data?.resource)) {
      this.getScopeByResource(this.data?.resource, true);
    }
    if (this.data && this.screenType !== ScreenType.Create) {
      this.form.get('type')?.disable();
      this.form.get('name')?.disable();
      this.form.get('resource')?.disable();
      this.form.patchValue(this.data);
    }
    this.state?.listResource?.forEach((item: any) => {
      item.desName = `${item.code} - ${item.description}`;
    });
    this.form.get('type')?.valueChanges.subscribe((value: string) => {
      if (value === 'scope') {
        this.form.get('scopes')?.setValidators(Validators.required);
      } else {
        this.form.get('scopes')?.setValidators(null);
      }
      this.form.get('scopes')?.updateValueAndValidity();
    });
  }

  override save() {
    if (this.loadingService.loading) {
      return;
    }
    cleanDataForm(this.form);
    if (this.form?.status === 'VALID') {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          const data = this.form.getRawValue();
          if (data.type === 'resource') {
            data.scopes = this.listScopeByResource.map((x) => x.id);
          }
          if (this.screenType == ScreenType.Create) {
            this.create(data);
          } else {
            this.update(data);
          }
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }
}
