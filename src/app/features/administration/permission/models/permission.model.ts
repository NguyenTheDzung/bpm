import { CommonModel } from '@common-category/models/common-category.model';

export interface PermissionModel {
  code?: string;
  id?: string;
  name?: string;
  description?: string;
  scopes?: string;
  policy?: string;
  type?: string;
  search?: string;
  data?: any;
  rsid?: string;
  scope?: string[];
  parentResources?: string;
  resources?: string;
  value?: string;
}

export interface StateRequest {
  listResource: CommonModel[];
}
