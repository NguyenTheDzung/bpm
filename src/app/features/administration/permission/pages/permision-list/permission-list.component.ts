import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import {cloneDeep, filter, isEmpty, map} from 'lodash';
import { TreeNode } from 'primeng/api';
import { PermissionActionComponent } from '../../components';
import { PermissionModel } from '../../models/permission.model';
import { PermissionService } from '../../services/permission.service';
import { TreeTable } from 'primeng/treetable';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.scss'],
})
export class PermissionListComponent extends BaseTableComponent<PermissionModel> implements OnInit {
  dataFlat: PermissionModel[] = [];
  data: TreeNode<PermissionModel>[] = [];
  override params: PermissionModel = {
    search: '',
    name: '',
  };
  @ViewChild(TreeTable) treeTable?: TreeTable;

  constructor(inject: Injector, private service: PermissionService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.getState();
  }

  override getState() {
    this.serviceBase.getState().subscribe({
      next: (state) => {
        this.propData = cloneDeep(state);
        this.stateData = cloneDeep(state);
        this.mapState();
        this.getDataTree();
      },
    });
  }

  override search(firstPage?: boolean) {
    this.treeTable?.filterGlobal(this.params.search?.trim(), 'contains');
    const timer = setTimeout(() => {
      if (this.treeTable?.filters['global'] && !this.treeTable.filteredNodes?.length) {
        this.messageService.error('MESSAGE.notfoundrecord');
      }
      clearTimeout(timer);
    }, 500);
  }

  override viewCreate() {
    if (!this.configAction?.component) {
      return;
    }
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toUpperCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        screenType: ScreenType.Create,
        state: this.propData,
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.getDataTree();
          this.getState();
        }
      },
    });
  }

  override viewEdit(code: string) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();
    this.serviceBase.findByCode(code).subscribe({
      next: (data) => {
        const dialog = this.dialogService?.open(this.configAction!.component, {
          header: `${this.configAction!.title.toLowerCase()}`,
          showHeader: false,
          width: this.configAction!.dialog?.width || '85%',
          data: {
            model: data.data,
            state: this.propData,
            screenType: ScreenType.Update,
          },
        });

        dialog?.onClose.subscribe({
          next: (isSuccess) => {
            if (isSuccess) {
              this.getState();
              this.getDataTree();
            }
          },
        });
        this.loadingService.complete();
      },
      error: (_e) => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  override initConfigAction() {
    this.configAction = {
      component: PermissionActionComponent,
      title: '',
      dialog: {
        width: '50%',
      },
    };
  }

  getDataTree() {
    this.loadingService.start();
    const params = {
      ...this.params,
    };
    this.service.getDataTree(params).subscribe({
      next: (dataTree) => {
        this.dataFlat = dataTree?.data?.content || [];
        const listNode: TreeNode<PermissionModel>[] = map(dataTree?.data?.content, (item) => {
          return <TreeNode<PermissionModel>>{
            data: item,
            label: item?.name,
            expanded: true,
          };
        });

        this.data = filter(listNode, (item) => isEmpty(item.data?.parentResources) && !isEmpty(item.data?.resources));
        this.mapTreePermission(listNode, this.data);
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }

  mapTreePermission(list: TreeNode<PermissionModel>[], listParent: TreeNode<PermissionModel>[]) {
    for (const item of listParent) {
      const listChildren = list?.filter((i) => i.data?.parentResources === item.data?.resources);
      if (listChildren.length > 0) {
        item.children = listChildren;
        this.mapTreePermission(list, item.children);
      }
    }
  }

  override deleteItem(id: string | number) {
    if (this.loadingService.loading) {
      return;
    }
    this.messageService.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        this.serviceBase.delete(id).subscribe({
          next: () => {
            this.messageService.success('MESSAGE.DELETE_SUCCESS');
            this.getDataTree();
          },
          error: (e) => {
            this.loadingService.complete();
            this.messageService.error(createErrorMessage(e));
          },
        });
      }
    });
  }

  showActionDelete(rowNode: TreeNode) {
    return this.checkFunctionDelete() && !(rowNode?.children && rowNode?.children?.length > 0);
  }
}
