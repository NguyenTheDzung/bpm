import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { FunctionCode } from '@cores/utils/enums';
import { PermissionListComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: PermissionListComponent,
    canActivate: [FunctionGuardService],
    data: {
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.Permission}`,
      functionCode: FunctionCode.Permission,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PermisionRoutingModule {}
