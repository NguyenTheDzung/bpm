import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { catchError, forkJoin, map, Observable, of } from 'rxjs';
import { PermissionModel } from '../models/permission.model';

@Injectable({
  providedIn: 'root',
})
export class PermissionService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.employee_url}/permission`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}`, { params: newParam })

        .pipe(map((data) => mapDataTableEmployee(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  getFunction() {
    return this.http.get<any>(`${environment.employee_url}/resource?page=0&size=10000`);
  }

  getRole() {
    return this.http.get<any>(`${environment.employee_url}/role?page=0&size=10000`);
  }

  getScopeByResource(id?: string): Observable<any> {
    return this.http.get<any>(`${environment.employee_url}/scope/get-by-resource/` + id);
  }

  getAction() {
    return this.http.get<any>(`${environment.employee_url}/scope?page=0&size=10000`);
  }

  override findByCode(id?: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  override getState(): Observable<any> {
    return forkJoin({
      listRole: this.getRole().pipe(map((data) => data.data?.content)),
      listAction: this.getAction().pipe(map((data) => data.data?.content)),
      listResource: this.getFunction().pipe(
        map((data) => data.data?.content),
        catchError(() => of<any[]>([]))
      ),
    }).pipe(
      map(
        (data) =>
          (this.state = {
            ...this.state,
            ...data,
          })
      )
    );
  }

  getDataTree(params?: any): Observable<PermissionModel> {
    return this.http.get<PermissionModel>(this.baseUrl, {
      params: { ...params },
    });
  }
}
