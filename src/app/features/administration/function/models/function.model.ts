import { ManipulationModel } from '@manipulation/models/manipulation.model';
import { FunctionModel } from '@cores/models/function.model';

export interface StateData {
  listScopes: ManipulationModel[];
  listResource: FunctionModel[];
}
