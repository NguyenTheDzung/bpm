import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FunctionListComponent } from './pages';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { FunctionCode } from '@cores/utils/enums';

const routes: Routes = [
  {
    path: '',
    component: FunctionListComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.Function,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.Function}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FunctionRoutingModule {}
