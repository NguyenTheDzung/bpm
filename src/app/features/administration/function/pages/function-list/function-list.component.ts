import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { FunctionModel } from '@cores/models/function.model';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { cloneDeep, filter, isEmpty, map } from 'lodash';
import { TreeNode } from 'primeng/api';
import { FunctionActionComponent } from '../../compoments';
import { FunctionService } from '../../services/function.service';
import { TreeTable } from 'primeng/treetable';
import { createErrorMessage } from '@cores/utils/functions';
import { FileUpload } from 'primeng/fileupload';
import { StateData } from '@functions/models/function.model';

@Component({
  selector: 'app-function-list',
  templateUrl: './function-list.component.html',
  styleUrls: ['./function-list.component.scss'],
})
export class FunctionListComponent extends BaseTableComponent<FunctionModel> implements OnInit {
  data: TreeNode<FunctionModel>[] = [];
  dataFlat: FunctionModel[] = [];
  @ViewChild('tt') treeTable?: TreeTable;
  override params: FunctionModel = {
    code: '',
    name: '',
  };
  ids: string[] = [];
  override stateData: StateData = {
    listScopes: [],
    listResource: [],
  };

  constructor(inject: Injector, private service: FunctionService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.getState();
  }

  override search(_firstPage?: boolean) {
    this.params.search = this.params.search?.trim();
    this.treeTable?.filterGlobal(this.params.search, 'contains');
    const timer = setTimeout(() => {
      if (this.treeTable?.filters['global'] && !this.treeTable.filteredNodes?.length) {
        this.messageService.error('MESSAGE.notfoundrecord');
      }
      clearTimeout(timer);
    }, 500);
  }

  override getState() {
    this.service.getState().subscribe({
      next: (state) => {
        this.propData = cloneDeep(state);
        this.stateData = cloneDeep(state);
        this.getDataTree();
      },
    });
  }

  override initConfigAction() {
    this.configAction = {
      component: FunctionActionComponent,
      title: 'title',
      dialog: {
        width: '50%',
      },
    };
  }

  override viewCreate() {
    if (!this.configAction?.component) {
      return;
    }
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toUpperCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        screenType: ScreenType.Create,
        state: this.stateData,
        dataTree: this.dataFlat,
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.getDataTree();
        }
      },
    });
  }

  getDataTree() {
    this.loadingService.start();

    const params = {
      ...this.params,
      page: 0,
    };
    this.dataFlat = cloneDeep(this.stateData.listResource || []);
    // map về model treenode
    const listNode: TreeNode<FunctionModel>[] = map(cloneDeep(this.stateData.listResource || []), (item) => {
      return <TreeNode<FunctionModel>>{
        data: item,
        label: item?.name,
        expanded: true,
      };
    });
    this.data = filter(listNode, (item) => isEmpty(item.data?.parent)); //lấy cha,điều kiện key parent = null thì lấy
    this.mapTreeFunction(listNode, this.data);
    this.loadingService.complete();
  }

  mapTreeFunction(list: TreeNode<FunctionModel>[], listParent: TreeNode<FunctionModel>[]) {
    for (const item of listParent) {
      const listChildren = list?.filter((i) => i.data?.parent === item.data?.id); //

      if (listChildren.length > 0) {
        item.children = listChildren;
        this.mapTreeFunction(list, item.children);
      }
    }
  }

  override viewEdit(data: string) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toLowerCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: data,
        state: this.stateData,
        screenType: ScreenType.Update,
        dataTree: this.dataFlat,
      },
    });
    this.loadingService.complete();
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.getState();
        }
      },
    });
  }

  override deleteItem(id: string | number) {
    if (this.loadingService.loading) {
      return;
    }

    this.messageService.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        this.serviceBase.delete(id).subscribe({
          next: () => {
            this.messageService.success('MESSAGE.DELETE_SUCCESS');
            this.getState();
          },
          error: (e) => {
            this.messageService.error(createErrorMessage(e));
            this.loadingService.complete();
          },
        });
      }
    });
  }

  showActionDelete(rowNode: TreeNode) {
    return this.checkFunctionDelete() && !(rowNode?.children && rowNode?.children?.length > 0);
  }

  importFile(event: { files: File[] }, elFileUpload: FileUpload) {
    const data: FormData = new FormData();
    data.append('file', event.files[0]);
    this.loadingService.start();
    this.service.importFile(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.ids = [];
        elFileUpload.clear();
        this.getDataTree();
      },
      error: () => {
        elFileUpload.clear();
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  exportFile() {
    this.loadingService.start();
    this.service.exportFile('danh-sach-chuc-nang.json', this.ids).subscribe({
      next: () => {
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }
}
