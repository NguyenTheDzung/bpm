import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FunctionModel } from '@cores/models/function.model';
import { BaseService } from '@cores/services/base.service';
import { environment } from '@env';
import { catchError, forkJoin, map, Observable, of } from 'rxjs';
import { saveAs } from 'file-saver';
import { StateData } from '@functions/models/function.model';

@Injectable({
  providedIn: 'root',
})
export class FunctionService extends BaseService {
  override state: StateData = {
    listResource: [],
    listScopes: [],
  };

  constructor(http: HttpClient) {
    super(http, `${environment.employee_url}/resource`);
  }

  getDataTree(params?: any): Observable<FunctionModel> {
    return this.http.get<FunctionModel>(this.baseUrl, {
      params: { ...params },
    });
  }

  override getState(): Observable<StateData> {
    return forkJoin({
      listScopes: this.getScope().pipe(map((data) => data.data?.content)),
      listResource: this.getDataTree().pipe(
        map((data) => data.data?.content),
        catchError(() => of<any[]>([]))
      ),
    }).pipe(
      map(
        (data) =>
          (this.state = {
            ...this.state,
            ...data,
          })
      )
    );
  }

  getScope() {
    return this.http.get<any>(`${environment.employee_url}/scope`);
  }

  exportFile(fileName: string, params: string[]): Observable<boolean> {
    return this.http.post(`${this.baseUrl}/exportResource`, params).pipe(
      map((res: any) => {
        saveAs(
          new Blob([JSON.stringify(res.data)], {
            type: 'application/octet-stream',
          }),
          fileName
        );
        console.log(res);
        return true;
      })
    );
  }

  importFile(data: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}/importResource`, data);
  }
}
