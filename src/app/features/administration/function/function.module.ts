import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FunctionRoutingModule } from './function-routing.module';
import { SharedModule } from '@shared/shared.module';
import { pages } from './pages';
import { components } from './compoments';

@NgModule({
  declarations: [...pages, ...components],
  imports: [SharedModule, FunctionRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class FunctionModule {}
