import { Component, Injector, OnInit } from '@angular/core';
import { FormArray, Validators } from '@angular/forms';
import { cleanDataForm, validateAllFormFields } from '@cores/utils/functions';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { TreeNode } from 'primeng/api';
import { FunctionService } from '../../services/function.service';
import { filter } from 'lodash';
import { StateData } from '@functions/models/function.model';

@Component({
  selector: 'app-function-action',
  templateUrl: './function-action.component.html',
  styleUrls: ['./function-action.component.scss'],
})
export class FunctionActionComponent extends BaseActionComponent implements OnInit {
  dataTree!: TreeNode[];
  override form = this.fb.group({
    parent: [''],
    code: ['', [Validators.required, Validators.pattern('^[A-Z0-9_]+'), Validators.maxLength(255)]],
    name: ['', [Validators.required, Validators.maxLength(255)]],
    iconUri: ['', [Validators.required, Validators.maxLength(255)]],
    uri: ['', [Validators.maxLength(255)]],
    description: [''],
    id: [''],
    order: [0, [Validators.required, Validators.maxLength(255)]],
    scope: [[], Validators.required],
    api: this.fb.array([]),
  });
  override state: StateData = {
    listResource: [],
    listScopes: [],
  };

  constructor(inject: Injector, service: FunctionService) {
    super(inject, service);
    this.dataTree = this.configDialog.data?.dataTree;
    this.state = this.configDialog.data?.state;
  }

  get getApis() {
    return this.form.get('api') as FormArray;
  }

  ngOnInit(): void {
    if (this.data && this.screenType !== ScreenType.Create) {
      this.form.patchValue(this.data);
      this.data?.api?.forEach((value: string) => {
        this.addApi(value);
      });
    }
  }

  addApi(value: string = '') {
    this.getApis.insert(0, this.fb.control(value));
  }

  deleteIndex(index: number) {
    this.getApis.removeAt(index);
  }

  override save() {
    if (this.loadingService.loading) {
      return;
    }

    const data = cleanDataForm(this.form);
    data.api = filter(data.api, (value) => value !== '');

    if (this.form?.status === 'VALID') {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          if (this.screenType === ScreenType.Create) {
            this.create(data);
          } else {
            this.update(data);
          }
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }
}
