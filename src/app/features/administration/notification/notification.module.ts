import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';
import { NotificationRoutingModule } from './notification-routing.module';
import { pages } from './pages';

@NgModule({
  imports: [NotificationRoutingModule, SharedModule],
  declarations: [...components, ...pages],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class NotificationModule {}
