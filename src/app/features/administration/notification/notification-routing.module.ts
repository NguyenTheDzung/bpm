import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { FunctionCode } from '@cores/utils/enums';
import { NotificationComponent } from '@notification-system/pages/notification/notification.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.Notification,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.Notification}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationRoutingModule {}
