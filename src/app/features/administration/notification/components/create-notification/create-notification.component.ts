import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { CommonModel } from '@common-category/models/common-category.model';
import { FileUpload } from 'primeng/fileupload';
import { FileCollection } from '@create-requests/models/collection.model';
import { typeFileCollection, ZCOL_PR } from '@create-requests/utils/constants';
import { Validators } from '@angular/forms';
import { createErrorMessage, getValueDateTimeLocal, validateAllFormFields } from '@cores/utils/functions';
import { forkJoin } from 'rxjs';
import { MAX_FILE_SIZE } from '@cores/utils/constants';
import { NotificationSystemService } from '../../services/notification-system.service';

@Component({
  selector: 'app-create-notification',
  templateUrl: './create-notification.component.html',
  styleUrls: ['./create-notification.component.scss'],
})
export class CreateNotificationComponent extends BaseActionComponent implements OnInit {
  @ViewChild('fileInput') fileInput!: FileUpload;
  item: any;
  notificationType!: CommonModel[];
  roleSystem: any;
  files: FileCollection[] = typeFileCollection;
  maxFileSize: number = 0;
  valueTypeNotification: string = '';

  constructor(inject: Injector, private services: NotificationSystemService) {
    super(inject, services);
    this.item = this.configDialog.data.state;
    this.files[0].required = true;
  }

  override form = this.fb.group({
    type: [null, Validators.required],
    dateStart: [null, Validators.required],
    dateEnd: [null, Validators.required],
    content: [null, Validators.required],
    departments: [null, Validators.required],
    status: [null],
  });

  ngOnInit(): void {
    if (this.item) {
      this.form.patchValue(this.item);
    }
    const getNotificationType = this.services.getNotificationType();
    const getRoleSystem = this.services.getRoleSystem();
    this.loadingService.start();
    forkJoin([getNotificationType, getRoleSystem]).subscribe({
      next: (res) => {
        this.notificationType = res[0];
        this.roleSystem = res[1].data.content;
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close();
    } else {
      this.location.back();
    }
  }

  fileHandler(event: FileUpload, index: number) {
    if (event?.files.length > 3) {
      this.messageService.error(this.translateService.instant(`MESSAGE.TOTAL_MAX_LENGTH_FILE_UPLOAD`, { count: 3 }));
    }
    this.files[index].content = [];
    event?.files?.forEach((file) => {
      if (file.size == 0) {
        this.messageService.error('MESSAGE.FILE_SIZE_NOT_EMPTY');
      }
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.files[index].content?.push(
          (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1)
        );
      };
    });
    this.files[index].file = event?.files;
  }

  handleFileSize() {
    const requestFileDTOs: any[] = [];
    this.maxFileSize = 0;
    this.files.forEach((item) => {
      if (item.file) {
        item.file.forEach((file, i) => {
          this.maxFileSize += file.size;
          requestFileDTOs.push({
            name: file.name,
            type: ZCOL_PR,
            extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
            content: item.content![i],
            mimeType: file.type,
            size: file.size,
          });
        });
      }
    });
    return requestFileDTOs;
  }

  override save() {
    validateAllFormFields(this.form!);
    const data = this.getDataForm();
    data.id = this.item.id;
    data.dateStart = getValueDateTimeLocal(data.dateStart);
    data.dateEnd = getValueDateTimeLocal(data.dateEnd);
    data.status = true;
    if (this.valueTypeNotification === '1') {
      data.requestFileDTOs = this.handleFileSize();
    }
    if (this.valueTypeNotification === '2') {
      data.requestFileDTOs = [];
    }
    if (this.form?.status !== 'VALID') {
      return;
    }
    if (this.maxFileSize > MAX_FILE_SIZE) {
      return this.messageService.error('MESSAGE.TOTAL_MAX_FILE_SIZE_UPLOAD');
    }
    if (data.requestFileDTOs) {
      if (data.requestFileDTOs.length > 3) {
        return this.messageService.error(
          this.translateService.instant(`MESSAGE.TOTAL_MAX_LENGTH_FILE_UPLOAD`, { count: 3 })
        );
      }
      for (let i = 0; i < data.requestFileDTOs.length; i++) {
        if (data.requestFileDTOs[i].size == 0) {
          return this.messageService.error('MESSAGE.FILE_SIZE_NOT_EMPTY');
        }
      }
    }
    this.loadingService.start();
    this.services.changeData(data).subscribe({
      next: () => {
        this.loadingService.complete();
        this.closeDialog();
        this.messageService.success('MESSAGE.SUCCESS');
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  changValueType($event: any) {
    this.valueTypeNotification = $event.value;
  }
}
