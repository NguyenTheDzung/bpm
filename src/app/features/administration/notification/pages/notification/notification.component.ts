import { Component, Injector, OnInit } from '@angular/core';
import { BaseTableComponent } from '@shared/components';
import { ScreenType } from '@cores/utils/enums';
import { CreateNotificationComponent } from '@notification-system/components';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationSystemService } from '../../services/notification-system.service';
import { NotificationPostData } from '../../model';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent extends BaseTableComponent<any> implements OnInit {
  balanceFrozen: boolean = true;

  constructor(injector: Injector, private service: NotificationSystemService, private domSanitizer: DomSanitizer) {
    super(injector, service);
  }

  ngOnInit(): void {
    this.getData(true);
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.getData(false);
  }

  getData(firstPage: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    this.service.getData(this.dataTable.currentPage, this.dataTable.size).subscribe({
      next: (res: any) => {
        this.dataTable = {
          content: res?.data?.notificationDetailDTOS || [],
          currentPage: res?.data?.pageNumber || 0,
          size: res?.data?.pageSize || 10,
          totalElements: res?.data?.totalElements || 0,
          totalPages: res?.data?.totalPages || 0,
          numberOfElements: res?.data?.numberOfElements || 0,
          first: (this.dataTable?.size || 0) * (this.dataTable?.currentPage || 0),
        };
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  deleteConfirm(item: NotificationPostData) {
    this.messageService.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        this.service.deleteData(item).subscribe({
          next: () => {
            this.loadingService.complete();
            this.getData(false);
          },
          error: (err) => {
            this.messageService.error(createErrorMessage(err));
            this.loadingService.complete();
          },
        });
      }
    });
  }

  editPopup(data: any) {
    this.dialogService
      ?.open(CreateNotificationComponent, {
        showHeader: false,
        width: '50%',
        data: {
          screenType: ScreenType.Create,
          state: data,
        },
      })
      .onClose.subscribe(() => {
        if (data) {
          this.getData(false);
        } else {
          this.getData(true);
        }
      });
  }

  changStatus(item: NotificationPostData) {
    this.loadingService.start();
    this.service.changeData(item).subscribe({
      next: () => {
        this.loadingService.complete();
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  innerCheck(content: string) {
    if (!content) return '';
    return this.domSanitizer.bypassSecurityTrustHtml(content);
  }
}
