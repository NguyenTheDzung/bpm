import {Injectable} from '@angular/core';
import {BaseService} from '@cores/services/base.service';
import {HttpClient} from '@angular/common/http';
import {CommonCategoryService} from '@cores/services/common-category.service';
import {environment} from '@env';
import {NotificationPostData} from '../model';
import {Observable, tap} from 'rxjs';
import {Router} from '@angular/router';
import * as moment from 'moment/moment';
import {AuthService} from '@cores/services/auth.service';
import {Roles} from '@cores/utils/constants';
import {IResponseModel} from "@cores/models/response.model";

@Injectable({
  providedIn: 'root',
})
export class NotificationSystemService extends BaseService {
  public listNotify: NotificationPostData[] = [];

  constructor(
    http: HttpClient,
    private common: CommonCategoryService,
    private router: Router,
    private authService: AuthService
  ) {
    super(http, `${environment.category_url}/system-notification`);
    setInterval(() => {
      this.checkTimeMaintenance();
    }, 5000);
  }

  getNotificationType() {
    return this.common.getCommonCategory('NOTIFICATION_TYPE');
  }

  getRoleSystem(): Observable<any> {
    return this.http.get(`${environment.employee_url}/role?page=0&size=100000000`);
  }

  getData(page: number, size: number, checkIsOnTimeMaintain: boolean = true) {
    return this._getSystemNotificationData(page, size).pipe(
      tap((res) => {
        this.listNotify = res.data?.notificationDetailDTOS || [];
        checkIsOnTimeMaintain && this.checkTimeMaintenance();
      })
    );
  }

  private _getSystemNotificationData(page: number, size: number) {
    return this.http.get<IResponseModel>(`${this.baseUrl}?page=${page}&size=${size}&sort=id,desc`)
  }

  changeData(data: NotificationPostData) {
    return this.http.post(`${this.baseUrl}`, data);
  }

  deleteData(data: NotificationPostData) {
    return this.http.delete(`${this.baseUrl}/`, {
      body: data,
    });
  }

  checkTimeMaintenance() {
    const item = this.listNotify?.find((item) => item.notification.type === '2' && item.notification.status);
    const currRoles = this.authService.getUserRoles();
    if (
      item &&
      moment().isSameOrAfter(moment(item?.notification.dateStart)) &&
      moment().isSameOrBefore(moment(item?.notification.dateEnd)) &&
      currRoles &&
      !currRoles.includes(Roles.SYSTEM_ADMIN)
    ) {
      this.router.navigateByUrl('/under-maintenance', {skipLocationChange: true}).then();
    }
  }

  exportFile(id: number) {
    return this.http.get(`${environment.category_url}/attachment-management?id=${id}`);
  }
}
