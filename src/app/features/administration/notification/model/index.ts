export interface NotificationPostData {
  notification: {
    attachmentList: [
      {
        createdBy: string,
        createdDate: string,
        lastUpdatedBy: string,
        lastUpdatedDate: string,
        id: number,
        fileName: string,
        licenseBase64: string,
        fileType: string,
        type: string,
        fileNameS3: string,
        requestId: string
      }
    ],
    id: number,
    type: string,
    dateStart: string,
    dateEnd: string,
    content: string,
    departments: string[],
    status: boolean,
    fileName: string
  }
}
