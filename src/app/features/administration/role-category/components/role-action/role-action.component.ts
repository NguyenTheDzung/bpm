import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseActionComponent } from '@shared/components';
import { ScreenType } from 'src/app/core/utils/enums';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-role-action',
  templateUrl: './role-action.component.html',
  styleUrls: ['./role-action.component.scss'],
})
export class RoleActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    code: ['', [Validators.required, Validators.pattern('^[A-Z0-9_]+'), Validators.maxLength(255)]],
    id: [''],
    name: ['', [Validators.required, Validators.maxLength(255)]],
    description: [''],
  });

  constructor(injector: Injector, service: RoleService) {
    super(injector, service);
  }

  ngOnInit(): void {
    if (this.screenType === ScreenType.Detail) {
      this.form?.disable();
    } else if (this.screenType === ScreenType.Update) {
      this.form?.get('code')!.disable();
    }
    if (this.data && this.screenType !== ScreenType.Create) {
      this.form.patchValue(this.data);
    }
  }
}
