export interface RoleModel {
  search?: string | null;
  id?: number;
  code?: string | null;
  name?: string | null;
  description?: string;
  createdBy?: string;
  createdDate?: Date;
  updateBy?: string;
  updateDate?: Date;
  page?: number;
  size?: number;
  data?: any;
}
