import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionCode } from '@cores/utils/enums';
import { RoleListComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: RoleListComponent,
    data: {
      functionCode: FunctionCode.Role,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.Role}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleCategoryRoutingModule {}
