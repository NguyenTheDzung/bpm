import { Component, Injector, OnInit } from '@angular/core';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { RoleActionComponent } from '../../components';
import { RoleModel } from '../../models/role.model';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss'],
})
export class RoleListComponent extends BaseTableComponent<RoleModel> implements OnInit {
  override params: RoleModel = {
    code: '',
    name: '',
    search: '',
  };

  constructor(injector: Injector, service: RoleService) {
    super(injector, service);
  }

  override viewDetail(data: string) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }

    this.loadingService.start();
    this.dialogService?.open(this.configAction.component, {
      showHeader: false,
      header: `${this.configAction.title}`,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: data,
        state: this.stateData,
        screenType: ScreenType.Detail,
      },
    });
    this.loadingService.complete();
  }

  override viewEdit(data: string) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toLowerCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: data,
        state: this.propData,
        screenType: ScreenType.Update,
      },
    });
    this.loadingService.complete();
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.search();
        }
      },
    });
  }

  override initConfigAction() {
    this.configAction = {
      component: RoleActionComponent,
      title: 'title',
      dialog: {
        width: '50%',
      },
    };
  }

  override mapDataSearch(): RoleModel {
    return { page: this.dataTable.currentPage, size: this.dataTable.size, ...this.params };
  }

  ngOnInit() {
    this.getState();
  }
}
