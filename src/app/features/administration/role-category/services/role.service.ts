import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { map, Observable } from 'rxjs';
import { BaseService } from 'src/app/core/services/base.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RoleService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.employee_url}/role`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}`, { params: newParam })
        .pipe(map((data) => mapDataTableEmployee(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }
}
