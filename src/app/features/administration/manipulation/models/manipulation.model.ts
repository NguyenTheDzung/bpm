export interface ManipulationModel {
  code?: string;
  id?: string;
  name?: string;
  displayName?: string;
  description?: string;
}
