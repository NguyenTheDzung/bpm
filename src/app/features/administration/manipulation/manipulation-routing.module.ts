import { FunctionGuardService } from '@cores/services/function-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManipulationListComponent } from './pages';
import { FunctionCode } from '@cores/utils/enums';

const routes: Routes = [
  {
    path: '',
    component: ManipulationListComponent,
    canActivate: [FunctionGuardService],
    data: {
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.Action}`,
      functionCode: FunctionCode.Action,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManipulationRoutingModule {}
