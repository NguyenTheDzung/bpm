import { Component, Injector, OnInit } from '@angular/core';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { ManipulationActionComponent } from '../../components';
import { ManipulationModel } from '../../models/manipulation.model';
import { ManipulationService } from '../../services/manipulation.service';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-manipulation-list',
  templateUrl: './manipulation-list.component.html',
  styleUrls: ['./manipulation-list.component.scss'],
})
export class ManipulationListComponent extends BaseTableComponent<ManipulationModel> implements OnInit {
  override params: ManipulationModel = {
    id: '',
    name: '',
    displayName: '',
  };

  constructor(inject: Injector, service: ManipulationService) {
    super(inject, service);
  }

  ngOnInit() {
    this.loadingService.start();
    this.getState();
  }

  override viewEdit(data: string) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toLowerCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: data,
        state: this.propData,
        screenType: ScreenType.Update,
      },
    });
    this.loadingService.complete();
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.search();
        }
      },
    });
  }

  override deleteItem(id: string | number) {
    if (this.loadingService.loading) {
      return;
    }

    this.messageService.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        this.serviceBase.delete(id).subscribe({
          next: () => {
            this.messageService.success('MESSAGE.DELETE_SUCCESS');
            this.search();
          },
          error: (e) => {
            this.messageService.error(createErrorMessage(e));

            this.loadingService.complete();
          },
        });
      }
    });
  }

  override initConfigAction() {
    this.configAction = {
      component: ManipulationActionComponent,
      title: '',
      dialog: {
        width: '50%',
      },
    };
  }
}
