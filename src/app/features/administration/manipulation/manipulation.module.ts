import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { pages } from './pages';
import { ManipulationRoutingModule } from './manipulation-routing.module';
import { ManipulationActionComponent } from './components';

@NgModule({
  declarations: [...pages, ManipulationActionComponent],
  imports: [SharedModule, ManipulationRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ManipulationModule {}
