import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { ManipulationService } from '../../services/manipulation.service';

@Component({
  selector: 'app-manipulation-action',
  templateUrl: './manipulation-action.component.html',
  styleUrls: ['./manipulation-action.component.scss'],
})
export class ManipulationActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    id: [''],
    name: ['', [Validators.required, Validators.maxLength(256), Validators.pattern('^[A-Z0-9_]+')]],
    description: [''],
    displayName: ['', [Validators.required, Validators.maxLength(256)]],
    iconUri: [''],
  });

  constructor(injector: Injector, requestService: ManipulationService) {
    super(injector, requestService);
  }

  ngOnInit(): void {
    if (this.data && this.screenType !== ScreenType.Create) {
      this.form?.get('name')!.disable();
      this.form.patchValue(this.data);
    }
  }
}
