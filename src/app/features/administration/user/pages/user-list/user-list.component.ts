import { Component, Injector, OnInit } from '@angular/core';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { UserActionComponent } from '../../components';
import { StateUser, UserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent extends BaseTableComponent<UserModel> implements OnInit {
  override params: UserModel = {
    username: '',
    name: '',
    code: '',
    unlock: null,
    status: null,
    group: '',
  };
  override stateData: StateUser = {
    listStatus: [],
    lock: [],
  };

  constructor(inject: Injector, private service: UserService) {
    super(inject, service);
  }

  ngOnInit() {
    this.loadingService.start();
    this.getState();
  }

  override initConfigAction() {
    this.configAction = {
      component: UserActionComponent,
      title: 'user',
      dialog: {
        width: '50%',
      },
    };
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const params = this.mapDataSearch();

    this.serviceBase.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.error('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: () => {
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
        this.loadingService.complete();
      },
    });
  }

  override viewEdit(data: string) {
    if (this.loadingService.loading || !this.configAction?.component || this.isPopupShow) {
      return;
    }
    this.loadingService.start();
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toLowerCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: data,
        state: this.propData,
        screenType: ScreenType.Update,
      },
    });
    this.isPopupShow = true;
    this.loadingService.complete();
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        this.isPopupShow = false;
        if (isSuccess) {
          this.search();
        }
      },
    });
  }

  changeLockValue(data: UserModel) {
    let body = {
      userName: data.account,
      status: data.locked,
    };
    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.service.changeStatusUser(body).subscribe({
          next: () => {
            this.messageService.success('Đổi trạng thái thành công ');
            this.search();
          },
        });
      } else {
        this.search();
      }
    });
  }

  changeStatusUser(data: UserModel) {
    let body = {
      userName: data.account,
      status: data.status,
    };
    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.service.changeStatusUser(body, true).subscribe({
          next: () => {
            this.messageService.success('Đổi trạng thái thành công ');
            this.search();
          },
        });
      } else {
        this.search();
      }
    });
  }

  resetPassword(username: string) {
    this.loadingService.start();
    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.service.resetPassword(username).subscribe({
          next: () => {
            this.messageService.success('MESSAGE.UPDATE_SUCCESS');
            this.loadingService.complete();
          },
          error: (err) => {
            this.messageService.error(`MESSAGE.${err?.error?.messageKey ? err.error.messageKey : 'E_INTERNAL_SERVER'}`);
            this.loadingService.complete();
          },
        });
      }
    });
    this.loadingService.complete();
  }

  checkStatusUser() {
    //check readonly btn
    return !this.objFunction?.scopes?.includes(this.action.Update);
  }
}
