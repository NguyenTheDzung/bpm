import { CommonModel } from '@common-category/models/common-category.model';

export interface UserModel {
  id?: string;
  username?: string;
  name?: string;
  role?: string;
  group?: string;
  email?: string;
  phoneNumber?: string;
  account?: string;
  unlock?: boolean | string | null;
  code?: string;
  page?: number;
  size?: number;
  locked?: boolean;
  status?: boolean | null | string;
}

export interface StateUser {
  listStatus?: CommonModel[];
  lock?: CommonModel[];
}
