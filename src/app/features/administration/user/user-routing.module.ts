import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { FunctionCode } from '@cores/utils/enums';
import { UserListComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: UserListComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.User,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.User}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
