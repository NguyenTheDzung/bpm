import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { pages } from './pages';
import { UserRoutingModule } from './user-routing.module';
import { components } from './components';

@NgModule({
  imports: [UserRoutingModule, SharedModule],
  declarations: [...pages, components],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class UserModule {}
