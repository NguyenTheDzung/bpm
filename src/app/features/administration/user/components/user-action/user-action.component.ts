import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { UserService } from '../../services/user.service';
import { CustomValidators } from '@cores/utils/custom-validators';
import { StateUser } from '../../models/user.model';

@Component({
  selector: 'app-user-action',
  templateUrl: './user-action.component.html',
  styleUrls: ['./user-action.component.scss'],
})
export class UserActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    code: [''],
    name: ['', [Validators.required, Validators.maxLength(256)]],
    role: ['', [Validators.required, Validators.maxLength(256)]],
    group: ['', [Validators.required, Validators.maxLength(256)]],
    email: ['', [Validators.required, CustomValidators.email, Validators.maxLength(256)]],
    phoneNumber: [''],
    status: [true],
    account: ['', Validators.required],
    isExitKeyCloak: true,
  });
  override state?: StateUser;

  constructor(injector: Injector, requestService: UserService) {
    super(injector, requestService);
    this.state?.listStatus?.forEach((item) => {
      item.value = !!+item.value;
    });
  }

  ngOnInit(): void {
    if (this.screenType === ScreenType.Detail) {
      this.form?.disable();
    } else if (this.screenType === ScreenType.Update) {
      this.form?.get('code')!.disable();
    }
    if (this.data && this.screenType !== ScreenType.Create) {
      this.form.patchValue(this.data);
    }
  }
}
