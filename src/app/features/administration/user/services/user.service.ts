import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { forkJoin, map, Observable, of } from 'rxjs';
import { StateUser, UserModel } from '../models/user.model';
import * as _ from 'lodash';
import { ACCOUNT_IS_LOCK, ACCOUNT_STATUS } from '@cores/utils/constants';
import { CommonCategoryService } from '@cores/services/common-category.service';

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService {
  constructor(http: HttpClient, private commonService: CommonCategoryService) {
    super(http, `${environment.employee_url}/user`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}`, { params: newParam })

        .pipe(map((data) => mapDataTableEmployee(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override getState(): Observable<StateUser> {
    if (_.isEmpty(this.state)) {
      return forkJoin({
        listStatus: this.commonService.getCommonCategory(ACCOUNT_STATUS),
        lock: this.commonService.getCommonCategory(ACCOUNT_IS_LOCK),
      }).pipe(
        map(
          (data) =>
            (this.state = {
              ...this.state,
              ...data,
            })
        )
      );
    }
    return of(this.state!);
  }

  resetPassword(username: string) {
    return this.http.get(`${this.baseUrl}/reset-password?username=${username}`);
  }

  changeStatusUser(data: UserModel, isLock?: boolean) {
    if (isLock) {
      return this.http.post(`${this.baseUrl}/lockUser`, data);
    }
    return this.http.post(`${this.baseUrl}/disableUser`, data);
  }
}
