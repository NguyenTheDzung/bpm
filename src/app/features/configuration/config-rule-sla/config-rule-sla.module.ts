import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ConfigRuleSLARoutingModule } from './config-rule-sla-routing.module';
import { pages } from './pages';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';

@NgModule({
  declarations: [...pages, ...components],
  imports: [SharedModule, ConfigRuleSLARoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ConfigRuleSLAModule {}
