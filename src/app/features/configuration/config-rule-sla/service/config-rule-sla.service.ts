import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { dataURItoBlob, mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { forkJoin, map, Observable } from 'rxjs';
import { PermissionService } from '@permission/services/permission.service';
import { saveAs } from 'file-saver';

const ruleType = {
  refund: 'RULE_SLA_REFUND',
  collection: 'RULE_SLA_COLLECTION',
};

@Injectable({
  providedIn: 'root',
})
export class ConfigRuleSLAService extends BaseService {
  constructor(http: HttpClient, private service: PermissionService) {
    super(http, `${environment.category_url}/common?common-category-code=RULE`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);

    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override getState(): Observable<any> {
    return forkJoin({
      listRole: this.service.getRole().pipe(map((data) => data?.data?.content)),
    }).pipe(
      map(
        (data) =>
          (this.state = {
            ...this.state,
            ...data,
          })
      )
    );
  }

  override update(data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${data.code}`, data);
  }

  uploadFile(file: any, code: string, endpoint: string): Observable<any> {
    return this.http.post(this.getUrlUploadOrDownload(code, endpoint), file);
  }

  downloadTemplate(fileName: string, code: string, endpoint: string): Observable<any> {
    return this.http.get(`${this.getUrlUploadOrDownload(code, endpoint)}?fileName=${fileName}`).pipe(
      map((res: any) => {
        const file = dataURItoBlob(res?.data);
        saveAs(
          new Blob([file], {
            type: 'application/octet-stream',
          }),
          fileName
        );
        return true;
      })
    );
  }

  getUrlUploadOrDownload(code: string, endpoint: string): string {
    let url = '';
    if (code === ruleType.refund) {
      url = environment.refund_url;
    } else if (code === ruleType.collection) {
      url = environment.collection_url;
    }
    url += endpoint;
    return url;
  }
}
