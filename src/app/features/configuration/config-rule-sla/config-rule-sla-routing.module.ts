import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigRuleSLAComponent } from './pages';
import { FunctionCode } from '@cores/utils/enums';
import { FunctionGuardService } from '@cores/services/function-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ConfigRuleSLAComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ConfigRule,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ConfigRule}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigRuleSLARoutingModule {}
