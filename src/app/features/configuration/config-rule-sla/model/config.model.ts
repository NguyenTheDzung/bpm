export interface ConfigRuleSLAModel {
  code?: string | null;
  page?: number;
  size?: number;
}
