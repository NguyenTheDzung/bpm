import {Component, Injector, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {ScreenType} from '@cores/utils/enums';
import {BaseActionComponent} from '@shared/components';
import {ConfigRuleSLAService} from '../../service/config-rule-sla.service';

@Component({
  selector: 'app-config-rule-sla-action',
  templateUrl: './config-rule-sla-action.component.html',
  styleUrls: ['./config-rule-sla-action.component.scss'],
})
export class ConfigRuleSLAActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    code: ['', Validators.required],
    name: ['', Validators.required],
    subject: ['', Validators.required],
    description: [''],
    body: ['', Validators.required],
    toRoles: [[], Validators.required],
    ccRoles: [[], Validators.required],
    bccRoles: [[], Validators.required],
  });

  constructor(inject: Injector, service: ConfigRuleSLAService) {
    super(inject, service);
  }

  ngOnInit(): void {
    if (this.screenType !== ScreenType.Create) {
      this.form.get('code')?.disable();
      this.form.patchValue(this.data);
    }
  }
}
