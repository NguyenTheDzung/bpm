import { Component, Injector, OnInit } from '@angular/core';
import { BaseTableComponent } from '@shared/components';
import { FileUpload } from 'primeng/fileupload';
import { ConfigRuleSLAModel } from '../../model/config.model';
import { ConfigRuleSLAService } from '../../service/config-rule-sla.service';
import { MAX_FILE_SIZE } from '@cores/utils/constants';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-config-rule-sla',
  templateUrl: './config-rule-sla.component.html',
  styleUrls: ['./config-rule-sla.component.scss'],
})
export class ConfigRuleSLAComponent extends BaseTableComponent<ConfigRuleSLAModel> implements OnInit {
  override params: ConfigRuleSLAModel = {
    code: null,
  };

  constructor(inject: Injector, private configRuleService: ConfigRuleSLAService) {
    super(inject, configRuleService);
  }

  ngOnInit(): void {
    this.getState();
  }

  uploadHandler(event: any, fileInput: FileUpload, rowData: any) {
    fileInput.clear();
    if (!event?.files[0]) {
      return;
    }
    if (event?.files[0] && event?.files[0].size > MAX_FILE_SIZE) {
      return this.messageService.warn(
        `File upload ${event?.files[0].name} ` + this.translateService.instant('MESSAGE.MAX_FILE_SIZE_UPLOAD')
      );
    }

    const formData = new FormData();

    this.loadingService.start();
    formData.append('file', event?.files[0]);
    this.configRuleService.uploadFile(formData, rowData.code, rowData.value).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  downloadTemplate(item: any) {
    this.loadingService.start();
    this.configRuleService.downloadTemplate(item.name, item.code, item.value).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }
}
