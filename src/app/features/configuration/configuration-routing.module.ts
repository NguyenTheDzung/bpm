import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'common-category',
    loadChildren: () => import('./common-category/common-category.module').then((m) => m.CommonCategoryModule),
  },
  {
    path: 'config-email',
    loadChildren: () => import('./config-email/config-email.module').then((m) => m.ConfigEmailModule),
  },
  {
    path: 'rule-sla',
    loadChildren: () => import('./config-rule-sla/config-rule-sla.module').then((m) => m.ConfigRuleSLAModule),
  },
  {
    path: 'request-assignment',
    loadChildren: () => import('./config-request-assignment/config-request.module').then((m) => m.ConfigRequestModule),
  },
  {
    path: 'business-approve',
    loadChildren: () =>
      import('./config-business-approve/config-business-approve.module').then((m) => m.ConfigBusinessApproveModule),
  },
  {
    path: 'approval-level-amount',
    loadChildren: () =>
      import('./config-approval-level-amount/config-approval-level-amount.module').then(
        (m) => m.ConfigApprovalLevelAmountModule
      ),
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationRoutingModule {}
