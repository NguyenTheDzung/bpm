export interface ConfigEmailModel {
  code?: string;
  name?: string;
  subject?: string;
  description?: string;
  body?: string;
  toRoles?: string;
  ccRoles?: string;
  bccRoles?: string;
}
