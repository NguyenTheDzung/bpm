import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ConfigEmailRoutingModule } from './config-email-routing.module';
import { pages } from './pages';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';

@NgModule({
  declarations: [...pages, ...components],
  imports: [SharedModule, ConfigEmailRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ConfigEmailModule {}
