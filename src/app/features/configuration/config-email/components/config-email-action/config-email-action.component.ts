import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { ConfigEmailService } from '../../service/config-email.service';

@Component({
  selector: 'app-config-email-action',
  templateUrl: './config-email-action.component.html',
  styleUrls: ['./config-email-action.component.scss'],
})
export class ConfigEmailActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    code: ['', Validators.required],
    name: ['', Validators.required],
    subject: [''],
    description: [''],
    body: ['', Validators.required],
    toRoles: [],
    ccRoles: [],
    bccRoles: [],
  });

  constructor(inject: Injector, service: ConfigEmailService) {
    super(inject, service);
  }

  ngOnInit(): void {
    if (this.screenType !== ScreenType.Create) {
      this.form.get('code')?.disable();
      this.form.patchValue(this.data);
    }
  }
}
