import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigEmailComponent } from './pages';
import { FunctionCode } from '@cores/utils/enums';
import { FunctionGuardService } from '@cores/services/function-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ConfigEmailComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ConfigEmail,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ConfigEmail}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigEmailRoutingModule {}
