import { Component, Injector, OnInit } from '@angular/core';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { ConfigEmailActionComponent } from '../../components';
import { ConfigEmailModel } from '../../model/config.model';
import { ConfigEmailService } from '../../service/config-email.service';

@Component({
  selector: 'app-config-email',
  templateUrl: './config-email.component.html',
  styleUrls: ['./config-email.component.scss'],
})
export class ConfigEmailComponent extends BaseTableComponent<ConfigEmailModel> implements OnInit {
  override params: ConfigEmailModel = {
    code: '',
    name: '',
  };

  constructor(inject: Injector, service: ConfigEmailService) {
    super(inject, service);
  }

  override initConfigAction() {
    this.configAction = {
      component: ConfigEmailActionComponent,
      title: 'title',
      dialog: {
        width: '80%',
      },
    };
  }

  ngOnInit(): void {
    this.getState();
  }

  override viewEdit(code: string) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();

    const dialog = this.dialogService?.open(this.configAction.component, {
      header: ``,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: code,
        state: this.stateData,
        screenType: ScreenType.Update,
      },
    });

    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.search();
        }
      },
    });
    this.loadingService.complete();
  }
}
