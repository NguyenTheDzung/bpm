import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { forkJoin, map, Observable } from 'rxjs';
import { PermissionService } from '@permission/services/permission.service';

@Injectable({
  providedIn: 'root',
})
export class ConfigEmailService extends BaseService {
  constructor(http: HttpClient, private service: PermissionService) {
    super(http, `${environment.category_url}/email-template`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);

    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override getState(): Observable<any> {
    return forkJoin({
      listRole: this.service.getRole().pipe(map((data) => data?.data?.content)),
    }).pipe(
      map(
        (data) =>
          (this.state = {
            ...this.state,
            ...data,
          })
      )
    );
  }

  override update(data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${data.code}`, data);
  }
}
