import { Component, Injector, OnInit } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { PaginatorModel } from '@cores/models/paginator.model';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { CategoryActionComponent } from '../../components';
import { CommonCategoryModel } from '../../models/common-category.model';
import { CategoryService } from '../../services/category.service';

@Component({
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
})
export class CategoryListComponent extends BaseTableComponent<CommonCategoryModel> implements OnInit {
  override params: CommonCategoryModel = {
    code: '',
    name: '',
  };
  detailId: string = '';
  data: DataTable = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };

  constructor(injector: Injector, private service: CategoryService) {
    super(injector, service);
  }

  override initConfigAction() {
    this.configAction = {
      component: CategoryActionComponent,
      title: 'Nhóm danh mục dùng chung',
      dialog: {
        width: '85%',
      },
    };
  }

  viewEditCategory(data: CommonCategoryModel) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();
    this.serviceBase.findByCode(data.code).subscribe({
      next: (res) => {
        this.loadingService.complete();
        const mapValue: CommonCategoryModel = {
          code: data.code,
          name: data.name,
          description: data.description,
          commons: [],
        };
        res.content.forEach((element: CommonCategoryModel) => {
          if (data.code === element.commonCategoryCode) {
            mapValue.commons!.push(element);
          }
        });
        const dialog = this.dialogService?.open(this.configAction!.component, {
          header: `Cập nhật ${this.configAction!.title.toLowerCase()}`,
          showHeader: false,
          width: this.configAction!.dialog?.width || '85%',
          data: {
            model: mapValue,
            state: this.stateData,
            screenType: ScreenType.Update,
          },
        });
        dialog?.onClose.subscribe({
          next: (isSuccess) => {
            if (isSuccess) {
              this.search();
              this.viewDetail(this.detailId);
            }
          },
        });
      },
    });
  }

  override viewDetail(code: string, firstPage?: boolean) {
    this.loadingService.start();
    if (firstPage) {
      this.data.currentPage = 0;
    }
    const params = {
      page: this.data.currentPage,
      size: this.data.size,
    };
    this.service.findByCodeTable(code, params).subscribe({
      next: (res) => {
        this.detailId = code;
        this.data = res;
        this.prevParams = params;
        this.loadingService.complete();
      },
      error: (_e) => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  ngOnInit() {
    this.getState();
  }

  pageChangeDetail(paginator: PaginatorModel) {
    this.data.currentPage = paginator.page;
    this.data.size = paginator.rows;
    this.data.first = paginator.first;
    this.viewDetail(this.detailId);
  }
}
