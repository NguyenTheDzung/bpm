export interface CommonCategoryModel {
  code: string;
  name: string;
  description?: string;
  page?: number;
  size?: number;
  commons?: CommonModel[];
  commonCategoryCode?: string;
}

export interface CommonModel {
  id?: any;
  fromDate?: any;
  fromTime?: any;
  toDate?: any;
  toTime?: any;
  selectedSupervisor2?: any;
  selectedLeave2?: any;
  account?: string;
  code?: string;
  name?: string;
  value?: any;
  orderNum?: number;
  isDefault?: boolean;
  description?: string;
  items?: any;
  disabled?: boolean;
  multiLanguage?: any;
  commonCategoryCode?: string;
}

export interface WardModel {
  countryCode: string;
  provinceCode: string;
  districtCode: string;
  wardsCode: string;
  address: string;
}

export interface Pic {
  account: string;
  code: string;
  name: string;
}
