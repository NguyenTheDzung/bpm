import { Component, Injector, OnInit } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { BaseActionComponent } from '@shared/components';
import { cleanDataForm, validateAllFormFields } from '@cores/utils/functions';
import { CategoryService } from '../../services/category.service';
import { ScreenType } from '@cores/utils/enums';
import { CommonCategoryModel, CommonModel } from '../../models/common-category.model';
import { orderBy } from 'lodash';

@Component({
  templateUrl: './category-action.component.html',
  styleUrls: ['./category-action.component.scss'],
})
export class CategoryActionComponent extends BaseActionComponent implements OnInit {
  formListChildren = new FormArray([]);
  override form = this.fb.group({
    code: ['', [Validators.required, Validators.pattern('^[A-Z0-9_]+')]],
    name: ['', Validators.required],
    description: [''],
    isActive: true,
  });
  code: any = null;

  constructor(injector: Injector, service: CategoryService) {
    super(injector, service);
  }

  ngOnInit(): void {
    if (this.screenType === ScreenType.Update) {
      this.form?.get('code')!.disable();
    }
    if (this.data && this.screenType !== ScreenType.Create) {
      this.data.commons = orderBy(this.data.commons, 'orderNum');
      this.form.patchValue(this.data);
      this.data.commons?.forEach((item: CommonModel) => {
        this.addNewFormChildren(item);
      });
    }
  }

  addNewFormChildren(data?: CommonModel) {
    const formNewChildren = this.fb.group({
      code: ['', [Validators.required, Validators.pattern('^[A-Z0-9_]+')]],
      name: ['', Validators.required],
      value: ['', Validators.required],
      description: [''],
      orderNum: ['', Validators.required],
      isDefault: false,
      isActive: true,
    });
    if (data) {
      formNewChildren.patchValue(data);
    }
    this.formListChildren.push(formNewChildren);
  }

  override save() {
    let isValid = true;
    const dataList = [];
    for (const item of this.formListChildren.controls) {
      if (isValid && item.status === 'INVALID') {
        isValid = false;
      }
      const data: CommonCategoryModel = cleanDataForm(<FormGroup>item);
      dataList.push(data);
      validateAllFormFields(<FormGroup>item);
    }
    const data = this.getDataForm();
    data.commons = dataList;
    if (this.form?.status === 'VALID' && isValid) {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          if (this.screenType === ScreenType.Create) {
            this.create(data);
          } else {
            this.update(data);
          }
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  deleteItem(item: FormGroup) {
    const code = item.controls['code'].value;
    this.formListChildren.removeAt(this.formListChildren.value.findIndex((x: CommonModel) => x.code === code));
  }
}
