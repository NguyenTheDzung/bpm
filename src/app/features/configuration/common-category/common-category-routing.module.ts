import { FunctionCode } from '@cores/utils/enums';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { CategoryListComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: CategoryListComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.CategoryCommon,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.CategoryCommon}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommonCategoryRoutingModule {}
