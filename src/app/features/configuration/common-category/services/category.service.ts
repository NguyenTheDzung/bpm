import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { map, Observable } from 'rxjs';
import { BaseService } from '@cores/services/base.service';
import { environment } from '@env';
import { CommonCategoryModel } from '../models/common-category.model';

@Injectable({
  providedIn: 'root',
})
export class CategoryService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.category_url}`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<DataTable>(`${this.baseUrl}/common-category`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override create(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/common-category`, data);
  }

  override findByCode(code: string) {
    return this.http
      .get<DataTable>(`${this.baseUrl}/common?common-category-code=${code}`)
      .pipe(map((data) => mapDataTableEmployee(data, code)));
  }

  findByCodeTable(code: string, params: any) {
    return this.http
      .get<DataTable>(`${this.baseUrl}/common?common-category-code=${code}`, { params: { ...params } })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override update(data: CommonCategoryModel) {
    return this.http.put<string>(`${this.baseUrl}/common-category/${data.code}`, data);
  }

  override delete(id: string | number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/common-category/${id}`);
  }
}
