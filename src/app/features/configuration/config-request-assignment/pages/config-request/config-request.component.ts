import {Component, Injector, OnInit} from '@angular/core';
import {BaseTableComponent} from "@shared/components";
import {RequestConfigService} from "../../services/request-config.service";
import {createErrorMessage} from "@cores/utils/functions";
import {DataConfigRequest, IEditConfigModel} from "../../model/config-request.model";
import {ScreenType} from "@cores/utils/enums";
import {EditRequestConfigComponent} from "../../components/edit-request-config/edit-request-config.component";
import {CommonModel} from "@common-category/models/common-category.model";

@Component({
  selector: 'app-config-request',
  templateUrl: './config-request.component.html',
  styleUrls: ['./config-request.component.scss']
})
export class ConfigRequestComponent extends BaseTableComponent<any> implements OnInit {

  dataDetail!: DataConfigRequest[]
  typeRequest!: CommonModel[]
  constructor(inject: Injector, private service: RequestConfigService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.getData()
    this.getTypeRequest()
  }

  getData() {
    this.loadingService.start()
    this.service.getData().subscribe({
      next: (res: any) => {
        this.dataDetail = res.data
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete()
      }
    })
  }

  getTypeRequest() {
    this.loadingService.start()
    this.service.getRequestType('REQUEST_TYPE_CONFIG').subscribe({
      next: (res) => {
        this.typeRequest = res
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }

  editRequest(data: IEditConfigModel) {
    this.dialogService.open(
      EditRequestConfigComponent,
      {
        showHeader: false,
        width: '60%',
        data: data
      }
    ).onClose.subscribe((result) => {
      if (result) {
        this.getData()
      }
    })
  }
}
