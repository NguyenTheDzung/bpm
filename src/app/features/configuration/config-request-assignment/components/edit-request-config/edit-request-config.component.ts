import { Component, Injector, OnInit } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { RequestConfigService } from '../../services/request-config.service';
import { Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { CommonModel } from '@common-category/models/common-category.model';
import { createErrorMessage } from '@cores/utils/functions';
import { IEditConfigModel } from '../../model/config-request.model';

@Component({
  selector: 'app-edit-request-config',
  templateUrl: './edit-request-config.component.html',
  styleUrls: ['./edit-request-config.component.scss'],
})
export class EditRequestConfigComponent extends BaseActionComponent implements OnInit {
  typeRequest!: CommonModel[];
  roleForward!: CommonModel[];
  roleSystem: CommonModel[] = [];
  department: any;
  user!: CommonModel[];
  dataDialog!: IEditConfigModel;

  constructor(inject: Injector, private service: RequestConfigService) {
    super(inject, service);
    this.dataDialog = this.configDialog.data;
  }

  ngOnInit(): void {
    const requestType = this.service.getRequestType('REQUEST_TYPE_CONFIG');
    const roleSystem = this.service.getRoleSystem();
    const department = this.service.getDepartment();
    this.loadingService.start();
    forkJoin([requestType, roleSystem, department]).subscribe({
      next: ([requestType, roleSystem, department]) => {
        this.typeRequest = requestType;
        this.roleForward = roleSystem?.data?.content;
        this.department = department?.data;
        this.changeValueDepartment(this.configDialog?.data.department);
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
    if (this.configDialog?.data) {
      this.getPicAssignRole({ value: this.dataDialog?.assignRole });
      this.checkTypeRequest({ value: this.dataDialog?.assignType });
      this.form.patchValue(this.configDialog.data);
    }
  }

  override form = this.fb.group({
    id: [null],
    department: [null, Validators.required],
    assignType: [null, Validators.required],
    assignRole: [null, Validators.required],
    forwardRole: [null],
    listUser: [null, Validators.required],
  });

  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close(false);
    } else {
      this.location.back();
    }
  }

  override save() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    this.loadingService.start();
    const data = this.form.value;
    data.requestType = this.dataDialog?.requestType;
    this.service.editRequestConfig(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.refDialog.close(true);
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  resetListUser() {
    this.form.controls['listUser'].reset([]);
  }

  resetAssignRole() {
    this.form.controls['assignRole'].reset();
  }

  onChangePicAssignRole($event: any) {
    this.resetListUser();
    this.getPicAssignRole($event);
  }

  onChangeDepartment($event: any) {
    this.resetAssignRole();
    this.resetListUser();
    this.user = [];
    this.changeValueDepartment($event);
  }

  getPicAssignRole($event: any) {
    this.loadingService.start();
    this.service.getPic($event?.value).subscribe({
      next: (res) => {
        this.user = res;
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  checkTypeRequest($event: any) {
    switch ($event?.value) {
      case 'NO_ASSIGN':
        this.disableControl();
        break;
      case 'DEFAULT_EQUAL':
        this.disableControl();
        break;
      default:
        this.form.controls['listUser'].enable();
        this.form.controls['listUser'].addValidators(Validators.required);
        this.form.controls['listUser'].updateValueAndValidity();
        this.form.updateValueAndValidity();
    }
  }

  disableControl() {
    this.form.controls['listUser'].setValue(null);
    this.form.controls['listUser'].disable();
    this.form.controls['listUser'].removeValidators(Validators.required);
    this.form.updateValueAndValidity();
  }

  changeValueDepartment(department: string) {
    this.roleSystem = [];
    const role = this.department?.find((value: any) => {
      return value.code === department;
    })?.roles;
    role?.forEach((value: any) => {
      this.roleSystem?.push({
        value: value,
        code: value,
      });
    });
    if (this.roleSystem.length == 1) {
      this.form.controls['assignRole'].setValue(this.roleSystem[0].code);
      this.getPicAssignRole(this.roleSystem[0]);
    }
  }
}
