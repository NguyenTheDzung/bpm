import {pages} from "./pages";
import {components} from "./components";
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {ConfigRequestRoutingModule} from "./config-request-routing.module";
import {ButtonModule} from "primeng/button";
import {CommonModule} from "@angular/common";
import {SharedModule} from "@shared/shared.module";
import {TableModule} from "primeng/table";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [ConfigRequestRoutingModule, SharedModule, ButtonModule, CommonModule, SharedModule, TableModule, TranslateModule],
  declarations: [...pages, ...components],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ConfigRequestModule {
}
