import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { CommonCategoryService } from '@cores/services/common-category.service';
import { IEditConfigModel } from '../model/config-request.model';
import { NotificationSystemService } from '@notification-system/services/notification-system.service';
import { RequestService } from '@requests/services/request.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RequestConfigService extends BaseService {
  constructor(
    http: HttpClient,
    private service: NotificationSystemService,
    private common: CommonCategoryService,
    private requestService: RequestService
  ) {
    super(http, `${environment.category_url}/request-type-config`);
  }

  getData() {
    return this.http.get(`${this.baseUrl}/list`);
  }

  editRequestConfig(body: IEditConfigModel) {
    return this.http.put(`${this.baseUrl}`, body);
  }

  getRequestType(param: string) {
    return this.common.getCommonCategory(param);
  }

  getRoleSystem() {
    return this.service.getRoleSystem();
  }

  getDepartment(): Observable<any> {
    return this.http.get(`${environment.category_url}/department/group/BPM`);
  }

  getPic(value: string[]) {
    return this.requestService.getPic({ roleName: value, activeOnly: true });
  }
}
