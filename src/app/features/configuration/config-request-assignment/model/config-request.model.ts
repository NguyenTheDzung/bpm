export interface DataConfigRequest {
  createdBy: string
  createdDate: string
  lastUpdatedBy: string
  lastUpdatedDate: string
  id: number
  requestType: string
  assignType: string
  assignRole: string
  listUser: any[]
  department: string
}

export interface IEditConfigModel {
  id: number
  department: string
  requestType: string
  assignType: string
  assignRole: string
  forwardRole: string
  listUser: string[]
}
