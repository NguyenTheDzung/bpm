import {FunctionCode} from '@cores/utils/enums';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FunctionGuardService} from '@cores/services/function-guard.service';
import {ConfigRequestComponent} from "./pages/config-request/config-request.component";

const routes: Routes = [
  {
    path: '',
    component: ConfigRequestComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.RequestConfig,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.RequestConfig}`,
    },
  },
  {path: '**', redirectTo: 'notfound', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigRequestRoutingModule {
}
