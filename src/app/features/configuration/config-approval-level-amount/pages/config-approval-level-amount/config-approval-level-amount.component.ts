import { AfterViewChecked, Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { ConfigApprovalLevelAmountService } from '../../service/config-approval-level-amount.service';
import { AmountLevel, ApprovalAmountMoneyRule, ApprovalRule } from '../../model/config-approval-level-amount.model';
import { cloneDeep } from 'lodash';
import { RoleService } from '@role-category/services/role.service';
import { RoleModel } from '@role-category/models/role.model';
import { MenuItem } from 'primeng/api';
import { NgForm } from '@angular/forms';
import { createErrorMessage } from '@cores/utils/functions';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-config-approval-level-amount',
  templateUrl: './config-approval-level-amount.component.html',
  styleUrls: ['./config-approval-level-amount.component.scss'],
})
export class ConfigApprovalLevelAmountComponent extends BaseComponent implements OnInit, AfterViewChecked {
  dataConfig: ApprovalRule = {
    amountOfMoneys: [],
    approverLevels: [],
    approverAmountMoneyRules: [],
  };
  dataConfigNew: ApprovalRule = {
    amountOfMoneys: [],
    approverLevels: [],
    approverAmountMoneyRules: [],
  };
  listAction = [
    {
      key: 'P',
      value: 'DECISION',
    },
    {
      key: 'Y',
      value: 'OPINION',
    },
    {
      key: '-',
      value: 'NO_ACTION',
    },
  ];
  listOperator = [
    {
      value: '<',
    },
    {
      value: '<=',
    },
  ];
  listRole: RoleModel[] = [];
  isEdit = false;
  menuRow: MenuItem[] = [
    {
      label: 'Thêm hàng bên trên',
      command: () => this.addRow(true),
    },
    { label: 'Thêm hàng bên dưới', command: () => this.addRow(false) },
    { label: 'Xoá hàng', command: () => this.deleteRow() },
  ];
  menuColumn: MenuItem[] = [
    { label: 'Thêm cột bên trái', command: () => this.addColumn(true) },
    { label: 'Thêm cột bên phải', command: () => this.addColumn(false) },
    { label: 'Xoá cột', command: () => this.deleteColumn() },
  ];
  indexRowSelected: number | null = null;
  indexColumnSelected: number | null = null;
  submited = false;

  constructor(inject: Injector, private service: ConfigApprovalLevelAmountService, private roleService: RoleService) {
    super(inject);
  }

  ngOnInit(): void {
    this.loadingService.start();
    forkJoin({
      listRole: this.roleService.search({ page: 0, size: 10000 }),
      dataConfig: this.service.getRuleApproval(),
    }).subscribe({
      next: (data) => {
        this.listRole = data.listRole.content;
        this.dataConfig = data.dataConfig;
        this.dataConfigNew = cloneDeep(data.dataConfig);
        this.loadingService.complete();
      },
    });
  }

  onClickColumnOrRow(index: number, type: 'col' | 'row') {
    if (type === 'col') {
      this.indexColumnSelected = index;
    } else {
      this.indexRowSelected = index;
    }
  }

  onEdit(value: boolean) {
    if (value) {
      this.dataConfigNew = cloneDeep(this.dataConfig);
    } else {
      this.submited = false;
    }
    this.isEdit = value;
  }

  addRow(isBefore: boolean) {
    const indexInsert = isBefore ? this.indexRowSelected! : this.indexRowSelected! + 1;
    this.dataConfigNew.approverLevels.splice(indexInsert, 0, {
      step: indexInsert,
      department: 'CLAIM',
      roleNames: [],
      description: '',
    });
    const rowRuleApproval: ApprovalAmountMoneyRule[] = [];
    this.dataConfigNew.amountOfMoneys.forEach((_item, i) => {
      rowRuleApproval.push({
        department: 'CLAIM',
        approverAction: 'NO_ACTION',
        approveStep: indexInsert,
        orderMoney: i,
      });
    });
    this.dataConfigNew.approverAmountMoneyRules.splice(indexInsert, 0, rowRuleApproval);
  }

  addColumn(isBefore: boolean) {
    const indexInsert = isBefore ? this.indexColumnSelected! : this.indexColumnSelected! + 1;
    this.dataConfigNew.amountOfMoneys.splice(indexInsert, 0, {
      amountTo: null,
      amountFrom: null,
      amountFromComparison: '>',
      amountToComparison: '<',
      amountStep: indexInsert,
      department: 'CLAIM',
    });
    this.dataConfigNew.approverAmountMoneyRules.forEach((item) => {
      item.splice(indexInsert, 0, {
        approverAction: 'NO_ACTION',
        orderMoney: indexInsert,
        approveStep: 0,
        department: 'CLAIM',
      });
    });
  }

  deleteRow() {
    this.dataConfigNew.approverLevels.splice(this.indexRowSelected!, 1);
    this.dataConfigNew.approverAmountMoneyRules.splice(this.indexRowSelected!, 1);
  }

  deleteColumn() {
    this.dataConfigNew.amountOfMoneys.splice(this.indexColumnSelected!, 1);
    this.dataConfigNew.approverAmountMoneyRules.forEach((item) => {
      item.splice(this.indexColumnSelected!, 1);
    });
  }

  getDisplayHeader(item: AmountLevel) {
    return `${item.amountFrom ?? ''} ${item.amountFromComparison ?? ''} x ${item.amountToComparison ?? ''} ${
      item.amountTo ?? ''
    }`;
  }

  getDisplayAction(item: ApprovalAmountMoneyRule) {
    return this.listAction.find((o) => o.value === item.approverAction)?.key;
  }

  onCreate(ngForm: NgForm) {
    this.submited = true;
    if (ngForm.form.valid && !this.loadingService.loading) {
      const data = cloneDeep(this.dataConfigNew);
      data.amountOfMoneys.forEach((item, i) => {
        item.amountStep = i + 1;
      });
      data.approverLevels.forEach((item, i) => {
        item.step = i + 1;
      });
      data.approverAmountMoneyRules.forEach((item, i) => {
        item.forEach((itemChild, indexChild) => {
          itemChild.approveStep = i + 1;
          itemChild.orderMoney = indexChild + 1;
        });
      });
      this.loadingService.start();
      this.service.save(data).subscribe({
        next: () => {
          this.messageService.success('MESSAGE.SUCCESS');
          this.loadingService.complete();
        },
        error: (e) => {
          this.loadingService.complete();
          this.messageService.error(createErrorMessage(e));
        },
      });
    }
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }
}
