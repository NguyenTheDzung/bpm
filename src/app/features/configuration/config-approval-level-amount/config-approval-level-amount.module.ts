import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ConfigApprovalLevelAmountRoutingModule } from './config-approval-level-amount-routing.module';
import { pages } from './pages';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';

@NgModule({
  declarations: [...pages, ...components],
  imports: [SharedModule, ConfigApprovalLevelAmountRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ConfigApprovalLevelAmountModule {}
