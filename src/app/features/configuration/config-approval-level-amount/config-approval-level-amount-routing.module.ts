import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigApprovalLevelAmountComponent } from './pages';
import { FunctionCode } from '@cores/utils/enums';
import { FunctionGuardService } from '@cores/services/function-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ConfigApprovalLevelAmountComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ConfigApprovalAmountLevel,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ConfigApprovalAmountLevel}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigApprovalLevelAmountRoutingModule {}
