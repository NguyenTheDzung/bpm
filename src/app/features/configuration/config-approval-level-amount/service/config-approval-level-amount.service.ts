import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { environment } from '@env';
import { map, Observable } from 'rxjs';
import { IResponseModel } from '@cores/models/response.model';
import { ApprovalRule } from '../model/config-approval-level-amount.model';

@Injectable({
  providedIn: 'root',
})
export class ConfigApprovalLevelAmountService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.category_url}/approved-amount-rule`);
  }

  getRuleApproval(): Observable<ApprovalRule> {
    return this.http.get<IResponseModel<ApprovalRule>>(`${this.baseUrl}/CLAIM`).pipe(map((res) => res.data));
  }

  save(data: ApprovalRule): Observable<any> {
    return this.http.put(`${this.baseUrl}/CLAIM`, data);
  }
}
