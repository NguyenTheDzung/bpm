export interface AmountLevel {
  id?: number;
  amountFrom: number | null;
  amountFromComparison: '>' | '>=';
  amountTo: number | null;
  amountToComparison: '<' | '<=';
  department: 'CLAIM';
  amountStep: number;
}

export interface ApprovalAmountMoneyRule {
  orderMoney: number;
  approveStep: number;
  department: 'CLAIM';
  approverAction: string;
}

export interface ApprovalLevel {
  step: number;
  roleNames: string[];
  description: string;
  department: 'CLAIM';
}

export interface ApprovalRule {
  amountOfMoneys: AmountLevel[];
  approverAmountMoneyRules: ApprovalAmountMoneyRule[][];
  approverLevels: ApprovalLevel[];
}
