import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

export interface ErrorModel {
  required?: boolean;
  minValue?: {
    value: number;
    min: number;
  };
  maxValue?: {
    value: number;
    max: number;
  };
}

export interface ValidatorDisplay {
  value: number;
  key: string;
}

@Component({
  selector: 'app-validator-config-rule',
  templateUrl: './validator-config-rule.component.html',
  styleUrls: ['./validator-config-rule.component.scss'],
})
export class ValidatorConfigRuleComponent {
  @Input() set errorLeft(error: ValidationErrors | null) {
    this._errorLeft = error;
  }

  _errorLeft: ErrorModel | null = null;
  _errorRight: ErrorModel | null = null;

  @Input() set errorRight(error: ValidationErrors | null) {
    this._errorRight = error;
  }

  get errors(): ValidatorDisplay | null {
    if (this._errorLeft) {
      return {
        key: this._errorLeft.required
          ? 'VALIDATE_ERROR.REQUIRED_LEFT_AMOUNT_RULE'
          : `VALIDATE_ERROR.${this._errorLeft.minValue ? 'MIN' : 'MAX'}_LEFT_AMOUNT_RULE`,
        value: this._errorLeft.maxValue ? this._errorLeft.maxValue.max : this._errorLeft.minValue?.min!,
      };
    }
    if (this._errorRight) {
      return {
        key: this._errorRight.required
          ? 'VALIDATE_ERROR.REQUIRED_RIGHT_AMOUNT_RULE'
          : `VALIDATE_ERROR.${this._errorRight.minValue ? 'MIN' : 'MAX'}_RIGHT_AMOUNT_RULE`,
        value: this._errorRight.maxValue ? this._errorRight.maxValue.max : this._errorRight.minValue?.min!,
      };
    }
    return null;
  }
}
