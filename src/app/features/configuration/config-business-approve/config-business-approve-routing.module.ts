import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FunctionCode } from '@cores/utils/enums';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { BusinessApproveListComponent } from '@config-business-approve/pages';

const routes: Routes = [
  {
    path: '',
    component: BusinessApproveListComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ConfigBusinessApprove,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ConfigBusinessApprove}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigBusinessApproveRoutingModule {}
