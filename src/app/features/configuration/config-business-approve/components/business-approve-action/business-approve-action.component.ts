import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { BusinessApproveService } from '@config-business-approve/services/business-approve.service';
import { BusinessApproveModel, StepBusinessApprove } from '@config-business-approve/models/business-approve.model';
import { RoleModel } from '@role-category/models/role.model';
import { createErrorMessage, validateAllFormFields } from '@cores/utils/functions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-business-approve-action',
  templateUrl: './business-approve-action.component.html',
  styleUrls: ['./business-approve-action.component.scss'],
})
export class BusinessApproveActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    code: [null, Validators.required],
    name: [null, Validators.required],
    description: [null],
    id: null,
  });
  formStep = this.fb.group({
    roleCode: [null, Validators.required],
    description: null,
    step: [null, Validators.required],
  });
  listStep: StepBusinessApprove[] = [];
  listRole: RoleModel[] = [];

  constructor(inject: Injector, private service: BusinessApproveService) {
    super(inject, service);
  }

  ngOnInit(): void {
    if (this.screenType !== ScreenType.Create) {
      this.form.get('code')?.disable();
      this.form.patchValue(this.data);
      this.listStep = this.configDialog?.data?.listStep || [];
      this.listRole = this.state?.listRole || [];
    }
  }

  addStep() {
    if (this.formStep.valid) {
      const dataForm = this.formStep.getRawValue();
      const item: StepBusinessApprove = {
        ...dataForm,
        businessApproveCode: this.data?.code,
        roleName: this.listRole?.find((item) => item.code === dataForm.roleCode)?.name,
      };
      this.listStep = [...this.listStep, item];
      this.formStep.reset();
    } else {
      validateAllFormFields(this.formStep);
    }
  }

  deleteItem(index: number) {
    this.listStep.splice(index, 1);
  }

  override update(data: BusinessApproveModel) {
    this.loadingService.start();
    let api: Observable<any>;
    if (this.title === 'BUSINESS_APPROVE.TITLE_UPDATE_STEP') {
      data.steps = this.listStep;
      api = this.service.updateBusinessStep(data);
    } else {
      api = this.service.update(data);
    }
    api.subscribe({
      next: () => {
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        this.refDialog.close(true);
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }
}
