import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { forkJoin, map, Observable, of } from 'rxjs';
import { BusinessApproveModel } from '@config-business-approve/models/business-approve.model';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class BusinessApproveService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.category_url}/business-approve`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);

    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override getState(): Observable<any> {
    if (_.isEmpty(this.state)) {
      return forkJoin({
        listRole: this.getRole().pipe(map((data) => data?.data?.content)),
      }).pipe(
        map(
          (data) =>
            (this.state = {
              ...this.state,
              ...data,
            })
        )
      );
    } else {
      return of(this.state);
    }
  }

  getRole() {
    return this.http.get<any>(`${environment.employee_url}/role?page=0&size=10000`);
  }

  override update(data: BusinessApproveModel): Observable<any> {
    return this.http.put(`${this.baseUrl}/${data.id}`, data);
  }

  updateBusinessStep(data: BusinessApproveModel): Observable<any> {
    return this.http.post(`${this.baseUrl}/assignStep`, data);
  }

  findById(id: number): Observable<BusinessApproveModel[]> {
    return this.http.get<BusinessApproveModel[]>(`${this.baseUrl}/${id}`).pipe(map((res: any) => res.data));
  }
}
