import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { pages } from './pages';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';
import { ConfigBusinessApproveRoutingModule } from '@config-business-approve/config-business-approve-routing.module';

@NgModule({
  declarations: [...pages, ...components],
  imports: [SharedModule, ConfigBusinessApproveRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ConfigBusinessApproveModule {}
