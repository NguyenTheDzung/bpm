import { BusinessApproveListComponent } from '@config-business-approve/pages/business-approve-list/business-approve-list.component';

export const pages = [BusinessApproveListComponent];

export * from '@config-business-approve/pages/business-approve-list/business-approve-list.component';
