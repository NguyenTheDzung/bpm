import { Component, Injector, OnInit } from '@angular/core';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { BusinessApproveModel } from '@config-business-approve/models/business-approve.model';
import { BusinessApproveService } from '@config-business-approve/services/business-approve.service';
import { BusinessApproveActionComponent } from '@config-business-approve/components';

@Component({
  selector: 'app-business-approve-list',
  templateUrl: './business-approve-list.component.html',
})
export class BusinessApproveListComponent extends BaseTableComponent<BusinessApproveModel> implements OnInit {
  override params: BusinessApproveModel = {
    code: '',
    name: '',
  };

  constructor(inject: Injector, private service: BusinessApproveService) {
    super(inject, service);
  }

  override initConfigAction() {
    this.configAction = {
      component: BusinessApproveActionComponent,
      title: 'title',
      dialog: {
        width: '80%',
      },
    };
  }

  ngOnInit(): void {
    this.getState();
  }

  override viewCreate() {
    if (!this.configAction?.component) {
      return;
    }
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: 'BUSINESS_APPROVE.TITLE_CREATE',
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        screenType: ScreenType.Create,
        state: this.propData,
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.search();
        }
      },
    });
  }

  viewUpdate(item: BusinessApproveModel) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();

    const dialog = this.dialogService?.open(this.configAction.component, {
      header: 'BUSINESS_APPROVE.TITLE_UPDATE',
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        model: item,
        state: this.stateData,
        screenType: ScreenType.Update,
      },
    });

    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.search();
        }
      },
    });
    this.loadingService.complete();
  }

  viewUpdateStep(item: BusinessApproveModel) {
    if (this.loadingService.loading || !this.configAction?.component) {
      return;
    }
    this.loadingService.start();
    this.service.findById(item.id!).subscribe({
      next: (data) => {
        this.dialogService?.open(this.configAction!.component, {
          header: 'BUSINESS_APPROVE.TITLE_UPDATE_STEP',
          showHeader: false,
          width: this.configAction!.dialog?.width || '85%',
          data: {
            model: item,
            listStep: data,
            state: this.stateData,
            screenType: ScreenType.Update,
          },
        });
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }
}
