export interface BusinessApproveModel {
  id?: number;
  code?: string;
  name?: string;
  description?: string;
  steps?: StepBusinessApprove[];
}

export interface StepBusinessApprove {
  step?: number;
  description?: string;
  roleCode?: string;
  roleName?: string;
  businessApproveCode?: string;
}
