import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonModel } from '@common-category/models/common-category.model';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { CommonCategoryService } from '@cores/services/common-category.service';
import { cleanData, mapDataTable } from '@cores/utils/functions';
import { environment } from '@env';
import { catchError, map, Observable, of } from 'rxjs';
import { CategoryHistoryRequest } from '../models/history.model';

@Injectable({
  providedIn: 'root',
})
export class HistoryService extends BaseService {
  override state: CategoryHistoryRequest | undefined;

  constructor(http: HttpClient, private commonService: CommonCategoryService) {
    super(http, `${environment.refund_url}/history`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = cleanData(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}/search`, { params: newParam })
        .pipe(map((data) => mapDataTable(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}/search`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTable(data, params)));
  }

  override getState(): Observable<CategoryHistoryRequest> {
    return this.getHistoryDroplist()
      .pipe(catchError(() => of<CommonModel[]>([])))
      .pipe(
        map(
          (data: any) =>
            (this.state = {
              ...this.state,
              ...data,
            })
        )
      );
  }

  getHistoryDroplist() {
    return this.http.get<CommonModel[]>(`${this.baseURL}/search?flag=true&groupPath=/NB/NB_EMPLOYEE`);
  }
}
