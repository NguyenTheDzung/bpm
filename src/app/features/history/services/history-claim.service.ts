import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { HistoryClaim, ParamSearch } from '@history/models/history.model';
import { mapDataTableWithType, removeParamSearch } from '@cores/utils/functions';
import { map, Observable } from 'rxjs';
import { CommonCategoryService } from '@cores/services/common-category.service';
import { IPageableResponseModel, IResponseModel } from '@cores/models/response.model';
import { DataTable } from '@cores/models/data-table.model';
import { CreateClaimService } from '@create-requests/service/create-claim.service';
import { RequestService } from '@requests/services/request.service';

@Injectable({
  providedIn: 'root',
})
export class HistoryClaimService {
  constructor(
    private http: HttpClient,
    private commonService: CommonCategoryService,
    private createClaimService: CreateClaimService,
    private requestService: RequestService
  ) {}

  search(params: ParamSearch): Observable<DataTable<HistoryClaim>> {
    return this.http
      .get<IResponseModel<IPageableResponseModel<HistoryClaim>>>(`${environment.claim_url}/claim/histories`, {
        params: removeParamSearch(params),
      })
      .pipe(map((res) => mapDataTableWithType(res.data, params)));
  }
}
