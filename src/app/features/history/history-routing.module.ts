import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoryListComponent } from './pages';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { FunctionCode } from '@cores/utils/enums';
import { HistoryClaimComponent } from '@history/pages/history-claim/history-claim.component';

const routes: Routes = [
  {
    path: 'claims',
    data: { breadcrumb: `LAYOUTS.MENU.${FunctionCode.HistoryClaim}` },
    children: [
      {
        path: '',
        component: HistoryClaimComponent,
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.HistoryClaim,
          // breadcrumb: `LAYOUTS.MENU.${FunctionCode.HistoryClaim}`,
        },
      },
      {
        path: 'detail',
        loadChildren: () => import('@claim-detail/claim-tpa.module').then((m) => m.ClaimTPAModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.HistoryClaim,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
    ],
  },
  {
    path: 'requests',
    data: {
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.History}`,
    },
    children: [
      {
        path: '',
        component: HistoryListComponent,
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.History,
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.History}`,
        },
      },
      {
        path: 'detail/fee-refund/:requestId/:type',
        loadChildren: () => import('../detail-request/fee-refund/fee-refund.module').then((m) => m.FeeRefundModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.History,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/nbu/:requestId/:type',
        loadChildren: () => import('../detail-request/nbu/nbu.module').then((m) => m.NbuModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.History,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/lapse-reversal/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/lapse-reversal/lapse-reversal.module').then((m) => m.LapseReversalModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.History,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/submit-payment-receipt/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/submit-payment-receipt/submit-payment-receipt.module').then(
            (m) => m.SubmitPaymentReceiptModule
          ),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.History,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/premium-adjustment/:requestId/07',
        loadChildren: () =>
          import('../detail-request/premium-adjustment/premium-adjustment.module').then(
            (m) => m.PremiumAdjustmentModule
          ),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.History,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
    ],
  },

  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryRoutingModule {}
