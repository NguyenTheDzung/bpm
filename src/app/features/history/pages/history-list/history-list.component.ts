import { Component, Injector, OnInit } from '@angular/core';
import { getValueDateTimeLocal } from '@cores/utils/functions';
import { ScreenType } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import * as _ from 'lodash';
import { RequestService } from 'src/app/features/requests/services/request.service';
import { CategoryHistoryRequest, HistoryModel } from '../../models/history.model';
import { HistoryService } from '../../services/history.service';
import { RequestModel } from 'src/app/features/requests/models/request.model';
import { getUrlDetailRequest } from '@detail-request/shared/utils/functions';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss'],
})
export class HistoryListComponent extends BaseTableComponent<HistoryModel> implements OnInit {
  username: string = '';
  // isEmpty = false;
  screenType!: ScreenType.Detail;
  isSearch = true;
  override stateData: CategoryHistoryRequest = {};
  override params: HistoryModel = {
    requestId: null,
    requestCode: null,
    status: null,
    requestType: null,
    policyNumber: null,
    submitType: null,
    startDate: null,
    endDate: null,
    icCode: null,
    icName: null,
    pic: null,
  };

  constructor(injector: Injector, service: HistoryService, private requestService: RequestService) {
    super(injector, service);
  }

  onReset() {
    this.params = {
      requestId: null,
      requestCode: null,
      status: null,
      requestType: null,
      policyNumber: null,
      submitType: null,
      startDate: null,
      endDate: null,
      icCode: null,
      icName: null,
      pic: null,
    };
    this.dataTable = {
      content: [],
      currentPage: 0,
      size: 10,
      totalElements: 0,
      totalPages: 0,
      first: 0,
      numberOfElements: 0,
    };
  }

  ngOnInit() {
    this.loadingService.start();
    this.username = this.currUser?.username;
    this.requestService.getState().subscribe({
      next: (state) => {
        this.stateData.resolutionDropList = state.listResolution || [];
        this.stateData.statusDroplist = state.listStatus || [];
        this.stateData.requestTypeDroplist = state.listRequestType || [];
        this.stateData.distributionChannelDroplist = state.listDistributionChannel || [];
        this.requestService.getPic({ roleName: state.listGroup?.map((x: RequestModel) => x.value!) ?? [] }).subscribe({
          next: (res) => {
            this.stateData.picDroplist = res;
          },
        });
        if (this.objFunction?.prevParams) {
          this.params = _.cloneDeep(this.objFunction.prevParams);
          this.search();
        } else {
          this.loadingService.complete();
        }
      },
    });
  }

  checkParamEmptyAndUser() {
    return (
      Object.values(this.params).every((x) => _.isEmpty(x)) &&
      _.isEmpty(getValueDateTimeLocal(this.params.startDate || this.params.endDate))
    );
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    const params = this.mapDataSearch();
    if (this.checkParamEmptyAndUser()) {
      return;
    }
    this.loadingService.start();
    this.requestService.history(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.error('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
        this.objFunction!.prevParams = params;
      },
      error: (err) => {
        this.messageService?.error(err.error.message);
        this.loadingService.complete();
      },
    });
  }

  override mapDataSearch(): HistoryModel {
    return {
      ...this.params,
      startDate: getValueDateTimeLocal(this.params.startDate),
      endDate: getValueDateTimeLocal(this.params.endDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }

  doDetail(item: RequestModel) {
    this.router
      .navigate([getUrlDetailRequest(this.router.url, item.requestTypeCode!, item.requestCode!)], {
        state: item,
      })
      .then();
  }

  onChangeValueDate() {
    const startDate = this.params.startDate ? new Date(this.params.startDate).valueOf() : 0;
    const endDate = this.params.endDate ? new Date(this.params.endDate).valueOf() : 0;
    if (startDate > 0 && endDate > 0 && startDate > endDate) {
      this.messageService?.warn(
        'The date of receipt from the date should not be greater than the date of receipt to the date'
      );
      this.isSearch = false;
    } else {
      this.isSearch = true;
    }
  }

  getRequestTypeDesc(item: RequestModel) {
    return this.stateData?.requestTypeDroplist?.find((itemType) => itemType.value === item.requestTypeCode)
      ?.multiLanguage[this.translateService.currentLang];
  }
}
