import { Component, Injector, OnInit } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { HistoryClaim, ParamSearch } from '@history/models/history.model';
import { HistoryClaimService } from '@history/services/history-claim.service';
import { DataTable } from '@cores/models/data-table.model';
import { PaginatorModel } from '@cores/models/paginator.model';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import * as moment from 'moment/moment';
import * as _ from 'lodash';
import { RequestType } from '@create-requests/utils/constants';
import { CategoryClaimState } from '../../../claims/models/claim-management.model';
import { ClaimService } from '../../../claims/services/claim.service';

@Component({
  selector: 'app-history-claim',
  templateUrl: './history-claim.component.html',
  styleUrls: ['./history-claim.component.scss'],
})
export class HistoryClaimComponent extends BaseComponent implements OnInit {
  params = this.fb.group({
    requestId: null,
    claimId: null,
    status: null,
    resolution: null,
    requestSource: null,
    fromDate: null,
    toDate: null,
    typeRequest: null,
    claimStatus: null,
    policyNumber: null,
    pic: null,
    icCode: null,
    icName: null,
  });
  categories: CategoryClaimState = {
    listPic: [],
    listSource: [],
    listStatus: [],
    listStatusClaim: [],
    listResolution: [],
    listRequestType: [],
  };
  dataTable: DataTable<HistoryClaim> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  prevParams?: ParamSearch;

  constructor(injector: Injector, private service: HistoryClaimService, private claimService: ClaimService) {
    super(injector);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.claimService.getCategories().subscribe({
      next: (data) => {
        this.categories = data;
        this.categories.listRequestType = data.listRequestType?.filter((item) =>
          [RequestType.TYPE11].includes(item.value)
        );
        // this.search(true);
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }

  search(firstPage?: boolean) {
    if (this.loadingService.loading || this.checkParamEmptyAndUser()) {
      return;
    }
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const dataForm = this.params.getRawValue();
    const params: ParamSearch = {
      ...dataForm,
      fromDate: getValueDateTimeLocal(dataForm.fromDate, moment.HTML5_FMT.DATE),
      toDate: getValueDateTimeLocal(dataForm.toDate, moment.HTML5_FMT.DATE),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
    this.service.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  onReset() {
    this.params.reset();
    this.dataTable = {
      content: [],
      currentPage: 0,
      size: 10,
      totalElements: 0,
      totalPages: 0,
      first: 0,
      numberOfElements: 0,
    };
    // this.search(true);
  }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.search();
  }

  checkParamEmptyAndUser() {
    const params = this.params.getRawValue();
    return (
      Object.values(params).every((x) => _.isEmpty(x)) &&
      _.isEmpty(getValueDateTimeLocal(params.fromDate || params.toDate))
    );
  }

  async doDetail(item: typeof this.dataTable.content[0], _index: number) {
    this.loadingService.start();
    const requestId = item.requestId;
    try {
      await this.router.navigate(['detail', 'tpa', requestId], { relativeTo: this.route });
    } finally {
      this.loadingService.complete();
    }
  }
}
