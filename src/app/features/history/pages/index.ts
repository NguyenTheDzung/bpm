import { HistoryListComponent } from './history-list/history-list.component';
import { HistoryClaimComponent } from '@history/pages/history-claim/history-claim.component';

export const pages = [HistoryListComponent, HistoryClaimComponent];

export * from './history-list/history-list.component';
