import { CommonModel } from '@common-category/models/common-category.model';

export interface HistoryModel {
  requestId?: number | string | null;
  requestCode?: string | null;
  policyNumber?: string | null;
  requestType?: boolean | string | null;
  id?: string;
  status?: boolean | string | null;
  pic?: boolean | string | null;
  submitType?: number | string | null;
  startDate?: string | null;
  endDate?: string | null;
  icCode?: string | null;
  icName?: string | null;
  page?: number;
  size?: number;
  requestTypeCode?: string;
}

export interface CategoryHistoryRequest {
  distributionChannelDroplist?: CommonModel[];
  requestTypeDroplist?: CommonModel[];
  picDroplist?: CommonModel[];
  statusDroplist?: CommonModel[];
  resolutionDropList?: CommonModel[];
}

export interface HistoryClaim {
  requestId: string;
  requestType: string;
  policyNumber: string;
  icCode: string;
  icName: string;
  policyHolder: string;
  policyName: string;
  claimType: string;
  status: string;
  resolution: string;
  claimStatus: string;
  pic: string;
  sla: number;
  requestSource: string;
  createdDate: string;
  updateDate: string;
  submitFirstRequest: string;
  bpLA: string;
  bpLAName: string;
  bp: string;
  bpName: string;
}

export type ParamSearch = HistoryClaim & { page: number; size: number };

export type Columns = 'claimId' | 'requestId';
