import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { HistoryRoutingModule } from './history-routing.module';
import { pages } from './pages';

@NgModule({
  declarations: [...pages],
  imports: [HistoryRoutingModule, SharedModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  exports: [],
})
export class HistoryModule {}
