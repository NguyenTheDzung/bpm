import { P2pListComponent } from './p2p-list/p2p-list.component';
import { NonP2pListComponent } from './non-p2p-list/non-p2p-list.component';

export const pages = [P2pListComponent, NonP2pListComponent];

export * from './p2p-list/p2p-list.component';
export * from './non-p2p-list/non-p2p-list.component';
