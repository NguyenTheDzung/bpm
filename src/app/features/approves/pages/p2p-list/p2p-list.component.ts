import { Component, Injector, OnInit } from '@angular/core';
import { getValueDateTimeLocal } from '@cores/utils/functions';
import { BaseTableComponent } from '@shared/components';
import { RequestModel, StateRequest } from '@requests/models/request.model';
import { Resolution, StatusValue } from '@cores/utils/constants';
import { ApproveService } from '../../services/approve.service';
import { RequestService } from '@requests/services/request.service';
import { getUrlDetailRequest } from '@detail-request/shared/utils/functions';

@Component({
  selector: 'p2p-list',
  templateUrl: './p2p-list.component.html',
  styleUrls: ['./p2p-list.component.scss'],
})
export class P2pListComponent extends BaseTableComponent<RequestModel> implements OnInit {
  override params: RequestModel = {
    status: [StatusValue.In_process],
    startDate: null,
    endDate: null,
    policyNumber: null,
    customerName: null,
    amount: null,
    requestType: this.requestType.TYPE08,
    resolution: [Resolution.COL_P2P_INPROCESS],
    requestCode: null,
    pics: null,
  };
  override stateData: StateRequest = {};

  constructor(inject: Injector, private service: ApproveService, private requestService: RequestService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.requestService.getState().subscribe({
      next: (state) => {
        const roles = this.authService.getUserRoles();
        this.stateData.listStatus = state.listStatus || [];
        this.stateData.listResolution = state.resolutionCollection || [];
        for (const role of roles) {
          if (role.indexOf('FI') > -1) {
            this.params.resolution = [Resolution.COL_FI_INPROCESS];
            break;
          }
        }
        this.stateData.listGroup = state.listGroup || [];
        this.stateData.listPic = state.listPic;
        this.search(true);
      },
    });
  }

  override mapDataSearch() {
    return {
      ...this.params,
      startDate: getValueDateTimeLocal(this.params.startDate),
      endDate: getValueDateTimeLocal(this.params.endDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }

  doDetail(item: RequestModel) {
    this.router
      .navigate([getUrlDetailRequest(this.router.url, item.requestTypeCode!, item.requestId!)], {
        state: item,
      })
      .then();
  }

  getStatusDesc(status: string) {
    return this.stateData?.listStatus?.find((item) => item.value === status)?.multiLanguage[
      this.translateService.currentLang
    ];
  }
}
