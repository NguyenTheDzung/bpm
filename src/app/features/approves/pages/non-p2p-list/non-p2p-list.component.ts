import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { RequestService } from '@requests/services/request.service';
import { RequestModel, StateRequest } from '@requests/models/request.model';
import { Resolution, Roles, StatusValue } from '@cores/utils/constants';
import { ClaimsComponent } from '@approves/components/claims/claims.component';
import { CollectionComponent } from '@approves/components/collections/collection.component';
import { getValueDateTimeLocal } from '@cores/utils/functions';
import { DataTable } from '@cores/models/data-table.model';
import { size } from 'lodash';

@Component({
  selector: 'non-p2p-list',
  templateUrl: './non-p2p-list.component.html',
  styleUrls: ['./non-p2p-list.component.scss'],
})
export class NonP2pListComponent extends BaseComponent implements OnInit {
  @ViewChild(ClaimsComponent) claimComponent?: ClaimsComponent;
  @ViewChild(CollectionComponent) collectionComponent?: CollectionComponent;

  params: RequestModel = {
    status: [StatusValue.In_process],
    startDate: null,
    endDate: null,
    policyNumber: null,
    customerName: null,
    amount: null,
    requestType: this.requestType.TYPE10,
    resolution: null,
    requestCode: null,
    pics: null,
  };
  stateData: StateRequest = {};
  tabIndex: number = 0;

  constructor(inject: Injector, private requestService: RequestService) {
    super(inject);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.requestService.getState().subscribe({
      next: (state) => {
        const roles = this.authService.getUserRoles();
        this.stateData.listStatus = state.listStatus || [];
        this.stateData.listResolution = state.resolutionCollection || [];
        for (const role of roles) {
          switch (role) {
            case Roles.OP_COL_MANAGER:
              this.params.resolution = [Resolution.OP_COL_MANAGER_INPROCESS];
              break;
            case Roles.OP_DIRECTOR:
              this.params.resolution = [Resolution.OP_DIRECTOR_INPROCESS];
              break;
          }
        }
        this.stateData.listGroup = state.listGroup || [];
        this.stateData.listPic = state.listPic;
        this.search(true);
      },
    });
  }

  mapDataSearch(dataTable: DataTable) {
    return {
      ...this.params,
      startDate: getValueDateTimeLocal(this.params.startDate),
      endDate: getValueDateTimeLocal(this.params.endDate),
      page: dataTable.currentPage,
      size: dataTable.size,
    };
  }

  approveOrReject(status: number) {
    if (this.tabIndex === 1) {
      this.claimComponent?.approve(status);
    } else {
      this.collectionComponent?.approve(status);
    }
  }

  search(firstPage?: boolean) {
    if (this.tabIndex === 1) {
      this.claimComponent?.search(firstPage);
    } else {
      this.collectionComponent?.search(firstPage);
    }
  }

  isDisabledAction(): boolean {
    if (this.tabIndex === 1) {
      return size(this.claimComponent?.selectionItem) == 0;
    } else {
      return size(this.collectionComponent?.selectionItem) == 0;
    }
  }

  changeTabView(index: number) {
    this.tabIndex = index;
    // kiểm tra nếu view tab claim lần đầu thì gọi hàm search
    if (index === 1 && !this.claimComponent?.searched) {
      this.claimComponent?.search(true);
    }
  }
}
