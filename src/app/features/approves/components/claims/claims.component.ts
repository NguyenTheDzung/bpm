import { Component, Injector, Input } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { ApproveNonP2p, RequestModel, StateRequest } from '@requests/models/request.model';
import { getUrlDetailRequest } from '@detail-request/shared/utils/functions';
import { createErrorMessage } from '@cores/utils/functions';
import { ApproveClaimService } from '@approves/services/approve-claim.service';
import { NonP2pListComponent } from '@approves/pages';
import { DataTable } from '@cores/models/data-table.model';
import { PaginatorModel } from '@cores/models/paginator.model';

@Component({
  selector: 'approve-claim-table',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss'],
})
export class ClaimsComponent extends BaseComponent {
  @Input() stateData?: StateRequest;
  dataTable: DataTable = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  prevParams?: RequestModel;
  isApproveDone = true;
  selectionItem: any[] = [];
  searched = false;

  constructor(
    inject: Injector,
    private service: ApproveClaimService,
    private nonP2pListComponent: NonP2pListComponent
  ) {
    super(inject);
  }

  // selectRow(isClick: boolean) {
  //   if (isClick) {
  //     this.selectionItem = this.selectionItem.filter((value) => value.stepApprove !== '1');
  //   } else {
  //     this.selectionItem = [];
  //   }
  // }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.search();
  }

  search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.searched = true;
    const params = this.nonP2pListComponent.mapDataSearch(this.dataTable);
    this.loadingService.start();
    this.service.getData(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0 && this.isApproveDone) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.isApproveDone = true;
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  approve(status: number) {
    const data: ApproveNonP2p = {
      status: status,
      requests: this.selectionItem?.map((item) => item.requestId),
      type: 2,
    };
    if (status === 0) {
      this.messageService?.confirmReject().subscribe((res) => {
        if (res.isConfirm) {
          data.note = res.data;
          this.updateData(data);
        }
      });
    } else {
      this.messageService.confirm().subscribe((isConFirm) => {
        if (isConFirm) {
          this.updateData(data);
        }
      });
    }
  }

  updateData(data: ApproveNonP2p) {
    this.loadingService.start();
    this.service.approveClaimTPA(data).subscribe({
      next: () => {
        this.isApproveDone = false;
        this.search();
        this.selectionItem = [];
        this.messageService.success('MESSAGE.APPROVE_SUCCESS');
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  doDetail(item: RequestModel) {
    const step = this.requestType.TYPE09 === item.requestTypeCode ? 1 : 0;
    this.router
      .navigate([getUrlDetailRequest(this.router.url, item.requestTypeCode!, item.requestId!)], {
        state: item,
        queryParams: {
          tab: 0,
          step,
        },
      })
      .then();
  }
}
