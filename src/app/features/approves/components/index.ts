import { ClaimsComponent } from '@approves/components/claims/claims.component';
import { CollectionComponent } from '@approves/components/collections/collection.component';

export const components = [ClaimsComponent, CollectionComponent];
