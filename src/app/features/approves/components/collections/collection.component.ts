import { Component, Injector, Input } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { ApproveNonP2p, RequestModel, StateRequest } from '@requests/models/request.model';
import { RequestService } from '@requests/services/request.service';
import { getUrlDetailRequest } from '@detail-request/shared/utils/functions';
import { createErrorMessage } from '@cores/utils/functions';
import * as _ from 'lodash';
import { DataTable } from '@cores/models/data-table.model';
import { NonP2pListComponent } from '@approves/pages';
import { PaginatorModel } from '@cores/models/paginator.model';

@Component({
  selector: 'approve-collection-table',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
})
export class CollectionComponent extends BaseComponent {
  @Input() stateData?: StateRequest;
  dataTable: DataTable<RequestModel> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  selectionItem: RequestModel[] = [];
  prevParams?: RequestModel;
  isApproveDone = true;
  searched = false;

  constructor(
    inject: Injector,
    private requestService: RequestService,
    private nonP2pListComponent: NonP2pListComponent
  ) {
    super(inject);
  }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.search();
  }

  selectRow(isClick: boolean) {
    if (isClick) {
      this.selectionItem = _.filter(this.selectionItem, (value) => value.stepApprove !== '2');
    } else this.selectionItem = [];
  }

  approve(status: number) {
    const data: ApproveNonP2p = {
      status: status,
      requests: this.selectionItem.map((item) => item.requestId!),
      type: 2,
    };

    if (status === 0) {
      this.messageService?.confirmReject().subscribe((res) => {
        if (res.isConfirm) {
          data.note = res.data;
          this.updateData(data);
        }
      });
    } else {
      this.messageService.confirm().subscribe((isConFirm) => {
        if (isConFirm) {
          this.updateData(data);
        }
      });
    }
  }

  updateData(data: ApproveNonP2p) {
    this.loadingService.start();
    this.requestService.updateActionContractRestore(data).subscribe({
      next: () => {
        this.isApproveDone = false;
        this.search();
        this.selectionItem = [];
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  search(firstPage?: boolean) {
    this.selectionItem = [];
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.searched = true;
    this.loadingService.start();
    const params = this.nonP2pListComponent.mapDataSearch(this.dataTable);
    this.requestService.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0 && this.isApproveDone) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.isApproveDone = true;
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  doDetail(item: RequestModel) {
    const step = [this.requestType.TYPE07, this.requestType.TYPE09].includes(item.requestTypeCode as keyof typeof this.requestType) ? 1 : 0;
    this.router
      .navigate([getUrlDetailRequest(this.router.url, item.requestTypeCode!, item.requestId!)], {
        state: item,
        queryParams: {
          tab: 0,
          step,
        },
      })
      .then();
  }
}
