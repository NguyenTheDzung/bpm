import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ApprovesRoutingModule } from './approves-routing.module';
import { pages } from './pages';
import { components } from './components';

@NgModule({
  imports: [ApprovesRoutingModule, SharedModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [...pages, ...components],
})
export class ApprovesModule {}
