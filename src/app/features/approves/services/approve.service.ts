import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';

@Injectable({
  providedIn: 'root',
})
export class ApproveService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.task_url}/approve`);
  }
}
