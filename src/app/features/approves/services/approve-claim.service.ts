import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { map, Observable } from 'rxjs';
import { DataTable } from '@cores/models/data-table.model';
import { mapDataTableWithType, removeParamSearch } from '@cores/utils/functions';
import { IPageableResponseModel, IResponseModel } from '@cores/models/response.model';
import { ApproveNonP2p } from '@requests/models/request.model';

@Injectable({
  providedIn: 'root',
})
export class ApproveClaimService extends BaseService {
  constructor(http: HttpClient) {
    super(http, `${environment.claim_url}/claim`);
  }

  getData(params?: any): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<IResponseModel<IPageableResponseModel<{ claimid: any }>>>(`${this.baseUrl}/manage`, { params: newParam })
      .pipe(
        map((response) => {
          return mapDataTableWithType(response.data, params);
        })
      );
  }

  approveClaimTPA(data: ApproveNonP2p) {
    return this.http.put(`${this.baseUrl}/approve`, data);
  }
}
