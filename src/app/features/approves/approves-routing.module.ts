import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { FunctionCode } from '@cores/utils/enums';
import { NonP2pListComponent, P2pListComponent } from './pages';

const routes: Routes = [
  {
    path: 'p2p',
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ListP2P,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ListP2P}`,
    },
    children: [
      {
        path: '',
        component: P2pListComponent,
      },
      {
        path: 'detail/fee-refund/:requestId/:type',
        loadChildren: () => import('../detail-request/fee-refund/fee-refund.module').then((m) => m.FeeRefundModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/nbu/:requestId/:type',
        loadChildren: () => import('../detail-request/nbu/nbu.module').then((m) => m.NbuModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/lapse-reversal/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/lapse-reversal/lapse-reversal.module').then((m) => m.LapseReversalModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/submit-payment-receipt/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/submit-payment-receipt/submit-payment-receipt.module').then(
            (m) => m.SubmitPaymentReceiptModule
          ),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/premium-adjustment/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/premium-adjustment/premium-adjustment.module').then(
            (m) => m.PremiumAdjustmentModule
          ),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
    ],
  },
  {
    path: 'non-p2p',
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ListNonP2P,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ListNonP2P}`,
    },
    children: [
      {
        path: '',
        component: NonP2pListComponent,
      },
      {
        path: 'detail/fee-refund/:requestId/:type',
        loadChildren: () => import('../detail-request/fee-refund/fee-refund.module').then((m) => m.FeeRefundModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListNonP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/nbu/:requestId/:type',
        loadChildren: () => import('../detail-request/nbu/nbu.module').then((m) => m.NbuModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListNonP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/lapse-reversal/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/lapse-reversal/lapse-reversal.module').then((m) => m.LapseReversalModule),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListNonP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/submit-payment-receipt/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/submit-payment-receipt/submit-payment-receipt.module').then(
            (m) => m.SubmitPaymentReceiptModule
          ),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListNonP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/premium-adjustment/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/premium-adjustment/premium-adjustment.module').then(
            (m) => m.PremiumAdjustmentModule
          ),
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ListNonP2P,
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
    ],
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApprovesRoutingModule {}
