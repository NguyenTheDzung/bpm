import { Component, Injector, OnInit } from '@angular/core';
import { getValueDateTimeLocal } from '@cores/utils/functions';
import { BaseTableComponent } from '@shared/components';
import { RequestService } from 'src/app/features/requests/services/request.service';
import { ReportModel } from '../../models/report.model';
import { ReportService } from '../../services/report.service';
import { Frequency } from '@cores/utils/constants';
import { StateRequest } from '@reports/models/fi-report.model';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-freelook-cancellation-report',
  templateUrl: './freelook-cancellation-report.component.html',
  styleUrls: ['./freelook-cancellation-report.component.scss'],
})
export class FreelookCancellationReportComponent extends BaseTableComponent<ReportModel> implements OnInit {
  override stateData: StateRequest = {
    resolutionRefund: [],
    requestTypeRefund: [],
    listRefuseReason: [],
    listRequestType: [],
    listPic: [],
  };

  minDate = new Date();
  override params: ReportModel = {
    mainProductName: null,
    policyNum: null,
    reversedToDate: null,
    reversedFromDate: null,
    selectedResolutions: null,
    requestType: null,
    pics: null,
    appNum: null,
  };

  constructor(inject: Injector, private service: ReportService, private requestService: RequestService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.getState();

    this.fileNameExcel = 'bao-cao-huy-freelook.xlsx';
  }

  override getState() {
    this.requestService.getState().subscribe({
      next: (state) => {
        this.stateData = cloneDeep(state);
        this.search(true);
      },
    });
  }

  override mapDataSearch() {
    return {
      ...this.params,
      reversedFromDate: getValueDateTimeLocal(this.params.reversedFromDate),
      reversedToDate: getValueDateTimeLocal(this.params.reversedToDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }

  periovic(item: string) {
    switch (item) {
      case Frequency.Weeklus:
        return 'Weeklus';
      case Frequency.Annually:
        return 'Annually';
      case Frequency.SemiAnnually:
        return 'SemiAnnually';
      case Frequency.Monthly:
        return 'Monthly';
      case Frequency.SinglePerium:
        return 'SinglePerium';
      case Frequency.Quartelly:
        return 'Quartelly';
      default:
        return 'Biweekly';
    }
  }

  onReset() {
    this.params = {
      mainProductName: null,
      policyNum: null,
      reversedToDate: null,
      reversedFromDate: null,
    };
    this.search();
  }

  validatorDate(event: any) {
    this.params.reversedFromDate = event;
    const fromDate = this.params.reversedFromDate ? new Date(this.params.reversedFromDate).valueOf() : 0;
    const toDate = this.params.reversedToDate ? new Date(this.params.reversedToDate).valueOf() : 0;
    this.minDate = new Date(this.params.reversedFromDate!);
    if (fromDate > toDate) {
      this.params.reversedToDate = null;
    }
  }

  getReversedReasonDesc(item: ReportModel) {
    return (
      item.requestType === this.requestType.TYPE05 ? this.stateData?.listRefuseReason : this.stateData.listRequestType
    )?.find((o) => o.value === item.reversedReasonName)?.multiLanguage[this.translateService.currentLang];
  }
}
