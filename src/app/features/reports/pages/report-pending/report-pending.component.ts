import { Component, Injector, OnInit } from '@angular/core';
import { PaginatorModel } from '@cores/models/paginator.model';
import { cleanData, getValueDateTimeLocal } from '@cores/utils/functions';
import { BaseTableComponent } from '@shared/components';
import { cloneDeep } from 'lodash';
import { ReportPendingModel } from '../../models/report.model';
import { ReportService } from '../../services/report.service';

@Component({
  selector: 'app-report-pending',
  templateUrl: './report-pending.component.html',
  styleUrls: ['./report-pending.component.scss'],
})
export class ReportPendingComponent extends BaseTableComponent<ReportPendingModel> implements OnInit {
  override params: ReportPendingModel = {
    applicationNum: null,
    requestNumber: null,
    policyNum: null,
    fromDate: null,
    toDate: null,
  };

  constructor(inject: Injector, private service: ReportService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.getState();
    this.fileNameExcel = 'bao-cao-pending.xlsx';
  }

  override exportExcel(isBase64?: boolean, isType: string = '') {
    if (this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    this.service.exportExcel(this.fileNameExcel, cleanData(this.prevParams), isBase64, isType).subscribe({
      next: () => {
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  override getState() {
    this.serviceBase.getState().subscribe({
      next: (state) => {
        this.propData = cloneDeep(state);
        this.stateData = cloneDeep(state);
        this.searchPending();
        this.mapState();
      },
    });
  }

  onReset() {
    this.params = {
      applicationNum: null,
      requestNumber: null,
      policyNum: null,
    };
    this.searchPending();
  }

  override pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.searchPending();
  }

  checkPendingOther(item: string) {
    return ['004', '011', '910'].includes(item);
  }

  searchPending(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const params = this.mapDataSearch();
    this.service.search(params, true).subscribe({
      next: (data) => {
        this.dataTable = data;
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.messageService?.error(err.error.message);
        this.loadingService.complete();
      },
    });
  }

  override mapDataSearch(): ReportPendingModel {
    return {
      ...this.params,
      fromDate: getValueDateTimeLocal(this.params.fromDate),
      toDate: getValueDateTimeLocal(this.params.toDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }
}
