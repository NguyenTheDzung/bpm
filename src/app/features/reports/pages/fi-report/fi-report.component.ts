import { Component, Injector, OnInit } from '@angular/core';
import { BaseTableComponent } from '@shared/components';
import { FileUpload } from 'primeng/fileupload';
import { MAX_FILE_SIZE } from '@cores/utils/constants';
import { FileRefund } from 'src/app/features/create-requests/models/refund.model';
import { typeFileRefund } from 'src/app/features/create-requests/utils/constants';
import { FIReportModel, StateRequest } from 'src/app/features/reports/models/fi-report.model';
import { FIReportService } from '../../services/fi-report.service';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';

@Component({
  selector: 'app-fi-report',
  templateUrl: './fi-report.component.html',
  styleUrls: ['./fi-report.component.scss'],
})
export class FIReportComponent extends BaseTableComponent<FIReportModel> implements OnInit {
  base64Content: any;
  isSearch = true;
  maxFileSize: number = 0;
  files: FileRefund[] = typeFileRefund;
  file: any;
  fileName: any;
  today = new Date();
  override stateData: StateRequest = {};

  paymentType = [
    {
      label: 'CLAIMS',
      value: '01',
    },
    {
      label: 'REFUNDS',
      value: '02',
    },
  ];

  listStatus = [
    {
      label: 'SUCCESS',
      value: '1',
    },
    {
      label: 'FAIL',
      value: '2',
    },
  ];

  override params: FIReportModel = {
    paymentType: null,
    status: null,
    fromDate: null,
    toDate: null,
    policyNumber: null,
    bpNumber: null,
    paymentNumber: null,
  };

  constructor(inject: Injector, private fiReportService: FIReportService) {
    super(inject, fiReportService);
  }

  ngOnInit(): void {
    this.getState();
    this.serviceBase.getState().subscribe({
      next: (state) => {
        this.stateData.statusDroplist = state.listStatus || [];
      },
    });
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params = this.mapDataSearch();
    this.serviceBase.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (firstPage && this.dataTable.content.length === 0) {
          this.messageService.error('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.messageService?.error(err.error.message);
        this.loadingService.complete();
      },
    });
  }

  onChangeValueDate() {
    const fromDate = this.params.fromDate ? new Date(this.params.fromDate).valueOf() : 0;
    const toDate = this.params.toDate ? new Date(this.params.toDate).valueOf() : 0;
    if (fromDate > 0 && toDate > 0 && fromDate > toDate) {
      this.isSearch = false;
      this.messageService?.warn('request.requestList.checkDateNotFuture');
    } else {
      this.isSearch = true;
    }
  }

  uploadHandler(event: FileUpload) {
    const formData = new FormData();
    this.maxFileSize = event?.files[0].size;
    if (this.maxFileSize > MAX_FILE_SIZE) {
      this.messageService.warn(
        `File upload ${event?.files[0].name} ` + this.translateService.instant('MESSAGE.MAX_FILE_SIZE_UPLOAD')
      );
    }

    this.loadingService.start();
    this.file = event?.files[0];
    this.fileName = event?.files[0].name;
    formData.append('file', this.file);
    this.fiReportService.uploadFile(formData).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
        this.file = [];
        this.search(true);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  downloadTemplate() {
    this.loadingService.start();
    this.fiReportService.downloadTemplate().subscribe({
      next: (res) => {
        this.base64Content = res.data;
        this.loadingService.complete();
        this.onClickDownload();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  saveAsXlsxFile(base64String: string, fileName: string) {
    var source = `data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${base64String}`;
    var a = document.createElement('a');
    a.href = source;
    a.download = `${fileName}.xlsx`;
    a.click();
  }

  onClickDownload() {
    let base64String = this.base64Content;
    this.saveAsXlsxFile(base64String, 'fi_template');
  }

  override mapDataSearch(): FIReportModel {
    if (this.params.paymentNumber == '') {
      this.params.paymentNumber = null;
    }
    if (this.params.policyNumber == '') {
      this.params.policyNumber = null;
    }
    if (this.params.bpNumber == '') {
      this.params.bpNumber = null;
    }
    return {
      ...this.params,
      fromDate: getValueDateTimeLocal(this.params.fromDate),
      toDate: getValueDateTimeLocal(this.params.toDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }
}
