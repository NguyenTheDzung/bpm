import { Component, Injector, OnInit } from '@angular/core';
import { cleanData, createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import { BaseTableComponent } from '@shared/components';
import { ReportPaymentLotReverseModel } from '../../models/report.model';
import { ReverseService } from '../../services/reverse.service';

@Component({
  selector: 'app-report-payment-lot-reverse',
  templateUrl: './report-payment-lot-reverse.component.html',
  styleUrls: ['./report-payment-lot-reverse.component.scss'],
})
export class ReportPaymentLotReverseComponent
  extends BaseTableComponent<ReportPaymentLotReverseModel>
  implements OnInit
{
  minDate: any;
  maxDate: any;
  typeData: any;
  override params: ReportPaymentLotReverseModel = {
    id: 0,
    fromDate: '',
    toDate: '',
    reportType: '',
    usageText: '',
    transactionID: '',
  };

  constructor(inject: Injector, private service: ReverseService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.service.getRequestTypePaymentLotConfig().subscribe({
      next: (res) => {
        this.typeData = res;
      },
      error: () => {},
    });
    this.getState();
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const params = {
      ...this.params,
      fromDate: getValueDateTimeLocal(this.params.fromDate),
      toDate: getValueDateTimeLocal(this.params.toDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };

    this.service.paymentLotReverseAll(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  onReset() {
    this.params = {
      id: 0,
      fromDate: '',
      toDate: '',
      reportType: '',
      usageText: '',
      transactionID: '',
    };
    this.search();
  }

  validatorFromDate(event: any) {
    this.params.fromDate = event;
    this.minDate = new Date(this.params.fromDate!);
  }

  validatorToDate(event: any) {
    this.params.toDate = event;
    this.maxDate = new Date(this.params.toDate || new Date());
  }

  exportExcelPaymentLot(isBase64?: boolean) {
    if (this.loadingService.loading || this.dataTable.totalElements === 0) {
      return;
    }
    this.loadingService.start();
    this.service.exportExcel(this.fileNameExcel, cleanData(this.prevParams) || {}, isBase64).subscribe({
      next: () => {
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  doDetail(item: any) {
    let path: string;
    path = 'detail';
    this.router
      .navigate([this.router.url, path, encodeURIComponent(item.lotID), item.item], {
        state: item,
      })
      .then();
  }
}
