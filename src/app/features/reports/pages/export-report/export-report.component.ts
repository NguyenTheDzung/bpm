import { Component, Injector } from "@angular/core";
import { BaseComponent } from "@shared/components";
import { ExportReportService } from "@reports/services/export-report.service";

@Component({
  selector: 'app-export-report',
  templateUrl: 'export-report.component.html',
  styleUrls: [ 'export-report.component.scss' ],
  providers: [
    ExportReportService
  ]
})
export class ExportReportComponent extends BaseComponent {

  constructor( injector: Injector) {
    super(injector);
  }


}
