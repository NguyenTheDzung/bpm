import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent, DiagramBpmnComponent } from '@shared/components';
import { IFormProcessSearch, ProcessSearch } from '@reports/models/report.model';
import { REQUEST_TYPE } from '@cores/utils/constants';
import { CommonModel } from '@common-category/models/common-category.model';
import { Validators } from '@angular/forms';
import { RequestService } from '@requests/services/request.service';
import { getValueDateTimeLocal } from '@cores/utils/functions';

@Component({
  selector: 'app-report-process',
  templateUrl: './report-process.component.html',
  styleUrls: ['./report-process.component.scss'],
})
export class ReportProcessComponent extends BaseComponent implements OnInit {
  params = this.fb.group({
    fromDate: [null, Validators.required],
    toDate: [null, Validators.required],
    typeCode: [null, Validators.required],
    version: [null, Validators.required],
  });
  listRequestType: CommonModel[] = [];
  listVersions: CommonModel[] = [];

  @ViewChild(DiagramBpmnComponent) bpmn!: DiagramBpmnComponent;

  constructor(inject: Injector, private requestService: RequestService) {
    super(inject);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.commonService.getCommonCategory(REQUEST_TYPE).subscribe({
      next: (data) => {
        this.listRequestType = data;
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
    this.controlsParam.typeCode.valueChanges.subscribe(() => {
      this.getVersionProcess();
    });
    this.controlsParam.fromDate.valueChanges.subscribe(() => {
      this.getVersionProcess();
    });
    this.controlsParam.toDate.valueChanges.subscribe(() => {
      this.getVersionProcess();
    });
  }

  get controlsParam() {
    return <IFormProcessSearch>this.params.controls;
  }

  search() {
    if (this.loadingService.loading || this.params.invalid) {
      this.params.markAllAsTouched();
      return;
    }
    const dataSearch: ProcessSearch = this.params.getRawValue();
    dataSearch.fromDate = getValueDateTimeLocal(dataSearch.fromDate);
    dataSearch.toDate = getValueDateTimeLocal(dataSearch.toDate);
    this.bpmn?.getFileBpmnByProcess(dataSearch);
  }

  getVersionProcess() {
    const dataSearch: ProcessSearch = this.params.getRawValue();
    if (!dataSearch.typeCode || !dataSearch.toDate || !dataSearch.fromDate) {
      return;
    }
    dataSearch.fromDate = getValueDateTimeLocal(dataSearch.fromDate);
    dataSearch.toDate = getValueDateTimeLocal(dataSearch.toDate);
    this.requestService.getVersionsBpmn(dataSearch).subscribe({
      next: (data) => {
        this.listVersions = data?.map((item: number) => {
          return { value: item };
        });
        if (!this.listVersions.find((item) => item.value === this.controlsParam.version.value)) {
          this.controlsParam.version.setValue(null);
        }
      },
      error: () => {
        this.listVersions = [];
        this.controlsParam.version.setValue(null);
      },
    });
  }
}
