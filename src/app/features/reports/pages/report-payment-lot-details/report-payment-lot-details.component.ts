import { Component, Injector, OnInit } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { ReverseViewPaymentLot } from '../../models/reverse.model';
import { ReverseService } from '../../services/reverse.service';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';

@Component({
  selector: 'app-report-payment-lot-details',
  templateUrl: './report-payment-lot-details.component.html',
  styleUrls: ['./report-payment-lot-details.component.scss'],
})
export class ReportPaymentLotDetailsComponent extends BaseActionComponent implements OnInit {
  isEdit: boolean = true;
  dataDetail!: ReverseViewPaymentLot;
  sttAfterReverse: any;
  listUser: any;

  constructor(injector: Injector, private service: ReverseService) {
    super(injector, service);
    this.dataDetail = this.router.getCurrentNavigation()?.extras.state as ReverseViewPaymentLot;
  }

  override form = this.fb.group({
    id: null,
    item: null,
    lotID: null,
    newPaymentLotItem: null,
    newUsageText: null,
    note: null,
    paymentAmount: null,
    postingDate: null,
    reversedReason: null,
    transactionDate: null,
    transactionID: null,
    usagesText: null,
    reversedDate: null,
    paymentByUser: null,
    paymentRequestDate: null,
    paymentRequestNumber: null,
    statusAfterReversed: null,
    createNewPaymentLotDate: null,
  });

  ngOnInit(): void {
    const lotID = this.route.snapshot.paramMap.get('lotID');
    const item = this.route.snapshot.paramMap.get('item');
    if (lotID && item) {
      this.loadingService.start();
      this.service.getRequestStatusAfterReversedConfig().subscribe({
        next: (res) => {
          this.sttAfterReverse = res;
        },
        error: () => {},
      });
      this.service.getPic().subscribe({
        next: (res) => {
          this.listUser = res;
        },
      });
      this.service.getDetailPaymentLot(lotID, item).subscribe({
        next: (res: any) => {
          if (res.data) {
            this.dataDetail = res.data;
          }
          this.form.patchValue(this.dataDetail);
        },
        complete: () => {
          this.loadingService.complete();
        },
      });
    }
  }

  changEdit() {
    this.isEdit = false;
  }

  desTroyEdit() {
    this.router.navigate(['bpm/report/paymentLot-Reverse']);
  }

  saveEdit() {
    if (this.dataDetail.lotID) {
      this.dataDetail.paymentRequestDate;
      this.loadingService.start();
      const data = this.form.getRawValue();
      data.paymentRequestDate = getValueDateTimeLocal(data.paymentRequestDate);
      data.createNewPaymentLotDate = getValueDateTimeLocal(data.createNewPaymentLotDate);
      this.service.editReversePaymentLot(data).subscribe({
        next: () => {
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        },
        error: (err: any) => {
          this.form.patchValue(this.dataDetail);
          this.messageService.error(createErrorMessage(err));
        },
      });
      this.loadingService.complete();
    }
    this.isEdit = true;
  }
}
