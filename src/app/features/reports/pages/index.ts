import { FreelookCancellationReportComponent } from './freelook-cancellation-report/freelook-cancellation-report.component';
import { ReportPendingComponent } from './report-pending/report-pending.component';
import { FIReportComponent } from './fi-report/fi-report.component';
import { ReportPaymentLotReverseComponent } from './report-payment-lot-reverse/report-payment-lot-reverse.component';
import { ReportPaymentLotDetailsComponent } from './report-payment-lot-details/report-payment-lot-details.component';
import { ReportProcessComponent } from '@reports/pages/report-process/report-process.component';
import { ExportReportComponent } from "@reports/pages/export-report/export-report.component";
import { ReportTatComponent } from "@reports/components/report-tat/report-tat.component";

export const pages = [
  FreelookCancellationReportComponent,
  ReportPendingComponent,
  FIReportComponent,
  ReportPaymentLotReverseComponent,
  ReportPaymentLotDetailsComponent,
  ReportProcessComponent,
  ExportReportComponent
];

export * from './freelook-cancellation-report/freelook-cancellation-report.component';
export * from './report-pending/report-pending.component';
export * from './fi-report/fi-report.component';
export * from './report-payment-lot-reverse/report-payment-lot-reverse.component';
export * from './report-payment-lot-details/report-payment-lot-details.component';
export * from './report-process/report-process.component';
export * from './export-report/export-report.component';
