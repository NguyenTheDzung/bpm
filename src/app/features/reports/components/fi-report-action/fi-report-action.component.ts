import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ScreenType } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { FIReportService } from '../../services/fi-report.service';

@Component({
  selector: 'app-fi-report-action',
  templateUrl: './fi-report-action.component.html',
  styleUrls: ['./fi-report-action.component.scss'],
})
export class FIReportActionComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    code: ['', Validators.required],
    name: ['', Validators.required],
    subject: ['', Validators.required],
    description: [''],
    body: ['', Validators.required],
    toRoles: [[], Validators.required],
    ccRoles: [[], Validators.required],
    bccRoles: [[], Validators.required],
  });

  constructor(inject: Injector, service: FIReportService) {
    super(inject, service);
  }

  ngOnInit(): void {
    if (this.screenType !== ScreenType.Create) {
      this.form.get('code')?.disable();
      this.form.patchValue(this.data);
    }
  }
}
