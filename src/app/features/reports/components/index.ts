import { FIReportActionComponent } from './fi-report-action/fi-report-action.component';
import { ReportTatComponent } from "@reports/components/report-tat/report-tat.component";
import {
  ReportReasonRejectPolicyComponent
} from "@reports/components/report-reason-reject-policy/report-reason-reject-policy.component";

export const components = [FIReportActionComponent, ReportTatComponent, ReportReasonRejectPolicyComponent];
export * from './fi-report-action/fi-report-action.component';
