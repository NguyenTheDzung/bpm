import {
  AfterViewInit,
  Component,
  EventEmitter,
  Injector,
  OnInit,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import { BaseComponent } from '@shared/components';
import { NgModel } from '@angular/forms';
import * as moment from 'moment/moment';
import { HTML5_FMT } from 'moment/moment';
import { createErrorMessage, exportFile } from '@cores/utils/functions';
import { RequestExportReasonRejectPolicy } from '../../models/export-report.interfaces';
import { MINE_TYPE } from '@cores/utils/mine-type';
import { IDropdownItemWithTranslate } from '@detail-request/shared/models/dropdown-with-translate.model';
import { RequestType } from "@create-requests/utils/constants";
import { SelectItem } from "primeng/api/selectitem";
import { ExportReportService } from "@reports/services/export-report.service";
import { TranslateService } from "@ngx-translate/core";

@Component( {
  selector: 'app-report-reason-reject-policy',
  templateUrl: './report-reason-reject-policy.component.html',
} )
export class ReportReasonRejectPolicyComponent
  extends BaseComponent
  implements OnInit, AfterViewInit {
  @ViewChildren( NgModel ) ngModels?: QueryList<NgModel>;

  /**
   * Chỉ có requestType 01 -> 05 là được export
   * @private
   * @readonly
   */
  readonly #RequestTypeAllowToExport = [
    RequestType.TYPE01,
    RequestType.TYPE02,
    RequestType.TYPE03,
    RequestType.TYPE04,
    RequestType.TYPE05,
  ]

  dateSelected?: Date[];

  options: IDropdownItemWithTranslate<string>[] = [];

  selectedType: string[] = []

  @Output() onClose = new EventEmitter<any>();
  disableSelectOpts: boolean = false;
  maxSelectedLabel?: number


  constructor( injector: Injector, private exportReportService: ExportReportService ) {
    super( injector );
  }

  ngOnInit() {
    this.translateService.onLangChange.subscribe( () => {
        this.options = this.initOptions()
        this.maxSelectedLabel = this.options.length - 1
      }
    )
  }

  ngAfterViewInit() {
    const options  = this.initOptions();
    this.maxSelectedLabel = options.length -1
    setTimeout(
      () => {
        this.options = options
        this.selectedType = this.options.map(
          item => item.value
        )
      }
    )
    this.ref.detectChanges()
  }

  initOptions() {
   return this.#RequestTypeAllowToExport.map(
      ( requestType ) => {
        return <SelectItem<string>>{
          label: requestType,
          value: requestType
        }
      }
    )
  }



  convertDate( dates: Date[], autoConvertEndDate = true ) {
    const startDateConverted = moment( dates[ 0 ] );
    let endDateConverted;

    if ( dates[ 1 ] ) {
      endDateConverted = moment( dates[ 1 ] );
    } else {
      autoConvertEndDate && ( endDateConverted = moment( dates[ 0 ] ) );
    }
    return [
      startDateConverted.format( HTML5_FMT.DATETIME_LOCAL ),
      endDateConverted?.format( HTML5_FMT.DATETIME_LOCAL ) ?? undefined,
    ];
  }

  formatDateString( date?: Date ) {
    if ( !date ) return undefined;
    return moment( date ).format( 'DD-MM-YYYY' );
  }

  onExport() {
    let valid = true;
    this.ngModels?.forEach( ( model ) => {
      model.control.markAllAsTouched();
      if ( model.invalid ) {
        valid = false;
      }
    } );
    if ( !valid ) return;
    this.loadingService.start()
    let request: RequestExportReasonRejectPolicy = <any>{};

    if ( this.dateSelected?.[ 0 ] ) {
      const [ startDate, endDate ] = this.convertDate( this.dateSelected, true );
      request.fromDate = startDate!
      request.toDate = endDate!
    }
    request.requestType = this.selectedType

    this.exportReportService.exportReasonRejectPolicy( request ).subscribe( {
      next: ( response ) => {
        const fileName = `bao_cao_ly_do_huy_hop_dong_${ moment().format( 'DD_MM_YYYYTHHMM' ) }` +
          '_' + ( this.formatDateString( this.dateSelected?.[ 0 ] ) ?? '' ) +
          '_' + ( this.formatDateString( this.dateSelected?.[ 1 ] ) ?? '' )

        exportFile(
          fileName,
          response.data[ 'data' ],
          MINE_TYPE[ 'xlsx' ]
        );
        this.loadingService.complete()
      },
      error: (error) => {
        this.loadingService.complete()
        this.messageService.error(createErrorMessage(error))
      }
    } );
  }

  genLabel(value: string[], options: SelectItem[], translateService: TranslateService){
    if(!value || !value.length) return  "empty"
    if(  options.length == value.length){
      return translateService.instant('month.all')
    }
    return value.map(
      value => {
        return translateService.instant('requestType.' + value)
      }
    ).join(", ")
  }

}
