import { Component, Injector, QueryList, ViewChildren } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { Roles } from '@cores/utils/constants';
import { NgModel } from '@angular/forms';
import * as moment from 'moment/moment';
import { HTML5_FMT } from 'moment/moment';
import { createErrorMessage, exportFile } from '@cores/utils/functions';
import { MINE_TYPE } from '@cores/utils/mine-type';
import { IDropdownItemWithTranslate } from '@detail-request/shared/models/dropdown-with-translate.model';
import { ExportReportService } from "@reports/services/export-report.service";
import { DASHBOARD_TYPE } from "../../../dashboard/utils/enum";
import { RequestReportTAT } from "@reports/models/export-report.interfaces";

export const ALL_DEPARTMENT = 'ALL'

@Component( {
  selector: 'app-report-tat',
  templateUrl: './report-tat.component.html',
} )
export class ReportTatComponent extends BaseComponent {
  @ViewChildren( NgModel ) ngModels?: QueryList<NgModel>;

  dateSelected?: Date[];

  options: IDropdownItemWithTranslate<DASHBOARD_TYPE | typeof ALL_DEPARTMENT>[] = [
    {
      value: ALL_DEPARTMENT,
      multiLanguage: {
        vi: 'Tất cả phòng ban',
        en: 'All department',
      },
    },
    { value: DASHBOARD_TYPE.NBU, label: DASHBOARD_TYPE.NBU },
    { value: DASHBOARD_TYPE.COL, label: DASHBOARD_TYPE.COL },
  ];

  selectedType!: DASHBOARD_TYPE | typeof ALL_DEPARTMENT;

  disableSelectOpts: boolean = false;

  constructor( injector: Injector, private exportReportService: ExportReportService ) {
    super( injector );
    this.initOptions();
    this.initType();
  }

  initOptions() {
    this.validateOptsDisplay();
    if ( this.options.every( ( opt ) => opt.disabled ) ) throw new Error( 'User dont have roles to view this dialogs' );
  }

  initType() {
    this.selectedType = this.options.find( ( opt ) => !opt.disabled )?.value!;
  }

  validateOptsDisplay() {
    this.options.find( ( value ) => {
      value.disabled = true;
    } );
    this.disableSelectOpts = true;

    if ( this.authService.getUserRoles().find( ( value ) => [ Roles.OP_DIRECTOR, Roles.VIEW_DASHBOARD ].includes( value ) ) ) {
      this.options.forEach( ( value ) => {
        value.disabled = false;
      } );
      this.disableSelectOpts = false;
      return;
    }

    if ( this.authService.getUserRoles().find( ( value ) => Roles.OP_NBU_ADMIN == value ) ) {
      this.options[ 1 ].disabled = false;
    }
    if ( this.authService.getUserRoles().find( ( value ) => Roles.OP_COL_ADMIN == value ) ) {
      this.options[ 2 ].disabled = false;
    }

    if ( !this.options[ 1 ].disabled && !this.options[ 2 ].disabled ) {
      this.disableSelectOpts = false;
    }

  }

  convertDate( dates: Date[], autoConvertEndDate = true ) {
    const startDateConverted = moment( dates[ 0 ] );
    let endDateConverted;

    if ( dates[ 1 ] ) {
      endDateConverted = moment( dates[ 1 ] );
    } else {
      autoConvertEndDate && ( endDateConverted = moment( dates[ 0 ] ).endOf( 'day' ) );
    }
    return [
      startDateConverted.format( HTML5_FMT.DATETIME_LOCAL ),
      endDateConverted?.format( HTML5_FMT.DATETIME_LOCAL ) ?? undefined,
    ];
  }

  formatDateString( date?: Date ) {
    if ( !date ) return undefined;
    return moment( date ).format( 'DD-MM-YYYY' );
  }

  onExport() {
    let valid = true;
    this.ngModels?.forEach( ( model ) => {
      model.control.markAllAsTouched();
      if ( model.invalid ) valid = false;
    } );
    if ( !valid ) return;
    this.loadingService.start()
    let request: RequestReportTAT = {};
    if ( this.dateSelected?.[ 0 ] ) {
      const [startDate, endDate] = this.convertDate(this.dateSelected, true);
      request = { startDate, endDate };
    }

    if ( this.selectedType == ALL_DEPARTMENT ) {
      request.departments = [ DASHBOARD_TYPE.COL, DASHBOARD_TYPE.NBU ];
    } else {
      request.departments = [ this.selectedType! ];
    }

    this.exportReportService.exportTATReport( request )
      .subscribe( {
        next: ( response ) => {
          exportFile(
            `report-tat_${ request.departments ?? 'all-departments' }_${
              this.formatDateString( this.dateSelected?.[ 0 ] ) ?? ''
            }${ '_' + this.formatDateString( this.dateSelected?.[ 1 ] ) ?? '' }`,
            response.data,
            MINE_TYPE[ 'xlsx' ]
          );
          this.loadingService.complete()
        },
        error: ( error ) => {
          this.loadingService.complete()
          this.messageService.error( createErrorMessage( error ) )
        }
      } );
  }
}
