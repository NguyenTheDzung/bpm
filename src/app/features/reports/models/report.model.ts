import {Nullable} from '@cores/models/ts-helpers';
import {ExtractFormControl} from '@cores/models/form.model';

export interface ReportModel {
  pics?: string[] | null;
  selectedResolutions?: string | null;
  mainProductName?: string | null;
  policyNum?: string | null;
  appNum?: string | null;
  reversedFromDate: string | Date | null;
  reversedToDate: string | Date | null;
  code?: string | null;
  value?: string | null;
  name?: string | null;
  requestType?: string | null;
  reversedReasonName?: string;
}

export interface ReportPendingModel {
  applicationNum?: string | null;
  requestNumber?: string | null;
  policyNum?: string | null;
  fromDate?: string | Date | null;
  toDate?: string | Date | null;
  page?: number;
  size?: number;
}

export interface ReportPaymentLotReverseModel {
  id: string | number | boolean;
  fromDate?: string | null;
  toDate?: string | null;
  reportType?: string | number | boolean;
  transactionID?: string | null;
  usageText?: string | null;
}

export interface ProcessSearch {
  fromDate: Nullable<string>;
  toDate: Nullable<string>;
  typeCode: Nullable<string>;
  version: Nullable<string>;
}


export interface IFormProcessSearch extends ExtractFormControl<ProcessSearch> {
}
