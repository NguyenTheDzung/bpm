

export type RequestReportTAT = Partial<{
  startDate: string,
  endDate: string,
  departments: string[]
}>


export type RequestExportReasonRejectPolicy = {
  requestType: string[],
  fromDate: string,
  toDate: string
}
