import { CommonModel } from '@common-category/models/common-category.model';

export interface FIReportModel {
  id?: string | number;
  accountHolder?: string;
  applicationNumber?: string;
  bankAccount?: string;
  bankName?: string;
  bankNumber?: string;
  bpName?: string;
  bpNumber?: string | null;
  description?: string;
  netValue?: number | string;
  note?: number;
  payAmount?: number | string | null;
  paymentNumber?: number | string | null;
  requestId?: number | string | null;
  paymentType?: string | null;
  policyNumber?: string | null;
  reason?: string | null;
  page?: number;
  size?: number;
  status?: string | null;
  fromDate?: string | Date | null;
  toDate?: string | Date | null;
  createdDate?: string | Date | null;
}

export interface StateRequest {
  distributionChannelDroplist?: CommonModel[];
  requestTypeDroplist?: CommonModel[];
  picDroplist?: CommonModel[];
  statusDroplist?: CommonModel[];
  listRequestType?: CommonModel[];
  listSubmitChannel?: CommonModel[];
  listPic?: CommonModel[];
  listGroup?: CommonModel[];
  resolutionDropList?: CommonModel[];
  listTitle?: CommonModel[];
  distributionChannel?: CommonModel[];
  listDecision?: CommonModel[];
  listReason?: CommonModel[];
  listSubReasonApprove?: CommonModel[];
  listBank?: CommonModel[];
  listPendingReason?: CommonModel[];
  listRejectReason?: CommonModel[];
  listAdditionalAttachment?: CommonModel[];
  resolutionRefund?: CommonModel[];
  requestTypeRefund?: CommonModel[];
  listRefuseReason?: CommonModel[];
}
