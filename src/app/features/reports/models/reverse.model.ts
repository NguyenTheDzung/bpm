export interface ReverseModelResponse {
  code: number
  creDtTm: string
  data: ReverseViewPaymentLot
  frSystm: string
  message: string
  msgId: string
  path: string
  status: number
  timestamp: string
}

export interface ReverseViewPaymentLot {
  accountNumber?: string,
  id?: number | string
  item?: string
  lotID?: string
  newPaymentLotItem?: string
  newUsageText?: string
  note?: string
  paymentAmount?: string
  postingDate?: string
  reversedReason?: string
  transactionDate?: string
  transactionID?: string
  usagesText?: string
  reversedDate?: string
  paymentByUser?: string
  paymentRequestDate?: string
  paymentRequestNumber?: string
  statusAfterReversed?: string
  createNewPaymentLotDate?: string
}
