import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {ReportsRoutingModule} from './reports-routing.module';
import {SharedModule} from '@shared/shared.module';
import {pages} from './pages';
import {components} from './components';

@NgModule({
  declarations: [...pages, ...components],
  imports: [SharedModule, ReportsRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ReportsModule {}
