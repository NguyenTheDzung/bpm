import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { CommonCategoryService } from '@cores/services/common-category.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { STATUS } from '@cores/utils/constants';
import { environment } from '@env';
import { forkJoin, map, Observable } from 'rxjs';
import { PermissionService } from '@permission/services/permission.service';

@Injectable({
  providedIn: 'root',
})
export class FIReportService extends BaseService {
  constructor(http: HttpClient, private service: PermissionService, private commonService: CommonCategoryService) {
    super(http, `${environment.task_url}/fi-payment`);
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<DataTable>(`${environment.payment_url}/payment/fi-payment`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  override getState(): Observable<any> {
    return forkJoin({
      listStatus: this.commonService.getCommonCategory(STATUS),
    }).pipe(
      map(
        (data) =>
          (this.state = {
            ...this.state,
            ...data,
          })
      )
    );
  }

  override update(data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/${data.code}`, data);
  }

  uploadFile(file: any): Observable<any> {
    return this.http.post(`${environment.task_url}/fi-payment`, file);
  }

  downloadTemplate(): Observable<any> {
    return this.http.get(`${environment.task_url}/fi-payment/template`);
  }
}
