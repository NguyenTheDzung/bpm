import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { dataURItoBlob, mapDataTable, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import * as saveAs from 'file-saver';
import { map, Observable } from 'rxjs';
import { CommonCategoryService } from '@cores/services/common-category.service';

@Injectable({
  providedIn: 'root',
})
export class ReportService extends BaseService {
  constructor(http: HttpClient, private common: CommonCategoryService) {
    super(http, `${environment.refund_url}/report`);
  }

  override search(params?: any, isPending?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    if (isPending) {
      return this.http
        .get<DataTable>(`${this.baseUrl}/pending`, {
          params: { ...newParam },
        })
        .pipe(map((data) => mapDataTable(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}/freelook`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTable(data, params)));
  }

  override exportExcel(
    fileName: string,
    params: any,
    isBase64?: boolean,
    type: string = 'freelook'
  ): Observable<boolean> {
    const responseType = isBase64 ? 'json' : 'arraybuffer';
    const option: any = { params, responseType };
    return this.http.get(`${this.baseUrl}/${type}/export`, option).pipe(
      map((res: any) => {
        if (isBase64) {
          res = dataURItoBlob(res?.data?.data);
        }
        saveAs(
          new Blob([res], {
            type: 'application/octet-stream',
          }),
          fileName
        );
        return true;
      })
    );
  }
}
