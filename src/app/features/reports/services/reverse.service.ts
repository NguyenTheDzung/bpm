import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { environment } from '@env';
import { BaseService } from '@cores/services/base.service';
import { ReverseViewPaymentLot } from '../models/reverse.model';
import { CommonCategoryService } from '@cores/services/common-category.service';
import { RequestService } from '@requests/services/request.service';
import { dataURItoBlob, mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import * as saveAs from 'file-saver';
import { DataTable } from '@cores/models/data-table.model';

@Injectable({
  providedIn: 'root',
})
export class ReverseService extends BaseService {
  constructor(http: HttpClient, private common: CommonCategoryService, private requestService: RequestService) {
    super(http, `${environment.collection_url}/paymentLot`);
  }

  getDetailPaymentLot(lotID: any, item: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/detail?lotId=${lotID}&item=${item}`);
  }

  editReversePaymentLot(param: ReverseViewPaymentLot): Observable<any> {
    return this.http.put(`${this.baseUrl}`, param);
  }

  getRequestStatusAfterReversedConfig() {
    return this.common.getCommonCategory('STATUS_AFTER_REVERSED');
  }

  getPic(params?: any) {
    return this.requestService.getPic(params);
  }

  override exportExcel(fileName: string, params: any, isBase64?: boolean): Observable<boolean> {
    const responseType = isBase64 ? 'json' : 'arraybuffer';
    const option: any = { params, responseType };
    return this.http.get(`${this.baseUrl}/export`, option).pipe(
      map((res: any) => {
        if (isBase64) {
          res = dataURItoBlob(res?.data?.data);
        }
        saveAs(
          new Blob([res], {
            type: 'application/octet-stream',
          }),
          fileName
        );
        return true;
      })
    );
  }

  paymentLotReverseAll(params: any) {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  getRequestTypePaymentLotConfig() {
    return this.common.getCommonCategory('PAYMENT_LOT_REVERSE_TYPE');
  }
}
