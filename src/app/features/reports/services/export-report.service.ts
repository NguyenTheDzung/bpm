import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BaseService } from "@cores/services/base.service";
import { environment } from "@env";
import { IDashboardResponseModel } from "../../dashboard/interfaces/dashboard-response.interface";
import { RequestExportReasonRejectPolicy, RequestReportTAT } from "@reports/models/export-report.interfaces";

@Injectable()
export class ExportReportService extends BaseService {
  constructor( Http: HttpClient ) {
    // Không sử dụng baseURL
    super( Http, '' )
  }

  /**
   * API Xuất báo cáo TAT
   */
  exportTATReport( request: RequestReportTAT ) {
    const url = `${ environment.task_url }/dashboard/tat-report/export`
    return this.http.get<IDashboardResponseModel<string>>( url, { params: request } )
  }

  /**
   * API Xuất báo cáo lý do hủy
   */
  exportReasonRejectPolicy(request: RequestExportReasonRejectPolicy) {
    const url = `${environment.refund_url}/report/revesal-reason/export`
    return this.http.get<IDashboardResponseModel<{data: string}>>(url, {params: request})
  }
}
