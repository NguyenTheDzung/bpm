import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  FIReportComponent,
  FreelookCancellationReportComponent,
  ReportPaymentLotReverseComponent,
  ReportPendingComponent,
  ReportProcessComponent,
} from './pages';
import { FunctionCode } from '@cores/utils/enums';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { ReportPaymentLotDetailsComponent } from '@reports/pages';
import { ExportReportComponent } from "@reports/pages/export-report/export-report.component";

const routes: Routes = [
  {
    path: 'process',
    component: ReportProcessComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ReportProcess,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ReportProcess}`,
    },
  },
  {
    path: 'free-look',
    component: FreelookCancellationReportComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ReportFreeLook,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ReportFreeLook}`,
    },
  },
  {
    path: 'report-pending',
    component: ReportPendingComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ReportPending,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ReportPending}`,
    },
  },
  {
    path: 'payment-fi',
    component: FIReportComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.ReportPaymentFI,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ReportPaymentFI}`,
    },
  },
  {
    path: 'paymentLot-Reverse',
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.PaymentLotReverse,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.PaymentLotReverse}`,
    },
    children: [
      {
        path: '',
        component: ReportPaymentLotReverseComponent,
      },
      {
        path: 'detail/:lotID/:item',
        component: ReportPaymentLotDetailsComponent,
      },
    ],
  },
  {
    path:'export_report',
    data: {
      functionCode: FunctionCode.ExportReport,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.ExportReport}`,
    },
    component: ExportReportComponent
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule {}
