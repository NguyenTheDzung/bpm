import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseService } from "@cores/services/base.service";
import { environment } from "@env";
import {
  ICompareReportRequest,
  IDashboardRequestModel,
  IGetUserListRequest
} from "../interfaces/dashboard-request.interface";
import {
  IDashboardCompareReportResponse,
  IDashboardDecisionChartResponse,
  IDashboardOverviewApprovedResponse,
  IDashboardOverviewResponse,
  IDashboardRequestChartResponse,
  IDashboardResponseModel,
  IDashboardSLAChartResponse,
  IDashboardUserChartResponse
} from "../interfaces/dashboard-response.interface";
import { BehaviorSubject, catchError, filter, finalize, map, ReplaySubject, scan, throwError } from "rxjs";
import { CommonCategoryService } from "@cores/services/common-category.service";
import { DashboardConstant } from "../utils/constant";
import { SelectItem } from "primeng/api/selectitem";
import { DASHBOARD_REQUEST_CODE, DASHBOARD_TYPE } from "../utils/enum";
import { NotificationMessageService } from "@cores/services/message.service";
import { IDashboardSelectConvertedValue } from "../interfaces/dashboard-select.interface";
import { createErrorMessage } from "@cores/utils/functions";

@Injectable()
export class DashboardService extends BaseService {
  /**
   * - SelectedType chứa giá trị mà dashboard đang hiển thị theo phòng nào NBU  || COL
   * - SelectedType luôn có giá trị vì đã có check quyền ở phần dashboard view
   * - Nếu không có giá trị thì không thể truy cập vào dashboard vì không xác định được user sẽ xem ở phòng ban nào
   */
  selectedType$ = new BehaviorSubject<DASHBOARD_TYPE>( <any>null );
  /**
   * Thực hiện cơ chế loading trên màn hình dashboard dành cho call nhiều API
   * -  Mỗi khi call api thì sẽ tăng biến điếm lên (1) đơn vị
   * -  Mỗi khi call Api xong thì sẽ trừ đi (1) đơn vị
   * -  Mỗi lần nhận giá trị thì sẽ so sánh biến count với 0 để thực hiện tắt loading hay là tiếp tục loading
   * @private
   */
  private _loading$ = new ReplaySubject<number>();
  /**
   * Chứa thông tin, event về các giá trị mới nhất của lựa chọn ở phần dashboardSelect
   * @private
   */
  private _changeSelection$ = new BehaviorSubject<IDashboardSelectConvertedValue>( <any>null );

  constructor(
    http: HttpClient,
    private common: CommonCategoryService,
    private notificationMessageService: NotificationMessageService
  ) {
    super( http, `${ environment.task_url }` );
  }

  set currentSelectedType( value: DASHBOARD_TYPE ) {
    this.selectedType$.next( value );
  }

  get currentSelectedType(): DASHBOARD_TYPE {
    return this.selectedType$.getValue();
  }

  loading$() {
    return this._loading$.pipe(
      scan( ( acc, value ) => acc + value, 0 ),
      map( ( value ) => value > 0 )
    );
  }

  getRequest$() {
    return this.selection$.pipe(
      map( ( convertedSelection ) => {
        const request: Omit<IDashboardRequestModel, 'code' | 'type'> = {
          isYear: convertedSelection.isYear,
          condition: {
            pics: convertedSelection.selectedUsers.length == 0 ? undefined : convertedSelection.selectedUsers,
            startDateFirst: [ convertedSelection.startDateConverted ],
            endDateFirst: [ convertedSelection.endDateConverted ],
          },
        };
        return request;
      } )
    );
  }

  set selection( value: IDashboardSelectConvertedValue ) {
    this._changeSelection$.next( value );
  }

  get selection() {
    return this._changeSelection$.getValue();
  }

  get selection$() {
    return this._changeSelection$.asObservable().pipe( filter( ( selection ) => !!selection ) );
  }

  increaseLoadingAmount() {
    this._loading$.next( 1 );
  }

  decreaseLoadingAmount() {
    this._loading$.next( -1 );
  }

  /*===============================================================================================================*/
  /*                                          CALL API STAGE                                                       */

  /*===============================================================================================================*/

  /**
   * Call api lấy thông tin hiển thị compare report
   */
  getCompareReport( request: ICompareReportRequest ) {
    this.increaseLoadingAmount();
    const url = `${ environment.task_url }/dashboard/compare`;
    return this.http.post<IDashboardResponseModel<IDashboardCompareReportResponse>>( url, request ).pipe(
      finalize( () => this.decreaseLoadingAmount() ),
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }

  /**
   * Subscribe khi thay đổi dữ liệu đổ ra là xem theo năm hay tháng
   * để cập nhật danh sách người dùng có thể chọn
   * @private
   */
  private getDashboardData<T>( request: Omit<IDashboardRequestModel, 'type'> ) {
    const body: IDashboardRequestModel & { picApprove?: string[] } = { ...request, type: this.currentSelectedType };

    if ( request.code == DASHBOARD_REQUEST_CODE.DASHBOARD_GENERAL_APPROVE ) {
      body.picApprove = body.condition.pics;
      delete body.condition.pics;
    }

    this.increaseLoadingAmount();
    let url = `${ this.baseUrl }/dashboard`;
    return this.http.post<IDashboardResponseModel<T>>( url, body ).pipe(
      finalize( () => this.decreaseLoadingAmount() ),
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }

  getOverviewInfoDashboard( request: Omit<IDashboardRequestModel, 'code' | 'type'> ) {
    return this.getDashboardData<IDashboardOverviewResponse>( {
      ...request,
      code: DASHBOARD_REQUEST_CODE.DASHBOARD_GENERAL,
    } );
  }

  getRequestChartInfoDashboard( request: Omit<IDashboardRequestModel, 'code' | 'type'> ) {
    return this.getDashboardData<IDashboardRequestChartResponse>( {
      ...request,
      code: DASHBOARD_REQUEST_CODE.DASHBOARD_TOTAL_IN_REQUEST,
    } );
  }

  getDecisionChartInfoDashboard( request: Omit<IDashboardRequestModel, 'code' | 'type'> ) {
    return this.getDashboardData<IDashboardDecisionChartResponse>( {
      ...request,
      code: DASHBOARD_REQUEST_CODE.DASHBOARD_TOTAL_IN_STATUS,
    } );
  }

  getSLAChartInfoDashboard( request: Omit<IDashboardRequestModel, 'code' | 'type'> ) {
    return this.getDashboardData<IDashboardSLAChartResponse>( {
      ...request,
      code: DASHBOARD_REQUEST_CODE.DASHBOARD_TOTAL_IN_SLA,
    } );
  }

  getUserChartInfoDashboard( request: Omit<IDashboardRequestModel, 'code' | 'type'> ) {
    return this.getDashboardData<IDashboardUserChartResponse>( {
      ...request,
      code: DASHBOARD_REQUEST_CODE.DASHBOARD_TOTAL_IN_USER,
    } );
  }

  getApprovedData( request: Omit<IDashboardRequestModel, 'code' | 'type'> ) {
    return this.getDashboardData<IDashboardOverviewApprovedResponse>( {
      ...request,
      code: DASHBOARD_REQUEST_CODE.DASHBOARD_GENERAL_APPROVE,
    } );
  }

  getYearsHaveData( dashboardType: DASHBOARD_TYPE ) {
    this.increaseLoadingAmount();
    const url = this.baseUrl + '/dashboard/year';
    return this.http.get<IDashboardResponseModel<number[]>>( url, { params: { type: dashboardType } } ).pipe(
      finalize( () => this.decreaseLoadingAmount() ),
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }

  getRequestChartColorConfig() {
    return this.common.getCommonCategory( DashboardConstant.CONFIG_REQUEST_CHART_COLOR ).pipe(
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }

  getSLAChartConfigColor() {
    return this.common.getCommonCategory( DashboardConstant.CONFIG_SLA_CHART_COLOR ).pipe(
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }

  getDecisionChartConfigColor() {
    return this.common.getCommonCategory( DashboardConstant.CONFIG_DECISION_CHART_COLOR ).pipe(
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }

  getUserList( request: IGetUserListRequest ) {
    this.increaseLoadingAmount();
    const url = environment.task_url + '/dashboard/list-active-employee-role';
    return this.http.post<IDashboardResponseModel<string[]>>( url, request ).pipe(
      finalize( () => this.decreaseLoadingAmount() ),
      map( ( response ) => {
        return response.data.map(
          ( user ) =>
            <SelectItem<string>>{
              value: user,
              label: user,
              disabled: false,
            }
        );
      } ),
      catchError( ( error ) => {
        this.notificationMessageService.error( createErrorMessage( error ) );
        return throwError( error );
      } )
    );
  }


}
