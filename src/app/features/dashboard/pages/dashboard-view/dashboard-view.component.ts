import { Component, Injector, NgZone, OnInit } from "@angular/core";
import { DashboardService } from "../../services/dashboard.service";
import { SelectItem } from "primeng/api/selectitem";
import { BaseComponent } from "@shared/components";
import { Resolution, Roles } from "@cores/utils/constants";
import { DASHBOARD_TYPE, REDIRECT_WITH_PARAMS, SLA_CHART_FIELD } from "../../utils/enum";
import { auditTime, tap } from "rxjs";
import {
  IDashboardSelectConvertedValue,
  IDropdownItemWithTranslate,
  ISelectedMonthYear,
  TRedirectWithParams
} from "../../interfaces/dashboard-select.interface";
import * as moment from "moment";
import { Moment } from "moment";
import { getMonthNames } from "../../utils/functions";
import { IGetUserListRequest } from "../../interfaces/dashboard-request.interface";
import { last } from "lodash";

@Component({
  selector: "dashboard-view",
  templateUrl: "./dashboard-view.component.html",
  styleUrls: ["./dashboard-view.component.scss"],
  providers: [DashboardService]
})
export class DashboardViewComponent extends BaseComponent implements OnInit {
  options: SelectItem<DASHBOARD_TYPE>[] = [];

  selectedDashboardType!: DASHBOARD_TYPE;

  private isNBUAdmin: boolean = false;
  private isNBUUser: boolean = false;
  private isCOLAdmin: boolean = false;
  private isCOLUser: boolean = false;
  private isAllowViewDashboard: boolean = false;
  private isDirector: boolean = false;
  private isNBUManager: boolean = false;
  private isCOLManager: boolean = false;

  showDashboard = false;

  years: SelectItem<number>[] = [];
  months: IDropdownItemWithTranslate<number>[] = [];
  users: SelectItem<string>[] = [];

  selectedUsers: string[] = [];
  selectedMonth!: number;
  selectedYear!: number;

  canViewFullCardType: boolean = false;
  isShowUsersChart: boolean = false;

  constructor(
    private dashboardService: DashboardService,
    injector: Injector,
    private ngZone: NgZone
  ) {
    super(injector);

  }

  ngOnInit() {
    this.validateUserRole()
    this.subscriptions.push(
      this.dashboardService.loading$()
        .pipe(
          tap((isLoading) => isLoading && !this.loadingService.loading && this.loadingService.start()),
          auditTime(1500),
          tap((isLoading) => !isLoading && this.loadingService.loading && this.loadingService.complete())
        )
        .subscribe()
    );
    this.options = this.getTypeDashboard();
    this.showDashboard = this.options.length > 0;
    if (!this.showDashboard) {
      console.error("Không tìm thấy role phù hợp | user không có quyền xem dashboard");
      return;
    }
    this.selectedDashboardType = this.options[0].value;
    this.onSelectedType();
  }


  getUsers(isTypeChanged: boolean = false) {
    const roles = this.getViewRoles();

    if (!roles.length) {
      const currentUser = {
        value: this.currUser.username,
        label: this.currUser.username,
        disabled: true
      };
      this.users = [currentUser];
      this.selectedUsers = [currentUser.value];
      this.emitNewSelectionRequest(this.selectedUsers);
      return;
    }

    const convertedSelection = this.convertSelectedMonthYear();
    const request: IGetUserListRequest = {
      roleName: roles,
      typeRoles: [this.selectedDashboardType],
      startDateFirst: new Date(convertedSelection.startDateConverted).toISOString(),
      endDateFirst: new Date(convertedSelection.endDateConverted).toISOString(),
      isYear: convertedSelection.isYear
    };
    this.dashboardService.getUserList(request).pipe(
      tap((users) => {
        this.users = users;
        if (!this.selectedUsers || isTypeChanged) {
          this.selectedUsers = users.map((user) => user.value);
        }
        this.emitNewSelectionRequest(this.selectedUsers);
      })
    ).subscribe();

  }

  getMonths(selectedYear: number = this.selectedYear) {
    let months: IDropdownItemWithTranslate<number>[] = [];
    Object.assign(months, this.months);

    if (!this.months.length) {
      months = this.initMonthsValue();
    }

    const currentMonth = moment().get("month");
    const currentYear = moment().get("year");

    if (selectedYear != currentYear) {
      months.forEach((month) => {
        if (month.value == null) return;
        month.disabled = this.selectedYear == currentYear;
      });
    } else {
      months.forEach((month) => {
        if (month.value == null) return;
        month.disabled = month.value > currentMonth;
      });
    }
    this.months = months;
    const month = months.find(month => month.value == this.selectedMonth);
    if (!this.selectedMonth || month?.disabled) {
      this.selectedMonth = currentMonth;
    }
  }

  getYears(yearsResponse: number[]) {
    const years = yearsResponse;
    this.years = years.map((year) => ({ label: String(year), value: year }));
    if (!years.includes(this.selectedYear)) {
      if (years.includes(moment().get("year"))) {
        this.selectedYear = moment().get("year");
      } else {
        this.selectedYear = last(years)!;
      }
    }
  }

  initMonthsValue() {
    let months: IDropdownItemWithTranslate<number>[] = [];
    let monthsLangVi = getMonthNames("vi", "MM");
    let monthsLangEn = getMonthNames("en", "MM");
    months.unshift({
      label: "",
      value: <any>null,
      disabled: false,
      multiLanguage: {
        en: "All",
        vi: "Tất cả"
      }
    });
    for (let i = 0; i < 12; i++) {
      months.push({
        value: i,
        label: "",
        disabled: false,
        multiLanguage: {
          en: monthsLangEn[i],
          vi: monthsLangVi[i]
        }
      });
    }
    return months;
  }


  checkCanViewFullCardOverView() {
    this.canViewFullCardType = !!this.authService.getUserRoles().find(
      (role) => {
        return [
          Roles.OP_NBU_ADMIN, Roles.OP_NBU_MANAGER, Roles.OP_COL_ADMIN,
          Roles.OP_COL_MANAGER, Roles.OP_DIRECTOR, Roles.VIEW_DASHBOARD
        ].includes(role);
      }
    );
  }

  checkShowUsersChart() {
    this.isShowUsersChart = !!this.authService.getUserRoles().find(
      (role) => {
        return [Roles.OP_NBU_ADMIN, Roles.OP_COL_ADMIN, Roles.OP_DIRECTOR, Roles.VIEW_DASHBOARD].includes(role);
      }
    );
  }

  onSelectedType() {
    this.checkCanViewFullCardOverView();
    this.checkShowUsersChart();
    this.dashboardService.currentSelectedType = this.selectedDashboardType;
    this.dashboardService.getYearsHaveData(this.selectedDashboardType)
      .pipe(
        tap((response) => {
          this.getYears(response.data)
        }),
        tap(() => {
          this.getMonths();
        }),
        tap(() => {
          this.getUsers(true)
        })
      )
      .subscribe();
  }

  getTypeDashboard() {
    let result: SelectItem<DASHBOARD_TYPE.COL | DASHBOARD_TYPE.NBU>[] = [];
    if (this.isAllowViewDashboard || this.isNBUUser || this.isNBUAdmin || this.isDirector || this.isNBUManager) {
      result.push({ value: DASHBOARD_TYPE.NBU, label: DASHBOARD_TYPE.NBU });
    }
    if (this.isAllowViewDashboard || this.isCOLUser || this.isCOLAdmin || this.isDirector || this.isCOLManager) {
      result.push({ value: DASHBOARD_TYPE.COL, label: DASHBOARD_TYPE.COL });
    }
    return result;
  }

  getViewRoles(): string[] {
    if (
      (this.isAllowViewDashboard || this.isNBUAdmin || this.isNBUManager || this.isDirector) &&
      this.selectedDashboardType == DASHBOARD_TYPE.NBU
    ) {
      return [Roles.OP_NBU_USER, Roles.OP_NBU_MANAGER];
    }

    if (
      (this.isAllowViewDashboard || this.isCOLAdmin || this.isCOLManager || this.isDirector) &&
      this.selectedDashboardType == DASHBOARD_TYPE.COL
    ) {
      return [Roles.OP_COL_USER, Roles.OP_COL_MANAGER];
    }

    if (this.isNBUUser || this.isCOLUser) {
      return [];
    }

    console.error("Không tìm thấy role phù hợp | user không có quyền xem dashboard");
    this.showDashboard = false;
    throw Error("Invalid roles");
  }

  validateUserRole() {
    for (let role of this.authService.getUserRoles()) {
      if(Roles.OP_NBU_ADMIN == role) this.isNBUAdmin = true

      if(Roles.OP_NBU_USER == role) this.isNBUUser = true

      if(Roles.OP_COL_ADMIN == role) this.isCOLAdmin = true

      if(Roles.OP_COL_USER == role) this.isCOLUser = true

      if(Roles.VIEW_DASHBOARD == role) this.isAllowViewDashboard = true

      if(Roles.OP_DIRECTOR == role) this.isDirector = true

      if(Roles.OP_NBU_MANAGER == role) this.isNBUManager = true

      if(Roles.OP_COL_MANAGER == role) this.isCOLManager = true

    }
  }

  convertSelectedMonthYear(): ISelectedMonthYear {
    let startConverted;
    let endConverted;
    {
      let dateStart: Moment;
      let dateEnd: Moment;
      if(this.selectedMonth == moment().get('month') && this.selectedYear == moment().get('year')) {
        dateStart = moment().startOf("month");
        dateEnd = moment().endOf("day")
      } else if (this.selectedMonth != null) {
        dateStart = moment().set({ month: this.selectedMonth, year: this.selectedYear }).startOf("month");
        dateEnd = moment().set({ month: this.selectedMonth, year: this.selectedYear }).endOf("month");
      } else {
        dateStart = moment().set({ year: this.selectedYear }).startOf("year");
        dateEnd = moment().set({ year: this.selectedYear }).endOf("year");
      }
      startConverted = dateStart.format();
      endConverted = dateEnd.format();
    }

    if (!startConverted || !endConverted) {
      throw new Error("Không tạo được localDateTime");
    }

    return {
      isYear: this.selectedMonth == null,
      startDateConverted: startConverted,
      endDateConverted: endConverted
    };
  }

  override onDestroy() {
    super.onDestroy();
  }

  onChangeSelectedYear($event: typeof this.selectedYear) {
    this.getMonths($event)
    setTimeout(
      () => {
        this.getUsers()
      }, 0)
  }

  onChangeSelectedMonth() {
    setTimeout(
      () => this.getUsers(), 0
    )
  }

  onChangeSelectedUsers($event: string[]) {
    this.emitNewSelectionRequest($event);
  }

  redirectToRequestScreenWithParams({ code, requestCodes, resolution, status }: TRedirectWithParams) {
    const redirectUrl = "/bpm/requests";
    const startDate = this.dashboardService.selection.startDateConverted;
    const endDate = this.dashboardService.selection.endDateConverted;
    let state = {};
    const pic = this.selectedUsers.length == this.users.length ? [] : this.selectedUsers;
    switch (code) {
      case SLA_CHART_FIELD.DONE:
        state = {
          requestCodes,
          pic
        };
        break;

      case SLA_CHART_FIELD.OVERTIME:
        state = {
          requestCodes,
          pic
        };
        break;

      case SLA_CHART_FIELD.OVERDUE:
        state = {
          requestCodes,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.SLA_LATE:
        state = {
          requestCodes,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.SLA_APPROVED_LATE:
        state = {
          requestCodes
        };
        break;

      case REDIRECT_WITH_PARAMS.REMAINING_REQUEST:
        state = {
          pic,
          startDate,
          endDate,
          resolution: (() => {
            switch (this.selectedDashboardType) {
              case DASHBOARD_TYPE.NBU:
                return Resolution.NBU_INPROCESS;
              case DASHBOARD_TYPE.COL:
                return Resolution.OP_COL_USER_INPROCESS;
            }
          })()
        };
        break;

      case REDIRECT_WITH_PARAMS.REMAINING_APPROVED_REQUEST:
        state = {
          requestCodes
        };
        break;

      case REDIRECT_WITH_PARAMS.SLA_DONE:
        state = {
          requestCodes,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.SLA_APPROVED_DONE:
        state = {
          requestCodes
        };
        break;

      case REDIRECT_WITH_PARAMS.DECISION_APPROVED:
        state = {
          resolution,
          requestCodes,
          startDate,
          endDate,
          status,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.DECISION_PENDING:
        state = {
          resolution,
          startDate,
          requestCodes,
          endDate,
          status,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.DECISION_CLOSE:
        state = {
          resolution,
          startDate,
          requestCodes,
          endDate,
          status,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.DECISION_REJECT:
        state = {
          resolution,
          startDate,
          endDate,
          requestCodes,
          status,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.DECISION_OVERDUE:
        state = {
          resolution,
          startDate,
          endDate,
          requestCodes,
          status,
          pic
        };
        break;

      case REDIRECT_WITH_PARAMS.LINE_DONE_REQUEST:
        state = {
          requestCodes,
          pic
        };
        break;

    }
    this.ngZone.run(() => this.router.navigate([redirectUrl], { state }));
  }

  emitNewSelectionRequest(selectedUsers: string[]) {
    const convert = this.convertSelectedMonthYear();
    if ((selectedUsers.length == this.users.length) && (selectedUsers.length != 1)) {
      selectedUsers = [];
    }
    this.dashboardService.selection = {
      isYear: convert.isYear,
      selectedUsers: selectedUsers,
      startDateConverted: convert.startDateConverted,
      endDateConverted: convert.endDateConverted
    } as IDashboardSelectConvertedValue;
  }

}
