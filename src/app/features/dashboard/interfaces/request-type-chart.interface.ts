import {IDashboardRequestModel} from "./dashboard-request.interface";
import {IDashboardListTaskGroupByTypeItem} from "./dashboard-response.interface";

export interface IRequestChartSeriesDynamicData {
  id: string,
  name: string,
  color: string,
  query: string,  // Dùng cho translate
  data: (number | undefined) [] // Nếu không có dữ liệu trong ngày thì sẽ là undefined
  total: number // Tổng dữ liệu trong một tháng hoặc một năm
}

export interface IRequestTypeChartAPIChange{
  request: Omit<IDashboardRequestModel, 'code' | 'type'>,
  response: IDashboardListTaskGroupByTypeItem[]
}

