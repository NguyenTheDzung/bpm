import {DASHBOARD_REQUEST_CODE, DASHBOARD_TYPE} from "../utils/enum";

export interface IDashboardRequestModel {
  type:  DASHBOARD_TYPE ,
  code: DASHBOARD_REQUEST_CODE,
  isYear: boolean // Sử dụng để backend biết là user đang chọn xem theo tháng hay xem theo năm
  condition: {
    startDateFirst: [string],
    endDateFirst: [string],
    pics?: string[]
  }
}

export interface ICompareReportRequest {
  //Example Date 2023-04-18T03:56:51.849Z
  startDateFirst: string,
  startDateSecond: string,
  endDateFirst: string,
  endDateSecond: string,
  type: string,
  typeP2P?: boolean
}

export interface IGetUserListRequest {
  roleName: string[] | [string, string] | [string]
  typeRoles: [DASHBOARD_TYPE],
  //Example date request :"2023-05-11T07:36:44.975Z"
  startDateFirst: string,
  //Example date request :"2023-05-11T07:36:44.975Z"
  endDateFirst: string,
  isYear: boolean
}


