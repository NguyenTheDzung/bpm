export interface ICompareReportTableData {
  name: string,
  data: {
    column1: number | string,
    column2: number | string,
    percent: number | string,
  }
  index: number
  childData: ICompareReportChildItem[],
  toggle: boolean
}

export interface ICompareReportChildItem extends Omit<ICompareReportTableData, 'childData'| 'toggle'> {}
