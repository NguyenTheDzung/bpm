import {SelectItem} from "primeng/api/selectitem";
import {OVERVIEW_STATUS_OPT, REDIRECT_WITH_PARAMS, SLA_CHART_FIELD} from "../utils/enum";


export interface IDashboardSelectionModel {
  selectedMonth: number | null,
  selectedYear: number,
  selectedUser: string[]
}

export interface IDashboardSelectConvertedValue {
  startDateConverted: string,
  endDateConverted: string,
  selectedUsers: string[],
  isYear: boolean,
  // isAdminView: boolean
}

export interface IDropdownItemWithTranslate<T> extends SelectItem<T> {
  multiLanguage?: {
    [p: string]: string
  }
}

export interface ISelectedMonthYear  {
  startDateConverted: string,
  endDateConverted: string,
  isYear: boolean
}

export type TOverViewItemDataDashboard = {
  value: number,
  percentage: number,
  status: OVERVIEW_STATUS_OPT,
  requestCodes?: string[]
}

export type TOverViewDoneRequest = { value: number, pending: number, requestCodes?: string[] }
export type TOverViewRemainingApprovedRequest =  {value: number, requestCodes?: string[]}
export type TRedirectWithParams = {
  code: REDIRECT_WITH_PARAMS | SLA_CHART_FIELD,
  requestCodes?: string[],
  resolution?: string | string[],
  status?: string
}


