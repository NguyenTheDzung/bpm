export interface IEchartsSelectChangedEvent {
  escapeConnect: any
  fromAction: TSelectChangeFormAction,
  fromActionPayload: {
    dataIndexInside: number,
    dataType?: string ,
    isFromClick: boolean,
    seriesIndex: number,
    type: TSelectChangeFormAction
  }
  isFromClick: true
  selected: ({ dataIndex: number[], seriesIndex: number })[]
  type: "selectchanged"
}

export type TSelectChangeFormAction  = 'select' | 'toggleSelect' | 'unselect'
