import { IResponseModel } from "@cores/models/response.model";

export interface IDashboardResponseModel<T> extends IResponseModel<T> {}

export interface IDashboardOverviewResponse {
  taskViewDashBoardResponseFirst: {
    totalSlaOnetimeList: string[],
    totalSlaLastedList: string[],
    doneRequestPendingList: string[]

    totalRequest: number,
    totalRequestLast: number,
    totalPending: number,
    totalPendingLast: number,
    totalSlaLasted: number,
    totalSlaLastedLast: number,
    totalSlaOnetime: number,
    totalSlaOnetimeLast: number,
    doneRequest: number
    doneRequestPending: number,
  }
}

export interface IDashboardRequestChartResponse {
  listTaskGroupByType: IDashboardListTaskGroupByTypeItem[]
}

export interface IDashboardListTaskGroupByTypeItem {
  requestType: string,
  requestTypeCode: string,
  createdDate: string,
  total: number
}

export interface IDashboardDecisionChartResponse {
  taskViewDashBoardResponseEnd: {
    approve: number,
    reject: number,
    pending: number,
    close: number,
    overdue: number,

    approveList: string[],
    rejectList: string[],
    overdueList: string[],
    pendingList: string[],
    closeList: string[]
  }
}

export interface IDashboardOverviewApprovedResponse {
  taskViewDashBoardResponseFirst: {
    totalApprovalRequest: number,
    totalRemainingApprovalRequest: number,
    totalRemainingApprovalRequestList: string[]
    totalSlaLastedApprove: number,
    totalSlaLastedApproveList: string[],
    totalSlaLastedApproveLast: number,
    totalSlaApprovedDone: number,
    totalSlaApprovedDoneList: string[],
    totalSlaApprovedDoneLast: number,
    totalTAT: number
  }
}

export interface IDashboardSLAChartResponse {
  taskViewDashBoardResponseEndSla: {
    doneOnTime: number,
    doneOverTime: number,
    overDue: number,
    doneOnTimeList: string[],
    doneOverTimeList: string[],
    overDueList: string[]
  }
}

export interface ICompareReportTaskViewDashboardDTO {
  requestTypeCode: string
  total: number
  total2: number
}

export interface ICompareReportViewDashBoardOPUserDTO {
  user: string,
  opuserFirst: number,
  opuserSecond: number
}

export interface ICompareReportViewDashBoardApprovedDTO {
  user: string,
  approvedFirst: number,
  approvedSecond: number
}

export interface IDashboardCompareReportResponse {
  taskViewDashBoardDTO: ICompareReportTaskViewDashboardDTO[]
  viewDashBoardOPUserDTOS: ICompareReportViewDashBoardOPUserDTO[],
  viewDashBoardApprovedDTOS: ICompareReportViewDashBoardApprovedDTO[],
  fiuserFirst: number,
  fiuserSecond: number,
  waitingFirst: number,
  waitingSecond: number,
}

export interface IDashboardUserChartResponse {
  totalRequestInUser: { [key: string]: number }
}
