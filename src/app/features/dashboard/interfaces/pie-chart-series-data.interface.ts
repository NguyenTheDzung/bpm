import { TRedirectWithParams } from "./dashboard-select.interface";

export interface IPieChartSeriesData<T> {
  id: T,
  value: number ,
  name: string,
  itemStyle: {
    color?: string
  },
  query?: string  // Sử dụng để đẩy vào translatePipe
}

export interface IDecisionChartSeriesData<TypeId> extends IPieChartSeriesData<TypeId> {
  params:  {
   NBU: TChartRedirectParams,
   COL: TChartRedirectParams
  } & Omit<TRedirectWithParams, 'resolution' | 'status'> //Link để điều hướng đến màn request và fill sẵn dữ liệu
}

export type TChartRedirectParams = {
  resolution?: string[],
  status?: string[]
}
