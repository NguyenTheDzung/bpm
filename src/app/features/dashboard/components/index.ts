import {OverviewComponent} from "./overview/overview.component";
import {SLAChartComponent} from "./sla-chart/sla-chart.component";
import {RequestChartComponent} from "./request-chart/request-chart.component";
import {DecisionChartComponent} from "./decision-chart/decision-chart.component";
import {DashboardSelectComponent} from "./dashboard-select/dashboard-select.component";
import {UserChartComponent} from "./user-chart/user-chart.component";

export const components = [
  OverviewComponent,
  SLAChartComponent,
  RequestChartComponent,
  DecisionChartComponent,
  DashboardSelectComponent,
  UserChartComponent
];
