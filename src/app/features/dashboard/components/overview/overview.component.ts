import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DashboardService} from "../../services/dashboard.service";
import {IDashboardRequestModel} from "../../interfaces/dashboard-request.interface";
import {finalize, map} from "rxjs";
import {DASHBOARD_TYPE, OVERVIEW_STATUS_OPT, REDIRECT_WITH_PARAMS} from "../../utils/enum";
import {
  TOverViewDoneRequest,
  TOverViewItemDataDashboard,
  TOverViewRemainingApprovedRequest,
  TRedirectWithParams
} from '../../interfaces/dashboard-select.interface';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  readonly OverviewStatus = OVERVIEW_STATUS_OPT;
  isLoading: boolean = true

  totalRequest?: TOverViewItemDataDashboard
  remainingRequest?: TOverViewItemDataDashboard
  SLADone?: TOverViewItemDataDashboard
  SLALate?: TOverViewItemDataDashboard
  slaApprovedDone?: TOverViewItemDataDashboard
  slaApprovalLate?: TOverViewItemDataDashboard
  doneRequest?: TOverViewDoneRequest
  totalApproveRequest?: number
  remainingApproveRequest?: TOverViewRemainingApprovedRequest
  totalTAT?: string

  currentSelectedType!: DASHBOARD_TYPE
  readonly DASHBOARD_TYPE = DASHBOARD_TYPE;


  isViewYear: boolean = false
  REDIRECT_WITH_PARAMS = REDIRECT_WITH_PARAMS;
  @Output() redirect = new EventEmitter<TRedirectWithParams>()



  @Input() canViewFullCard: boolean = false

  constructor(
    private dashboardService: DashboardService,
  ) {
  }

  ngOnInit() {
    this.dashboardService.selectedType$.subscribe(
      () => this.currentSelectedType = this.dashboardService.currentSelectedType
    )

    this.dashboardService.getRequest$().subscribe(
      {
        next: (request) => {
          this.isViewYear = request.isYear
          this.getData(request)
          this.canViewFullCard && this.getApprovedData(request)
        }
      }
    )
  }

  getData(request: Omit<IDashboardRequestModel, 'code' | 'type'>) {
    this.isLoading = true
    this.dashboardService.getOverviewInfoDashboard(request)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        {
          next: (response) => {
            const data = response.data.taskViewDashBoardResponseFirst
            this.totalRequest = this.createOverviewItem(data.totalRequestLast, data.totalRequest)
            this.remainingRequest = this.createOverviewItem(data.totalPendingLast, data.totalPending)
            this.SLADone = {
              ...this.createOverviewItem(data.totalSlaOnetimeLast, data.totalSlaOnetime),
              requestCodes: data.totalSlaOnetimeList
            }
            this.SLALate = {
              ...this.createOverviewItem(data.totalSlaLastedLast, data.totalSlaLasted, true),
              requestCodes: data.totalSlaLastedList
            }
            this.doneRequest = {
              value: data.doneRequest,
              pending: data.doneRequestPending,
              requestCodes: data.doneRequestPendingList
            }
          },
          error: () => {
            let fakeErrorData: TOverViewItemDataDashboard = {
              value: 0,
              percentage: 0,
              status: OVERVIEW_STATUS_OPT.unset
            }
            this.totalRequest =
              this.remainingRequest =
                this.SLADone = fakeErrorData
            this.SLALate = {...fakeErrorData, requestCodes: []}
          }
        },
      )
  }

  getApprovedData(request: Omit<IDashboardRequestModel, 'code' | 'type'>) {
    this.dashboardService.getApprovedData(request)
      .pipe(
        map((response) => {
          return response.data.taskViewDashBoardResponseFirst
        })
      )
      .subscribe(
        {
          next:
            (data) => {
              this.slaApprovedDone = {
                ...this.createOverviewItem(data.totalSlaApprovedDoneLast, data.totalSlaApprovedDone),
                requestCodes: data.totalSlaApprovedDoneList
              }
              this.slaApprovalLate = {
                ...this.createOverviewItem(data.totalSlaLastedApproveLast, data.totalSlaLastedApprove, true),
                requestCodes: data.totalSlaLastedApproveList
              }
              this.totalTAT = !isFinite(data.totalTAT) ? '---' : parseFloat(data.totalTAT.toFixed(2)) + ' Hrs'
              this.totalApproveRequest = data.totalApprovalRequest
              this.remainingApproveRequest  = {
                value:  data.totalRemainingApprovalRequest,
                requestCodes: data.totalRemainingApprovalRequestList
              }
            },
          error: () => {
            let fakeErrorData: TOverViewItemDataDashboard = {
              value: 0,
              percentage: 0,
              status: OVERVIEW_STATUS_OPT.unset
            }
            this.slaApprovedDone =
              this.slaApprovalLate =
                {...fakeErrorData, requestCodes: []}
            this.totalApproveRequest = 0
            this.remainingApproveRequest = {value: 0, requestCodes: []}
          }
        }
      )
  }

  createOverviewItem(lastMonth: number, currentMonth: number, isReverse: boolean = false): TOverViewItemDataDashboard {
    let percentage: number = 0
    let status: OVERVIEW_STATUS_OPT = OVERVIEW_STATUS_OPT.unset
    if (lastMonth != 0) {
      percentage = (Math.round(((currentMonth - lastMonth) / lastMonth * 100) * 100) / 100)
      if (percentage > 0) {
        if (isReverse) {
          status = OVERVIEW_STATUS_OPT.negative
        } else {
          status = OVERVIEW_STATUS_OPT.positive
        }
      } else if (percentage < 0) {
        if (isReverse) {
          status = OVERVIEW_STATUS_OPT.positive
        } else {
          status = OVERVIEW_STATUS_OPT.negative
        }
      } else {
        status = OVERVIEW_STATUS_OPT.unset
      }
    }
    return {
      percentage,
      value: currentMonth,
      status
    }
  }

  redirectToRequestScreen(code: REDIRECT_WITH_PARAMS, requestCodes?: string[]) {
    this.redirect.emit({code, requestCodes})
  }
}

