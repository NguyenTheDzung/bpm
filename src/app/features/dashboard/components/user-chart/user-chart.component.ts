import {Component} from '@angular/core';
import {ECharts, EChartsOption, SeriesOption} from "echarts";
import {combineLatest, map, startWith, Subject} from "rxjs";
import {IDashboardUserChartResponse} from "../../interfaces/dashboard-response.interface";
import {DashboardService} from "../../services/dashboard.service";
import {TranslateService} from "@ngx-translate/core";
import {IDashboardRequestModel} from "../../interfaces/dashboard-request.interface";
import {DashboardConstant} from "../../utils/constant";
import {IPieChartSeriesData} from "../../interfaces/pie-chart-series-data.interface";


@Component({
  selector: 'app-user-chart',
  templateUrl: './user-chart.component.html',
  styleUrls: ['./user-chart.component.scss']
})
export class UserChartComponent {
  chartOpts: EChartsOption = this.initChartOptions()
  chartOptsUpdated!: EChartsOption
  chartData = this.initChartData()
  apiDataChange$ = new Subject<IDashboardUserChartResponse>()
  chartInstant?: ECharts

  constructor(
    private dashboardService: DashboardService,
    private translateService: TranslateService
  ) {
    this.subscribeOnChangeSelection()
    this.subscribeOnChangeAPIAndLanguage()
  }

  subscribeOnChangeSelection() {
    this.dashboardService.getRequest$().subscribe(this.getData.bind(this))
  }

  getData(request: Omit<IDashboardRequestModel, 'code' | 'type'>) {
    this.dashboardService.getUserChartInfoDashboard(request).pipe(
      map((response, index) => {
        return response.data
      })
    ).subscribe(
      {
        next: (userChartResponse) => this.apiDataChange$.next(userChartResponse)
      }
    )
  }

  subscribeOnChangeAPIAndLanguage() {
    combineLatest([this.apiDataChange$, this.translateService.onLangChange.pipe(
      startWith(this.translateService.currentLang)
    )])
      .pipe(
        map(([responseData, lang]) => {
          return responseData.totalRequestInUser
        })
      ).subscribe(
      (responseData) => {
        this.chartData = Object.keys(responseData).map(
          (key) => {
           return {id: key, value: responseData[key], name: key, itemStyle: {}}
          }
        )
        const newData: EChartsOption = this.initChartOptions();
        (newData.series as SeriesOption).data = this.chartData
        this.chartInstant?.setOption(newData, true)
      }
    )
  }

  initChartOptions() {
    return DashboardConstant.PIE_CHART_CONFIG
  }

  initChartData(): IPieChartSeriesData<string>[] {
    return []
  }

  onChartInit($event: ECharts) {
    this.chartInstant = $event
  }
}
