import { Component, EventEmitter, Injector, Output } from '@angular/core';
import { ECharts, EChartsOption, SeriesOption } from 'echarts';
import { combineLatest, map, startWith, Subject } from 'rxjs';
import { DashboardService } from '../../services/dashboard.service';
import { IDashboardRequestModel } from '../../interfaces/dashboard-request.interface';
import { DashboardConstant } from '../../utils/constant';
import { IDashboardSLAChartResponse } from '../../interfaces/dashboard-response.interface';
import { SLA_CHART_FIELD } from '../../utils/enum';
import { BaseComponent } from '@shared/components';
import { HttpErrorResponse } from '@angular/common/http';
import { IEchartsSelectChangedEvent } from '../../interfaces/echarts-select-changed-event.interface';
import { IPieChartSeriesData } from '../../interfaces/pie-chart-series-data.interface';
import { TRedirectWithParams } from '../../interfaces/dashboard-select.interface';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-sla-chart',
  templateUrl: './sla-chart.component.html',
  styleUrls: ['./sla-chart.component.scss'],
})
export class SLAChartComponent extends BaseComponent {
  @Output() redirect = new EventEmitter<TRedirectWithParams>();
  chartOpts: EChartsOption = this.chartOptTemplate();
  chartInstant?: ECharts;
  chartData: (IPieChartSeriesData<SLA_CHART_FIELD> & { requestCodes?: string[] })[] = this.initChartData();
  apiDataChange$ = new Subject<IDashboardSLAChartResponse>();

  constructor(private dashboardService: DashboardService, inject: Injector) {
    super(inject);
    this.subscribeOnChangeSelection();
    this.getColorConfig();
    this.subscribeOnChangeApiDataAndLanguage();
  }

  subscribeOnChangeSelection() {
    this.dashboardService.getRequest$().subscribe(this.getData.bind(this));
  }

  getData(request: Omit<IDashboardRequestModel, 'code' | 'type'>) {
    this.dashboardService
      .getSLAChartInfoDashboard(request)
      .pipe(
        map((responseData) => {
          return responseData.data;
        })
      )
      .subscribe({
        next: (responseData) => {
          this.apiDataChange$.next(responseData);
        },
        error: (error: HttpErrorResponse) => {
          this.messageService.error(createErrorMessage(error));
        },
      });
  }

  subscribeOnChangeApiDataAndLanguage() {
    combineLatest([
      this.apiDataChange$,
      this.translateService.onLangChange.pipe(startWith(this.translateService.currentLang)),
    ])
      .pipe(
        map(([responseData, lang]) => {
          return responseData.taskViewDashBoardResponseEndSla;
        })
      )
      .subscribe((responseData) => {
        this.chartData.forEach((data) => {
          if (data.id == SLA_CHART_FIELD.DONE) {
            data.value = responseData.doneOnTime;
            data.requestCodes = responseData.doneOnTimeList;
          }
          if (data.id == SLA_CHART_FIELD.OVERTIME) {
            data.value = responseData.doneOverTime;
            data.requestCodes = responseData.doneOverTimeList;
          }
          if (data.id == SLA_CHART_FIELD.OVERDUE) {
            data.value = responseData.overDue;
            data.requestCodes = responseData.overDueList;
          }
          data.query && (data.name = this.translateService.instant(data.query));
        });
        const newData: EChartsOption = this.chartOptTemplate();
        (newData.series as SeriesOption).data = this.chartData;
        this.chartInstant?.setOption(newData, true);
      });
  }

  chartOptTemplate() {
    let chart = DashboardConstant.PIE_CHART_CONFIG;
    (chart.series as SeriesOption).data! = DashboardConstant.SLA_CHART_DATA;
    return chart;
  }

  getColorConfig() {
    this.dashboardService.getSLAChartConfigColor().subscribe((responseData) => {
      responseData.forEach((common) => {
        let index = this.chartData.findIndex((value) => value.id == common.code);
        if (index > -1) {
          this.chartData[index].itemStyle.color = common.value;
        }
      });
    });
  }

  initChartData(): IPieChartSeriesData<SLA_CHART_FIELD>[] {
    return DashboardConstant.SLA_CHART_DATA;
  }

  onChartInit(charts: ECharts) {
    this.chartInstant = charts;
    this.chartInstant?.on('selectchanged', 'select', ($event: any) => {
      this.chartClick($event);
    });
  }

  chartClick($event: IEchartsSelectChangedEvent) {
    const clickedItem = this.chartData[$event.fromActionPayload.dataIndexInside!];
    this.redirect.emit({ code: clickedItem.id, requestCodes: clickedItem?.requestCodes });
  }
}
