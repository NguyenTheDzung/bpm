import {Component, EventEmitter, Output} from '@angular/core';
import {ECharts, EChartsOption, SeriesOption} from 'echarts';
import {DashboardService} from '../../services/dashboard.service';
import {IDashboardRequestModel} from '../../interfaces/dashboard-request.interface';
import {combineLatest, map, startWith, Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {DashboardConstant} from '../../utils/constant';
import {IDashboardDecisionChartResponse} from '../../interfaces/dashboard-response.interface';
import {DASHBOARD_TYPE, DECISION_STATUS} from '../../utils/enum';
import {IEchartsSelectChangedEvent} from '../../interfaces/echarts-select-changed-event.interface';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationMessageService} from '@cores/services/message.service';
import {TRedirectWithParams} from '../../interfaces/dashboard-select.interface';
import {createErrorMessage} from '@cores/utils/functions';

@Component({
  selector: 'app-decision-chart',
  templateUrl: './decision-chart.component.html',
  styleUrls: ['./decision-chart.component.scss'],
})
export class DecisionChartComponent {
  chartsOption: EChartsOption = this.initChartOptions();
  chartInstant?: ECharts;
  chartData = this.initChartData();
  onApiDataChange$ = new Subject<IDashboardDecisionChartResponse>();
  currentSelectedType!: DASHBOARD_TYPE;
  @Output() redirect = new EventEmitter<TRedirectWithParams>();

  constructor(
    private dashboardService: DashboardService,
    private translateService: TranslateService,
    private notificationMessageService: NotificationMessageService
  ) {
    this.getColorConfig();
    this.subscribeOnChangeSelection();
    this.subscribeOnChangeAPIDataAndLanguage();
  }

  getData(request: Omit<IDashboardRequestModel, 'code' | 'type'>) {
    this.dashboardService
      .getDecisionChartInfoDashboard(request)
      .pipe(
        map((response) => {
          return response.data;
        })
      )
      .subscribe({
        next: (responseData) => {
          if (this.currentSelectedType) this.onApiDataChange$.next(responseData);
        },
        error: (error: HttpErrorResponse) => {
          this.notificationMessageService.error(createErrorMessage(error));
        },
      });
  }

  subscribeOnChangeAPIDataAndLanguage() {
    combineLatest([
      this.onApiDataChange$,
      this.translateService.onLangChange.pipe(startWith(this.translateService.currentLang)),
    ])
      .pipe(
        map(([response]) => {
          return response.taskViewDashBoardResponseEnd;
        })
      )
      .subscribe((responseData) => {
        this.chartData.forEach((data) => {
          switch (data.id) {
            case DECISION_STATUS.PENDING:
              data.value = responseData.pending;
              data.params.requestCodes = responseData.pendingList;
              break;
            case DECISION_STATUS.REJECT:
              data.value = responseData.reject;
              data.params.requestCodes = responseData.rejectList;
              break;
            case DECISION_STATUS.OVERDUE:
              data.value = responseData.overdue;
              data.params.requestCodes = responseData.overdueList;
              break;
            case DECISION_STATUS.CLOSE:
              data.value = responseData.close;
              data.params.requestCodes = responseData.closeList;
              break;
            case DECISION_STATUS.APPROVE:
              data.value = responseData.approve;
              data.params.requestCodes = responseData.approveList;
              break;
          }
          data.query && (data.name = this.translateService.instant(data.query));
        });
        const newData = this.initChartOptions();
        (newData.series as SeriesOption).data = [...this.chartData];
        this.chartInstant?.setOption(newData, true);
      });
  }

  subscribeOnChangeSelection() {
    this.dashboardService.getRequest$().subscribe((request) => {
      this.currentSelectedType = this.dashboardService.currentSelectedType!;
      this.chartData = this.checkChartData();
      this.getData(request);
    });
  }

  initChartOptions() {
    let pieChartConfig = DashboardConstant.PIE_CHART_CONFIG;
    (pieChartConfig.series as SeriesOption).data = DashboardConstant.DECISION_CHART_DATA;
    return pieChartConfig;
  }

  getColorConfig() {
    this.dashboardService.getDecisionChartConfigColor().subscribe({
      next: (responseData) => {
        responseData.forEach((common) => {
          let index = this.chartData.findIndex((value) => value.id == common.code);
          if (index > -1) {
            this.chartData[index].itemStyle.color = common.value;
          }
        });
      },
    });
  }

  initChartData() {
    return [...DashboardConstant.DECISION_CHART_DATA];
  }

  checkChartData() {
    let chart = this.initChartData();
    if (this.currentSelectedType == DASHBOARD_TYPE.NBU) {
      // OVERDUE chỉ hiển thị tại TYPE COL
      let indexOfOverdue = chart.findIndex((data) => data.id == DECISION_STATUS.OVERDUE);
      indexOfOverdue != -1 && chart.splice(indexOfOverdue, 1);
    } else if (this.currentSelectedType == DASHBOARD_TYPE.COL) {
      // CLOSE Chỉ hiển thị tại type NBU
      let indexOfClose = chart.findIndex((data) => data.id == DECISION_STATUS.CLOSE);
      indexOfClose != -1 && chart.splice(indexOfClose, 1);
    }
    return chart;
  }

  onChartInit($event: ECharts) {
    this.chartInstant = $event;
    this.chartInstant?.on('selectchanged', 'select', ($event: any) => {
      this.chartClick($event);
    });
  }

  chartClick($event: IEchartsSelectChangedEvent) {
    const payloads = this.chartData[$event.fromActionPayload.dataIndexInside];
    this.redirect.emit({
      code: payloads.params.code,
      requestCodes: payloads.params.requestCodes,
      ...(payloads.params[this.dashboardService.currentSelectedType!] as any),
    });
  }
}
