import { Component, Injector, OnInit } from '@angular/core';
import { ECharts, EChartsOption, Payload, SeriesOption } from 'echarts';
import { DashboardService } from '../../services/dashboard.service';
import { combineLatest, map, startWith, Subject } from 'rxjs';
import { IDashboardRequestModel } from '../../interfaces/dashboard-request.interface';
import {
  IRequestChartSeriesDynamicData,
  IRequestTypeChartAPIChange,
} from '../../interfaces/request-type-chart.interface';
import { DashboardConstant } from '../../utils/constant';
import { IDashboardListTaskGroupByTypeItem } from '../../interfaces/dashboard-response.interface';
import { InitDateListByMonthSelected, InitMonthList } from '../../utils/functions';
import { BaseComponent } from '@shared/components';
import { DASHBOARD_TYPE } from '../../utils/enum';
import { CommonModel } from '@common-category/models/common-category.model';
import { HttpErrorResponse } from '@angular/common/http';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-request-chart',
  templateUrl: './request-chart.component.html',
  styleUrls: ['./request-chart.component.scss'],
})
export class RequestChartComponent extends BaseComponent implements OnInit {
  chartOpts: EChartsOption = this.createChartOpts();
  apiData$ = new Subject<IRequestTypeChartAPIChange>();
  listChartSeriesConfig: any[] = [];
  fontSize = DashboardConstant.echartTextStyle.fontSize;
  chartInstance!: ECharts;

  private requestConfigColor: CommonModel[] = [];

  constructor(private dashboardService: DashboardService, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getConfigColor();
    this.subscribeOnChangeSelection();
    this.subscribeOnChangeLanguageAndApiData();
  }

  subscribeOnChangeSelection() {
    this.dashboardService.getRequest$().subscribe(this.getData.bind(this));
  }

  private getData(request: Omit<IDashboardRequestModel, 'code' | 'type'>) {
    this.dashboardService
      .getRequestChartInfoDashboard(request)
      .pipe(
        map((response) => {
          return response.data.listTaskGroupByType;
        })
      )
      .subscribe({
        next: (response) => {
          this.apiData$.next({
            request: request,
            response: response,
          });
        },
        error: (error: HttpErrorResponse) => {
          this.messageService.error(createErrorMessage(error));
        },
      });
  }

  mapTaskDataAndChartSeries(listTask: IRequestChartSeriesDynamicData[], listData: IDashboardListTaskGroupByTypeItem[]) {
    listTask.forEach((task) => {
      let dataRs = listData.find((value) => value.requestTypeCode == task.id);
      task.data.push(dataRs ? dataRs.total : undefined);
      task.total += dataRs ? dataRs.total : 0;
    });
    return listTask;
  }

  private subscribeOnChangeLanguageAndApiData() {
    combineLatest([
      this.apiData$,
      this.translateService.onLangChange.pipe(startWith(this.translateService.currentLang)),
    ])
      .pipe(
        map(([apiChange]) => {
          return apiChange;
        })
      )
      .subscribe({
        next: (nextApiValue) => {
          const startRequestDate = nextApiValue.request.condition.startDateFirst[0];
          const endRequestDate = nextApiValue.request.condition.endDateFirst[0];
          const isDataViewByYear = nextApiValue.request.isYear;

          if (!startRequestDate || !endRequestDate) {
            return;
          }
          const listDate = !isDataViewByYear
            ? InitDateListByMonthSelected(startRequestDate)
            : InitMonthList(startRequestDate);
          const [chartData, labels] = this.convertData(listDate, nextApiValue.response, isDataViewByYear);
          //@ts-ignore
          const config: SeriesOption[] = [
            ...chartData.map((chartSeriesData) => {
              //Mapping Color
              let colorIndex = this.requestConfigColor.findIndex((common) => common.code == chartSeriesData.id);
              if (colorIndex > -1) {
                chartSeriesData.color = this.requestConfigColor[colorIndex].value || undefined;
              }

              // Mapping translation
              chartSeriesData.query && (chartSeriesData.name = this.translateService.instant(chartSeriesData.query));

              return { ...chartSeriesData, ...DashboardConstant.REQUEST_CHART_SERIES_CONFIG, legendSelected: false };
            }),
          ];
          let chartOpts: EChartsOption = this.createChartOpts();
          this.listChartSeriesConfig = config;
          chartOpts.xAxis = { data: labels };
          chartOpts.series = config;
          this.chartInstance?.setOption(chartOpts, true);
        },
      });
  }

  createChartOpts() {
    return DashboardConstant.CONFIG_REQUEST_CHART;
  }

  /**
   * Thực hiện xử lý thông tin trả về data được convert view theo năm hoặc theo tháng
   * @param listDate      Danh sách ngày trong tháng
   * @param responseData  Dữ liệu trả về
   * @param viewYear      Xác nhận xem user đang chọn xem theo năm hay theo tháng
   *
   * @return  Trả về một danh sách gồm 2 phần tử là danh sách đữ liệu được convert và một danh sách label cho chart
   */
  convertData(
    listDate: Date[],
    responseData: IDashboardListTaskGroupByTypeItem[],
    viewYear: boolean
  ): [IRequestChartSeriesDynamicData[], number[]] {
    let listDataConvert: IRequestChartSeriesDynamicData[] = this.getChartViewLegend();
    let labels: number[] = [];
    // clear old data
    listDataConvert.forEach((value) => {
      value.total = 0;
      value.data = [];
    });

    listDate.forEach((date, index) => {
      let listDataFilter = responseData.filter((data) => {
        if (viewYear) {
          return new Date(data.createdDate).getMonth() == date.getMonth();
        } else {
          return new Date(data.createdDate).getDate() == date.getDate();
        }
      });

      labels.push(index + 1);
      listDataConvert = this.mapTaskDataAndChartSeries(listDataConvert, listDataFilter);
    });
    return [listDataConvert, labels];
  }

  getChartViewLegend() {
    let listDataConvert: IRequestChartSeriesDynamicData[] = [];

    if (this.dashboardService.currentSelectedType == DASHBOARD_TYPE.COL) {
      listDataConvert = [...DashboardConstant.REQUEST_CHART_REQUEST_TYPE_COL];
    } else if (this.dashboardService.currentSelectedType == DASHBOARD_TYPE.NBU) {
      listDataConvert = [...DashboardConstant.REQUEST_CHART_REQUEST_TYPE_NBU];
    }
    return listDataConvert;
  }

  getConfigColor() {
    this.dashboardService.getRequestChartColorConfig().subscribe((response) => {
      this.requestConfigColor = response;
    });
  }

  onChartInit($event: ECharts) {
    this.chartInstance = $event;
  }

  onMouseOver(index: number) {
    if (!this.chartInstance) {
      return;
    }
    let payload: Payload = {
      type: 'highlight',
      seriesIndex: index,
    };
    this.chartInstance.dispatchAction(payload);
  }

  onMouseLeave() {
    if (!this.chartInstance) {
      return;
    }
    let payload: Payload = {
      type: 'highlight',
    };
    this.chartInstance.dispatchAction(payload);
  }

  onToggle(name: string, index: number) {
    if (!this.chartInstance) {
      return;
    }
    let payload: Payload = {
      type: 'legendToggleSelect',
      name: name,
    };
    this.listChartSeriesConfig[index].legendSelected = !this.listChartSeriesConfig[index].legendSelected;
    this.chartInstance.dispatchAction(payload);
  }
}
