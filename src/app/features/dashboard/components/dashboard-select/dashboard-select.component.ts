import { Component, EventEmitter, Input, Output } from "@angular/core";
import { SelectItem } from "primeng/api/selectitem";
import { IDashboardSelectionModel, IDropdownItemWithTranslate } from "../../interfaces/dashboard-select.interface";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import * as moment from "moment";

@Component({
  selector: 'dashboard-select',
  templateUrl: './dashboard-select.component.html',
  styleUrls: ['./dashboard-select.component.scss'],
})
export class DashboardSelectComponent {
  readonly DEFAULT_MAX_SELECTED_LABELS = 3;
  maxSelectedLabels = this.DEFAULT_MAX_SELECTED_LABELS;
  @Input('Years') years: SelectItem<number>[] = [];

  _users: SelectItem<string>[] = [];
  @Input('Users')
  set users(users: SelectItem<string>[]) {
    if (users.length != 1) {
      this.maxSelectedLabels = users.length - 1;
    } else {
      this.maxSelectedLabels = this.DEFAULT_MAX_SELECTED_LABELS;
    }
    setTimeout(() => {
      this._users = users;
    }, 0);
  }


  @Input('Months') months: IDropdownItemWithTranslate<number>[] = [];

  @Input() selectedUsers: string[] = [];
  @Output() selectedUsersChange = new EventEmitter<typeof this.selectedUsers>();
  @Input() selectedYear!: number;
  @Output() selectedYearChange = new EventEmitter<typeof this.selectedYear>();
  @Input() selectedMonth!: number;
  @Output() selectedMonthChange = new EventEmitter<typeof this.selectedMonth>();
  @Output() selectionChange = new EventEmitter<IDashboardSelectionModel>();


  isUsersChanged: boolean = false;

  constructor() {}

  checkUsers() {
    /**
     * Nếu chỉ còn một user thì thực hiện disable user đó
     */
    if (this.selectedUsers?.length == 1) {
      const selectedUserIndex = this.users.findIndex((user) => user.value === this.selectedUsers[0]);
      this.users[selectedUserIndex] && (this.users[selectedUserIndex].disabled = true);
    } else if (this.selectedUsers.length == 0) {
      /**
       *     Nếu bỏ chọn tất cả thì sẽ chỉ định user đầu tiên được chọn
       *     và bị disable để không thể cho chọn lại
       */
      this.selectedUsers = [this.users[0].value];
      this.users[0].disabled = true;
    } else {
      /**
       *    re-assign lại mảng để nhận detectChanges
       */
      this.users.forEach((user) => (user.disabled = false));
    }
  }



  changeSelectedUsers() {
    if (!this.isUsersChanged) return;
    this.isUsersChanged = false;
    this.selectedUsersChange.emit(this.selectedUsers);
    this.changeSelection();
  }

  changeSelectedMonth($event: number = this.selectedMonth) {
    this.selectedMonthChange.emit($event);
    this.changeSelection();
  }

  changeSelectedYear($event: number = this.selectedYear) {
    this.selectedYearChange.emit($event);
    this.changeSelection();
  }

  changeSelection() {
    this.selectionChange.emit({
      selectedYear: this.selectedYear,
      selectedMonth: this.selectedMonth,
      selectedUser: this.selectedUsers,
    });
  }

  saveAsPDF() {
    const el = document.getElementById('dashboardView')!;

    html2canvas(el, { backgroundColor: '#eceff4' }).then((canvas) => {
      const image = canvas.toDataURL('image/png', 1.0);
      const doc = new jsPDF('p', 'px', 'a4');
      const pageWidth = doc.internal.pageSize.getWidth();
      const pageHeight = doc.internal.pageSize.getHeight();

      const widthRatio = pageWidth / canvas.width;
      const heightRatio = pageHeight / canvas.height;
      const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

      const canvasWidth = canvas.width * ratio;
      const canvasHeight = canvas.height * ratio;

      const marginX = (pageWidth - canvasWidth) / 2;
      // const marginY = (pageHeight - canvasHeight) / 2;

      doc.addImage(image, 'PNG', marginX, 0, canvasWidth, canvasHeight);
      doc.save(`bieu-do-thong-ke-${moment().format('DDMMYYYYHHmmss')}.pdf`);
    });
  }

}
