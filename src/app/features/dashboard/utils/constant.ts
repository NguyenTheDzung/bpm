import { EChartsOption } from 'echarts';
import { RequestType } from '@create-requests/utils/constants';
import { IRequestChartSeriesDynamicData } from '../interfaces/request-type-chart.interface';
import { IDecisionChartSeriesData, IPieChartSeriesData } from '../interfaces/pie-chart-series-data.interface';
import { DECISION_STATUS, REDIRECT_WITH_PARAMS, SLA_CHART_FIELD } from './enum';
import { CallbackDataParams } from 'echarts/types/dist/shared';
import { Resolution, StatusValue } from '@cores/utils/constants';

export class DashboardConstant {
  public static readonly echartTextStyle = {
    fontFamily:
      '-apple-system, BlinkMacSystemFont,' +
      ' "Segoe UI", Roboto, "Helvetica Neue",' +
      ' Arial, sans-serif, "Apple Color Emoji",' +
      ' "Segoe UI Emoji", "Segoe UI Symbol"',
    fontSize: 13,
  };

  public static readonly CONFIG_REQUEST_CHART = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      textStyle: this.echartTextStyle,
      formatter: (params: CallbackDataParams[]): string | HTMLElement | HTMLElement[] => {
        if (!params.some((param) => Boolean(param.value))) {
          return '';
        }
        let currentName = '';
        let tooltipMessage = '<div style="margin: 0 0 0;line-height:1;">';
        let wrapper = (margin?: number) => `<div style="margin: ${margin || 0}px 0 0;line-height:1;">`;
        let total = 0;
        let valueEl = (value: number) =>
          `<span style="float:right;margin-left:20px;font-size:14px;color:#666;font-weight:900">${value}</span>`;
        let seriesEl = (name: string) =>
          `<span style="font-size:14px;color:#666;font-weight:400;margin-left:2px">${name}</span>`;
        let closeWrapper = '</div>';
        params.forEach((param, index) => {
          if (!param.value) {
            return;
          }
          total += Number(param.value);
          !currentName &&
            (currentName = `<span style="font-size:14px;color:#666;font-weight:400;line-height:1;margin: 0 0 10px 0">${param.name}</span>`);
          tooltipMessage += `${wrapper(index == 0 ? 0 : 10)}
                                   ${param.marker}${seriesEl(String(param.seriesName))} ${valueEl(Number(param.value))}
                                 ${closeWrapper}`;
        });
        return wrapper() + currentName + valueEl(total) + wrapper(10) + tooltipMessage + closeWrapper + closeWrapper;
      },
    },
    legend: {
      show: false,
    },
    grid: {
      top: '3%',
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true,
    },
    textStyle: this.echartTextStyle,
    xAxis: {
      type: 'category',
      data: [],
      axisLine: { onZero: true },
      splitLine: { show: false },
      splitArea: { show: false },
    },
    yAxis: {
      type: 'value',
      nameTextStyle: {
        color: '#000',
      },
      splitNumber: 10,
    },
    series: [],
  } as EChartsOption;

  public static readonly REQUEST_CHART_SERIES_CONFIG = {
    type: 'bar',
    stack: 'total',
    label: {
      show: true,
      fontSize: 8,
      lineHeight: 20,
      color: '#fff',
    },
    emphasis: {
      focus: 'series',
    },
  };

  /*
   * NBU
   * 01 Hủy HSYCBH và hoàn phí BH
   * 02 Hủy HSYCBH và chuyển phí sang HSYCBH mới
   * 03 Hủy HĐBH trong 21 ngày và hoàn phí BH
   * 04 Hủy HĐBH trong 21 ngày và chuyển phí sang HSYCBH mới
   * 05 Hủy HSYCBH theo quyết định của phòng thẩm định
   * 08 RefundRequest
   *
   *  COL
   * 09 FeeInformationRequest
   * 10 ReinstatementPolicyRequest
   */

  public static readonly REQUEST_CHART_REQUEST_TYPE_NBU: IRequestChartSeriesDynamicData[] = [
    {
      id: RequestType.TYPE01,
      name: '',
      query: `requestType.${RequestType.TYPE01}`,
      color: '#050BA6',
      data: [],
      total: 0,
    },
    {
      id: RequestType.TYPE02,
      name: '',
      query: `requestType.${RequestType.TYPE02}`,
      color: '#3DA453',
      data: [],
      total: 0,
    },
    {
      id: RequestType.TYPE03,
      name: '',
      query: `requestType.${RequestType.TYPE03}`,
      color: '#DFA22F',
      data: [],
      total: 0,
    },
    {
      id: RequestType.TYPE04,
      name: '',
      query: `requestType.${RequestType.TYPE04}`,
      color: '#D91919',
      data: [],
      total: 0,
    },
    {
      id: RequestType.TYPE05,
      name: '',
      query: `requestType.${RequestType.TYPE05}`,
      color: '#5AC2F7',
      data: [],
      total: 0,
    },
    {
      id: RequestType.TYPE08,
      name: '',
      query: `requestType.${RequestType.TYPE08}`,
      color: '#FF7D94',
      data: [],
      total: 0,
    },
  ];

  public static readonly REQUEST_CHART_REQUEST_TYPE_COL: IRequestChartSeriesDynamicData[] = [
    { id: RequestType.TYPE09, name: '', query: `requestType.${RequestType.TYPE09}`, color: '', data: [], total: 0 },
    { id: RequestType.TYPE07, name: '', query: `requestType.${RequestType.TYPE07}`, color: '', data: [], total: 0 },
    { id: RequestType.TYPE10, name: '', query: `requestType.${RequestType.TYPE10}`, color: '', data: [], total: 0 },
  ];

  public static readonly DECISION_CHART_DATA: IDecisionChartSeriesData<DECISION_STATUS>[] = [
    {
      id: DECISION_STATUS.APPROVE,
      itemStyle: { color: '#5AC2F7' },
      params: {
        NBU: {
          status: [StatusValue.Complete, StatusValue.In_process],
          resolution: [
            Resolution.NBU_APPROVE,
            Resolution.NBU_P2P_INPROCESS,
            Resolution.NBU_FI_INPROCESS,
            Resolution.NBU_FI_DONE,
          ],
        },
        COL: {
          status: [StatusValue.Complete],
        },
        code: REDIRECT_WITH_PARAMS.DECISION_APPROVED,
      },
      name: ``,
      query: `base-status.${DECISION_STATUS.APPROVE}`,
      value: 0,
    },
    {
      id: DECISION_STATUS.PENDING,
      itemStyle: { color: '#1676F3' },
      params: {
        COL: {
          status: [StatusValue.In_process],
          resolution: [Resolution.COL_SALE_PENDING],
        },
        NBU: {
          status: [StatusValue.In_process],
          resolution: [Resolution.NBU_SALE_INPROCESS, Resolution.NBU_SALE_PENDING],
        },
        code: REDIRECT_WITH_PARAMS.DECISION_PENDING,
      },
      name: ``,
      query: `base-status.${DECISION_STATUS.PENDING}`,
      value: 0,
    },
    {
      id: DECISION_STATUS.REJECT,
      itemStyle: { color: '#FF7D94' },
      params: {
        COL: {
          status: [StatusValue.Reject],
        },
        NBU: {
          status: [StatusValue.Reject],
        },
        code: REDIRECT_WITH_PARAMS.DECISION_REJECT,
      },
      name: ``,
      query: `base-status.${DECISION_STATUS.REJECT}`,
      value: 0,
    },
    {
      id: DECISION_STATUS.OVERDUE,
      itemStyle: {},
      params: {
        COL: {
          status: [StatusValue.Overdue],
        },
        NBU: {},
        code: REDIRECT_WITH_PARAMS.DECISION_OVERDUE,
      },
      name: ``,
      query: `base-status.${DECISION_STATUS.OVERDUE}`,
      value: 0,
    },
    {
      id: DECISION_STATUS.CLOSE,
      itemStyle: {},
      params: {
        COL: {},
        NBU: {
          status: [StatusValue.Close],
        },
        code: REDIRECT_WITH_PARAMS.DECISION_CLOSE,
      },
      name: ``,
      query: `base-status.${DECISION_STATUS.CLOSE}`,
      value: 0,
    },
  ];

  public static readonly SLA_CHART_DATA: IPieChartSeriesData<SLA_CHART_FIELD>[] = [
    {
      id: SLA_CHART_FIELD.DONE,
      value: 0,
      name: ``,
      itemStyle: { color: '#41E564' },
      query: `base-status.${SLA_CHART_FIELD.DONE}`,
    },
    {
      id: SLA_CHART_FIELD.OVERTIME,
      value: 0,
      name: ``,
      itemStyle: { color: '#FFAD12' },
      query: `base-status.${SLA_CHART_FIELD.OVERTIME}`,
    },
    {
      id: SLA_CHART_FIELD.OVERDUE,
      value: 0,
      name: ``,
      itemStyle: { color: '#D91919' },
      query: `base-status.${SLA_CHART_FIELD.OVERDUE}`,
    },
  ];

  public static readonly PIE_CHART_CONFIG: EChartsOption = {
    tooltip: {
      trigger: 'item',
      textStyle: this.echartTextStyle,
    },
    legend: {
      orient: 'vertical',
      left: 'left',
      textStyle: this.echartTextStyle,
    },
    series: {
      name: '',
      type: 'pie',
      radius: '50%',
      data: [],
      label: {
        formatter: '{d}%',
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  };

  public static readonly CONFIG_REQUEST_CHART_COLOR = 'CONFIG_REQUEST_CHART_COLOR';
  public static readonly CONFIG_SLA_CHART_COLOR = 'CONFIG_SLA_CHART_COLOR';
  public static readonly CONFIG_DECISION_CHART_COLOR = 'CONFIG_DECISION_CHART_COLOR';

}
