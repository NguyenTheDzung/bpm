import * as moment from 'moment/moment';
import * as _ from 'lodash';
import { isDate } from 'lodash';

export function InitDateListByMonthSelected( startDate: Date | string) {
  !isDate(startDate) && (startDate = new Date(startDate));
  if (_.isNaN(startDate) ) {
    throw new Error('[InitDateListByMonthSelected]::error Invalid date input');
  }

  let endDateNumber = moment(startDate).endOf("month").get("date");
  let result: Date[] = [];
  for (let dt = 1; dt <= endDateNumber; dt ++) {
    result.push(moment().set({date: dt}).toDate());
  }
  return result;
}

export function InitMonthList(date: string | Date) {
  !_.isDate(date) && (date = new Date(date));
  if (_.isNaN(date)) {
    throw new DOMException('Invalid Date');
  }
  let year = date.getFullYear();
  let yearList: Date[] = [];
  for (let i = 0; i < 12; i++) {
    yearList.push(moment().set({ year: year, month: i }).toDate());
  }
  return yearList;
}

export function getMonthNames(lang: string, format: string) {
  let userLang = moment.locale(lang);
  let months = [];
  let month = moment.months();
  for (let i = 0; i < 12; i++) {
    let firstLetter = month[i].charAt(0).toUpperCase();
    month[i] = month[i].slice(1, month[i].length);
    month[i] = firstLetter + month[i];
    months.push(month[i]);
  }
  moment.locale(userLang);
  return months;
}
