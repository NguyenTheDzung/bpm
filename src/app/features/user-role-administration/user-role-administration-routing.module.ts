import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'assign-role-user',
    loadChildren: () => import('./assign-role-user/assign-role-user.module').then((m) => m.AssignRoleUserModule),
  },
  {
    path: 'assign-permission-role',
    loadChildren: () =>
      import('./assign-permission-role/assign-permission-role.module').then((m) => m.AssignPermissionRoleModule),
  },

  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoleAdministrationRoutingModule {}
