import { FunctionCode } from '@cores/utils/enums';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { AssignPermissionRoleComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: AssignPermissionRoleComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.AssignPermissionForRole,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.AssignPermissionForRole}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignPermissionRoleRoutingModule {}
