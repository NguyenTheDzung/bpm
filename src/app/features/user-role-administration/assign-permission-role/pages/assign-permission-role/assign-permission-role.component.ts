import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { PaginatorModel } from '@cores/models/paginator.model';
import { BaseTableComponent } from '@shared/components';
import * as _ from 'lodash';
import { TreeNode } from 'primeng/api';
import { AssignPremissitionRoleModel, PremissitionModel, RoleModel } from '../../models/assign-Premissition-Role';
import { AssignPremissionRoleService } from '../../service/assign-premission-role.service';

@Component({
  selector: 'app-assign-permission-role',
  templateUrl: './assign-permission-role.component.html',
  styleUrls: ['./assign-permission-role.component.scss'],
})
export class AssignPermissionRoleComponent extends BaseTableComponent<AssignPremissitionRoleModel> implements OnInit {
  isBtnReadonly: boolean = true;
  data!: TreeNode[];
  override params: RoleModel = {
    name: '',
    code: '',
    search: null,
  };
  paramPermission: PremissitionModel = {
    name: '',
    code: '',
  };
  roleCode: string = '';
  form = this.fb.group({
    roleCode: [''],
    permissions: this.fb.array([], Validators.required),
  });
  selected: string[] = [];

  constructor(inject: Injector, private service: AssignPremissionRoleService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.getDataTableRole();
    this.getDataTreePermission();
  }

  assign() {
    this.loadingService.start();
    let dataForm = {
      roleCode: this.roleCode,
      permissions: this.selected,
    };

    this.form.patchValue(dataForm);
    this.loadingService.complete();
    this.service.assignPermission(dataForm).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.assignSuccess');
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
        this.loadingService.complete();
      },
    });
  }

  getDataTableRole(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const params = this.mapDataSearch();

    this.service.assignRoleUser(<RoleModel>params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.error('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: () => {
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
        this.loadingService.complete();
      },
    });
  }

  getDataTreePermission() {
    this.loadingService.start();
    this.service.getDataPremission().subscribe({
      next: (dataTree) => {
        const listNode: TreeNode[] = _.map(dataTree.data?.content, (item) => {
          return <TreeNode>{
            data: item,
            label: item?.name,
            expanded: true,
            id: item.id,
            parent: item.data?.name,
          };
        });

        this.data = _.filter(listNode, (item) => !item.data?.parentResources && !_.isEmpty(item.data?.resources)); //lấy cha,điều kiện key parent = null thì lấy
        this.mapTreeFunction(listNode, this.data);
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }

  mapTreeFunction(list: TreeNode[], listParent: TreeNode[]) {
    for (const item of listParent) {
      const listChildren = list?.filter((i) => i?.data?.parentResources === item.data?.resources); //

      if (listChildren.length > 0) {
        item.children = listChildren;
        this.mapTreeFunction(list, item.children);
      }
    }
  }

  override pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.getDataTableRole();
  }

  override viewDetail(code: string) {
    this.loadingService.start();
    this.serviceBase.findByCode(code).subscribe({
      next: (data) => {
        this.isBtnReadonly = false;
        this.roleCode = code;
        this.selected = _.map(data?.data, (item) => item.id);
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  onChangeValue(checked: boolean, node: TreeNode) {
    node.data.checked = !node.data.checked;
    if (node.data.checked && _.size(node.children) > 0) {
      this.checkedNodeChildren(node.children!);
    }
    this.selected = _.uniq(this.selected);
  }

  checkedNodeChildren(nodes: TreeNode[]) {
    nodes.forEach((node) => {
      this.selected.push(node.data.id);
      if (_.size(node.children) > 0) {
        this.checkedNodeChildren(node.children!);
      }
    });
  }
}
