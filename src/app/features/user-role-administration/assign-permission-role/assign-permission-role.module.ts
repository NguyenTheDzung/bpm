import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AssignPermissionRoleRoutingModule } from './assign-permission-role-routing.module';
import { SharedModule } from '@shared/shared.module';
import { pages } from './pages';

@NgModule({
  declarations: [...pages],
  imports: [AssignPermissionRoleRoutingModule, SharedModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AssignPermissionRoleModule {}
