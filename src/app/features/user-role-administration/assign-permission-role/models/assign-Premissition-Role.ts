export interface AssignPremissitionRoleModel {
  data?: any;
  code?: string;
  name?: string;
  description?: string;
  search?: string | null;
  id?: string;
}
export interface RoleModel {
  code?: string;
  name?: string;
  search?: null | string;
}
export interface PremissitionModel {
  code?: string;
  name?: string;
}
