import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { map, Observable } from 'rxjs';
import { RoleService } from '@role-category/services/role.service';

@Injectable({
  providedIn: 'root',
})
export class AssignPremissionRoleService extends BaseService {
  constructor(http: HttpClient, private roleService: RoleService) {
    super(http, `${environment.employee_url}`);
  }

  override search(params?: any, isPost?: boolean): Observable<any> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<any>(`${this.baseUrl}`, { params: newParam })
        .pipe(map((data) => mapDataTableEmployee(data, params)));
    }
    return this.http
      .get<any>(`${this.baseUrl}/permission`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  assignRoleUser(params?: any) {
    return this.roleService.search(params);
  }

  override findByCode(code: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/role/get-permisisons/${code}`);
  }

  assignPermission(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/permission/assign`, data);
  }

  getDataPremission() {
    return this.http.get<any>(`${this.baseUrl}/permission`);
  }

  getFunction(): Observable<any> {
    return this.http.get<any>(`${environment.employee_url}/resource`);
  }
}
