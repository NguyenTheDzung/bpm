import { FunctionCode } from '@cores/utils/enums';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionGuardService } from '@cores/services/function-guard.service';
import { AssignRoleUserListComponent } from './pages';

const routes: Routes = [
  {
    path: '',
    component: AssignRoleUserListComponent,
    canActivate: [FunctionGuardService],
    data: {
      functionCode: FunctionCode.AssignRoleForUser,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.AssignRoleForUser}`,
    },
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignRoleUserRoutingModule {}
