import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataTable } from '@cores/models/data-table.model';
import { BaseService } from '@cores/services/base.service';
import { mapDataTableEmployee, removeParamSearch } from '@cores/utils/functions';
import { environment } from '@env';
import { map, Observable } from 'rxjs';
import { UserService } from '@user/services/user.service';

@Injectable({
  providedIn: 'root',
})
export class AssignRoleUserService extends BaseService {
  constructor(http: HttpClient, private userService: UserService) {
    super(http, `${environment.employee_url}`);
  }

  override search(params?: any, isPost?: boolean): Observable<any> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}`, { params: newParam })

        .pipe(map((data) => mapDataTableEmployee(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}/role`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  assignRole(data: any): Observable<any> {
    return this.http.post(`${environment.employee_url}/user/assignRolesToUser`, data);
  }

  override findByUsername(username: string): Observable<any> {
    return this.http.get(`${environment.employee_url}/user/role/${username}`);
  }

  assignRoleUser(params?: any) {
    return this.userService.search(params);
  }
}
