export interface AssignRoleUserModel {
  id?: string;
  account?: string;
  name?: string;
  username?: string;
  search?: string | null;
  data?: any;
  code?: string;
}
export interface RoleModel {
  code?: string;
  name?: string;
  username?: string;
  search?: string;
}
export interface UserModel {
  code?: string;
  name?: string;
  username?: string;
  search?: string | null;
}
