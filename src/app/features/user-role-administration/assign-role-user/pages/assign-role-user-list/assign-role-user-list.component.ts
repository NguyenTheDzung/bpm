import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { PaginatorModel } from '@cores/models/paginator.model';
import { BaseTableComponent } from '@shared/components';
import { AssignRoleUserModel, RoleModel, UserModel } from '../../models/assign-role-user.model';
import { AssignRoleUserService } from '../../services/assign-role-user.service';

@Component({
  selector: 'app-assign-role-user-list',
  templateUrl: './assign-role-user-list.component.html',
  styleUrls: ['./assign-role-user-list.component.scss'],
})
export class AssignRoleUserListComponent extends BaseTableComponent<AssignRoleUserModel> implements OnInit {
  isBtnReadonly: boolean = true;
  override params: RoleModel = {
    name: '',
    code: '',
    username: '',
  };
  paramUser: UserModel = {
    name: '',
    code: '',
    username: '',
  };
  userName: string = '';
  itemSelection: AssignRoleUserModel[] = [];
  dataTableRole = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  form = this.fb.group({
    userCode: [''],
    role: this.fb.array([], Validators.required),
  });

  constructor(inject: Injector, private service: AssignRoleUserService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.getState();
    this.getDataTableUser();
  }

  assign() {
    this.loadingService.start();
    let ids = [];
    if (this.itemSelection.length > 0) {
      for (let item of this.itemSelection) {
        ids.push(item);
      }
    }
    const dataForm = {
      userName: this.userName,
      role: ids,
    };
    this.form.patchValue(dataForm);
    this.service.assignRole(dataForm).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.assignSuccess');
        this.itemSelection = [];
        this.viewRole(dataForm.userName);
      },
      error: () => {
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
        this.loadingService.complete();
      },
    });
  }

  getDataTableUser(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const params = {
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
      ...this.paramUser,
    };

    this.service.assignRoleUser(<RoleModel>params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.error('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: () => {
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
        this.loadingService.complete();
      },
    });
  }

  pageChangeRole(paginator: PaginatorModel) {
    this.dataTableRole.currentPage = paginator.page;
    this.dataTableRole.size = paginator.rows;
    this.dataTableRole.first = paginator.first;
    this.search();
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTableRole.currentPage = 0;
    }

    this.loadingService.start();
    const params = {
      page: this.dataTableRole.currentPage,
      size: this.dataTableRole.size,
      ...this.params,
    };

    this.service.search(<AssignRoleUserModel>params).subscribe({
      next: (data) => {
        this.dataTableRole = data;
        if (this.dataTableRole.content.length === 0) {
          this.messageService.error('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: () => {
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
        this.loadingService.complete();
      },
    });
  }

  override pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.getDataTableUser();
  }

  viewRole(username: string) {
    this.loadingService.start();
    this.serviceBase.findByUsername(username).subscribe({
      next: (data) => {
        this.isBtnReadonly = false;
        this.userName = username;
        this.itemSelection = data?.data;
        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }
}
