import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AssignRoleUserRoutingModule } from './assign-role-user-routing.module';
import { SharedModule } from '@shared/shared.module';
import { pages } from './pages';

@NgModule({
  declarations: [...pages],
  imports: [SharedModule, AssignRoleUserRoutingModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AssignRoleUserModule {}
