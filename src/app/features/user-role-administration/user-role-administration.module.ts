import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { UserRoleAdministrationRoutingModule } from './user-role-administration-routing.module';

@NgModule({
  imports: [UserRoleAdministrationRoutingModule, SharedModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  declarations: [],
})
export class UserRoleAdministrationModule {}
