import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BaseActionComponent } from '@shared/components';
import { createErrorMessage } from '@cores/utils/functions';
import { RequestService } from '@requests/services/request.service';
import { UserModel } from '@requests/models/request.model';
import { ClaimService } from '../../services/claim.service';
import { DataAssign } from '../../models/claim-request.model';

@Component({
  selector: 'app-forward-claim',
  templateUrl: './forward-claim.component.html',
  styleUrls: ['./forward-claim.component.scss'],
})
export class ForwardClaimComponent extends BaseActionComponent implements OnInit {
  userList: UserModel[] = [];
  nameUser?: string | null;
  override form = this.fb.group({
    dept: null,
    requestId: [this.data],
    newPic: [[], [Validators.required]],
  });

  constructor(
    inject: Injector,
    private services: RequestService,
    private translate: TranslateService,
    private claimService: ClaimService
  ) {
    super(inject, services);
  }

  ngOnInit(): void {
    this.onChange();
  }

  onChange() {
    if (this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    const roleName = this.configDialog?.data?.roleName;
    this.form.get('dept')?.setValue(roleName);
    this.services.getPic({ roleName, activeOnly: true }).subscribe({
      next: (data) => {
        this.userList = data || [];
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  onChangUser(event: any) {
    let intersection = this.userList.filter((itemArr) => event.value.includes(itemArr.account));
    let user = intersection.map((item) => item.name);
    this.nameUser = user.join();
  }

  override create(data: DataAssign) {
    if (this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    this.claimService.assignRequest(data).subscribe({
      next: () => {
        this.messageService.success(this.translate.instant('MESSAGE.FORWARD_SUCCESS'));
        this.form.reset();
        this.refDialog.close(true);
      },
      error: (e) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(e));
      },
    });
  }

  override getDataForm() {
    const data: DataAssign = {
      requestIds: this.data,
      userName: this.form.get('newPic')?.value,
    };
    return data;
  }
}
