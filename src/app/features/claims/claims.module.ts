import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { pages } from './pages';
import { SharedModule } from '@shared/shared.module';
import { ClaimsRoutingModule } from './claims-routing.module';
import { components } from './components';
import {ClaimTPAModule} from "@claim-detail/claim-tpa.module";

@NgModule({
  imports: [SharedModule, ClaimsRoutingModule, ClaimTPAModule],
  declarations: [...pages, ...components],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ClaimsModule {}
