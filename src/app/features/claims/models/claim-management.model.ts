import { Nullable } from '@cores/models/ts-helpers';
import { CommonModel } from '@common-category/models/common-category.model';
import { User } from '@requests/models/request.model';

export interface ClaimManageSearch {
  requestId: Nullable<string>;
  claimId: Nullable<string>;
  status: Nullable<string[]>;
  resolution: Nullable<string[]>;
  fromDate: Nullable<string>;
  toDate: Nullable<string>;
  pic: Nullable<string[]>;
  requestSource: Nullable<string[]>;
  typeRequest: Nullable<string[]>;
  claimStatus: Nullable<string[]>;
  policyNumber: Nullable<string>;
  bp: Nullable<string>;
  bpLA: Nullable<string>;
}

export type ClaimManageParams = ClaimManageSearch & {
  page: number;
  size: number;
};

export interface ClaimManage {
  claimId: number;
  requestId: string;
  policyNumber: string;
  bpName: string;
  laName: string;
  startEventDate: string;
  endEventDate: string;
  evaluateClaimAmount: string;
  status: string;
  resolution: string;
  pic: string;
  tpa: string;
  createdDate: string;
  updatedDate: string;
  requestSource: string;
  documentSource: string;
  stDate: string;
  phone: string;
  email: string;
  claimType: string;
}

export interface DataAssign {
  requestIds: string[];
  userName: string;
}

export interface CategoryClaimState {
  listResolution: CommonModel[];
  listStatus: CommonModel[];
  listStatusClaim: CommonModel[];
  listSource: CommonModel[];
  listRequestType: CommonModel[];
  listPic: User[];
}
