export interface Claim {
  requestId: string | null;
  claimId: string | null;
  typeRequest: string | null;
  policyNumber: string | null;
  fromDate: string | null;
  toDate: string | null;
  requestSource: string | null;
  status?: string;
  sla?: number;
  minutesSlaStep?: number;
  hoursSlaStep?: number;
  daySlaStep?: number;
}

export type SearchParams = Claim & {
  page: number;
  size: number;
};

export interface DataAssign {
  requestIds: string[];
  userName: string;
}
