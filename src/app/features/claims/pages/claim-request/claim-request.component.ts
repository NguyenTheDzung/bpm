import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseTableComponent } from '@shared/components';
import { ClaimService } from '../../services/claim.service';
import { Claim, DataAssign, SearchParams } from '../../models/claim-request.model';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import * as moment from 'moment';
import { CLAIM_SOURCE_REQUEST, REQUEST_TYPE, Roles } from '@cores/utils/constants';
import { CommonModel } from '@common-category/models/common-category.model';
import { isNumber } from 'lodash';
import { combineLatest } from 'rxjs';
import { ScreenType } from '@cores/utils/enums';
import { ForwardClaimComponent } from '../../components/forward-claim/forward-claim.component';
import { Table } from 'primeng/table';
import { RequestService } from '@requests/services/request.service';
import { PermissionForward } from '@requests/models/request.model';
import { RequestType } from '@create-requests/utils/constants';

@Component({
  selector: 'claim-request',
  templateUrl: './claim-request.component.html',
  styleUrls: ['./claim-request.component.scss'],
})
export class ClaimRequestComponent extends BaseTableComponent<Claim> implements OnInit {
  override params: Claim = {
    requestId: null,
    claimId: null,
    toDate: null,
    fromDate: null,
    typeRequest: null,
    requestSource: null,
    policyNumber: null,
  };
  selectedItem: Claim[] = [];
  listType: CommonModel[] = [];
  listSource: CommonModel[] = [];
  forwardConfig: PermissionForward | null = null;
  @ViewChild(Table) table?: Table;

  constructor(injector: Injector, private service: ClaimService, private requestService: RequestService) {
    super(injector, service);
  }

  ngOnInit() {
    this.loadingService.start();
    combineLatest([
      this.commonService.getCommonCategory(CLAIM_SOURCE_REQUEST),
      this.commonService.getCommonCategory(REQUEST_TYPE),
      this.requestService.getPermissionForwardRequest(),
    ]).subscribe({
      next: ([listSource, listType, forwardConfig]) => {
        this.listType = listType?.filter((item) => [RequestType.TYPE11].includes(item.value));
        this.listSource = listSource;
        this.forwardConfig = forwardConfig;
        this.search(true);
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }

  override mapDataSearch(): SearchParams {
    return {
      ...this.params,
      fromDate: getValueDateTimeLocal(this.params.fromDate, moment.HTML5_FMT.DATE),
      toDate: getValueDateTimeLocal(this.params.toDate, moment.HTML5_FMT.DATE),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params = this.mapDataSearch();
    this.service.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (firstPage && this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.dataTable.content.forEach((item) => {
          if (item.status) {
            if (isNumber(item.sla)) {
              item.sla = Math.abs(item.sla);
              item.minutesSlaStep = item.sla % 60;
              item.hoursSlaStep = Math.floor(item.sla / 60);
              item.daySlaStep = Math.floor(item.hoursSlaStep / 24);
            }
            if (isNumber(item.daySlaStep) && item.daySlaStep > 0) {
              item.hoursSlaStep = item.hoursSlaStep! % 24;
              item.status = `request.detailRequest.underwrite.${item.status}_DAY`;
            } else {
              item.status = `request.detailRequest.underwrite.${item.status}`;
            }
          }
        });
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  doDetail(item: Claim) {
    /**
     * Chuyển hướng đến chi tiết claim TPA (lớn)
     * và chỉ hiển thị thông tin không hiển thị xử lý
     */
    this.loadingService.start();
    this.router
      .navigate(['detail', 'claim', 'tpa', item.requestId], { relativeTo: this.route })
      .then(() => this.loadingService.complete());
  }

  assign() {
    if (this.loadingService.loading || !this.currUser?.username || this.selectedItem.length === 0) {
      return;
    }
    if (
      this.authService.getUserRoles().includes(Roles.OP_CLAIM_ASSESSOR) &&
      !this.authService.getUserRoles().includes(Roles.OP_CLAIM_ADMIN)
    ) {
      const data: DataAssign = {
        requestIds: this.selectedItem.map((item) => item.requestId!),
        userName: this.currUser.username,
      };
      this.loadingService.start();
      this.service.assignRequest(data).subscribe({
        next: () => {
          this.selectedItem = [];
          this.table!.selectionKeys = {};
          this.search(false);
          this.messageService.success('MESSAGE.SUCCESS');
        },
        error: (e) => {
          this.loadingService.complete();
          this.messageService.error(createErrorMessage(e));
        },
      });
    } else {
      const dialog = this.dialogService?.open(ForwardClaimComponent, {
        header: 'forward user',
        showHeader: false,
        width: '40%',
        data: {
          screenType: ScreenType.Create,
          state: this.stateData,
          model: this.selectedItem.map((item) => item.requestId!),
          roleName: this.forwardConfig?.forwardTypeCodes?.find((item) =>
            item.types.includes(this.selectedItem[0]?.typeRequest!)
          )?.role,
        },
      });
      dialog?.onClose.subscribe({
        next: (isSuccess) => {
          if (isSuccess) {
            this.selectedItem = [];
            this.table!.selectionKeys = {};
            this.search();
          }
        },
      });
    }
  }

  showBtnAssign() {
    return (
      (this.authService.getUserRoles().includes(Roles.OP_CLAIM_ADMIN) ||
        this.authService.getUserRoles().includes(Roles.OP_CLAIM_ASSESSOR)) &&
      this.objFunction?.scopes?.includes(this.action.Forward)
    );
  }

  onReset() {
    this.params = {
      requestId: null,
      claimId: null,
      toDate: null,
      fromDate: null,
      typeRequest: null,
      requestSource: null,
      policyNumber: null,
    };
    this.search();
  }
}
