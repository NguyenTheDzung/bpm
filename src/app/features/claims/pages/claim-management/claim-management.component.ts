import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { ClaimService } from '../../services/claim.service';
import { Claim } from '../../models/claim-request.model';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import * as moment from 'moment';
import {
  CategoryClaimState,
  ClaimManage,
  ClaimManageParams,
  ClaimManageSearch,
  DataAssign,
} from '../../models/claim-management.model';
import { DataTable } from '@cores/models/data-table.model';
import { PaginatorModel } from '@cores/models/paginator.model';
import { Roles } from '@cores/utils/constants';
import { RequestType } from '@create-requests/utils/constants';
import { ForwardClaimComponent } from '../../components/forward-claim/forward-claim.component';
import { ScreenType } from '@cores/utils/enums';
import { Table } from 'primeng/table';
import { PermissionForward } from '@requests/models/request.model';
import { forkJoin, map, tap } from 'rxjs';
import { RequestService } from '@requests/services/request.service';

@Component({
  selector: 'claim-management',
  templateUrl: './claim-management.component.html',
  styleUrls: ['./claim-management.component.scss'],
})
export class ClaimManagementComponent extends BaseComponent implements OnInit {
  params: ClaimManageSearch = {
    requestId: null,
    claimId: null,
    toDate: null,
    fromDate: null,
    typeRequest: null,
    requestSource: null,
    policyNumber: null,
    bp: null,
    pic: null,
    status: null,
    resolution: null,
    bpLA: null,
    claimStatus: null,
  };
  dataTable: DataTable<ClaimManage> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  categories: CategoryClaimState = {
    listPic: [],
    listSource: [],
    listStatus: [],
    listStatusClaim: [],
    listResolution: [],
    listRequestType: [],
  };
  prevParams?: ClaimManageParams;
  selectedItem: ClaimManage[] = [];
  forwardConfig: PermissionForward | null = null;
  @ViewChild(Table) table?: Table;

  /**
   * Chỉ có roles admin mới show pic
   */
  containAdminOPClaim$ = this.authService.getUserRoles$().pipe(
    map((roles) => {
      return roles.includes(Roles.OP_CLAIM_ADMIN)
    })
  )

  constructor(injector: Injector, private service: ClaimService, private requestService: RequestService) {
    super(injector);
  }

  ngOnInit() {
    this.loadingService.start();
    forkJoin( [
      this.service.getCategories(
        [ Roles.OP_CLAIM_ASSESSOR ]
      ),
      this.requestService.getPermissionForwardRequest()
    ] ).subscribe( {
      next: ( [ dataCategories, forwardConfig ] ) => {
        this.categories = dataCategories;
        this.forwardConfig = forwardConfig;
        this.categories.listRequestType = dataCategories.listRequestType?.filter( ( item ) =>
          [ RequestType.TYPE11 ].includes( item.value )
        );
        console.log( this.categories.listRequestType );
        // set default params all user
        this.params.status = [ '001' ];
        this.params.resolution = [ '006' ];
        // set default params user admin
        if ( this.authService.getUserRoles().includes( Roles.OP_CLAIM_ADMIN ) ) {
          this.params.resolution = [ '006', '008', '009', '010', '011', '012', '013' ];
        }
        // set default params user assessor
        if (
          this.authService.getUserRoles().includes( Roles.OP_CLAIM_ASSESSOR ) &&
          !this.authService.getUserRoles().includes( Roles.OP_CLAIM_ADMIN )
        ) {
          this.params.pic = [ this.currUser.username ];
        }

        this.search( true );
      },
      error: () => {
        this.loadingService.complete();
      },
    } );
  }

  mapDataSearch(): ClaimManageParams {
    return {
      ...this.params,
      fromDate: getValueDateTimeLocal(this.params.fromDate, moment.HTML5_FMT.DATE),
      toDate: getValueDateTimeLocal(this.params.toDate, moment.HTML5_FMT.DATE),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
  }

  search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params = this.mapDataSearch();
    this.service.searchManagement(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (firstPage && this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        // this.dataTable.content.forEach((item) => {
        //   if (item.status) {
        //     if (isNumber(item.sla)) {
        //       item.sla = Math.abs(item.sla);
        //       item.minutesSlaStep = item.sla % 60;
        //       item.hoursSlaStep = Math.floor(item.sla / 60);
        //       item.daySlaStep = Math.floor(item.hoursSlaStep / 24);
        //     }
        //     if (isNumber(item.daySlaStep) && item.daySlaStep > 0) {
        //       item.hoursSlaStep = item.hoursSlaStep! % 24;
        //       item.status = `request.detailRequest.underwrite.${item.status}_DAY`;
        //     } else {
        //       item.status = `request.detailRequest.underwrite.${item.status}`;
        //     }
        //   }
        // });
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  doDetail(item: Claim) {
    /**
     * Chuyển hướng đến chi tiết claim TPA (lớn)
     * và chỉ hiển thị thông tin không hiển thị xử lý
     */
    this.loadingService.start();
    this.router
      .navigate(['detail', 'claim', 'tpa', item.requestId], { relativeTo: this.route })
      .then(() => this.loadingService.complete());
  }

  assign() {
    if (this.loadingService.loading || !this.currUser?.username || this.selectedItem.length === 0) {
      return;
    }
    if (
      this.authService.getUserRoles().includes(Roles.OP_CLAIM_ASSESSOR) &&
      !this.authService.getUserRoles().includes(Roles.OP_CLAIM_ADMIN)
    ) {
      const data: DataAssign = {
        requestIds: this.selectedItem.map((item) => item.requestId!),
        userName: this.currUser.username,
      };
      this.loadingService.start();
      this.service.assignRequest(data).subscribe({
        next: () => {
          this.selectedItem = [];
          this.table!.selectionKeys = {};
          this.search(false);
          this.messageService.success('MESSAGE.SUCCESS');
        },
        error: (e) => {
          this.loadingService.complete();
          this.messageService.error(createErrorMessage(e));
        },
      });
    } else {
      const dialog = this.dialogService?.open(ForwardClaimComponent, {
        header: 'forward user',
        showHeader: false,
        width: '40%',
        data: {
          screenType: ScreenType.Create,
          model: this.selectedItem.map((item) => item.requestId!),
          roleName: this.forwardConfig?.forwardTypeCodes?.find((item) => item.types.includes(this.requestType.TYPE11))
            ?.role,
        },
      });
      dialog?.onClose.subscribe({
        next: (isSuccess) => {
          if (isSuccess) {
            this.selectedItem = [];
            this.table!.selectionKeys = {};
            this.search();
          }
        },
      });
    }
  }

  showBtnAssign() {
    return (
      (this.authService.getUserRoles().includes(Roles.OP_CLAIM_ADMIN) ||
        this.authService.getUserRoles().includes(Roles.OP_CLAIM_ASSESSOR)) &&
      this.objFunction?.scopes?.includes(this.action.Forward)
    );
  }

  onReset() {
    this.params = {
      requestId: null,
      claimId: null,
      toDate: null,
      fromDate: null,
      typeRequest: null,
      requestSource: null,
      policyNumber: null,
      bp: null,
      pic: null,
      status: null,
      resolution: null,
      bpLA: null,
      claimStatus: null,
    };
    this.search();
  }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.search();
  }
}
