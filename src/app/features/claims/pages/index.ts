import { ClaimRequestComponent } from './claim-request/claim-request.component';
import { ClaimManagementComponent } from './claim-management/claim-management.component';

export const pages = [ClaimRequestComponent, ClaimManagementComponent];
