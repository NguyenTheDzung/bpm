import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClaimRequestComponent } from './pages/claim-request/claim-request.component';
import { ClaimManagementComponent } from './pages/claim-management/claim-management.component';
import { FunctionCode } from '@cores/utils/enums';

export const routes: Routes = [
  {
    path: 'requests',
    data: {
      functionCode: FunctionCode.ListClaim,
    },
    children: [
      {
        path: '',
        component: ClaimRequestComponent,
      },
      {
        path: 'detail/claim',
        loadChildren: () => import('@claim-detail/claim-tpa.module').then((m) => m.ClaimTPAModule),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
    ],
  },
  {
    path: 'claim-management',
    data: {
      functionCode: FunctionCode.ClaimManagement,
    },
    children: [
      {
        path: '',
        component: ClaimManagementComponent,
      },
      {
        path: 'detail/claim',
        loadChildren: () => import('@claim-detail/claim-tpa.module').then((m) => m.ClaimTPAModule),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
    ],
  },

  { path: '', redirectTo: 'requests' },
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClaimsRoutingModule {}
