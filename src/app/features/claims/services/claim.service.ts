import { Injectable } from '@angular/core';
import { BaseService } from '@cores/services/base.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { forkJoin, map, Observable } from 'rxjs';
import { Claim, DataAssign, SearchParams } from '../models/claim-request.model';
import { mapDataTableWithType, removeParamSearch } from '@cores/utils/functions';
import { IPageableResponseModel, IResponseModel } from '@cores/models/response.model';
import { DataTable } from '@cores/models/data-table.model';
import { CategoryClaimState, ClaimManage, ClaimManageParams } from '../models/claim-management.model';
import { CLAIM_SOURCE_REQUEST, ClaimCommonType, REQUEST_TYPE, Roles } from '@cores/utils/constants';
import { ClaimPending } from '@create-requests/models/claim.model';
import { CommonCategoryService } from '@cores/services/common-category.service';
import { User } from '@requests/models/request.model';

@Injectable({
  providedIn: 'root',
})
export class ClaimService extends BaseService {
  constructor(http: HttpClient, private commonService: CommonCategoryService) {
    super(http, `${environment.claim_url}/claim`);
  }

  override search(params: SearchParams, _isPost?: boolean): Observable<DataTable<Claim>> {
    return this.http
      .get<IResponseModel<IPageableResponseModel<Claim>>>(`${this.baseUrl}/requests`, {
        params: removeParamSearch(params),
      })
      .pipe(map((res) => mapDataTableWithType(res.data, params)));
  }

  searchManagement(params: ClaimManageParams): Observable<DataTable<ClaimManage>> {
    return this.http
      .get<IResponseModel<IPageableResponseModel<ClaimManage>>>(`${this.baseUrl}/manage`, {
        params: removeParamSearch(params),
      })
      .pipe(map((res) => mapDataTableWithType(res.data, params)));
  }

  assignRequest(data: DataAssign): Observable<IResponseModel> {
    return this.http.put<IResponseModel>(`${this.baseUrl}/assign`, data);
  }

  getPic(roles: string[]): Observable<User[]> {
    return this.http
      .get(`${environment.employee_url}/user/list-active-employee`, {
        params: { roleName: roles },
      })
      .pipe(map((res: any) => res.data));
  }

  submitPendingAttachment(data: ClaimPending): Observable<any> {
    return this.http.post(`${this.baseUrl}/pending-attachment`, data);
  }


  /**
   *
   * @param listPicRoles Chua thong tin ve role call api lay ra list pic
   */
  getCategories(listPicRoles = [Roles.OP_CLAIM_USER]): Observable<CategoryClaimState> {
    return forkJoin({
      listStatusClaim: this.commonService.getCommonClaimByTypes(ClaimCommonType.STATUS_CLAIM),
      listStatus: this.commonService.getCommonClaimByTypes(ClaimCommonType.STATUS_REQUEST),
      listResolution: this.commonService.getCommonClaimByTypes(ClaimCommonType.RESOLUTION_REQUEST),
      listSource: this.commonService.getCommonCategory(CLAIM_SOURCE_REQUEST),
      listRequestType: this.commonService.getCommonCategory(REQUEST_TYPE),
      listPic: this.getPic(listPicRoles),
    });
  }
}
