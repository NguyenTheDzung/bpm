import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { CommonModel } from '@common-category/models/common-category.model';
import { BaseComponent } from '@shared/components';
import { AttachmentFeeInfo, CollectionDetailRequestModel } from '@detail-request/shared/models/collection-detail.model';
import { RequestService } from '@requests/services/request.service';
import { Decisions, ScreenType } from '@cores/utils/enums';
import {
  cleanDataForm,
  createErrorMessage,
  getValueDateTimeLocal,
  removeParamSearch,
  updateValidity,
  validateAllFormFields,
} from '@cores/utils/functions';
import * as _ from 'lodash';
import { Validators } from '@angular/forms';
import { DataTable } from '@cores/models/data-table.model';
import { PaginatorModel } from '@cores/models/paginator.model';
import { StatusPaymentLot } from '@cores/utils/constants';
import { Table } from 'primeng/table';
import { ContractRestoreComponent } from '@create-requests/components';
import { TypeFee } from '@create-requests/utils/constants';
import { ConfirmSubmitFeePayComponent } from '../confirm-submit-fee-pay/confirm-submit-fee-pay.component';
import { SortEvent } from 'primeng/api';
import * as moment from 'moment';

@Component({
  selector: 'app-submit-fee-pay',
  templateUrl: './submit-fee-pay.component.html',
  styleUrls: ['./submit-fee-pay.component.scss'],
})
export class SubmitFeePayComponent extends BaseComponent implements OnChanges, OnInit {
  @ViewChild('dt1') table?: Table;
  @Input() itemRequest?: CollectionDetailRequestModel;
  @Input() decisionDropList: CommonModel[] = [];
  @Input() dailyNewAction!: CommonModel[];
  @Input() category!: CommonModel[];
  @Input() listDocumentType!: CommonModel[];
  @Input() sapIdentifyTypeCode!: CommonModel[];
  @Input() decisionContractStore!: CommonModel[];
  @Input() activeIndexSteps: number = 0;
  @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
  itemRequestStepOne?: CollectionDetailRequestModel;
  screenType: ScreenType | undefined;
  paymentDocDTOs: AttachmentFeeInfo[] = [];
  statusPaymentLot = StatusPaymentLot;
  paymentLotTable: DataTable = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  typeOther = TypeFee;
  paymentLotResults: AttachmentFeeInfo[] = [];
  collectionDecision = Decisions;
  isDisableLot: boolean = true;
  isAllocated: boolean = false;
  isCreateLot: boolean = false;
  totalAmount: number = 0;
  selectionItemLot: AttachmentFeeInfo[] = [];
  dateNotFuture = new Date();
  form = this.fb.group({
    decision: ['', Validators.required],
    reasonNote: null,
    requestId: null,
    paymentAttachmentAuthorization: false,
    paymentAttachment: false,
    cthd: null, //chi tiết hd
    paymentLots: [],
    isDistribution: false,
    isMultiAttInfo: false,
    attachmentFeeInfos: [],
    chronologies: [],
    paymentLotResults: [],
  });
  paymentLots: AttachmentFeeInfo[] = [];
  params: AttachmentFeeInfo = {
    postingDate: null,
    paymentAmount: null,
    searchTerm: null,
    postingFromDate: null,
    postingToDate: null,
  };
  today = new Date();
  isPaymentAmountGreaterRemainingAmount = false;
  formFilterChronology = this.fb.group({
    docNo: null,
    paymentLot: null,
    item: null,
    amount: null,
    netDueDate: null,
    postingDate: null,
    debit: null,
    origin: null,
    changedBy: null,
    changedOn: null,
    enteredAt: null,
    nameOfOrgin: null,
    creditMemo: null,
    cleared: null,
  });

  constructor(inject: Injector, private collectionService: RequestService) {
    super(inject);
  }

  get DescDocType() {
    if (this.itemRequestStepOne?.documentType === TypeFee.other) {
      return this.itemRequestStepOne?.documentTypeOther;
    }
    return this.collectionService.state?.listFeeVoucherType?.find(
      (item) => item.code === this.itemRequestStepOne?.documentType
    )?.multiLanguage[this.translateService.currentLang];
  }

  ngOnInit(): void {
    if (!this.form.get('paymentAttachment')?.value) {
      this.requiredDecision(true);
    }

    this.form?.controls['paymentAttachment'].valueChanges.subscribe((data) => {
      if (!data) {
        this.requiredDecision(true);
        this.form.get('decision')?.setValue(null);
      } else {
        this.requiredDecision(false);
      }
      this.decisionDropList = [...this.decisionDropList];
    });
    this.form?.controls['decision'].valueChanges.subscribe((decision) => {
      if (decision === Decisions.Pending || decision === Decisions.Reject) {
        updateValidity(this.form?.get('reasonNote'), [Validators.required, Validators.maxLength(1000)]);
      } else {
        updateValidity(this.form?.get('reasonNote'), null);
      }
    });
  }

  checkPaymentAttachment(event: any) {
    this.form.get('paymentAttachment')?.setValue(event.checked);
  }

  requiredDecision(value: boolean) {
    this.decisionDropList?.forEach((item) => {
      if (item.code === Decisions.Approved) {
        item.disabled = value;
      }
    });
  }

  addNewLine() {
    this.selectionItemLot.push({
      isAllocated: false,
      attachmentType: null,
      numberOfPremiums: null,
      paymentReceiveDate: '',
      paymentInfo: '',
      paymentLot: '',
      item: '',
      requestId: this.itemRequestStepOne?.requestId,
      isEdit: false,
      isChosen: false,
      processInfo: '',
      attachmentTypeOther: '',
      orderNum: undefined,
      statusPaymentLotItem: '',
      disabled: true,
    });
  }

  deleteLine(item: AttachmentFeeInfo) {
    this.selectionItemLot = this.selectionItemLot.filter((x) => x !== item)!;
    this.isCreateLot =
      this.selectionItemLot.findIndex((x) => !x.disabled && x.status === StatusPaymentLot.REVERSED) > -1;
  }

  editLine(item: AttachmentFeeInfo) {
    this.selectionItemLot.forEach((val) => {
      if (val !== item) {
        val.disabled = true;
        val.isEdit = false;
        val.isChosen = false;
      }
    });
    item.isEdit = true;
    if (item.isEdit) {
      item.disabled = false;
      item.isChosen = true;
    }
    this.isAllocated = !!item.isAllocated;
    this.isDisableLot = !!item.isAllocated;
    this.isCreateLot = item.status === StatusPaymentLot.REVERSED && !item.isCreatePaymentLot;
  }

  ngOnChanges(_changes: SimpleChanges): void {
    if (_changes['itemRequest']) {
      this.itemRequestStepOne = this.itemRequest?.step1;
      //payment lot
      this.paymentLots = _.clone(this.itemRequestStepOne?.paymentLots) || [];
      this.paymentLots?.forEach((item) => {
        item.postingDate = moment(item.postingDate).format('DD/MM/YYYY');
      });
      this.itemRequestStepOne?.attachmentFeeInfoDTOS?.forEach((x) => {
        x.paymentReceiveDate = !_.isEmpty(x.paymentReceiveDate) ? new Date(x.paymentReceiveDate!) : '';
        x.netDueDate = x.netDueDate?.split('-').reverse().join('/');
        x.disabled = true;
        x.checked = true;
      });
      this.selectionItemLot = _.sortBy(this.itemRequestStepOne?.attachmentFeeInfoDTOS, 'orderNum');
      this.paymentLotResults = this.itemRequestStepOne?.paymentLotResults || [];
      if (this.selectionItemLot.length === 1 && !this.selectionItemLot[0].isProcessSuccess) {
        this.selectionItemLot[0].disabled = false;
        this.selectionItemLot[0].isEdit = true;
        this.selectionItemLot[0].isChosen = true;
        this.isDisableLot = this.selectionItemLot[0].isAllocated!;
        this.isAllocated = this.selectionItemLot[0].isAllocated!;
      }
      const dataForm = {
        reasonNote: this.itemRequestStepOne?.reasonNote,
        requestId: this.itemRequestStepOne?.requestId,
        paymentAttachmentAuthorization: this.itemRequestStepOne?.paymentAttachmentAuthorization,
        paymentAttachment: this.itemRequestStepOne?.paymentAttachment,
        isMultiAttInfo: this.itemRequestStepOne?.isMultiAttInfo,
        attachmentFeeInfos: this.itemRequestStepOne?.attachmentFeeInfoDTOS,
        chronologies: [],
        decision: this.itemRequestStepOne?.decision,
      };
      this.form.patchValue(dataForm);
    }
  }

  clickIsMultiAttInfo(checked: boolean) {
    if (checked && this.selectionItemLot?.length <= 1) {
      this.addNewLine();
    }
  }

  hasIconDelete() {
    return (
      (!this.form.get('isMultiAttInfo')?.value && this.selectionItemLot.length > 1) ||
      (this.form.get('isMultiAttInfo')?.value && this.selectionItemLot.length > 2)
    );
  }

  clearChrolonogy() {
    this.formFilterChronology?.reset();
    this.table?.clear();
    this.table?.reset();
  }

  getPaymentDoc() {
    //chronology
    if (this.paymentDocDTOs.length === 0) {
      this.loadingService.start();
      this.collectionService.getChronological(this.itemRequestStepOne?.requestId!).subscribe({
        next: (res) => {
          this.loadingService.complete();
          this.paymentDocDTOs = res?.data?.map((item: any) => {
            return {
              ...item,
              changedOn: item.changedOn?.split('-').reverse().join('/'),
              postingDate: item.postingDate?.split('-').reverse().join('/'),
              netDueDate: item.netDueDate?.split('-').reverse().join('/'),
            };
          });
        },
        error: () => {
          this.loadingService.complete();
        },
      });
    } else {
      this.paymentDocDTOs = [];
    }
  }

  selectionPaymentLotItem(data: AttachmentFeeInfo, checked: boolean, isChronology?: boolean) {
    this.isCreateLot = checked && data.status === StatusPaymentLot.REVERSED && !this.isDisableLot;
    const index = this.selectionItemLot.findIndex((x) => !x.disabled);
    if (!isChronology) {
      if (!checked) {
        this.paymentLots = this.paymentLots.filter(
          (item) => !(item.item === data.item && item.paymentLot === data.paymentLot)
        );
      } else if (!this.paymentLots.find((item) => item.item === data.item && item.paymentLot === data.paymentLot)) {
        this.paymentLots = this.paymentLots.filter(
          (item) =>
            !(
              item.item === this.selectionItemLot[index]?.item &&
              item.paymentLot === this.selectionItemLot[index]?.paymentLot
            )
        );
        this.paymentLots.push({ ...data, checked });
      }
    }
    if (index !== -1) {
      if (checked) {
        let numberOfPremiums: number | null;
        if (isChronology) {
          numberOfPremiums = +data.amount! < 0 ? data.amount! * -1 : data.amount!;
        } else if (data.status === StatusPaymentLot.REVERSED) {
          numberOfPremiums = data.paymentAmount!;
        } else {
          numberOfPremiums = this.selectionItemLot[index].numberOfPremiums || null;
        }

        this.selectionItemLot[index] = {
          ...this.selectionItemLot[index],
          ...data,
          paymentReceiveDate: new Date((<string>data.postingDate)?.split('/').reverse()?.join('-')),
          dailyNote: this.itemRequestStepOne?.policyNumber,
          paymentInfo: isChronology ? data.txtVw : data.usageText,
          checked: true,
          isAllocated: isChronology,
          status: data.status,
          amount: data.amount,
          numberOfPremiums: numberOfPremiums,
        };
      } else {
        const item = this.selectionItemLot[index];
        this.selectionItemLot[index] = {
          isAllocated: item.isAllocated,
          attachmentType: item.attachmentType,
          attachmentTypeOther: item.attachmentTypeOther,
          numberOfPremiums: item.numberOfPremiums,
          paymentReceiveDate: item.paymentReceiveDate,
          isEdit: true,
          isChosen: true,
          disabled: false,
        };
        this.isAllocated = !!item.isAllocated;
        this.isDisableLot = !!item.isAllocated;
      }
    }
  }

  filterPaymentLotSelection(item: any, itemSelection: any, isChronology?: boolean) {
    if (isChronology) {
      return (
        item.paymentLot === itemSelection.paymentLot &&
        item.item === itemSelection.item &&
        item.docNo === itemSelection.docNo &&
        itemSelection.netDueDate &&
        itemSelection.netDueDate === item.netDueDate
      );
    } else {
      return item.paymentLot === itemSelection.paymentLot && item.item === itemSelection.item;
    }
  }

  //tích vào phân bổ sẽ lấy lot vs item ở chornolory ,chưa tích lấy api payment lot fill
  checkAllocated(isCheck: boolean, item: AttachmentFeeInfo) {
    this.isAllocated = isCheck;
    this.isDisableLot = isCheck;
    item.item = undefined;
    item.paymentLot = undefined;
    item.paymentInfo = undefined;
    item.isAllocated = isCheck;
    item.status = undefined;
  }

  hasPaymentLotItem(data: AttachmentFeeInfo) {
    return !_.isEmpty(data.item) && !_.isEmpty(data.paymentLot);
  }

  getDataTableLot(firstPage?: boolean) {
    if (this.loadingService.loading || !!this.itemRequestStepOne?.isSubmit) {
      return;
    }
    if (_.isNull(this.params.postingFromDate) && !_.isNull(this.params.postingToDate)) {
      this.params.postingFromDate = moment(this.params.postingToDate).add(-14, 'day').toDate();
    }
    if (!_.isNull(this.params.postingFromDate) && _.isNull(this.params.postingToDate)) {
      this.params.postingToDate = moment(this.params.postingFromDate).add(14, 'day').toDate();
    }
    if (moment(this.params.postingFromDate).isAfter(this.params.postingToDate)) {
      return this.messageService.warn('MESSAGE.FROM_DATE_NOT_MORE_THAN_TO_DATE');
    }
    if (moment(this.params.postingToDate).diff(moment(this.params.postingFromDate), 'day') > 14) {
      return this.messageService.warn(
        this.translateService.instant('MESSAGE.PERIOD_TWO_DATE_NOT_MORE_THAN_DAYS', { days: 14 })
      );
    }
    if (
      !_.isEmpty(this.params.searchTerm) &&
      _.isNull(this.params.postingFromDate) &&
      _.isNull(this.params.postingToDate) &&
      !_.isNumber(this.params.paymentAmount)
    ) {
      return this.messageService.warn('MESSAGE.FILL_PAYMENT_AMOUNT_OR_POSTING_DATE');
    }
    if (firstPage) {
      this.paymentLotTable.currentPage = 0;
    }
    const params = {
      ...this.params,
      postingDate: getValueDateTimeLocal(this.params.postingDate),
      postingFromDate: getValueDateTimeLocal(this.params.postingFromDate),
      postingToDate: getValueDateTimeLocal(this.params.postingToDate),
      requestId: this.itemRequestStepOne?.requestId,
      page: this.paymentLotTable.currentPage,
      size: this.paymentLotTable.size,
    };
    this.loadingService.start();
    this.collectionService.getLot(params).subscribe({
      next: (data) => {
        this.paymentLotTable = data;
        if (this.paymentLotTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        //
        this.paymentLotTable.content?.forEach((tb) => {
          tb.dailyNextAction = 'Auto allocate(Periodic)';
          tb.postingDate = tb.postingDate?.split('-').reverse().join('/');
        });

        this.loadingService.complete();
      },
      error: () => {
        this.loadingService.complete();
      },
    });
  }

  pageChange(paginator: PaginatorModel) {
    this.paymentLotTable.currentPage = paginator.page;
    this.paymentLotTable.size = paginator.rows;
    this.paymentLotTable.first = paginator.first;
    this.getDataTableLot();
  }

  checkValueNumber(value: any) {
    return _.isNumber(+value);
  }

  checkPriceDifference() {
    const dataForm = this.form.getRawValue();
    const listPaymentGreaterThanRemaining = this.selectionItemLot.filter(
      (item) =>
        !item.isAllocated &&
        !item.isProcessSuccess &&
        _.isNumber(+item.remainingAmount!) &&
        +item.remainingAmount! < item.numberOfPremiums!
    );
    const listPaymentLessThanRemaining = this.selectionItemLot.filter(
      (item) =>
        !item.isAllocated &&
        !item.isProcessSuccess &&
        _.isNumber(+item.remainingAmount!) &&
        +item.remainingAmount! > item.numberOfPremiums!
    );
    if (dataForm.decision === Decisions.Approved) {
      this.totalAmount = 0;
      this.selectionItemLot.forEach((item) => {
        this.totalAmount += +(item.numberOfPremiums || 0);
      });

      this.isPaymentAmountGreaterRemainingAmount = listPaymentGreaterThanRemaining.length > 0;

      if (listPaymentGreaterThanRemaining.length > 0) {
        return this.messageService.error('MESSAGE.PAYMENT_AMOUNT_GREATER_THAN_REMAINING_AMOUNT');
      }
      if (
        this.selectionItemLot?.find(
          (item) => item.status === StatusPaymentLot.REVERSED && item.isProcessSuccess === undefined
        )
      ) {
        return this.messageService.error('MESSAGE.TRANS_HAS_NOT_CREATE_NEW_PAYMENT_LOT');
      }
      if (
        (+this.itemRequestStepOne?.amount! !== this.totalAmount || listPaymentLessThanRemaining.length > 0) &&
        this.form.get('decision')?.value === Decisions.Approved
      ) {
        return this.showConfirm(listPaymentLessThanRemaining);
      }
    }
    this.submit();
  }

  showConfirm(list: AttachmentFeeInfo[]) {
    const dialog = this.dialogService?.open(ConfirmSubmitFeePayComponent, {
      showHeader: false,
      width: '40%',
      data: {
        screenType: ScreenType.Create,
        state: {
          totalAmount: this.totalAmount,
          amount: +this.itemRequestStepOne?.amount!,
          listItem: list,
        },
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.submit();
        }
      },
    });
  }

  submit() {
    if (this.loadingService.loading || this.form.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    const data = this.getDataActionSubmitOrSave();
    this.loadingService.start();
    this.collectionService.saveOrSubmitFeeAttachmentRequest(data, true).subscribe({
      next: () => {
        this.isPaymentAmountGreaterRemainingAmount = false;
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        if ([Decisions.Pending, Decisions.Reject].includes(data?.decision)) {
          return this.back();
        }
        this.refreshData.emit(true);
        window.scrollTo(0, 500);
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  save() {
    this.loadingService.start();
    this.collectionService.saveOrSubmitFeeAttachmentRequest(this.getDataActionSubmitOrSave()).subscribe({
      next: () => {
        this.refreshData.emit(true);
        // this.getPaymentDoc(); //call api này
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  getDataActionSubmitOrSave() {
    const data = cleanDataForm(this.form);
    data.paymentLots = _.clone(this.paymentLots);
    data.paymentLots?.forEach((item: AttachmentFeeInfo) => {
      item.postingDate = item.postingDate?.split('/').reverse().join('-');
    });
    data.attachmentFeeInfos = _.cloneDeep(this.selectionItemLot);
    data.attachmentFeeInfos?.forEach((item: AttachmentFeeInfo, i: number) => {
      item.orderNum = i;
      item.paymentReceiveDate = getValueDateTimeLocal(item.paymentReceiveDate)!;
      item.postingDate = item.postingDate?.split('/').reverse().join('-');
      item.netDueDate = item.netDueDate?.split('/').reverse().join('-');
    });
    data.chronologies;
    return data;
  }

  processing() {
    if (this.isCreateLot && this.selectionItemLot?.every((item) => !item.isChosen)) {
      return this.messageService.error('MESSAGE.NOT_SELECT_ANY_LINE_YET');
    }
    const data = this.getDataActionSubmitOrSave();
    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        this.collectionService.updateProcess(data).subscribe({
          next: () => {
            this.isCreateLot = false;
            this.isDisableLot = true;
            this.isAllocated = true;
            this.paymentLotTable = {
              content: [],
              currentPage: 0,
              size: 10,
              totalElements: 0,
              totalPages: 0,
              first: 0,
              numberOfElements: 0,
            };
            this.params = {
              paymentAmount: null,
              postingDate: null,
              searchTerm: null,
              postingFromDate: null,
              postingToDate: null,
            };
            this.refreshData.emit(true);
            this.messageService.success('MESSAGE.UPDATE_SUCCESS');
          },
          error: (err) => {
            this.refreshData.emit(true);
            this.messageService.error(createErrorMessage(err));
          },
        });
      }
    });
  }

  typeSapIdentify() {
    return this.sapIdentifyTypeCode?.find((item) => item.code === this.itemRequestStepOne?.idType)?.multiLanguage[
      this.translateService.currentLang
    ];
  }

  viewRestorePolicy() {
    const dialog = this.dialogService?.open(ContractRestoreComponent, {
      showHeader: false,
      width: '85%',
      styleClass: 'dialog-contract-restore',
      data: {
        screenType: ScreenType.Create,
        state: this.itemRequestStepOne,
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.refreshData.emit(true);
        }
      },
    });
  }

  disableSubmit() {
    return (
      (this.isCreateLot && this.form.get('decision')?.value === Decisions.Approved) || this.itemRequestStepOne?.isSubmit
    );
  }

  getItemLot(itemLot: any, isChronology?: boolean) {
    return (
      this.selectionItemLot?.find((itemSelect) => this.filterPaymentLotSelection(itemLot, itemSelect, isChronology)) ||
      {}
    );
  }

  showAttachmentTypeOther() {
    return this.selectionItemLot.filter((x) => x.attachmentType === this.typeOther.other)?.length > 0;
  }

  disableDecision() {
    return this.selectionItemLot.filter((x) => x.isProcessSuccess)?.length > 0 || !!this.itemRequestStepOne?.isSubmit;
  }

  getItemLotSubmitted(item: AttachmentFeeInfo) {
    return (
      this.itemRequestStepOne?.paymentLots?.find((o) => o.paymentLot === item.paymentLot && o.item === item.item) ||
      item
    );
  }

  customSort(event: SortEvent) {
    event!.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result: any;

      if (value1 == null && value2 != null) result = -1;
      else if (value1 != null && value2 == null) result = 1;
      else if (value1 == null && value2 == null) result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string') result = value1.localeCompare(value2);
      else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

      return event.order! * result;
    });
  }

  isRestore() {
    return (
      this.currUser?.username === this.itemRequestStepOne?.pic &&
      this.itemRequestStepOne?.isSubmit &&
      this.itemRequestStepOne?.decision === Decisions.Approved
    );
  }

  hasParamsPaymentLot() {
    return _.isEmpty(removeParamSearch(this.params));
  }

  isBoolean(value: Boolean) {
    return _.isBoolean(value);
  }

  override onDestroy() {
    this.collectionService.stop();
  }
}
