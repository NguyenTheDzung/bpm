import {
  SubmitFeePayComponent
} from '@detail-request/submit-payment-receipt/components/submit-fee-pay/submit-fee-pay.component';
import { ConfirmSubmitFeePayComponent } from './confirm-submit-fee-pay/confirm-submit-fee-pay.component';

export const components = [SubmitFeePayComponent, ConfirmSubmitFeePayComponent];
