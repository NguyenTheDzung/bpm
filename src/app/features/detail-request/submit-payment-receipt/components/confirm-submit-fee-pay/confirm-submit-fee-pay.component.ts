import { Component, Injector, OnInit } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { RequestService } from '@requests/services/request.service';
import { AttachmentFeeInfo } from '../../../shared/models/collection-detail.model';

@Component({
  selector: 'app-confirm-submit-fee-pay',
  templateUrl: './confirm-submit-fee-pay.component.html',
  styleUrls: ['./confirm-submit-fee-pay.component.scss'],
})
export class ConfirmSubmitFeePayComponent extends BaseActionComponent implements OnInit {
  constructor(inject: Injector, private collectionService: RequestService) {
    super(inject, collectionService);
  }

  totalAmount: number = 0;
  amount: number = 0;
  list: AttachmentFeeInfo[] = [];

  ngOnInit(): void {
    this.totalAmount = this.configDialog.data.state.totalAmount;
    this.amount = this.configDialog.data.state.amount;
    this.list = this.configDialog.data.state.listItem;
  }

  override save() {
    this.refDialog.close(true);
  }
}
