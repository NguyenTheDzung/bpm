import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubmitPaymentReceiptRoutingModule } from './submit-payment-receipt-routing.module';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';
import { pages } from './pages';
import { DetailRequestSharedModule } from '../shared/detail-request-shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, SubmitPaymentReceiptRoutingModule, DetailRequestSharedModule],
  declarations: [...components, ...pages],
})
export class SubmitPaymentReceiptModule {}
