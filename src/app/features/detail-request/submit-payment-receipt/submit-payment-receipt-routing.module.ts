import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubmitPaymentReceiptDetailComponent } from './pages/submit-payment-receipt-detail/submit-payment-receipt-detail.component';

const routes: Routes = [{ path: '', component: SubmitPaymentReceiptDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubmitPaymentReceiptRoutingModule {}
