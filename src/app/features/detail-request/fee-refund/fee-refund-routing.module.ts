import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeeRefundDetailComponent } from './pages/fee-refund-detail/fee-refund-detail.component';

const routes: Routes = [{ path: '', component: FeeRefundDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeeRefundRoutingModule {}
