import { CollectionDetailRequestModel } from '@detail-request/shared/models/collection-detail.model';
import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { CommonModel } from '@common-category/models/common-category.model';
import { RefundService } from '@create-requests/service/refund.service';
import { Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-payment-information',
  templateUrl: './payment-information.component.html',
  styleUrls: ['./payment-information.component.scss'],
})
export class PaymentInformationComponent extends BaseComponent {
  status?: string;
  date?: string;
  form = this.fb.group({
    requestCode: null,
    decision: [null, Validators.required],
    approverComment: [null, Validators.required],
  });
  @Input() itemRequest?: CollectionDetailRequestModel;
  @Input() decisionDropList: CommonModel[] = [];
  @Input() listGender: CommonModel[] = [];
  @Input() listIdentifyType: CommonModel[] = [];
  @Input() requestTypeCode?: string;
  @Output() refreshData: EventEmitter<boolean> = new EventEmitter();

  constructor(injector: Injector, private service: RefundService, private translate: TranslateService) {
    super(injector);
  }

  get paymentMethodDesc() {
    switch (this.itemRequest?.originPaymentMethod?.toLowerCase()) {
      case 'banking':
        return 'request.detailRequest.underwrite.banking';
      case 'cash':
        return 'request.detailRequest.underwrite.cash';
      default:
        return '';
    }
  }

  get genderName() {
    return this.listGender?.find((item) => item.code === this.itemRequest?.gender)?.multiLanguage[
      this.translate.currentLang
    ];
  }

  get IdentifyTypeDesc() {
    return this.listIdentifyType?.find((item) => item.code === this.itemRequest?.idType)?.multiLanguage[
      this.translate.currentLang
    ];
  }
}
