import {
  BankDataDTO,
  BpDetailDTO,
  CollectionDetailRequestModel,
  PaymentRequestDetailDTO,
} from '@detail-request/shared/models/collection-detail.model';
import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, NgForm, Validators } from '@angular/forms';
import { CommonModel } from '@common-category/models/common-category.model';
import { Decisions, PaymentMethodCode, ScreenType } from '@cores/utils/enums';
import {
  cleanDataForm,
  createErrorMessage,
  dataURItoBlob,
  getValueDateTimeLocal,
  updateValidity,
  validateAllFormFields,
} from '@cores/utils/functions';
import { BaseComponent } from '@shared/components';
// import * as _ from 'lodash';
import { Bank, PaymentLotModel } from '@detail-request/shared/models/detail-request.model';
import { RequestService } from '@requests/services/request.service';
import { AddBankAccountComponent } from '@detail-request/shared/components/add-bank-account/add-bank-account.component';
import { SendQMComponent } from '../send-qm/send-qm.component';
import { IdentificationType, Pattern, PaymentMethod, Resolution } from '@cores/utils/constants';
import { FeeRefundDetailComponent } from '../../pages/fee-refund-detail/fee-refund-detail.component';
import { cloneDeep, isArray, isEmpty, isNumber, remove, size, toLower } from 'lodash';

@Component({
  selector: 'app-collection-decision',
  templateUrl: './collection-decision.component.html',
  styleUrls: ['./collection-decision.component.scss'],
})
export class CollectionDecisionComponent extends BaseComponent implements OnChanges, OnInit {
  @Input() itemRequest?: CollectionDetailRequestModel;
  @Input() form?: FormGroup;
  paymentMethod = PaymentMethodCode;
  decision = Decisions;
  dataPaymentLot: PaymentLotModel[] = []; //
  dataNewPaymentLot: PaymentLotModel[] = [];
  isScroll?: boolean;
  resolution = Resolution;
  isCheckedFPCPL = false;
  disabledPaymentLot = ['Repayment', 'Completed', 'In Process'];
  @Input() bankDropList: Bank[] = [];
  @Input() reasonDropList: CommonModel[] = [];
  @Input() subReasonDropList: CommonModel[] = [];
  @Input() pendingReasonDropList: CommonModel[] = [];
  bankDataDTO: BankDataDTO[] = [];
  @Input() decisionDropList: CommonModel[] = [];
  @Input() rejectReasonDropList: CommonModel[] = [];
  @Input() additionalAttachmentsDropList: CommonModel[] = [];
  @Input() approvedDropList: CommonModel[] = [];
  @Input() listIdentifyType: CommonModel[] = [];
  @Input() listGender: CommonModel[] = [];
  @Input() paymentMethodList: CommonModel[] = [];
  formBP = this.fb.group({
    customerName: [null, Validators.required],
    idType: [null, Validators.required],
    idNumber: [null, Validators.required],
    dateOfIssue: [null, Validators.required],
    institute: [null, Validators.required],
    gender: [null, Validators.required],
    bankName: null,
    bankCode: null,
    bankAccount: null,
    branch: null,
  });

  paramPaymentLot = {
    paymentAmount: <number | null>null,
    usageText: null,
    postingDate: null,
  };

  formPaymentMethod = this.fb.group({
    id: null,
    customerName: null,
    bankName: null,
    cashBankName: null,
    cashBankCode: null,
    bankCode: null,
    branch: null,
    placeOfIssue: null,
    dateOfIssue: null,
    idNumber: null,
    bankAccountName: null,
    bankAccount: null,
    bpBankId: null,
  });
  configSendQMPopup = {
    component: SendQMComponent,
    title: 'title',
    dialog: {
      width: '30%',
    },
  };
  showBP = false;
  isProcessing = false;
  dataPaymentRequestDetail: any[] = [];
  isSaveChangePaymentMethod = true;
  paymentRequestDetail = [
    {
      label: 'customerName',
      key: 'customerName',
      disabled: true,
      order: 1,
    },
    {
      label: 'idNumber',
      key: 'idNumber',
      disabled: true,
      order: 2,
    },
    {
      label: 'dateOfIssue',
      key: 'dateOfIssue',
      type: 'Date',
      disabled: true,
      order: 3,
    },
    {
      label: 'institute',
      key: 'institute',
      disabled: true,
      order: 4,
    },
    {
      label: 'idType',
      key: 'idType',
      disabled: true,
      order: 5,
      type: 'Select',
    },
    {
      label: 'amount',
      key: 'amountList',
      type: 'Number',
      disabled: false,
      order: 6,
    },
    {
      label: 'bpNumber',
      key: 'bpNumber',
      disabled: true,
      order: 7,
    },
    {
      label: 'paymentMethod',
      key: 'paymentMethod',
      disabled: true,
      order: 8,
      type: 'Select',
    },
    {
      label: 'bankName',
      key: 'bankName',
      disabled: true,
      order: 9,
    },
    {
      label: 'bankAccount',
      key: 'bankAccount',
      disabled: true,
      order: 10,
    },
    {
      label: 'branch',
      key: 'branch',
      disabled: false,
      order: 11,
    },
    {
      label: 'reason',
      key: 'reasonList',
      disabled: false,
      order: 12,
    },
    {
      label: 'description',
      key: 'descriptionList',
      disabled: false,
      order: 13,
    },
    {
      label: 'note',
      key: 'noteList',
      disabled: false,
      order: 14,
    },
    {
      label: 'documentNumber',
      key: 'documentNumberList',
      disabled: true,
      order: 15,
    },
    {
      label: 'repaymentNumber',
      key: 'repaymentNumberList',
      disabled: true,
      order: 16,
    },
  ];
  today = new Date();

  constructor(inject: Injector, private service: RequestService, private detailRequest: FeeRefundDetailComponent) {
    super(inject);
    this.validateBP();
  }

  validateBP() {
    this.formBP.get('idType')?.valueChanges.subscribe((value) => {
      if (value === IdentificationType.CCCD || value === IdentificationType.CMND) {
        updateValidity(this.formBP.get('idNumber'), [Validators.required, Validators.pattern(Pattern.IDENTIFY_CARD)]);
      } else if (value === IdentificationType.PASSPORT) {
        updateValidity(this.formBP.get('idNumber'), [Validators.required, Validators.pattern(Pattern.PASSPORT)]);
      } else if (value === IdentificationType.GPLX) {
        updateValidity(this.formBP.get('idNumber'), [Validators.required, Validators.pattern(Pattern.DRIVING_LICENSE)]);
      } else {
        updateValidity(this.formBP.get('idNumber'), Validators.required);
      }
    });
  }

  onChangeBank(value: any) {
    const bank = this.bankDataDTO.find((bank: BankDataDTO) => bank.bpBankId === value);
    const dataForm = {
      bankCode: bank?.bankCode,
      bankName: bank?.bankName,
      bankAccount: bank?.bankAccount,
      bankAccountName: this.formBP.get('customerName')?.value,
      bpBankId: bank?.bpBankId,
    };
    this.formPaymentMethod.patchValue(dataForm);
  }

  ngOnInit(): void {
    this.form?.get('reason')?.setValue(this.itemRequest?.reason);
    this.form?.get('note')?.setValue(this.itemRequest?.note);
    this.checkHasRefundRequestProcessingList();
    this.formPaymentMethod.get('cashBankCode')?.valueChanges.subscribe((value) => {
      const item = this.bankDropList.find((o) => o.citadCode === value);
      this.formPaymentMethod.get('cashBankName')?.setValue(item?.name);
    });
    this.paramPaymentLot.paymentAmount = this.itemRequest?.refundAmount ? +this.itemRequest?.refundAmount : null;
  }

  onChangePaymentMethod() {
    const paymentMethod = this.form?.get('paymentMethod')?.value;
    if (paymentMethod?.toUpperCase() === PaymentMethodCode.Banking?.toUpperCase()) {
      updateValidity(this.formPaymentMethod.get('bpBankId'), Validators.required);
      updateValidity(this.formPaymentMethod.get('customerName'), null);
      updateValidity(this.formPaymentMethod.get('cashBankCode'), null);
      updateValidity(this.formPaymentMethod.get('branch'), null);
      updateValidity(this.formPaymentMethod.get('idNumber'), null);
      updateValidity(this.formPaymentMethod.get('placeOfIssue'), null);
      updateValidity(this.formPaymentMethod.get('dateOfIssue'), null);
    } else {
      updateValidity(this.formPaymentMethod.get('customerName'), Validators.required);
      updateValidity(this.formPaymentMethod.get('cashBankCode'), Validators.required);
      updateValidity(this.formPaymentMethod.get('branch'), Validators.required);
      updateValidity(this.formPaymentMethod.get('idNumber'), Validators.required);
      updateValidity(this.formPaymentMethod.get('placeOfIssue'), Validators.required);
      updateValidity(this.formPaymentMethod.get('dateOfIssue'), Validators.required);
      updateValidity(this.formPaymentMethod.get('bpBankId'), null);
    }
  }

  checkHasRefundRequestProcessingList() {
    if (this.itemRequest?.refundRequestProcessingList?.length > 0) {
      this.checkFPCPL();
      this.isProcessing = true;
      this.dataNewPaymentLot = this.itemRequest?.refundRequestProcessingList || [];
    }
  }

  ngOnChanges(_changes: SimpleChanges): void {
    if (_changes['itemRequest']) {
      this.dataNewPaymentLot = this.itemRequest?.refundRequestProcessingList ?? [];
      this.bankDataDTO = this.itemRequest?.paymentMethodDTO?.bankDataDTO ?? [];
      if (this.itemRequest?.bpDetailDTO) {
        this.formBP.patchValue(this.itemRequest.bpDetailDTO);
        this.formBP.disable();
      } else {
        const data = {
          customerName: this.itemRequest?.customerName,
          idType: this.itemRequest?.idType,
          idNumber: this.itemRequest?.idNo,
          dateOfIssue: this.itemRequest?.dateOfIssue,
          institute: this.itemRequest?.placeOfIssue,
          gender: this.itemRequest?.gender,
          bankCode: this.itemRequest?.bankCode,
          bankName: this.itemRequest?.bankCode,
          bankAccount: this.itemRequest?.beneficiaryAccount,
          branch: this.itemRequest?.branch,
        };
        this.formBP.patchValue(data);
      }
      if (this.itemRequest?.isSubmit || this.itemRequest?.isProcessing) {
        this.formPaymentMethod.disable();
        this.form?.get('paymentMethod')?.disable();
        this.checkHasRefundRequestProcessingList();
      }
      if (this.itemRequest?.paymentMethodDTO?.requestId) {
        this.formPaymentMethod.patchValue(this.itemRequest.paymentMethodDTO);
      } else {
        this.fillDataPaymentMethod();
      }
      if (this.itemRequest?.isPaymentRun) {
        this.formBP.disable();
        this.formPaymentMethod.disable();
      }
      if (this.itemRequest?.paymentRequestHistoryList) {
        let configDisabled: any[] = [];
        this.paymentRequestDetail?.forEach((item) => {
          configDisabled.push({
            ...item,
            disabled: true,
            required: false,
          });
        });

        this.itemRequest?.paymentRequestHistoryList?.forEach((itemHis) => {
          itemHis.dataTable = [];
          this.getDataTablePaymentRequestDetail(itemHis.dataTable, itemHis, configDisabled);
        });
      }
      this.dataPaymentRequestDetail = [];
      if (
        this.itemRequest?.resolution === Resolution.NBU_INPROCESS &&
        !!this.itemRequest?.approveList?.find((item) => item.ActionDescription === Decisions.Reject)
      ) {
        this.isSaveChangePaymentMethod = false;
        this.form?.get('decision')?.enable();
        this.form?.get('reason')?.enable();
        this.form?.get('note')?.enable();
        this.form?.get('paymentMethod')?.enable();
        this.formPaymentMethod.enable();
      }
      this.getDataTablePaymentRequestDetail(this.dataPaymentRequestDetail, this.itemRequest, this.paymentRequestDetail);
      this.onChangePaymentMethod();
    }
  }

  fillDataPaymentMethod() {
    const bank = this.bankDataDTO?.find((item) => item.bpBankId === this.itemRequest?.paymentMethodDTO?.bpBankId);
    const dataPaymentMethod = {
      customerName: this.itemRequest?.bpDetailDTO?.customerName,
      cashBankName: this.itemRequest?.bpDetailDTO?.bankName,
      cashBankCode: this.itemRequest?.bpDetailDTO?.bankCode,
      bankCode: this.itemRequest?.bpDetailDTO?.bankCode,
      branch: this.itemRequest?.bpDetailDTO?.branch,
      placeOfIssue: this.itemRequest?.bpDetailDTO?.institute,
      dateOfIssue: this.itemRequest?.bpDetailDTO?.dateOfIssue,
      idNumber: this.itemRequest?.bpDetailDTO?.idNumber,
      bankAccountName: this.itemRequest?.bpDetailDTO?.customerName,
      bankAccount: bank?.bankAccount,
      bankName: bank?.bankName,
      bpBankId: bank?.bpBankId,
    };
    this.formPaymentMethod?.patchValue(dataPaymentMethod);
  }

  getDataTablePaymentRequestDetail(result: any[], paymentDTO: any, config: any[]) {
    if (paymentDTO?.bpmPaymentRequestDetailDTO) {
      config.forEach((item) => {
        if (isArray(this.getValue(paymentDTO?.bpmPaymentRequestDetailDTO, item.key))) {
          for (let i = 0; i < this.getValue(paymentDTO?.bpmPaymentRequestDetailDTO, item.key).length; i++) {
            const p2pValue = this.getValue(paymentDTO?.p2pPaymentRequestDetailDTO, `${item.key}[${i}]`);
            // required ghi chú tương ứng nếu paymentlot là reversed
            const requiredNote =
              item.key === 'noteList' &&
              !!(this.getValue(paymentDTO?.p2pPaymentRequestDetailDTO, `lotItemConcat[${i}]`) as string)?.startsWith(
                'Reversed'
              );
            result.push({
              label: item.label,
              bpmValue: this.getValue(paymentDTO?.bpmPaymentRequestDetailDTO, `${item.key}[${i}]`),
              sapValue: this.getValue(paymentDTO?.sapPaymentRequestDetailDTO, `${item.key}[${i}]`),
              p2pValue: item.type === 'Number' ? +p2pValue : p2pValue,
              p2pValueOld: item.type === 'Number' ? +p2pValue : p2pValue,
              type: item.type,
              disabled: item.disabled,
              key: item.key,
              order: item.order,
              required: requiredNote,
              index: i,
            });
          }
        } else {
          result.push({
            label: item.label,
            bpmValue: this.getValue(paymentDTO?.bpmPaymentRequestDetailDTO, item.key),
            sapValue: this.getValue(paymentDTO?.sapPaymentRequestDetailDTO, item.key),
            p2pValue: this.getValue(paymentDTO?.p2pPaymentRequestDetailDTO, item.key),
            type: item.type,
            disabled: item.disabled,
            key: item.key,
            order: item.order,
            required: false,
          });
        }
      });
    }
  }

  createBP() {
    const id = this.route.snapshot.paramMap.get('requestId');
    validateAllFormFields(this.formBP);
    if (this.loadingService.loading || this.formBP.invalid || !id || !this.itemRequest?.typeCode) {
      return;
    }
    let data = this.formBP.getRawValue();
    data.dateOfIssue = getValueDateTimeLocal(data.dateOfIssue, 'YYYY-MM-DD');
    data.bankName = this.bankDropList?.find((item) => item.citadCode === data.bankCode)?.name;
    this.loadingService.start();
    this.service.createBP(id, this.itemRequest.typeCode, data).subscribe({
      next: (res) => {
        this.itemRequest!.bpNumber = res?.data['BusinessPartner'];
        this.bankDataDTO = res?.data?.bankDataDTO || [];
        this.itemRequest!.bpDetailDTO = this.formBP.getRawValue();
        this.fillDataPaymentMethod();
        this.formBP.disable();
        this.loadingService.complete();
        this.messageService.success('MESSAGE.CREATE_BP_INFO_SUCCESS');
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  sendDecisionQM() {
    if (!this.configSendQMPopup?.component && this.itemRequest?.isPaymentRun) {
      return;
    }
    const dialog = this.dialogService?.open(this.configSendQMPopup.component, {
      header: `${this.configSendQMPopup.title.toUpperCase()}`,
      showHeader: false,
      width: this.configSendQMPopup.dialog?.width || '85%',
      data: {
        screenType: ScreenType.Create,
        itemRequest: this.itemRequest,
        requestId: this.route.snapshot.paramMap.get('requestId'),
      },
    });
    dialog?.onClose.subscribe({
      next: (data: any) => {
        if (data) {
          this.itemRequest!.qmEvidenceFile = data.qmEvidenceFile;
          this.itemRequest!.qmEvidenceFileId = data.qmEvidenceFileId;
          this.itemRequest!.isGreedy = data.isGreedy;
        }
      },
    });
  }

  checkFPCPL() {
    this.isCheckedFPCPL = true;
  }

  showBtnProcessing() {
    return (
      !this.disableProcessing() &&
      this.objFunction?.scopes?.includes(this.action.Process) &&
      !this.itemRequest?.isSubmit &&
      this.dataNewPaymentLot.length > 0
    );
  }

  sendEmailToQM() {
    const id = this.route.snapshot.paramMap.get('requestId');
    const data = {};
    if (id && this.itemRequest?.typeCode) {
      this.loadingService.start();
      this.service.sendQmEmail(id, data, this.itemRequest.typeCode).subscribe({
        next: () => {
          this.messageService.success('MESSAGE.SUCCESS_SEND_QM_EMAIL');
          this.detailRequest.ngOnInit();
        },
        error: (err: { error: { messageKey: string } }) => {
          this.messageService.error(
            `MESSAGE.${err?.error?.messageKey ? err.error.messageKey : 'UNSUCCESS_SEND_QM_EMAIL'}`
          );
          this.loadingService.complete();
        },
      });
    }
  }

  addBankAccount() {
    const dialog = this.dialogService?.open(AddBankAccountComponent, {
      header: `Add New Bank`,
      showHeader: false,
      width: '70%',
      data: {
        screenType: ScreenType.Create,
        state: {
          bankDropList: this.bankDropList,
          bpNumber: this.itemRequest?.bpNumber,
          requestId: this.route.snapshot.paramMap.get('type'),
        },
      },
    });
    dialog?.onClose.subscribe({
      next: (result) => {
        if (result) {
          this.bankDataDTO = [...result.listBank];
          const bank: BankDataDTO = this.bankDataDTO?.find(
            (item) => item.bankAccount === result?.data?.bankAccount && item.bankName && result?.data?.bankName
          )!;
          const dataForm = {
            bankCode: bank?.bankCode,
            bankName: bank?.bankName,
            bankAccount: bank?.bankAccount,
            bankAccountName: this.formBP.get('customerName')?.value,
            bpBankId: bank?.bpBankId,
          };
          this.formPaymentMethod.patchValue(dataForm);
        }
      },
    });
  }

  hasProcess() {
    return (
      !!this.itemRequest?.typeCode &&
      [
        this.requestType.TYPE06,
        this.requestType.TYPE07,
        this.requestType.TYPE08,
        this.requestType.TYPE09,
        this.requestType.TYPE10,
      ].includes(this.itemRequest?.typeCode)
    );
  }

  processing() {
    if (size(this.dataNewPaymentLot) === 0) {
      this.messageService.error('MESSAGE.PAYMENTLOT_REQUIRED');
      return;
    }
    if (this.loadingService.loading || this.formPaymentMethod?.invalid) {
      validateAllFormFields(this.formPaymentMethod);
      return;
    }
    this.loadingService.start();
    const id = this.route.snapshot.paramMap.get('requestId');
    if (id && this.itemRequest?.typeCode) {
      const data = {
        searchPaymentLotDTO: {
          PaymentAmount: this.formPaymentMethod?.get('paymentAmount')?.value,
        },
        items: this.dataNewPaymentLot,
        paymentMethodDTO: this.formPaymentMethod.getRawValue(),
        paymentMethod: this.form?.get('paymentMethod')?.value,
      };
      data.paymentMethodDTO.dateOfIssue = getValueDateTimeLocal(data.paymentMethodDTO.dateOfIssue);
      this.service.paymentLotProcessing(id, data, this.itemRequest?.typeCode).subscribe({
        next: () => {
          this.isProcessing = true;
          this.loadingService.complete();
          this.detailRequest.ngOnInit();
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        },
        error: (err) => {
          this.loadingService.complete();
          this.messageService.error(createErrorMessage(err));
        },
      });
    }
  }

  checkBP() {
    const id = this.route.snapshot.paramMap.get('requestId');
    const dataForm = this.form?.getRawValue();
    const data = {
      insuranceClaimForm: dataForm.insuranceClaimForm,
      identification: dataForm.identification,
      feePaymentVoucher: dataForm.feePaymentVoucher,
      decision: this.form?.get('decision')?.value,
    };
    if (id && this.itemRequest?.typeCode && !this.itemRequest?.isCheckBP) {
      this.loadingService.start();
      this.service.checkBp(id, this.itemRequest.typeCode, data).subscribe({
        next: (res) => {
          this.itemRequest!.isCheckBP = true;
          if (res.data) {
            const data: BpDetailDTO = res.data;
            this.bankDataDTO = res.data?.bankDataDTO || [];
            this.itemRequest!.bpDetailDTO = data;
            this.itemRequest!.bpNumber = data?.bpNumber!;
            this.itemRequest!.hasBP = true;
            this.itemRequest!.isCVTV = data.isCVTV;
            this.formBP.reset();
            this.formBP.patchValue(this.itemRequest?.bpDetailDTO!);
            this.fillDataPaymentMethod();
            this.formBP.disable();
          }
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  disableProcessing() {
    return !!this.itemRequest?.isProcessing;
  }

  hasFormPayment() {
    return (
      this.isCheckedFPCPL &&
      !this.itemRequest?.approveList?.find((item) => item.ActionDescription === Decisions.Reject) &&
      this.itemRequest?.paymentRequestHistoryList?.length === 0
    );
  }

  containsObject(obj: PaymentLotModel, list: PaymentLotModel[]) {
    return list.findIndex((elem) => elem.Item === obj.Item && elem.PaymentLot === obj.PaymentLot) > -1;
  }

  getPaymentLot() {
    if (!isNumber(this.paramPaymentLot.paymentAmount)) {
      return this.messageService.warn('MESSAGE.SEARCH_PAYMENT_AMOUNT');
    }
    if (isEmpty(this.paramPaymentLot.usageText)) {
      return this.messageService.warn('MESSAGE.SEARCH_PAYMENT_USAGE_TEXT');
    }
    const params: any = cloneDeep(this.paramPaymentLot);
    params.postingDate = getValueDateTimeLocal(this.paramPaymentLot.postingDate);

    this.dataPaymentLot = [];
    this.loadingService.start();
    this.service.getPaymentLotCollection(params).subscribe({
      next: (data) => {
        this.loadingService.complete();
        this.dataPaymentLot = [];
        if (this.dataNewPaymentLot.length > 0) {
          for (let item of data.data?.Items) {
            let found = this.containsObject(item, this.dataNewPaymentLot);
            if (!found) {
              this.dataPaymentLot.push(item);
            }
          }
        } else {
          this.dataPaymentLot = data.data?.Items || [];
        }
        if (this.dataPaymentLot?.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        //nếu độ dài bản ghi >10 thì cho thanh scroll dọc với độ dài 400px
        this.isScroll = this.dataPaymentLot?.length >= 10;
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  pushDataPaymentLot(item: PaymentLotModel, isCheck?: boolean) {
    if (this.disabledPaymentLot.includes(item.CfcStatusDes!)) {
      return;
    }
    if (isCheck) {
      remove(this.dataPaymentLot, item);
      this.dataNewPaymentLot?.push(item);
    } else {
      this.dataPaymentLot?.push(item);
      remove(this.dataNewPaymentLot, item);
    }
  }

  createPR(form: NgForm) {
    const id = this.route.snapshot.paramMap.get('requestId');
    if (form.invalid || this.loadingService.loading || !id || !this.itemRequest?.typeCode) {
      return;
    }
    const data: PaymentRequestDetailDTO = {};
    const objPayment = this.itemRequest?.bpmPaymentRequestDetailDTO!;
    Object.keys(this.itemRequest?.bpmPaymentRequestDetailDTO!).forEach((key) => {
      if (isArray(objPayment[key])) {
        data[key] = this.dataPaymentRequestDetail.filter((item) => item.key === key).map((o) => o.p2pValue);
      } else {
        data[key] = this.dataPaymentRequestDetail.find((item) => item.key === key)?.p2pValue;
      }
    });
    data.lotItemConcat = this.itemRequest?.p2pPaymentRequestDetailDTO?.lotItemConcat;
    const dataPaymentMethod = cloneDeep(this.formPaymentMethod.getRawValue());
    if (dataPaymentMethod?.value?.toUpperCase() === PaymentMethod.Banking?.toUpperCase()) {
      const bank = this.bankDataDTO?.find((item) => item.bankCode === dataPaymentMethod.bpBankId);
      data.bankCode = bank?.bankCode;
      data.bankName = bank?.bankName;
      data.bpBankId = bank?.bpBankId;
    } else {
      const bank = this.bankDropList?.find((item) => item.citadCode === dataPaymentMethod.cashBankCode);
      data.bankCode = bank?.citadCode;
      data.bankName = bank?.name;
    }
    this.loadingService.start();
    this.service.createPR(id, this.itemRequest.typeCode, data).subscribe({
      next: () => {
        this.itemRequest!.isPaymentRequest = true;
        this.dataPaymentRequestDetail = [];
        this.detailRequest.ngOnInit();
        this.messageService.success('MESSAGE.CREATE_PR_INFO_SUCCESS');
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  sortTable(event: any) {
    return event.order;
  }

  changeValue(item: any) {
    if (item.key === 'amountList') {
      this.dataPaymentRequestDetail.filter((itemP: any) => itemP.key === 'noteList')[item.index].required =
        item.p2pValue !== item.p2pValueOld;
    }
  }

  getOptions(key: string) {
    if (key === 'idType') {
      return this.listIdentifyType;
    } else if (key === 'paymentMethod') {
      return this.paymentMethodList;
    }
    return [];
  }

  getNameDropdown(key: string, value: string) {
    if (key === 'idType') {
      return this.listIdentifyType?.find((option) => option.code === value)?.multiLanguage[
        this.translateService.currentLang
      ];
    } else if (key === 'paymentMethod') {
      return this.paymentMethodList?.find((option) => option.code === value)?.multiLanguage[
        this.translateService.currentLang
      ];
    }
    return null;
  }

  saveChangePaymentMethod() {
    if (this.formPaymentMethod.invalid) {
      validateAllFormFields(this.formPaymentMethod);
      return;
    }
    const dataForm = cloneDeep(this.formPaymentMethod.getRawValue());
    const dataSave = cleanDataForm(this.form!);
    dataSave.paymentMethodDTO = cloneDeep(dataForm);
    dataSave.paymentMethodDTO.dateOfIssue = getValueDateTimeLocal(dataSave.paymentMethodDTO?.dateOfIssue);
    const id = this.route.snapshot.paramMap.get('requestId');
    this.loadingService.start();
    this.service.saveDetailRefundRequest(id!, this.itemRequest?.typeCode!, dataSave).subscribe({
      next: () => {
        this.detailRequest.ngOnInit();
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        const sub = this.loadingService.loading$.subscribe((isloading: boolean) => {
          if (!isloading) {
            const timer = setTimeout(() => {
              this.isSaveChangePaymentMethod = true;
              this.formPaymentMethod.disable();
              this.form?.get('paymentMethod')?.disable();
              this.dataPaymentRequestDetail = [];
              this.getDataTablePaymentRequestDetail(
                this.dataPaymentRequestDetail,
                this.itemRequest,
                this.paymentRequestDetail
              );

              clearTimeout(timer);
            }, 100);
            sub.unsubscribe();
          }
          this.ref.detectChanges();
        });
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  viewFileQM() {
    if (!this.itemRequest?.qmEvidenceFile || !this.itemRequest?.qmEvidenceFileId || this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    this.service.getAttachmentCollection(this.itemRequest?.qmEvidenceFileId).subscribe({
      next: (data: any) => {
        this.openFile(data?.data.mainDocument!, toLower(data?.data?.description) === 'application/pdf');
      },
    });
    this.loadingService.complete();
  }

  openFile(content: string, isPDF: boolean) {
    if (isPDF) {
      const url = URL.createObjectURL(dataURItoBlob(content, 'application/pdf'));
      window.open(url, '_blank')?.focus();
    } else {
      const extensionType = this.itemRequest?.qmEvidenceFile?.substring(
        this.itemRequest?.qmEvidenceFile?.lastIndexOf('.') + 1
      );
      let image = new Image();
      image.src = `data:image/${extensionType};base64,${content}`;
      let wd = window.open('', '_blank');
      wd!.document.write(image.outerHTML);
    }
    this.loadingService.complete();
  }
}
