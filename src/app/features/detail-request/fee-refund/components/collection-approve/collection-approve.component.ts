import { Component, EventEmitter, Injector, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { BaseComponent } from '@shared/components';
import * as _ from 'lodash';
import { CommonModel } from '@common-category/models/common-category.model';
import { createErrorMessage, getUrlByFunctionCode, validateAllFormFields } from '@cores/utils/functions';
import { RefundStepApproveModel } from '@detail-request/shared/models/refund-step-approve.model';
import { Validators } from '@angular/forms';
import {
  ApprovedCollectionList,
  CollectionDetailRequestModel,
} from '@detail-request/shared/models/collection-detail.model';
import { CollectionService } from '@create-requests/service/collection.service';
import { Decisions } from '@cores/utils/enums';

@Component({
  selector: 'app-collection-approve',
  templateUrl: './collection-approve.component.html',
  styleUrls: ['./collection-approve.component.scss'],
})
export class CollectionApproveComponent extends BaseComponent implements OnChanges {
  status?: string;
  approvedDTOList: ApprovedCollectionList[] = [];
  date?: string;
  form = this.fb.group({
    requestCode: null,
    decision: [null, Validators.required],
    note: [null, Validators.required],
  });
  showBtnApproved = true;
  @Input() itemRequest?: CollectionDetailRequestModel;
  @Input() decisionDropList: CommonModel[] = [];
  @Input() requestTypeCode?: string;
  @Output() refreshData: EventEmitter<boolean> = new EventEmitter();

  constructor(injector: Injector, private service: CollectionService) {
    super(injector);
    this.translateService.onLangChange.subscribe(() => {
      this.mapDataApproveList();
    });
  }

  ngOnChanges(_changes: SimpleChanges): void {
    this.showBtnApproved = this.itemRequest?.currentApprove?.find(
      (item) => item.ApprovedBy?.toLowerCase() === this.currUser?.username?.toLowerCase()
    );
    this.approvedDTOList = _.orderBy(this.itemRequest?.approveList, (item) => item.step);
    this.mapDataApproveList();
  }

  mapDataApproveList() {
    this.approvedDTOList?.forEach((item: any) => {
      item.actionDescription = this.decisionDropList?.find((o) => o.code === item.ActionDescription)?.multiLanguage[
        this.translateService.currentLang
      ];
    });
  }

  process() {
    if (this.loadingService.loading || this.form.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    this.loadingService.start();
    const data: RefundStepApproveModel = this.form.getRawValue();
    data.requestCode = this.route.snapshot.paramMap.get('requestId')!;
    data.decision = _.capitalize(data.decision);
    this.service.processStepApprove(data).subscribe({
      next: () => {
        if (data.decision !== Decisions.Approved) {
          this.router.navigateByUrl(getUrlByFunctionCode(this.objFunction?.rsname!, this.currMenu)).then();
        } else {
          this.refreshData.emit(true);
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        }
        this.form.reset();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }
}
