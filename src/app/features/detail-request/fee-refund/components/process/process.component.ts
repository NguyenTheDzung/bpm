import { CollectionDetailRequestModel } from '@detail-request/shared/models/collection-detail.model';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { BaseComponent } from '@shared/components';
import { CommonModel } from '@common-category/models/common-category.model';
import { MenuItem } from 'primeng/api';
import { RequestService } from '@requests/services/request.service';
import { Bank, BankSAP } from '@detail-request/shared/models/detail-request.model';
import { cleanDataForm, createErrorMessage, updateValidity, validateAllFormFields } from '@cores/utils/functions';
import { Validators } from '@angular/forms';
import { Decisions, PaymentMethodCode } from '@cores/utils/enums';
import { CollectionDecisionComponent } from '../collection-decision/collection-decision.component';
import { FeeRefundDetailComponent } from '../../pages/fee-refund-detail/fee-refund-detail.component';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.scss'],
})
export class ProcessComponent extends BaseComponent implements OnChanges, OnInit {
  state: any;
  items: MenuItem[] = [];
  decision = Decisions;
  paymentMethod = PaymentMethodCode;
  @Input() approvedDropList: CommonModel[] = [];
  @Input() itemRequest?: CollectionDetailRequestModel;
  @Input() bankDropList: Bank[] = [];
  @Input() reasonDropList: CommonModel[] = [];
  @Input() subReasonDropList: CommonModel[] = [];
  @Input() pendingReasonDropList: CommonModel[] = [];
  @Input() bankResDTOList: BankSAP[] = [];
  @Input() decisionDropList: CommonModel[] = [];
  @Input() rejectReasonDropList: CommonModel[] = [];
  @Input() additionalAttachmentsDropList: CommonModel[] = [];
  @Input() listIdentifyType: CommonModel[] = [];
  @Input() listGender: CommonModel[] = [];
  @Input() paymentMethodList: CommonModel[] = [];
  @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
  @Output() nextStep: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('formDecision') formDecision!: CollectionDecisionComponent;

  dataProcess = this.fb.group({
    insuranceClaimForm: false,
    identification: false,
    feePaymentVoucher: false,
    decision: null,
    reason: null,
    additionalAttachments: null,
    paymentMethod: null,
    note: null,
    paymentRun: false,
    isProcessing: false,
    searchPaymentLotDTO: [],
  });

  constructor(injector: Injector, private service: RequestService, private detailRequest: FeeRefundDetailComponent) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataProcess.get('decision')?.valueChanges.subscribe((decision) => {
      if (decision === Decisions.Reject || decision === Decisions.Pending) {
        updateValidity(this.dataProcess.get('reason'), Validators.required);
      } else {
        updateValidity(this.dataProcess.get('reason'), null);
      }
      if (decision !== Decisions.Approved) {
        updateValidity(this.dataProcess.get('paymentMethod'), null);
      }
    });
  }

  ngOnChanges(_changes: SimpleChanges): void {
    if (_changes['itemRequest']) {
      const dataForm = {
        decision: this.itemRequest?.decision,
        insuranceClaimForm: !!this.itemRequest?.insuranceClaimForm,
        identification: !!this.itemRequest?.identification,
        feePaymentVoucher: !!this.itemRequest?.feePaymentVoucher,
        isSubmit: !!this.itemRequest?.isSubmit,
        reason: this.itemRequest?.reason,
        additionalAttachments: this.itemRequest?.additionalAttachments,
        requestCode: this.itemRequest?.requestCode,
        paymentMethod: this.itemRequest?.paymentMethod,
        requestTypeCode: this.itemRequest?.requestTypeCode,
        searchPaymentLotDTO: this.itemRequest?.searchPaymentLotDTO || [],
        paymentRun: this.itemRequest?.isPaymentRun,
        isProcessing: this.itemRequest?.isProcessing,
      };
      this.dataProcess.patchValue(dataForm);
      this.onChangeDocument(true);
      if (this.itemRequest?.isProcessing) {
        this.dataProcess.disable();
        this.dataProcess.get('insuranceClaimForm')?.enable();
        this.dataProcess.get('identification')?.enable();
        this.dataProcess.get('feePaymentVoucher')?.enable();
      }
      if (this.itemRequest?.isSubmit) {
        this.dataProcess.disable();
      }
    }
  }

  disableSubmit() {
    return (
      this.loadingService.loading ||
      this.itemRequest?.isSubmit ||
      !this.dataProcess.get('decision')?.value ||
      (!this.itemRequest?.isPaymentRequest && this.dataProcess.get('decision')?.value === Decisions.Approved) ||
      ([Decisions.Pending, Decisions.Pending].includes(this.dataProcess.get('decision')?.value) &&
        this.dataProcess.get('reason')?.value === '')
    );
  }

  disableSave() {
    return (
      this.loadingService.loading ||
      this.itemRequest?.isSubmit ||
      this.itemRequest?.isPaymentRun ||
      ([Decisions.Pending, Decisions.Pending].includes(this.dataProcess.get('decision')?.value) &&
        this.dataProcess.get('reason')?.value === '')
    );
  }

  saveDetail() {
    const id = this.route.snapshot.paramMap.get('requestId');
    if (this.loadingService.loading) {
      return;
    }
    const data = cleanDataForm(this.dataProcess);
    data.paymentMethodDTO = this.formDecision.formPaymentMethod.getRawValue();

    if (id && this.itemRequest?.typeCode) {
      this.loadingService.start();
      this.service.saveDetailRefundRequest(id, this.itemRequest?.typeCode, data).subscribe({
        next: () => {
          this.refreshData.emit(true);
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  submit() {
    const id = this.route.snapshot.paramMap.get('requestId');
    const dataForm = cleanDataForm(this.dataProcess);

    if (
      dataForm.decision === Decisions.Approved &&
      (!dataForm.insuranceClaimForm || !dataForm.identification || !dataForm.feePaymentVoucher)
    ) {
      return this.messageService.error('MESSAGE.REQUEST_SELECT_ALL_REFUND_DOCUMENT');
    }

    if (this.dataProcess.invalid) {
      validateAllFormFields(this.dataProcess);
      return;
    }
    const data = {
      ...this.itemRequest,
      ...dataForm,
    };

    if (id && this.itemRequest?.typeCode) {
      this.loadingService.start();
      this.service.submitDetailRefundRequest(id, this.itemRequest?.typeCode, data).subscribe({
        next: () => {
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
          this.loadingService.complete();
          if ([Decisions.Pending, Decisions.Reject].includes(data?.decision)) {
            return this.back();
          }
          this.detailRequest.ngOnInit();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  savePicNote() {
    if (!this.loadingService.loading) {
      this.loadingService.start();
      const id = this.route.snapshot.paramMap.get('requestId');
      this.service.noteRequest(id!, this.itemRequest?.picNote!).subscribe({
        next: () => {
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  onChangeDocument(initData?: boolean) {
    const data = this.dataProcess?.getRawValue();
    if (!data.insuranceClaimForm || !data.identification || !data.feePaymentVoucher) {
      this.decisionDropList?.forEach((item) => {
        if (item.code === Decisions.Approved) {
          item.disabled = true;
        }
      });
      if (!initData) {
        this.dataProcess.get('decision')?.setValue(null);
      }
    } else {
      this.decisionDropList?.forEach((item) => {
        if (item.code === Decisions.Approved) {
          item.disabled = false;
        }
      });
    }
    this.decisionDropList = [...this.decisionDropList];
  }

  disabledCheckbox() {
    return !!this.itemRequest?.isProcessing;
  }

  override onDestroy() {
    this.service.stop();
  }
}
