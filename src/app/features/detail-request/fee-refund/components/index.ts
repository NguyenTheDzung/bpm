import { ProcessComponent } from './process/process.component';
import { CollectionApproveComponent } from './collection-approve/collection-approve.component';
import { PaymentInformationComponent } from './payment-information/payment-information.component';
import { CollectionDecisionComponent } from './collection-decision/collection-decision.component';
import { SendQMComponent } from './send-qm/send-qm.component';

export const components = [
  ProcessComponent,
  CollectionApproveComponent,
  PaymentInformationComponent,
  CollectionDecisionComponent,
  SendQMComponent,
];
