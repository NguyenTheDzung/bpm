import { RequestService } from '@requests/services/request.service';
import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { FileUpload } from 'primeng/fileupload';
import { MAX_FILE_SIZE } from '@cores/utils/constants';
import * as _ from 'lodash';
import { createErrorMessage } from '@cores/utils/functions';

@Component({
  selector: 'app-send-qm',
  templateUrl: './send-qm.component.html',
  styleUrls: ['./send-qm.component.scss'],
})
export class SendQMComponent extends BaseActionComponent implements OnInit {
  maxFileSize: number = 0;
  responseSelect: boolean | null = null;
  file: File | null = null;
  fileName: string | null = null;
  fileBase64 = '';
  @ViewChild(FileUpload) fileUpload!: FileUpload;

  override state = {
    requestId: '',
  };

  constructor(inject: Injector, service: RequestService, private requestService: RequestService) {
    super(inject, service);
    this.state = this.configDialog?.data;
    this.fileName = this.configDialog?.data?.itemRequest?.qmEvidenceFile;
    if (_.isBoolean(this.configDialog?.data?.itemRequest?.isGreedy)) {
      this.responseSelect = !!this.configDialog?.data?.itemRequest?.isGreedy;
    }
  }

  submitSendQM() {
    if (this.loadingService.loading || !this.fileBase64) {
      return;
    }
    let requestFileDTOs = {
      name: this.fileName?.substring(0, this.fileName?.lastIndexOf('.')),
      type: this.file?.type,
      content: this.fileBase64,
    };

    const data = {
      isGreedy: this.responseSelect,
      file: requestFileDTOs,
    };
    this.loadingService.start();
    this.requestService.qmDecision(this.state.requestId, data, this.configDialog?.data.itemRequest.typeCode).subscribe({
      next: (res) => {
        this.messageService.success('MESSAGE.SUBMIT_SUCCESS');
        this.loadingService.complete();
        this.refDialog.close({
          qmEvidenceFileId: res?.data,
          qmEvidenceFile: requestFileDTOs.name,
          isGreedy: this.responseSelect,
        });
        this.clearFile();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  ngOnInit(): void {
    this.state = this.configDialog?.data;
  }

  uploadHandler(event: FileUpload) {
    this.file = null;
    this.fileBase64 = '';
    this.maxFileSize = event?.files[0].size;
    if (this.maxFileSize > MAX_FILE_SIZE) {
      this.messageService.warn(
        `File upload ${event?.files[0].name} ` + this.translateService.instant('MESSAGE.MAX_FILE_SIZE_UPLOAD')
      );
    }
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.fileBase64 = (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1);
      };
    });
    this.file = event?.files[0];
    this.fileName = this.file.name;
    this.fileUpload.clear();
  }

  clearFile() {
    this.file = null;
    this.fileName = null;
    this.fileBase64 = '';
    this.fileUpload.clear();
  }
}
