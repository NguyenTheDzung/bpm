import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { MenuItem } from 'primeng/api';
import { Bank, BankSAP } from '@detail-request/shared/models/detail-request.model';
import { RequestService } from '@requests/services/request.service';
import { forkJoin } from 'rxjs';
import * as _ from 'lodash';
import { CommonModel } from '@common-category/models/common-category.model';
import {
  CollectionDetailRequestModel,
  PendingHistoryCollectionModel,
} from '@detail-request/shared/models/collection-detail.model';
import { ReportComponent } from '@detail-request/shared/components/report/report.component';
import { SlaDisplayModel } from '@detail-request/shared/models/sla-display.model';
import { calculatorSla } from '@detail-request/shared/utils/functions';
import { createErrorMessage } from '@cores/utils/functions';
import { Roles } from '@cores/utils/constants';

@Component({
  selector: 'app-collection-detail-request',
  templateUrl: './fee-refund-detail.component.html',
  styleUrls: ['./fee-refund-detail.component.scss'],
})
export class FeeRefundDetailComponent extends BaseComponent implements OnInit {
  activeIndexSteps: number = 0;
  activeIndex: number = 0;
  readonlyStep = true;
  header?: string;
  disabled = false;
  typeForm!: string;
  reports?: string;

  itemRequest?: CollectionDetailRequestModel;
  decisionDropList: CommonModel[] = [];
  items: MenuItem[] = [
    {
      label: 'Process',
      id: 'process',
      command: (_event: any) => {
        this.activeIndexSteps = 0;
      },
    },
    {
      label: 'Approve',
      id: 'approve',
      command: (_event: any) => {
        this.activeIndexSteps = 1;
      },
    },

    {
      label: 'Refund',
      id: 'refund',
      command: (_event: any) => {
        this.activeIndexSteps = 2;
      },
    },
  ];
  approvedDropList: CommonModel[] = [];
  bankDropList: Bank[] = [];
  reasonDropList: CommonModel[] = [];
  subReasonDropList: CommonModel[] = [];
  bankResDTOList: BankSAP[] = [];
  pendingReasonDropList: CommonModel[] = [];
  rejectReasonDropList: CommonModel[] = [];
  additionalAttachmentsDropList: CommonModel[] = [];
  listDecisionStepApprove: CommonModel[] = [];
  decisionContractStore: CommonModel[] = [];
  listCategory: CommonModel[] = [];
  listDailyNewAction!: CommonModel[];
  listPendingHistory: PendingHistoryCollectionModel[] = [];
  listDocumentType: CommonModel[] = [];
  listIdentifyType: CommonModel[] = [];
  listGender: CommonModel[] = [];
  paymentMethodList: CommonModel[] = [];
  sapIdentifyTypeCode: CommonModel[] = [];
  objSla: SlaDisplayModel = {
    messageKeySla: '',
    classNameSlaStep: '',
  };

  @ViewChild('report1') report1?: ReportComponent;

  constructor(injector: Injector, private service: RequestService) {
    super(injector);
    this.typeForm = this.route.snapshot.paramMap.get('type')!;
    this.reports = 'report';
    this.mapLanguageSteps();
    this.translateService.onLangChange.subscribe(() => {
      this.mapLanguageSteps();
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('requestId');
    if (id) {
      this.loadingService.start();
      forkJoin([this.service.getState(), this.service.findByRequestId(decodeURIComponent(id))]).subscribe({
        next: ([state, itemRequest]) => {
          this.decisionDropList = state.listDecision || [];
          this.approvedDropList = state.listApprovedReason || [];
          this.reasonDropList = state.listReason || [];
          this.bankDropList = state.listBank || [];
          this.listDecisionStepApprove = state.listDecisionStepApprove || [];
          this.subReasonDropList = state.listSubReasonApprove || [];
          this.pendingReasonDropList = state.listPendingReason || [];
          this.rejectReasonDropList = state.listRejectReason || [];
          this.additionalAttachmentsDropList = state.listAdditionalAttachment || [];
          this.decisionContractStore = state.listCollectionDecisionContractStore || [];
          this.listCategory = state.listCategory || [];
          this.listDailyNewAction = state.listDailyNewAction || [];
          this.listPendingHistory = itemRequest?.pendingHistories || [];
          this.listDocumentType = state.listFeeVoucherType || [];
          this.listIdentifyType = state.listIdentifyType || [];
          this.listGender = state.listGender || [];
          this.paymentMethodList = state.listPaymentMethod || [];
          this.sapIdentifyTypeCode = state.sapIdentifyTypeCode || [];
          this.itemRequest = itemRequest;
          if (itemRequest) {
            this.itemRequest!.requestCode = id;
          }
          if (this.currUser?.username !== this.itemRequest?.pic) {
            this.itemRequest!.isSubmit = true;
          }
          const params = this.route.snapshot.queryParams;
          if (params) {
            setTimeout(() => {
              this.activeIndex = +(params['tab'] || 0);
              this.activeIndexSteps = +(params['step'] || 0);
              calculatorSla(this.objSla, this.itemRequest?.slaStepList[this.activeIndexSteps]);
            }, 10);
          }

          this.bankResDTOList = itemRequest?.bpBankResDTOS || [];
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  refresh(value: boolean) {
    if (value) {
      this.ngOnInit();
    }
  }

  onChangeTabOrStep(tab?: boolean) {
    setTimeout(() => {
      calculatorSla(this.objSla, this.itemRequest?.slaStepList[this.activeIndexSteps]);
      this.router
        .navigate([], {
          relativeTo: this.route,
          queryParams: { tab: this.activeIndex, step: this.activeIndexSteps },
          queryParamsHandling: 'merge',
        })
        .then(() => {
          if (tab && this.activeIndex === 1) {
            this.report1?.getReport(false, true);
          }
        });
    }, 200);
  }

  doAfterSubmit(event: any) {
    if (event) {
      this.activeIndexSteps = 1;
      this.readonlyStep = false;
    }
  }

  mapLanguageSteps() {
    const steps = this.translateService.instant('steps');
    const items = [...this.items];
    items.forEach((item) => {
      item.label = _.get(steps, item.id!);
    });
    this.items = [...this.items];
  }

  readonly Roles = Roles;
}
