import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LapseReversalDetailComponent } from './pages/lapse-reversal-detail/lapse-reversal-detail.component';

const routes: Routes = [{ path: '', component: LapseReversalDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LapseReversalRoutingModule {}
