import { Component } from '@angular/core';
import { Observable } from "rxjs";
import { FormGroup } from "@angular/forms";
import {
  ColumnWithFilterSort,
  InputFilterType,
  IReceivable,
  TAccountDisplayTableRequest,
  TFilterMatchMode
} from "../../models";
import { SAP_PIPE } from "@detail-request/premium-adjustment/utils/enum";
import { Fpl9Table } from "@detail-request/premium-adjustment/share/fpl9-table";

@Component({
  selector: 'app-receivable',
  templateUrl: './../../share/fpl9-table.html',
})
export class ReceivableComponent extends Fpl9Table<IReceivable>{

  showCheckBox: boolean = true


  columns: ColumnWithFilterSort<keyof IReceivable,SAP_PIPE>[] = [
    {
      name: 'Status',
      fieldName: 'trafficLight',
      sort: true,
      pipeName: SAP_PIPE.TRAFFIC_LIGHT,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'status',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Contract',
      fieldName: 'contact',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'contract',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'PG',
      fieldName: 'productGroup',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'PG',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Stat.key',
      fieldName: 'statisticalKey',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'statKey',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Doc.No',
      fieldName: 'docNumber',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'docNo',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Bill Period',
      fieldName: 'billPeriod',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        inputType: InputFilterType.Date,
        formControlName: 'billPeriod',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Bill Per. To',
      fieldName: 'billPeriodTo',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        inputType: InputFilterType.Date,
        formControlName: 'billPerTo',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'DT',
      fieldName: 'docType',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'DT',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Net Date',
      fieldName: 'netDueDate',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        inputType: InputFilterType.Date,
        formControlName: 'netDate',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Amount',
      fieldName: 'amount',
      sort: true,
      pipeName: SAP_PIPE.MONEY,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'amount',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Still Open',
      fieldName: 'stillOpen',
      sort: true,
      pipeName: SAP_PIPE.MONEY,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'stillOpen',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Clear Date',
      fieldName: 'clearDate',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        inputType: InputFilterType.Date,
        formControlName: 'clearDate',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Clear Doc',
      fieldName: 'clearDoc',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'clearDoc',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'CR',
      fieldName: 'clearReason',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'CR',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

    {
      name: 'Crcy',
      fieldName: 'currency',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'Crcy',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },

  ]
  override initFilterForm(): FormGroup {
    return this.fb.group(
      {
        status: [null],
        contract: [null],
        PG: [null],
        statKey: [null],
        docNo: [null],
        billPeriod: [null],
        billPerTo: [null],
        DT: [null],
        netDate: [null],
        amount: [null],
        stillOpen: [null],
        clearDate: [null],
        clearDoc: [null],
        CR: [null],
        Crcy: [null],
      }
    )
  }

  override callApiGetData(params: TAccountDisplayTableRequest): Observable<IReceivable[]> {
    return  this.detailService.getReceivable(params);
  }

  override get moneyField(): string[] {
    return this.columns
      .filter(col =>  col.pipeName === SAP_PIPE.MONEY)
      .map(val => <string>(val.fieldName))
  }

}


