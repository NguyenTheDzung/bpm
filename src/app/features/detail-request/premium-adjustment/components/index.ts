import { ContractInfoComponent } from "./contract-info/contract-info.component";
import { CheckDocumentComponent } from "./check-document/check-document.component";
import { AccountDisplayComponent } from "./account-display/account-display.component";
import { ChronologyComponent } from "./chronology/chronology.component";
import { ReceivableComponent } from "./receivable/receivable.component";
import { DownpaymentComponent } from "./downpayment/downpayment.component";
import { DecisionComponent } from "./decision/decision.component";
import { ResultCreateAllocateComponent } from "./result-create-allocate/result-create-allocate.component";
import { ResultAccountMaintenanceComponent } from "./result-account-maintenance/result-account-maintenance.component";
import { ResultPaymentRunComponent } from "./result-payment-run/result-payment-run.component";
import { ResultCreatePaymentLotComponent } from "./result-create-payment-lot/result-create-payment-lot.component";
import { ResultResetClearingComponent } from "./result-reset-clearing/result-reset-clearing.component";
import { ResultReverseComponent } from "./result-reverse/result-reverse.component";
import { ApplicationJournalComponent } from "./application-journal/application-journal.component";
import { PremiumAttachmentComponent } from "./premium-attachment/premium-attachment.component";
import { PremiumReportComponent } from "./premium-report/premium-report.component";
import { SummaryComponent } from "./summary/summary.component";
import {
  ConfirmRequestInformationComponent
} from "./confirm-request-information/confirm-request-information.component";
import {
  PaymentLotAccountSuspendedComponent
} from "./payment-lot-account-suspended/payment-lot-account-suspended.component";
import { ResultSelectPaymentLotComponent } from "./result-select-payment-lot/result-select-payment-lot.component";
import {
  AccountSuspendedTableComponent
} from "@detail-request/premium-adjustment/components/account-suspended-table/account-suspended-table.component";

export const COMPONENTS = [
  ApplicationJournalComponent,
  ContractInfoComponent,
  CheckDocumentComponent,
  AccountDisplayComponent,
  ChronologyComponent,
  ReceivableComponent,
  DownpaymentComponent,
  DecisionComponent,
  ResultCreateAllocateComponent,
  ResultAccountMaintenanceComponent,
  ResultPaymentRunComponent,
  ResultCreatePaymentLotComponent,
  ResultResetClearingComponent,
  ResultReverseComponent,
  PremiumAttachmentComponent,
  PremiumReportComponent,
  SummaryComponent,
  ConfirmRequestInformationComponent,
  PaymentLotAccountSuspendedComponent,
  ResultSelectPaymentLotComponent,
  AccountSuspendedTableComponent
];
