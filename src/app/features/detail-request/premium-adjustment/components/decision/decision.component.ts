import {
  AfterContentInit,
  Component,
  forwardRef,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { DECISION_VALUE } from '../../utils/enum';
import { DetailPremiumAdjustmentService } from '../../services/detail-premium-adjustment.service';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgControl,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import { CommonModel } from '@common-category/models/common-category.model';
import { IDecisionFormValue } from '@detail-request/premium-adjustment/models';
import { IFormGroupModel } from '@cores/models/form.model';

@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DecisionComponent),
      multi: true,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DecisionComponent),
      multi: true,
    },
  ],
})
export class DecisionComponent implements ControlValueAccessor, Validator, AfterContentInit, OnChanges, OnInit {
  private onTouchFunction: () => void = () => {};
  private onChangeFunction: (value: any) => void = () => {};
  private controlRef: AbstractControl | null = null;

  readonly MAX_LENGTH = 256;

  public form: IFormGroupModel<IDecisionFormValue>;

  isShowReason: boolean = false;

  @Input() decisions: CommonModel[] = [];

  constructor(private detailService: DetailPremiumAdjustmentService, private injector: Injector) {
    this.form = <IFormGroupModel<IDecisionFormValue>>new FormGroup({
      decision: new FormControl(null),
      reason: new FormControl(null, [Validators.required, Validators.maxLength(this.MAX_LENGTH)]),
    });
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => this.onTouchFunction());
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['decisions']) {
      const decisionChanged = changes['decisions'].currentValue as typeof this.decisions;
      if (!decisionChanged.length) return;
      if (
        this.form.value.decision == this.approveValue &&
        decisionChanged.find((common) => common.value == this.approveValue)!.disabled
      ) {
        this.selectDecisionForm.setValue(null);
        this.reasonForm.reset();
        this.reasonForm.removeValidators([Validators.required]);
        this.form.updateValueAndValidity();
        this.onChangeForm();
        return;
      }
      !this.reasonForm.hasValidator(Validators.required) && this.reasonForm.addValidators(Validators.required);
    }
  }

  onChangeForm() {
    this.onChangeFunction(this.form.value);
  }

  ngAfterContentInit() {
    const ngControl = this.injector.get(NgControl);
    this.controlRef = ngControl?.control;
  }

  updateReasonValueAndValidity(selectedDecision?: number) {
    if (this.isApproveDecision(selectedDecision)) {
      if (this.form.enabled) {
        this.reasonForm.enable();
        this.reasonForm.addValidators(Validators.required);
      }
      this.isShowReason = true;
    } else {
      this.reasonForm.disable();
      this.isShowReason = false;
    }
    this.reasonForm.updateValueAndValidity();
  }

  isApproveDecision(selectedDecision?: number) {
    return !!this.detailService.decisionMap.size && selectedDecision != null && selectedDecision !== this.approveValue;
  }

  get selectDecisionForm() {
    return this.form.controls.decision;
  }

  get reasonForm() {
    return this.form.controls.reason;
  }

  changeDecision(decision: number) {
    this.updateReasonValueAndValidity(decision);
    this.reasonForm.reset(null);
    this.onChangeForm();
  }

  changeReason() {
    this.onChangeForm();
  }

  registerOnChange(fn: any): void {
    this.onChangeFunction = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchFunction = fn;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (this.form.invalid) {
      return {
        require: true,
      };
    }
    return null;
  }

  writeValue(obj: IDecisionFormValue): void {
    if (typeof obj != 'object' || !('decision' in obj)) return;
    this.form.patchValue(obj);
    this.updateReasonValueAndValidity(obj?.decision!);
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.form.disable() : this.form.enable();
    this.form.updateValueAndValidity();
  }

  get approveValue() {
    return this.detailService.decisionMap.get(DECISION_VALUE.APPROVE);
  }
}
