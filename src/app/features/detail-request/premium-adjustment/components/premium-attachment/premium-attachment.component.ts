import { Component, Injector, Input } from "@angular/core";
import { dataURItoBlob } from "@cores/utils/functions";
import { BaseComponent } from "@shared/components";
import { toLower } from "lodash";
import { saveAs } from "file-saver";
import { IDetailAdjustmentResponseModel } from "@detail-request/premium-adjustment/models";
import { Attachment, AttachmentSAP } from "../../../shared/models/detail-request.model";
import {
  DetailPremiumAdjustmentService
} from "@detail-request/premium-adjustment/services/detail-premium-adjustment.service";

@Component({
  selector: 'app-premium-attachment',
  templateUrl: './premium-attachment.component.html',
  styleUrls: ['./premium-attachment.component.scss'],
})
export class PremiumAttachmentComponent extends BaseComponent {
  constructor(injector: Injector, private service: DetailPremiumAdjustmentService) {
    super(injector);
  }

  attachments: Attachment[] = [];
  attachmentSAP: AttachmentSAP[] = [];

  @Input('itemRequest') set ItemRequest(value: IDetailAdjustmentResponseModel | undefined) {
    if (!value) {
      console.warn('Invalid itemRequest');
      return;
    }
    this.attachments = value.attachmentFromBPM;
    this.attachmentSAP = value.attachmentFromSAP;
  }

  openFile(content: string, isPDF: boolean) {
    this.loadingService.start();
    try {
      if (isPDF) {
        const url = URL.createObjectURL(dataURItoBlob(content, "application/pdf"));
        window.open(url, "_blank")?.focus();
      } else {
        let image = new Image();
        image.src = `data:image/png;base64,${content}`;
        let wd = window.open("", "_blank");
        wd!.document.write(image.outerHTML);
      }
    } catch (e) {
      console.error(e);
    } finally {
      this.loadingService.complete();
    }
  }

  viewAttachment(id: string) {
    this.loadingService.start();
    this.service.getAttachment(id)
      .subscribe({
      next: (data: any) => {
        this.openFile(data?.data.mainDocument!, toLower(data?.data?.description) === 'application/pdf');
      },
    });

  }

  viewFileFromSAP(id: string) {
    this.loadingService.start();
    this.service.downloadFileSAP(id).subscribe({
      next: (data: any) => {
        this.openFile(data.data.RESULT, data.data.TYPE === 'PDF');
      },
    });
  }

  downloadFileFromSAP(id: string) {
    this.loadingService.start();
    this.service.downloadFileSAP(id).subscribe({
      next: (data: any) => {
        const blob = dataURItoBlob(data.data.RESULT);
        saveAs(
          new Blob([blob], {
            type: 'application/pdf',
          }),
          data.data.FILENAME
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('An error occurred');
        this.loadingService.complete();
      },
    });
  }

  download(id: string) {
    this.loadingService.start();
    this.service.getAttachment(id).subscribe({
      next: (data: any) => {
        const blob = dataURItoBlob(data?.data.mainDocument);
        saveAs(
          new Blob([blob], {
            type: 'application/pdf',
          }),
          data.data.attachmentName
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('An error occurred');
        this.loadingService.complete();
      },
    });
  }
}
