import { Component, Input } from "@angular/core";
import { ISummaryDTO } from "@detail-request/premium-adjustment/models";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styles: [
    ".overflow-wrap__break {word-break: break-word}"
  ]
})
export class SummaryComponent {

  @Input() summary!: ISummaryDTO | null
}
