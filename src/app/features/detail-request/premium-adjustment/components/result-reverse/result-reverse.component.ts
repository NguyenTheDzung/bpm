import { Component } from "@angular/core";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { map, Observable } from "rxjs";

@Component({
  selector: 'app-result-reverse',
  templateUrl: './result-reverse.component.html',
  styleUrls: ['./result-reverse.component.scss']
})
export class ResultReverseComponent {

  disableAllocate$: Observable<boolean> = this.detailService.marks$.pipe(
    map(({isAllocate, isReverse, isPIC, isSubmit}) => {
      return  !isPIC || !isReverse || isSubmit || isAllocate
    })
  )

  dataTable$ = this.detailService.resultReverse$

  constructor(
    private detailService: DetailPremiumAdjustmentService,
  ) {}

  allocate() {
    this.detailService.allocate()
      .subscribe()
  }
}
