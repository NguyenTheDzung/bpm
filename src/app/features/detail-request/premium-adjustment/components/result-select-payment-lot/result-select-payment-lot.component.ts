import { Component } from "@angular/core";
import {
  DetailPremiumAdjustmentService
} from "@detail-request/premium-adjustment/services/detail-premium-adjustment.service";

@Component({
  selector: 'app-result-select-payment-lot',
  templateUrl: './result-select-payment-lot.component.html',
})
export class ResultSelectPaymentLotComponent {

  dataTable$ = this.detailService.resultSelectLot$

  constructor(
    private detailService: DetailPremiumAdjustmentService
  ) { }

}
