import { Component, Input } from "@angular/core";
import { RequestService } from "@requests/services/request.service";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { LoadingService } from "@cores/services/loading.service";

@Component({
  selector: 'app-result-account-maintenance',
  templateUrl: './result-account-maintenance.component.html',
  styleUrls: ['./result-account-maintenance.component.scss'],
})
export class ResultAccountMaintenanceComponent {
  @Input() showFilter: boolean = true;
  dataTable$  = this.detailService.resultAccountMaintenance$

  constructor(
    private loadingService: LoadingService,
    private service: RequestService,
    private detailService: DetailPremiumAdjustmentService
  ) {}


}
