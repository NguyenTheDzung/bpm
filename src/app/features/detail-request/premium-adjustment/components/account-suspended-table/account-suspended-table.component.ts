import { Component, EventEmitter, Input, Output } from "@angular/core";
import { DataTable } from "@cores/models/data-table.model";
import { LazyLoadEvent } from "primeng/api";

@Component({
  selector: 'app-account-suspended-table',
  template: `
    <p-table (onLazyLoad)="onLazyLoad.emit($event )"
             [(selection)]="selection"
             (selectionChange)="!disableCheckBox && selectionChange.emit($event)"
             [first]="dataTable.first"
             [totalRecords]="dataTable.totalElements"
             [lazyLoadOnInit]="false"
             [lazy]="true"
             [pageLinks]="3"
             [paginator]="paginator"
             [rowsPerPageOptions]="[10,20,30]"
             [rows]="dataTable.size"
             [showCurrentPageReport]="true"
             [value]="dataTable.content">
      <ng-template pTemplate="header">
        <tr>
          <th *ngIf="showCheckbox"></th>
          <th>Payment Lot</th>
          <th>Item</th>
          <th>Posting Date</th>
          <th>Usage text</th>
          <th>Payment Amount</th>
          <th>Remaining Amount</th>
          <th>Allocated Policy</th>
          <th>Status</th>
        </tr>
      </ng-template>
      <ng-template pTemplate="body" let-index="rowIndex" let-rowValue>
        <tr>
          <td *ngIf="showCheckbox">
            <p-tableCheckbox [disabled]="disableCheckBox" [index]="index" [value]="rowValue"></p-tableCheckbox>
          </td>
          <td>{{rowValue.paymentLot}}</td>
          <td>{{rowValue.item}}</td>  
          <td>{{rowValue.postingDate | MapSAPDate}}</td>
          <td [pTooltip]="rowValue.usageText" tooltipPosition="right">{{rowValue.usageText}}</td>
          <td>{{rowValue.paymentAmount ? (rowValue.paymentAmount | currency: 'VND' : '': '') : ''}}</td>
          <td>{{rowValue.remainingAmount ? (rowValue.remainingAmount | currency: 'VND' : '': '') : ''}}</td>
          <td>{{rowValue.policyAllocated}}</td>
          <td>{{rowValue.status}}</td>
        </tr>
      </ng-template>
      <ng-template pTemplate="emptymessage">
        <tr>
          <td [colSpan]="9" class="text-center">
            {{'request.detailRequest.premiumAdjustment.no-item'| translate}}
          </td>
        </tr>
      </ng-template>
    </p-table>
  `,
})
export class AccountSuspendedTableComponent {

  @Input() selection: any
  @Output() selectionChange = new EventEmitter<any>()

  @Input() dataTable: DataTable<any> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0
  }

  @Output() onLazyLoad = new EventEmitter<LazyLoadEvent>()
  @Input() paginator: boolean = true
  @Input() showCheckbox = true
  @Input() disableCheckBox = false
}
