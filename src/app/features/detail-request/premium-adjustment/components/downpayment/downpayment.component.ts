import { Component } from '@angular/core';
import {
  ColumnWithFilterSort,
  IDownpayment,
  InputFilterType,
  TAccountDisplayTableRequest,
  TFilterMatchMode
} from "../../models";
import { Observable } from "rxjs";
import { FormGroup } from "@angular/forms";
import { SAP_PIPE } from "@detail-request/premium-adjustment/utils/enum";
import { Fpl9Table } from "@detail-request/premium-adjustment/share/fpl9-table";

@Component({
  selector: 'app-downpayment',
  templateUrl: './../../share/fpl9-table.html',
})
export class DownpaymentComponent extends Fpl9Table<IDownpayment> {

  showCheckBox = false

  override columns: ColumnWithFilterSort<keyof IDownpayment, SAP_PIPE>[] = [
    {
      name: 'Status',
      fieldName: 'trafficLight',
      sort: true,
      pipeName: SAP_PIPE.TRAFFIC_LIGHT,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'status',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Business Partner',
      fieldName: 'bpNumber',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'businessPartner',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Contract',
      fieldName: 'contact',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'contract',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'PG',
      fieldName: 'productGroup',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'PG',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Stat.key',
      fieldName: 'statisticalKey',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'statkey',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: ' Doc.No',
      fieldName: 'docNumber',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'docNo',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'DT',
      fieldName: 'docType',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'DT',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Net Date',
      fieldName: 'netDueDate',
      pipeName: SAP_PIPE.DATE,
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Date,
        formControlName: 'netDate',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Amount',
      fieldName: 'amount',
      pipeName: SAP_PIPE.MONEY,
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'amount',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Clear Date',
      fieldName: 'clearDate',
      pipeName: SAP_PIPE.DATE,
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Date,
        formControlName: 'clearDate',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Clear Doc',
      fieldName: 'clearDoc',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'clearDoc',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'CR',
      fieldName: 'clearReason',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'CR',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    },
    {
      name: 'Crcy',
      fieldName: 'currency',
      sort: true,
      filterConfig: {
        inputType: InputFilterType.Text,
        formControlName: 'Crcy',
        filterMatchMode: TFilterMatchMode.CONTAINS
      }
    }
  ]

  override initFilterForm(): FormGroup {
    return this.fb.group(
      {
        businessPartner: [null],
        status: [null],
        contract: [null],
        PG: [null],
        statkey: [null],
        docNo: [null],
        DT: [null],
        netDate: [null],
        amount: [null],
        clearDate: [null],
        clearDoc: [null],
        CR: [null],
        Crcy: [null],
      }
    )
  }

  override callApiGetData(params: TAccountDisplayTableRequest): Observable<IDownpayment[]> {
    return this.detailService.getDownPayment(params)
  }

  override get moneyField(): string[] {
    return this.columns
      .filter(col =>  col.pipeName === SAP_PIPE.MONEY)
      .map(val => <string>(val.fieldName))
  }

}
