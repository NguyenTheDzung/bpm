import { Component, Injector, Input, OnInit } from "@angular/core";
import { dataURItoBlob } from "@cores/utils/functions";
import { TranslateService } from "@ngx-translate/core";
import { BaseComponent } from "@shared/components";
import { saveAs } from "file-saver";
import { RequestService } from "@requests/services/request.service";
import { IDetailAdjustmentResponseModel } from "@detail-request/premium-adjustment/models";
import { finalize, map } from "rxjs";
import { REPORT_TAB_INDEX } from "../../utils/enum";


@Component({
  selector: 'app-premium-report',
  templateUrl: './premium-report.component.html',
  styleUrls: ['./premium-report.component.scss'],
})
export class PremiumReportComponent extends BaseComponent implements OnInit {
  @Input() itemRequest?: IDetailAdjustmentResponseModel;
  @Input() reversal: boolean = false;
  reportProcessObjectUrl = '';
  contractResumptionReportObjectUrl = '';
  /**
   * Báo cáo tiến trình
   */
  reportProcessBlob?: Blob;
  contractResumptionReportBlob?: Blob
  tabViewModel = REPORT_TAB_INDEX


  constructor(inject: Injector, private service: RequestService, public translate: TranslateService) {
    super(inject);
  }

  ngOnInit() {
    this.getReport(false, true);
  }

  handleChange($event: any) {
    if ($event.index == 1) {
      this.getReverse(false, true);
    }
  }

  getReport(isDownload = false, showLoading = false) {
    const type = this.route.snapshot.paramMap.get('type')!;
    const requestId = this.route.snapshot.paramMap.get('requestId');

    if (showLoading) {
      this.loadingService.start();
    }
    this.service.getReport(requestId!, type!)
      .pipe(
        finalize(() => {
          showLoading && this.loadingService.complete()
        }),
        map((response) => {
          return dataURItoBlob(response?.data)
        })
      )
      .subscribe({
        next: (data) => {
          this.reportProcessBlob = data
          this.reportProcessObjectUrl = URL.createObjectURL(data);
          if (isDownload && this.reportProcessBlob) {
            this.downloadReport();
          }
        },
        error: () => {
          if (isDownload) {
            this.messageService.error('Error!');
          }
        },
      });
  }

  getReverse(isDownload = false, showLoading = false) {
    const id = this.route.snapshot.paramMap.get('requestId');
    if (showLoading) {
      this.loadingService.start();
    }
    this.service.getReport(id!, '10')
      .pipe(
        finalize(() => {
          showLoading && this.loadingService.complete()
        }),
        map((response) => {
          return dataURItoBlob(response?.data)
        })
      )
      .subscribe({
        next: (data) => {
          this.contractResumptionReportBlob = data
          this.contractResumptionReportObjectUrl = URL.createObjectURL(data)
          if (isDownload && this.contractResumptionReportBlob) {
            this.downloadReverse();
          }
        },
        error: () => {
          if (isDownload) {
            this.messageService.error('Error!');
          }
        },
      });
  }

  downloadReport() {
    if (this.reportProcessBlob) {
      saveAs(
        new Blob([this.reportProcessBlob!], {
          type: 'application/pdf',
        })
      );
    } else {
      this.getReport(true);
    }
  }

  downloadReverse() {
    if (this.contractResumptionReportBlob) {
      saveAs(
        new Blob([this.contractResumptionReportBlob!], {
          type: 'application/pdf',
        })
      );
    } else {
      this.getReverse(true);
    }
  }
}
