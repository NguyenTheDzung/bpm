import { Component } from "@angular/core";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";

@Component({
  selector: 'app-result-payment-run',
  templateUrl: './result-payment-run.component.html',
  styleUrls: ['./result-payment-run.component.scss']
})
export class ResultPaymentRunComponent{

  constructor(
    private detailService: DetailPremiumAdjustmentService,
  ) {}

  datatable$ = this.detailService.resultPaymentRun$
}
