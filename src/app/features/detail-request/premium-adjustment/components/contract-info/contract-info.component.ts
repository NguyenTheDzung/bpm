import { Component, OnInit } from "@angular/core";
import { POLICY_INFO_BUTTON } from "../../utils/enum";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { IPolicyViewDTO } from "@detail-request/premium-adjustment/models";
import { TranslateService } from "@ngx-translate/core";
import { IDropdownItemWithTranslate } from "@detail-request/shared/models/dropdown-with-translate.model";

@Component({
  selector: "app-contract-info",
  templateUrl: "./contract-info.component.html",
  styleUrls: ["./contract-info.component.scss"]
})
export class ContractInfoComponent implements OnInit {
  selectedTab: number = POLICY_INFO_BUTTON.POLICY_ON_RECEIPT;
  readonly policyInfoButton = POLICY_INFO_BUTTON;

  /**
   * Lưu trữ dữ liệu của hai hợp đồng chứng từ và hợp đồng cần điều chỉnh
   */
  data?: {
    onAttachmentPolicyViewDTO: IPolicyViewDTO,
    feeAdjustPolicyViewDTO: IPolicyViewDTO
  };

  commonIdentificationTypeCode: IDropdownItemWithTranslate<string>[] = [];

  constructor(
    private detailService: DetailPremiumAdjustmentService,
    public translateService: TranslateService
  ) {
  }

  ngOnInit() {
    this.detailService.detailPremiumAdjustment$.subscribe(
      value => {
        this.data = {
          onAttachmentPolicyViewDTO: value.adjustmentStep.onAttachmentPolicyViewDTO,
          feeAdjustPolicyViewDTO: value.adjustmentStep.feeAdjustPolicyViewDTO
        };
      }
    );
    this.commonIdentificationTypeCode = this.detailService.commonIdentificationTypeCode.map(
      (item) => {
        return {
          value: String(item.code),
          multiLanguage: JSON.parse(item.description!)
        };
      }
    );
  }

  onClickViewContract(clickData: POLICY_INFO_BUTTON) {
    this.selectedTab = clickData;
  }
}
