import { Component } from "@angular/core";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { finalize, map } from "rxjs";
import { LoadingService } from "@cores/services/loading.service";

@Component({
  selector: 'app-result-create-payment-lot',
  templateUrl: './result-create-payment-lot.component.html',
  styleUrls: ['./result-create-payment-lot.component.scss'],
})
export class ResultCreatePaymentLotComponent {

  dataTable$  = this.detailService.resultPaymentLot$
  disableAllocateButton$ = this.detailService.marks$.
    pipe(
      map(mark => {
        return !mark.isPIC || mark.isAllocate || !mark.isCreatePaymentLot || mark.isSubmit
      })
  )
  constructor(
    private loadingService: LoadingService,
    private detailService: DetailPremiumAdjustmentService
  ) {}

  allocate() {
    this.detailService
      .allocate()
      .pipe(finalize(() => this.loadingService.complete()))
      .subscribe();
  }
}
