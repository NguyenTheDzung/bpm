import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ICheckDocumentValue } from "../../models/detail-adjustment.model";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";

@Component({
  selector: 'app-check-document',
  templateUrl: './check-document.component.html',
  styleUrls: ['./check-document.component.scss']
})
export class CheckDocumentComponent {

  premiumAdjustmentForm: boolean;
  paymentAttachment: boolean;
  paymentAttachmentAuthorization: boolean;

  @Input() set checked(value: ICheckDocumentValue) {
    this.premiumAdjustmentForm = value.premiumAdjustmentForm
    this.paymentAttachmentAuthorization = value.paymentAttachmentAuthorization
    this.paymentAttachment = value.paymentAttachment
  }

  @Output() checkedChange = new EventEmitter<ICheckDocumentValue>()

  @Input() disabled: boolean = false

  constructor(
    private service: DetailPremiumAdjustmentService
  ) {

    this.premiumAdjustmentForm =
      this.paymentAttachment =
        this.paymentAttachmentAuthorization = false
  }

  onChangeCheckBox() {
    if(!this.service.marks.isPIC && this.service.marks.isSubmit) return
    this.checkedChange.emit(
      {
        premiumAdjustmentForm: this.premiumAdjustmentForm,
        paymentAttachment: this.paymentAttachment,
        paymentAttachmentAuthorization: this.paymentAttachmentAuthorization,
      }
    )
  }

}
