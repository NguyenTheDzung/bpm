import { Component, EventEmitter, Input, Output } from "@angular/core";
import {
  ICheckDocumentValue,
  IConfirmRequestInformationRequest,
  ISummaryDTO
} from "@detail-request/premium-adjustment/models";
import {
  DetailPremiumAdjustmentService
} from "@detail-request/premium-adjustment/services/detail-premium-adjustment.service";
import { DECISION_VALUE } from "@detail-request/premium-adjustment/utils/enum";
import { map } from "rxjs";

@Component({
  selector: "app-confirm-request-information",
  templateUrl: "./confirm-request-information.component.html",
  styleUrls: ["./confirm-request-information.component.scss"]
})
export class ConfirmRequestInformationComponent {
  summary: ISummaryDTO | null = null;

  onAttachmentPolicyNumber?: string;

  @Input("summaryInfo") set _summaryInfo(value: ISummaryDTO | null) {
    if (!value) return;
    this.summary = value;
    this.onAttachmentPolicyNumber = value.onAttachmentPolicyNumber;
  }

  @Output() onChangeAttachmentPolicyNumber = new EventEmitter<typeof this.onAttachmentPolicyNumber>()
  @Input() checkedDocument!: ICheckDocumentValue;

  disableAction$ = this.detailService.marks$.pipe(
    map((mark) => {
      return !mark.isPIC || mark.isAllocate || mark.isSubmit || mark.isConfirmed;
    })
  );

  constructor(private detailService: DetailPremiumAdjustmentService) {
  }

  confirmPolicyNumber() {
    const request: Omit<IConfirmRequestInformationRequest, "requestId"> = {
      decision: this.detailService.decisionMap.get(DECISION_VALUE.APPROVE)!, // Mặc định
      onAttachmentPolicyNumber: this.onAttachmentPolicyNumber?.toString() ?? undefined,
      ...this.checkedDocument
    };
    this.detailService.confirmRequestInformation(request).subscribe();
  }

}
