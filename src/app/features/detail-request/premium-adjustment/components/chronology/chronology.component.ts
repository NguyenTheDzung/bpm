import { Component } from "@angular/core";
import { Fpl9Table } from "@detail-request/premium-adjustment/share/fpl9-table";
import {
  ColumnWithFilterSort,
  IChronology,
  InputFilterType,
  TAccountDisplayTableRequest,
  TFilterMatchMode
} from "@detail-request/premium-adjustment/models";
import { SAP_PIPE } from "@detail-request/premium-adjustment/utils/enum";
import { FormGroup } from "@angular/forms";
import { Observable } from "rxjs";

@Component({
  selector: 'app-chronology',
  templateUrl: './../../share/fpl9-table.html',
  styles: [
    `.usage-text {
      max-width: 350px;
      overflow: hidden;
      text-overflow: ellipsis;
    }`
  ]
})
export class ChronologyComponent extends Fpl9Table<IChronology> {
  showCheckBox = false

  override columns: ColumnWithFilterSort<keyof IChronology, SAP_PIPE>[] = [
    {
      name: 'Doc No',
      fieldName: 'docNumber',
      sort: true,
      filterConfig: {
        formControlName: "docNo",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Origin',
      fieldName: 'origin',
      sort: true,
      filterConfig: {
        formControlName: "orgin",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Changed by',
      fieldName: 'createdBy',
      sort: true,
      filterConfig: {
        formControlName: "changedBy",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Entry Date',
      fieldName: 'entryDate',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        formControlName: "entryDate",
        inputType: InputFilterType.Date,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Entered at',
      fieldName: 'entryAt',
      sort: true,
      filterConfig: {
        formControlName: "enteredAt",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Posting date',
      fieldName: 'postingDate',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        formControlName: "postingDate",
        inputType: InputFilterType.Date,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Net Due Date',
      fieldName: 'netDueDate',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        formControlName: "netDueDate",
        inputType: InputFilterType.Date,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Crcy',
      fieldName: 'currency',
      sort: true,
      filterConfig: {
        formControlName: "Crcy",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Name of Orgin',
      fieldName: 'originName',
      sort: true,
      filterConfig: {
        formControlName: "nameOfOrgin",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Amount',
      fieldName: 'amount',
      sort: true,
      pipeName: SAP_PIPE.MONEY,
      filterConfig: {
        formControlName: "amount",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Debit',
      fieldName: 'debit',
      sort: true,
      pipeName: SAP_PIPE.MONEY,
      filterConfig: {
        formControlName: "debit",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Credit memo',
      fieldName: 'creditMemo',
      sort: true,
      pipeName: SAP_PIPE.MONEY,
      filterConfig: {
        formControlName: "creditMemo",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Cleared',
      fieldName: 'created',
      sort: true,
      pipeName: SAP_PIPE.MONEY,
      filterConfig: {
        formControlName: "cleared",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Payment lot',
      fieldName: 'paymentLot',
      sort: true,
      filterConfig: {
        formControlName: "paymentLot",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Item',
      fieldName: 'paymentLotItem',
      sort: true,
      filterConfig: {
        formControlName: "item",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: "Usage text",
      fieldName: 'usageText',
      sort: true,
      styleClass: 'usage-text',
      toolTips: true,
      filterConfig: {
        formControlName: "usageText",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
    {
      name: 'Transaction ID',
      fieldName: 'ft',
      sort: true,
      toolTips: true,
      styleClass: 'usage-text',
      filterConfig: {
        formControlName: "ft",
        inputType: InputFilterType.Text,
        filterMatchMode: TFilterMatchMode.CONTAINS
      },
    },
  ]

  override initFilterForm(): FormGroup {
    return this.fb.group(
      {
        docNo: [],
        orgin: [],
        changedBy: [],
        entryDate: [],
        enteredAt: [],
        postingDate: [],
        netDueDate: [],
        Crcy: [],
        nameOfOrgin: [],
        amount: [],
        debit: [],
        creditMemo: [],
        cleared: [],
        paymentLot: [],
        item: [],
        ft: [],
        usageText: []
      }
    )
  };

  override callApiGetData(params: TAccountDisplayTableRequest): Observable<IChronology[]> {
    return this.detailService.getChronology(params)
  }

  override get moneyField(): string[] {
    return this.columns
      .filter(col =>  col.pipeName === SAP_PIPE.MONEY)
      .map(val => <string>(val.fieldName))
  }


}
