import { Component } from "@angular/core";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";

@Component({
  selector: 'app-result-reset-clearing',
  templateUrl: './result-reset-clearing.component.html',
  styleUrls: ['./result-reset-clearing.component.scss']
})
export class ResultResetClearingComponent{

  constructor(private service: DetailPremiumAdjustmentService,) {}
  dataTable$ = this.service.resultResetClearing$

}
