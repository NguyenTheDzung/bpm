import {Component, EventEmitter, Injector, Output, ViewChild} from '@angular/core';
import {
  ColumnWithFilterSort,
  IBusinessProcessData,
  IBusinessProcessingResponse,
  IBusinessProcessResponse,
  IGetBusinessProcessRequest,
  IGetProcessingRequest,
  InputFilterType,
  ISAPOpenShortcutRequestParams,
  TFilterMatchMode
} from "../../models";
import {BaseTableComponent} from "@shared/components";
import {DataTable} from "@cores/models/data-table.model";
import {PaginatorModel} from "@cores/models/paginator.model";
import {FormGroup} from "@angular/forms";
import {finalize, map, Observable} from "rxjs";
import {IErrorResponse} from "@cores/models/response.model";
import {expandCollapseAnimation} from '../../../shared/utils/animation';
import {DetailPremiumAdjustmentService} from "../../services/detail-premium-adjustment.service";
import * as moment from "moment";
import {Table} from "primeng/table";
import {SAP_PIPE} from "@detail-request/premium-adjustment/utils/enum";
import {clearColumnFilter, customSort, filterColumn} from '@detail-request/premium-adjustment/utils/functions';
import {createErrorMessage} from "@cores/utils/functions";

@Component({
  selector: 'app-application-journal',
  templateUrl: './application-journal.component.html',
  styleUrls: ['./application-journal.component.scss'],
  animations: [expandCollapseAnimation({duration: 400})]
})
export class ApplicationJournalComponent extends BaseTableComponent<IBusinessProcessResponse> {
  readonly STATUS_POLICY_ACTIVE  = "ACTIVE"
  @ViewChild('table') tb!: Table
  selection?: IBusinessProcessResponse
  filterForm!: FormGroup;

  /**
   * Reset button chỉ enable khi selection có dữ liệu
   * @default false
   */
  disableResetButton$: Observable<boolean> = this.detailService.marks$.pipe(
    map((marks) => {
      return !marks.isPIC || marks.isSubmit
    })
  )
  readonly SAP_PIPE = SAP_PIPE

  processingDataTable: DataTable = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  }

  isLoadingProcessingTable: boolean = false

  @Output() refresh = new EventEmitter()
  readonly columns: ColumnWithFilterSort<keyof IBusinessProcessResponse>[] = [
    {
      name: 'Processing',
      fieldName: 'applicationResume',
      sort: true,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Text,
        formControlName: 'processing'
      }
    },
    {
      name: 'Business process number',
      fieldName: 'bizprcnoId',
      sort: true,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Text,
        formControlName: 'processNumber'
      }
    },
    {
      name: 'App. No',
      fieldName: 'applicationCd',
      sort: true,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Text,
        formControlName: 'appNo'
      }
    },
    {
      name: 'Business Process Decs',
      fieldName: 'bizprcTt',
      sort: true,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Text,
        formControlName: 'processDecs'
      }
    },
    {
      name: 'App. Date',
      fieldName: 'applDt',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Date,
        formControlName: 'appDate'
      }
    },
    {
      name: 'App.Rec. Date',
      fieldName: 'applinDt',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Date,
        formControlName: 'appRecDate'
      }
    },
    {
      name: 'Effective Date',
      fieldName: 'effectiveDt',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Date,
        formControlName: 'effectiveDate'
      }
    },
    {
      name: 'Start on',
      fieldName: 'modDt',
      sort: true,
      pipeName: SAP_PIPE.DATE,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Date,
        formControlName: 'startOn'
      }
    },
    {
      name: 'Start From',
      fieldName: 'modNameTt',
      sort: true,
      filterConfig: {
        filterMatchMode: TFilterMatchMode.CONTAINS,
        inputType: InputFilterType.Text,
        formControlName: 'startFrom'
      }
    }
  ]

  constructor(inject: Injector,
              private detailService: DetailPremiumAdjustmentService
  ) {
    super(inject, detailService);
    this.filterForm = this.fb.group(
      {
        processing: [null],
        processNumber: [null],
        appNo: [null],
        processDecs: [null],
        appDate: [null],
        appRecDate: [null],
        effectiveDate: [null],
        startOn: [null],
        startFrom: [null]
      }
    )
    this.initStateProcessingTable()
    this.search(true)
  }

  onChangeSelection(selection?: IBusinessProcessData) {
    if (selection) {
      this.getProcessingData(true)
    } else {
      this.initStateProcessingTable()
    }
  }

  initStateProcessingTable() {
    this.processingDataTable = {
      content: <IBusinessProcessingResponse[]>[],
      currentPage: 0,
      size: 10,
      totalElements: 0,
      totalPages: 0,
      first: 0,
      numberOfElements: 0,
    }
  }


  override pageChange(paginator: PaginatorModel) {

  }

  getProcessingData(firstPage = false) {
    this.loadingService.start()
    this.isLoadingProcessingTable = true
    if (firstPage) {
      this.processingDataTable.currentPage = 0
    }
    const selection = this.selection
    if (!selection) return
    const request: IGetProcessingRequest = {
      applicationCd: selection.applicationCd,
      entryOptionId: String(selection.bizprcnoId),
      requestId: this.detailService.detailPremiumAdjustment.requestId,
    }
    this.detailService.getBusinessPossessing(request)
      .pipe(
        finalize(() => {
          this.loadingService.complete();
          this.isLoadingProcessingTable = false
        })
      )
      .subscribe(
        {
          next: (response) => {
            const dataTable = response
            this.processingDataTable = {
              currentPage: 0,
              first: 0,
              numberOfElements: dataTable.length,
              size: 10,
              totalElements: dataTable.length,
              totalPages: Math.round(dataTable.length / 10),
              content: dataTable
            }
          }
        }
      )
  }

  onRefresh(tableEl: Table) {
    this.selection = undefined
    this.refresh.emit()
    this.onChangeSelection()
    this.clearAllFilter(tableEl)
    this.search(true)
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    const request: IGetBusinessProcessRequest = {
      page: this.dataTable.currentPage,
      requestId: this.detailService.detailPremiumAdjustment.requestId,
      size: this.dataTable.size
    }
    this.detailService.getBusinessProcessData(request)
      .pipe(finalize(() => this.loadingService.complete()))
      .subscribe(
        {
          next: (data: IBusinessProcessResponse[]) => {
            const dataTable = data
            this.dataTable = {
              currentPage: 0,
              first: 0,
              numberOfElements: dataTable.length,
              size: 10,
              totalElements: dataTable.length,
              totalPages: Math.round(dataTable.length / 10),
              content: dataTable.map(
                (item) => {
                  const bizprcnoId = parseFloat(<string>item.bizprcnoId)
                  return {
                    ...item,
                    bizprcnoId: isFinite(bizprcnoId)? bizprcnoId : 0
                  }
                }
              )
            }
          },
        }
      )
  }


  /**
   * Download một shortcut cho user tự nhấn vào SAP để chỉnh sửa
   * thay vì sẽ thực hiện reset trên giao diện
   *
   * Đối với các yêu cầu có trạng thái HĐ trên chứng từ <> "ACTIVE" thì thực hiện disable (Bắn message lỗi)
   */
  downloadSAPShortcut() {
    const selection = this.selection
    const status = this.detailService.detailPremiumAdjustment.adjustmentStep.onAttachmentPolicyViewDTO.policyInfo.status
    if(status.toUpperCase() !== this.STATUS_POLICY_ACTIVE){
      this.messageService.warn("request.detailRequest.premiumAdjustment.notification.on-attachment-policy-inactive")
      return
    }
    if (!selection) {
      this.messageService.warn('request.detailRequest.premiumAdjustment.notification.reset')
      return
    }
    const params: ISAPOpenShortcutRequestParams = {
      policyNumber: this.detailService.detailPremiumAdjustment?.adjustmentStep.onAttachmentPolicyViewDTO.policyInfo.policyNumber,
      startDate: moment().format('DD.MM.yyyy'),
      endDate: moment().format('DD.MM.yyyy'),
    }
    this.loadingService.start()
    this.detailService.downloadSAPShortcut(params)
      .subscribe(
        {
          next: (response) => {
            const fileLink = document.createElement('a')
            const arrayBuffer = new Uint8Array([...window.atob(response.data)].map(char => char.charCodeAt(0)))
            fileLink.href = window.URL.createObjectURL(new Blob([arrayBuffer]))
            /**
             * Sap shortcut có extension là .sap
             * Đặt tên file theo thời gian để tránh trùng lặp
             */
            const fileName = `sap${Date.now()}.sap`

            fileLink.setAttribute('download', fileName)
            document.body.appendChild(fileLink)
            fileLink.click()
            fileLink.remove()
            this.loadingService.complete()
          },
          error: (err: IErrorResponse) => {
            this.loadingService.complete()
            this.messageService.error(createErrorMessage(err))
          }
        }
      )
  }


  filterColumn(table: Table, $event: any, column: typeof this.columns[0]) {
    filterColumn(table, $event, column)
  }

  clearColumnFilter(table: Table, column:  typeof this.columns[0]) {
    clearColumnFilter(table,column, this.filterForm)
  }

  clearAllFilter(tableEl: Table) {
    this.columns.forEach(
      (value) => {
        tableEl.filter(undefined, value.fieldName, TFilterMatchMode.CONTAINS)
      }
    )
    tableEl.reset()
    this.filterForm.reset()
  }


  customSort($event: any) {
    customSort($event)
  }
}
