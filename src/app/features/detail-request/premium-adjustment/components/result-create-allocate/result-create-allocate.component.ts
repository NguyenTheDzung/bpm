import { Component } from "@angular/core";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { map } from "rxjs";

@Component({
  selector: 'app-result-create-allocate',
  templateUrl: './result-create-allocate.component.html',
  styleUrls: ['./result-create-allocate.component.scss']
})
export class ResultCreateAllocateComponent {

  dataTable$ = this.detailService.resultAllocated$
  policyNumber$ =  this.detailService.detailPremiumAdjustment$.pipe(
    map((detail) => {
      return detail.adjustmentStep.onAttachmentPolicyViewDTO.policyInfo.policyNumber
    })
  )

  constructor(private detailService: DetailPremiumAdjustmentService) {
  }

}
