import { Component, Injector, OnInit } from '@angular/core';
import * as moment from 'moment';
import { DataTable } from '@cores/models/data-table.model';
import {
  IAccountSuspendedRequest,
  IConfirmedSelectLotList,
  ICreatePaymentLotResult,
  IPaymentLotAccountSuspendedForm,
  IPaymentLotSuspendedResponse,
  ISelectedLotDtoList,
} from '@detail-request/premium-adjustment/models';
import { LazyLoadEvent } from 'primeng/api';
import { BaseComponent } from '@shared/components';
import { DetailPremiumAdjustmentService } from '@detail-request/premium-adjustment/services/detail-premium-adjustment.service';
import { IResponseModel, TPageableSearchParams } from '@cores/models/response.model';
import { map } from 'rxjs';
import { PAYMENT_LOT_FREE_STATUS, PAYMENT_LOT_REVERSED_STATUS } from '@detail-request/premium-adjustment/utils/enum';
import { ScreenType } from '@cores/utils/enums';
import { CreatePaymentLotComponent } from '@detail-request/premium-adjustment/dialogs/create-payment-lot/create-payment-lot.component';
import { forEach, isEmpty, isNull } from 'lodash';
import { ApproveAllocateInfoComponent } from '@detail-request/premium-adjustment/dialogs/approve-allocate-info/approve-allocate-info.component';

@Component({
  selector: 'app-payment-lot-account-suspended',
  templateUrl: './payment-lot-account-suspended.component.html',
  styleUrls: ['./payment-lot-account-suspended.component.scss'],
})
export class PaymentLotAccountSuspendedComponent extends BaseComponent implements OnInit {
  lotSelection: (IPaymentLotSuspendedResponse & { validToDate: string })[] = [];
  paymentLotData: IConfirmedSelectLotList[] = [];
  maxSelectFromDate: Date | null = null;
  minSelectToDate: Date | null = null;
  form: IPaymentLotAccountSuspendedForm;

  dataTableAccountSuspended: DataTable<IPaymentLotSuspendedResponse & { validToDate: string }> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };

  disableAllocateButton$ = this.detailService.marks$.pipe(
    map((mark) => {
      return !mark.isPIC || mark.isAllocate || mark.isSubmit;
    })
  );

  disableAction$ = this.detailService.marks$.pipe(
    map((mark) => {
      return !mark.isPIC || mark.isAllocate || mark.isSubmit || mark.haveItemAllocateFailed;
    })
  );

  constructor(injector: Injector, private detailService: DetailPremiumAdjustmentService) {
    super(injector);
    this.form = this.fb.group({
      usageText: [null],
      paymentAmount: [null],
      fromDate: [null],
      toDate: [null],
    }) as IPaymentLotAccountSuspendedForm;

    this.detailService.detailPremiumAdjustment$.subscribe((detail) => {
      this.paymentLotData = detail.adjustmentStep.processingInfoDTO.selectedPaymentLots;
    });
  }

  ngOnInit(): void {
    this.form.controls.fromDate.valueChanges.subscribe((newValue) => {
      if (!this.isValidDate(newValue)) {
        this.minSelectToDate = null;
        return;
      }
      this.minSelectToDate = moment(newValue).toDate();
    });
    this.form.controls.toDate.valueChanges.subscribe((newValue) => {
      if (!this.isValidDate(newValue)) {
        this.maxSelectFromDate = null;
        return;
      }
      this.maxSelectFromDate = moment(newValue).toDate();
    });
  }

  isValidDate(value: string | null | Date) {
    return !!value && typeof value == 'object';
  }

  allocate() {
    this.dialogService.open(ApproveAllocateInfoComponent, {
      showHeader: false,
      width: '60%',
      data: {
        screenType: ScreenType.Create,
        paymentLotData: this.paymentLotData,
      },
    });
  }

  mapDataSearch(params: object): TPageableSearchParams<Partial<IAccountSuspendedRequest>> {
    let formattedParams = {};
    Object.keys(params).forEach((key) => {
      // @ts-ignore
      if (params[key] == undefined || params[key] == null) {
        return;
      }
      // @ts-ignore
      formattedParams[key] = params[key];
    });
    return {
      page: this.dataTableAccountSuspended.currentPage,
      size: this.dataTableAccountSuspended.size,
      ...formattedParams,
    };
  }

  validateSearch(showMessage = true) {
    const formValue = this.form.value;

    if (isNull(formValue.fromDate) && !isNull(formValue.toDate)) {
      this.form.controls.fromDate.setValue(moment(formValue.toDate).add(-14, 'day').toDate());
    }

    if (!isNull(formValue.fromDate) && isNull(formValue.toDate)) {
      this.form.controls.toDate.setValue(moment(formValue.fromDate).add(14, 'day').toDate());
    }

    if (moment(formValue.fromDate).isAfter(formValue.toDate)) {
      showMessage && this.messageService.warn('MESSAGE.FROM_DATE_NOT_MORE_THAN_TO_DATE');
      return false;
    }

    if (moment(formValue.toDate).diff(moment(formValue.fromDate), 'day') > 14) {
      showMessage &&
        this.messageService.warn(
          this.translateService.instant('MESSAGE.PERIOD_TWO_DATE_NOT_MORE_THAN_DAYS', { days: 14 })
        );
      return false;
    }
    let countParams = 0;
    forEach(formValue, (value, key) => {
      if (!isNull(value) && !isEmpty(value?.toString())) {
        countParams += 1;
      }
    });

    if (countParams < 2) {
      showMessage &&
        this.messageService.warn(this.translateService.instant('MESSAGE.REQUIRED_MIN_COUNT_PARAM', { count: 2 }));
      return false;
    }

    return true;
  }

  formatDate(value: string | undefined | Date) {
    if (!value) return undefined;
    return moment(value).format('YYYY-MM-DD');
  }

  search(firstPage?: boolean) {
    if (!this.validateSearch()) return;

    if (firstPage) {
      this.dataTableAccountSuspended.currentPage = 0;
    }

    const formVal = this.form.value;
    const additionalRequestParams: Partial<IAccountSuspendedRequest> = {
      paymentAmount: formVal.paymentAmount?.toString(),
      postingDateFrom: this.formatDate(formVal.fromDate),
      usageText: formVal?.usageText,
      postingDateTo: this.formatDate(formVal.toDate),
    };
    const requestParams = this.mapDataSearch(additionalRequestParams);
    this.detailService.getPaymentLotAccountSuspended(requestParams).subscribe({
      next: (data) => {
        this.lotSelection = [];
        data.content = data.content.map((value) => {
          return {
            ...value,
            validToDate: value.valut,
          };
        });
        this.dataTableAccountSuspended = data as typeof this.dataTableAccountSuspended;

        this.loadingService.complete();
      },
    });
  }

  validateConfirmItem() {
    if (!this.lotSelection.length) return false;
    if (!!this.lotSelection.find((lot) => lot.status != PAYMENT_LOT_FREE_STATUS)) {
      this.messageService.warn('request.detailRequest.premiumAdjustment.notification.need-select-free-lot');
      return false;
    }
    return true;
  }

  validateCreatePaymentLot() {
    if (!this.lotSelection.length) return false;

    if (!!this.lotSelection.find((lot) => lot.status != PAYMENT_LOT_REVERSED_STATUS)) {
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.create-payment-lot-status-reverse'
      );
      return false;
    }

    if (this.lotSelection.length > 1) {
      this.messageService.warn('MESSAGE.WARN.ONLY_ONE_PAYMENT_LOT_REVERSE');
      return false;
    }
    return true;
  }

  confirmItem() {
    if (!this.validateConfirmItem()) return;
    let selectedLotDTOList: ISelectedLotDtoList[] = this.lotSelection.map((val) => {
      const select: ISelectedLotDtoList = {
        currency: 'VND',
        item: val.item,
        paymentAmount: val.paymentAmount,
        paymentLot: val.paymentLot,
        postingDate: val.postingDate,
        remainingAmount: val.remainingAmount,
        status: val.status,
        usageText: val.usageText,
        validToDate: val.validToDate,
      };
      val?.policyAllocated && (select.policyAllocated = val?.policyAllocated);
      return select;
    });
    this.detailService.confirmSelectLot(selectedLotDTOList).subscribe((response) => {
      this.paymentLotData = response.data;
      this.search(true);
    });
  }

  createPaymentLot() {
    if (!this.validateCreatePaymentLot()) return;
    this.dialogService
      ?.open(CreatePaymentLotComponent, {
        showHeader: false,
        width: '70%',
        data: {
          screenType: ScreenType.Create,
          selection: this.lotSelection,
        },
      })
      .onClose.subscribe((changed?: IResponseModel<ICreatePaymentLotResult>) => {
        if (changed) {
          changed && this.search(true);
          this.paymentLotData = changed.data.selectedPaymentLots;
        }
      });
  }

  pageChange($event: LazyLoadEvent) {
    console.log($event);
    this.dataTableAccountSuspended.currentPage = $event.first! / $event.rows!;
    this.dataTableAccountSuspended.size = $event.rows!;
    this.dataTableAccountSuspended.first = $event.first!;
    this.search();
  }

  removeItem(rowIndex: number) {
    const itemValue = this.paymentLotData[rowIndex];

    // Có id nghĩa là đã lưu trong DB
    // Phải thực hiện xóa bằng API
    if (!!itemValue.id) {
      this.detailService.deSelectLot([itemValue.id]).subscribe(() => {
        this.paymentLotData.splice(rowIndex, 1);
        if (this.validateSearch(false)) this.search(true);
      });
      return;
    }
  }
}
