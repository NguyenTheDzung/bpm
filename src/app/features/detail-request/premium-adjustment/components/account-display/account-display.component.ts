import { Component, Injector, OnInit } from "@angular/core";
import { ScreenType } from "@cores/utils/enums";
import {
  ApprovedResetClearingComponent
} from "../../dialogs/approved-reset-clearing/approved-reset-clearing.component";
import { IDownpayment, IReceivable } from "../../models";
import { ACCOUNT_DISPLAY_TAB_VALUE, TRAFFIC_LIGHT_CODE } from "../../utils/enum";
import { ApprovedPaymentRunComponent } from "../../dialogs/approved-payment-run/approved-payment-run.component";
import { ApprovedReversedComponent } from "../../dialogs/approved-reversed/approved-reversed.component";
import {
  ApprovedAccountMaintenanceComponent
} from "../../dialogs/approved-account-maintenance/approved-account-maintenance.component";
import { BaseComponent } from "@shared/components";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";

@Component({
  selector: 'app-account-display',
  templateUrl: './account-display.component.html',
  styleUrls: ['./account-display.component.scss']
})
export class AccountDisplayComponent extends BaseComponent implements OnInit{
  readonly DOCTYPE_11 = "11"
  readonly DOCTYPE_73 = "73"
  readonly DOCTYPE_30 = "30"


  selectedTab: number = ACCOUNT_DISPLAY_TAB_VALUE.RECEIVABLE;
  selection: IDownpayment[] | IReceivable[] = []

  enableAccountMaintenance: boolean = false
  enableClearing: boolean = false
  enableReverse: boolean = false
  enablePaymentRun: boolean = false

  constructor(
    private detailService: DetailPremiumAdjustmentService,
    injector: Injector
  ) {
    super(injector)
  }

  ngOnInit() {
    this.detailService.marks$.subscribe(
      () => {
        this.checkEnableButton()
      }
    )
  }

  /**
   * Reset clearing sẽ không được thực hiện với request có DT <> DT 11
   */
  resetClearing() {
    if(!this.enableClearing) return

    if (this.selection.length == 0) {
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.reset-clearing'
      )
      return
    }

    if(this.selection.length > 1) {
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.reset-clearing-one'
      )
      return
    }

    if(this.selection.find(
      select =>
        select.docType !== this.DOCTYPE_11 || select.trafficLight !== TRAFFIC_LIGHT_CODE.RECEIVE_PAY
    )) {
      this.messageService.warn(
        "request.detailRequest.premiumAdjustment.notification.reset-clearing-require-doctype"
      )
      return
    }

    this.dialogService?.open(
      ApprovedResetClearingComponent,
      {
        showHeader: false,
        width: '60%',
        data: {
          screenType: ScreenType.Create,
          selection: this.selection,
          selectedTab: this.selectedTab
        },
      }).onClose.subscribe(
      (changed: boolean) => {
        if (changed) {
          this.reloadTab()
        }
      }
    )
  }

  accountMaintenance() {
    if (!this.enableAccountMaintenance) return

    if(this.selection.length == 0){
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.account-maintenance'
      )
      return
    }

    if(this.selection.length < 2){
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.mapping-doc-require-two-row'
      )
      return
    }

    this.dialogService?.open(
      ApprovedAccountMaintenanceComponent, {
        showHeader: false,
        width: '60%',
        data: {
          screenType: ScreenType.Create,
          selection: this.selection,
          selectedTab: this.selectedTab
        },
      }).onClose.subscribe(
      (changed: boolean) => {
        if (changed) {
          this.reloadTab()
        }
      }
    );
  }

  paymentRun() {
    if (!this.enablePaymentRun) return

    if(this.selection.length == 0){
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.payment-run'
      )
      return
    }

    if (this.selection.find(select => (
        ![this.DOCTYPE_30, this.DOCTYPE_73].includes(select.docType)
      )
    )) {
      this.messageService.warn(
        "request.detailRequest.premiumAdjustment.notification.payment-run-require-doctype"
      );
      return;
    }

    this.dialogService?.open(
      ApprovedPaymentRunComponent, {
        showHeader: false,
        width: '60%',
        data: {
          screenType: ScreenType.Create,
          selection: this.selection,
          selectedTab: this.selectedTab
        },
      }).onClose.subscribe(
      (changed: boolean) => {
        if (changed) {
          this.reloadTab()
        }
      }
    );
  }

  reverse() {
    if (!this.enableReverse) return
    if(this.selection.length == 0){
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.reverse'
      )
      return
    }
    if(this.selection.length > 1){
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.reverse-one'
      )
      return
    }

    if (this.selection.find(
      ({docType, trafficLight}) => (
        docType !== this.DOCTYPE_30 || trafficLight !== TRAFFIC_LIGHT_CODE.CREDIT_OPEN
      )
    )) {
      this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.reverse-require-doctype'
      )
      return
    }

    this.dialogService?.open(
      ApprovedReversedComponent, {
        showHeader: false,
        width: '60%',
        data: {
          screenType: ScreenType.Create,
          selection: this.selection,
          selectedTab: this.selectedTab
        },
      }).onClose.subscribe(
      (changed: boolean) => {
        if (changed) {
          this.reloadTab()
        }
      }
    );
  }

  changeTab() {
    this.onChangeSelection([])
    this.selection = []
  }

  onChangeSelection($event: IDownpayment[] | IReceivable[]) {
    this.selection = $event
    this.checkEnableButton()
  }

  checkEnableButton(){
    const marks = this.detailService.marks
    /*
      * Nếu đã Submit(Nộp) hoặc đã allocate thì không thể nhấn được các nút
      */
    if(!marks.isPIC || marks.isSubmit || marks.isAllocate || marks.isCreatePaymentLot){
      this.enableReverse =
        this.enablePaymentRun =
          this.enableClearing =
            this.enableAccountMaintenance = false
      return
    }


    /*
     * Các tab khác receivable sẽ không chọn được row nên cũng không có action gì
     *
     * Chọn 1 row thì sẽ cho nhấn reset clearing và  reverse
     *
     * Chọn nhiều hơn 1 row thì sẽ cho nhấn payment run và account maintenance
     *
     * Nếu yêu cầu đã reverse thí sẽ không thể làm gì khác ngoài tiếp tục reverse
     */
    if (ACCOUNT_DISPLAY_TAB_VALUE.RECEIVABLE === this.selectedTab) {

      if(marks.isReverse){
        this.enableReverse = this.selection.length == 1
        this.enableClearing =
          this.enablePaymentRun =
            this.enableAccountMaintenance = false
        return
      }
      
      if(marks.isAccountMaintenance || marks.isPaymentRun || marks.isResetClearing ){
        this.enableReverse = false
        this.enableClearing =
          this.enablePaymentRun =
            this.enableAccountMaintenance = true
        return
      }

      /**
       * Nếu chưa có action gì thì các nút đều accessible
       */
      this.enableReverse =
        this.enableClearing =
      this.enablePaymentRun =
        this.enableAccountMaintenance = true

    } else {

      this.enableReverse =
        this.enablePaymentRun =
          this.enableClearing =
            this.enableAccountMaintenance = false
    }
  }

  clearFilter() {
    this.detailService.clearFilterAccountDisplay()
  }


  reloadTab() {
    this.detailService.resetAccountDisplay()
  }
}
