import { Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DECISION_VALUE, STEP, TAB_BUTTON } from '../../utils/enum';
import {
  ICheckDocumentValue,
  IDecisionFormValue,
  IDetailAdjustmentResponseModel,
  IPremiumAdjustmentDecisionRequest,
  ISaveStateRequest,
} from '../../models';
import { BaseComponent } from '@shared/components';
import { SubmitPolicyRestoreComponent } from '../../dialogs/submit-policy-restore/submit-policy-restore.component';
import { Actions, ScreenType } from '@cores/utils/enums';
import { DetailPremiumAdjustmentService } from '../../services/detail-premium-adjustment.service';
import { auditTime, combineLatest, finalize, forkJoin, map, Observable, of, take, tap } from 'rxjs';
import { DialogService } from 'primeng/dynamicdialog';
import { MenuItem } from 'primeng/api';
import { fadeExpandCollapseAnimation } from '../../../shared/utils/animation';
import { PendingHistoryCollectionModel } from '@detail-request/shared/models/collection-detail.model';
import { Resolution, Roles } from '@cores/utils/constants';
import { SlaDisplayModel } from '@detail-request/shared/models/sla-display.model';
import { calculatorSla } from '@detail-request/shared/utils/functions';
import { CommonModel } from '@common-category/models/common-category.model';
import { FormControl, Validators } from '@angular/forms';
import { IFormControlModel } from '@cores/models/form.model';
import { DecisionComponent } from '@detail-request/premium-adjustment/components/decision/decision.component';

@Component({
  selector: 'app-premium-adjustment',
  templateUrl: './premium-adjustment-detail.component.html',
  styleUrls: ['./premium-adjustment-detail.component.scss'],
  providers: [DetailPremiumAdjustmentService, DialogService],
  animations: [fadeExpandCollapseAnimation({ duration: 700 })],
})
export class PremiumAdjustmentDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild(DecisionComponent) decisionComp?: DecisionComponent;

  readonly STEP = STEP;
  readonly DECISION_VALUE = DECISION_VALUE;
  readonly TAB_BUTTON = TAB_BUTTON;

  /**
   * Tên biến lưu request id trên url
   */
  readonly REQUEST_ID_PARAM_NAME = 'requestId';

  listPendingHistory: PendingHistoryCollectionModel[] = [];

  /**
   * Xác định Index của tab được chọn
   * Mặc định luôn chọn tab process đầu tiên
   */
  selectedTab: number = TAB_BUTTON.PROCESS;

  /**
   * Thông tin kiểm tra chứng từ
   * @interface ICheckDocumentValue
   * @default { contractAdjustment: false, proofOfPayment: false, authorizationLetter: false}
   * false mặc định là không check
   */
  checkedDocument: ICheckDocumentValue = {
    premiumAdjustmentForm: false,
    paymentAttachment: false,
    paymentAttachmentAuthorization: false,
  };

  /**
   * Lưu thông tin detail của request
   */
  detailAdjustmentResponse!: IDetailAdjustmentResponseModel;

  disableChangeCheckDocument = false;

  /**
   * Thông tin sla
   */
  objSla: SlaDisplayModel = {
    messageKeySla: '',
    classNameSlaStep: '',
  };

  /**
   * Disable button payment lot
   */
  disablePaymentLotButton$ = this.detailService.marks$.pipe(
    map((mark) => {
      return !mark.isPIC || !mark.isPaymentRun || mark.isCreatePaymentLot || mark.isSubmit || mark.isAllocate;
    })
  );

  readonlyStep$: Observable<boolean> = combineLatest([
    this.authService.getUserRoles$(),
    this.detailService.detailPremiumAdjustment$,
  ]).pipe(
    map(([roles, detail]) => {
      if (
        [Resolution.PA_USER_DONE, Resolution.PA_USER_INPROCESS].includes(
          detail?.step2?.requestResolution?.toUpperCase()!
        )
      ) {
        return false;
      }

      for (let role of roles) {
        if ([Roles.OP_COL_USER, Roles.OP_COL_MANAGER, Roles.OP_DIRECTOR].includes(role)) {
          return false;
        }
      }

      return true;
    })
  );

  decisionCommonList: CommonModel[] = [];
  decisionContractStore: CommonModel[] = [];
  decisionMap: Map<DECISION_VALUE, number> = new Map();

  /**
   * Xác định xem điều chỉnh phí đã có kết quả nào hay chưa
   * để hiển thị các bảng kết quả
   * @type boolean
   * @default false
   */
  haveResult$: Observable<boolean> = this.detailService.haveResult$.pipe(take(1));

  /**
   * Vị trí của step hiện tại
   * @default 0
   * @enum STEP
   */
  activeStep: number = STEP.STEP1;

  /**
   * Danh sách các step
   * Label sẽ được thay đổi theo ngôn ngữ sau trong code
   */
  steps: MenuItem[] = [
    { label: 'Xử lý', id: 'process' },
    { label: 'Phê duyệt', id: 'approve' },
    { label: 'Khôi phục trạng thái HĐ', id: 'reset' },
  ];

  viewPolicyReverse$ = this.detailService.marks$.pipe(
    map(({ isSubmit, isAllocate }) => {
      return isSubmit;
    })
  );

  disablePolicyReversed$ = combineLatest([this.detailService.marks$, this.detailService.detailPremiumAdjustment$]).pipe(
    map(([mark, detail]) => {
      return !mark.isPIC || !mark.isSubmit || !!detail?.isReinstatement;
    })
  );

  /**
   * Observable chứa thông tin summary
   */
  summary$ = this.detailService.detailPremiumAdjustment$.pipe(map((detail) => detail.adjustmentStep.summaryDTO));

  /**
   * Hiển thị button payment lot
   */
  viewButtonCreatePaymentLot$ = this.detailService.marks$.pipe(
    map((mark) => {
      return mark.isPaymentRun;
    })
  );

  /**
   * Dùng kiểm soát dữ liệu của quyết định điều chỉnh
   */
  decisionFormControl;
  /**
   * Lưu lại dữ liệu on attachment policy của người dùng nhập vào để thực hiện
   * gọi api lưu
   */
  onAttachmentPolicyNumber?: string;

  constructor(injector: Injector, public detailService: DetailPremiumAdjustmentService) {
    super(injector);
    this.decisionFormControl = new FormControl(
      {
        decision: null,
        reason: null,
      },
      Validators.required
    ) as IFormControlModel<IDecisionFormValue>;
  }

  ngOnInit() {
    this.detailService.loadingCount
      .pipe(
        tap((isLoading) => {
          isLoading && !this.loadingService.loading && this.loadingService.start();
        }),
        auditTime(1500),
        tap((isLoading) => {
          !isLoading && this.loadingService.loading && this.loadingService.complete();
        })
      )
      .subscribe();

    this.getData();

    this.translateService.onLangChange.subscribe(() => this.detailAdjustmentResponse && this.mapTranslateSteps());

    this.detailService.detailPremiumAdjustment$.subscribe((premiumDetailData) => {
      this.detailAdjustmentResponse = premiumDetailData;
      of(null)
        .pipe(
          tap(() => this.mapTranslateSteps()),
          tap(() => this.checkActiveStep())
        )
        .subscribe();
    });

    this.decisionFormControl.valueChanges.subscribe((value: IDecisionFormValue) => {
      this.ref.detectChanges();
      this.detailService.decision = value.decision!;
    });

    this.detailService.marks$
      .pipe(
        map((mark) => {
          return (
            !mark.isPIC ||
            mark.isConfirmed ||
            mark.isReverse ||
            mark.isAllocate ||
            mark.isSubmit ||
            mark.isCreatePaymentLot ||
            mark.isAccountMaintenance ||
            mark.isResetClearing ||
            mark.isPaymentRun
          );
        })
      )
      .subscribe((result) => {
        this.disableChangeCheckDocument = result;
        result ? this.decisionFormControl.disable() : this.decisionFormControl.enable();
        this.decisionFormControl.updateValueAndValidity();
      });
  }

  checkActiveStep(data: IDetailAdjustmentResponseModel = this.detailAdjustmentResponse) {
    const params = this.route.snapshot.queryParams;
    if (params) {
      setTimeout(() => {
        this.selectedTab = +(params['tab'] || STEP.STEP1);

        /**
         * Vì step 2, step3 có được check quyền như nhau
         * và nếu người dùng vào được yêu cầu chắc chắn cũng có role = step 1
         */
        if (!data?.isReinstatement || this.steps[STEP.STEP2].disabled) {
          this.activeStep = STEP.STEP1;
        } else {
          this.activeStep = +(params['step'] || STEP.STEP1);
        }

        calculatorSla(this.objSla, data?.slaStepList[this.activeStep]);
      }, 10);
    }
  }

  /**
   * Gọi API để lấy dữ liệu chi tiết điều chỉnh phí của hợp đồng
   */
  getData() {
    this.loadingService.start();
    let requestId = this.route.snapshot.paramMap.get(this.REQUEST_ID_PARAM_NAME)!;
    forkJoin([this.detailService.getDetailPremiumAdjustment(requestId), this.detailService.getCommonCode()]).subscribe({
      next: ([{ data }, { commonDecisions, commonDecisionMapped, decisionContractStore }]) => {
        this.listPendingHistory = data?.pendingHistories || [];
        const premiumDetailData = data;
        this.checkedDocument = this.detailService.checkDocumentChecked;
        this.decisionMap = commonDecisionMapped;
        this.decisionContractStore = decisionContractStore;
        this.onAttachmentPolicyNumber = data.adjustmentStep.summaryDTO.onAttachmentPolicyNumber;
        this.decisionFormControl.patchValue({
          decision: premiumDetailData.adjustmentStep.processingInfoDTO.decision ?? null,
          reason: premiumDetailData.adjustmentStep.processingInfoDTO.decisionReason ?? null,
        });
        this.decisionCommonList = commonDecisions.map((decision: CommonModel, index) => ({
          label: decision.name,
          orderNum: index,
          value: parseInt(decision.value),
          multiLanguage: JSON.parse(decision.description!),
        }));
        this.onChangeSelection(this.checkedDocument);
      },
    });
  }

  /**
   * Mở dialog khôi phục hợp đồng
   */
  policyReversed() {
    this.dialogService
      ?.open(SubmitPolicyRestoreComponent, {
        showHeader: false,
        width: '70%',
        data: {
          screenType: ScreenType.Create,
          state: this.detailAdjustmentResponse,
        },
      })
      .onClose.subscribe((result) => {
        if (result) {
          this.getData();
        }
      });
  }

  onChangeSelection(checkDocument: ICheckDocumentValue) {
    this.detailService.checkDocumentChecked = checkDocument;
    this.decisionCommonList = this.decisionCommonList.map((common) => {
      if (common.value == this.decisionMap.get(DECISION_VALUE.APPROVE)) {
        common.disabled = !checkDocument.premiumAdjustmentForm;
      }
      return common;
    });
  }

  /**
   * Đối với TH user là PIC , khi click vào button Back request → Lưu trạng thái hiện thái hiện tại của request và trở về màn hình danh sách request
   *
   * Đối với TH user là không phải là PIC , khi click vào button Back request → trở về màn hình danh sách request
   */
  cancel() {
    this.returnRequestsScreen();
  }

  /**
   * Tạo request model lưu lại state của request
   */
  createRequestSaveState(): ISaveStateRequest {
    const { decision, reason } = this.decisionFormControl.value;
    const request: ISaveStateRequest = {
      requestId: this.detailAdjustmentResponse.requestId,
      decision: decision!,
      decisionReason: reason ?? '',
      premiumAdjustmentForm: this.checkedDocument.premiumAdjustmentForm,
      paymentAttachment: this.checkedDocument.paymentAttachment,
      paymentAttachmentAuthorization: this.checkedDocument.paymentAttachmentAuthorization,
    };
    if (this.onAttachmentPolicyNumber) {
      request.onAttachmentPolicyNumber = this.onAttachmentPolicyNumber;
    }
    return request;
  }

  /**
   * Trở về màn requests
   */
  returnRequestsScreen() {
    this.router.navigate(['/bpm/requests']).then();
  }

  /**
   *  Đối với TH user là PIC , khi click vào button Save→ Lưu trạng thái hiện thái hiện tại của request.
   *
   *  Đối với TH user là không phải là PIC , button bị disable.
   */
  save() {
    const { isSubmit, isPIC } = this.detailService.marks;
    if (isSubmit || !isPIC) return;
    this.loadingService.start();
    const request: ISaveStateRequest = this.createRequestSaveState();
    this.detailService.saveState(request).subscribe();
  }

  /**
   * Khi reverse đã allocate thì có thể nộp.
   *
   * Đối với TH user là không phải là PIC , button bị disable.
   *
   * Hoặc khi đã thực hiện allocate thì có thể nộp.
   */
  submit() {
    const { isSubmit, isPIC } = this.detailService.marks;
    this.markFormAsTouched();
    if (isSubmit || !isPIC || this.decisionFormControl.invalid) return;

    if (this.decisionFormControl.value.decision == this.decisionMap.get(DECISION_VALUE.APPROVE)) {
      this.detailService
        .submitDecisionApprove(this.detailAdjustmentResponse.requestId)
        .pipe(finalize(() => this.loadingService.complete()))
        .subscribe();
    } else {
      const { decision, reason } = this.decisionFormControl.value;
      const request: IPremiumAdjustmentDecisionRequest = {
        requestId: this.detailAdjustmentResponse!.requestId,
        decision: decision!,
        decisionReason: reason ?? '',
      };

      this.detailService
        .submitDecisionRejectPending(request)
        .pipe(finalize(() => this.loadingService.complete()))
        .subscribe(() => this.returnRequestsScreen());
    }
  }

  checkDisableSubmit() {
    const { isConfirmed, isPIC, isSubmit } = this.detailService.marks;
    if (isSubmit || !isPIC) return true;
    if (!this.decisionMap.size) return true;
    // Check phải chọn decision
    const { decision } = this.decisionFormControl.value;
    if (decision == null) return true;

    /**
     *    Cập nhật theo nghiệp vụ yêu cầu bỏ
     *    check chỉ enable nút submit sau đã allocate
     *
     *    Giờ sau khi confirm là có thể nộp
     *
     */

    if (decision == this.decisionMap.get(DECISION_VALUE.APPROVE)! && !isConfirmed) return true;

    // if (decision == this.decisionMap.get(DECISION_VALUE.APPROVE)) {
    //   return !this.detailService.marks.isAllocate;
    // }
    return false;
  }

  checkDisableSave() {
    const { isSubmit, isPIC } = this.detailService.marks;
    return isSubmit || !isPIC;
  }

  mapTranslateSteps() {
    this.steps = this.steps.map((step, index) => {
      step.label = this.translateService.instant(`steps.${step.id}`);
      switch (index) {
        case 0:
          step.disabled = false;
          break;
        case 1:
          step.disabled = !this.detailAdjustmentResponse?.isReinstatement;
          break;
        case 2:
          const stepHaveBeenApproved =
            (this.detailAdjustmentResponse.step2.approveList ?? []).filter((approved) => {
              return approved.statusStr === Actions.Approved;
            }).length == 2;

          step.disabled = !stepHaveBeenApproved;
          break;
      }
      return step;
    });
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.detailService.cancelAsyncWaitingResponse();
  }

  onChangeActiveIndex() {
    const timer = setTimeout(() => {
      calculatorSla(this.objSla, this.detailAdjustmentResponse?.slaStepList[this.activeStep]);
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { tab: this.selectedTab, step: this.activeStep },
        queryParamsHandling: 'merge',
        replaceUrl: true,
      });
      clearTimeout(timer);
    }, 200);
  }

  /**
   * Tạo payment lot
   */
  createPaymentLot() {
    this.detailService.createPaymentLot().subscribe();
  }

  isSelectApproveDecision() {
    return (
      this.detailAdjustmentResponse &&
      !!this.decisionMap.size &&
      this.decisionFormControl.value.decision != null &&
      this.decisionFormControl.value.decision === this.decisionMap.get(DECISION_VALUE.APPROVE) &&
      this.checkedDocument.premiumAdjustmentForm
    );
  }

  requestHadOnAttachmentPolicyNumber() {
    return !!this.detailAdjustmentResponse.adjustmentStep.summaryDTO.onAttachmentPolicyNumber;
  }

  canViewAdjustmentBody() {
    return (
      this.isSelectApproveDecision() &&
      this.detailService.marks.isConfirmed &&
      this.requestHadOnAttachmentPolicyNumber()
    );
  }

  canViewPaymentLotAccountSuspended() {
    return (
      this.isSelectApproveDecision() &&
      this.detailService.marks.isConfirmed &&
      !this.requestHadOnAttachmentPolicyNumber()
    );
  }

  markFormAsTouched() {
    this.decisionFormControl.markAllAsTouched();
    this.decisionComp && this.decisionComp.form.markAllAsTouched();
  }

  readonly Roles = Roles;
}
