import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PremiumAdjustmentRoutingModule } from "./premium-adjustment-routing.module";
import { SharedModule } from "@shared/shared.module";
import { COMPONENTS } from "./components";
import { DIALOGS } from "./dialogs";
import { PAGES } from "./pages";
import { PIPES } from "./pipes";
import { DetailRequestSharedModule } from "@detail-request/shared/detail-request-shared.module";
import { RippleModule } from "primeng/ripple";


@NgModule({
  imports: [CommonModule, SharedModule, PremiumAdjustmentRoutingModule, DetailRequestSharedModule, RippleModule],
  declarations: [...COMPONENTS, ...DIALOGS, ...PAGES, PIPES]
})
export class PremiumAdjustmentModule {
}
