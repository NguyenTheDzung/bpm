import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PremiumAdjustmentDetailComponent} from "./pages/premium-adjustment-detail/premium-adjustment-detail.component";

const routes: Routes = [
  {path: '', component: PremiumAdjustmentDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PremiumAdjustmentRoutingModule { }
