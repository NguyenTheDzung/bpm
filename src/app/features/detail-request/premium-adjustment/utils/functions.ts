import { isNull, isNumber, isString } from "lodash";
import { formatCurrency } from "@angular/common";
import * as moment from "moment";
import { Table } from "primeng/table";
import { ColumnWithFilterSort, TFilterMatchMode } from "@detail-request/premium-adjustment/models";
import { FormGroup } from "@angular/forms";
import { FilterMetadata, SortEvent } from "primeng/api";
import { SAP_PIPE } from "@detail-request/premium-adjustment/utils/enum";

export function formatSAPAmount(value: string | number, extra = {showMinusFirst: false, prefix: ""}) {
  const {showMinusFirst, prefix} = extra
  if (isNull(value) || !(isString(value) || isNumber(value))) {
    return
  }
  let amount = String(value)
  let negative = false
  const regexCheckNumber = new RegExp(/[\.\-]+/g)
  if (amount.match(regexCheckNumber)) {
    amount = amount.replace(regexCheckNumber, "")
    negative = true
  }
  try {
    let splitAmount = parseFloat(amount)
    if (!isFinite(splitAmount)) {
      console.error(splitAmount, "Invalid amount")
      return
    }
    return (showMinusFirst && negative ? "-" : "") +
      formatCurrency(splitAmount, "en-US", "", prefix || "VND") +
      (!showMinusFirst && negative ? "-" : "") +
      ` ${prefix}`
  } catch (e) {
    console.error(e)
    return
  }
}

export function formatSAPDate(value: string, format: string, valueFormFormat?: string) {
  const date = moment(value, valueFormFormat)
  if (date.isValid()) {
    return date.format(format)
  } else {
    return
  }
}

export function filterColumn<ColumnType>(table: Table, $event: any, column: ColumnWithFilterSort<ColumnType>) {
  let filterValue: string = $event.target?.value?.trim() ?? ""
  console.log(filterValue)
  switch (column.pipeName) {
    case SAP_PIPE.DATE:
      filterValue = filterValue.split("-").reverse().join("-")
      break;
    case SAP_PIPE.MONEY:
      const isNegative = filterValue.includes("-")
      // @ts-ignore
      const isAmountField = column.fieldName === "amount"
      filterValue = (!isAmountField && isNegative ? "-": "") + (filterValue.replace(new RegExp(/[,-]/g), "")) +  (isAmountField && isNegative ? "-": "")
      break
  }
  // @ts-ignore
  table.filter(filterValue, column.fieldName, TFilterMatchMode.CONTAINS)
}

export function clearColumnFilter<ColumnType>(table: Table, {
  fieldName,
  filterConfig
}: ColumnWithFilterSort<ColumnType>, filterForm: FormGroup) {
  // @ts-ignore
  let col = (table.filters[fieldName] as FilterMetadata)
  col && (col.value = undefined)
  // @ts-ignore
  table.filter(undefined, fieldName, TFilterMatchMode.CONTAINS)
  filterForm.controls[filterConfig.formControlName]?.reset()
}

export function convertSAPNumberToJSNumber(value: string): number | undefined {
  let negative = value.includes("-")
  let newAmount = value.replace(/[,-]/g, "")
  return (negative ? -1 : 1) * parseFloat(newAmount)
}

export function customSort($event: SortEvent, moneyField: string[] = []) {
  $event.data!.sort((data1, data2) => {
    let value1 = data1[$event.field!];
    let value2 = data2[$event.field!];
    let result = null;

    if (moneyField.includes($event.field!) && value1 != undefined && value2 != undefined) {
      value1 = convertSAPNumberToJSNumber(value1)
      value2 = convertSAPNumberToJSNumber(value2)
    }

    if (value1 == null && value2 != null) result = -1;
    else if (value1 != null && value2 == null) result = 1;
    else if (value1 == null && value2 == null) result = 0;
    else if (typeof value1 === 'string' && typeof value2 === 'string') result = value1.localeCompare(value2);
    else result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;

    return $event.order! * result;
  });
}