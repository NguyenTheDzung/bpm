type ValueOf<ValueType> =  ValueType[keyof ValueType]

export const TAB_BUTTON  = {
  PROCESS: 0,
  REPORT: 1,
  ATTACHMENT: 2
} as const
export type TAB_BUTTON = ValueOf<typeof TAB_BUTTON>

export const POLICY_INFO_BUTTON = {
  POLICY_ON_RECEIPT: 0,
  ADJUSTMENT_CONTRACT: 1
}as const
export type POLICY_INFO_BUTTON = ValueOf<typeof POLICY_INFO_BUTTON>

export const ACCOUNT_DISPLAY_TAB_VALUE = {
  CHRONOLOGY: 2,
  DOWNPAYMENT: 1,
  RECEIVABLE: 0
}as const
export type ACCOUNT_DISPLAY_TAB_VALUE = ValueOf<typeof ACCOUNT_DISPLAY_TAB_VALUE>
export const TRAFFIC_LIGHT_CODE =  {
  RECEIVE_PAY: '@08@',
  RECEIVE_OPEN_AND_DUE: '@0A@',
  REVERSED: '@11@',
  CREDIT_OPEN: '@5G@'
} as const
export type TRAFFIC_LIGHT_CODE = ValueOf<typeof TRAFFIC_LIGHT_CODE>

export const STEP =  {
  STEP1: 0,
  STEP2: 1,
  STEP3: 2
}as const
export type STEP = ValueOf<typeof STEP>

export const REPORT_TAB_INDEX ={
  REPORT_PROCESS: 0,
  CONTRACT_RESUMPTION_REPORT: 1
}as const
export type REPORT_TAB_INDEX = ValueOf<typeof REPORT_TAB_INDEX>

export const DECISION_VALUE = {
  PENDING: 'PENDING',
  REJECT: "REJECT",
  APPROVE: 'APPROVE',
} as const
export type DECISION_VALUE = ValueOf<typeof DECISION_VALUE>

export const SAP_PIPE =  {
  MONEY: "MONEY",
  TRAFFIC_LIGHT: "TRAFFIC_LIGHT",
  DATE: "DATE"
}as const
export type SAP_PIPE = ValueOf<typeof SAP_PIPE>

export const PAYMENT_LOT_FREE_STATUS  = "FREE"
export const PAYMENT_LOT_REVERSED_STATUS  = "REVERSED"
// Trong trường hợp allocate có một lot lỗi thì sẽ trả về message này
export const HAVE_ITEM_FAIL_TO_ALLOCATE = "HAVE_ITEM_FAIL_TO_ALLOCATE"
