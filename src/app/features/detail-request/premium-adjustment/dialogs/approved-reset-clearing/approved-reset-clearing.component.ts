import { Component, Injector } from '@angular/core';
import { BaseActionComponent } from "@shared/components";
import { RequestService } from "@requests/services/request.service";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { IReceivable, IResetClearingParamsDialogProvide } from "../../models";


@Component({
  selector: 'app-approved-reset-clearing',
  templateUrl: './approved-reset-clearing.component.html',
  styleUrls: ['./approved-reset-clearing.component.scss']
})
export class ApprovedResetClearingComponent extends BaseActionComponent {

  dataTable: IReceivable[] = []

  constructor(
    inject: Injector,
    private services: RequestService,
    private detailService: DetailPremiumAdjustmentService,
  ) {
    super(inject, services);
    this.dataTable = this.configDialog.data['selection']
  }

  /**
   * @param result
   * **true** -> Đóng popup và reload lại table
   * **false** -> Chỉ đóng popup và không làm gì hết
   */
  closeDialog(result: any = false) {
    if (this.configDialog) {
      this.refDialog.close(result);
    } else {
      this.location.back();
    }
  }

  /**
   * Call Api reset clearing
   */
  resetClearing() {
    this.loadingService.start()
    const [selectedData] = this.dataTable
    const params: IResetClearingParamsDialogProvide = {
      docNumber: selectedData.docNumber,
      docType: selectedData.docType,
      statKey: selectedData?.statisticalKey ?? '',
      trafficLight: selectedData.trafficLight
    }
    this.detailService.resetClearing(params)
      .subscribe(() => {
          this.closeDialog(true)
        }
      )
  }
}
