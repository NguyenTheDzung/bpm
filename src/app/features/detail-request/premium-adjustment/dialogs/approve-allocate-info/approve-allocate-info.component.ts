import { Component, Injector, ViewChild } from "@angular/core";
import { NgModel } from "@angular/forms";
import { RequestService } from "@requests/services/request.service";
import {
  DetailPremiumAdjustmentService
} from "@detail-request/premium-adjustment/services/detail-premium-adjustment.service";
import { BaseActionComponent } from "@shared/components";
import { IAllocateRequestItem, IConfirmedSelectLotList } from "@detail-request/premium-adjustment/models";

type ApproveAllocateTableData ={
  allocateAmount: number,
  remainingAmount: number
} &  IConfirmedSelectLotList

type SelectionType = null | {
  value: ApproveAllocateTableData,
  index: number
}


@Component({
  selector: "app-approve-allocate-info",
  templateUrl: "./approve-allocate-info.component.html",
  styleUrls: ["./approve-allocate-info.component.scss"]
})
export class ApproveAllocateInfoComponent extends BaseActionComponent {
  @ViewChild("allocateModel") allocateModel!: NgModel;

  tableData!: ApproveAllocateTableData[];
  hasItemAllocatedAmount = false;

  allocateAmount: number = 0
  totalAllocated = 0;

  selection: SelectionType
  max!: number


  constructor(
    inject: Injector,
    private services: RequestService,
    private detailService: DetailPremiumAdjustmentService
  ) {
    super(inject, services);

    const paymentLotData = this.configDialog.data.paymentLotData as IConfirmedSelectLotList[]

    this.tableData = this.convertPaymentLotData(paymentLotData)

    this.selection = { value: this.tableData[0], index: 0 };
    this.allocateAmount = this.selection.value.allocateAmount
    this.calculateTotalAllocate();
    this.checkHasGrossClearing();

  }

  reformRemainingAmount(remainingAmount: string) {
   return isFinite(parseInt(remainingAmount)) ? parseInt(remainingAmount) : 0
  }

  convertPaymentLotData(data: IConfirmedSelectLotList[]) {
   return data.map(
      dataItem => {
        const reformRemainingAmount = this.reformRemainingAmount(dataItem.remainingAmount)
        this.totalAllocated += reformRemainingAmount
        return { ...dataItem,
          allocateAmount: reformRemainingAmount,
          remainingAmount: reformRemainingAmount
        } as ApproveAllocateTableData
      }
    )
  }

  /**
   * @param result
   * **true** -> Đóng popup và reload lại table
   * **false** -> Chỉ đóng popup và không làm gì hết
   */
  closeDialog(result = false) {
    if (this.configDialog) {
      this.refDialog.close(result);
    } else {
      this.location.back();
    }
    this.loadingService.complete();
  }

  calculateTotalAllocate() {
    this.totalAllocated = this.tableData.reduce((previousValue, currentValue) => {
      return (previousValue) + (currentValue?.allocateAmount ?? 0);
    }, 0);
  }

  checkHasGrossClearing() {
    this.hasItemAllocatedAmount = !!this.tableData.find(data => !!data.allocateAmount);
  }

  override save() {
    if (!this.selection) {
      return;
    }
    this.allocateModel?.control.markAsTouched();
    if (this.allocateModel.invalid){
      this.messageService.warn('MESSAGE.WARN.INVALID_ALLOCATE_AMOUNT_ACCOUNT_SUSPENDED')
      return;
    }
    this.tableData[this.selection.index].allocateAmount = this.allocateAmount;
    this.calculateTotalAllocate();
    this.checkHasGrossClearing();
  }

  allocate() {
    this.loadingService.start();
    const params: IAllocateRequestItem[] = this.tableData.map(
      ({ allocateAmount, paymentLot, item }) => {
        return {
          allocateAmount: allocateAmount + "",
          paymentLot,
          item
        } as IAllocateRequestItem;
      }
    );
    this.detailService.allocate(params)
      .subscribe(
        {
          next: () => {
            this.closeDialog(true)
          }
        }
      )
  }

  onChangeSelection($event: typeof this.selection | null) {
    if($event?.value){
      this.allocateAmount = $event.value.allocateAmount
    }
    this.selection = $event
  }
}
