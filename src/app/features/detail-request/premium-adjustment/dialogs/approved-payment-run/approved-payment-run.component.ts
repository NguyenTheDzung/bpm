import { Component, Injector } from '@angular/core';
import { BaseActionComponent } from "@shared/components";
import { TranslateService } from "@ngx-translate/core";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { IPaymentRunDialogProvide, IPaymentRunRequestItem, IPolicyViewDTO, IReceivable } from "../../models";

@Component({
  selector: 'app-approved-payment-run',
  templateUrl: './approved-payment-run.component.html',
  styleUrls: ['./approved-payment-run.component.scss']
})
export class ApprovedPaymentRunComponent extends BaseActionComponent {

  tableData: [IReceivable]

  constructor(
    inject: Injector,
    private translate: TranslateService,
    private detailService: DetailPremiumAdjustmentService
  ) {
    super(inject, detailService);

    this.tableData = this.configDialog.data['selection'] ?? []
  }

  /**
   * @param result
   * **true** -> Đóng popup và reload lại table
   * **false** -> Chỉ đóng popup và không làm gì hết
   */
  closeDialog(result = false) {
    if (this.configDialog) {
      this.refDialog.close(result);
    } else {
      this.location.back();
    }
    this.loadingService.complete();
  }

  paymentRun() {
    /**
     * User click button thực hiện xử lý call API update doc, payment run  và refesh lại fpl9
     * Nếu xử lý thành công → thông báo Thành công
     * Nếu xử lý Thất bại → trả message Thất bại tương ứng
     */
    this.loadingService.start()
    const attachmentInfo: IPolicyViewDTO = this.detailService.detailPremiumAdjustment.adjustmentStep.onAttachmentPolicyViewDTO
    const paymentRunItemDTOList: IPaymentRunRequestItem[] =  this.tableData.map(
      (item: IReceivable) => {
        return {
          docNumber: item.docNumber,
          docType: item.docType,
          productGroup: item.productGroup,
          netDate: item.netDueDate,
          amount: item.amount,
          currency: item.currency ?? '',
          statKey: item.statisticalKey,
          clearingDoc: item?.clearDoc,
          clearingDate: item?.clearDate,
          stillOpen: item?.stillOpen
        } as IPaymentRunRequestItem
      }
    )
    const params: IPaymentRunDialogProvide = {
      policyNumber: attachmentInfo.policyInfo.policyNumber,
      bpNumber: attachmentInfo.holderInfoViewDTO.bpNumber,
      paymentRunItemDTOList,
    }
    this.detailService.paymentRun(params)
      .subscribe(
        {
          next: () => {
            this.closeDialog(true)
          }
        }
      )
  }

}
