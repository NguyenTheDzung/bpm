import { Component, Injector, ViewChild } from '@angular/core';
import { RequestService } from "@requests/services/request.service";
import { BaseActionComponent } from "@shared/components";
import { NgModel } from "@angular/forms";
import {
  IAccountMaintenanceDialogProvide,
  IAccountMaintenanceGrossClearing,
  IAccountMaintenanceRequestItem,
  IDownpayment,
  IReceivable
} from "../../models";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";

@Component({
  selector: 'app-approved-account-maintenance',
  templateUrl: './approved-account-maintenance.component.html',
  styleUrls: ['./approved-account-maintenance.component.scss']
})
export class ApprovedAccountMaintenanceComponent extends BaseActionComponent {
  @ViewChild('grossClearingModel') grossClearingModel!: NgModel

  tableData!: ((IDownpayment | IReceivable) & IAccountMaintenanceGrossClearing) []
  hadAnItemGrossClearing = false

  grossClearing?: number
  totalDiff = 0

  selection: {
    value: ((IReceivable | IDownpayment) & IAccountMaintenanceGrossClearing) ,
    index: number
  }
  currentSelectedIndex: number = -1

  max!: number
  min!: number
  isNegative: boolean = false

  contractAccount = this.detailService.detailPremiumAdjustment.adjustmentStep.onAttachmentPolicyViewDTO.policyInfo.policyNumber

  constructor(
    inject: Injector,
    private services: RequestService,
    private detailService: DetailPremiumAdjustmentService
  ) {
    super(inject, services);
    this.tableData = this.configDialog.data.selection
    this.tableData.forEach((value: any) => value.grossClearing = undefined)
    this.tableData.forEach(
      (data, index) => {
        const isNegative = data.amount.includes("-")
        const grossClearing = parseFloat(data.amount.replace("-", ""))
        data.grossClearing = (isNegative ? -1 : 1) * grossClearing
        data.index = index
      }
    )

    this.selection = {value: this.tableData[0], index: 0}

    this.calculateTotalDiff()
    this.checkHasGrossClearing()
    this.onChangeSingleSelection(this.selection)
  }

  /**
   * @param result
   * **true** -> Đóng popup và reload lại table
   * **false** -> Chỉ đóng popup và không làm gì hết
   */
  closeDialog(result = false) {
    if (this.configDialog) {
      this.refDialog.close(result);
    } else {
      this.location.back();
    }
    this.loadingService.complete();
  }

  calculateTotalDiff() {
    this.totalDiff = this.tableData.reduce((previousValue, currentValue) => {
      return (previousValue) + (currentValue?.grossClearing ?? 0)
    }, 0)
  }

  checkHasGrossClearing() {
    this.hadAnItemGrossClearing = !!this.tableData.find(data => !!data.grossClearing)
  }

  override save() {
    if (!this.selection) {
      return
    }
    this.grossClearingModel?.control.markAsTouched()
    if (this.grossClearingModel.invalid || this.currentSelectedIndex === -1 ) return
    this.tableData[this.currentSelectedIndex].grossClearing = this.grossClearing
    this.calculateTotalDiff()
    this.checkHasGrossClearing()
  }

  validateAccountMaintenance() {
    if (!this.hadAnItemGrossClearing) {
      this.messageService.warn('request.detailRequest.premiumAdjustment.notification.account-maintenance-warning-require')
      return false
    }
    if (this.totalDiff !== 0) {
      this.messageService.warn('request.detailRequest.premiumAdjustment.notification.account-maintenance-warning-diff')
      return false
    }
    return true
  }

  accountMaintenance() {
    if (!this.validateAccountMaintenance()) {
      return
    }
    this.loadingService.start()
    const params: IAccountMaintenanceDialogProvide = {
      items: this.tableData.map(
        (value) => {
          const requestItem: IAccountMaintenanceRequestItem = {
            bpNumber: value.bpNumber,
            contract: value.contact,
            contractAccount: this.contractAccount,
            docNumber: value.docNumber,
            netDate: value.netDueDate,
            docType: value.docType,
            docNumberIndex: (value as IReceivable)?.docNumberIndex,
            grossAmount: value.amount,
            grossClearing: String(value.grossClearing ? Math.abs(value.grossClearing) : "") +
              (value.grossClearing && value.grossClearing < 0 ? '-' : '')
          }
          return requestItem
        }
      )
    }
    this.detailService.accountMaintenance(params)
      .subscribe(
        {
          next: () => {
            this.closeDialog(true)
          }
        }
      )
  }

  onChangeSingleSelection(selection: {
    value: ((IReceivable | IDownpayment) & IAccountMaintenanceGrossClearing)
    index: number
  } | null) {
    this.selection = selection!
    this.currentSelectedIndex = selection?.index ?? -1

    if(!selection) return
    const {value, index} = selection

    const amountNegative = value?.amount.includes('-') ?? false
    this.isNegative = amountNegative
    const absAmount = Math.abs(parseInt(value?.amount ?? "0"))
    this.max = amountNegative ? -1 : absAmount
    this.min = amountNegative ? absAmount * -1 : 1
    this.grossClearingModel?.reset(
      value?.grossClearing
    )
    this.grossClearing = value?.grossClearing

  }

  checkGrossClearing() {
    const grossClearing = this.grossClearing
    if (this.selection?.value.amount.includes('-') && grossClearing && grossClearing > this.max) {
      this.grossClearing = -grossClearing
    }
  }
}
