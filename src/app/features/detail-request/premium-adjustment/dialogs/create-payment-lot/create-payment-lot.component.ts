import { Component, Injector } from "@angular/core";
import { BaseActionComponent } from "@shared/components";
import {
  DetailPremiumAdjustmentService
} from "@detail-request/premium-adjustment/services/detail-premium-adjustment.service";
import { DataTable } from "@cores/models/data-table.model";
import { IPaymentLotSuspendedResponse } from "@detail-request/premium-adjustment/models";

@Component({
  selector: "app-create-payment-lot",
  templateUrl: "./create-payment-lot.component.html",
  styleUrls: ["./create-payment-lot.component.scss"]
})
export class CreatePaymentLotComponent extends BaseActionComponent {
  dataTable: DataTable<(IPaymentLotSuspendedResponse & { validToDate: string })>;

  constructor(
    injector: Injector,
    private detailService: DetailPremiumAdjustmentService
  ) {
    super(injector, detailService);
    this.dataTable = {
      content: this.configDialog.data["selection"],
      currentPage: 0,
      size: 10,
      totalElements: 0,
      totalPages: 0,
      first: 0,
      numberOfElements: 0
    }
  }

  closeDialog(result?: any) {
    if (this.configDialog) {
      this.refDialog.close(result);
    } else {
      this.location.back();
    }
    this.loadingService.complete();
  }

  createPaymentLot() {
    const selectedItem = this.dataTable.content.map(
      value =>  {
        return <Parameters<typeof this.detailService.createPaymentLot>[0]> {
          oldItem: value.item,
          oldPaymentLot: value.paymentLot,
          oldStatus: value.status,
          oldUsageText: value.usageText,
          paymentAmount: value.paymentAmount,
          postingDate: value.postingDate,
          validToDate: value.validToDate
        }
      }
    )
    this.detailService.createPaymentLot(selectedItem[0]).subscribe(
      (response) => this.closeDialog(response)
    )
  }
}
