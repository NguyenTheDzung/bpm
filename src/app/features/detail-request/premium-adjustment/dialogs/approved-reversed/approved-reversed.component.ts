import { Component, Injector, ViewChild } from '@angular/core';
import { BaseActionComponent } from "@shared/components";
import { NgModel } from "@angular/forms";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import * as moment from "moment";
import { IReceivable, IReverseParamsDialogProvide } from "../../models";

@Component({
  selector: 'app-approved-reversed',
  templateUrl: './approved-reversed.component.html',
  styleUrls: ['./approved-reversed.component.scss']
})
export class ApprovedReversedComponent extends BaseActionComponent{
  @ViewChild('grossClearingModel') grossClearingModel!: NgModel

  dataTable: [IReceivable]
  grossClearing: number
  max: number

  constructor(
    inject: Injector,
    private detailService: DetailPremiumAdjustmentService
  ) {
    super(inject, detailService);
    this.dataTable = this.configDialog.data['selection'] ?? []
    this.max = Math.abs(parseInt(this.dataTable[0].amount))
    this.grossClearing =
      Math.abs(detailService.detailPremiumAdjustment.adjustmentStep.summaryDTO.adjustmentAmount)
  }

  /**
   * @param result
   * **true** -> Đóng popup và reload lại table
   * **false** -> Chỉ đóng popup và không làm gì hết
   */
  closeDialog(result = false) {
    if (this.configDialog) {
      this.refDialog.close(result);
    } else {
      this.location.back();
    }
    this.loadingService.complete();
  }

  reverse(){
    this.grossClearingModel.control.markAllAsTouched();
    if (this.grossClearingModel.invalid){
      return;
    }

    /**
     * User click button, check field “Số tiền hủy” với field “Amount” từ line được chọn reverse,nếu:
     * @deprecated Bằng nhau → gọi API reverse toàn phần
     * @deprecated Khác nhau → gọi API reverse một phần
     * Tất cả đều gọi vào revert part
     * Nếu xử lý thành công → thông báo Thành công
     * xử lý Thất bại → trả message Thất bại tương ứng
    */
    this.reversePart()
  }

  /**
   * Call API reverse một phần
   */
  reversePart() {
    this.loadingService.start()
    const params: IReverseParamsDialogProvide = {
      docNumber: this.dataTable[0].docNumber,
      docType: this.dataTable[0].docType,
      partAmount: Number(this.grossClearing).toString(),
      netDate: this.dataTable[0].netDueDate,
      clearingDate: moment().format('yyyy-mm-dd'),
      trafficLight: this.dataTable[0].trafficLight
    }
    this.detailService.reverse(params)
      .subscribe(() => this.closeDialog(true))
  }
}
