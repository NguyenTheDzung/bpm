import { ApprovedResetClearingComponent } from "./approved-reset-clearing/approved-reset-clearing.component";
import {
  ApprovedAccountMaintenanceComponent
} from "./approved-account-maintenance/approved-account-maintenance.component";
import { ApprovedPaymentRunComponent } from "./approved-payment-run/approved-payment-run.component";
import { ApprovedReversedComponent } from "./approved-reversed/approved-reversed.component";
import { SubmitPolicyRestoreComponent } from "./submit-policy-restore/submit-policy-restore.component";
import { CreatePaymentLotComponent } from "./create-payment-lot/create-payment-lot.component";
import {
  ApproveAllocateInfoComponent
} from "@detail-request/premium-adjustment/dialogs/approve-allocate-info/approve-allocate-info.component";

export const DIALOGS = [
  ApprovedResetClearingComponent,
  ApprovedAccountMaintenanceComponent,
  ApprovedPaymentRunComponent,
  ApprovedReversedComponent,
  SubmitPolicyRestoreComponent,
  ApproveAllocateInfoComponent,
  CreatePaymentLotComponent
];

