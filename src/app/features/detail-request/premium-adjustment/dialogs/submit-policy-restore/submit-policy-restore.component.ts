import { Component, Injector, OnInit } from "@angular/core";
import { RequestService } from "@requests/services/request.service";
import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, Validators } from "@angular/forms";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { Location } from "@angular/common";
import { LoadingService } from "@cores/services/loading.service";
import { DetailPremiumAdjustmentService } from "../../services/detail-premium-adjustment.service";
import { IFormGroupModel } from "@cores/models/form.model";
import { finalize, map } from "rxjs";
import { BaseActionComponent } from "@shared/components";
import { IDetailAdjustmentResponseModel, IResultAllocate } from "@detail-request/premium-adjustment/models";
import { CollectionService } from "@create-requests/service/collection.service";
import { cleanDataForm, createErrorMessage, validateAllFormFields } from "@cores/utils/functions";

interface IReverseFormValue {
  policyNumber: string;
  mainProductName: string;
  paymentFrequency: string;
  reason: string;
  name: string;
  startDate: string;
  paidToDate: string;
  suggestedContent: string;
}

@Component({
  selector: 'app-submit-policy-restore',
  templateUrl: './submit-policy-restore.component.html',
  styleUrls: ['./submit-policy-restore.component.scss'],
})
export class SubmitPolicyRestoreComponent extends BaseActionComponent implements OnInit {
  item!: IDetailAdjustmentResponseModel
  resultAllocated: IResultAllocate[] = []
  dataTable$: IResultAllocate[] = []
  fpl9Selected: IResultAllocate[] = []
  override form: IFormGroupModel<IReverseFormValue>;
  disableSaveButton$ = this.detailService.marks$.pipe(
    map(({isSubmit}) => {
      return !isSubmit
    })
  )

  constructor(
    inject: Injector,
    private services: RequestService,
    private translate: TranslateService,
    private dynamicDialogConfig: DynamicDialogConfig,
    override refDialog: DynamicDialogRef,
    private location2: Location,
    override fb: FormBuilder,
    override loadingService: LoadingService,
    private detailService: DetailPremiumAdjustmentService,
    private collectionService: CollectionService
  ) {
    super(inject, services);
    this.item = this.configDialog.data.state
    const feeAdjustPolicyViewDTO = this.item.adjustmentStep.feeAdjustPolicyViewDTO;
    const frequency = this.detailService.payerFQCD.find(
      (item) => {
        return item.value === feeAdjustPolicyViewDTO.policyInfo.paymentFrequency
      }
    )?.code
    this.form = this.fb.group({
      policyNumber: [feeAdjustPolicyViewDTO.policyInfo.policyNumber, Validators.required],
      mainProductName: [feeAdjustPolicyViewDTO.policyInfo.mainProductName],
      paymentFrequency: [
        frequency ? this.translate.instant(`frequency.${frequency}`) : ''
      ],
      reason: [null, [Validators.required, Validators.maxLength(1000)]],
      name: [feeAdjustPolicyViewDTO.holderInfoViewDTO.name],
      startDate: [feeAdjustPolicyViewDTO.policyInfo.startDate],
      paidToDate: [feeAdjustPolicyViewDTO.policyInfo.paidToDate],
      suggestedContent: [null, [Validators.required, Validators.maxLength(1000)]],
    }) as IFormGroupModel<IReverseFormValue>;
  }

  ngOnInit() {
    this.loadingService.start();
    this.detailService.resultAllocated$.subscribe(value => this.resultAllocated = value)
    const info = {
      paymentLot: 'X',
      receivables: '',
      downPayment: '',
      chronology: '',
      page: 0,
      pageSize: 99999,
      policyNumber: this.form.getRawValue().policyNumber,
    };
    this.collectionService.getInfoIntegration(info).subscribe({
      next: (res) => {
        this.dataTable$ = res.data?.paymentLot
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  closeDialog() {
    if (this.dynamicDialogConfig) {
      this.refDialog.close(false);
    } else {
      this.location2.back();
    }
    this.loadingService.complete();
  }

  submit() {
    validateAllFormFields(this.form)
    if (this.form?.invalid) {
      return;
    }
    if (this.fpl9Selected.length == 0) {
      return this.messageService.warn(
        'request.detailRequest.premiumAdjustment.notification.lapse'
      )
    }
    this.messageService?.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        const data = cleanDataForm(this.form);
        data.otherRequestType = '07'
        data.feeAttachmentRequestId = this.configDialog.data.state.requestId
        const lastData = {
          requestType: '10',
          reinstatementPolicyRequestCreateDTO: {
            ...data,
            fpl9dto: this.fpl9Selected
          }
        }
        this.collectionService.createApiCollection(lastData)
          .pipe(finalize(() => this.loadingService.complete()))
          .subscribe({
            next: () => {
              this.refDialog.close(true);
              this.messageService.success('MESSAGE.SUCCESS');
            },
            error: (err) => {
              this.messageService.error(createErrorMessage(err));
            }
          })
      }
    });
  }
}
