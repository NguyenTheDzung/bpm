import { IFormGroupModel } from "@cores/models/form.model";

/**
 * Kiểm tra chứng từ model
 */
export interface ICheckDocumentValue {

  //Đơn điều chỉnh phí hợp đồng
  premiumAdjustmentForm: boolean

  //Chứng từ nộp tiền (nếu có)
  paymentAttachment: boolean

  //Chứng từ ủy quyền nộp tiền (Nếu có)
  paymentAttachmentAuthorization: boolean
}


export interface IDecisionFormValue {
  decision: number | null,
  reason: string | null,
}

export interface IBusinessProcessData {
  id: number,
  processing: string
  businessProcessNumber: number
  appNo: number
  businessProcessDecs: string
  appDate: string
  appRecDate: string
  effectiveDate: string
  startOn: string
  startFrom: string
}


export interface IDownpaymentData{
  status: string
  contract: string
  PG: string
  statkey: string
  docNo: string
  billPeriod: string
  billPerTo: string
  DT: string
  netDate: string
  amount: string
  stillOpen: string
  clearDate: string
  clearDoc: string
  CR: string
  Crcy: string
}

export interface IAccountMaintenanceGrossClearing{
  grossClearing?: number,
  index: number
}

export interface ColumnModel<FieldName = string,PipeName = string> {
  name: string,
  fieldName: FieldName,
  pipeName?: PipeName
}
export interface ColumnWithFilterModel<FieldName = string, PipeName = string> extends ColumnModel<FieldName ,PipeName>{
  filterConfig: {
    inputType: InputFilterType
    formControlName: string,
    /**
     * Dựa theo filterMatchMode của primeNg
     */
    filterMatchMode: TFilterMatchMode
    options?: any
  }
}

export interface ColumnWithSort<FieldName = string,PipeName = string> extends ColumnModel<FieldName, PipeName> {
  sort: boolean
}

export interface ColumnWithFilterSort<FieldName = string,PipeName = string> extends ColumnWithFilterModel<FieldName,PipeName>, ColumnWithSort<FieldName,PipeName> {
  styleClass?: string,
  toolTips?: boolean
}


export enum InputFilterType {
  Number,
  Text,
  Date,
  Dropdown
}

/**
 * @class FilterMatchMode
 * Dựa theo filterMatchMode của primeNg
 */

export enum TFilterMatchMode {
  STARTS_WITH = "startsWith",
  CONTAINS = "contains",
  NOT_CONTAINS = "notContains",
  ENDS_WITH = "endsWith",
  EQUALS = "equals",
  NOT_EQUALS = "notEquals",
  IN = "in",
  LESS_THAN = "lt",
  LESS_THAN_OR_EQUAL_TO = "lte",
  GREATER_THAN = "gt",
  GREATER_THAN_OR_EQUAL_TO = "gte",
  BETWEEN = "between",
  IS = "is",
  IS_NOT = "isNot",
  BEFORE = "before",
  AFTER = "after",
  DATE_IS = "dateIs",
  DATE_IS_NOT = "dateIsNot",
  DATE_BEFORE = "dateBefore",
  DATE_AFTER = "dateAfter",
}

/**
 * Model kiểm tra các hành động đã thực hiện của user
 *
 * Example: isSubmit -> Yêu cầu đã được nộp hay chưa
 */
export interface IMarkResults {
  isReverse: boolean
  isSubmit: boolean

  isAllocate: boolean
  /**
   * Có phải là PIC (Personal in charge) của yêu cầu hay ko
   */
  isPIC: boolean
  isResetClearing: boolean
  isAccountMaintenance: boolean
  isPaymentRun: boolean
  isCreatePaymentLot: boolean
  isConfirmed: boolean
  haveItemAllocateFailed: boolean
}

export interface IPaymentLotAccountSuspendedValue {
  usageText: string,
  paymentAmount: number,
  fromDate: Date,
  toDate: Date
}

export interface IPaymentLotAccountSuspendedForm extends IFormGroupModel<IPaymentLotAccountSuspendedValue> {
}




