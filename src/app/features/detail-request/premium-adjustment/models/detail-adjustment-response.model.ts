import { Attachment, AttachmentSAP } from "@detail-request/shared/models/detail-request.model";
import {
  CollectionDetailRequestModel,
  PendingHistoryCollectionModel
} from "@detail-request/shared/models/collection-detail.model";
import { HttpStatusCode } from "@angular/common/http";

/**
 *  Tóm tắt yêu cầu
 */
export interface ISummaryDTO {
  /**
   * Số HĐBH trên chứng từ nộp tiền
   */
  onAttachmentPolicyNumber?: string

  /**
   * Số HĐBH trên chứng từ nộp tiền khi tạo
   */
  initOnAttachmentPolicyNumber?: string

  /**
   * Số HĐBH đề nghị điều chỉnh
   */
  feeAdjustPolicyNumber: number

  /**
   *  BMBH của HĐBH đề nghị điều chỉnh
   */
  policyHolderName: string

  /**
   * Số HĐBH trên chứng từ nộp tiền
   */
  onAttachmentAmount: number

  /**
   * Số phí đề nghị điều chỉnh
   */
  adjustmentAmount: number

  /**
   * Số ticket
   */
  crmTicket: number

  /**
   * Ngày ký đơn
   */
  signDate: string
  /**
   * Email CVTV/ CC Email
   */
  icEmail: string
}

export interface IPolicyViewDTO {
  /**
   * Thông tin hợp đồng
   */
  policyInfo: IPolicyInfo
  /**
   * Thông tin của holder
   */
  holderInfoViewDTO: IHolderInfoViewDTO
  /**
   * Thông tin CTV
   */
  icInfoViewDTO: IIcInfoViewDTO
}

export interface IProcessingInfoDTO {
  pic: string
  icCode: string
  icName: string
  distributeChannel: string
  resolution: string
  statusPolicy: string
  processInstanceId: string
  isProcessed: boolean
  /**
   * @description Decision là quyết định của yêu cầu
   * @description Decision được định nghĩa trong danh mục dùng chung
   * @description Decision có thể không được trả về
   */
  decision?: number
  premiumAdjustmentForm: boolean
  paymentAttachment: boolean
  paymentAttachmentAuthorization: boolean

  isReverse: boolean
  isResetClearing: boolean
  isAccountMaintenance: boolean
  isPaymentRun: boolean
  isCreatePaymentLot: boolean
  isAllocate: boolean
  isSubmit: boolean
  isConfirmed: boolean

  haveItemAllocateFailed: boolean

  decisionReason?: string

  selectedPaymentLots: IConfirmedSelectLotList[]
}



/**
 * Thông tin hợp đồng
 */
export interface IPolicyInfo {
  /**
   * Số HĐBH
   */
  policyNumber: string
  applicationNumber: string
  /**
   * Tên sản phẩm chính
   */
  mainProductName: string
  /**
   * Trạng thái
   */
  status: string
  startDate: string
  paidToDate: string
  ackDate: string
  freeLookEndDate: string
  paymentFrequency: string
}

export interface IHolderInfoViewDTO {
  bpNumber: string
  name: string
  email: string
  phoneNum: string
  gender: string
  dob: string
  idNum: string
  idInstitute: string
  idType: string
  idIssueDate: string
}

/**
 * Thông tin CTV
 */
export interface IIcInfoViewDTO {
  bpNumber: string
  /**
   * Mã CTV
   */
  icNumber: string
  name: string
  email: string
  phoneNum: string
  channel: string
  subChannel: string
}

export interface IAdjustmentStep {
  summaryDTO: ISummaryDTO
  /**
   * Thông tin hợp đồng trên chứng từ
   */
  onAttachmentPolicyViewDTO: IPolicyViewDTO
  /**
   * Thông tin hợp đồng cần điều chỉnh
   */
  feeAdjustPolicyViewDTO: IPolicyViewDTO
  /**
   * Thông tin về các thao tác trên màn hình
   * Bao gồm disable các button cùng với các thao tác được cho phép hay không
   */
  processingInfoDTO: IProcessingInfoDTO
}

export interface IDetailAdjustmentResponseModel extends CollectionDetailRequestModel{
  requestId: string
  adjustmentStep: IAdjustmentStep
  attachmentFromSAP: AttachmentSAP[]
  attachmentFromBPM: Attachment[]
  pendingHistories?: PendingHistoryCollectionModel[]
  isReinstatement: boolean
  sla: number
  slaStep: number
  // example date: 2023-05-17T15:30:00
  slaStartDate: string
  slaEndDate: string
  status: number
  // step2?: IDetailPremiumAdjustmentStep2,
  // step1?: IDetailPremiumAdjusmentStep1,
  slaStepList: ISLAList[],
  slaPercent: number,
  slaStepPercent: number ,
  slaStepMinutesRemaining: number ,
  step: number ,
  slaResolution: string,
  slaStepMessage: string,
  slaPercentWarning: number 
}

export interface ISLAList {
  slaResolution: string ,
  slaPercent: number,
  slaStep: number,
  slaStepPercent: number,
  slaStepMessage: string ,
  slaStepMinutesRemaining: number,
  slaPercentWarning: number,
  step: number
}

export interface IDetailPremiumAdjusmentStep1 {
  requestId: string,
  typeCode: string,
  channel: string,
  onAttachmentPolicyNumber: string,
  feeAdjustPolicyNumber: string,
  policyHolderName: string,
  onAttachmentAmount: number,
  adjustmentAmount: number,
  // 2023-06-01
  signDate: string,
  decision: number,
  icEmail: string
}

export interface IDetailPremiumAdjustmentStep2 {
  channel?: string,
  policyNumber?: string,
  longText?: string,
  //2022-09-24
  effectiveDate?: string,
  reason?: string,
  pic?: string,
  distributeChannel?: string,
  resolution?: string,
  statusPolicy?: string,
  customerName?: string,
  periodicFeePayment?:string,
  paidDate?: string,
  requestTypeCode?: string,
  requestId?: string,
  type?: number,
  picPosition?: string,
  // 2023-06-20
  createdDate?: string,
  picEmail?:  string,
  approveList?: IApprovedList[],
  suggestedContent?: string,
  requestResolution?: string,
  fplReinstatements: IFplReinstatements[]
}

export interface IApprovedList {
  createdBy: string,
  lastUpdatedBy: string,
  id: number,
  step: number,
  userName: string,
  userCode: string,
  requestId: string,
  policyNumber: string
}

export interface IFplReinstatements {
  id: number,
  infof: string,
  transactionContent: string,
  netDueDate: string,
  amount: string,
  requestId: string,
  paymentLot: string,
  item: string,
  documentNo: string
}

export interface IReceivable {
  bpNumber: string
  policyNumber: string
  contact: string
  productGroup: string
  statisticalKey: string
  docNumber: string
  billPeriod: string
  billPeriodTo: string
  docType: string
  netDueDate: string
  amount: string
  stillOpen: string
  currency: string
  clearDate: string
  clearDoc: string
  clearReason: string
  trafficLight: string
  docNumberIndex: string
}

export interface IReceivableResponse {
  receivables: IReceivable[]
  status: HttpStatusCode
}

export interface IChronologyResponse {
  chronology: IChronology[]
  status: HttpStatusCode
}
export interface IDownPaymentResponse {
  downPayment: IDownpayment[]
  status: HttpStatusCode
}

export interface IChronology {
  docNumber: string,
  origin: string,
  createdBy: string,
  entryDate: string,
  entryAt: string,
  postingDate: string,
  netDueDate: string,
  originName: string,
  creditMemo?: string,
  created?: string,
  paymentLot: string,
  paymentLotItem: string
  amount: string
  currency: string
  debit: string
  usageText: string
  ft: string,
}

export interface IBusinessProcessResponse {
  applDt: string
  applicationCd: string
  applicationResume: string
  applinDt: string
  bizprcTt: string
  bizprcnoId: string | number
  effectiveDt: string
  externalRunId: string
  journalStatCd: number
  modDt: string
  modNameTt: string
  modTs: string
}

export interface IBusinessProcessingResponse {
  processId: string
  polEditFg: string
  orderNoId: string
  journalNoId: string
  applicationNumber: string
  dateCd: string
  processingTt: string
  validFromDate: string
  intBeginDate: string
  intEndDate: string
  updateTypeId: string
  fineCtrlId: string
  modDt: string
  modNameTt: string
  procCatTt: string
  iscsReleventFg: string
  lapseReasonTt: string
}

export interface IAllResultsResponse {
  reverseItems: IResultReverse[]
  allocateItems: IResultAllocate[]
  resetClearingItems: IResultResetClearing[],
  accountMaintenanceItems: IResultAccountMaintenance[],
  paymentRunItems: IResultPaymentRun[],
  paymentLotItems: IResultPaymentLot[],
  selectLotItems: IResultSelectLotItem[]
}

export interface IResultSelectLotItem {
  id: number
  requestId: string
  processItemType: string
  amount: string
  amountStr: string
  netDateStr: string
  clearingDateStr: string
  executeDate: string
  executeBy: string
  executeDateStr: string
  grossAmountStr: string
  grossClearingStr: string
  postingDate: string
  postingDateStr: string
  paymentLot: string
  paymentLotItem: string
  paymentAmount: string
  paymentAmountStr: string
  usageText: string
  reversedAmountStr: string
  remainingAmount: string,
  validToDate: string
}

export interface IResultReverse {
  id: number
  requestId: string
  processItemType: 'REVERSE'
  isExecuted: boolean
  docNumber: string
  executeDate: string
  paymentLot: string
  paymentLotItem: string
  reversedAmount: string
}

export interface IResultResetClearing {
  id: string
  requestId: string
  processItemType: "RESET_CLEARING"
  docNumber: string
  docType: string
  statKey: string
  amount: string
  netDate: string
  clearingDoc: string
  executeDate: string
}

export interface IResultAccountMaintenance {
  id: number,
  requestId: string,
  processItemType: "ACCOUNT_MAINTENANCE",
  docNumber: string,
  executeDate: string,
  docType: string,
  amount: string,
  netDate: string,
  executeDateStr: string
  bpNumber : string
  contract : string
  contractAccount : string
  grossAmount : string
  grossClearing : string
  executeBy : string
}

export interface IResultPaymentRun {
  id: number,
  requestId: string,
  processItemType: "PAYMENT_RUN",

  docNumber:  string,
  produceGroup:  string,
  statKey?: string,
  docType:  string,
  netDate: string,
  amount: string,
  stillOpen: string,
  // clearDate?: string
  // clearDoc?: string

  currency:  string,
  executeDate:  string,
  executeDateStr:  string,
  bpNumber:  string,
  policyNumber:  string
}

export interface IResultPaymentLot {
  requestId : string
  processItemType : string
  postingDate : string
  selectionCat : string
  bankClearingAccount : string
  paymentLot : string
  paymentLotItem : string
  amount : string
  usageText: string
  executeDateSt: string
  id: number
}

export interface IResultAllocate {
  id: number
  netDate: string
  requestId: string
  processItemType: 'ALLOCATE'
  amount: string
  executeDate: string
  parentItemId: number,
  paymentLot: string,
  paymentLotItem: string
  docAllocated: string
}

export interface IDownpayment {
  statisticalKey: string,
  clearDate: string,
  clearDoc: string,
  clearReason: string,
  bpNumber: string,
  contact: string,
  productGroup: string,
  docNumber: string,
  docType: string,
  netDueDate: string,
  amount: string,
  trafficLight: string,
  currency?: string
}

export interface IPaymentLotSuspendedResponse {
  valut: string; // ValidToDate
  paymentLotItem: string
  paymentLot: string
  item: string
  paymentAmount: string
  remainingAmount: string
  postingDate: string
  createDate: string
  category: string
  dailyNote: string
  dailyNextAction: string
  weeklyNote: string
  usageText: string
  status: string
  policyAllocated?: string
}

export interface IConfirmedSelectLotList{
  id: number
  requestId: string
  paymentLot: string
  item: string
  postingDate: string
  usageText: string
  paymentAmount: string
  remainingAmount: string
  status: string,
  validToDate: string
  oldPaymentLot: string,
  oldItem: string,
  currency?: string
}
export interface ICreatePaymentLotResult {
  paymentLotResult: boolean
  selectedPaymentLots: IConfirmedSelectLotList[]
}

export interface IAllocateResponse {
  allocateResult: boolean;
  haveItemAllocateFailed: boolean
  failedList: string[]
}
