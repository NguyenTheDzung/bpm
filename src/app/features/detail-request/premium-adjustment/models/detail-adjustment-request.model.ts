import { TPageableSearchParams } from "@cores/models/response.model";

export interface IGetAccountDisplayChronology {
  requestId: string,
  docNo?: string,
  clearDoc?: string
}

export interface ISAPOpenShortcutRequestParams {
  // Số hợp đồng
  policyNumber: string
  //Date format DD.MM.YYYY
  startDate: string
  //Date format DD.MM.YYYY
  endDate: string
}

export interface IReverseDocumentRequestModel extends IReverseParamsDialogProvide, TSaveStateWithDecisionApproved {
}

export interface IReverseParamsDialogProvide {
  netDate: string,
  docNumber: string,
  partAmount: string,
  clearingDate: string,
  docType: string
  trafficLight: string,
}

export interface IResetClearingRequest extends IResetClearingParamsDialogProvide, TSaveStateWithDecisionApproved {
}

export interface IResetClearingParamsDialogProvide {
  docNumber: string,
  docType: string,
  statKey: string,
  trafficLight: string,
}

export interface IAccountMaintenanceRequest extends IAccountMaintenanceDialogProvide, TSaveStateWithDecisionApproved {
}

export interface IAccountMaintenanceDialogProvide {
  items: IAccountMaintenanceRequestItem[]
}

export interface IAccountMaintenanceRequestItem {
  docNumber: string,
  netDate: string,
  docType: string,

  grossAmount: string
  grossClearing: string
  docNumberIndex: string

  bpNumber: string
  contract: string
  contractAccount: string
}

export interface IPaymentRunRequest extends IPaymentRunDialogProvide, TSaveStateWithDecisionApproved {
}

export interface IPaymentRunDialogProvide {
  policyNumber: string,
  bpNumber: string,
  paymentRunItemDTOList: IPaymentRunRequestItem[],
}

export interface IPaymentRunRequestItem {
  docNumber: string,
  docType: string,
  productGroup: string,
  netDate: string,
  amount: string,
  currency: string,
  stillOpen?: string,
  clearingDoc?: string
  statKey: string,
  clearingDate?: string
}

export interface IAllocateRequest {
  requestId: string,
  /**
   * Chỉ dành cho allocate payment lot treo
   */
  allocateRequestItemList?: IAllocateRequestItem[]
}

export interface IAllocateRequestItem {
  paymentLot: string,
  item: string,
  allocateAmount: string
}

export type TAccountDisplayTableRequest = TPageableSearchParams<{ requestId: string }>

export interface ICreatePaymentLotRequest {
  requestId: string,
}

export interface ICreatePaymentLotSuspended{
  paymentAmount: string
  oldPaymentLot: string
  oldItem: string
  oldStatus: string
  oldUsageText: string
  postingDate: string
  validToDate: string
}

export interface IGetBusinessProcessRequest extends TPageableSearchParams<{ requestId: string }> {
}

export interface IGetProcessingRequest {
  requestId: string,
  applicationCd: string,
  entryOptionId: string
}

export interface IPremiumAdjustmentDecisionRequest {
  requestId: string,
  decision: number,
  decisionReason: string,
}

export type TSaveStateWithDecisionApproved = Omit<ISaveStateRequest, "decision" | "decisionReason">

export interface ISaveStateRequest {
  requestId: string,
  decision: number,
  decisionReason: string,
  premiumAdjustmentForm: boolean,
  paymentAttachment: boolean,
  paymentAttachmentAuthorization: boolean
  onAttachmentPolicyNumber?: string
}


export interface IConfirmRequestInformationRequest {
  // Mặc định là approve
  decision: number,
  onAttachmentPolicyNumber?: string,
  requestId: string,
  premiumAdjustmentForm: boolean,
  paymentAttachment: boolean,
  paymentAttachmentAuthorization: boolean
}

export interface IAccountSuspendedRequest{
  requestId: string
  paymentAmount: string,
  usageText: string
  postingDateFrom: string,
  postingDateTo: string,
}
export interface ISearchPaymentLotRequest {
  requestId: string
  selectedLotDTOList: ISelectedLotDtoList[]
}

export interface ISelectedLotDtoList {
  paymentLot: string
  item: string
  postingDate: string
  usageText: string
  paymentAmount: string
  remainingAmount: string
  currency: string
  validToDate: string
  status: string
  policyAllocated?: string
}



export interface IDeleteLotList {
  requestId: string,
  deselectIdList: number[]
}

