import { Directive, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from "@angular/core";
import {
  DetailPremiumAdjustmentService
} from "@detail-request/premium-adjustment/services/detail-premium-adjustment.service";
import { Table } from "primeng/table";
import {
  ColumnWithFilterSort,
  TAccountDisplayTableRequest,
  TFilterMatchMode
} from "@detail-request/premium-adjustment/models";
import { FormBuilder, FormGroup } from "@angular/forms";
import { PaginatorModel } from "@cores/models/paginator.model";
import { map, Observable, of, shareReplay } from "rxjs";
import { SAP_PIPE } from "../utils/enum";
import { DataTable } from "@cores/models/data-table.model";
import { LoadingService } from "@cores/services/loading.service";
import { ActivatedRoute } from "@angular/router";
import { clearColumnFilter, customSort, filterColumn } from "../utils/functions";
import { SortEvent } from "primeng/api";


@Directive()
export abstract class Fpl9Table<TableType = any> implements OnInit{

  @ViewChild('table') tb!: Table

  @Input() showFilter: boolean = true;
  @Input() selection: TableType[] = []
  @Output() selectionChange = new EventEmitter<TableType[]>()

  abstract showCheckBox: boolean

  dataTable: DataTable<TableType> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };


  filterForm!: FormGroup;

  @Input('tableData') set tableData(value: TableType[]) {
    if (!value || value.length > 0) {
      this.dataTable.content = value
    }
  }

  private _isTabView = false
  @Input('isTabView') set isTabView(value: boolean) {
    if (value) this.callAPI(true)
    this._isTabView = value
  }

  get isTabView() {
    return this._isTabView
  }

  disableCheckBox$: Observable<boolean> = of(false)
  readonly SAP_PIPE = SAP_PIPE;
  abstract columns: ColumnWithFilterSort<keyof TableType, SAP_PIPE>[]

  abstract initFilterForm(): FormGroup

  protected detailService: DetailPremiumAdjustmentService
  protected loadingService: LoadingService
  protected route: ActivatedRoute
  protected fb: FormBuilder

  constructor(protected injector: Injector) {
    this.detailService = injector.get(DetailPremiumAdjustmentService)
    this.loadingService = injector.get(LoadingService)
    this.route = injector.get(ActivatedRoute)
    this.fb = injector.get(FormBuilder)
  }


  ngOnInit() {
    this.disableCheckBox$ = this.detailService.marks$.pipe(
      map((marks) => {
        return marks.isAllocate || marks.isCreatePaymentLot || marks.isSubmit || !marks.isPIC
      }),
      shareReplay()
    )
    this.filterForm = this.initFilterForm()
    this.isTabView && this.detailService.resetAccountDisplayChange().subscribe(() => this.reset())
    this.isTabView && this.detailService.clearFilterAccountDisplayChange().subscribe(() => this.clearFilter())
  }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
  }

  callAPI(firstPage: boolean) {
    this.loadingService.start();
    if (firstPage) this.dataTable.currentPage = 0;
    const params: TAccountDisplayTableRequest = {
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
      requestId: this.detailService.detailPremiumAdjustment.requestId
    }
    this.callApiGetData(params)
      .subscribe({
        next: (response) => {
          const data = response
          this.dataTable = {
            currentPage: 0,
            first: 0,
            numberOfElements: data.length,
            size: 10,
            totalElements: data.length,
            totalPages: Math.round(data.length / 10),
            content: data
          }
        }
      });
  }

  abstract callApiGetData(params: TAccountDisplayTableRequest): Observable<TableType[]>

  changeSelection($event: TableType[]) {
    this.selectionChange.emit($event)
  }

  filterColumn(table: Table, $event: any, column: typeof this.columns[0]) {
    filterColumn(table, $event, column)
  }

  clearColumnFilter(table: Table, column:  typeof this.columns[0]) {
    clearColumnFilter(table,column, this.filterForm)
  }

  reset() {
    this.clearFilter()
    this.selection = []
    this.selectionChange.emit([])
    this.callAPI(true)
  }

  clearFilter() {
    this.columns.forEach(
      (value) => {
        this.tb.filter(undefined, <string>value.fieldName, TFilterMatchMode.CONTAINS)
      }
    )
    this.tb.reset()
    this.filterForm.reset()
  }

  customSort($event: SortEvent) {
    customSort($event, this.moneyField)
  }

  abstract get moneyField() : string[]

}