import { EventEmitter, Injectable } from "@angular/core";
import {
  BehaviorSubject,
  combineLatest,
  delay,
  filter,
  finalize,
  map,
  Observable,
  OperatorFunction,
  repeatWhen,
  scan,
  skipWhile,
  Subject,
  switchMap,
  takeUntil,
  tap
} from "rxjs";
import { BaseService } from "@cores/services/base.service";
import { HttpClient, HttpErrorResponse, HttpStatusCode } from "@angular/common/http";
import { environment } from "@env";
import { DECISION_VALUE } from "../utils/enum";
import { IPageableResponseModel, IResponseModel, TPageableSearchParams } from "@cores/models/response.model";
import { CommonCategoryService } from "@cores/services/common-category.service";
import {
  IAccountMaintenanceDialogProvide,
  IAccountMaintenanceRequest,
  IAccountSuspendedRequest,
  IAllocateRequest,
  IAllocateRequestItem,
  IAllocateResponse,
  IAllResultsResponse,
  IBusinessProcessingResponse,
  IBusinessProcessResponse,
  ICheckDocumentValue,
  IChronologyResponse,
  IConfirmedSelectLotList,
  IConfirmRequestInformationRequest,
  ICreatePaymentLotRequest,
  ICreatePaymentLotResult,
  ICreatePaymentLotSuspended,
  IDeleteLotList,
  IDetailAdjustmentResponseModel,
  IDownPaymentResponse,
  IGetAccountDisplayChronology,
  IGetBusinessProcessRequest,
  IGetProcessingRequest,
  IMarkResults,
  IPaymentLotSuspendedResponse,
  IPaymentRunDialogProvide,
  IPaymentRunRequest,
  IPremiumAdjustmentDecisionRequest,
  IProcessingInfoDTO,
  IReceivableResponse,
  IResetClearingParamsDialogProvide,
  IResetClearingRequest,
  IResultAccountMaintenance,
  IResultAllocate,
  IResultPaymentLot,
  IResultPaymentRun,
  IResultResetClearing,
  IResultReverse,
  IResultSelectLotItem,
  IReverseDocumentRequestModel,
  IReverseParamsDialogProvide,
  ISAPOpenShortcutRequestParams,
  ISaveStateRequest,
  ISearchPaymentLotRequest,
  TAccountDisplayTableRequest
} from "../models";
import { NotificationMessageService } from "@cores/services/message.service";
import { AuthService } from "@cores/services/auth.service";
import { DetailRequestModel } from "@detail-request/shared/models/detail-request.model";
import {
  COLLECTION_DECISION_CONTRACT_STORE,
  INTERNAL_ERROR_MESSAGE,
  PAYFRQ_CD,
  PREMIUM_ADJUSTMENT_DECISION,
  SAP_IDENTIFICATION_TYPE_CODE
} from "@cores/utils/constants";
import { CommonModel } from "@common-category/models/common-category.model";
import { TranslateService } from "@ngx-translate/core";
import { mapCommonCategoryByCode, mapDataTableWithType } from "@cores/utils/functions";

@Injectable()
export class DetailPremiumAdjustmentService extends BaseService {
  private _resultReverse$ = new BehaviorSubject<IResultReverse[]>([]);
  private _resultResetClearing$ = new BehaviorSubject<IResultResetClearing[]>([]);
  private _resultAccountMaintenance$ = new BehaviorSubject<IResultAccountMaintenance[]>([]);
  private _resultPaymentRun$ = new BehaviorSubject<IResultPaymentRun[]>([]);
  private _resultPaymentLot$ = new BehaviorSubject<IResultPaymentLot[]>([]);
  private _resultAllocated$ = new BehaviorSubject<IResultAllocate[]>([]);
  private _resultSelectLot$ = new BehaviorSubject<IResultSelectLotItem[]>([]);
  /**
   * Thông tin chi tiết điều chỉnh phí
   * @private
   */
  private _detailPremiumAdjustment$ = new BehaviorSubject<IDetailAdjustmentResponseModel>(<any>null);
  /**
   *  Reset table màn account display (3 TAB)
   */
  private _resetAccountDisplay = new EventEmitter();
  /**
   * Bỏ filter màn account display (3 TAB)
   */
  private _clearFilterAccountDisplay = new EventEmitter();

  /**
   * Giá trị checkbox ở phần kiểm tra chứng từ
   */
  checkDocumentChecked: ICheckDocumentValue = {
    premiumAdjustmentForm: false,
    paymentAttachment: false,
    paymentAttachmentAuthorization: false
  };

  /**
   * Quyết định của bên nghiệp vụ (Chấp thuận, Yêu cầu nộp bổ sung, Từ chối)
   */
  decision: number | null = null;
  /**
   * Thông tin về các quyết định lấy từ danh mục dùng chung
   */
  decisionMap = new Map<DECISION_VALUE, number>();

  /**
   * Thông tin về payerFQCD
   */
  payerFQCD: CommonModel[] = [];

  /**
   * Đánh dấu người dùng đã thực hiện các hành động gì
   */
  private _marks$ = new BehaviorSubject<IMarkResults>({
    isAccountMaintenance: false,
    isAllocate: false,
    isCreatePaymentLot: false,
    isPIC: false,
    isPaymentRun: false,
    isResetClearing: false,
    isReverse: false,
    isSubmit: false,
    isConfirmed: false,
    haveItemAllocateFailed: false
  });

  /**
   * Thông tin giấy tờ tùy thân lấy ra từ danh mục dùng chung
   */
  private _commonIdentificationTypeCode: CommonModel[] = [];

  /**
   * Hủy chờ async dữ liệu
   */
  private cancelAsyncWaitingResponse$ = new Subject<void>();

  /**
   * Kiểm soát loading bên trong service
   */
  private _loading$ = new BehaviorSubject<number>(0);
  /**
   * js private #
   * @private
   */
  private loadingCount$ = this._loading$.pipe(
    scan((acc, value) => acc + value),
    map((value) => {
      return !!value;
    })
  );

  constructor(
    override http: HttpClient,
    private common: CommonCategoryService,
    private messageService: NotificationMessageService,
    private authService: AuthService,
    private translateService: TranslateService
  ) {
    super(http, `${environment.collection_url}/insurance-adjustment`);
  }

  get loadingCount() {
    return this.loadingCount$;
  }

  loading() {
    this._loading$.next(1);
  }

  stopLoading() {
    this._loading$.next(-1);
  }

  cancelAsyncWaitingResponse() {
    this.cancelAsyncWaitingResponse$.next();
  }

  get detailPremiumAdjustment() {
    return this._detailPremiumAdjustment$.getValue();
  }

  get detailPremiumAdjustment$() {
    return this._detailPremiumAdjustment$.pipe(filter((value) => value != null));
  }

  get haveResult$() {
    return combineLatest([
      this.resultReverse$,
      this.resultAccountMaintenance$,
      this.resultPaymentRun$,
      this.resultResetClearing$,
      this.resultPaymentLot$,
      this.resultAllocated$,
      this.resultSelectLot$
    ]).pipe(
      filter((nextValue) => {
        return !!nextValue.find(arr => !!arr.length);
      }),
      map(() => true)
    );
  }

  get resultPaymentRun$(): Observable<IResultPaymentRun[]> {
    return this._resultPaymentRun$.asObservable();
  }

  get resultAccountMaintenance$(): Observable<IResultAccountMaintenance[]> {
    return this._resultAccountMaintenance$.asObservable();
  }

  get resultResetClearing$() {
    return this._resultResetClearing$.asObservable();
  }

  get resultReverse$() {
    return this._resultReverse$.asObservable();
  }

  get resultPaymentLot$() {
    return this._resultPaymentLot$.asObservable();
  }

  get resultAllocated$() {
    return this._resultAllocated$.asObservable();
  }

  get resultSelectLot$() {
    return this._resultSelectLot$.asObservable();
  }

  clearFilterAccountDisplay() {
    this._clearFilterAccountDisplay.emit();
  }

  clearFilterAccountDisplayChange() {
    return this._clearFilterAccountDisplay.asObservable();
  }

  resetAccountDisplay() {
    this._resetAccountDisplay.emit();
  }

  resetAccountDisplayChange() {
    return this._resetAccountDisplay.asObservable();
  }

  private checkRequestHaveResults(processingInfoDTO: IProcessingInfoDTO, requestId: string) {
    this._marks$.next({
      isSubmit: processingInfoDTO.isSubmit,
      isAllocate: processingInfoDTO.isAllocate,
      isReverse: processingInfoDTO.isReverse,
      isPIC: processingInfoDTO.pic == this.authService.userProfile?.userDTO.username,
      isAccountMaintenance: processingInfoDTO.isAccountMaintenance,
      isCreatePaymentLot: processingInfoDTO.isCreatePaymentLot,
      isPaymentRun: processingInfoDTO.isPaymentRun,
      isResetClearing: processingInfoDTO.isResetClearing,
      isConfirmed: processingInfoDTO.isConfirmed,
      haveItemAllocateFailed: processingInfoDTO.haveItemAllocateFailed
    });

    if (
      processingInfoDTO.isSubmit ||
      processingInfoDTO.isAllocate ||
      processingInfoDTO.isReverse ||
      processingInfoDTO.isAccountMaintenance ||
      processingInfoDTO.isCreatePaymentLot ||
      processingInfoDTO.isPaymentRun ||
      processingInfoDTO.isResetClearing ||
      processingInfoDTO.haveItemAllocateFailed
    ) {
      this.getAllResults(requestId);
    }
  }

  get marks$() {
    return this._marks$.asObservable();
  }

  get marks(): IMarkResults {
    return this._marks$.getValue();
  }

  patchValueMarks(value: Partial<IMarkResults>) {
    this._marks$.next({
      ...this.marks,
      ...value
    });
  }

  get commonIdentificationTypeCode() {
    return this._commonIdentificationTypeCode;
  }

  showError<T>(): OperatorFunction<T, T> {
    return tap({
      error: (error) => this.messageService.error(this.createErrorMessage(error))
    });
  }

  /**
   * Chỉ sử dụng trong màn điều chỉnh phí
   * @param errorResponse
   */
  createErrorMessage(errorResponse: HttpErrorResponse) {
    if (typeof errorResponse.error == "object" && Object(errorResponse.error).hasOwnProperty("messageKey")) {
      const keyTranslateErrorMessage = `MESSAGE.${errorResponse.error.messageKey}`;

      // Bắt lỗi thao tác đối với luồng 07
      // Xảy ra khi SAP lỗi và backend trả về messageKey như dưới
      if (
        [
          "MAPPING_DOC_ERROR_WITH_MSG",
          "REVERSE_DOC_ERROR_WITH_MSG",
          "RESET_CLEARING_ERROR_WITH_MSG",
          "CREATE_PAYMENT_LOT_FAIL_WITH_MSG",
          "AUTO_FEE_FAIL_WITH_MSG",
          "UPDATE_DOC_FAIL_WITH_MSG",
          "PAYMENT_RUN_ERROR_WITH_MSG",
          "AUTO_FEE_FAIL_WITH_MSG"
        ].includes(errorResponse.error.messageKey) &&
        Object(errorResponse.error).hasOwnProperty("message")
      ) {
        return `${this.translateService.instant(`MESSAGE.${INTERNAL_ERROR_MESSAGE}`)}: ${errorResponse.error.message}`;
      }

      // Chỉ hiển thị lỗi ở dev environment
      if (
        this.translateService.instant(keyTranslateErrorMessage) == keyTranslateErrorMessage &&
        environment.production
      ) {
        return `MESSAGE.${INTERNAL_ERROR_MESSAGE}`;
      }

      return keyTranslateErrorMessage;
    }

    return `MESSAGE.${INTERNAL_ERROR_MESSAGE}`;
  }


  /* ========================================================================================
   *                                   API STAGE
   *========================================================================================*/

  /**
   * Lấy dữ liệu test cho chi tiết điều chỉnh phí
   * @param requestId mã số yêu cầu
   * @return
   */
  getDetailPremiumAdjustment(requestId: string) {
    this.loading();
    return this.http
      .get<IResponseModel<{ data: IDetailAdjustmentResponseModel }>>(
        `${environment.task_url}/task/detail/?code=${requestId}`
      )
      .pipe(
        finalize(() => this.stopLoading()),
        this.showError(),
        map((response) => response.data),
        tap((response) => {
          const processingInfoDTO = response.data.adjustmentStep.processingInfoDTO;
          this.decision = processingInfoDTO.decision ?? null;
          this.checkDocumentChecked = {
            paymentAttachmentAuthorization: processingInfoDTO?.paymentAttachmentAuthorization ?? false,
            premiumAdjustmentForm: processingInfoDTO?.premiumAdjustmentForm ?? false,
            paymentAttachment: processingInfoDTO?.paymentAttachment ?? false
          };
          this._detailPremiumAdjustment$.next(response.data);
          this.checkRequestHaveResults(processingInfoDTO, response.data.requestId);

        })
      );
  }

  /**
   * Call api để lấy về SAP shortcut
   * để user mở app SAP
   * @param params
   * @interface ISAPOpenShortcutRequestParams
   * @return Trả về một string là base64
   */
  downloadSAPShortcut(params: ISAPOpenShortcutRequestParams) {
    const url = `${environment.collection_url}/collection/premium-adjustment/open-sap`;
    return this.http.get<IResponseModel<string>>(url, { params: { ...params } }).pipe(
      this.showError()
    );
  }

  /**
   * Get thông tin receivable account display
   * @param params
   */
  getReceivable(params: TAccountDisplayTableRequest) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/account-display/receivables`;
    return this.http.get<IResponseModel<IReceivableResponse>>(url, { params: { ...params } }).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      map((response) => {
        if (response.data.status !== HttpStatusCode.Ok) {
          return [];
        }
        return response.data.receivables;
      })
    );
  }

  /**
   * Call Api lấy dữ liệu cho table Chronology
   */
  getChronology(params: TPageableSearchParams<IGetAccountDisplayChronology>) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/account-display/chronology`;
    return this.http.get<IResponseModel<IChronologyResponse>>(url, { params: { ...params } }).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      map((response) => {
        if (response.data.status !== HttpStatusCode.Ok) {
          return [];
        }
        return response.data.chronology;
      })
    );
  }

  getDownPayment(params: TAccountDisplayTableRequest) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/account-display/down-payment`;
    return this.http.get<IResponseModel<IDownPaymentResponse>>(url, { params: { ...params } }).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      map((response) => {
        if (response.data.status !== HttpStatusCode.Ok) {
          return [];
        }
        return response.data.downPayment;
      })
    );
  }

  reverse(params: IReverseParamsDialogProvide) {
    this.loading();
    const { paymentAttachment, premiumAdjustmentForm, paymentAttachmentAuthorization } = this.checkDocumentChecked;
    const requestBody: IReverseDocumentRequestModel = {
      paymentAttachment,
      paymentAttachmentAuthorization,
      premiumAdjustmentForm,
      requestId: this.detailPremiumAdjustment.requestId,
      ...params
    };
    const url = `${environment.collection_url}/collection/premium-adjustment/reverse`;
    return this.http.post<IResponseModel>(url, requestBody).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap(() => {
        this.patchValueMarks({
          isReverse: true
        });
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.reverse");
        this.getAllResults(this.detailPremiumAdjustment.requestId);
      })
    );
  }

  /**
   * Call khi nhấn button reset clearing ở màn **Chi tiết điều chỉnh phí** phần **Account display**
   */
  resetClearing(params: IResetClearingParamsDialogProvide) {
    this.loading();
    const { paymentAttachment, premiumAdjustmentForm, paymentAttachmentAuthorization } = this.checkDocumentChecked;
    const request: IResetClearingRequest = {
      requestId: this.detailPremiumAdjustment.requestId,
      premiumAdjustmentForm,
      paymentAttachment,
      paymentAttachmentAuthorization,
      ...params
    };
    const url = `${environment.collection_url}/collection/premium-adjustment/reset-clearing`;
    return this.http.post<IResponseModel>(url, request).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.reset-clearing");
        this.getAllResults(this.detailPremiumAdjustment.requestId);
        this.patchValueMarks({
          isResetClearing: true
        });
      })
    );
  }

  /**
   * Call khi nhấn button account maintenance ở màn **Chi tiết điều chỉnh phí** phần **Account display**
   */
  accountMaintenance(params: IAccountMaintenanceDialogProvide) {
    this.loading();
    const { paymentAttachment, premiumAdjustmentForm, paymentAttachmentAuthorization } = this.checkDocumentChecked;
    const request: IAccountMaintenanceRequest = {
      requestId: this.detailPremiumAdjustment.requestId,
      premiumAdjustmentForm,
      paymentAttachment,
      paymentAttachmentAuthorization,
      ...params
    };
    const url = `${environment.collection_url}/collection/premium-adjustment/mapping-doc`;
    return this.http.post<IResponseModel>(url, request).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.account-maintenance");
        this.getAllResults(this.detailPremiumAdjustment.requestId);
        this.patchValueMarks({ isAccountMaintenance: true });
      })
    );
  }

  /**
   *
   * @param params - Example `
   * {
   *     "docNumber":"555000003077",
   *     "amount":"20000000",
   *     "netDate":"2022-06-30",
   *     "docType":"11"
   * }`
   */
  paymentRun(params: IPaymentRunDialogProvide) {
    this.loading();
    const { paymentAttachment, premiumAdjustmentForm, paymentAttachmentAuthorization } = this.checkDocumentChecked;
    const request: IPaymentRunRequest = {
      requestId: this.detailPremiumAdjustment.requestId,
      premiumAdjustmentForm,
      paymentAttachment,
      paymentAttachmentAuthorization,
      ...params
    };
    const url = `${environment.collection_url}/collection/premium-adjustment/payment-run`;
    return this.http.post<IResponseModel>(url, request).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.payment-run");
        this.getAllResults(this.detailPremiumAdjustment.requestId);
        this.patchValueMarks({
          isPaymentRun: true
        });
      })
    );
  }

  allocate(allocateRequestItemList?: IAllocateRequestItem[]) {
    this.loading();
    const params: IAllocateRequest = {
      requestId: this.detailPremiumAdjustment.requestId
    };
    if(allocateRequestItemList) params.allocateRequestItemList = allocateRequestItemList
    const url = `${environment.collection_url}/collection/premium-adjustment/allocate`;
    return this.http.post<IResponseModel<IAllocateResponse>>(url, params).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap({
          next: (response) => {
            this.getAllResults(this.detailPremiumAdjustment.requestId);
            if (response.data.allocateResult) {
              this.messageService.success("request.detailRequest.premiumAdjustment.message-success.allocate");
              this.patchValueMarks({ isAllocate: true });
            }
            if(response.data.haveItemAllocateFailed) {
              this.patchValueMarks({ haveItemAllocateFailed: true });
              const translate = this.translateService.instant("MESSAGE.WARN.HAVE_ITEM_FAIL_TO_ALLOCATE")
              const failedListString = (response.data.failedList ?? []).join(", ")
              this.messageService.warn(translate + ' : ' + failedListString )
            }
          }
        }
      )
    );
  }

  createPaymentLot(selectedItem?: ICreatePaymentLotSuspended): Observable<IResponseModel> {
    this.loading();
    const request: ICreatePaymentLotRequest & Partial<ICreatePaymentLotSuspended> = {
      requestId: this.detailPremiumAdjustment.requestId,
      ...selectedItem
    };
    const url = `${environment.collection_url}/collection/premium-adjustment/payment-lot`;
    return this.http.post<IResponseModel<ICreatePaymentLotResult>>(url, request).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap((response) => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.payment-lot");
        this.getAllResults(this.detailPremiumAdjustment.requestId);
        if (response.data.paymentLotResult) this.patchValueMarks({ isCreatePaymentLot: true });
      })
    );
  }

  getBusinessProcessData(params: IGetBusinessProcessRequest) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/application-information`;
    return this.http.get<IResponseModel<IBusinessProcessResponse[]>>(url, { params }).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      map((response) => {
        return response.data;
      })
    );
  }

  getBusinessPossessing(params: IGetProcessingRequest) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/application-processing`;
    return this.http.get<IResponseModel<IBusinessProcessingResponse[]>>(url, { params: { ...params } }).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      map((response) => {
        return response.data;
      })
    );
  }

  submitDecisionApprove(requestId: string) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/submit`;
    return this.http.put<IResponseModel>(url, { requestId }).pipe(
      this.asyncSubmit(environment.collection_url),
      this.showError(),
      finalize(() => this.stopLoading()),
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.submit");
        this.getDetailPremiumAdjustment(this.detailPremiumAdjustment.requestId).subscribe();
        this.patchValueMarks({ isSubmit: true });
      })
    );
  }

  asyncSubmit(endPointUrl: string): OperatorFunction<any, any> {
    return switchMap((res: any) => {
      let count = 200; // repeat 200 lần cách nhau 3 giây, sau 200 lần sẽ throw
      return this.http.get(`${endPointUrl}/process-management?id=${res.data}`).pipe(
        map((val: any) => {
          if (count === 0) {
            this.cancelAsyncWaitingResponse$.next();
            throw { error: { status: "ERROR" } };
          }
          if (["COMPLETE", "ERROR"].includes(val.data?.status)) {
            this.cancelAsyncWaitingResponse$.next();
            if ("ERROR" === val?.data?.status) {
              val.error = val.data;
              throw val;
            }
          }
          return val;
        }),
        skipWhile((resCheck: any) => !["COMPLETE", "ERROR"].includes(resCheck.data?.status)),
        repeatWhen((ob) =>
          ob.pipe(
            delay(3000),
            takeUntil(this.cancelAsyncWaitingResponse$),
            tap(() => {
              if (count == 0) {
                this.cancelAsyncWaitingResponse$.next();
              }
              count--;
            })
          )
        )
      );
    });
  }

  submitDecisionRejectPending(request: IPremiumAdjustmentDecisionRequest) {
    return this.callAPISubmitDecision(request).pipe(
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.submit");
        this.getDetailPremiumAdjustment(this.detailPremiumAdjustment.requestId).subscribe();
        this.patchValueMarks({
          isSubmit: true
        });
      })
    );
  }

  private callAPISubmitDecision(request: object) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/decision`;
    return this.http.put<IResponseModel>(url, request).pipe(
      finalize(() => this.stopLoading()),
      this.showError()
    );
  }

  confirmRequestInformation(request: Omit<IConfirmRequestInformationRequest, "requestId">) {
    const body: IConfirmRequestInformationRequest = {
      ...request,
      requestId: this.detailPremiumAdjustment.requestId
    };
    return this.callAPISubmitDecision(body).pipe(
      tap(() => {
        const change = this.detailPremiumAdjustment
        change.adjustmentStep.summaryDTO.onAttachmentPolicyNumber = request.onAttachmentPolicyNumber
        this._detailPremiumAdjustment$.next(change)
        this.patchValueMarks({ isConfirmed: true });
        this.getDetailPremiumAdjustment(this.detailPremiumAdjustment.requestId).subscribe()
      })
    );
  }

  /**
   * Lấy decision code từ danh mục dùng chung
   */
  getCommonCode() {
    this.loading();
    return this.common
      .getCommonByCodes([
        SAP_IDENTIFICATION_TYPE_CODE,
        PREMIUM_ADJUSTMENT_DECISION,
        COLLECTION_DECISION_CONTRACT_STORE,
        PAYFRQ_CD
      ])
      .pipe(
        finalize(() => this.stopLoading()),
        this.showError(),
        map((listCommon) => {
          const commonDecisions = listCommon.filter(
            (common) => common.commonCategoryCode === PREMIUM_ADJUSTMENT_DECISION
          );
          const commonDecisionMapped = new Map<DECISION_VALUE, number>(
            listCommon
              .filter((common) => common.commonCategoryCode === PREMIUM_ADJUSTMENT_DECISION)
              .map((common) => {
                return [common.name as DECISION_VALUE, parseInt(common.value)];
              })
          );

          this.payerFQCD = mapCommonCategoryByCode(listCommon, PAYFRQ_CD);
          this.decisionMap = commonDecisionMapped;
          this._commonIdentificationTypeCode = mapCommonCategoryByCode(listCommon, SAP_IDENTIFICATION_TYPE_CODE);
          const decisionContractStore = mapCommonCategoryByCode(listCommon, COLLECTION_DECISION_CONTRACT_STORE);
          return { commonDecisions, commonDecisionMapped, decisionContractStore };
        })
      );
  }

  /**
   * Lấy tất cả các kết quả của payment run ,allocate, .... (Tất cả của điều chỉnh phí)
   * @param requestId - id của yêu cầu
   * example BPM-216000012578-07-008
   */
  getAllResults(requestId: string) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/result`;
    return this.http
      .get<IResponseModel<IAllResultsResponse>>(url, { params: { requestId } })
      .pipe(
        finalize(() => this.stopLoading()),
        this.showError(),
        tap((response) => {
          for (let [key, value] of Object.entries(response.data)) {
            switch ((key as keyof typeof response.data)) {
              case "reverseItems":
                this._resultReverse$.next(value);
                break;
              case "allocateItems":
                this._resultAllocated$.next(value);
                break;
              case "resetClearingItems":
                this._resultResetClearing$.next(value);
                break;
              case "accountMaintenanceItems": {
                this._resultAccountMaintenance$.next(value);
                break;
              }
              case "paymentRunItems": {
                this._resultPaymentRun$.next(value);
                break;
              }
              case "paymentLotItems": {
                this._resultPaymentLot$.next(value);
                break;
              }
              case "selectLotItems" : {
                this._resultSelectLot$.next(value);
              }
            }
          }
        })
      )
      .subscribe();
  }

  saveState(request: ISaveStateRequest) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment`;
    return this.http.put(url, request).pipe(
      finalize(() => this.stopLoading()),
      this.showError(),
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.save-state");
      })
    );
  }

  getAttachment(id: string): Observable<DetailRequestModel> {
    this.loading();
    return this.http
      .get<DetailRequestModel>(`${environment.collection_url}/collection/attachment-management?id=${id}`)
      .pipe(
        finalize(() => this.stopLoading()),
        this.showError()
      );
  }

  downloadFileSAP(id: string): Observable<any> {
    this.loading();
    return this.http.post(`${environment.refund_url}/sap/policy-attachment-file`, id).pipe(
      finalize(() => this.stopLoading()),
      this.showError()
    );
  }

  getPaymentLotAccountSuspended(params: TPageableSearchParams<Partial<IAccountSuspendedRequest>>) {
    const requestParams: TPageableSearchParams<Partial<IAccountSuspendedRequest>> = {
      ...params,
      requestId: this.detailPremiumAdjustment?.requestId ?? "BPM-190000036371-07-001"
    };
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/select-lot`;
    return this.http.get<IResponseModel<IPageableResponseModel<IPaymentLotSuspendedResponse>>>(url, { params: { ...requestParams } }).pipe(
      map((response) => mapDataTableWithType(response.data, params)),
      this.showError(),
      finalize(() => this.stopLoading())
    );
  }

  confirmSelectLot(selectedLotDTOList: ISearchPaymentLotRequest["selectedLotDTOList"]) {
    this.loading();
    const requestBody: ISearchPaymentLotRequest = {
      selectedLotDTOList,
      requestId: this.detailPremiumAdjustment?.requestId ?? "BPM-190000036371-07-001"
    };
    const url = `${environment.collection_url}/collection/premium-adjustment/select-lot`;
    return this.http.post<IResponseModel<IConfirmedSelectLotList[]>>(url, requestBody).pipe(
      tap(() => {
        this.messageService.success("request.detailRequest.premiumAdjustment.message-success.confirm-lot");
      }),
      this.showError(),
      finalize(() => this.stopLoading())
    );
  }

  deSelectLot(deselectIdList: IDeleteLotList["deselectIdList"]) {
    this.loading();
    const url = `${environment.collection_url}/collection/premium-adjustment/select-lot`;
    const body: IDeleteLotList = {
      requestId: this.detailPremiumAdjustment.requestId,
      deselectIdList
    };
    return this.http.delete(url, { body }).pipe(
      this.showError(),
      finalize(() => this.stopLoading())
    );
  }

}
