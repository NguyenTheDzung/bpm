import {Pipe, PipeTransform} from "@angular/core";
import {TRAFFIC_LIGHT_CODE} from "../utils/enum";


@Pipe(
  {name: 'trafficLight'}
)
export class TrafficLightPipe implements PipeTransform{
  transform(value: TRAFFIC_LIGHT_CODE | string): string {
    if(!value) return ''
    let assetsPath = 'assets/icons/traffic-icon/'
    return assetsPath + value + '.svg'
  }
}

