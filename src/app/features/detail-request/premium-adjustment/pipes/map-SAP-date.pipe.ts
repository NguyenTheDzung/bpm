import { Pipe, PipeTransform } from "@angular/core";
import { formatSAPDate } from "@detail-request/premium-adjustment/utils/functions";

@Pipe(
  {
    name: "MapSAPDate"
  }
)
export class MapSAPDatePipe implements PipeTransform {
  transform(value: string, currentValueFormat: string = "yyyy-mm-DD",format: string = "DD-mm-yyyy"): any {
    return formatSAPDate(value, format, currentValueFormat)
  }
}