import { Pipe, PipeTransform } from "@angular/core";
import { formatSAPAmount } from "@detail-request/premium-adjustment/utils/functions";

@Pipe(
  {
    name: 'MapSAPAmount'
  }
)
export class MapSAPAmountPipe implements PipeTransform{

  transform(value: string | number, extra = {showMinusFirst: false, prefix: ""}) {
    return formatSAPAmount(value, extra)
  }
}