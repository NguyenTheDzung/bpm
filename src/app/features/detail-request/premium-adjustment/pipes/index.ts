import { TrafficLightPipe } from "./trafficLight.pipe";
import { MapSAPAmountPipe } from "@detail-request/premium-adjustment/pipes/map-SAP-amount.pipe";
import { MapSAPDatePipe } from "@detail-request/premium-adjustment/pipes/map-SAP-date.pipe";

export const PIPES = [
  MapSAPDatePipe,
  TrafficLightPipe,
  MapSAPAmountPipe
]
