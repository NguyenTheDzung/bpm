import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NbuDetailComponent } from './pages/nbu-detail/nbu-detail.component';

const routes: Routes = [{ path: '', component: NbuDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NbuRoutingModule {}
