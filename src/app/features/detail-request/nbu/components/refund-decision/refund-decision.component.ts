import { Component, Injector, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { CommonModel } from '@common-category/models/common-category.model';
import { PaymentMethod } from '@cores/utils/constants';
import { Decisions, ScreenType } from '@cores/utils/enums';
import { createErrorMessage, getValueDateTimeLocal, validateAllFormFields } from '@cores/utils/functions';
import { Refund05ProcessOption, RequestType } from '@create-requests/utils/constants';
import { BaseComponent } from '@shared/components';
import * as _ from 'lodash';
import {
  Bank,
  BankSAP,
  DetailRequestModel,
  PaymentLotNBUModel,
} from '@detail-request/shared/models/detail-request.model';
import { RequestService } from '@requests/services/request.service';
import { AddBankAccountComponent } from '@detail-request/shared/components/add-bank-account/add-bank-account.component';
import { CheckbpRefundComponent } from '../checkbp-refund/checkbp-refund.component';
import { UnderwriteComponent } from '../underwrite/underwrite.component';
import { NbuDetailComponent } from '../../pages/nbu-detail/nbu-detail.component';

@Component({
  selector: 'app-refund-decision',
  templateUrl: './refund-decision.component.html',
  styleUrls: ['./refund-decision.component.scss'],
})
export class RefundDecisionComponent extends BaseComponent implements OnChanges {
  @Input() itemRequest?: DetailRequestModel;
  @Input() form?: FormGroup;
  paymentMethod = PaymentMethod;
  decision = Decisions;
  dataPaymentLot: PaymentLotNBUModel[] = []; //
  dataNewPaymentLot: PaymentLotNBUModel[] = [];
  isScroll?: boolean;
  processOption = Refund05ProcessOption;
  @Input() bankDropList: Bank[] = [];
  @Input() subReasonDropList: CommonModel[] = [];
  @Input() pendingReasonDropList: CommonModel[] = [];
  @Input() bankResDTOList: BankSAP[] = [];
  @Input() authBankResDTOS: BankSAP[] = [];

  @Input() decisionDropList: CommonModel[] = [];
  @Input() rejectReasonDropList: CommonModel[] = [];
  @Input() additionalAttachmentsDropList: CommonModel[] = [];
  @Input() listPostText: CommonModel[] = [];
  @Input() approvedDropList: CommonModel[] = [];
  formPayment = this.fb.group({
    paymentAmount: [null, Validators.required],
    usageText: [null, Validators.required],
    postingDate: null,
  });
  today = new Date();

  constructor(
    inject: Injector,
    private service: RequestService,
    private detailRequest: NbuDetailComponent,
    private underWrite: UnderwriteComponent
  ) {
    super(inject);
  }

  get showCheckNoPayment() {
    return (
      this.itemRequest?.decisionList?.length === 0 &&
      ((this.itemRequest?.requestTypeCode === this.requestType.TYPE05 &&
        this.form?.get('processOption')?.value === this.processOption.refund) ||
        this.itemRequest?.requestTypeCode === RequestType.TYPE01) &&
      this.form?.get('decision')?.value === this.decision.Approved
    );
  }

  get dataForm() {
    return this.form?.getRawValue();
  }

  onChangeBank(bkvId: string | null) {
    let bank: BankSAP = this.switchBank().find((bank: BankSAP) => bank.bkvId === bkvId)!;
    this.fillDataBank(bank);
  }

  fillDataBank(bank?: BankSAP) {
    this.form?.get('bankAcct')?.setValue(bank?.bankN);
    this.form?.get('bankName')?.setValue(bank?.bankA);
    this.form?.get('bkDetails')?.setValue(bank?.bkvId);
    this.form?.get('bankKey')?.setValue(bank?.bankL);
    this.form?.get('bankL')?.setValue(bank?.bankL);
  }

  switchBank(listBank?: BankSAP[]) {
    // lấy list bank uỷ quyền khi request chưa P2P reject
    if (
      _.size(this.itemRequest?.decisionList) === 0 &&
      (this.dataForm.authorizationDoc || this.dataForm.authorizedPersonId) &&
      !_.isEmpty(this.itemRequest?.authorizationCustomerName)
    ) {
      return (this.authBankResDTOS = listBank || this.authBankResDTOS);
    }
    // lấy list bank uỷ quyền khi request P2P reject
    if (
      _.size(this.itemRequest?.decisionList) > 0 &&
      this.dataForm.authorizationDocReDecision &&
      !_.isEmpty(this.itemRequest?.authorizationCustomerName)
    ) {
      return (this.authBankResDTOS = listBank || this.authBankResDTOS);
    }
    return (this.bankResDTOList = listBank || this.bankResDTOList);
  }

  ngOnChanges(_changes: SimpleChanges): void {
    if (_changes['itemRequest']) {
      this.dataNewPaymentLot = this.itemRequest?.selectedPaymentLotItemDTOS || [];
      const bank = this.switchBank().find(
        (item: BankSAP) => item.bankN === this.itemRequest?.bankAcct && item.bankA === this.itemRequest?.bankName
      );
      if (bank) {
        this.onChangeBank(bank.bkvId!);
      }
    }
    if (this.itemRequest?.isSubmit || this.disableProcessing()) {
      this.form?.get('paymentMethod')?.disable();
    } else {
      this.form?.get('paymentMethod')?.enable();
    }
  }

  addBankAccount() {
    const dialog = this.dialogService?.open(AddBankAccountComponent, {
      header: `Add New Bank`,
      showHeader: false,
      width: '70%',
      data: {
        screenType: ScreenType.Create,
        state: {
          bankDropList: this.bankDropList,
          requestId: this.form?.get('requestCode')?.value,
          bkDetails: this.form?.get('bkDetails')?.value,
          authorizationDoc: this.form?.get('authorizationDoc')?.value,
          authorizedPersonId: this.form?.get('authorizedPersonId')?.value,
          authorizationDocReDecision: this.form?.get('authorizationDocReDecision')?.value,
        },
      },
    });
    dialog?.onClose.subscribe({
      next: (result) => {
        if (result) {
          const bank = this.switchBank(result.listBank).find(
            (item: BankSAP) => item.bankN === result.data?.bankAcct && item.bankL === result.data?.bankKey
          );
          this.fillDataBank(bank);
        }
      },
    });
  }

  containsObject(obj: PaymentLotNBUModel, list: PaymentLotNBUModel[]) {
    return list.some((elem) => elem.item === obj.item && elem.paymentLot === obj.paymentLot);
  }

  getPaymentLot() {
    const params = {
      ...this.formPayment.getRawValue(),
      postingDate: getValueDateTimeLocal(this.formPayment.get('postingDate')?.value),
    };
    if (!_.isNumber(this.formPayment.get('paymentAmount')?.value)) {
      return this.messageService.warn('MESSAGE.SEARCH_PAYMENT_AMOUNT');
    }
    if (_.isEmpty(this.formPayment.get('usageText')?.value)) {
      return this.messageService.warn('MESSAGE.SEARCH_PAYMENT_USAGE_TEXT');
    }
    this.loadingService.start();
    this.service.getPaymentLot(params).subscribe({
      next: (data) => {
        this.loadingService.complete();
        const list = data.data?.items || [];
        if (this.dataNewPaymentLot.length > 0) {
          this.dataPaymentLot = [];
          for (let item of list) {
            let found = this.containsObject(item, this.dataNewPaymentLot);
            if (!found) {
              this.dataPaymentLot.push(item);
            }
          }
        } else {
          this.dataPaymentLot = list;
        }
        if (_.size(this.dataPaymentLot) === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        //nếu độ dài bản ghi >10 thì cho thanh scroll dọc với độ dài 400px
        this.dataPaymentLot?.length >= 10 ? (this.isScroll = true) : (this.isScroll = false);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  hasFormPayment() {
    return (
      [RequestType.TYPE01, RequestType.TYPE03].includes(this.itemRequest?.requestTypeCode) ||
      (RequestType.TYPE05 === this.itemRequest?.requestTypeCode &&
        this.form?.get('processOption')?.value === this.processOption.refund)
    );
  }

  hasFormPaymentLot() {
    return (
      this.itemRequest?.decisionList?.length === 0 &&
      (RequestType.TYPE01 === this.itemRequest?.requestTypeCode ||
        (RequestType.TYPE05 === this.itemRequest?.requestTypeCode &&
          this.form?.get('processOption')?.value === this.processOption.refund))
    );
  }

  pushDataPaymentLot(item: PaymentLotNBUModel, isCheck?: boolean) {
    if (isCheck) {
      this.dataNewPaymentLot.push(item);
      this.form?.get('selectedPaymentLotItemDTOS')?.setValue(this.dataNewPaymentLot);
      _.remove(this.dataPaymentLot!, item);
    } else {
      this.dataPaymentLot!.push(item);
      _.remove(this.dataNewPaymentLot, item);
    }
  }

  disableProcessing() {
    return (
      !!this.itemRequest?.isPaymentRun &&
      [RequestType.TYPE03, RequestType.TYPE04, RequestType.TYPE01, RequestType.TYPE05].includes(
        this.itemRequest?.requestTypeCode
      )
    );
  }

  showBp() {
    return (
      ([RequestType.TYPE03, RequestType.TYPE01, RequestType.TYPE05].includes(this.itemRequest?.requestTypeCode) &&
        (this.form?.get('authorizationDoc')?.value || this.form?.get('authorizedPersonId')?.value) &&
        _.size(this.itemRequest?.decisionList) === 0) ||
      (this.form?.get('authorizationDocReDecision')?.value && _.size(this.itemRequest?.decisionList) > 0)
    );
  }

  showBtnProcessing() {
    return (
      !this.disableProcessing() &&
      this.objFunction?.scopes?.includes(this.action.Process) &&
      !this.itemRequest?.isSubmit &&
      this.form?.get('processOption')?.value !== Refund05ProcessOption.transfer
    );
  }

  hasProcess() {
    return (
      [RequestType.TYPE04, RequestType.TYPE03, RequestType.TYPE01, RequestType.TYPE05].includes(
        this.itemRequest?.requestTypeCode
      ) && _.size(this.itemRequest?.decisionList) === 0
    );
  }

  processing() {
    if (!this.form?.get('cancellationRequestForm')?.value && this.itemRequest?.requestTypeCode !== RequestType.TYPE05) {
      this.messageService.error('MESSAGE.POLICY_CANCEL_FORM');
      return;
    }
    //TH dùng cho luồng hủy hoàn hs
    if (
      _.isEmpty(this.form?.get('selectedPaymentLotItemDTOS')?.value) &&
      [RequestType.TYPE01, RequestType.TYPE05].includes(this.itemRequest?.requestTypeCode)
    ) {
      this.messageService.error('MESSAGE.PAYMENTLOT_REQUIRED');
      return;
    }
    if (this.loadingService.loading || this.form?.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    this.loadingService.start();
    const data = this.form?.getRawValue();
    data.refundCashEntryDate = getValueDateTimeLocal(data.refundCashEntryDate);
    this.service.saveProcessDetail(data, this.itemRequest?.requestTypeCode).subscribe({
      next: () => {
        this.detailRequest.ngOnInit();
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.detailRequest.ngOnInit();
      },
    });
  }

  viewCreate() {
    const dialog = this.dialogService?.open(CheckbpRefundComponent, {
      header: `Check bp`,
      showHeader: false,
      width: '55%',
      data: {
        screenType: ScreenType.Create,
        state: {
          requestId: this.itemRequest?.requestCode,
          decision: this.form?.get('decision')?.value,
          processOption: this.form?.get('processOption')?.value,
          //7 loại giấy tờ
          authorizationDoc: this.form?.get('authorizationDoc')?.value,
          authorizedPersonId: this.form?.get('authorizedPersonId')?.value,
          cancellationRequestForm: this.form?.get('cancellationRequestForm')?.value,
          policyHolderIdentification: this.form?.get('policyHolderIdentification')?.value,
          signatureConfirmationLetter: this.form?.get('signatureConfirmationLetter')?.value,
          paymentDocPR: this.form?.get('paymentDocPR')?.value,
          paymentDocTR: this.form?.get('paymentDocTR')?.value,
          //2 lí do approve
          subReason: this.form?.get('subReason')?.value,
          otherReasonApprove: this.form?.get('otherReasonApprove')?.value,
          postingText: this.form?.get('postingText')?.value,
        },
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          const dataForm = _.cloneDeep(this.form?.getRawValue());
          this.detailRequest.ngOnInit();
          const sub = this.loadingService.loading$.subscribe((isLoading: boolean) => {
            if (!isLoading) {
              const timer = setTimeout(() => {
                this.form?.patchValue(dataForm, { emitEvent: false });
                this.onChangeAuthorizationDoc(true);
                clearTimeout(timer);
              }, 500);
              sub.unsubscribe();
            }
            this.ref.detectChanges();
          });
        }
      },
    });
  }

  onChangeAuthorizationDoc(isChecked: boolean) {
    this.form?.get('paymentMethod')?.setValue(null);
    this.form?.get('bkDetails')?.setValue(null);
    this.form?.get('bankName')?.setValue(null);
    this.form?.get('bankAcct')?.setValue(null);
    const hasBP = isChecked && !!this.itemRequest?.authorizationCustomerName;
    this.form
      ?.get('accountName')
      ?.setValue(hasBP ? this.itemRequest?.authorizationCustomerName : this.itemRequest?.policyHolderName);
    this.underWrite.mapRefundCash(hasBP);
  }
}
