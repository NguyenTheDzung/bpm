import { Component, Injector, Input } from '@angular/core';
import { dataURItoBlob } from '@cores/utils/functions';
import { BaseComponent } from '@shared/components';
import * as lodash from 'lodash';
import { saveAs } from 'file-saver';
import { Attachment, AttachmentSAP, DetailRequestModel } from '@detail-request/shared/models/detail-request.model';
import { RequestService } from '@requests/services/request.service';

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
})
export class AttachmentComponent extends BaseComponent {
  constructor(injector: Injector, private service: RequestService) {
    super(injector);
  }

  attachments: Attachment[] = [];
  attachmentSAP: AttachmentSAP[] = [];

  @Input('itemRequest') set ItemRequest(value: DetailRequestModel | undefined) {
    if (!value) {
      console.warn('Invalid itemRequest');
      return;
    }
    this.attachments = value.attachments ?? [];
    this.attachmentSAP = value.archiveKeyItemResDTOList ?? [];
  }

  openFile(content: string, isPDF: boolean) {
    if (isPDF) {
      const url = URL.createObjectURL(dataURItoBlob(content, 'application/pdf'));
      window.open(url, '_blank')?.focus();
    } else {
      let image = new Image();
      image.src = `data:image/png;base64,${content}`;
      let wd = window.open('', '_blank');
      wd!.document.write(image.outerHTML);
    }
    this.loadingService.complete();
  }

  viewAttachment(id: string) {
    this.loadingService.start();
    this.service.getAttachment(id).subscribe({
      next: (data: any) => {
        this.openFile(data?.data.mainDocument!, lodash.toLower(data?.data?.description) === 'application/pdf');
      },
    });
    this.loadingService.complete();
  }

  viewFileFromSAP(id: string) {
    this.loadingService.start();
    this.service.downloadFileSAP(id).subscribe({
      next: (data: any) => {
        this.openFile(data.data.RESULT, data.data.TYPE === 'PDF');
      },
    });
    this.loadingService.complete();
  }

  downloadFileFromSAP(id: string) {
    this.loadingService.start();
    this.service.downloadFileSAP(id).subscribe({
      next: (data: any) => {
        const blob = dataURItoBlob(data.data.RESULT);
        saveAs(
          new Blob([blob], {
            type: 'application/pdf',
          }),
          data.data.FILENAME
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('An error occurred');
        this.loadingService.complete();
      },
    });
  }

  download(id: string) {
    this.loadingService.start();
    this.service.getAttachment(id).subscribe({
      next: (data: any) => {
        const blob = dataURItoBlob(data?.data.mainDocument);
        saveAs(
          new Blob([blob], {
            type: 'application/pdf',
          }),
          data.data.attachmentName
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('An error occurred');
        this.loadingService.complete();
      },
    });
  }
}
