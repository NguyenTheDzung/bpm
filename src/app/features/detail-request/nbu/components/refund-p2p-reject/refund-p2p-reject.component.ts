import { Component, Injector, Input } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { Bank, BankSAP, DetailRequestModel } from '@detail-request/shared/models/detail-request.model';
import { CommonModel } from '@common-category/models/common-category.model';
import { FormGroup } from '@angular/forms';
import { PaymentMethod } from '@cores/utils/constants';
import { NbuDetailComponent } from '../../pages/nbu-detail/nbu-detail.component';

@Component({
  selector: 'refund-p2p-reject',
  templateUrl: './refund-p2p-reject.component.html',
  styleUrls: ['./refund-p2p-reject.component.scss'],
})
export class RefundP2pRejectComponent extends BaseComponent {
  @Input() itemRequest?: DetailRequestModel;
  @Input() form?: FormGroup;
  @Input() approvedDropList: CommonModel[] = [];
  @Input() subReasonDropList: CommonModel[] = [];
  @Input() listPostText: CommonModel[] = [];
  @Input() listTypeCancel: CommonModel[] = [];
  @Input() listRefuseReason: CommonModel[] = [];
  @Input() listSubRefuseReason: CommonModel[] = [];
  @Input() decisionDropList: CommonModel[] = [];
  @Input() rejectReasonDropList: CommonModel[] = [];
  @Input() pendingReasonDropList: CommonModel[] = [];
  @Input() bankResDTOList: BankSAP[] = [];
  @Input() authBankResDTOS: BankSAP[] = [];
  @Input() bankDropList: Bank[] = [];
  @Input() additionalAttachmentsDropList: CommonModel[] = [];
  paymentMethod = PaymentMethod;

  constructor(injector: Injector, private detailComponent: NbuDetailComponent) {
    super(injector);
  }

  get itemOrigin(): DetailRequestModel {
    return this.detailComponent.itemRequestOrigin! || {};
  }
}
