import { Component, Input } from '@angular/core';
import { RequestType } from '@create-requests/utils/constants';
import { DetailRequestModel } from '@detail-request/shared/models/detail-request.model';
import { BaseComponent } from '@shared/components';

@Component({
  selector: 'app-application-data',
  templateUrl: './application-data.component.html',
  styleUrls: ['./application-data.component.scss'],
})
export class ApplicationDataComponent extends BaseComponent {
  @Input() itemRequest?: DetailRequestModel;

  hasCheckType() {
    return [RequestType.TYPE03, RequestType.TYPE04].includes(this.itemRequest?.requestTypeCode);
  }
}
