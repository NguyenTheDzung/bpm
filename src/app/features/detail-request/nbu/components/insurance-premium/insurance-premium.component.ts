import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DetailRequestModel } from '@detail-request/shared/models/detail-request.model';

@Component({
  selector: 'app-insurance-premium',
  templateUrl: './insurance-premium.component.html',
  styleUrls: ['./insurance-premium.component.scss'],
})
export class InsurancePremiumComponent {
  @Input() itemRequest?: DetailRequestModel;
  @Input() form?: FormGroup;
  @Output() refreshData: EventEmitter<any> = new EventEmitter();
}
