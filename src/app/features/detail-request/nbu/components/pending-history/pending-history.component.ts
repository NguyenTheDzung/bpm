import { Component, Injector, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { DetailRequestModel, PendingHistoryViewDTOList } from '@detail-request/shared/models/detail-request.model';
import { RequestService } from '@requests/services/request.service';

@Component({
  selector: 'app-pending-history',
  templateUrl: './pending-history.component.html',
  styleUrls: ['./pending-history.component.scss'],
})
export class PendingHistoryComponent extends BaseComponent implements OnChanges {
  constructor(injector: Injector, private service: RequestService) {
    super(injector);
  }

  @Input() itemRequest?: DetailRequestModel;
  pendingHistoryViewDTOList: PendingHistoryViewDTOList[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    this.pendingHistoryViewDTOList = this.itemRequest?.pendingHistories || [];
  }

  checkPendingOther(item: string) {
    return ['004', '011', '910'].includes(item);
  }
}
