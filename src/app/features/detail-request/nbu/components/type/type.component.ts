import { Component, Injector, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CommonModel } from '@common-category/models/common-category.model';
import { DetailRequestModel } from '@detail-request/shared/models/detail-request.model';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@shared/components';
import { RequestService } from '@requests/services/request.service';
import { createErrorMessage, updateValidity, validateAllFormFields } from '@cores/utils/functions';
import * as _ from 'lodash';
import { Refund05ProcessOption } from '@create-requests/utils/constants';
import { NbuDetailComponent } from '../../pages/nbu-detail/nbu-detail.component';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.scss'],
})
export class TypeComponent extends BaseComponent implements OnChanges {
  @Input() listTypeCancel: CommonModel[] = [];
  @Input() listRefuseReason: CommonModel[] = [];
  @Input() listSubRefuseReason: CommonModel[] = [];
  @Input() itemRequest?: DetailRequestModel;
  @Input() form?: FormGroup;
  initComponent = false;
  optionSubReason: CommonModel[] = [];
  isDecisionList = false;

  constructor(injector: Injector, private service: RequestService, private detailRequest: NbuDetailComponent) {
    super(injector);
  }

  get dataForm() {
    return this.form?.getRawValue();
  }

  resetAuthorized(value: any) {
    if (value === Refund05ProcessOption.transfer) {
      this.form?.get('authorizationDoc')?.reset();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.optionSubReason = _.cloneDeep(this.listSubRefuseReason);
    if (this.form && !this.initComponent) {
      this.initComponent = true;
      updateValidity(this.form.get('refusalReason'), Validators.required);
      this.form.get('refusalReason')?.valueChanges.subscribe((value) => {
        if (value === '92') {
          updateValidity(this.form!.get('refusalSubReason'), null);
          this.optionSubReason = this.listSubRefuseReason?.filter((item) => ['911', '912', '910'].includes(item.value));
        } else if (['98', '99'].includes(value)) {
          this.optionSubReason = this.listSubRefuseReason?.filter((item) =>
            ['901', '902', '903', '904', '910'].includes(item.value)
          );
          updateValidity(this.form!.get('refusalSubReason'), Validators.required);
        } else {
          this.form?.get('refusalSubReason')?.setValue(null);
          updateValidity(this.form!.get('refusalSubReason'), null);
          this.optionSubReason = [];
        }
      });
      this.form.get('refusalSubReason')?.valueChanges.subscribe((value) => {
        if (value === '910' || value === '903') {
          updateValidity(this.form!.get('refusalOtherReason'), Validators.maxLength(160));
        } else {
          updateValidity(this.form!.get('refusalOtherReason'), null);
        }
      });
    }
    this.isDecisionList = _.size(this.itemRequest?.decisionList) > 0;
  }

  refuse() {
    if (this.loadingService.loading || this.form?.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    if (!this.loadingService.loading) {
      const data = this.form?.getRawValue();
      this.loadingService.start();
      this.service.refuse(data).subscribe({
        next: () => {
          this.detailRequest.ngOnInit();
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  close() {
    if (!this.loadingService.loading) {
      this.loadingService.start();
      this.service.closeRequest(this.itemRequest?.requestCode!).subscribe({
        next: () => {
          this.back();
        },
        error: (err) => {
          this.loadingService.complete();
          this.messageService.error(createErrorMessage(err));
        },
      });
    }
  }
}
