import { Component, EventEmitter, Injector, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { CommonModel } from '@common-category/models/common-category.model';
import { MenuItem } from 'primeng/api';
import * as _ from 'lodash';
import { RequestService } from '@requests/services/request.service';
import { Bank, BankSAP, DetailRequestModel } from '@detail-request/shared/models/detail-request.model';
import { Refund05ProcessOption, RequestType } from '@create-requests/utils/constants';
import {
  cleanDataForm,
  createErrorMessage,
  getValueDateTimeLocal,
  updateValidity,
  validateAllFormFields,
} from '@cores/utils/functions';
import { Validators } from '@angular/forms';
import { Decisions } from '@cores/utils/enums';
import { PaymentMethod } from '@cores/utils/constants';

@Component({
  selector: 'app-underwrite',
  templateUrl: './underwrite.component.html',
  styleUrls: ['./underwrite.component.scss'],
})
export class UnderwriteComponent extends BaseComponent implements OnChanges {
  state: any;
  items: MenuItem[] = [];
  decision = Decisions;
  paymentMethod = PaymentMethod;
  typeRequest: string;
  @Input() approvedDropList: CommonModel[] = [];
  @Input() itemRequest?: DetailRequestModel;
  @Input() bankDropList: Bank[] = [];
  @Input() reasonDropList: CommonModel[] = [];
  @Input() subReasonDropList: CommonModel[] = [];
  @Input() pendingReasonDropList: CommonModel[] = [];
  @Input() bankResDTOList: BankSAP[] = [];
  @Input() authBankResDTOS: BankSAP[] = [];
  @Input() decisionDropList: CommonModel[] = [{ value: '' }];
  @Input() rejectReasonDropList: CommonModel[] = [];
  @Input() additionalAttachmentsDropList: CommonModel[] = [];
  @Input() listTypeCancel: CommonModel[] = [];
  @Input() listRefuseReason: CommonModel[] = [];
  @Input() listSubRefuseReason: CommonModel[] = [];
  @Input() listPostText: CommonModel[] = [];
  @Output() refreshData: EventEmitter<boolean> = new EventEmitter();
  @Output() nextStep: EventEmitter<boolean> = new EventEmitter();

  dataProcess = this.fb.group({
    requestId: [''],
    cancellationRequestForm: false,
    policyHolderIdentification: false,
    signatureConfirmationLetter: false,
    paymentDocPR: false,
    paymentDocTR: false,
    authorizationDoc: false,
    authorizedPersonId: false,
    hospitalSurgicalCash: 0,
    finalDecision: null,
    decision: null,
    ctnt: false,
    ctuqnt: false,
    reversed: false,
    paymentRun: false,
    sapObject: '/PM0/ABPLC',
    archivId: 'Z1',
    arObject: 'ZSAP_CA',
    reserve: 'PDF',
    amount: 0,
    paymentAmount: 0,
    mainFee: 0,
    riderFee: 0,
    topUpFee: 0,
    pmntMeth: 'A',
    opTxt: 'DANh DDD',
    subReason: null,
    rejectNote: null,
    reasonReject: null,
    reasonPending: null,
    reason: null,
    otherReasonApprove: null,
    additionalAttachments: null,
    pendingNote: null,
    otherReasonReject: null,
    requestCode: null,
    otherReasonPending: null,
    paymentMethod: 'Banking',
    //bank
    bkDetails: '',
    bankL: '',
    bankKey: '',
    bankAcct: '',
    accountName: '',
    bankName: '',
    //tien mặt
    refundCashFullName: '',
    refundCashEntryDate: '',
    refundCashBankBranch: '',
    refundCashIdCardNum: '',
    refundCashBankName: '',
    refundCashInstitute: '',
    refundCashIdentifyId: '',
    paymentNote: ['', Validators.maxLength(100)],
    premiumAmount: null,
    paymentOnAccount: null,
    beneficiaryApplicationNumber: null,
    isRefused: false, //của luồng hồ sơ,
    selectedPaymentLotItemDTOS: [],
    postingText: null,
    refusalReason: null,
    refusalSubReason: null,
    refusalOtherReason: null,
    processOption: null,
    isSendToSale: false,
    noRefund: false,
    bkvId: null,
    authorizationDocReDecision: false,
    manualFinalDecision: null,
    manualMedicalFee: null,
  });

  constructor(injector: Injector, private service: RequestService) {
    super(injector);
    this.typeRequest = this.route.snapshot.paramMap.get('type')!;
    this.onValueChange();
  }

  validatorForDecisionApproved() {
    if (!this.dataProcess.get('decision')?.value) {
      return;
    }
    if (this.dataProcess.get('decision')?.value !== Decisions.Approved || !!this.dataProcess.get('noRefund')?.value) {
      updateValidity(this.dataProcess.get('beneficiaryApplicationNumber'), null);
      updateValidity(this.dataProcess.get('paymentMethod'), null);
      updateValidity(this.dataProcess.get('subReason'), null);
      updateValidity(this.dataProcess.get('postingText'), null);
    } else if (this.itemRequest?.requestTypeCode === RequestType.TYPE02) {
      updateValidity(this.dataProcess.get('beneficiaryApplicationNumber'), [
        Validators.required,
        Validators.maxLength(20),
      ]);
    } else if (this.itemRequest?.requestTypeCode === RequestType.TYPE03) {
      updateValidity(this.dataProcess.get('paymentMethod'), Validators.required);
      updateValidity(this.dataProcess.get('subReason'), Validators.required);
    } else if (this.itemRequest?.requestTypeCode === RequestType.TYPE01) {
      updateValidity(this.dataProcess.get('paymentMethod'), Validators.required);
      updateValidity(this.dataProcess.get('subReason'), Validators.required);
      updateValidity(this.dataProcess.get('postingText'), Validators.required);
    } else if (
      this.itemRequest?.requestTypeCode === RequestType.TYPE05 &&
      this.itemRequest.processOption === Refund05ProcessOption.refund
    ) {
      updateValidity(this.dataProcess.get('paymentMethod'), Validators.required);
      updateValidity(this.dataProcess.get('postingText'), Validators.required);
    }
  }

  onValueChange() {
    if (!this.hasInsurancePremium()) {
      this.dataProcess.get('manualMedicalFee')?.valueChanges.subscribe((value) => {
        this.dataProcess.get('manualFinalDecision')?.setValue((this.itemRequest?.paymentAmount || 0) - (value || 0));
      });
    } else {
      this.dataProcess.get('hospitalSurgicalCash')?.valueChanges.subscribe((value) => {
        const amount =
          (this.itemRequest?.premiumAmount || 0) +
          (this.itemRequest?.paymentOnAccount || 0) +
          (this.itemRequest?.topUpPrem || 0) -
          (value || 0);
        this.dataProcess.get('manualFinalDecision')?.setValue(amount);
      });
    }
    this.dataProcess.get('reasonPending')?.valueChanges.subscribe((value) => {
      if (value === '011') {
        updateValidity(this.dataProcess!.get('otherReasonPending'), Validators.maxLength(1000));
      } else {
        updateValidity(this.dataProcess!.get('otherReasonPending'), null);
      }
    });
    this.dataProcess.get('reasonReject')?.valueChanges.subscribe((value) => {
      if (value === '004') {
        updateValidity(this.dataProcess!.get('otherReasonReject'), Validators.maxLength(1000));
      } else {
        updateValidity(this.dataProcess!.get('otherReasonReject'), null);
      }
    });
    this.dataProcess.get('subReason')?.valueChanges.subscribe((value) => {
      if (value === '910') {
        const validators =
          this.itemRequest?.requestTypeCode === RequestType.TYPE04
            ? [Validators.required, Validators.maxLength(160)]
            : Validators.maxLength(160);
        updateValidity(this.dataProcess!.get('otherReasonApprove'), validators);
      } else {
        updateValidity(this.dataProcess!.get('otherReasonApprove'), null);
      }
    });
    this.dataProcess.get('decision')?.valueChanges.subscribe((decision) => {
      if (decision === Decisions.Pending) {
        updateValidity(this.dataProcess.get('additionalAttachments'), Validators.required);
        updateValidity(this.dataProcess.get('reasonPending'), Validators.required);
        updateValidity(this.dataProcess.get('pendingNote'), Validators.maxLength(1000));
      } else {
        updateValidity(this.dataProcess.get('additionalAttachments'), null);
        updateValidity(this.dataProcess.get('reasonPending'), null);
        updateValidity(this.dataProcess.get('pendingNote'), null);
      }
      if (decision === Decisions.Reject) {
        updateValidity(this.dataProcess.get('reasonReject'), Validators.required);
        updateValidity(this.dataProcess.get('rejectNote'), Validators.maxLength(1000));
      } else {
        updateValidity(this.dataProcess.get('reasonReject'), null);
        updateValidity(this.dataProcess.get('rejectNote'), null);
      }
      this.validatorForDecisionApproved();
    });
    this.dataProcess.get('paymentMethod')?.valueChanges.subscribe((pay) => {
      //bankking
      if (pay === 'Banking' && this.dataProcess.get('decision')?.value === Decisions.Approved) {
        updateValidity(this.dataProcess.get('bkDetails'), Validators.required);
      } else {
        updateValidity(this.dataProcess.get('bkDetails'), null);
      }
      //cash
      if (pay === 'Cash' && this.dataProcess.get('decision')?.value === Decisions.Approved) {
        updateValidity(this.dataProcess.get('refundCashFullName'), Validators.required);
        updateValidity(this.dataProcess.get('refundCashBankName'), Validators.required);
        updateValidity(this.dataProcess.get('refundCashIdCardNum'), Validators.required);
        updateValidity(this.dataProcess.get('refundCashInstitute'), [Validators.required, Validators.maxLength(40)]);
        updateValidity(this.dataProcess.get('refundCashEntryDate'), Validators.required);
        updateValidity(this.dataProcess.get('refundCashBankBranch'), Validators.required);
      } else {
        updateValidity(this.dataProcess.get('refundCashFullName'), null);
        updateValidity(this.dataProcess.get('refundCashBankName'), null);
        updateValidity(this.dataProcess.get('refundCashIdCardNum'), null);
        updateValidity(this.dataProcess.get('refundCashInstitute'), null);
        updateValidity(this.dataProcess.get('refundCashEntryDate'), null);
        updateValidity(this.dataProcess.get('refundCashBankBranch'), null);
      }
    });
    this.dataProcess.get('noRefund')?.valueChanges.subscribe(() => {
      this.validatorForDecisionApproved();
    });
  }

  ngOnChanges(_changes: SimpleChanges): void {
    if (_changes['itemRequest'] && !_.isEmpty(this.itemRequest)) {
      const dataForm = {
        manualFinalDecision: this.itemRequest?.manualFinalDecision,
        manualMedicalFee: this.itemRequest?.manualMedicalFee,
        decision: this.itemRequest?.decision,
        authorizationDoc: !!this.itemRequest?.authorizationDoc,
        paymentDocPR: !!this.itemRequest?.paymentDocPR,
        paymentDocTR: !!this.itemRequest?.paymentDocTR,
        cancellationRequestForm: !!this.itemRequest?.cancellationRequestForm,
        policyHolderIdentification: !!this.itemRequest?.policyHolderIdentification,
        signatureConfirmationLetter: !!this.itemRequest?.signatureConfirmationLetter,
        authorizedPersonId: !!this.itemRequest?.authorizedPersonId,
        paymentNote: this.itemRequest?.paymentNote,
        reversed: !!this.itemRequest?.isReversed,
        isSubmit: !!this.itemRequest?.isSubmit,
        resolution: this.itemRequest?.requestItemHistoryDTOList?.resolution,
        reason: this.approvedDropList.find((x) => x.description === this.itemRequest?.requestTypeCode)?.value,
        reasonPending: this.itemRequest?.reasonPending,
        reasonReject: this.itemRequest?.reasonReject,
        additionalAttachments: this.itemRequest?.additionalAttachments,
        rejectNote: this.itemRequest?.rejectNote,
        pendingNote: this.itemRequest?.pendingNote,
        otherReasonReject: this.itemRequest?.otherReasonReject,
        otherReasonApprove: this.itemRequest?.otherReasonApprove,
        otherReasonPending: this.itemRequest?.otherReasonPending,
        requestCode: this.itemRequest?.requestCode,
        paymentMethod: this.itemRequest?.paymentMethod,
        subReason: this.itemRequest?.subReason || this.checkOtherSubReason(),
        requestTypeCode: this.itemRequest?.requestTypeCode,
        applicationNumber: this.itemRequest?.applicationNumber,
        paymentRun: this.itemRequest?.isPaymentRun,
        requestResolution: this.itemRequest?.requestResolution,
        sapTransferType: this.itemRequest?.policyDataDTO?.sapTransferType,
        policyHolderName: this.itemRequest?.policyHolderName,
        hospitalSurgicalCash: this.itemRequest?.hospitalSurgicalCash || 0,
        riderFee: this.itemRequest?.riderFee || 0,
        mainFee: this.itemRequest?.mainFee || 0,
        topUpFee: this.itemRequest?.topUpFee || 0,
        overPayment: this.itemRequest?.overPayment || 0,
        finalDecision: this.itemRequest?.finalDecision,
        policyNum: this.itemRequest?.policyDataDTO?.policyNumber,
        refundCashFullName: this.itemRequest?.refundCashFullName,
        refundCashEntryDate: _.isEmpty(this.itemRequest?.refundCashEntryDate)
          ? null
          : new Date(this.itemRequest?.refundCashEntryDate),

        refundCashIdentifyId: this.itemRequest?.refundCashIdentifyId,
        refundCashIdCardNum: this.itemRequest?.refundCashIdCardNum,
        refundCashInstitute: this.itemRequest?.refundCashInstitute,
        refundCashBankBranch: this.itemRequest?.refundCashBankBranch,
        refundCashBankName: this.itemRequest?.refundCashBankName,
        premiumAmount: this.itemRequest?.premiumAmount,
        paymentOnAccount: this.itemRequest?.paymentOnAccount,
        ctnt: !!this.itemRequest?.paymentDocPR && !!this.itemRequest?.paymentDocTR,
        ctuqnt: !!this.itemRequest?.authorizationDoc && !!this.itemRequest.authorizedPersonId,
        ///bank
        bankName: this.itemRequest?.bankName,
        bankAcct: this.itemRequest?.bankAcct,
        bkDetails: this.itemRequest?.bkDetails,
        bankL: this.itemRequest?.bankL,
        bankKey: this.itemRequest?.bankKey || this.itemRequest?.bankL,
        //hồ sơ
        beneficiaryApplicationNumber: this.itemRequest?.beneficiaryApplicationNumber,
        postingText: this.itemRequest?.postingText || null,
        isRefused: this.itemRequest?.isRefused,
        selectedPaymentLotItemDTOS: this.itemRequest?.selectedPaymentLotItemDTOS || [],
        refusalReason: this.itemRequest?.refusalReason,
        refusalSubReason: this.itemRequest?.refusalSubReason,
        refusalOtherReason: this.itemRequest?.refusalOtherReason,
        processOption: this.itemRequest?.processOption,
        isSendToSale: !!this.itemRequest?.isSendToSale,
        noRefund: !!this.itemRequest?.noRefund,
        authorizationDocReDecision: !!this.itemRequest?.authorizationDocReDecision,
      };
      this.dataProcess.patchValue(_.cloneDeep(dataForm));
      if (this.currUser?.username !== this.itemRequest?.pic) {
        this.dataProcess.disable();
      } else {
        this.dataProcess.enable();
      }
      const isAuthorizationDoc =
        (_.size(this.itemRequest?.decisionList) > 0 && !!this.itemRequest?.authorizationDocReDecision) ||
        ((!!this.itemRequest?.authorizationDoc || !!this.itemRequest?.authorizedPersonId) &&
          !_.isEmpty(this.itemRequest?.authorizationCustomerName));
      this.dataProcess
        .get('accountName')
        ?.setValue(
          isAuthorizationDoc && !_.isEmpty(this.itemRequest?.authorizationCustomerName)
            ? this.itemRequest?.authorizationCustomerName
            : this.itemRequest?.policyHolderName
        );
      if (_.isEmpty(this.itemRequest?.refundCashFullName)) {
        this.mapRefundCash(isAuthorizationDoc);
      }

      this.itemRequest.isUsedToP2pReject = _.size(this.itemRequest?.decisionList) > 0;

      if (this.itemRequest.isUsedToP2pReject && [RequestType.TYPE03, RequestType.TYPE01].includes(this.typeRequest)) {
        this.dataProcess.get('subReason')?.setValue(this.itemRequest?.decisionList![0].subReason);
      }

      if (
        this.itemRequest?.isUsedToP2pReject &&
        _.size(this.itemRequest?.decisionList) > 0 &&
        !this.itemRequest?.isSubmit
      ) {
        this.itemRequest.isPaymentRun = false;
        updateValidity(this.dataProcess.get('manualFinalDecision'), Validators.required);
      }
    }
  }

  mapRefundCash(isAuthorizationDoc?: boolean) {
    let dataFormCash: any;
    if (isAuthorizationDoc) {
      dataFormCash = {
        refundCashFullName: this.itemRequest?.authorizationCustomerName,
        refundCashIdCardNum: this.itemRequest?.authBpIdentificationResDTOS?.idNumber,
        refundCashInstitute: this.itemRequest?.authBpIdentificationResDTOS?.institute,
        refundCashEntryDate: this.itemRequest?.authBpIdentificationResDTOS?.validDateFrom
          ? new Date(this.itemRequest?.authBpIdentificationResDTOS?.validDateFrom)
          : null,
        refundCashBankName: '',
        refundCashBankBranch: '',
      };
      this.dataProcess.patchValue(dataFormCash);
    } else if (!_.isEmpty(this.itemRequest?.policyDataDTO?.customerName)) {
      dataFormCash = {
        refundCashFullName: this.itemRequest?.policyDataDTO?.customerName,
        refundCashIdCardNum: this.itemRequest?.policyDataDTO?.customerId,
        refundCashInstitute: this.itemRequest?.policyDataDTO?.customerIdIssuedBy,
        refundCashEntryDate: this.itemRequest?.policyDataDTO?.customerIdDate
          ? new Date(this.itemRequest?.policyDataDTO?.customerIdDate)
          : null,
        refundCashBankName: this.itemRequest?.bpBankResDTOS ? this.itemRequest?.bpBankResDTOS[0]?.bankA : null,
        refundCashBankBranch: this.itemRequest?.bpBankResDTOS ? this.itemRequest?.bpBankResDTOS[0]?.branch : null,
      };
      this.dataProcess.patchValue(dataFormCash);
    }
  }

  checkOtherSubReason() {
    if (this.itemRequest?.requestTypeCode === RequestType.TYPE04) {
      return this.subReasonDropList.find((x) => x.value.includes('910'))?.value;
    }
    return null;
  }

  disableSubmit() {
    if (this.itemRequest?.pic !== this.currUser?.username || !!this.itemRequest?.isSubmit || !this.dataForm.decision) {
      return true;
    } else {
      return (
        !this.itemRequest?.isPaymentRun &&
        !this.dataForm.noRefund &&
        _.size(this.itemRequest?.decisionList) === 0 &&
        this.dataForm.decision === Decisions.Approved &&
        (this.dataForm.processOption === Refund05ProcessOption.refund ||
          [RequestType.TYPE03, RequestType.TYPE04, RequestType.TYPE01].includes(this.itemRequest?.requestTypeCode))
      );
    }
  }

  disableSave() {
    return this.loadingService.loading || this.itemRequest?.isSubmit || this.itemRequest?.isPaymentRun;
  }

  saveDetail() {
    if (this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    this.clearReason();
    this.service.saveDetail(this.getDataFormSaveAndSubmit(), this.itemRequest?.requestTypeCode).subscribe({
      next: () => {
        this.refreshData.emit(true);
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  submit() {
    this.clearReason();
    if (this.loadingService.loading || this.dataProcess.invalid) {
      validateAllFormFields(this.dataProcess);
      return;
    }

    if (
      !this.dataForm.cancellationRequestForm &&
      this.dataForm.decision === Decisions.Approved &&
      this.itemRequest?.requestTypeCode !== RequestType.TYPE05
    ) {
      this.messageService.error('MESSAGE.POLICY_CANCEL_FORM');
      return;
    }

    this.loadingService.start();
    const data = this.getDataFormSaveAndSubmit();
    this.service.submitInfo(data, this.itemRequest?.requestTypeCode).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUBMIT_SUCCESS');
        if (
          [Decisions.Pending, Decisions.Reject].includes(data?.decision) ||
          [this.requestType.TYPE02, this.requestType.TYPE04].includes(this.typeRequest)
        ) {
          return this.back();
        }
        this.refreshData.emit(true);
        if (
          data?.decision === Decisions.Approved &&
          (([RequestType.TYPE01, RequestType.TYPE05].includes(this.itemRequest?.requestTypeCode) && !data?.noRefund) ||
            this.itemRequest?.requestTypeCode === RequestType.TYPE03)
        ) {
          this.nextStep.emit(true);
        }
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  clearReason() {
    if (([Decisions.Reject, Decisions.Pending] as string[]).includes(this.dataForm.decision!)) {
      this.dataProcess.get('subReason')?.setValue(null);
      this.dataProcess.get('otherReasonApprove')?.setValue(null);
    }
  }

  getDataFormSaveAndSubmit() {
    const data = cleanDataForm(this.dataProcess);
    if (!data.hospitalSurgicalCash) {
      data.hospitalSurgicalCash = 0;
    }
    data.refundCashEntryDate = getValueDateTimeLocal(data.refundCashEntryDate);
    return data;
  }

  // có THÔNG TIN PHÍ BẢO HIỂM
  hasInsurancePremium() {
    return [RequestType.TYPE04, RequestType.TYPE03].includes(this.typeRequest);
  }

  showResultSection() {
    return (
      !!this.itemRequest?.isPaymentRun &&
      [RequestType.TYPE04, RequestType.TYPE03, RequestType.TYPE01, RequestType.TYPE05].includes(this.typeRequest)
    );
  }

  showDecisionSection() {
    return (
      (this.typeRequest === RequestType.TYPE05 && !!this.itemRequest?.isRefused && !!this.dataForm.processOption) ||
      [RequestType.TYPE04, RequestType.TYPE03, RequestType.TYPE01, RequestType.TYPE02].includes(this.typeRequest)
    );
  }

  savePicNote() {
    if (!this.loadingService.loading) {
      this.loadingService.start();
      this.service.noteRequest(this.itemRequest?.requestCode!, this.itemRequest?.picNote!).subscribe({
        next: () => {
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  get dataForm(): DetailRequestModel {
    return this.dataProcess.getRawValue() || {};
  }

  override onDestroy() {
    this.service.stop();
  }
}
