import { Component, Injector, OnInit } from '@angular/core';
import { BaseActionComponent } from '@shared/components';
import { RequestService } from '@requests/services/request.service';
import { CommonModel } from '@common-category/models/common-category.model';
import { BankSAP, BpSapModel } from '@detail-request/shared/models/detail-request.model';
import { cleanDataForm, getValueDateTimeLocal, updateValidity, validateAllFormFields } from '@cores/utils/functions';
import { FormArray, Validators } from '@angular/forms';
import * as moment from 'moment';
import * as _ from 'lodash';
import { IdentificationTypeSAP, Pattern } from '@cores/utils/constants';

@Component({
  selector: 'app-checkbp-refund',
  templateUrl: './checkbp-refund.component.html',
  styleUrls: ['./checkbp-refund.component.scss'],
})
export class CheckbpRefundComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    idType: [null, Validators.required],
    idNumber: [null, [Validators.required]],
    arrayForm: this.fb.array([]),
  });
  stateData: BpSapModel = {
    listInfoIdentify: [],
    message: '',
    status: false,
  };
  minDate = new Date();
  showFormAddBp: boolean = false;
  showTextNotBp: boolean = false; //show dòng text khi call api
  listIdentifyTypeSap: CommonModel[] = [];
  listGender: CommonModel[] = [];
  bankDropList: BankSAP[] = [];
  selectionItem: BpSapModel | undefined;

  constructor(inject: Injector, private service: RequestService) {
    super(inject, service);
  }

  get arrayForm() {
    return this.form.get('arrayForm') as FormArray;
  }

  ngOnInit(): void {
    this.service.getState().subscribe({
      next: (state) => {
        this.listIdentifyTypeSap =
          state.sapIdentifyTypeCode?.filter((item) => item.code !== IdentificationTypeSAP.Type01) || [];
        this.listGender = state.listGender || [];
        this.bankDropList = state.listBank?.filter((item: any) => item.regex) || [];
      },
    });
  }

  //TH  có bp
  onChangeRequiredIdNumberHasStatus(type: string) {
    if (type === IdentificationTypeSAP.Type02 || type === IdentificationTypeSAP.Type03) {
      updateValidity(this.form.get('idNumber'), [Validators.required, Validators.pattern(Pattern.IDENTIFY_CARD)]);
    } else if (type === IdentificationTypeSAP.Type04) {
      updateValidity(this.form.get('idNumber'), [Validators.required, Validators.pattern(Pattern.PASSPORT)]);
    } else if (type === IdentificationTypeSAP.Type07) {
      updateValidity(this.form.get('idNumber'), [Validators.required, Validators.pattern(Pattern.DRIVING_LICENSE)]);
    }
  }

  checkBpInfor() {
    this.selectionItem = undefined;
    const data = cleanDataForm(this.form);
    updateValidity(this.arrayForm.controls[0]?.get('idNumber'), null);
    updateValidity(this.arrayForm.controls[0]?.get('gender'), null);
    updateValidity(this.arrayForm.controls[0]?.get('issuedPlace'), null);
    updateValidity(this.arrayForm.controls[0]?.get('idType'), null);
    updateValidity(this.arrayForm.controls[0]?.get('issuedDate'), null);
    updateValidity(this.arrayForm.controls[0]?.get('name'), null);
    if (this.form.status === 'INVALID') {
      validateAllFormFields(this.form);
      return;
    } else this.loadingService.start();
    this.service.checkBpInfo(data).subscribe({
      next: (data) => {
        this.stateData.listInfoIdentify =
          data.data?.item?.map((x: BpSapModel) => {
            x.gender = _.upperCase(x.gender!);
            x.validDate = new Date(moment(x.validDate!).format('YYYY/MM/DD'));

            return x;
          }) || [];

        this.arrayForm.controls = [];
        this.stateData.listInfoIdentify?.map((d) =>
          this.arrayForm.push(
            this.fb.group({
              name: d.fullName,
              issuedDate: d.validDate,
              gender: d.gender,
              issuedPlace: d.issuedPlace,
              bankKey: d.bankKey,
              bankAccount: d.bankAccount,
              bpNumber: d.bpNumber,
              idNumber: this.form.get('idNumber')?.value,
              idType: this.form.get('idType')?.value,
            })
          )
        );
        this.stateData.message = data.data?.message;
        if (data.data.status === '200') {
          this.stateData.status = true;
          this.showFormAddBp = true;
          this.showTextNotBp = false;
        }
        //mặc định tích vào bản ghi đầu tiên
        this.selectionItem = this.stateData?.listInfoIdentify![0];
        this.loadingService.complete();
      },
      error: (err) => {
        this.loadingService.complete();
        if (err.error?.status === 404) {
          this.arrayForm.controls = [];
          this.stateData!.listInfoIdentify = [
            {
              name: null,
              issuedDate: null,
              gender: null,
              issuedPlace: null,
              bankKey: null,
              bankAccount: null,
              bpNumber: null,
              idNumber: null,
            },
          ];
          this.stateData.status = false;
          this.showFormAddBp = true;
          this.showTextNotBp = true;
          this.stateData.listInfoIdentify?.map((d) => {
            this.arrayForm.push(
              this.fb.group({
                name: [null, [Validators.required, Validators.pattern(Pattern.NAME)]],
                issuedDate: [null, Validators.required],
                gender: [null, Validators.required],
                issuedPlace: [null, [Validators.required, Validators.maxLength(40)]],
                bankKey: [null],
                bankAccount: [null],
                bpNumber: null,
                idType: [{ value: this.form.get('idType')?.value, disabled: true }, Validators.required],
                idNumber: [{ value: this.form.get('idNumber')?.value, disabled: true }, Validators.required],
                requestId: this.state.requestId,
                decision: this.state.decision,
                processOption: this.state.processOption,
                authorizationDoc: this.state.authorizationDoc,
                authorizedPersonId: this.state.authorizedPersonId,
                cancellationRequestForm: this.state.cancellationRequestForm,
                policyHolderIdentification: this.state.policyHolderIdentification,
                signatureConfirmationLetter: this.state.signatureConfirmationLetter,
                paymentDocPR: this.state.paymentDocPR,
                paymentDocTR: this.state.paymentDocTR,
                //2 lí do approve
                subReason: this.state.subReason,
                otherReasonApprove: this.state.otherReasonApprove,
                postingText: this.state.postingText,
              })
            );
          });
        } else {
          if (err.error.status === 400) {
            this.messageService.error(err.error.message);
          }
        }
      },
    });
  }

  getCheckboxValue(data: BpSapModel) {
    this.selectionItem = data;
  }

  validDate(value: any) {
    if (value.toString() === 'Invalid Date') {
      return null;
    }
    return getValueDateTimeLocal(value);
  }

  saveBp() {
    let obj;
    if (this.stateData.status) {
      obj = {
        name: this.selectionItem?.fullName,
        idType: this.arrayForm.controls[0].get('idType')?.value,
        idNumber: this.arrayForm.controls[0].get('idNumber')?.value,
        bpNumber: this.selectionItem?.bpNumber,
        issuedPlace: this.selectionItem?.issuedPlace,
        issuedDate: this.validDate(this.selectionItem?.validDate!),
        gender: this.selectionItem?.gender,
        requestId: this.state.requestId,
        authorizationDoc: this.state.authorizationDoc,
        authorizedPersonId: this.state?.authorizedPersonId,
        decision: this.state?.decision,
        processOption: this.state.processOption,
        cancellationRequestForm: this.state.cancellationRequestForm,
        policyHolderIdentification: this.state.policyHolderIdentification,
        signatureConfirmationLetter: this.state.signatureConfirmationLetter,
        paymentDocPR: this.state.paymentDocPR,
        paymentDocTR: this.state.paymentDocTR,
        //2 lí do approve
        subReason: this.state.subReason,
        otherReasonApprove: this.state.otherReasonApprove,
        postingText: this.state.postingText,
      };
    } else {
      const dataArray = this.arrayForm.getRawValue();
      dataArray[0].issuedDate = this.validDate(this.arrayForm.controls[0]?.get('issuedDate')?.value);
      obj = Object.assign({}, ...dataArray);
    }
    if (this.arrayForm.status === 'INVALID') {
      validateAllFormFields(this.form);
      return;
    } else {
      this.loadingService.start();
      this.service.addBpInfo(obj).subscribe({
        next: () => {
          this.loadingService.complete();
          this.messageService.success('MESSAGE.SUCCESS');
          this.refDialog.close(true);
        },
        error: (err) => {
          this.messageService.error(err.error.message);
          this.loadingService.complete();
        },
      });
    }
  }
}
