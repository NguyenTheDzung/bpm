import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DetailRequestModel } from '@detail-request/shared/models/detail-request.model';
import { UnderwriteComponent } from '../underwrite/underwrite.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-document-category',
  templateUrl: './document-category.component.html',
})
export class DocumentCategoryComponent {
  @Input() itemRequest?: DetailRequestModel;
  @Input() form?: FormGroup;

  constructor(private fb: FormBuilder, private underwriteComponent: UnderwriteComponent) {}

  onChangeCheckList(event: any, type: string) {
    if (type === 'CTNT') {
      this.form!.get('paymentDocPR')?.setValue(event.checked);
      this.form!.get('paymentDocTR')?.setValue(event.checked);
    } else {
      this.form!.get('authorizationDoc')?.setValue(event.checked);
      this.form!.get('authorizedPersonId')?.setValue(event.checked);
      this.valueChangeByCash();
      this.clearFormBank();
    }
  }

  clearFormBank() {
    this.form?.get('bankAcct')?.reset();
    this.form?.get('bankName')?.reset();
    this.form?.get('bkDetails')?.reset();
    this.form?.get('bankKey')?.reset();
    const isAuthorizationDoc =
      (!!this.form!.get('authorizationDoc')?.value || !!this.form!.get('authorizedPersonId')?.value) &&
      !_.isEmpty(this.itemRequest?.authorizationCustomerName);
    this.form!.get('accountName')?.setValue(
      isAuthorizationDoc ? this.itemRequest?.authorizationCustomerName : this.itemRequest?.policyHolderName
    );
    this.form?.get('bankL')?.reset();
  }

  checkedPR(event: any) {
    this.form!.get('paymentDocPR')?.setValue(event.checked);
    this.setCheckedCTNT();
  }

  checkedTR(event: any) {
    this.form!.get('paymentDocTR')?.setValue(event.checked);
    this.setCheckedCTNT();
  }

  setCheckedCTNT() {
    this.form!.get('ctnt')?.setValue(this.form!.get('paymentDocPR')?.value && this.form!.get('paymentDocTR')?.value);
  }

  checkedAuthorizationDoc(event: any) {
    this.form!.get('authorizationDoc')?.setValue(event.checked);
    this.setCheckedCTUQNT();
    this.valueChangeByCash();
    this.clearFormBank();
  }

  checkedAuthorizedPersonId(event: any) {
    this.form!.get('authorizedPersonId')?.setValue(event.checked);
    this.setCheckedCTUQNT();
    this.valueChangeByCash();
    this.clearFormBank();
  }

  setCheckedCTUQNT() {
    this.form!.get('ctuqnt')?.setValue(
      this.form!.get('authorizationDoc')?.value && this.form!.get('authorizedPersonId')?.value
    );
  }

  disabledCheckbox() {
    return (
      this.form!.get('reversed')?.value ||
      this.itemRequest?.isSubmit ||
      this.itemRequest?.isPaymentRun ||
      this.itemRequest?.isUsedToP2pReject
    );
  }

  valueChangeByCash() {
    if (!this.form!.get('ctuqnt')?.value) {
      this.underwriteComponent.mapRefundCash();
    } else {
      this.form!.get('refundCashFullName')?.setValue(null);
      this.form!.get('refundCashIdCardNum')?.setValue(null);
      this.form!.get('refundCashInstitute')?.setValue(null);
      this.form!.get('refundCashEntryDate')?.setValue(null);
      this.form!.get('refundCashBankName')?.setValue(null);
      this.form!.get('refundCashBankBranch')?.setValue(null);
    }
  }
}
