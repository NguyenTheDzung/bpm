import { UnderwriteComponent } from './underwrite/underwrite.component';
import { AttachmentComponent } from './attachment/attachment.component';
import { PendingHistoryComponent } from './pending-history/pending-history.component';
import { ApplicationDataComponent } from './application-data/application-data.component';
import { DocumentCategoryComponent } from './document-category/document-category.component';
import { InsurancePremiumComponent } from './insurance-premium/insurance-premium.component';
import { RefundDecisionComponent } from './refund-decision/refund-decision.component';
import { RefundP2pRejectComponent } from './refund-p2p-reject/refund-p2p-reject.component';
import { TypeComponent } from './type/type.component';
import { CheckbpRefundComponent } from './checkbp-refund/checkbp-refund.component';

export const components = [
  DocumentCategoryComponent,
  UnderwriteComponent,
  AttachmentComponent,
  PendingHistoryComponent,
  ApplicationDataComponent,
  InsurancePremiumComponent,
  RefundDecisionComponent,
  RefundP2pRejectComponent,
  TypeComponent,
  CheckbpRefundComponent,
];
