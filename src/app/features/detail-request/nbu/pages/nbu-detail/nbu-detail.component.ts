import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { CommonModel } from '@common-category/models/common-category.model';
import { BaseComponent } from '@shared/components';
import { Bank, BankSAP, DetailRequestModel, SlaStep } from '@detail-request/shared/models/detail-request.model';
import { RequestService } from '@requests/services/request.service';
import { forkJoin } from 'rxjs';
import { Refund05ProcessOption, RequestType } from '@create-requests/utils/constants';
import { MenuItem } from 'primeng/api';
import * as _ from 'lodash';
import { ReportComponent } from '@detail-request/shared/components/report/report.component';
import { createErrorMessage } from '@cores/utils/functions';
import { Decisions } from '@cores/utils/enums';
import { Roles } from '@cores/utils/constants';

@Component({
  selector: 'app-nbu-detail',
  templateUrl: './nbu-detail.component.html',
  styleUrls: ['./nbu-detail.component.scss'],
})
export class NbuDetailComponent extends BaseComponent implements OnInit {
  readonly Roles = Roles;
  readonlyStep = true;
  header?: string;
  approvedDropList: CommonModel[] = [];
  activeStep: number = 0;
  @ViewChild(ReportComponent) child!: ReportComponent | undefined;
  items: MenuItem[] = [
    {
      label: 'Underwrite',
      command: (_event: any) => {
        this.activeStep = 0;
      },
    },
    {
      label: 'Approve',
      command: (_event: any) => {
        this.activeStep = 1;
      },
    },
    {
      label: 'Refund',
      command: (_event: any) => {
        this.activeStep = 2;
      },
    },
  ];
  activePanel: number = 0;
  itemRequest?: DetailRequestModel;
  bankDropList: Bank[] = [];
  subReasonDropList: CommonModel[] = [];
  bankResDTOList: BankSAP[] = [];
  authBankResDTOS: BankSAP[] = []; //TH khi user tích vào chứng từ ủy quyền
  decisionDropList: CommonModel[] = [];
  pendingReasonDropList: CommonModel[] = [];
  rejectReasonDropList: CommonModel[] = [];
  additionalAttachmentsDropList: CommonModel[] = [];
  listDecisionStepApprove: CommonModel[] = [];
  listPendingHistory: DetailRequestModel[] = [];
  listPostText: CommonModel[] = [];
  listTypeCancel: CommonModel[] = [];
  listRefuseReason: CommonModel[] = [];
  listSubRefuseReason: CommonModel[] = [];
  typeForm!: string;
  daySlaStep?: number;
  hoursSlaStep?: number;
  minutesSlaStep?: number;
  messageKeySla = '';
  classNameSlaStep = '';
  itemRequestOrigin?: DetailRequestModel;

  constructor(injector: Injector, private service: RequestService) {
    super(injector);
  }

  ngOnInit() {
    this.typeForm = this.route.snapshot.paramMap.get('type')!;
    const id = this.route.snapshot.paramMap.get('requestId');
    if (id) {
      this.loadingService.start();
      forkJoin([this.service.getState(), this.service.findByRequestId(decodeURIComponent(id))]).subscribe({
        next: ([state, itemRequest]) => {
          this.decisionDropList = state.listDecision || [];
          // nếu luồng thẩm định thì bỏ option Từ chối
          if (this.typeForm === this.requestType.TYPE05) {
            this.decisionDropList = this.decisionDropList.filter((item) => item.value !== Decisions.Reject);
          }
          this.approvedDropList = state.listApprovedReason || [];
          this.bankDropList = state.listBank || [];
          this.listDecisionStepApprove = state.listDecisionStepApprove || [];
          this.subReasonDropList = state.listSubReasonApprove || [];
          this.pendingReasonDropList = state.listPendingReason || [];
          this.rejectReasonDropList = state.listRejectReason || [];
          this.additionalAttachmentsDropList = state.listAdditionalAttachment || [];
          this.listPendingHistory = itemRequest?.pendingHistories || [];
          this.listPostText = state.listPostText || [];
          this.listTypeCancel = state.listTypeCancel || [];
          this.listRefuseReason = state.listRefuseReason || [];
          this.listSubRefuseReason = state.listSubRefuseReason || [];
          this.itemRequestOrigin = _.cloneDeep(itemRequest);
          this.itemRequest = itemRequest;
          if (this.currUser?.username !== itemRequest?.pic) {
            this.itemRequest!.isSubmit = true;
          }
          const params = this.route.snapshot.queryParams;
          if (params) {
            setTimeout(() => {
              this.activePanel = +(params['tab'] || 0);
              this.activeStep = +(params['step'] || 0);
              this.getMessageSLA();
            }, 10);
          }
          this.bankResDTOList = itemRequest?.bpBankResDTOS || [];
          this.authBankResDTOS = itemRequest?.authBpBankResDTOS || [];
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
    }
  }

  refresh(value: boolean) {
    if (value) {
      this.ngOnInit();
    }
  }

  doAfterSubmit(event: any) {
    if (event) {
      this.activeStep = 1;
      this.readonlyStep = false;
    }
  }

  showStep() {
    //luồng hủy hoàn HĐ và HS sẽ có 3 steps 03 01
    return (
      (this.typeForm === RequestType.TYPE01 && (!this.itemRequest?.noRefund || !this.itemRequest?.isSubmit)) ||
      (RequestType.TYPE05 === this.typeForm && this.itemRequest?.processOption === Refund05ProcessOption.refund) ||
      this.typeForm === RequestType.TYPE03
    );
  }

  onChangeTabOrStep(tab?: boolean) {
    const timer = setTimeout(() => {
      this.getMessageSLA();
      this.router
        .navigate([], {
          relativeTo: this.route,
          queryParams: { tab: this.activePanel, step: this.activeStep },
        })
        .then(() => {
          if (tab && this.activePanel === 1) {
            this.child?.getReport(false, true);
          }
        });
      clearTimeout(timer);
    }, 200);
  }

  getMessageSLA() {
    if (this.itemRequest?.slaStepList && this.itemRequest?.slaStepList[this.activeStep]) {
      let objSla: SlaStep = this.itemRequest!.slaStepList[this.activeStep]!;
      if (_.isNumber(objSla.slaStepMinutesRemaining)) {
        objSla.slaStepMinutesRemaining = Math.abs(objSla.slaStepMinutesRemaining);
        this.minutesSlaStep = objSla.slaStepMinutesRemaining % 60;
        this.hoursSlaStep = Math.floor(objSla.slaStepMinutesRemaining / 60);
        this.daySlaStep = Math.floor(this.hoursSlaStep / 24);
      }
      if (_.isNumber(this.daySlaStep) && this.daySlaStep > 0) {
        this.hoursSlaStep = this.hoursSlaStep! % 24;
        this.messageKeySla = `request.detailRequest.underwrite.${objSla.slaStepMessage}_DAY`;
      } else {
        this.messageKeySla = `request.detailRequest.underwrite.${objSla.slaStepMessage}`;
      }
      if (['DONE_IN_TIME', 'DONE_LATE'].includes(objSla.slaStepMessage!)) {
        this.classNameSlaStep = 'text-success';
      } else if (objSla.slaStepMessage === 'IN_PROCESS_LATE') {
        this.classNameSlaStep = 'text-danger';
      } else {
        this.classNameSlaStep = '';
      }
    } else {
      this.messageKeySla = '';
      this.classNameSlaStep = '';
    }
  }
}
