import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NbuRoutingModule } from './nbu-routing.module';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';
import { pages } from './pages';
import { DetailRequestSharedModule } from '../shared/detail-request-shared.module';

@NgModule({
  imports: [CommonModule, SharedModule, NbuRoutingModule, DetailRequestSharedModule],
  declarations: [...components, ...pages],
})
export class NbuModule {}
