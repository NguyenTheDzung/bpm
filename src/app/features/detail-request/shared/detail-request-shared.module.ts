import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { components } from './components';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [...components],
  exports: [...components],
})
export class DetailRequestSharedModule {}
