import * as _ from 'lodash';
import { SlaDisplayModel } from '../models/sla-display.model';
import { SlaStep } from '../models/detail-request.model';
import { RequestType } from '@create-requests/utils/constants';

export function calculatorSla(slaDisplay: SlaDisplayModel, objSla?: SlaStep) {
  if (!objSla) {
    slaDisplay.classNameSlaStep = '';
    slaDisplay.messageKeySla = '';
    return;
  }

  if (_.isNumber(objSla?.slaStepMinutesRemaining)) {
    objSla!.slaStepMinutesRemaining = Math.abs(objSla!.slaStepMinutesRemaining);
    slaDisplay.minutesSlaStep = objSla!.slaStepMinutesRemaining % 60;
    slaDisplay.hoursSlaStep = Math.floor(objSla!.slaStepMinutesRemaining / 60);
    slaDisplay.daySlaStep = Math.floor(slaDisplay.hoursSlaStep / 24);
  }
  if (_.isNumber(slaDisplay.daySlaStep) && slaDisplay.daySlaStep > 0) {
    slaDisplay.hoursSlaStep = slaDisplay.hoursSlaStep! % 24;
    slaDisplay.messageKeySla = `request.detailRequest.underwrite.${objSla?.slaStepMessage}_DAY`;
  } else {
    slaDisplay.messageKeySla = `request.detailRequest.underwrite.${objSla?.slaStepMessage}`;
  }
  if (['DONE_IN_TIME', 'DONE_LATE'].includes(objSla?.slaStepMessage!)) {
    slaDisplay.classNameSlaStep = 'text-success';
  } else if (objSla?.slaStepMessage === 'IN_PROCESS_LATE') {
    slaDisplay.classNameSlaStep = 'text-danger';
  } else {
    slaDisplay.classNameSlaStep = '';
  }
}

export function getUrlDetailRequest(url: string, type: string, requestId: string) {
  url = `${url}/detail`;
  if (type === RequestType.TYPE07) {
    url = `${url}/premium-adjustment`;
  } else if (type === RequestType.TYPE08) {
    url = `${url}/fee-refund`;
  } else if (type === RequestType.TYPE09) {
    url = `${url}/submit-payment-receipt`;
  } else if (type === RequestType.TYPE10) {
    url = `${url}/lapse-reversal`;
  } else if (
    [RequestType.TYPE01, RequestType.TYPE02, RequestType.TYPE03, RequestType.TYPE04, RequestType.TYPE05].includes(type)
  ) {
    url = `${url}/nbu`;
  }

  /**
   * Claim Request type
   */
  if(type == RequestType.TYPE13){
    url = `${url}/claim/underwriting`
  }else if(type == RequestType.TYPE12){
    url = `${url}/claim/investigation`
  }


  return `${url}/${encodeURIComponent(requestId!)}/${type}`;
}

// export function asyncSubmit
// <Response = any, RepeatAPIResponse = any>
// (
//   apiRepeatFn: Observable<IResponseModel<RepeatAPIResponse>>,
//   stop$: Subject<void> = new Subject<void>()
// ): OperatorFunction<Response, any> {
//   return switchMap((res: Response) => {
//     let count = 200; // repeat 200 lần cách nhau 3 giây, sau 200 lần sẽ throw
//     return apiRepeatFn.pipe(
//       switchMap(({data }) => {
//         if (count === 0) {
//           stop$.next()
//           throw new HttpErrorResponse(
//             {
//               error: {
//                 message: "Out of loop",
//               }
//             }
//           );
//         }
//         // @ts-ignore
//         if (['COMPLETE', 'ERROR'].includes(data?.status)) {
//           stop$.next();
//           // @ts-ignore
//           if ('ERROR' === data?.status) {
//             throw new HttpErrorResponse(
//               {
//                 error: {...data},
//               }
//             )
//           }
//         }
//         return of(data);
//       }),
//       skipWhile((resCheck: any) => !['COMPLETE', 'ERROR'].includes(resCheck?.status)),
//       repeatWhen((ob) =>
//         ob.pipe(
//           delay(3000),
//           takeUntil(stop$),
//           tap(() => {
//             if (count == 0) {
//               stop$.next();
//             }
//             count--;
//           })
//         )
//       )
//     );
//   });
// }
