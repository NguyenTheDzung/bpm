import {Component, Injector, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {BaseActionComponent} from '@shared/components';
import {Bank} from '../../models/detail-request.model';
import {RequestService} from '@requests/services/request.service';
import {createErrorMessage, validateAllFormFields} from '@cores/utils/functions';

@Component({
  selector: 'app-add-bank-account',
  templateUrl: './add-bank-account.component.html',
  styleUrls: ['./add-bank-account.component.scss'],
})
export class AddBankAccountComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    accountNumber: ['', [Validators.required]],
    accountName: [''],
    bankAcct: [''],
    bankName: '',
    bankKey: [null, Validators.required]
  });

  constructor(injector: Injector, private requestService: RequestService) {
    super(injector, requestService);
  }

  ngOnInit(): void {
    this.state = this.configDialog?.data?.state;
  }

  override save() {
    if (this.loadingService.loading) {
      return;
    }

    const dataForm = this.getDataForm();
    if (this.form?.status === 'VALID') {
      this.messageService?.confirm().subscribe((isConfirm) => {
        if (isConfirm) {
          this.addBank(dataForm);
        }
      });
    } else {
      validateAllFormFields(this.form);
    }
  }

  addBank(dataForm: any) {
    this.loadingService.start();
    let data: any;

    const item = this.state.bankDropList?.find((item: Bank) => item.citadCode === dataForm.bankKey);
    if (+this.state.requestId >= 7) {
      data = {
        requestId: this.state.requestId,
        bankName: item?.name,
        bankAccount: dataForm.accountNumber,
        bankCode: item?.citadCode,
        bankAccountName: dataForm.accountName,
        bpNumber: this.state?.bpNumber,
      };
      this.requestService.addNewBankAccountCollection(data).subscribe({
        next: (res) => {
          this.loadingService.complete();
          this.refDialog.close({ listBank: res.data, data: data });
          this.messageService.success('MESSAGE.SUCCESS');
        },
        error: (err) => {
          this.messageService.error(err.error.message);
          this.loadingService.complete();
        },
      });
    } else {
      data = {
        ...dataForm,
        requestId: this.state.requestId,
        bankName: item?.name,
        bankAcct: dataForm?.accountNumber,
        bankKey: item?.citadCode,
        authorizationDoc: !!this.state?.authorizationDoc,
        authorizedPersonId: !!this.state?.authorizedPersonId,
        authorizationDocReDecision: !!this.state?.authorizationDocReDecision,
      };
      this.requestService.addNewBankAccount(data).subscribe({
        next: (res) => {
          this.loadingService.complete();
          this.refDialog.close({ listBank: res.data?.bpBankResDTOs, data: data });
          this.messageService.success(res.message);
        },
        error: (err) => {
          this.messageService.error(err.error.message);
          this.loadingService.complete();
        },
      });
    }
  }

  fillAccountName() {
    const bankCode = this.state.bankDropList.find((val: any) => {
      return val.citadCode === this.form.controls['bankKey'].value
    })?.smlCode
    const data = {
      accountNumber: this.form.controls['accountNumber'].value,
      bankCode: bankCode ?? '',
      accountType: 'ACCOUNT',
      transferType: bankCode === '970422' ? 'INHOUSE' : 'NAPAS'
    }
    this.loadingService.start()
    this.requestService.getBankInfo(data).subscribe({
      next: (res: any) => {
        this.form.controls['accountName'].setValue(res?.data?.data?.accountName)
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }
}
