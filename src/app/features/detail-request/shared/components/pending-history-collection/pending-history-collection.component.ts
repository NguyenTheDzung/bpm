import {Component, Injector, Input} from '@angular/core';
import {BaseComponent} from '@shared/components';

@Component({
  selector: 'app-pending-history-collection',
  templateUrl: './pending-history-collection.component.html',
  styleUrls: ['./pending-history-collection.component.scss'],
})
export class PendingHistoryCollectionComponent extends BaseComponent {
  constructor(inject: Injector) {
    super(inject);
  }

  @Input() itemRequest?: any;
}
