import { Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { DetailRequestModel } from '../../models/detail-request.model';
import { BaseComponent } from '@shared/components';
import { MenuItem } from 'primeng/api';
import { CollectionDetailRequestModel } from '../../models/collection-detail.model';
import { Resolution } from '@cores/utils/constants';

@Component({
  selector: 'app-steps-process',
  templateUrl: './steps-process.component.html',
  styleUrls: ['./steps.process.component.scss'],
})
export class StepsProcessComponent extends BaseComponent {
  @Input() itemRequest?: DetailRequestModel | CollectionDetailRequestModel;
  @Input() activeIndex: number = 0;
  @Input() steps: MenuItem[] = [];
  @Output() changeStep: EventEmitter<number> = new EventEmitter();
  collectionResolutions = [Resolution.PA_USER_DONE, Resolution.PA_USER_INPROCESS];
  type = '';

  constructor(injector: Injector) {
    super(injector);
    const params = this.route.snapshot.queryParams;
    if (params) {
      setTimeout(() => {
        this.activeIndex = +(params['step'] || 0);
      }, 10);
    }
    this.type = this.route.snapshot.paramMap.get('type')!;
  }

  onChangeStep() {
    this.changeStep.emit(this.activeIndex);
  }
}
