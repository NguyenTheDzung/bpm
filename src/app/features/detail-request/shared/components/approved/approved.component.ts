import { Component, EventEmitter, Injector, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { BaseComponent } from '@shared/components';
import * as _ from 'lodash';
import { ApproverDTOList, DetailRequestModel } from '../../models/detail-request.model';
import { CommonModel } from '@common-category/models/common-category.model';
import { createErrorMessage, getUrlByFunctionCode, validateAllFormFields } from '@cores/utils/functions';
import { RefundService } from '@create-requests/service/refund.service';
import { RequestType } from '@create-requests/utils/constants';
import { RefundStepApproveModel } from '../../models/refund-step-approve.model';
import { Validators } from '@angular/forms';
import { Decisions } from '@cores/utils/enums';

@Component({
  selector: 'app-approved',
  templateUrl: './approved.component.html',
  styleUrls: ['./approved.component.scss'],
})
export class ApprovedComponent extends BaseComponent implements OnChanges {
  status?: string;
  approverDTOList: ApproverDTOList[] = [];
  approverListCurrentStep: ApproverDTOList[] = [];
  mapApprovedDTO: ApproverDTOList[] = [];
  date?: string;
  form = this.fb.group({
    requestCode: null,
    decision: [null, Validators.required],
    approverComment: [null, Validators.required],
  });
  showBtnApproved = false;
  @Input() itemRequest?: DetailRequestModel;
  @Input() decisionDropList: CommonModel[] = [];
  @Input() requestTypeCode?: string;
  @Output() refreshData: EventEmitter<boolean> = new EventEmitter();

  constructor(injector: Injector, private service: RefundService) {
    super(injector);
  }

  ngOnChanges(_changes: SimpleChanges): void {
    this.approverDTOList = this.itemRequest?.approverDTOList ?? [];
    this.approverListCurrentStep = this.itemRequest?.approverListCurrentStep ?? [];
    this.showBtnApproved = !!this.itemRequest?.approverListCurrentStep?.find(
      (item) => item.approvedBy?.toLowerCase() === this.currUser?.username?.toLowerCase()
    );
    this.mapApprovedDTO = [...this.approverDTOList, ...this.approverListCurrentStep].map((x: ApproverDTOList) => {
      x.stepNum = +x.step!;
      return x;
    });
    this.mapApprovedDTO = _.orderBy(this.mapApprovedDTO, 'stepNum', 'asc');
  }

  process() {
    if (this.loadingService.loading || this.form.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    this.loadingService.start();
    const data: RefundStepApproveModel = this.form.getRawValue();
    data.requestCode = this.itemRequest?.requestCode;
    this.service.processStepApprove(data).subscribe({
      next: () => {
        if (data?.decision !== Decisions.Approved) {
          setTimeout(() => {
            this.router.navigateByUrl(getUrlByFunctionCode(this.objFunction?.rsname!, this.currMenu)).then();
          }, 1000);
        } else {
          this.refreshData.emit(true);
          this.messageService.success('MESSAGE.UPDATE_SUCCESS');
        }
        this.form.reset();
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  checkActionApprove() {
    return (
      this.showBtnApproved &&
      [RequestType.TYPE01, RequestType.TYPE03, RequestType.TYPE05].includes(this.requestTypeCode!)
    );
  }
}
