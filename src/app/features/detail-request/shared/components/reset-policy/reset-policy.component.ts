import { Component, Injector, Input, OnInit } from "@angular/core";
import { BaseComponent } from "@shared/components";
import { RequestService } from "@requests/services/request.service";
import * as moment from "moment/moment";
import { CollectionDetailRequestModel, ResetPolicyModel } from "../../models/collection-detail.model";
import { createErrorMessage } from "@cores/utils/functions";

@Component({
  selector: 'app-reset-policy',
  templateUrl: './reset-policy.component.html',
  styleUrls: ['./reset-policy.component.scss'],
})
export class ResetPolicyComponent extends BaseComponent implements OnInit {
  itemResetPolicy!: ResetPolicyModel;
  @Input() itemRequest?: CollectionDetailRequestModel;

  constructor(inject: Injector, private requestService: RequestService) {
    super(inject);
  }

  ngOnInit(): void {
    this.viewResetPolicy();
  }

  viewResetPolicy() {
    this.loadingService.start();
    const requestId = decodeURIComponent(this.route.snapshot.paramMap.get('requestId')!);
    this.requestService.viewResetPolicy(requestId).subscribe({
      next: (data) => {
        this.loadingService.complete();
        this.itemResetPolicy = data.data;
        this.itemResetPolicy.completeDate =
          new Date(moment(this.itemResetPolicy?.completeDate).format('YYYY/MM/DD')) || null;
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }
}
