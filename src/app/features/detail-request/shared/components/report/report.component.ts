import { Component, Injector, Input, OnInit } from '@angular/core';
import { dataURItoBlob } from '@cores/utils/functions';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from '@shared/components';
import * as saveAs from 'file-saver';
import { CollectionDetailRequestModel } from '../../models/collection-detail.model';
import { DetailRequestModel } from '../../models/detail-request.model';
import { RequestService } from '@requests/services/request.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent extends BaseComponent implements OnInit {
  @Input() itemRequest?: DetailRequestModel | CollectionDetailRequestModel;
  @Input() reversal: boolean = false;
  url = '';
  blob?: Blob;

  constructor(inject: Injector, private service: RequestService, public translate: TranslateService) {
    super(inject);
  }

  ngOnInit() {
    const params = this.route.snapshot.queryParams;
    if (params['tab'] && +params['tab'] === 1) {
      this.getReport();
    }
  }

  getReport(isDownload = false, showLoading = false) {
    const type = this.reversal ? this.requestType.TYPE10 : this.route.snapshot.paramMap.get('type')!;
    const id = this.route.snapshot.paramMap.get('requestId');
    if (showLoading) {
      this.loadingService.start();
    }
    this.service.getReport(id!, type!).subscribe({
      next: (data) => {
        this.blob = dataURItoBlob(data?.data);
        this.url = URL.createObjectURL(this.blob!);
        if (showLoading) {
          this.loadingService.complete();
        }
        if (isDownload && this.blob) {
          this.downloadReport();
        }
      },
      error: () => {
        if (isDownload) {
          this.messageService.error('Error!');
        }
        this.loadingService.complete();
      },
    });
  }

  downloadReport() {
    if (this.blob) {
      saveAs(
        new Blob([this.blob!], {
          type: 'application/pdf',
        })
      );
    } else {
      this.getReport(true);
    }
  }
}
