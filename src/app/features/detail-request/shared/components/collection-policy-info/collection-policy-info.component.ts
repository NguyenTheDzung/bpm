import { Component, EventEmitter, Injector, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Validators } from '@angular/forms';
import { CommonModel } from '@common-category/models/common-category.model';
import { createErrorMessage, updateValidity, validateAllFormFields } from '@cores/utils/functions';
import { BaseComponent } from '@shared/components';
import * as _ from 'lodash';
import {
  ApprovedCollectionList,
  AttachmentFeeInfo,
  CollectionDetailRequestModel,
} from '../../models/collection-detail.model';
import { RequestService } from '@requests/services/request.service';
import * as moment from 'moment';

@Component({
  selector: 'app-collection-policy-info',
  templateUrl: './collection-policy-info.component.html',
  styleUrls: ['./collection-policy-info.component.scss'],
})
export class CollectionPolicyInfoComponent extends BaseComponent implements OnInit, OnChanges {
  @Input() decisions: CommonModel[] = [];
  @Input() activeIndexSteps: number = 0;
  @Input() isShowStep?: boolean;
  approvedDataTable: ApprovedCollectionList[] = [];
  @Input() itemRequest?: CollectionDetailRequestModel ;
  @Output() reloadData = new EventEmitter<boolean>();
  itemResetPolicy: any;
  fpl9Table: AttachmentFeeInfo[] = [];
  form = this.fb!.group({
    status: [null, Validators.required],
    requests: null,
    note: [''],
    type: null,
  });

  constructor(inject: Injector, private requestService: RequestService) {
    super(inject);
  }

  viewResetPolicy() {
    this.loadingService.start();
    this.requestService.viewResetPolicy(this.itemRequest?.requestId!).subscribe({
      next: (data) => {
        this.loadingService.complete();
        this.itemResetPolicy = data.data || {};
        this.itemResetPolicy.completeDate = new Date(moment(this.itemResetPolicy.completeDate).format('YYYY/MM/DD'));
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['itemRequest']) {
      this.approvedDataTable = _.orderBy(this.itemRequest?.step2?.approveList, 'step', 'asc') || [];
      this.itemRequest = this.itemRequest?.step2;
      this.form?.patchValue(this.itemRequest!);
      this.fpl9Table = this.itemRequest?.fplReinstatements || [];
    }
    if (this.activeIndexSteps === 1) {
      const timer = setTimeout(() => {
        this.viewResetPolicy();
        clearTimeout(timer);
      }, 200);
    }
  }

  ngOnInit(): void {
    this.form?.patchValue(this.itemRequest!);
    this.form.controls['status'].valueChanges.subscribe((value) => {
      if (value === 0) {
        updateValidity(this.form.get('note'), [Validators.required, Validators.maxLength(256)]);
      } else {
        updateValidity(this.form.get('note'), null);
      }
    });
  }

  save() {
    //save type=1
    this.form?.get('type')?.setValue(1);
    this.updateAction();
  }

  submit() {
    //submit type=2
    this.form?.get('type')?.setValue(2);
    this.updateAction();
  }

  updateAction() {
    this.form?.get('requests')?.setValue([this.itemRequest?.requestId]);
    if (this.form?.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    this.loadingService.start();
    const data = _.cloneDeep(this.form.getRawValue());
    this.requestService.updateActionContractRestore(data).subscribe({
      next: () => {
        this.reloadData.emit(true);
        this.form.controls['status']?.setValue(null);
        this.form.reset();
        this.messageService.success('MESSAGE.UPDATE_SUCCESS');
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }
}
