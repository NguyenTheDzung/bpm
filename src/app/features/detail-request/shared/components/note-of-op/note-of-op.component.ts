import { Component, EventEmitter, Injector, Input, Output, ViewChild } from "@angular/core";
import { BaseComponent } from "@shared/components";
import { RequestService } from "@requests/services/request.service";
import { createErrorMessage } from "@cores/utils/functions";
import { finalize } from "rxjs";
import { REQUEST_PARAMS } from "@detail-request/shared/utils/enums";
import { NgModel } from "@angular/forms";

@Component({
  selector: "app-note-of-op",
  template: `
    <p-card *ngIf="currUser?.username === pic || !!picNote" styleClass="mt-3">
      <div>
        <h4 class="title">{{ 'request.detailRequest.underwrite.picNote' | translate }}</h4>
        <div>
          <mbal-input-textarea
            (blur)="savePicNote()"
            [(ngModel)]="picNote"
            [disabled]="currUser?.username !== pic"
            [fieldForm]="false"
            [rows]="2"
            [maxlength]="1000"
            [showLabel]="false"></mbal-input-textarea>
        </div>
      </div>
    </p-card>
  `,
  styles: []
})
export class NoteOfOpComponent extends BaseComponent{

  @Input() picNote?: string;
  @Input() pic?: string;
  @Output() picNoteChange = new EventEmitter();

  @ViewChild(NgModel) inputModel?: NgModel

  constructor(
    inject: Injector,
    private service: RequestService
  ) {
    super(inject);
  }

  savePicNote() {
    if (
      this.loadingService.loading ||
      this.currUser?.username !== this.pic ||
      !this.isValid()
    ) {
      return;
    }
    this.loadingService.start();
    const id = decodeURIComponent(this.route.snapshot.paramMap.get(REQUEST_PARAMS.CLAIM_REQUEST_ID)!);
    this.service.noteRequest(id!, this.picNote ?? "")
      .pipe(finalize(() => this.loadingService.complete()))
      .subscribe({
        next: () => {
          this.picNoteChange.emit(this.picNote);
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
        }
      });
  }

  isValid() {
    return this.inputModel?.control.valid
  }

}
