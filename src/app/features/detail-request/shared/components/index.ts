import { AddBankAccountComponent } from "./add-bank-account/add-bank-account.component";
import { ReportComponent } from "./report/report.component";
import { StepsProcessComponent } from "./steps-process/steps-process.component";
import { ApprovedComponent } from "./approved/approved.component";
import { RefundComponent } from "./refund/refund.component";
import { AttachmentCollectionComponent } from "./attachment-collection/attachment-collection.component";
import { PendingHistoryCollectionComponent } from "./pending-history-collection/pending-history-collection.component";
import { CollectionPolicyInfoComponent } from "./collection-policy-info/collection-policy-info.component";
import { ResetPolicyComponent } from "./reset-policy/reset-policy.component";
import { NoteOfOpComponent } from "@detail-request/shared/components/note-of-op/note-of-op.component";


export const components = [
  RefundComponent,
  ApprovedComponent,
  AddBankAccountComponent,
  ReportComponent,
  StepsProcessComponent,
  AttachmentCollectionComponent,
  PendingHistoryCollectionComponent,
  CollectionPolicyInfoComponent,
  ResetPolicyComponent,
  NoteOfOpComponent
];
