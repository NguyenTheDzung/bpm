import { Component, Injector, Input, OnChanges } from '@angular/core';
import { BaseComponent } from '@shared/components';
import { CollectionDetailRequestModel } from '../../models/collection-detail.model';
import { Attachment } from '../../models/detail-request.model';
import { RequestService } from '@requests/services/request.service';
import { dataURItoBlob } from '@cores/utils/functions';
import { saveAs } from 'file-saver';
import { CollectionService } from '@create-requests/service/collection.service';

@Component({
  selector: 'app-attachment-collection',
  templateUrl: './attachment-collection.component.html',
  styleUrls: ['./attachment-collection.component.scss'],
})
export class AttachmentCollectionComponent extends BaseComponent implements OnChanges {
  attachments: Attachment[] = [];
  @Input() itemRequest?: CollectionDetailRequestModel;

  constructor(injector: Injector, private service: RequestService, private collectionService: CollectionService) {
    super(injector);
  }

  ngOnChanges(): void {
    this.attachments = this.itemRequest?.attachmentDetailList || [];
  }

  viewAttachment(id: number) {
    this.loadingService.start();
    this.service.getAttachmentCollection(id).subscribe({
      next: (data: any) => {
        this.openFile(data?.data.mainDocument!, true);
        this.loadingService.complete();
      },
    });
  }

  viewFileFromSAP(id: string) {
    this.loadingService.start();
    this.collectionService.getSapAttachment(id).subscribe({
      next: (data: any) => {
        this.openFile(data.data.RESULT, data.data.TYPE === 'PDF');
        this.loadingService.complete();
      },
    });
  }

  openFile(content: string, isPDF: boolean) {
    if (isPDF) {
      const url = URL.createObjectURL(dataURItoBlob(content, 'application/pdf'));
      window.open(url, '_blank')?.focus();
    } else {
      let image = new Image();
      image.src = `data:image/png;base64,${content}`;
      let wd = window.open('', '_blank');
      wd!.document.write(image.outerHTML);
    }
    this.loadingService.complete();
  }

  download(id: number) {
    this.loadingService.start();
    this.service.getAttachmentCollection(id).subscribe({
      next: (data: any) => {
        const blob = dataURItoBlob(data?.data.mainDocument);
        saveAs(
          new Blob([blob], {
            type: 'application/pdf',
          }),
          data.data.attachmentName
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('An error occurred');
        this.loadingService.complete();
      },
    });
  }

  downloadFileFromSAP(id: string) {
    this.loadingService.start();
    this.collectionService.getSapAttachment(id).subscribe({
      next: (data: any) => {
        const blob = dataURItoBlob(data.data.RESULT);
        saveAs(
          new Blob([blob], {
            type: 'application/pdf',
          }),
          data.data.FILENAME
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error('An error occurred');
        this.loadingService.complete();
      },
    });
  }
}
