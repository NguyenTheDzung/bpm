import { Component, Input } from '@angular/core';
import { PaymentMethod } from '@cores/utils/constants';
import { DetailRequestModel } from '../../models/detail-request.model';
import { CollectionDetailRequestModel } from '../../models/collection-detail.model';
import { RequestType } from '@create-requests/utils/constants';
import { BaseComponent } from '@shared/components';

@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss'],
})
export class RefundComponent extends BaseComponent {
  @Input() itemRequest?: DetailRequestModel | CollectionDetailRequestModel;
  status: string | undefined;
  public paymentMethod = PaymentMethod;

  doCheckFI() {
    return [RequestType.TYPE01, RequestType.TYPE05, RequestType.TYPE08].includes(
      this.route.snapshot.paramMap.get('type')!
    );
  }

  doCheckStatus() {
    if (this.itemRequest?.policyDataDTO?.status) {
      return `fiStatus.${this.itemRequest?.policyDataDTO?.status?.toLowerCase()}`;
    } else {
      return '';
    }
  }
}
