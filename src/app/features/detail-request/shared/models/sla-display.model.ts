export interface SlaDisplayModel {
  daySlaStep?: number;
  hoursSlaStep?: number;
  minutesSlaStep?: number;
  messageKeySla: string;
  classNameSlaStep: string;
}
