export interface RefundStepApproveModel {
  decision: string;
  approverComment: string;
  requestCode?: string;
}
