import {Attachment, SlaStep} from './detail-request.model';
import {BaseInterface} from '@cores/models/base.interface';

export interface CollectionDetailRequestModel {
  picNote?: string;
  ticketNumber?: string;
  pic?: string;
  approveStatus?: number;
  slaStepList: SlaStep[];
  resolution?: string;
  email?: string;
  paymentRequestHistoryList?: PaymentRequestHistoryList[];
  approveStatusDescription?: string;
  hasBP?: boolean;
  bpmPaymentRequestDetailDTO?: PaymentRequestDetailDTO;
  sapPaymentRequestDetailDTO?: PaymentRequestDetailDTO;
  p2pPaymentRequestDetailDTO?: PaymentRequestDetailDTO;
  isCVTV?: boolean;
  isCheckBP?: boolean;
  paymentMethodDTO?: PaymentMethodDTO;
  qmEvidenceFile?: string;
  qmEvidenceFileId?: number;
  isGreedy?: boolean;
  bankCode?: string;
  bankName?: string;
  placeOfIssue?: string;
  bpNumber?: string;
  bpDetailDTO?: BpDetailDTO;
  slaMessage?: string;
  slaPercentWarning?: number;
  onAttachmentPolicyNumber?: string,
  feeAdjustPolicyNumber?: string,
  onAttachmentAmount?: number,
  adjustmentAmount?: number,
  signDate?: string,
  crmTicket?: string,
  icCode?: string;
  icName?: string;
  paymentAttachmentAuthorization?: boolean;
  isCreatePaymentLot?: boolean;
  paymentReceiveDate?: string;
  paymentAttachment?: boolean;
  mainProduct?: string;
  requestId?: string;
  reason?: string;
  amount?: number;
  policyNumber?: string;
  channel?: string;
  longText?: string;
  effectiveDate?: string;
  statusPolicy?: string;
  customerName?: string;
  paidDate?: string;
  infof?: string;
  transactionContent?: string;
  periodicFeePayment?: string;
  netDueDate?: string;
  refundAmount?: number;
  phoneNo?: string;
  type?: number | string;
  paymentDate?: string;
  beneficiaryBank?: string;
  beneficiaryAccount?: string;
  branch?: string;
  idType?: string;
  idNo?: number;
  dateOfIssue?: number;
  gender?: string;
  genderName?: string;
  status?: number | boolean;
  noRefund?: boolean;
  typeCode?: string;
  approveList?: ApprovedCollectionList[];
  insuranceClaimForm?: boolean;
  identification?: boolean;
  feePaymentVoucher?: boolean;
  requestTypeCode?: any;
  decision: string | null;
  isSubmit?: boolean;
  isPaymentRequest?: boolean;
  additionalAttachments?: string;
  requestCode?: string;
  paymentMethod?: string;
  originPaymentMethod?: string;
  hospitalSurgicalCash: number;
  selectedPaymentLotItemDTOS: any;
  searchPaymentLotDTO: any;
  note?: string;
  isPaymentRun?: boolean;
  isRefused?: boolean;
  isReversed?: boolean;
  attachmentDetailList: any;
  policyHolderName?: string;
  phoneNumber?: string;
  isSendToQM?: boolean;
  refundRequestProcessingList?: any;
  paymentLotResults?: AttachmentFeeInfo[];
  paymentLots?: AttachmentFeeInfo[];
  isProcessing?: boolean;
  sapApproveStatusReceiveDTO?: any;
  paymentNumber?: string;
  paymentAmount?: number;
  attachments?: Attachment[];
  archiveKeyItemResDTOList?: Attachment[];
  isDistribution?: boolean;
  suggestedContent?: string;
  currentApprove?: any[];
  pendingHistories?: PendingHistoryCollectionModel[];
  isReinstatementPolicy?: boolean;
  slaPercent?: any;
  slaStepPercent?: any;
  slaStepMessage?: any;
  slaMinutesRemaining?: number;
  slaStepMinutesRemaining?: number;
  isTransfer?: boolean;
  policyDataDTO?: any;
  icPhone?: string;
  reinstatementPolicyRequestId?: string | null; //reset policy
  reinstatementPolicyTypeCode?: string;
  step1: CollectionDetailRequestModel;
  step2: CollectionDetailRequestModel;
  attachmentFeeInfoDTOS: AttachmentFeeInfo[];
  requestResolution?: string;
  fplReinstatements: AttachmentFeeInfo[];
  reasonNote?: string;
  isMultiAttInfo?: boolean;
  documentType?: string;
  documentTypeOther?: string;
  numberOfPremiums?: string | number;
  icEmail?: string;
}

export interface AttachmentFeeInfo {
  remainingAmount?: number;
  isCreatePaymentLot?: boolean;
  id?: number | string;
  attachmentType?: string | null;
  numberOfPremiums?: number | null;
  paymentReceiveDate?: string | Date;
  requestId?: string;
  paymentLot?: string;
  isEdit?: boolean;
  item?: string;
  isAllocated?: boolean;
  paymentInfo?: string;
  disabled?: boolean;
  isProcessSuccess?: boolean;
  checked?: boolean;
  isChosen?: boolean;
  processInfo?: string;
  processMessage?: string;
  attachmentTypeOther?: string;
  statusPaymentLotItem?: string;
  orderNum?: number;
  paymentAmount?: number | null;
  searchTerm?: string | null;
  postingFromDate?: string | Date | null;
  postingToDate?: string | Date | null;
  documentNo?: string;
  amount?: number;
  postingDate?: string | null;
  netDueDate?: string;
  usageText?: string;
  txtVw?: string;
  additionalInfo?: string;
  changedBy?: string;
  changedOn?: string;
  time?: string;
  createdDate?: string;
  policyNumber?: string;
  accountName?: string;
  bankAccountNo?: string;
  category?: string;
  dailyNote?: string;
  dailyNextAction?: string;
  status?: string;
  policyAllocated?: string;
  newUsageText?: string;
  newPaymentLot?: string;
  newItem?: string;
  isPaymentLot?: boolean;
  isUsed?: boolean;
}

export interface ApprovedCollectionList {
  id: string;
  function?: string;
  approvedBy?: string;
  step?: number | string;
  action?: string;
  ActionDescription?: string;
  actionDescription?: string;
  actionAt?: string | number;
  createdBy?: string;
  createdDate?: string;
  updatedDate?: string;
  reason?: string;
  stepNum?: number;
  userCode?: string;
  userName?: string;
  statusStr?: string;
  lastUpdatedDate?: string;
  note?: string;
  pic?: string;
}

export interface PendingHistoryCollectionModel {
  id?: string;
  dept?: string;
  description?: string;
  note?: string;
  createdBy?: string;
  createdDate?: string;
}

export interface BpDetailDTO {
  isCVTV?: boolean;
  bpNumber?: string | null;
  customerName?: string | null;
  idType?: string | null;
  idNumber?: string | null;
  dateOfIssue?: string | null;
  institute?: string | null;
  gender?: string | null;
  bankName?: string | null;
  bankCode?: string | null;
  bankAccount?: string | null;
  branch?: string | null;
  bankDataDTO?: BankDataDTO[];
}

export interface PaymentMethodDTO {
  customerName?: string;
  bankName?: string;
  cashBankName?: string;
  bankCode?: string;
  branch?: string;
  placeOfIssue?: string;
  dateOfIssue?: string;
  idNumber?: string;
  requestId?: string;
  bankAccountName?: string;
  bpBankId?: string;
  bankDataDTO?: BankDataDTO[];
}

export interface BankDataDTO {
  bankCode?: string;
  bankName?: string;
  bankAccount?: string;
  accountName?: string;
  bpBankId?: string;
}

export interface PaymentRequestDetailDTO extends BaseInterface {
  amountList?: string[];
  lotItemConcat?: string[];
  bankAccount?: string;
  bankCode?: string;
  bankName?: string;
  bpBankId?: string;
  bpNumber?: string;
  customerName?: string;
  descriptionList?: string[];
  idNumber?: string;
  idType?: string;
  institute?: string;
  noteList?: string[];
  paymentMethod?: string;
  reasonList?: string[];
  repaymentNumberList?: string[];
  dateOfIssue?: string;
  branch?: string;
  documentNumberList?: string;
}

export interface PaymentRequestHistoryList {
  paymentNumber: number;
  createDate: string;
  bpmPaymentRequestDetailDTO?: PaymentRequestDetailDTO;
  sapPaymentRequestDetailDTO?: PaymentRequestDetailDTO;
  p2pPaymentRequestDetailDTO?: PaymentRequestDetailDTO;
  dataTable?: any[];
}

export interface ResetPolicyModel {
  policyNumber?: string;
  longText?: string;
  customerName?: string;
  requestId?: string;
  updatedDate?: string;
  requestStatus?: string;
  statusPolicy?: string;
  completeDate?: string | Date;
  wFSubmissionId?: string;
}
