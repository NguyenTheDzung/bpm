import { CollectionDetailRequestModel } from "./collection-detail.model";

export interface DetailRequestModel {
  picNote?: string;
  isUsedToP2pReject?: boolean;
  decisionList?: DecisionHistory[];
  slaStepList: SlaStep[];
  topUpPrem?: number;
  nbuCanEdit?: boolean;
  decision?: string | null;
  requestId?: string;
  id?: string;
  data?: any;
  code?: string;
  status?: boolean | string | null;
  submitChannel?: string;
  requestType?: string;
  submitType?: string;
  signingDate?: Date;
  updateAt?: Date;
  email?: string;
  icCode?: string;
  icName?: string;
  policyHolderName?: string;
  attachment?: string;
  policyNumber?: string;
  requestCode?: string;
  combinedPolicyDataDTO?: any;
  policyDataDTO?: any;
  attachments?: Attachment[];
  sapAttachmentIdResponseDTO?: any;
  distributionChannel?: string;
  personInCharge?: string;
  authorizationDoc?: boolean;
  paymentDocPR?: boolean;
  bpBankResDTOS?: any;
  paymentDocTR?: boolean;
  cancellationRequestForm?: boolean;
  policyHolderIdentification?: boolean;
  signatureConfirmationLetter?: boolean;
  authorizedPersonId?: boolean;
  hospitalSurgicalCash: number;
  accountName?: string;
  reversed?: any;
  isPaymentRun?: boolean;
  isReversed?: boolean;
  isTransfer?: boolean;
  isSubmit?: any;
  refundCashFullName?: string;
  refundCashEntryDate?: any;
  refundCashIdentifyId?: any;
  refundCashIdCardNum?: string;
  refundCashInstitute?: string;
  refundCashBankBranch?: string;
  refundCashBankName?: string;
  transactionID?: string;
  paymentNumber?: string;
  transactionAt?: string;
  page?: number;
  paymentApproveStatus?: string;
  size?: number;
  archiveKeyItemResDTOList?: any;
  ftNumber?: string;
  paymentNote?: string;
  createdDate?: null;
  bankName?: string;
  rejectNote?: string;
  pendingNote?: string;
  otherReasonReject?: string;
  otherReasonPending?: string;
  otherReasonApprove?: string;
  paymentMethod?: string;
  paymentAmount?: number;
  bankAcct?: string;
  processId?: string;
  pic?: string;
  description?: string;
  subReason?: string;
  reason?: string;
  reasonPending?: string;
  reasonReject?: string;
  resolution?: string;
  bankKey?: string;
  requestResolution?: string;
  action?: string;
  approverDTOList?: ApproverDTOList[];
  approverListCurrentStep?: ApproverDTOList[];
  requestItemHistoryDTOList?: any;
  dropLists?: any;
  bankL?: string;
  isAutoTransfer?: boolean;
  slaMessage?: string;
  riderFee?: number;
  mainFee?: number;
  topUpFee?: number;
  overPayment?: number;
  slaPercent?: any;
  slaStepPercent?: any;
  slaStepMessage?: any;
  slaMinutesRemaining?: number;
  slaStepMinutesRemaining?: number;
  additionalAttachments?: any;
  ctnt?: boolean;
  ctuqnt?: boolean;
  paymentRun?: boolean;
  sapObject?: string;
  archivId?: string;
  arObject?: string;
  reserve?: string;
  amount?: number;
  pmntMeth?: string;
  opTxt?: string;
  sapTransferType?: string;
  finalDecision?: string;
  policyNum?: string;
  bkDetails?: string;
  fullName?: string;
  slaPercentWarning?: any;
  requestTypeCode?: any;
  applicationNumber?: any;
  premiumAmount?: number;
  paymentOnAccount?: number;
  beneficiaryApplicationNumber?: string;
  netValue?: number;
  postingText?: string;
  isRefused?: boolean;
  selectedPaymentLotItemDTOS: any;
  refusalReason?: string;
  refusalSubReason?: string;
  refusalOtherReason?: string;
  processOption?: string;
  isSendToSale?: boolean;
  //collection
  channel?: string;
  longText?: string;
  effectiveDate?: string;
  statusPolicy?: string;
  customerName?: string;
  paidDate?: string;
  infof?: string;
  transactionContent?: string;
  periodicFeePayment?: string;
  netDueDate?: string;
  refundAmount?: number;
  phoneNo?: string;
  paymentDate?: string;
  beneficiaryBank?: string;
  beneficiaryAccount?: string;
  branch?: string;
  idType?: string;
  idNo?: number;
  dateOfIssue?: number;
  gender?: string;
  noRefund?: boolean;
  authorizationDocReDecision?: boolean;
  typeCode?: string;
  insuranceClaimForm?: boolean;
  identification?: boolean;
  feePaymentVoucher?: boolean;
  authorizationCustomerCode?: string;
  authorizationCustomerName?: string;
  authorizationCustomerIdNum?: string;
  authBpIdentificationResDTOS: AuthBpIdentificationModel;
  pendingHistories: PendingHistoryViewDTOList[];
  step2?: DetailRequestModel;
  step1: CollectionDetailRequestModel;
  manualFinalDecision?: number;
  manualMedicalFee?: number;
  signingRequestDate?: string
}

export interface AuthBpIdentificationModel {
  idNumber?: string;
  institute?: string;
  validDateFrom?: string | Date;
}

export interface Attachment {
  id?: string;
  mainDocument?: string;
  description?: string;
  createdAt?: string;
  attachmentName?: string;
  type?: string;
}

export interface AttachmentSAP {
  arObject?: string;
  arcDocId?: string;
  docTypeName?: string;
  arDate?: string;
}

export interface PendingHistoryViewDTOList {
  id?: string;
  additionalAttachment?: string;
  dept?: string;
  description?: string;
  executionTime?: string;
  executor?: string;
  note?: string;
  reason?: string;
}

export interface BankSAP {
  accountName?: string;
  bankA?: string;
  bankN?: string;
  bankS?: string;
  bankL?: string;
  bkvId?: string;
  fullName?: string;
  bankKey?: string;
  branch?: string;
}

export interface Bank {
  name?: string;
  code?: string;
  value?: string;
  smlCode?: string;
  citadCode?: string;
}

export interface ApproverDTOList {
  id: string;
  function?: string;
  approvedBy?: string;
  step?: number;
  action?: string;
  actionAt?: string | number;
  createdBy?: string;
  createdDate?: string;
  updatedDate?: string;
  reason?: string;
  stepNum?: number;
}

export interface PaymentLotModel {
  PaymentLot?: string;
  Item?: string;
  Status?: number | null;
  StatusDes?: string;
  PaymentAmount?: string | null | number;
  BankClAccount?: string;
  CompCode?: number | null;
  PostingDate?: string | Date | null;
  PayerMemo?: string;
  CfcStatus?: string;
  CfcStatusDes?: string;
}

export interface PaymentLotNBUModel {
  paymentLot?: string;
  item?: string;
  status?: number | null;
  statusDes?: string;
  paymentAmount?: string | null | number;
  bankClAccount?: string;
  compCode?: number | null;
  postingDate?: string | Date | null;
  payerMemo?: string;
  cfcStatus?: string;
  cfcStatusDes?: string;
}

export interface BpSapModel {
  bpNumber?: string | null;
  fullName?: string;
  status?: boolean | null;
  gender?: string | null;
  instituteId?: string | null;
  mobile?: string | null;
  validDate?: string | Date;
  message?: string;
  email?: string;
  listInfoIdentify?: BpSapModel[];
  bankAccount?: string | null;
  bankKey?: string | null;
  issuedPlace?: string | null;
  name?: string | null;
  birthDay?: string | null;
  idNumber?: string | null;
  issuedDate?: string | null;
}

export interface SlaStep {
  slaPercent: number;
  slaPercentWarning: number;
  slaResolution: string;
  slaStep: number;
  slaStepMessage: string;
  slaStepMinutesRemaining: number;
  slaStepPercent: number;
  step: number;
}

export interface DecisionHistory {
  approverReject?: string;
  approverRejectFuncion?: string;
  decision?: string;
  generalRequestId?: number;
  id?: number;
  note?: string;
  paymentMethod?: string;
  paymentNumber?: string;
  refundBank?: string;
  refundBankAccount?: string;
  refundBankAccountName?: string;
  refundBankKey?: string;
  refundCashBankBranch?: string;
  refundCashBankName?: string;
  refundCashFullName?: string;
  refundCashIdCardNum?: string;
  refundCashInstitute?: string;
  rejectDate?: string;
  rejectReason?: string;
  rejectStep?: number;
  isPaymentRun?: boolean;
  isReversed?: boolean;
  reason?: string;
  subReason?: string;
}
