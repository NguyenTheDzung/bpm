import { SelectItem } from "primeng/api/selectitem";

export interface IDropdownItemWithTranslate<T> extends SelectItem<T>{
  multiLanguage?: {
    [p: string]: string
  }
}