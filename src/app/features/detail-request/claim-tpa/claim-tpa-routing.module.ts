import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {
  ClaimInvestigationViewComponent
} from "@claim-detail/pages/claim-investigator-view/claim-investigation-view.component";
import {
  ClaimUnderwritingViewComponent
} from "@claim-detail/pages/claim-underwriting-view/claim-underwriting-view.component";
import {RequestType} from "@create-requests/utils/constants";
import {REQUEST_PARAMS} from "@detail-request/shared/utils/enums";


const routes: Routes = [
  {
    path: `investigation/:${REQUEST_PARAMS.TASK_REQUEST_ID}/:${REQUEST_PARAMS.TYPE}`,
    component: ClaimInvestigationViewComponent,
    data: {type: RequestType.TYPE12}
  },
  {
    path: `underwriting/:${REQUEST_PARAMS.TASK_REQUEST_ID}/:${REQUEST_PARAMS.TYPE}`,
    component: ClaimUnderwritingViewComponent,
    data: {type: RequestType.TYPE13}
  },
  {
    path: `tpa/:${REQUEST_PARAMS.CLAIM_REQUEST_ID}`,
    loadChildren: () => import('./components/correspondence-view/correspondence.module').then((m) => m.CorrespondenceModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaimTPARoutingModule {
}
