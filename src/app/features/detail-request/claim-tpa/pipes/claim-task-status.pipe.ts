import { Pipe, PipeTransform } from "@angular/core";

@Pipe(
  {
    name: "TaskStatus"
  }
)
export class ClaimTaskStatusPipe implements PipeTransform{
  transform(value: string): string {
      if(!value) return ''
    let assetsPath = 'assets/icons/traffic-icon-claim/'
      switch (value) {
        case "IN_PROCESS":
          return assetsPath + 'blue.svg'
        case "CANCEL":
          return assetsPath + 'red.svg'
        case "COMPLETE":
          return assetsPath + 'yellow.svg'
        case "CLOSE":
          return assetsPath + 'green.svg'
        default:
          return assetsPath + 'white.svg'
      }
  }
}
