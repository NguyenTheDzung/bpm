import {Pipe, PipeTransform} from "@angular/core";

@Pipe(
  {
    name: "ClaimType"
  }
)
export class ClaimTypePipe implements PipeTransform {
  transform(value: string): string {
    if (!value) return ''
    switch (value) {
      case '12':
        return 'INVESTIGATION'
      case '13':
        return 'UNDERWRITING'
      case '14':
        return 'INSURANCE'
      default:
        return ''
    }
  }
}
