import {ClaimTaskStatusPipe} from "@claim-detail/pipes/claim-task-status.pipe";
import {ClaimTypePipe} from "@claim-detail/pipes/claim-type.pipe";
import {MapDepartmentNamePipe} from "@claim-detail/pipes/map-department-name.pipe";

export const PIPES = [
  ClaimTaskStatusPipe,
  ClaimTypePipe,
  MapDepartmentNamePipe
]

export * from "@claim-detail/pipes/claim-task-status.pipe";
export * from "@claim-detail/pipes/claim-type.pipe";
export * from "@claim-detail/pipes/map-department-name.pipe";
