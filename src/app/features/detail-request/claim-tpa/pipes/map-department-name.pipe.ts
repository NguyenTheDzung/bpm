import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'mapDepartmentName'
})
export class MapDepartmentNamePipe implements PipeTransform {

  transform(value: string): string {
    if (!value) return ''
    switch (value) {
      case 'OP_CLAIM_ASSESSOR':
        return 'CLAIM'
      case 'OP_NBU_UNDERWRITTING':
        return 'NBU'
      case 'OP_COL_USER':
        return 'COL'
      default:
        return ''
    }
  }
}
