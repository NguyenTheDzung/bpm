import {Component, Injector, OnInit, ViewChild} from "@angular/core";
import {
  ClaimInvestigationDecisionComponent
} from "@claim-detail/components/claim-investigator-decision/claim-investigation-decision.component";
import {BaseComponent} from "@shared/components";
import {DetailClaimService} from "@claim-detail/services/detail-claim.service";
import {REQUEST_PARAMS} from "@detail-request/shared/utils/enums";
import {INeedToWorkClaimResponse} from "@claim-detail/interfaces/claim-tpa-response.interface";
import {ISubmitOrSaveInvestigatorDecisionRequest} from "@claim-detail/interfaces/claim-tpa-request.interface";
import {ClaimInvestigationDecisionValue, FileUploadPreModel} from "@claim-detail/interfaces/claim-tpa.interface";
import {map} from "rxjs";
import {ClaimForwardUserComponent} from "@claim-detail/dialogs";
import {ScreenType} from "@cores/utils/enums";
import {Roles} from "@cores/utils/constants";

@Component({
  selector: "app-claim-investigation-view",
  template: `
    <app-claim-detail-wrap
      (onSubmit)="submit()"
      (onSave)="save()"
      [disableSubmit]="!needToWorkInvestigation || isSubmit || !isPIC"
      [disableSave]="!needToWorkInvestigation || isSubmit || !isPIC"
      [loadDetailOnInit]="false"
      [outsideClaimRequestID]="needToWorkInvestigation?.claimRequestId"
    >
      <section class="card mb-3" *ngIf="needToWorkInvestigation as task">
        <h5 class="claim__header uppercase">{{'request.detailRequest.claim-tpa.need-to-process'|translate}}</h5>
        <div class="claim__create-task-body">
          <div class="claim__content-process-wrapper">
            <app-task-info-view [displayCodeAfterSubmit]="false" [info]="task"></app-task-info-view>
          </div>
        </div>
      </section>
      <app-claim-investigation-decision
        (onClickForward)="forward()"
        *ngIf="needToWorkInvestigation && !isSubmit && isPIC"
        [(decisionValue)]="decisionValue">
      </app-claim-investigation-decision>
    </app-claim-detail-wrap>
  `,
  styleUrls: ["./claim-investigation-view.component.scss"]
})
export class ClaimInvestigationViewComponent extends BaseComponent implements OnInit {
  @ViewChild(ClaimInvestigationDecisionComponent)
  claimInvestDecisionComp?: ClaimInvestigationDecisionComponent;

  isSubmit: boolean = false;
  isPIC: boolean = false;

  needToWorkInvestigation?: INeedToWorkClaimResponse;
  decisionValue?: ClaimInvestigationDecisionValue;

  constructor(
    injector: Injector,
    private detailClaimService: DetailClaimService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.getNeedToWorkInvestigation();
  }

  get taskRequestId() {
    return this.route.snapshot.paramMap.get(REQUEST_PARAMS.TASK_REQUEST_ID)!;
  }

  getNeedToWorkInvestigation() {

    this.detailClaimService.getNeedToWorkInfo(this.taskRequestId)
      .pipe(map((res) => res.data.data))
      .subscribe(
        (response) => {
          this.isPIC = this.currUser.username == response.pic
          this.isSubmit = response.isSubmit
          this.needToWorkInvestigation = response
          this.decisionValue = {
            content: response.decisionContent,
            files: [],
            savedFiles: [...response?.attachments ?? []]
          };
      }
    );
  }

  submit() {
    if (!this.validToSubmit()) {
      return;
    }
    try {
      const request: ISubmitOrSaveInvestigatorDecisionRequest = {
        code: this.needToWorkInvestigation?.code!,
        decisionContent: this.decisionValue?.content!,
        requestFileDTOs: this.decisionValue?.files.map(
          value => {
            // @ts-ignore
            delete value["file"];
            return value as FileUploadPreModel;
          }
        )!,
        attachments: this.decisionValue?.savedFiles ?? []
      };

      this.detailClaimService.submitInvestigatorDecision(request).subscribe(
        (response) => {
          this.messageService.success("request.detailRequest.premiumAdjustment.message-success.submit")
          this.needToWorkInvestigation = response.data
          this.claimInvestDecisionComp?.clean()
          this.isSubmit = true
        }
      );

    } catch (e) {
      console.error(e);
    }
  }

  save() {

    try {
      const request: Partial<ISubmitOrSaveInvestigatorDecisionRequest> = {
        code: this.needToWorkInvestigation?.code!,
        decisionContent: this.decisionValue?.content!,
        requestFileDTOs: this.decisionValue?.files.map(
          value => {
            // @ts-ignore
            delete value["file"];
            return value as FileUploadPreModel;
          }
        )!,
        attachments: this.decisionValue?.savedFiles ?? []
      };

      this.detailClaimService.updateInvestigatorDecision(request).subscribe(
        (response) => {
          this.messageService.success("request.detailRequest.premiumAdjustment.message-success.save-state")
          this.claimInvestDecisionComp?.clean()
          this.needToWorkInvestigation = response.data
          this.decisionValue = {
            ...this.decisionValue!,
            savedFiles: response?.data?.attachments ?? [],
            files: [] as ClaimInvestigationDecisionValue["files"]
          }
        }
      );

    } catch (e) {
      console.error(e);
    }

  }

  forward() {
    this.dialogService.open(ClaimForwardUserComponent, {
      showHeader: false,
      width: '40%',
      data: {
        screenType: ScreenType.Create,
        roleName: Roles.OP_CLAIM_INVESTIGATOR,
        model: this.taskRequestId
      }
    }).onClose.subscribe(
      (isForwarded: boolean) => {
        isForwarded &&  this.getNeedToWorkInvestigation()
      }
    )
  }

  private validToSubmit() {
    // if (!this.decisionValue?.files?.length && !this.decisionValue?.savedFiles?.length) {
    //   this.messageService.warn("MESSAGE.WARN.AT_LEAST_ONE_ATTACHMENT");
    //   return false
    // }
    if(!this.claimInvestDecisionComp?.isValid()){
      return false
    }
    return true
  }
}
