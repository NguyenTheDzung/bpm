import {AfterViewInit, Component, Injector, OnInit, ViewChild} from "@angular/core";
import {BaseComponent} from "@shared/components";
import {ClaimDetailWrapComponent, ClaimTpaDecisionComponent} from "@claim-detail/components";
import {DetailClaimService} from "@claim-detail/services";

@Component({
  selector: "app-claim-tpa-view",
  templateUrl: "./claim-tpa-view.component.html",
  styleUrls: ["./claim-tpa-view.component.scss"]
})
export class ClaimTpaViewComponent extends BaseComponent implements OnInit{
  @ViewChild(ClaimTpaDecisionComponent) claimTpaDecisionComp?: ClaimTpaDecisionComponent
  @ViewChild(ClaimDetailWrapComponent) claimDetailWrapComp?: ClaimDetailWrapComponent

  isPIC = false
  constructor(
    injector: Injector,
    private detailClaimService: DetailClaimService
  ) {
    super(injector);
  }


  ngOnInit() {
    this.detailClaimService.displayPermission$.subscribe(
      value => {
        this.isPIC = value['isPIC']
      }
    )
  }

  submitDecision() {
    this.claimTpaDecisionComp?.submitDecision()
  }
}
