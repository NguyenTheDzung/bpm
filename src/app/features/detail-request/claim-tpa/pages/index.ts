import { ClaimTpaViewComponent } from "@claim-detail/pages/claim-tpa-view/claim-tpa-view.component";
import {
  ClaimUnderwritingViewComponent
} from "@claim-detail/pages/claim-underwriting-view/claim-underwriting-view.component";
import {
  ClaimInvestigationViewComponent
} from "@claim-detail/pages/claim-investigator-view/claim-investigation-view.component";

export const PAGES = [
  ClaimTpaViewComponent,
  ClaimUnderwritingViewComponent,
  ClaimInvestigationViewComponent
]
export * from "@claim-detail/pages/claim-tpa-view/claim-tpa-view.component";
export * from "@claim-detail/pages/claim-underwriting-view/claim-underwriting-view.component";
export * from "@claim-detail/pages/claim-investigator-view/claim-investigation-view.component";
