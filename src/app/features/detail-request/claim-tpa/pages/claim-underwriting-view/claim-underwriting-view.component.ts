import {Component, Injector, OnInit, ViewChild} from "@angular/core";
import {BaseComponent} from "@shared/components";
import {
  ClaimUnderwritingDecisionComponent
} from "@claim-detail/components/claim-underwriting-decision/claim-underwriting-decision.component";
import {map} from "rxjs";
import {DetailClaimService} from "@claim-detail/services/detail-claim.service";
import {INeedToWorkClaimResponse} from "@claim-detail/interfaces/claim-tpa-response.interface";
import {CommonModel} from "@common-category/models/common-category.model";
import {
  ISubmitOrSaveUnderwritingDecisionRequest
} from "@claim-detail/interfaces/claim-tpa-request.interface";
import {
  ClaimUnderWritingDecisionValue,
  FileUploadPreModel
} from "@claim-detail/interfaces/claim-tpa.interface";
import {REQUEST_PARAMS} from "@detail-request/shared/utils/enums";
import {cloneDeep} from "lodash";
import {ClaimForwardUserComponent} from "@claim-detail/dialogs";
import {ScreenType} from "@cores/utils/enums";
import {Roles} from "@cores/utils/constants";

@Component({
  selector: "app-claim-underwriting-view",
  template: `
    <app-claim-detail-wrap
      (onSubmit)="submit()"
      (onSave)="save()"
      [disableSubmit]="!needToWorkUnderwriting || isSubmit || !isPIC"
      [loadDetailOnInit]="false"
      [outsideClaimRequestID]="needToWorkUnderwriting?.claimRequestId"
      [disableSave]="!needToWorkUnderwriting || isSubmit || !isPIC">
      <app-claim-need-to-work-underwriting
        *ngIf="needToWorkUnderwriting"
        [requestCode]="needToWorkUnderwriting.code"
          [taskNeedToProcess]="needToWorkUnderwriting.tasks"
      ></app-claim-need-to-work-underwriting>
      <app-claim-underwriting-decision
        (onClickForward)="forward()"
        *ngIf="needToWorkUnderwriting && !isSubmit && isPIC"
        [decisions]="decisions"
        [(decisionValue)]="decisionValue"
      ></app-claim-underwriting-decision>
    </app-claim-detail-wrap>
  `,
  styleUrls: ["./claim-underwriting-view.component.scss"]
})
export class ClaimUnderwritingViewComponent extends BaseComponent implements OnInit {
  @ViewChild(ClaimUnderwritingDecisionComponent)
  claimUnderwritingDecisionComponent?: ClaimUnderwritingDecisionComponent;

  needToWorkUnderwriting?: INeedToWorkClaimResponse;

  decisions: CommonModel[] = [];

  decisionValue?: ClaimUnderWritingDecisionValue;

  isSubmit: boolean = false

  isPIC: boolean = false

  constructor(
    injector: Injector,
    private detailClaimService: DetailClaimService
  ) {
    super(injector);
  }

  get requestId() {
    return this.route.snapshot.paramMap.get(REQUEST_PARAMS.TASK_REQUEST_ID)!;
  }

  ngOnInit() {
    this.getNeedToWorkDetail();
    this.getUnderWritingDecision();
  }

  /**
   *  Lấy thông tin việc cần làm
   */
  getNeedToWorkDetail() {
    this.detailClaimService.getNeedToWorkInfo(this.requestId).pipe(
      map((response) => {
        return response.data.data;
      })
    ).subscribe(
      {
        next: (response) => {
          this.isPIC = this.currUser.username == response.pic
          this.isSubmit = response.isSubmit

          this.needToWorkUnderwriting = cloneDeep(response)

          this.decisionValue = {
            decision: response.decision,
            content: response.decisionContent,
            files: [],
            savedFiles: [...response?.attachments ?? []]
          }
        }
      }
    );
  }

  getUnderWritingDecision() {
    this.detailClaimService.getUnderwritingDecision().subscribe(
      response => {
        this.decisions = response;
      }
    );
  }

  validToSubmit() {
    // if (!this.decisionValue?.files?.length && !this.decisionValue?.savedFiles?.length) {
    //   this.messageService.warn("MESSAGE.WARN.AT_LEAST_ONE_ATTACHMENT");
    //   return false
    // }
    if (!this.claimUnderwritingDecisionComponent?.isValid()) {
      return false
    }
    return true
  }

  submit() {
    if (!this.validToSubmit()) {
      return;
    }
    try {
      const request: ISubmitOrSaveUnderwritingDecisionRequest = {
        code: this.needToWorkUnderwriting?.code!,
        decision: this.decisionValue?.decision!,
        decisionContent: this.decisionValue?.content!,
        requestFileDTOs: this.decisionValue?.files.map(
          value => {
            // @ts-ignore
            delete value["file"];
            return value as FileUploadPreModel;
          }
        )!,
        attachments: this.decisionValue?.savedFiles ?? []
      };

      this.detailClaimService.submitUnderWritingDecision(request).subscribe(
        (response) => {
          this.messageService.success("request.detailRequest.premiumAdjustment.message-success.submit")
          this.claimUnderwritingDecisionComponent?.clean()
          /**
           * Response không trả về tasks trong detail nữa nên phải call lại task detail
           */
          // this.needToWorkUnderwriting = response.data
          this.getNeedToWorkDetail()
          this.isSubmit = true
        }
      );

    } catch (e) {
      console.error(e);
    }

  }


  save() {

    try {
      const request: Partial<ISubmitOrSaveUnderwritingDecisionRequest> = {
        code: this.needToWorkUnderwriting?.code!,
        decision: this.decisionValue?.decision!,
        decisionContent: this.decisionValue?.content!,
        requestFileDTOs: this.decisionValue?.files.map(
          value => {
            // @ts-ignore
            delete value["file"];
            return value as FileUploadPreModel;
          }
        )!,
        attachments: this.decisionValue?.savedFiles ?? []
      };

      this.detailClaimService.updateUnderWritingDecision(request).subscribe(
        (response) => {
          this.messageService.success("request.detailRequest.premiumAdjustment.message-success.save-state")

          /**
           * Response không trả về tasks trong detail nữa nên phải call lại task detail
           */

          this.getNeedToWorkDetail()
          // this.needToWorkUnderwriting = response.data
          // this.claimUnderwritingDecisionComponent?.clean()
          // this.decisionValue = {
          //   ...this.decisionValue!,
          //   savedFiles: response?.data?.attachments ?? [],
          //   files: [] as ClaimUnderWritingDecisionValue["files"]
          // }
        }
      );

    } catch (e) {
      console.error(e);
    }

  }

  forward() {
    this.dialogService.open(ClaimForwardUserComponent, {
      showHeader: false,
      width: '40%',
      data: {
        screenType: ScreenType.Create,
        roleName: Roles.OP_NBU_UNDERWRITTING,
        model: this.requestId
      }
    }).onClose.subscribe(
      (isForwarded: boolean) => {
        isForwarded &&  this.getNeedToWorkDetail()
      }
    )
  }
}
