import {Injectable} from '@angular/core';
import {BaseService} from "@cores/services/base.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "@env";
import {ICloseTask} from "@claim-detail/interfaces/claim-tpa.interface";

@Injectable({
  providedIn: 'root'
})
export class BpmClaimService extends BaseService {

  constructor(
    http: HttpClient
  ) {
    super(http, `${environment.bpm_claim}/bpm-claim`);
  }

  postTask(body: any) {
    return this.http.post(`${this.baseUrl}/task`, body)
  }

  getListTask(requestId: string) {
    return this.http.get(`${this.baseUrl}/task?requestId=${requestId}`)
  }

  handleTask(body: ICloseTask) {
    return this.http.put(`${this.baseUrl}/task`, body)
  }

  exportFile(id: number) {
    return this.http.get(`${this.baseUrl}/task/attachment/${id}`)
  }
}
