import {Injectable} from "@angular/core";
import {BaseService} from "@cores/services/base.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "@env";
import {BehaviorSubject, filter, map, MonoTypeOperatorFunction, Observable, tap} from "rxjs";
import {IErrorResponse, IPageableResponseModel, IResponseModel} from "@cores/models/response.model";
import {
  ClaimSAPAttachments,
  IBaseClaimProcessItemResponse,
  IClaimAttachmentsResponse,
  IClaimDetailResponse,
  IClaimHistoryItem,
  IDetailRequestProcessResponse,
  INeedToWorkClaimResponse, ISapAttachmentDownload
} from "@claim-detail/interfaces/claim-tpa-response.interface";
import {CreateClaimService} from "@create-requests/service/create-claim.service";
import {CommonCategoryService} from "@cores/services/common-category.service";
import {
  CLAIM_UNDERWRITING_DECISION,
  ClaimCommonType,
  PAYMENT_METHOD,
  SAP_IDENTIFICATION_TYPE_CODE
} from "@cores/utils/constants";
import {createErrorMessage, mapCommonCategoryByCode, mapDataTableWithType} from "@cores/utils/functions";
import {NotificationMessageService} from "@cores/services/message.service";
import {
  ISubmitOrSaveInvestigatorDecisionRequest,
  ISubmitOrSaveUnderwritingDecisionRequest
} from "@claim-detail/interfaces/claim-tpa-request.interface";
import {AuthService} from "@cores/services/auth.service";
import {CommonClaimCategory} from "@cores/models/common-claim.model";
import {CommonModel} from "@common-category/models/common-category.model";

@Injectable({providedIn: 'root'})
export class DetailClaimService extends BaseService {

  private _claimDetail$ = new BehaviorSubject<IClaimDetailResponse>(null!);

  private _benefits$ = new BehaviorSubject<CommonClaimCategory[]>([]);
  private _sickness$ = new BehaviorSubject<CommonClaimCategory[]>([]);
  private _facility$ = new BehaviorSubject<CommonClaimCategory[]>([]);
  private _claimStatusCommon$ =  new BehaviorSubject<CommonClaimCategory[]>([])
  private _reUnderwritingCommon$ = new BehaviorSubject<CommonModel[]>([])

  private _loading = new BehaviorSubject<number>(0);
  private _lastRefer$ = new BehaviorSubject<IBaseClaimProcessItemResponse | null>(null)

  private _displayPermission$ = new BehaviorSubject<{ [p: string]: boolean }>({})

  constructor(
    http: HttpClient,
    private authService: AuthService,
    private createClaimService: CreateClaimService,
    private categoryService: CommonCategoryService,
    private messageService: NotificationMessageService,
  ) {
    super(http, `${environment.claim_url}/claim`);
    /**
     * Để không bị cancel request khi đổi trang quá nhanh
     */
    setTimeout(() => {
      this.init()
    }, 1000)
  }

  init() {
    this.getBenefits()
    this.getSickness()
    this.getFacility()
    this.getClaimStatus()
  }


  /*
   * ==================================================================================================
   *                                     VARIABLE STAGE
   * ==================================================================================================
   */


  get loadingCount$() {
    return this._loading.asObservable();
  }

  private loading() {
    this._loading.next(this._loading.value + 1);
  }

  private completeLoading() {
    this._loading.next(this._loading.value - 1);
  }

  get claimDetail$() {
    return this._claimDetail$.pipe(filter(detail => !!detail));
  }

  get benefits$() {
    return this._benefits$.asObservable().pipe(filter((benefits) => !!benefits.length));
  }

  get sickness$() {
    return this._sickness$.asObservable().pipe(filter((sickness) => !!sickness.length));
  }

  get facility$() {
    return this._facility$.asObservable().pipe(filter((facility) => !!facility.length));
  }

  get claimStatusCommon$() {
    return this._claimStatusCommon$.asObservable().pipe(filter(val => !!val.length))
  }

  get lastRefer() {
    return this._lastRefer$.pipe(filter((refer) => !!refer));
  }

  setLastRefer(lastRefer: IBaseClaimProcessItemResponse) {
    this._lastRefer$.next(lastRefer)
  }

  get displayPermission() {
    return this._displayPermission$.getValue()
  }

  get displayPermission$() {
    return this._displayPermission$
  }

  get reUnderwritingCommon$() {
    return this._reUnderwritingCommon$.asObservable()
  }


  /*
  * ==================================================================================================
  *                                      OPERATION STAGE
  * ==================================================================================================
  */

  defaultHandleAPI<T>(manualHandleLoading: boolean = false): MonoTypeOperatorFunction<T> {
    return tap(
      {
        error: (error: IErrorResponse) => {
          this.completeLoading()
          this.messageService.error(createErrorMessage(error));
        },
        complete: () => {
          !manualHandleLoading && this.completeLoading();
        }
      }
    );
  }


  /*
   * ==================================================================================================
   *                                      CALL API STAGE
   * ==================================================================================================
   */

  private getBenefits() {
    this.loading();
    this.createClaimService.getBenefit().pipe(
      this.defaultHandleAPI(),
      tap({
        next: (response) => this._benefits$.next(response)
      })
    ).subscribe();
  }

  private getSickness() {
    this.loading();
    this.createClaimService.getDiseases().pipe(
      this.defaultHandleAPI(),
      tap({
        next: (response) => this._sickness$.next(response)
      })
    ).subscribe();
  }

  private getFacility() {
    this.loading();
    this.createClaimService.getFacility().pipe(
      this.defaultHandleAPI(),
      tap({
        next: (response) => this._facility$.next(response)
      }),
    ).subscribe();
  }

  getDetail(requestId: string) {
    this.loading();
    const url = `${this.baseURL}/detail`;
    return this.http.get<IResponseModel<IClaimDetailResponse>>(url, {params: {requestId}}).pipe(
      map((response) => response.data),
      tap({
        next: (response) => {
          if (response?.allByRequestId?.pic == this.authService?.userProfile?.userDTO.username) {
            this.dispatchDisplayPermission({isPIC: true})
          }
          this._claimDetail$.next(response)
        },
      }),
      this.defaultHandleAPI()
    );
  }

  getCommonModel() {
    this.loading()
    return this.categoryService.getCommonByCodes(
      [SAP_IDENTIFICATION_TYPE_CODE]
    ).pipe(
      map((value) => mapCommonCategoryByCode(value, SAP_IDENTIFICATION_TYPE_CODE)),
      this.defaultHandleAPI()
    );
  }

  getPaymentMethod() {
    this.loading()
    return this.categoryService.getCommonByCodes(
      [PAYMENT_METHOD]
    ).pipe(
      map((value) => mapCommonCategoryByCode(value, PAYMENT_METHOD)),
      this.defaultHandleAPI()
    );
  }

  /**
   * @param partnerId
   * <pre>
   * Mã định danh người được bảo hiểm
   *</pre>
   *
   * @param page
   * @param size
   *
   * @param type { "1" | "2"}
   * <pre>
   *  “1”: Trạng thái tồn tại dữ liệu Claim
   *  “2”: Dữ liệu Claim
   * </pre>
   *
   * @param sourceData {"1" | "2" | ""}
   *<pre>
   *  ““: Tất cả dữ liệu claim : Claim Service + SAP
   *  “1”: Dữ liệu Claim trên SAP |
   *  “2”: Dữ liệu Claim trên Claim Service
   * </pre>
   */
  getHistoryClaim(partnerId: string, page: number, size: number, type: "1" | "2" = "2", sourceData: "" | "1" | "2" = "") {
    this.loading()
    const url = `${environment.claim_url}/claim/histories`;
    return this.http.get<IResponseModel<IPageableResponseModel<IClaimHistoryItem>>>(
      url,
      {
        params: {
          /**
           * Mã định danh người được bảo hiểm
           */
          partnerId,
          type,
          sourceData,
          page,
          size
        }
      })
      .pipe(
        this.defaultHandleAPI(),
        map((response) => {
          return mapDataTableWithType(response.data, {page, size})
        })
      );
  }


  getTypeCreateTask(param: string) {
    this.loading()
    return this.categoryService.getCommonCategory(param).pipe(
      this.defaultHandleAPI()
    );
  }

  getNeedToWorkInfo(requestId: string): Observable<IResponseModel<{ data: INeedToWorkClaimResponse }>> {
    this.loading()
    const url = `${environment.task_url}/task/detail?code=${requestId}`;
    return this.http.get<IResponseModel<{ data: INeedToWorkClaimResponse }>>(url)
      .pipe(
        this.defaultHandleAPI()
      );
  }

  /*
   * ============== UNDERWRITING API =================
   */

  getUnderwritingDecision() {
    this.loading()
    return this.categoryService.getCommonCategory(CLAIM_UNDERWRITING_DECISION)
      .pipe(
        this.defaultHandleAPI(),
        tap((rs) => {
          this._reUnderwritingCommon$.next(rs)
        })
      )
  }

  submitUnderWritingDecision(request: ISubmitOrSaveUnderwritingDecisionRequest) {
    this.loading()
    const url = `${environment.bpm_claim}/bpm-claim/task/re-underwriting/submit`
    return this.http.put<IResponseModel<INeedToWorkClaimResponse>>(url, request).pipe(
      this.defaultHandleAPI()
    )
  }

  updateUnderWritingDecision(request: Partial<ISubmitOrSaveUnderwritingDecisionRequest>) {
    this.loading()
    const url = `${environment.bpm_claim}/bpm-claim/task/re-underwriting/update`
    return this.http.put<IResponseModel<INeedToWorkClaimResponse>>(url, request).pipe(
      this.defaultHandleAPI()
    )
  }


  /*
   * ============== INVESTIGATOR HANDLE =================
   */


  updateInvestigatorDecision(request: Partial<ISubmitOrSaveInvestigatorDecisionRequest>) {
    this.loading()
    const url = `${environment.bpm_claim}/bpm-claim/task/investigation/update`
    return this.http.put<IResponseModel>(url, request).pipe(
      this.defaultHandleAPI()
    )
  }

  submitInvestigatorDecision(request: ISubmitOrSaveInvestigatorDecisionRequest) {
    this.loading()
    const url = `${environment.bpm_claim}/bpm-claim/task/investigation/submit`
    return this.http.put<IResponseModel>(url, request).pipe(
      this.defaultHandleAPI()
    )
  }

  getTypeCommonCategory(code: string) {
    return this.categoryService.getCommonCategory(code)
  }

  getProcessRequestInformation(requestId: string) {
    this.loading()
    const url = ` ${environment.claim_url}/claim/detail-process`
    return this.http.get<IResponseModel<IDetailRequestProcessResponse>>(url, {params: {requestId}}).pipe(
      this.defaultHandleAPI()
    )
  }

  getAttachments(claimCaseId: number) {
    this.loading()
    const url = `${environment.claim_url}/claim/attachment`
    return this.http.get<IResponseModel<IClaimAttachmentsResponse>>(url,
      {params: {claimCaseId}}).pipe(this.defaultHandleAPI())
  }

  removeFile(claimCaseId: number, caseID: string) {
    const url = `${environment.claim_url}/claim/attachment`
    return this.http.delete(url, {
      body: {
        claimCaseId,
        fileCaseIdList: [caseID]
      }
    })
  }

  getPicCreateTask(params: any) {
    return this.http.get(`${environment.category_url}/request-type-config/requestType/${params}`)
  }

  uploadFile(claimCaseId: number, recipient: string, files: any[]) {
    this.loading()
    const url = `${environment.claim_url}/claim/add-attachment`
    return this.http.post<IResponseModel>(url, {claimCaseId, recipient, files}).pipe(
      this.defaultHandleAPI()
    )
  }

  getAttachmentCreateTask(caseId: number, requestId: string) {
    return this.http.get(`${this.baseUrl}/attachment?claimCaseId=${caseId}&claimRequestId=${requestId}`)
  }

  getTemplateEmail() {
    return this.categoryService.getCommonCategory('EMAIL_CLAIM_TEMPALTE')
  }

  dispatchDisplayPermission(permission: { [p: string]: boolean }) {
    this._displayPermission$.next({
      ...this._displayPermission$.getValue(),
      ...permission
    })
  }

  private getClaimStatus() {
    this.loading()
    return this.categoryService.getCommonClaimByTypes(ClaimCommonType.STATUS_CLAIM)
      .pipe(
        this.defaultHandleAPI(),
        tap(data => {this._claimStatusCommon$.next(data)})
      ).subscribe()
  }

  getClaimResolution() {
    this.loading()
    return this.categoryService.getCommonClaimByTypes(ClaimCommonType.RESOLUTION_REQUEST)
      .pipe(
        this.defaultHandleAPI(),
      )
  }

  sendDecision(body: any) {
    return this.http.post(`${environment.claim_url}/claim/approve-decision`, body)
  }


  // configSendMail
  previewEmail(body: any) {
    return this.http.post(`${environment.claim_url}/claim/mail/preview`, body)
  }

  getDataEmailSaved(param: string) {
    return this.http.get(`${environment.claim_url}/claim/mail?claimRequestId=${param}`)
  }

  saveEmailTemplate(body: any) {
    return this.http.put(`${environment.claim_url}/claim/mail`, body)
  }

  getCorrespondence(claimId: string) {
    return this.http.get(`${environment.claim_url}/claim/correspondence?claimRequestId=${claimId}`)
  }

  sendEmail(data: any) {
    return this.http.post(`${environment.claim_url}/claim/mail`, data)
  }

  openEmail(idEmail: string) {
    return this.http.get(`${environment.claim_url}/claim/mail/attachment?attachmentId=${idEmail}`, {responseType: 'text'})
  }

  getSAPAttachment(policyNumber: string | number) {
    const url = `${environment.claim_url}/claim/attachment/sap`
    return this.http.get<IResponseModel<ClaimSAPAttachments>>(url, {
      params: {
        policyNumber
      }
    })
  }

  getSapAttachment(arcDocId: string  | number) {
    const url = `${environment.claim_url}/claim/attachment/sap/download`
    return this.http.get<IResponseModel<ISapAttachmentDownload>>(url,
      {
        params: {
          arcDocId
        }
      })
  }
}

