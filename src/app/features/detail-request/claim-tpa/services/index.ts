import { DetailClaimService } from "@claim-detail/services/detail-claim.service";
import { BpmClaimService } from "@claim-detail/services/bpm-claim.service";

export const SERVICES = [DetailClaimService, BpmClaimService]

export * from "@claim-detail/services/detail-claim.service";
export * from "@claim-detail/services/bpm-claim.service";
