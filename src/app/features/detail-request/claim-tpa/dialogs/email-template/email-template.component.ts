import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {DetailClaimService} from "@claim-detail/services/detail-claim.service";
import {CommonModel} from "@common-category/models/common-category.model";
import {Validators} from "@angular/forms";
import {CustomValidators} from "@cores/utils/custom-validators";
import {createErrorMessage} from "@cores/utils/functions";
import Editor from "@ckeditor5-bundle/ckeditor"
import {DialogService} from "primeng/dynamicdialog";
import {EmailPreviewComponent} from "@claim-detail/dialogs/email-preview/email-preview.component";
import {forkJoin} from "rxjs";
import {SendMailTemplateComponent} from "@claim-detail/dialogs";
import {ScreenType} from "@cores/utils/enums";
import {isArray} from "lodash";

@Component({
    selector: 'app-email-template',
    templateUrl: './email-template.component.html',
    styleUrls: ['../send-mail-template/send-mail-template.component.scss'],
})
export class EmailTemplateComponent extends BaseActionComponent implements OnInit {
    header?: string
    emailTypeList!: CommonModel[]
    dataEmail: any
    showConfirm: boolean = false
    showClose: boolean = false
    public Editor = Editor

    constructor(inject: Injector, private service: DetailClaimService, private dialogService: DialogService) {
        super(inject, service);
        this.header = this.configDialog?.header
        this.dataEmail = this.configDialog?.data?.dataEmail
    }

    ngOnInit(): void {
        this.loadingService.start()
        this.form.controls['subject'].setValue(`Thư Thông báo giải quyết quyền lợi bảo hiểm HĐ ${this.dataEmail?.policyNumber} Khách hàng ${this.dataEmail?.laName}`)
        const emailTypeList = this.service.getTypeCommonCategory('EMAIL_TYPE')
        const template = this.service.getTemplateEmail()
        forkJoin([emailTypeList, template]).subscribe({
            next: ([emailTypeList, template]) => {
                this.emailTypeList = emailTypeList
                this.form.patchValue(this.dataEmail)
                if (!this.dataEmail?.isSaved) {
                    this.form.controls['content'].setValue(template.find(value => {
                        return value.value === this.dataEmail.emailType
                    })?.description)
                }
                this.loadingService.complete()
            },
            error: (err) => {
                this.messageService.error(createErrorMessage(err))
                this.loadingService.complete()
            }
        })
    }

    override form = this.fb.group({
        subject: [null, [Validators.required, Validators.maxLength(500)]],
        to: [[], [Validators.required, CustomValidators.email]],
        cc: [[], CustomValidators.emailArray],
        bcc: [[], CustomValidators.emailArray],
        content: [null, Validators.required],
        emailType: [null],
        policyType: [null],
        amount: [null],
        terms: [null],
        decision: [null],
        rating: [null],
        exclusion: [null],
        cancelRider: [null],
        claimRequestId: [null]
    })

    sendMail() {
        this.showConfirm = false
        const data = this.form.value
        const to = []
        if (!isArray(data.to)) {
            to.push(data.to)
            data.to = to
        }
        this.loadingService.start()
        this.service.sendEmail(data).subscribe({
            next: () => {
                this.messageService.success('MESSAGE.SUCCESS')
                this.loadingService.complete()
                this.refDialog.close(true)
            },
            error: (err) => {
                this.messageService.error(createErrorMessage(err))
                this.loadingService.complete()
            }
        })
    }

    override save() {
        if (this.loadingService.loading) return;
        this.form.markAllAsTouched()
        if (this.form.invalid) {
            return
        }
        this.loadingService.start()
        const data = this.form.value
        const to = []
        if (!isArray(data.to)) {
            to.push(data.to)
            data.to = to
        }
        this.service.saveEmailTemplate(data).subscribe({
            next: (res) => {
                this.messageService.success('MESSAGE.SUCCESS')
                this.loadingService.complete()
                this.refDialog.close(true)
            },
            error: (err) => {
                this.messageService.error(createErrorMessage(err))
                this.loadingService.complete()
            }
        })
    }

    previewEmail() {
        this.dialogService?.open(EmailPreviewComponent, {
            showHeader: false,
            width: '70%',
            data: {
                body: this.form.value
            },
        })
    }

    chooseOtherEmail() {
        this.refDialog.close()
        this.dialogService?.open(SendMailTemplateComponent, {
            showHeader: false,
            width: '40%',
            data: {
                screenType: ScreenType.Create,
                claimRequestId: this.dataEmail.claimRequestId,
                policyNumber: this.dataEmail.policyNumber,
                laName: this.dataEmail.laName
            },
        })
    }

    popUpCloseMail() {
        this.showClose = true
    }

    closeDialog() {
        if (this.configDialog) {
            this.refDialog.close();
        } else {
            this.location.back()
        }
    }

}
