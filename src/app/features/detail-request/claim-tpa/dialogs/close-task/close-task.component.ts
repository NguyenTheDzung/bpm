import { Component, Injector } from "@angular/core";
import { BaseActionComponent } from "@shared/components";
import { ICreateTaskFormValue } from "@claim-detail/interfaces/claim-tpa.interface";
import { BpmClaimService } from "@claim-detail/services/bpm-claim.service";
import { createErrorMessage } from "@cores/utils/functions";

@Component({
  selector: "app-close-task",
  templateUrl: "./close-task.component.html",
  styleUrls: ["./close-task.component.scss"]
})
export class CloseTaskComponent extends BaseActionComponent {
  override data!: ICreateTaskFormValue
  constructor(
    protected inject: Injector,
    protected bpmClaimServer: BpmClaimService
  ) {
    super(inject, bpmClaimServer);
    this.data = this.configDialog.data
  }

  override form = this.fb.group({
    reason: [null]
  })

  override save() {
    this.form.markAllAsTouched()
    if (this.form.invalid) return
    this.loadingService.start()
    const data: any = {
      taskId: this.data.code,
      type: this.data.type,
      status: !!this.data.check ? "CLOSE" : "CANCEL"
    }
    if (!this.data.check) {
      data.reason = this.form.controls['reason'].value
    }
    this.bpmClaimServer.handleTask(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
        this.refDialog.close(true)
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete()
      }
    })
  }

}
