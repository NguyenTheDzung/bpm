import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {DetailClaimService} from "@claim-detail/services/detail-claim.service";
import {CommonModel} from "@common-category/models/common-category.model";
import {forkJoin} from "rxjs";
import {createErrorMessage} from "@cores/utils/functions";
import {Validators} from "@angular/forms";
import {ISendMailForm} from "@claim-detail/interfaces/send-mail.interface";
import {DialogService} from "primeng/dynamicdialog";
import {EmailTemplateComponent} from "@claim-detail/dialogs/email-template/email-template.component";

@Component({
  selector: 'app-send-mail-template',
  templateUrl: './send-mail-template.component.html',
  styleUrls: ['./send-mail-template.component.scss']
})
export class SendMailTemplateComponent extends BaseActionComponent implements OnInit {

  emailType!: CommonModel[]
  policyType!: CommonModel[]
  payAmount!: CommonModel[]
  terms!: CommonModel[]
  decisionEmail!: CommonModel[]
  rating!: CommonModel[]
  cancelRider!: CommonModel[]

  constructor(inject: Injector, private service: DetailClaimService, private dialogService: DialogService) {
    super(inject, service);
  }

  override form = this.fb.group({
    emailType: [null, Validators.required],
    policyType: [null, Validators.required],
    amount: [null, Validators.required],
    terms: [null, Validators.required],
    decision: [null, Validators.required],
    rating: [null, Validators.required],
    exclusion: [null, Validators.required],
    cancelRider: [null, Validators.required]
  }) as ISendMailForm;

  ngOnInit(): void {
    this.loadingService.start()
    const emailType = this.service.getTypeCommonCategory('EMAIL_TYPE')
    const policyType = this.service.getTypeCommonCategory('POLICY_TYPE')
    const payAmount = this.service.getTypeCommonCategory('AMOUNT')
    const terms = this.service.getTypeCommonCategory('TERMS')
    const decisionEmail = this.service.getTypeCommonCategory('DECISION_EMAIL')
    const rating = this.service.getTypeCommonCategory('RATING')
    const cancelRider = this.service.getTypeCommonCategory('CANCEL_RIDER')
    forkJoin([emailType, policyType, payAmount, terms, decisionEmail, rating, cancelRider]).subscribe({
      next: ([emailType, policyType, payAmount, terms, decisionEmail, rating, cancelRider]) => {
        this.emailType = emailType
        this.policyType = policyType
        this.payAmount = payAmount
        this.terms = terms
        this.decisionEmail = decisionEmail
        this.rating = rating
        this.cancelRider = cancelRider
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }

  handleValueEmailType(event: any) {
    this.form.reset({
      emailType: this.form.controls.emailType?.value
    })
    this.form.disable()
    this.form.controls.emailType?.enable()
    switch (event?.value) {
      case '2':
        this.form.controls.policyType?.enable()
        this.form.controls.amount?.enable()
        break
      case '1':
        this.form.controls.policyType?.enable()
        this.form.controls.terms?.enable()
        this.form.controls.decision?.enable()
        break
      case '3':
        this.form.controls.policyType?.enable()
        this.form.controls.terms?.enable()
        this.form.controls.rating?.enable()
        this.form.controls.exclusion?.enable()
        this.form.controls.cancelRider?.enable()
    }
  }

  override save() {
    this.form.markAllAsTouched()
    if (this.form.invalid) {
      return
    }
    this.dialogService?.open(EmailTemplateComponent, {
      header: this.form.controls.emailType?.value,
      showHeader: false,
      width: '45%',
      data: {
        listEmailType: this.emailType,
        dataEmail: {
          ...this.form.value,
          claimRequestId: this.configDialog.data?.claimRequestId,
          policyNumber: this.configDialog.data?.policyNumber,
          laName: this.configDialog.data?.laName
        }
      },
    }).onClose.subscribe(result => {
      if (result) {
        this.refDialog.close(true)
      }
    })
  }

  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close(false);
    } else {
      this.location.back()
    }
  }
}
