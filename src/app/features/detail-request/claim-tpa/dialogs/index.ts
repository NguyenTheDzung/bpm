import {CreateTaskDialogComponent} from "./create-task-dialog/create-task-dialog.component";
import {CloseTaskComponent} from "./close-task/close-task.component";
import {SendMailTemplateComponent} from "./send-mail-template/send-mail-template.component";
import {EmailTemplateComponent} from "./email-template/email-template.component";
import {ClaimForwardUserComponent} from "./claim-forward-user/claim-forward-user.component";
import {EmailPreviewComponent} from "@claim-detail/dialogs/email-preview/email-preview.component";

export const DIALOGS = [
  CreateTaskDialogComponent,
  CloseTaskComponent,
  SendMailTemplateComponent,
  EmailTemplateComponent,
  ClaimForwardUserComponent,
  EmailPreviewComponent
];

export * from "./create-task-dialog/create-task-dialog.component";
export * from "./close-task/close-task.component";
export * from "./send-mail-template/send-mail-template.component";
export * from "./email-template/email-template.component";
export * from "./claim-forward-user/claim-forward-user.component";
