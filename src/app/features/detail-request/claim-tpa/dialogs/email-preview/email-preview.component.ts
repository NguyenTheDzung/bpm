import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {DetailClaimService} from "@claim-detail/services";
import {createErrorMessage, dataURItoBlob} from "@cores/utils/functions";
import {isArray} from "lodash";

@Component({
  selector: 'app-email-preview',
  templateUrl: './email-preview.component.html',
  styleUrls: ['./email-preview.component.scss']
})
export class EmailPreviewComponent extends BaseActionComponent implements OnInit {
  body: any
  blob?: Blob;
  emailContentUrl = ''

  constructor(inject: Injector, private service: DetailClaimService) {
    super(inject, service)
    this.body = this.configDialog.data?.body
  }

  ngOnInit(): void {
    this.loadingService.start()
    const data = this.body
    const to = []
    if (!isArray(data.to)) {
      to.push(data.to)
      data.to = to
    }
    this.service.previewEmail(data).subscribe({
      next: (res: any) => {
        this.blob = dataURItoBlob(res.data);
        this.emailContentUrl = URL.createObjectURL(this.blob!);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })

  }

  completeLoading(event: any) {
    if (event) this.loadingService.complete()
  }

  close() {
    this.refDialog.close()
  }
}
