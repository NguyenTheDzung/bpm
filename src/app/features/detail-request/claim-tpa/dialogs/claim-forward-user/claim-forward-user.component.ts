import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BaseActionComponent } from '@shared/components';

import { createErrorMessage } from '@cores/utils/functions';
import { UserModel } from '@user/models/user.model';
import { RequestService } from '@requests/services/request.service';

@Component({
  selector: 'app-claim-forward-user',
  templateUrl: './claim-forward-user.component.html',
  styleUrls: ['./claim-forward-user.component.scss'],
})
export class ClaimForwardUserComponent extends BaseActionComponent implements OnInit {
  userList: UserModel[] = [];
  nameUser?: string | null;
  override form = this.fb.group({
    dept: null,
    requestId: [[this.data]],
    newPic: [[], [Validators.required]],
  });

  constructor(inject: Injector, private services: RequestService, private translate: TranslateService) {
    super(inject, services);
  }

  ngOnInit(): void {
    this.onChange();
  }

  onChange() {
    if (this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    const roleName = this.configDialog?.data?.roleName;
    this.form.get('dept')?.setValue(roleName);
    this.services
      .getPic({ roleName, activeOnly: true })
      .pipe()
      .subscribe({
        next: (response) => {
          this.userList = response || [];
          this.loadingService.complete();
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        },
      });
  }

  onChangUser(event: any) {
    let intersection = this.userList.filter((itemArr) => event.value.includes(itemArr.account));
    let user = intersection.map((item) => item.name);
    this.nameUser = user.join();
  }

  override create(data: any) {
    if (this.loadingService.loading) {
      return;
    }
    this.loadingService.start();
    this.services.forwardRequest(data).subscribe({
      next: () => {
        this.messageService.success(this.translate.instant('MESSAGE.FORWARD_SUCCESS'));
        this.form.reset();
        this.refDialog.close(true);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }
}
