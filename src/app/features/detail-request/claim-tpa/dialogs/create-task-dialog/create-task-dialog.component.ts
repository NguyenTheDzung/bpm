import {Component, Injector, Input, OnInit, QueryList, ViewChildren} from "@angular/core";
import {SelectItem} from "primeng/api/selectitem";
import {BaseActionComponent} from "@shared/components";
import {DetailClaimService} from "@claim-detail/services/detail-claim.service";
import {Validators} from "@angular/forms";
import {
  Attachment,
  CreateTaskFormModel,
  ICreateTaskFormValue
} from "@claim-detail/interfaces/claim-tpa.interface";
import {PROCESSING_TASK, REINSURANCE_ACTION} from "@claim-detail/utils/enums";
import {forkJoin} from "rxjs";
import {CommonModel} from "@common-category/models/common-category.model";
import {createErrorMessage, dataURItoBlob} from "@cores/utils/functions";
import {BpmClaimService} from "@claim-detail/services/bpm-claim.service";
import {FileUpload} from "primeng/fileupload";
import {FileCollection} from "@create-requests/models/collection.model";
import {typeFileCollection, ZCOL_AR} from "@create-requests/utils/constants";
import {CustomValidators} from "@cores/utils/custom-validators";
import {RequestService} from "@requests/services/request.service";

@Component({
  selector: "app-create-task-dialog",
  templateUrl: "./create-task-dialog.component.html",
  styleUrls: ["./create-task-dialog.component.scss", "../../shared/styles.scss"]
})
export class CreateTaskDialogComponent extends BaseActionComponent implements OnInit {
  override data!: CreateTaskFormModel
  @ViewChildren('fileInput') fileInputs: QueryList<FileUpload> | undefined;
  PROCESSING_TASK = PROCESSING_TASK;
  REINSURANCE_ACTION = REINSURANCE_ACTION;
  tasks: CommonModel[] = []
  executors!: CommonModel[]
  body: any
  maxFileSize: number = 0;
  dataDialog!: ICreateTaskFormValue
  checkTabView: number = 0

  override form: CreateTaskFormModel;

  files: FileCollection[] = typeFileCollection;
  department: CommonModel[] = []


  displayAttachment: Attachment[] = []
  attachment: Attachment[] = []


  actions: SelectItem<REINSURANCE_ACTION>[] = [
    {label: "Gửi email", value: REINSURANCE_ACTION.SEND_EMAIL},
    {label: "Nội dung tái bảo hiểm", value: REINSURANCE_ACTION.RE_INSURANCE}
  ];

  constructor(injector: Injector,
              private claimTpaService: DetailClaimService,
              private bpmClaimService: BpmClaimService,
              private requestService: RequestService,) {
    super(injector, claimTpaService);
    this.dataDialog = this.configDialog.data
    this.form = this.fb.group(
      {
        type: [null, Validators.required],
        department: [null, [Validators.required]],
        pic: [null],
        content: [""],
        action: [null, Validators.required],
        subject: [null, Validators.required],
        emailTo: [null, [CustomValidators.email, Validators.required]],
        emailCc: [null, CustomValidators.emailArray],
        emailContent: [null, [Validators.required, Validators.maxLength(3000)]],
        reInsuranceContent: [null]
      }
    ) as CreateTaskFormModel;

    if (!!this.dataDialog.attachmentClaims?.length) {
      this.displayAttachment = this.dataDialog.attachmentClaims
    }

    this.form.controls.type.valueChanges.subscribe(() => {
      this.changeValidateFormForProcessTask();
    })
  }

  ngOnInit(): void {
    this.loadingService.start();
    const task = this.claimTpaService.getTypeCreateTask('CLAIMS_TASK_PROCESS')
    const department = this.claimTpaService.getTypeCreateTask('GROUP_USER_V2')
    forkJoin([task, department]).subscribe({
      next: (res: any) => {
        this.tasks = res[0];
        this.department = res[1];
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
    if (this.configDialog.data?.type) {
      this.form.patchValue(this.configDialog.data)
      this.changeValidateFormForAction({value: this.configDialog.data.type})
    }
  }


  changeValidateFormForProcessTask() {
    const {controls} = this.form;
    switch (this.form.controls.type.value as PROCESSING_TASK || null) {
      case PROCESSING_TASK.INSPECT:
        controls.emailCc.disable();
        controls.emailTo.disable();
        controls.action.disable();
        controls.emailContent.disable();
        controls.subject.disable();
        controls.department.disable();
        controls.reInsuranceContent.disable();
        controls.department.setValue('OP_CLAIM_ASSESSOR');

        controls.content.enable();
        controls.pic.enable();
        break;
      case PROCESSING_TASK.REAPPRAISAL:
        controls.emailCc.disable();
        controls.emailTo.disable();
        controls.action.disable();
        controls.emailContent.disable();
        controls.subject.disable();
        controls.reInsuranceContent.disable();

        controls.content.enable();
        controls.pic.enable();
        controls.department.enable();
        if (!this.configDialog.data?.type) {
          controls.department.setValue('OP_NBU_UNDERWRITTING')
        }
        break;
      case PROCESSING_TASK.REINSURANCE:
        controls.action.enable()
        controls.content.disable()
        controls.pic.disable();
        controls.department.disable();
        break;
    }
  }

  changeValidateFormForAction(event?: any) {
    this.checkTabView = 0
    if (this.form.controls.type.value !== PROCESSING_TASK.REINSURANCE) {
      this.getPic(event?.value)
    }
    const {controls} = this.form;
    switch (this.form.controls.action.value as REINSURANCE_ACTION || null) {
      case  REINSURANCE_ACTION.SEND_EMAIL:
        controls.reInsuranceContent.disable();

        controls.emailCc.enable();
        controls.emailTo.enable();
        controls.action.enable();
        controls.emailContent.enable();
        controls.subject.enable();
        break;
      case REINSURANCE_ACTION.RE_INSURANCE:
        controls.reInsuranceContent.enable();
        controls.action.enable();

        controls.emailCc.disable();
        controls.emailTo.disable();
        controls.emailContent.disable();
        controls.subject.disable();
        break;
    }
  }


  handleFileUpload(event: FileUpload, index: number) {
    this.files[index].content = [];
    event?.files?.forEach((file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.files[index].content?.push(
          (<string>reader.result)?.substring((<string>reader.result).lastIndexOf(',') + 1)
        );
      };
    });
    this.files[index].file = event?.files;
  }

  handleFileSize() {
    const requestFileDTOs: any[] = [];
    this.maxFileSize = 0;
    this.files.forEach((item) => {
      if (item.file) {
        item.file.forEach((file, i) => {
          this.maxFileSize += file.size;
          requestFileDTOs.push({
            name: file.name,
            type: ZCOL_AR,
            extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
            content: item.content![i],
            mimeType: file.type,
          });
        });
      }
    });
    return requestFileDTOs;
  }

  override save() {
    if (this.loadingService.loading) {
      return;
    }
    this.form.markAllAsTouched()
    if (this.form.invalid) {
      return
    }
    const data: any = {
      ...this.form.value
    }
    switch (data.type) {
      case '14':
        this.body = {
          reInsuranceTaskCreate: {...data},
          requestFileDTOs: this.handleFileSize()
        }
        if (this.form.controls.action.value === REINSURANCE_ACTION.SEND_EMAIL) {
          this.body.attachmentClaims = this.displayAttachment
        }
        delete this.body.reInsuranceTaskCreate.type
        break;
      case '12':
        this.body = {
          ...data,
          department: this.form.controls['department'].value
        }
        break;
      default:
        this.body = {...data}
    }
    if (this.dataDialog?.type) {
      this.body.taskId = this.dataDialog.code
    }
    this.body.type = data.type || null
    this.body.requestId = this.dataDialog?.requestId
    this.body.claimId = this.dataDialog?.claimId
    this.body.policyNumber = this.dataDialog?.policyNumber
    this.body.icCode = this.dataDialog.icCode
    this.body.icName = this.dataDialog.icName
    this.body.bpCode = this.dataDialog.bpCode
    this.body.customerName = this.dataDialog.customerName
    this.body.longText = this.dataDialog.longText
    this.body.icNumber = this.dataDialog.icCode
    this.loadingService.start()
    this.bpmClaimService.postTask(this.body).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS')
        this.loadingService.complete()
        this.refDialog.close(true)
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  getPic(value: string) {
    this.executors = []
    this.form.controls['pic'].enable()
    this.loadingService.start()
    this.claimTpaService.getPicCreateTask(value).subscribe({
      next: (res: any) => {
        res.data?.listUser?.forEach((value: any) => {
          const account = value
          this.executors?.push({
            name: account,
            value: account
          })
        })
        if (this.executors.length === 1) {
          this.form.controls['pic'].setValue(this.executors[0].value)
        }
        if (this.configDialog.data?.type) {
          this.form.controls['pic'].setValue(this.configDialog.data.pic)
        }
        if (res.data?.assignType === 'NO_ASSIGN') {
          this.executors?.push({
            name: "ADMIN_FORWARD",
            value: 'ADMIN_FORWARD'
          })
          this.form.controls['pic'].setValue('ADMIN_FORWARD')
          this.form.controls['pic'].disable()
        }
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  changeCheckTabView(index: number) {
    this.checkTabView = index
    if (index == 2) {
      this.loadingService.start()
      this.claimTpaService.getAttachmentCreateTask(this.dataDialog.caseId, this.dataDialog.requestId).subscribe({
        next: (res: any) => {
          this.attachment = res?.data
          this.loadingService.complete()
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err))
          this.loadingService.complete();
        }
      })
    }
  }

  clearFile() {
    this.files = [
      {
        required: false,
        type: null,
        label: "createRequest.requestCollection.paymentAdjustmentRequestForm",
        file: null,
        content: null,
        mimeType: null,
        extension: null

      }
    ]
    this.fileInputs?.forEach(
      item => {
        item._files = []
      }
    )
    this.fileInputs?.forEach((item: FileUpload) => {
      item.clear();
    });
  }

  viewAttachment(id: number) {
    this.loadingService.start();
    this.requestService.getAttachmentCollection(id).subscribe({
      next: (data: any) => {
        this.openFile(data?.data.mainDocument!, true);
        this.loadingService.complete();
      },
    });
  }

  openFile(content: string, isPDF: boolean) {
    if (isPDF) {
      const url = URL.createObjectURL(dataURItoBlob(content, 'application/pdf'));
      window.open(url, '_blank')?.focus();
    } else {
      let image = new Image();
      image.src = `data:image/png;base64,${content}`;
      let wd = window.open('', '_blank');
      wd!.document.write(image.outerHTML);
    }
    this.loadingService.complete();
  }


  checkDisableSelected(item: Attachment) {
    return !!this.dataDialog.attachmentClaims?.find((value: any) => {
      return value.fileName === item.fileName
    })
  }

  onChangeSelection($event: any) {
    let attachmentGroup = [
      ...$event
    ]
    if (this.dataDialog?.attachmentClaims) {
      attachmentGroup = [
        ...this.dataDialog.attachmentClaims,
        ...attachmentGroup
      ]
    }
    this.displayAttachment = attachmentGroup
  }
}
