import {Component, EventEmitter, Input, Output} from "@angular/core";
import {expandCollapseAnimation} from "@detail-request/shared/utils/animation";

export type styleClass = string | string[] | Set<string> | { [p: string]: any }

@Component({
  selector: "claim-section-template",
  template: `
    <section [ngClass]="styleClass ?? ''" [class.card]="isCard">
      <div class="pl-2">
        <div class="relative">
          <a href="javascript:void(0)" class="absolute mbal-icon-expand-position" *ngIf="canExpand" (click)="expanding($event)">
            <i class="pi pi-chevron-right transition" [ngClass]="{ 'expanded': isExpanded }"></i>
          </a>
          <ng-container *ngIf="mode !== 'normal'">
            <div
              [ngClass]="{'claim__info-section-header': mode === 'info', 'claim__sub-section-header': mode === 'sub'}">
              <h5 class="m-0 header uppercase " (click)="isExpanded && expanding($event)" [ngClass]="headerStyleClass ?? ''">{{header! | translate}}</h5>
              <strong>{{subHeader ?? ''}}</strong>
            </div>
          </ng-container>
          <ng-container *ngIf="mode === 'normal'">
            <h5 class="text-base claim__header" [ngClass]="headerStyleClass ?? ''">{{header! | translate}}</h5>
          </ng-container>
        </div>
        <div
          *ngIf="(canExpand && isExpanded) || !canExpand"
          @expandCollapse
          class="claim__body"
          [ngClass]="bodyStyleClass ?? {'claim__section-body': mode !== 'normal'}">
          <ng-content></ng-content>
        </div>
      </div>
    </section>
  `,
  styles: [
    `.mbal-icon-expand-position {
      .expanded {
        rotate: 90deg;
      }
      .transition {
        transition: 200ms ease-out;
        font-size: 12px;
      }
    }
    `
  ],
  animations: [
    expandCollapseAnimation({duration: 200})
  ]
})
export class ClaimSectionTemplateComponent {

  @Input() header?: string;
  @Input() mode: "sub" | "info" | "normal" = "normal";
  @Input() styleClass?: styleClass;
  @Input() subHeader?: string;
  @Input() headerStyleClass?: styleClass;
  @Input() bodyStyleClass?: styleClass;
  @Input() isCard: boolean = false;
  @Input() canExpand: boolean = false;
  @Input() isExpanded = true;
  @Output() isExpandedChange = new EventEmitter<boolean>()


  expanding($event: MouseEvent) {
    $event.preventDefault();
    this.isExpanded = !this.isExpanded;
    this.isExpandedChange.emit(this.isExpanded)
  }
}
