import {AfterContentInit, Component, ContentChildren, Input, QueryList} from "@angular/core";
import {MbalTemplate} from "@shared/directives/mbal-template.directive";

@Component({
  selector: "value-display",
  template: `
    <ng-container *ngIf="!value && !valueTmpl else flowValue">
      <ng-template
        [ngIf]="emptyTmpl"
        [ngIfElse]="DefaultEmptyTemplate"
        [ngTemplateOutlet]="emptyTmpl">
      </ng-template>
    </ng-container>

    <ng-template #flowValue>
      <ng-template
        [ngIf]="valueTmpl"
        [ngIfElse]="DefaultValueTemplate"
        [ngTemplateOutlet]="valueTmpl"
        [ngTemplateOutletContext]="{$implicit: value}">
      </ng-template>
    </ng-template>

    <ng-template #DefaultEmptyTemplate>
      - - -
    </ng-template>

    <ng-template #DefaultValueTemplate>
      {{value}}
    </ng-template>
  `
})
export class ValueDisplayComponent implements AfterContentInit {
  @ContentChildren(MbalTemplate) templates: QueryList<MbalTemplate> | undefined

  emptyTmpl!: any;
  valueTmpl!: any;

  @Input() value: any;

  ngAfterContentInit() {
    if (!this.templates) return
    this.templates.forEach(
      item => {
        switch (item.getName()) {
          case "value":
            this.valueTmpl = item.getTemplate()
            break
          case "empty":
            this.emptyTmpl = item.getTemplate()
            break
        }
      }
    )
  }

}
