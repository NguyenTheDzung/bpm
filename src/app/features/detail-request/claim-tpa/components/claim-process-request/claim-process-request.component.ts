import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { DetailClaimService } from "@claim-detail/services/detail-claim.service";
import { CommonClaimCategory } from "@cores/models/common-claim.model";
import { IBenefitValue } from "@claim-detail/interfaces/claim-tpa.interface";
import { BaseComponent } from "@shared/components";
import { REQUEST_PARAMS } from "@detail-request/shared/utils/enums";
import { BehaviorSubject, filter, map } from "rxjs";
import { IDetailRequestProcessResponse } from "@claim-detail/interfaces/claim-tpa-response.interface";
import { findLast } from "lodash";

@Component({
  selector: 'app-claim-process-request',
  templateUrl: './claim-process-request.component.html',
  styleUrls: ['./claim-process-request.component.scss']
})
export class ClaimProcessRequestComponent extends BaseComponent implements OnInit, OnChanges {

  readonly DATE_FORMAT = "DD/MM/YYYY hh:mm:ss a"

  claimProcessResponse$ = new BehaviorSubject<IDetailRequestProcessResponse>(<any>null)

  /**
   * @description
   * Dùng để xác định xem có call API process khi khởi tạo không,
   * hay chờ khi nào truyền vào claimRequestID vào thì mới thực hiện gọi API.
   * Chỉ có tác dụng khi khởi tạo
   * - true - Call API luôn khi khởi tạo component
   * - false - call API khi mà truyền vào một claimRequestID mới
   * @default true
   */
  @Input() loadOnInit = true

  @Input() outsideClaimRequestID?: string

  benefits: (CommonClaimCategory & IBenefitValue)[] = [];

  claimStatusCommon: CommonClaimCategory[] = []

  /**
   * Lưu common config của resolution claim
   */
  claimResolution: CommonClaimCategory[] = []

  /**
   * Resolution hiện tại của request. <br>
   * Backend sẽ trả về mã code nên cần map với common claimResolution để ra được chính xác resolution
   */
  currentRequestResolution?: string

  constructor(
    private detailClaimService: DetailClaimService,
    injector: Injector
  ) {
    super(injector)
    this.latestReferDecision$().subscribe(
      (res) => {
        detailClaimService.setLastRefer(res!)
      })

    detailClaimService.claimStatusCommon$.subscribe(
      cs => this.claimStatusCommon = cs
    )

    detailClaimService.getClaimResolution().subscribe(
      (res) => {
        this.claimResolution = res
      }
    )

    detailClaimService.claimDetail$.subscribe(
      (detail) => {
        this.currentRequestResolution = detail.allByRequestId.resolution
      }
    )

  }

  ngOnChanges(changes: SimpleChanges) {
    /**
     * Chỉ check on changes của claimRequestID khi có loadDetailOnInit bằng false
     */
    if(
      !this.loadOnInit &&
      changes['outsideClaimRequestID'] &&
      !!changes['outsideClaimRequestID'].currentValue
    ) {
      this.getProcessData(this.outsideClaimRequestID)
    }
  }

  ngOnInit(): void {
    this.detailClaimService.benefits$.subscribe(
      {
        next: (benefitCommon) => {
          this.benefits = benefitCommon
        }
      }
    )
    if(this.loadOnInit) this.getProcessData()
  }

  downloadFile($event: MouseEvent, item: any) {

  }

  get claimRequestId() {
    return this.route.snapshot.paramMap.get(REQUEST_PARAMS.CLAIM_REQUEST_ID)!
  }

  getProcessData(claimRequestID: string = this.claimRequestId) {
    this.detailClaimService.getProcessRequestInformation(claimRequestID)
        .pipe(
            map((response) => response.data)
        )
        .subscribe({
          next: (response) => {
            this.claimProcessResponse$.next(response)
          }
        })
  }

  latestReferDecision$() {
    return this.claimProcessResponse$.pipe(
      filter(val => val != null),
      map((claimProcessResponse) => {
        return findLast(claimProcessResponse, (item) => item.decisionCode == '003')
      })
    )
  }

}
