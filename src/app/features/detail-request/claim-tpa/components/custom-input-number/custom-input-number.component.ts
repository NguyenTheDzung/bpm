import { Component, ElementRef, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator } from "@angular/forms";
import { MBALInputTextComponent } from "@shared/components";
import { DecimalPipe } from "@angular/common";

@Component({
  selector: "app-custom-input-number",
  templateUrl: "./custom-input-number.component.html",
  styleUrls: ["./custom-input-number.component.scss"],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CustomInputNumberComponent),
      multi: true
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomInputNumberComponent),
      multi: true
    },
    DecimalPipe
  ]
})
export class CustomInputNumberComponent extends MBALInputTextComponent implements ControlValueAccessor, Validator {

  currencyChars = new RegExp('[\.,]', 'g'); // we're going to remove commas and dots


  constructor(private element: ElementRef) {
    super(element);
  }

}
