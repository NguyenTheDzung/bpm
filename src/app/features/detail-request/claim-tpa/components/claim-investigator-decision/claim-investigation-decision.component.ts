import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren
} from "@angular/core";
import {BaseComponent} from "@shared/components";
import {FileUpload} from "primeng/fileupload";
import {MAX_FILE_SIZE} from "@cores/utils/constants";
import {
  ClaimInvestigationDecisionValue,
  FileUploadPreModel,
  SavedFileModel
} from "@claim-detail/interfaces/claim-tpa.interface";
import {ZCOL_PR} from "@create-requests/utils/constants";
import {convertFileToBase64} from "@claim-detail/utils/functions";
import {NgModel} from "@angular/forms";

@Component({
  selector: 'app-claim-investigation-decision',
  templateUrl: './claim-investigation-decision.component.html',
  styleUrls: ['./claim-investigation-decision.component.scss']
})
export class ClaimInvestigationDecisionComponent extends BaseComponent implements OnChanges{
  @ViewChild(FileUpload) fileInput?: FileUpload

  @ViewChildren(NgModel) ngModels?: QueryList<NgModel>;

  @Input() decisionValue?: ClaimInvestigationDecisionValue

  @Output() decisionValueChange = new EventEmitter<any>()

  @Output() onClickForward = new EventEmitter<Event>()

  content: string = ""
  files: FileUploadPreModel[] = []
  savedFiles: SavedFileModel[] = [];

  readonly MAX_FILE_SIZE = MAX_FILE_SIZE

  forwardPicRule: any

  constructor(
    injector: Injector,
  ) {
    super(injector)
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes['decisionValue'] && !!changes['decisionValue'].currentValue) {
      const value = this.decisionValue!
      this.content = value.content;
      this.savedFiles = value.savedFiles;
      this.files = value.files;
      this.decisionValue = value
    }
  }

  clean() {
    if(!this.fileInput) return
    this.fileInput.files = []
    this.fileInput._files = []
    this.fileInput.clear()
  }

  async fileHandler($event: { files: File[] }) {
    this.files = await Promise.all($event?.files?.map(async (file) => {
      const data: FileUploadPreModel = {
        name: file.name,
        type: ZCOL_PR,
        extension: file.name.substring(file.name.lastIndexOf(".")).toLowerCase(),
        content: await convertFileToBase64(file),
        mimeType: file.type,
        size: file.size + "",
        file: file
      };
      return data;
    }));
    this.onChange();
  }

  onChange() {
    const newValue: ClaimInvestigationDecisionValue = {
      files: this.files,
      content: this.content,
      savedFiles: this.savedFiles
    };
    this.decisionValueChange.emit(newValue);
  }

  removeFile($event: MouseEvent, i: number) {
    $event.preventDefault();
    this.savedFiles.splice(i, 1);
    this.onChange();
  }

  isValid() {
    this.ngModels?.forEach(item => item.control.markAllAsTouched());
    return !this.ngModels?.find(item => item.control.invalid);
  }

  forward() {
    this.onClickForward.emit()
  }
}
