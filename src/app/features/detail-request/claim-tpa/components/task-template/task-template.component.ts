import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChildren,
  Injector,
  Input,
  QueryList,
  TemplateRef
} from '@angular/core';
import { DATE_FORMAT, HISTORY_TYPE } from "@claim-detail/utils/enums";
import { BaseComponent } from "@shared/components";
import { BpmClaimService } from "@claim-detail/services/bpm-claim.service";
import { createErrorMessage, exportFile } from "@cores/utils/functions";
import { CommonModel } from "@common-category/models/common-category.model";
import { MbalTemplate } from "@shared/directives/mbal-template.directive";
import { UpdateHistory } from "@claim-detail/interfaces/claim-tpa.interface";

@Component( {
  selector: 'app-task-template',
  templateUrl: './task-template.component.html',
  styleUrls: [ './task-template.component.scss' ]
} )
export class TaskTemplateComponent extends BaseComponent implements AfterContentInit {
  @Input() info!: UpdateHistory
  @Input() index?: number
  @Input() department!: CommonModel[]

  readonly TYPE_RE_INSURANCE = '14'

  @ContentChildren( MbalTemplate )
  mbalTemplates?: QueryList<MbalTemplate>

  contentFirst?: TemplateRef<any>
  contentLast?: TemplateRef<any>

  readonly HISTORY_TYPE = HISTORY_TYPE;

  /**
   * Dùng để kiểm soát hiển thị field request xử lý
   * - true -> Hiển thị
   * - false -> Không hiển thị
   * @default true - Hiển thị
   */
  @Input() displayCodeAfterSubmit: boolean = true

  constructor( inject: Injector,
               private bpmClaimService: BpmClaimService
  ) {
    super( inject )
  }

  trackByTabs( item: any ) {
    return item
  }

  readonly DATE_FORMAT = DATE_FORMAT;

  ngAfterContentInit() {
    if ( !this.mbalTemplates ){
      return
    }
    this.mbalTemplates?.forEach(
      item => {
        switch ( item.name ) {
          case 'contentFirst' :
            this.contentFirst = item.getTemplate()
            break
          case 'contentLast':
            this.contentLast = item.getTemplate()
            break
        }
      }
    )
    this.ref.detectChanges()
  }

  checkTitleType( historyType?: HISTORY_TYPE | string ) {
    switch ( historyType ) {
      case HISTORY_TYPE.UPDATE_TASK:
        return 'request.detailRequest.claim-tpa.taskEditInfo'
      case HISTORY_TYPE.SUBMIT_TASK:
        return 'request.detailRequest.claim-tpa.summaryInfo'
      default:
        return 'request.detailRequest.claim-tpa.taskInfo'
    }
  }

  checkTitleDate( historyType?: HISTORY_TYPE | string ) {
    switch ( historyType ) {
      case HISTORY_TYPE.UPDATE_TASK:
        return 'request.detailRequest.claim-tpa.createDateEdit'
      case HISTORY_TYPE.SUBMIT_TASK:
        return 'request.detailRequest.claim-tpa.createDateEdit'
      default:
        return 'request.detailRequest.claim-tpa.createDate'
    }
  }

  exportFile( id: number ) {
    this.loadingService.start()
    this.bpmClaimService.exportFile( id ).subscribe( {
      next: ( res: any ) => {
        exportFile( res?.data?.attachmentName, res?.data?.mainDocument, res?.data?.fileType )
        this.loadingService.complete();
      },
      error: ( err ) => {
        this.messageService.error( createErrorMessage( err ) );
        this.loadingService.complete();
      }
    } )
  }
}
