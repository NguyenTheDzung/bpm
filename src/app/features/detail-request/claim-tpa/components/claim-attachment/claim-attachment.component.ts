import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { BaseComponent } from "@shared/components";
import { RequestService } from "@requests/services/request.service";
import { CollectionService } from "@create-requests/service/collection.service";
import { createErrorMessage, dataURItoBlob, openFile } from "@cores/utils/functions";
import { saveAs } from "file-saver";
import { DetailClaimService } from "@claim-detail/services/detail-claim.service";
import { map, switchMap } from "rxjs";
import { ClaimSAPAttachments, IClaimAttachmentItem } from "@claim-detail/interfaces/claim-tpa-response.interface";
import { MAX_FILE_SIZE } from "@cores/utils/constants";
import { FileUploadPreModel } from "@claim-detail/interfaces/claim-tpa.interface";
import { ZCOL_PR } from "@create-requests/utils/constants";
import { convertFileToBase64 } from "@claim-detail/utils/functions";
import { FileUpload } from "primeng/fileupload";

@Component( {
  selector: 'app-claim-attachment',
  templateUrl: './claim-attachment.component.html',
  styleUrls: [ './claim-attachment.component.scss' ]
} )
export class ClaimAttachmentComponent extends BaseComponent implements OnChanges, OnInit {

  @Input() claimCaseId?: number
  attachments: IClaimAttachmentItem[] = []
  readonly MAX_FILE_SIZE = MAX_FILE_SIZE
  readonly fileLimit = 10
  importDialogVisibleState = false

  canImport = this.authService.getUserRoles$().pipe(
    map( ( roles ) => {
      const requireRoles: string[] = [
        "OP_CLAIM_ASSESSOR",
        "OP_CLAIM_DEPUTY",
        "OP_CLAIM_MANAGER",
        "OP_CLAIM_ADMIN"
      ]
      return roles.find( value => requireRoles.includes( value ) )
    } )
  )

  SAPAttachments: ClaimSAPAttachments = []

  constructor(
    injector: Injector,
    private service: RequestService,
    private collectionService: CollectionService,
    private claimService: DetailClaimService
  ) {
    super( injector );
  }

  ngOnChanges( changes: SimpleChanges ): void {
    if ( changes[ 'claimCaseId' ] && !!changes[ 'claimCaseId' ].currentValue ) {
      this.getAttachments( changes[ 'claimCaseId' ].currentValue )
    }
  }

  ngOnInit() {
    this.getSAPAttachments()
  }

  getAttachments( claimCaseId: number ) {
    this.claimService.getAttachments( claimCaseId ).pipe(
      map( ( response ) => response.data )
    ).subscribe( {
      next: ( data ) => {
        this.attachments = data
      }
    } )
  }


  viewAttachment( id: number ) {
    this.loadingService.start();
    this.service.getAttachmentCollection( id ).subscribe( {
      next: ( data: any ) => {
        openFile( data?.data.mainDocument!, true );
        this.loadingService.complete();
      },
    } );
  }

  viewFileFromSAP( id: string ) {
    this.loadingService.start();
    this.claimService.getSapAttachment( id ).subscribe( {
      next: ( data ) => {
        openFile( data.data.result, data.data.type === 'PDF' );
        this.loadingService.complete();
      },
    } );
  }


  download( id: number ) {
    this.loadingService.start();
    this.service.getAttachmentCollection( id ).subscribe( {
      next: ( data: any ) => {
        const blob = dataURItoBlob( data?.data.mainDocument );
        saveAs(
          new Blob( [ blob ], {
            type: 'application/pdf',
          } ),
          data.data.attachmentName
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error( 'An error occurred' );
        this.loadingService.complete();
      },
    } );
  }

  downloadFileFromSAP( id: string ) {
    this.loadingService.start();
    this.claimService.getSapAttachment( id ).subscribe( {
      next: ( data ) => {
        console.log(data)
        const blob = dataURItoBlob( data.data.result );
        saveAs(
          new Blob( [ blob ], {
            type: 'application/pdf',
          } ),
          data.data.fileName
        );
        this.loadingService.complete();
      },
      error: () => {
        this.messageService.error( 'An error occurred' );
        this.loadingService.complete();
      },
    } );
  }

  removeFile( id: number ) {
    this.messageService.confirm().subscribe( ( confirm ) => {
      if ( confirm ) {
        this.loadingService.start()
        this.claimService.removeFile( this.claimCaseId!, id + "" ).subscribe( {
          next: () => {
            this.messageService.success( 'MESSAGE.DELETE_SUCCESS' )
            this.getAttachments( this.claimCaseId! )
            this.loadingService.complete()
          },
          error: ( error ) => {
            this.messageService.error( createErrorMessage( error ) )
            this.loadingService.complete()
          }
        } )
      }
    } )
  }

  async upload( file: FileUpload ) {
    if ( !this.claimCaseId ) return
    this.loadingService.start()
    const convertFile = await this.convertFile( file._files )
    this.claimService.uploadFile( this.claimCaseId, "", convertFile ).subscribe(
      ( response ) => {
        this.importDialogVisibleState = false
        this.messageService.success( "MESSAGE.SUCCESS" )
        file.clear()
        file.clearInputElement()
        this.getAttachments( this.claimCaseId! )
      }
    )
  }

  convertFile( files: File[] ) {
    return Promise.all( files?.map( async ( file, index ) => {
      const data: FileUploadPreModel = {
        name: file.name,
        type: ZCOL_PR,
        extension: file.name.substring( file.name.lastIndexOf( "." ) ).toLowerCase(),
        content: await convertFileToBase64( file ),
        mimeType: file.type,
        size: file.size + "",
        file: file
      };
      return data;
    } ) );
  }


  fileHandler( $event: any ) {
    console.log( $event )
  }

  openImportDialog() {
    this.importDialogVisibleState = true
  }

  private getSAPAttachments() {
    this.claimService.claimDetail$.pipe(
      switchMap( project => {
          return this.claimService.getSAPAttachment( project.policyInfoDto.policyInfo.policyNumber )
        }
      )
    ).subscribe(
      rs => {
        this.SAPAttachments = rs.data
      }
    )
  }
}

