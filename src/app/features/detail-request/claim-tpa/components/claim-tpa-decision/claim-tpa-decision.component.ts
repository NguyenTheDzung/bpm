import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import {BaseComponent} from "@shared/components";
import {
  CheckProvisionCheckboxValue,
  DecisionValue,
  IBenefitSelectionItem,
  IBenefitValue,
  ICreateTaskFormValue,
  IDecisionClaimForm
} from "@claim-detail/interfaces/claim-tpa.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {DetailClaimService} from "@claim-detail/services/detail-claim.service";
import {BenefitCheckboxComponent} from "@claim-detail/components/benefit-checkbox/benefit-checkbox.component";
import {CommonModel} from "@common-category/models/common-category.model";
import {createErrorMessage, openFile} from "@cores/utils/functions";
import {forkJoin} from "rxjs";
import {ScreenType} from "@cores/utils/enums";
import {SendMailTemplateComponent} from "@claim-detail/dialogs/send-mail-template/send-mail-template.component";
import {REQUEST_PARAMS} from "@detail-request/shared/utils/enums";
import {cloneDeep} from "lodash";
import {EmailTemplateComponent} from "@claim-detail/dialogs";


@Component({
  selector: "app-claim-tpa-decision",
  templateUrl: "./claim-tpa-decision.component.html",
  styleUrls: ["./claim-tpa-decision.component.scss"]
})
export class ClaimTpaDecisionComponent extends BaseComponent implements OnInit, OnChanges {
  @ViewChild(BenefitCheckboxComponent) benefitCheckboxComponent?: BenefitCheckboxComponent
  @Input() task!: ICreateTaskFormValue[]
  @Output() reCallDetail = new EventEmitter<void>()
  @Input() decisionValue?: DecisionValue
  @Input() isSentMail: boolean = false
  @Input() isSaveMail: boolean = false
  @Input() claimRequestId: string = ''
  @Input() policyNumber: string = ''
  @Input() laName: string = ''
  @Input() emailType: string = ''
  @Input() emailFileId: string = ''
  form: IDecisionClaimForm = new FormGroup(
    {
      code: new FormControl(null, [Validators.required]),
      note: new FormControl(null, [Validators.required]),
      benefits: new FormControl([]),
      provision: new FormControl([]),
      // suspectedFraud: new FormControl(false),
      // detail: new FormControl(null),
      amount: new FormControl(0),
      reason: new FormControl(null, [Validators.required]),
      reasonNote: new FormControl(null, [Validators.required]),
      reasonOther: new FormControl(null, [Validators.required]),
      sendMail: new FormControl(false),
      paymentException: new FormControl(false)
    }
  ) as IDecisionClaimForm;
  benefits: IBenefitValue[] = []
  decision!: CommonModel[]
  reasonReject!: CommonModel[]
  detailRejectReason!: CommonModel[]
  totalAmountBenefit: number = 0
  totalAmountProvision: number = 0
  totalAmount: number = 0
  deductionAmount: number = 0
  otherAmount: number = 0
  emailTypeList!: CommonModel[]

  // checkSuspectedFraudItem: string[] = []
  // checkBox = ['policy', 'hospital', 'diagnosis', 'other']

  readonly: boolean = false

  constructor(
    injector: Injector,
    private detailClaimService: DetailClaimService
  ) {
    super(injector);

    this.detailClaimService.benefits$.subscribe(
      (benefits) => {
        this.benefits = benefits
        this.form.controls.benefits.setValue([]);
      }
    );


  }

  handleAmount() {
    this.totalAmount = this.totalAmountProvision + this.totalAmountBenefit - this.deductionAmount + this.otherAmount
    if (this.totalAmount < 0) {
      this.form.controls.amount.setValue(this.totalAmountProvision + this.totalAmountBenefit)
      return this.messageService.warn('MESSAGE.CHECK_TOTAL_AMOUNT')
    }
    this.form.controls.amount.setValue(this.totalAmount)
  }

  handleBenefit(value: IBenefitSelectionItem[]) {
    this.totalAmountBenefit = 0
    value && value.forEach(value => {
      if (!value.money) {
        value.money = 0
      }
      this.totalAmountBenefit += value.money
    })
    if (!this.decisionValue?.isSubmit) {
      this.handleAmount()
    }
  }

  handleProvisionAndOtherAmount(value: CheckProvisionCheckboxValue[]) {
    this.totalAmountProvision = 0
    this.otherAmount = 0
    value && value.forEach(value => {
      if (!value.money) {
        value.money = 0
      }
      if (typeof value.money === 'string' && value.money) {
        if (value.money.startsWith('+')) {
          value.money = Number(value.money?.replace(/,/g, '').split(' ')[0])
        } else if (value.money.startsWith('-')) {
          this.otherAmount = Number(value.money?.replace(/,/g, '').split(' ')[0])
          value.money = Number(value.money?.replace(/,/g, '').split(' ')[0])
        }
      }
      if (value?.code !== 'deduction' && value?.money >= 0) {
        this.totalAmountProvision += Number(value.money)
      }
    })
    this.deductionAmount = Number(this.form.controls.provision?.value?.find(value => {
      return value.code === 'deduction'
    })?.money || 0)
    if (!this.decisionValue?.isSubmit) {
      this.handleAmount()
    }
  }

  ngOnInit() {
    this.loadingService.start();
    const decision = this.detailClaimService.getTypeCommonCategory('DECISION_TYPE');
    const rejectReason = this.detailClaimService.getTypeCommonCategory('REASON_REJECT_DECISION');
    const detailRejectReason = this.detailClaimService.getTypeCommonCategory('DETAIL_REASON_REJECT_DECISION');
    const emailType = this.detailClaimService.getTypeCommonCategory('EMAIL_TYPE')
    forkJoin([decision, rejectReason, detailRejectReason, emailType]).subscribe({
      next: ([decision, rejectReason, detailRejectReason, emailType]) => {
        this.decision = decision;
        this.reasonReject = rejectReason;
        this.detailRejectReason = detailRejectReason
        this.emailTypeList = emailType
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete()
      }
    })
    this.form.controls.benefits.valueChanges.subscribe(
      value => {
        this.handleBenefit(value);
      }
    )
    this.form.controls.provision?.valueChanges.subscribe(
      value => {
        this.handleProvisionAndOtherAmount(value);
      }
    )
  }

  ngOnChanges(changes: SimpleChanges) {
    this.decisionValue = changes['decisionValue']?.currentValue
    if (this.decisionValue?.isSubmit) {
      const benefits = this.decisionValue?.benefitInputs?.replace(/[{}"'\s]/g, '').split(',')
      const lastBenefits: any = []
      benefits?.forEach((value: any) => {
        lastBenefits.push({
          code: value.split(':')[0],
          money: parseInt(value.split(':')[1]),
          checked: true
        })
      })

      const provision = this.decisionValue?.adjustments?.replace(/[{}"'\s]/g, '').split(',')
      const lastProvision: any = []
      provision?.forEach((value: any) => {
        const money = parseInt(value.split(':')[1])
        if (value.split(':')[0] !== 'other') {
          lastProvision.push({
            code: value.split(':')[0],
            money: money,
            checked: true
          })
        } else {
          let moneyString = ''
          if (money > 0) {
            moneyString = '+' + money
          }
          if (money < 0) {
            moneyString = value.split(':')[1]
          }
          lastProvision.push({
            code: value.split(':')[0],
            money: moneyString,
            checked: true
          })
        }
      })
      this.handleBenefit(cloneDeep(lastBenefits));
      this.handleProvisionAndOtherAmount(cloneDeep(lastProvision));
      this.decisionValue.benefits = lastBenefits
      this.decisionValue.provision = lastProvision
      this.decisionValue.code = this.decisionValue.decisionCodeHistory
      this.form.patchValue(this.decisionValue, {emitEvent: false, onlySelf: true})
      this.readonly = true
      this.form.disable()
    }
  }

  // markAllTouch() {
  //   this.form.markAllAsTouched()
  //   this.benefitCheckboxComponent?.markAllAsTouch()
  // }

  // findValue() {
  //   if (this.checkSuspectedFraudItem?.includes('other')) {
  //     this.form.controls.detail.addValidators(Validators.required)
  //     this.form.controls.detail.updateValueAndValidity()
  //     this.form.updateValueAndValidity()
  //   } else {
  //     this.form.controls.detail.removeValidators(Validators.required)
  //     this.form.controls.detail.setValue('')
  //     this.form.controls.detail.updateValueAndValidity()
  //     this.form.updateValueAndValidity()
  //   }
  // }

  submitDecision() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return
    }
    if (this.form.controls.code?.value === 'APPROVE' && (this.form.controls.benefits?.value?.length == 0 || !this.form.controls.benefits?.value)) {
      return this.messageService.warn('MESSAGE.BENEFIT')
    }
    if (this.form.controls.benefits?.value?.length > 0 &&
      this.form.controls.benefits?.value.find(value => value.money == 0)) {
      return this.messageService.warn('MESSAGE.AMOUNT')
    }
    // if (this.form.controls.suspectedFraud?.value && this.checkSuspectedFraudItem?.length == 0) {
    //   return this.messageService.warn('MESSAGE.FRAUD')
    // }
    if (this.form.controls.provision?.value?.length > 0 &&
      this.form.controls.provision?.value.find(value => value.money == 0 || !value.money)) {
      return this.messageService.warn('MESSAGE.AMOUNT')
    }
    if (this.totalAmount < 0) {
      return this.messageService.warn('MESSAGE.CHECK_TOTAL_AMOUNT')
    }
    const data = this.form.value
    data.claimRequestId = this.route.snapshot.paramMap.get(REQUEST_PARAMS.CLAIM_REQUEST_ID)!
    if (data.code === 'APPROVE') {
      const objBenefits: any = {};
      const objAdjustments: any = {};
      data.benefits?.forEach(value => {
        objBenefits[value.code] = value.money
      })
      data.benefitInputs = objBenefits

      data.provision?.forEach(value => {
        if (value.code === 'deduction') {
          objAdjustments[value.code] = -value?.money!
        } else {
          objAdjustments[value.code] = value.money
        }
      })
      data.adjustments = objAdjustments
    }

    // data.checkSuspectedFraudItem = this.checkSuspectedFraudItem
    this.loadingService.start()
    this.detailClaimService.sendDecision(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS')
        this.loadingService.complete()
        this.form.disable()
        this.readonly = true
        this.reCallDetail.emit()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }

  // checkValueSuspected() {
  //   if (this.form.controls.suspectedFraud.value === false) {
  //     this.form.controls.detail.removeValidators(Validators.required)
  //     this.form.controls.detail.updateValueAndValidity()
  //     this.form.controls.detail.setValue('')
  //     this.checkSuspectedFraudItem = []
  //   }
  // }

  checkDecisionValue($event?: any) {
    if ($event?.value == 'REJECT' || $event?.value == 'APPROVE') {
      if (this.task?.find(value => {
        return value?.status !== 'CLOSE' && value?.status !== 'CANCEL'
      })) {
        this.form.controls.code?.setValue(null)
        return this.messageService.warn('MESSAGE.CHECK_CLOSE_TASK')
      }
    }
    this.form.reset({
      code: this.form.controls.code?.value!,
      // suspectedFraud: this.form.controls.suspectedFraud?.value!
    })
    switch ($event?.value) {
      case 'PENDING':
        this.disableControl()
        this.form.controls.note.addValidators(Validators.required)
        this.form.controls.note.enable()
        this.form.updateValueAndValidity()
        break;
      case 'REJECT':
        this.disableControl()

        this.form.controls.reason.enable()
        this.form.updateValueAndValidity()
        break;
      case 'APPROVE':
        this.disableControl()

        this.form.controls.note.removeValidators(Validators.required)
        this.form.controls.benefits.enable()
        this.form.controls.note.enable()
        this.form.controls.provision?.enable()
        this.form.controls.amount.enable()
        this.form.controls.sendMail.enable()
        this.form.controls.paymentException.enable()
        break;
    }
  }

  checkReasonRejectValue($event: any) {
    switch ($event.value) {
      case '0':
        this.form.controls.reasonNote.disable()
        this.form.controls.reasonOther.disable()
        this.form.updateValueAndValidity()
        break;
      case '1':
        this.form.controls.reasonNote.enable()
        this.form.updateValueAndValidity()
        break;
      case '2':
        this.form.controls.reasonNote.enable()
        this.form.updateValueAndValidity()
        break;
    }
  }

  checkDetailRejectValue($event: any) {
    switch ($event?.value) {
      case 'OTHER_EXCLUSION':
        this.form.controls.reasonOther.enable()
        this.form.updateValueAndValidity()
        break;
      default:
        this.form.controls.reasonOther.disable()
        this.form.updateValueAndValidity()
    }
  }

  disableControl() {
    this.form.controls.reason.disable()
    this.form.controls.note.disable()
    this.form.controls.benefits.disable()
    this.form.controls.reasonNote.disable()
    this.form.controls.reasonOther.disable()
    this.form.controls.provision?.disable()
    this.form.controls.amount.disable()
    this.form.controls.sendMail.disable()
    this.form.controls.paymentException.disable()
  }

  openSendMailPopup() {
    if (this.form.controls.sendMail.value && !this.isSaveMail) {
      this.dialogService?.open(SendMailTemplateComponent, {
        showHeader: false,
        width: '40%',
        data: {
          screenType: ScreenType.Create,
          claimRequestId: this.claimRequestId,
          policyNumber: this.policyNumber,
          laName: this.laName
        },
      }).onClose.subscribe(result => {
        if (result) {
          this.reCallDetail.emit()
        }
      });
    }
    if (this.form.controls.sendMail.value && this.isSaveMail) {
      this.loadingService.start()
      this.detailClaimService.getDataEmailSaved(this.claimRequestId).subscribe({
        next: (res: any) => {
          this.loadingService.complete()
          this.dialogService?.open(EmailTemplateComponent, {
            header: res?.data?.emailType,
            showHeader: false,
            width: '45%',
            data: {
              dataEmail: {
                ...res?.data,
                isSaved: true,
                claimRequestId: this.claimRequestId,
                policyNumber: this.policyNumber,
                laName: this.laName
              }
            },
          }).onClose.subscribe(result => {
            if (result) {
              this.reCallDetail.emit()
            }
          });
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err))
          this.loadingService.complete()
        }
      })
    }
  }

  viewAttachment(id: string) {
    this.loadingService.start();
    this.detailClaimService.openEmail(id).subscribe({
      next: (data: any) => {
        openFile(data, true);
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    });
  }
}
