import { ComponentFixture, TestBed } from "@angular/core/testing";

import { ClaimTpaDecisionComponent } from "./claim-tpa-decision.component";

describe('ClaimDecisionComponent', () => {
  let component: ClaimTpaDecisionComponent;
  let fixture: ComponentFixture<ClaimTpaDecisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClaimTpaDecisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimTpaDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
