import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import {
  IClaimCaseBenefitEntity,
  IClaimDetailResponse,
  IClaimHistoryItem, IIcDTO
} from "@claim-detail/interfaces/claim-tpa-response.interface";
import { DetailClaimService } from "@claim-detail/services/detail-claim.service";
import { forkJoin, take } from "rxjs";
import { IDropdownItemWithTranslate } from "@detail-request/shared/models/dropdown-with-translate.model";
import { TranslateService } from "@ngx-translate/core";
import { DataTable } from "@cores/models/data-table.model";
import { IBenefitValue } from "@claim-detail/interfaces/claim-tpa.interface";
import { LazyLoadEvent } from "primeng/api";
import { removeZeroHeadOfPolicyNumber } from "@claim-detail/utils/functions";
import { CommonClaimCategory } from "@cores/models/common-claim.model";
import * as moment from "moment/moment";

type ResultUnderwritingAndReUnderwriting = {
  status: string,
  reason: string,
  user: string,
  executeDate: string,
  executeTime: string
}

type ClaimInformationOverview = {
  claimId: string
  eventDatefrom: string
  laName: string
  eventDateto: string
  status: string
  submitFirstRequest: string
  createDate: string
  fullSubmissionDate: string
  savedBenefits: IBenefitValue[]
  sicknessName: string
  facilityName: string
  paymentMethod: string
  bankName: string
  branchName: string
  accNumber: string
  accName: string
  cashType: string
  cashNumber: string
  cashDate: string
  cashAdd: string
  email: string
  phone: string,
  claimRequestId: string
}

type PolicyInformation = {
  policyNumber: string
  policyStatus: string
  productName: string
  // Số ngày hiện tại - ngày bắt đầu hợp đồng
  durationContract: number
  startDate: string
  paidToDate: string
  resetDate: string
  laspeDate?: string
}



@Component( {
  selector: "app-claim-overview",
  templateUrl: "./claim-overview.component.html",
  styleUrls: [ "./claim-overview.component.scss" ]
} )
export class ClaimOverviewComponent implements OnChanges {
  public readonly removeZero = removeZeroHeadOfPolicyNumber;
  public readonly PAYMENT_METHOD_BANKING = 'BANKING'
  benefitsCommon: IBenefitValue[] = [];
  sicknessCommon: CommonClaimCategory[] = [];
  facilityCommon: CommonClaimCategory[] = [];
  claimStatusCommon: CommonClaimCategory[] = [];
  commonIdentificationTypeCode: IDropdownItemWithTranslate<string>[] = [];
  resultUnderwritingOrReUnderwriting?: ResultUnderwritingAndReUnderwriting[] = []
  listPaymentMethod: IDropdownItemWithTranslate<string>[] = []

  claimHistoryTable: DataTable<IClaimHistoryItem> = {
    totalPages: 0,
    totalElements: 0,
    currentPage: 0,
    size: 10,
    first: 0,
    numberOfElements: 0,
    content: []
  };

  icDTO?: IIcDTO['current']
  claimInformationOverview?: ClaimInformationOverview
  policyInformation?: PolicyInformation
  businessPartnerHolder?: IClaimDetailResponse['businessPartnerHolder']
  businessPartnerLA?: IClaimDetailResponse['businessPartnerLA']
  resultReUnderwritingUnderwriting: ResultUnderwritingAndReUnderwriting[] = []


  @Input() detailClaimResponse?: IClaimDetailResponse;

  constructor(
    public claimService: DetailClaimService,
    public translateService: TranslateService
  ) {
    forkJoin(
      [
        claimService.benefits$.pipe( take( 1 ) ),
        claimService.sickness$.pipe( take( 1 ) ),
        claimService.facility$.pipe( take( 1 ) ),
        claimService.claimStatusCommon$.pipe( take( 1 ) )
      ]
    ).subscribe(
      ( [ benefits, sickness, facility, claimStatusCommon ] ) => {
        this.benefitsCommon = benefits.map( item => {
          return {
            ...item,
            checked: false
          };
        } );
        this.sicknessCommon = sickness;
        this.facilityCommon = facility;
        this.claimStatusCommon = claimStatusCommon
      }
    );

    this.claimService.getCommonModel().subscribe(
      ( response ) => {
        this.commonIdentificationTypeCode = response.map(
          ( item ) => {
            return {
              value: item.code!, multiLanguage: JSON.parse( item.description ?? "{}" )
            };
          } );
      } );

    this.claimService.getPaymentMethod().subscribe(
      (response) => {
        this.listPaymentMethod = response.map(
          (item) => {
            return {
              value: item.code!, multiLanguage: JSON.parse(item.description ?? "{}")
            };
          });
      });
  }

  ngOnChanges( changes: SimpleChanges ) {
    if ( changes[ "detailClaimResponse" ] && !!changes[ "detailClaimResponse" ].currentValue ) {
      const detailClaim = this.detailClaimResponse!

      this.constructOverviewClaimInfo(detailClaim)
      this.constructContractInfo(
        detailClaim.allByRequestId,
        detailClaim.policyInfoDto,
        detailClaim.policyDate,
        detailClaim?.policyLapseResponse
      )
      this.#constructResultUnderwritingReUnderwriting(
      detailClaim.policyInfoDto.underWritings
      )
      this.icDTO = detailClaim?.icDto?.current
      this.businessPartnerHolder = detailClaim?.businessPartnerHolder;
      this.businessPartnerLA = detailClaim?.businessPartnerLA;

      this.getHistory()
    }
  }

  getHistory() {
    if ( !this.businessPartnerLA?.bpNumber ) {
      return
    }
    const bpId = this.businessPartnerLA.bpNumber
    const { currentPage, size } = this.claimHistoryTable
    this.claimService.getHistoryClaim( bpId, currentPage, size ).subscribe(
      {
        next: ( response ) => {
          this.claimHistoryTable = response
        }
      } );
  }

  pageChange( lazyLoadEvent: LazyLoadEvent ) {
    this.claimHistoryTable.currentPage = lazyLoadEvent?.first! / lazyLoadEvent?.rows!;
    this.claimHistoryTable.size = lazyLoadEvent.rows!;
    this.claimHistoryTable.first = lazyLoadEvent.first!;
    this.getHistory()
  }

  constructOverviewClaimInfo( detailClaim: IClaimDetailResponse) {
    const savedBenefits = ( detailClaim?.claimCaseBenefitEntities ?? [] )?.map(
      ( value: IClaimCaseBenefitEntity ) => {
        return <IBenefitValue>{
          code: value.benefitCode
        };
      } )

    this.claimInformationOverview = {
      accName: detailClaim.allByRequestId.accName,
      accNumber: detailClaim.allByRequestId.accNumber,
      bankName: detailClaim.allByRequestId.bankName,
      branchName: detailClaim.allByRequestId.branchName,
      cashAdd: detailClaim.allByRequestId.cashAdd,
      cashDate: detailClaim.allByRequestId.cashDate,
      cashNumber: detailClaim.allByRequestId.cashNumber,
      cashType: detailClaim.allByRequestId.cashType,
      claimId: detailClaim.allByRequestId.claimId,
      createDate: detailClaim.allByRequestId.createDate,
      email: detailClaim.allByRequestId.email,
      eventDatefrom: detailClaim.allByRequestId.eventDatefrom,
      eventDateto: detailClaim.allByRequestId.eventDateto,
      facilityName: detailClaim.facilityEntities?.[ 0 ]?.facilityName,
      fullSubmissionDate: detailClaim.allByRequestId.fullSubmissionDate,
      laName: detailClaim.allByRequestId.laName,
      paymentMethod: detailClaim.allByRequestId.paymentMethod,
      phone: detailClaim.allByRequestId.phone,
      savedBenefits,
      sicknessName: detailClaim.claimCaseSicknessEntities?.[ 0 ].sicknessName,
      status: detailClaim.allByRequestId.status,
      submitFirstRequest: detailClaim.allByRequestId.submitFirstRequest,
      claimRequestId: detailClaim.allByRequestId.claimRequestId
    }
  }

  private constructContractInfo(
    allByRequestId: IClaimDetailResponse['allByRequestId'],
    policyInfoDTO: IClaimDetailResponse['policyInfoDto'],
    policyDate: IClaimDetailResponse['policyDate'],
    policyLapseResponse: IClaimDetailResponse['policyLapseResponse']
  ) {
    const durationContract = moment().diff( moment(policyInfoDTO.coverages?.[0]?.startDate), 'day')
    this.policyInformation = {
      durationContract,
      laspeDate: policyLapseResponse?.laspeDateConverted,
      paidToDate: policyDate?.paidToDate,
      policyNumber: allByRequestId?.policy,
      policyStatus: policyInfoDTO?.coverages?.[0]?.status,
      productName: policyInfoDTO?.coverages?.[0]?.coverageName,
      resetDate: policyDate?.resetDate,
      startDate: policyDate?.startDate
    }
  }

  #constructResultUnderwritingReUnderwriting(
    results: IClaimDetailResponse['policyInfoDto']['underWritings']
  ) {
    this.resultUnderwritingOrReUnderwriting = results.map(
      ( result ) => {
        const rs: ResultUnderwritingAndReUnderwriting = {
          status: result.statusName,
          reason: result.reasonName,
          user: result.createdBy,
          executeDate: result.statusDate,
          executeTime: result.statusTime
        }
        return rs
      }
    )
  }
}
