import {
  BenefitCheckboxComponent
} from "./benefit-checkbox/benefit-checkbox.component";
import {
  AdjustContractCheckboxComponent
} from "./adjust-contract-checkbox/adjust-contract-checkbox.component";
import {ClaimTpaDecisionComponent} from "./claim-tpa-decision/claim-tpa-decision.component";
import {ClaimCreateTaskComponent} from "./claim-create-task/claim-create-task.component";
import {ClaimSectionTemplateComponent} from "./claim-section-template/claim-section-template.component";
import {ClaimOverviewComponent} from "./claim-overview/claim-overview.component";
import {ClaimProcessRequestComponent} from "./claim-process-request/claim-process-request.component";
import {
  ClaimNeedToWorkUnderwritingComponent
} from "./claim-need-to-work-underwriting/claim-need-to-work-underwriting.component";
import {ValueDisplayComponent} from "./value-display/value-display.component";
import {CustomInputNumberComponent} from "./custom-input-number/custom-input-number.component";
import {ClaimDetailWrapComponent} from "./claim-detail-wrap/claim-detail-wrap.component";
import {
  ClaimInvestigationDecisionComponent
} from "./claim-investigator-decision/claim-investigation-decision.component";
import {ClaimUnderwritingDecisionComponent} from "./claim-underwriting-decision/claim-underwriting-decision.component";
import {TaskInfoViewComponent} from "./task-info-view/task-info-view.component";
import {TaskTemplateComponent} from "@claim-detail/components/task-template/task-template.component";
import {ClaimAttachmentComponent} from "./claim-attachment/claim-attachment.component";
import {
  ClaimCorrespondenceComponent
} from "@claim-detail/components/claim-correspondence/claim-correspondence.component";
import {CorrespondenceViewComponent} from "@claim-detail/components/correspondence-view/correspondence-view.component";

export const COMPONENTS = [
  ClaimTpaDecisionComponent,
  BenefitCheckboxComponent,
  ClaimCreateTaskComponent,
  AdjustContractCheckboxComponent,
  ClaimSectionTemplateComponent,
  ClaimOverviewComponent,
  ClaimProcessRequestComponent,
  ClaimNeedToWorkUnderwritingComponent,
  ValueDisplayComponent,
  CustomInputNumberComponent,
  ClaimDetailWrapComponent,
  ClaimInvestigationDecisionComponent,
  ClaimUnderwritingDecisionComponent,
  TaskInfoViewComponent,
  TaskTemplateComponent,
  ClaimAttachmentComponent,
  ClaimCorrespondenceComponent,
  CorrespondenceViewComponent
];

export * from "@claim-detail/components/benefit-checkbox/benefit-checkbox.component";
export * from "@claim-detail/components/adjust-contract-checkbox/adjust-contract-checkbox.component";
export * from "@claim-detail/components/claim-tpa-decision/claim-tpa-decision.component";
export * from "@claim-detail/components/claim-create-task/claim-create-task.component";
export * from "@claim-detail/components/claim-section-template/claim-section-template.component";
export * from "@claim-detail/components/claim-overview/claim-overview.component";
export * from "@claim-detail/components/claim-process-request/claim-process-request.component";
export * from "@claim-detail/components/claim-need-to-work-underwriting/claim-need-to-work-underwriting.component";
export * from "@claim-detail/components/value-display/value-display.component";
export * from "@claim-detail/components/custom-input-number/custom-input-number.component";
export * from "@claim-detail/components/claim-detail-wrap/claim-detail-wrap.component";
export * from "@claim-detail/components/claim-investigator-decision/claim-investigation-decision.component";
export * from "@claim-detail/components/claim-underwriting-decision/claim-underwriting-decision.component";
export * from "@claim-detail/components/task-info-view/task-info-view.component";
export * from "@claim-detail/components/task-template/task-template.component";
export * from "@claim-detail/components/claim-attachment/claim-attachment.component";

