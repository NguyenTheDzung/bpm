import {
  ChangeDetectorRef,
  Component, ElementRef,
  forwardRef,
  Input,
  OnChanges,
  QueryList,
  SimpleChanges,
  ViewChildren
} from "@angular/core";
import { IBenefitSelectionItem } from "@claim-detail/interfaces/claim-tpa.interface";
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgModel,
  ValidationErrors,
  Validator,
  Validators
} from "@angular/forms";
import { IFormControlModel } from "@cores/models/form.model";
import { cloneDeep } from "lodash";

interface IBenefitCheckboxControl extends IFormControlModel<string[]>{}

@Component({
  selector: "app-benefit-checkbox",
  templateUrl: 'benefit-checkbox.component.html',
  styleUrls: ["./benefit-checkbox.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BenefitCheckboxComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => BenefitCheckboxComponent),
      multi: true
    },
  ]
})
export class BenefitCheckboxComponent implements  ControlValueAccessor, Validator, OnChanges {
  @ViewChildren( NgModel ) ngModels?: QueryList<NgModel>
  onChangeFn: Function = () => {
  };
  onTouchFn: Function = () => {
  };

  @Input() readonly = false;
  @Input() currency: string = " VND";
  @Input() showMoney = false;
  @Input() control?: AbstractControl;

  @Input() codeOptions: string = "code"
  @Input() moneyOptions: string = "money"

  checkBoxControl: IBenefitCheckboxControl = new FormControl( [ [] ] )
  private _selectValues: IBenefitSelectionItem[] = []

  touch: boolean = false;
  private _benefits : any[] = [];

  @Input() set benefits(value: typeof this._benefits) {
     this._benefits = cloneDeep(value)
  }
  get benefits() {
    return this._benefits
  }


  constructor(
    private ref: ChangeDetectorRef,
    private elementRef: ElementRef
  ) {
  }

  ngOnChanges( changes: SimpleChanges ) {
    if ( changes[ 'benefits' ] && changes[ 'benefits' ].currentValue.length ) {
        this.benefits = [...this.benefits].concat([])
        this._mapSelectionAndBenefits()
        this.onChange()
    }
  }

  onChange(): void {
    this.onChangeFn( this.mapOnChangeValue() );
  }

  mapOnChangeValue(): IBenefitSelectionItem[] {
    return ( this.checkBoxControl.value ?? [] ).map(
      code => {
        const item = this.benefits.find( value => value[this.codeOptions] == code );
        if ( item ) {
          return {
            ...item,
            [this.codeOptions]: item.code,
            [this.moneyOptions]: item?.money
          } as IBenefitSelectionItem
        }
        return { code }
      }
    )
  }

  setControl( control: AbstractControl ) {
    if ( this.hasBeenTouched( control ) ) {
      this.markAllAsTouch();
      this.detectChanges()
    }

    if ( this.hasRequiredValidator( control ) && !this.checkBoxControlHasRequired() ) {
      this.checkBoxControl.addValidators( Validators.required )
      this.checkBoxControl.updateValueAndValidity()
    }

    setTimeout( () => this.control = control )
  }

  checkBoxControlHasRequired( control: AbstractControl = this.checkBoxControl ) {
    return control.hasValidator( Validators.required )
  }

  writeValue( checkedList: IBenefitSelectionItem[] ): void {
    if ( !Array.isArray( checkedList ) ) return;
    this._selectValues = checkedList.concat([])
    this._mapSelectionAndBenefits()
  }

  registerOnChange( fn: Function ): void {
    this.onChangeFn = fn;
  }

  registerOnTouched( fn: any ): void {
    this.onTouchFn = fn;
  }

  validate( control: AbstractControl ): ValidationErrors | null {
    if ( !control.value ) {
      return null;
    }

    control && this.setControl( control )

    if (
      ( this.hasRequiredValidator( control ) &&
        !this.hasCheckedItem( control ) ) ||
      this.checkBoxControlHasErrorRequired( control )
      // this.hasInvalidMoneyField()
    ) {
      return {
        required: true
      };
    }

    return null;
  }

  hasCheckedItem( control: AbstractControl ) {
    return !!( control.value as IBenefitSelectionItem[] ).length
  }

  hasRequiredValidator( control = this.control ) {
    return control?.hasValidator( Validators.required ) ?? false;
  }

  hasBeenTouched( control = this.control ) {
    return !!control?.touched
  }

  checkBoxControlHasErrorRequired( control: AbstractControl = this.checkBoxControl ) {
    return !!control.errors?.[ 'required' ]
  }

  markAllAsTouch() {
    this.onTouchFn();
    this.ngModels?.forEach( value => value.control.markAllAsTouched() )
    this.checkBoxControl.markAllAsTouched()
  }

  // hasInvalidMoneyField() {
  //   return !!this.ngModels?.find(value => value.control.invalid)
  // }

  onChangeCheckbox(index: number) {
    this.benefits[index].checked = !this.benefits[index]?.checked
    if (!this.benefits[index].checked) {
      this.benefits[index].money = undefined
    }
    this.onChange();
    this.markAllAsTouch()
  }

  detectChanges() {
    this.ref.detectChanges()
  }

  private _mapSelectionAndBenefits() {
    if(!this._selectValues.length || !this.benefits.length) {
      return
    }

    let controlValue: any[] = []

    this._selectValues.forEach(
      selectValue => {
        controlValue.push(selectValue[this.codeOptions])
        const item = this.benefits.find(benefit => benefit.code == selectValue[this.codeOptions]);
        if (item) {
          item.money = selectValue[this.moneyOptions]!;
          item.checked = true
        }
      }
    )

    this.checkBoxControl.setValue(controlValue)
  }
}
