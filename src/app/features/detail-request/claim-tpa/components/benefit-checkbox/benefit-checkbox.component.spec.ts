import { ComponentFixture, TestBed } from "@angular/core/testing";

import { BenefitCheckboxComponent } from "./benefit-checkbox.component";
import { Component } from "@angular/core";
import { IBenefitValue } from "@claim-detail/interfaces/claim-tpa.interface";
import { By } from "@angular/platform-browser";
import { FormControl, FormsModule, Validators } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";

@Component(
  {
    template: `
      <app-benefit-checkbox
        [required]="true"
        [formControl]="checkBox"
        [benefits]="benefits"
        [showMoney]="false"
        [readonly]="readonly">
      </app-benefit-checkbox>
    `
  }
)
class BenefitCheckboxTestingComponent {
  checkBox = new FormControl([], {validators: Validators.required})
  readonly = false
  benefits: IBenefitValue[] = [
    {
      "id": 1,
      "code": "01",
      "name": "Nằm viện thường"
    },
    {
      "id": 2,
      "code": "02",
      "name": "Phẫu Thuật"
    },
    {
      "id": 3,
      "code": "03",
      "name": "Nằm viện tại khoa hồi sức tích cực"
    },
    {
      "id": 4,
      "code": "04",
      "name": "Nằm viện do mắc bệnh hiểm nghèo ( Ung thư, tai biến , nhồi máu cơ tim)"
    }
  ];

}

describe("BenefitCheckboxComponent", () => {
  let component: BenefitCheckboxTestingComponent;
  let fixture: ComponentFixture<BenefitCheckboxTestingComponent>;
  let benefitComponent: BenefitCheckboxComponent

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BenefitCheckboxTestingComponent, BenefitCheckboxComponent ],
      imports: [SharedModule, FormsModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitCheckboxTestingComponent);

    component = fixture.componentInstance;
    benefitComponent = fixture.debugElement.children[0].componentInstance
    fixture.detectChanges();
  });

  it("Tạo thành công", () => {
    expect(component).toBeTruthy();
    expect(benefitComponent).toBeTruthy();
  });

  it("Hiển thị danh sách quyền lợi", () => {
    expect(component.benefits.length).toBe(4)
    expect(benefitComponent.benefits.length).withContext(`Truyền vào sai benefits có ${benefitComponent.benefits.length} phần tử`).toBe(4)
    const listCheckbox  = fixture.debugElement.queryAll(By.css('p-checkbox'))
    expect(listCheckbox.length).withContext("Hiển thị trong DOM không đúng với truyền vào").toBe(4)
  })

  it("Click một lần để checked", () => {
    const boxEl = fixture.nativeElement.querySelector('.p-checkbox-box');
    boxEl.click();
    fixture.detectChanges();

    const input = fixture.nativeElement.querySelector('input');
    expect(input.checked).toBe(true);
    expect(benefitComponent.checkBoxControl.value.length).toBe(1)
    expect(component.checkBox.value.length).toBe(1)
  })

  it('Thực hiện readonly đối với các element không cần validate', () => {
    component.readonly = true
    fixture.detectChanges()
    const inputs: any[] = Array.from(fixture.nativeElement.querySelectorAll('input'));
    expect(inputs.every(value => value.readOnly)).toBeTrue()
  })

  it('Validate bắt buộc chọn một trong các checkbox', () => {
    expect(component.checkBox).toBeTruthy()
    component.checkBox?.markAsTouched()
    fixture.detectChanges()

    let checkboxEls: HTMLElement[] = Array.from(fixture.nativeElement.querySelectorAll('p-checkbox'));
    checkboxEls.forEach(
      val => {
        expect(val.className).toContain('ng-invalid');
      }
    );

    const boxEl = fixture.nativeElement.querySelector('.p-checkbox-box')
    boxEl.click()
    fixture.detectChanges()
    benefitComponent.detectChanges()
    expect(component.checkBox.value.length).toBe(1)

    checkboxEls.forEach(
      (val, index) => {
        expect(val.className).withContext("Invalid phần tử thứ " + (index + 1)).toContain('ng-valid')
      }
    )
  })

  it('Click 2 lần để uncheck ', () => {
    const boxEl = fixture.nativeElement.querySelector('.p-checkbox-box');
    boxEl.click();
    fixture.detectChanges();

    const input = fixture.nativeElement.querySelector('input');
    expect(input.checked).toBe(true);
    expect(benefitComponent.checkBoxControl.value.length).toBe(1)
    expect(component.checkBox.value.length).toBe(1)

    boxEl.click();
    fixture.detectChanges();
    expect(input.checked).toBe(false);
    expect(benefitComponent.checkBoxControl.value.length).toBe(0)
    expect(component.checkBox.value.length).toBe(0)
  })
});
