import { ComponentFixture, TestBed } from "@angular/core/testing";

import { AdjustContractCheckboxComponent } from "./adjust-contract-checkbox.component";

describe('AdjustContractCheckboxComponent', () => {
  let component: AdjustContractCheckboxComponent;
  let fixture: ComponentFixture<AdjustContractCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdjustContractCheckboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustContractCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
