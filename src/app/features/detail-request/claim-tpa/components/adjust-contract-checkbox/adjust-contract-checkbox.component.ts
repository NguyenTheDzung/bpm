import {ChangeDetectorRef, Component, forwardRef, Input, OnChanges, SimpleChanges} from "@angular/core";
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from "@angular/forms";
import {CheckProvisionCheckboxValue} from "@claim-detail/interfaces/claim-tpa.interface";
import {IFormControlModel} from "@cores/models/form.model";

interface IProvisionCheckboxControl extends IFormControlModel<string[]> {
}

@Component({
  selector: "app-adjust-contract-checkbox",
  templateUrl: "./adjust-contract-checkbox.component.html",
  styleUrls: ["./adjust-contract-checkbox.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AdjustContractCheckboxComponent),
      multi: true
    }
  ]
})
export class AdjustContractCheckboxComponent implements ControlValueAccessor, OnChanges {
    onChangeFn: Function = () => {
    };
    onTouchFn: Function = () => {
    };
    currency: string = ' VND'
    pattern = '(^[-+][1-9])(?:[0-9,]+|)\\sVND'
    @Input() readonly = false;
    @Input() provision: CheckProvisionCheckboxValue[] = [
        {
            code: 'deduction',
            checked: false,
            money: undefined
        },
        {
            code: 'refund',
            checked: false,
            money: undefined
        },
        {
            code: 'other',
            checked: false,
            money: ''
        }
    ];
    checkBoxControl: IProvisionCheckboxControl = new FormControl([])

  constructor(
    private ref: ChangeDetectorRef,
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['provision']) {
      const provision = changes['provision'].currentValue as CheckProvisionCheckboxValue[]
      if (!!provision.length) {
        const checkedItem = provision.filter(item => item.checked)
        this.checkBoxControl.setValue(this.mapSelectionOnWrite(checkedItem))
        this.onChange()
      }
    }
  }

  onChange(): void {
    this.onChangeFn(this.mapOnChangeValue());
  }

  mapOnChangeValue(): CheckProvisionCheckboxValue[] {
    return (this.checkBoxControl.value ?? []).map(
      code => {
        const item = this.provision.find(value => value.code == code);
        if (item) {
          if (item.code === 'other' && !item.money?.toString().match(this.pattern)) {
            item.money = ''
          }
          return {code: item.code, money: item?.money} as CheckProvisionCheckboxValue
        }
        return {code} as CheckProvisionCheckboxValue
      }
    )
  }

  mapSelectionOnWrite(checked: CheckProvisionCheckboxValue[]) {
    return checked.map(({money, code}) => {
        const item = this.provision.find(provision => provision.code == code);
        if (item) {
          item.money = money;
          item.checked = true
        }
        return code
      }
    );
  }

  writeValue(checkedList: CheckProvisionCheckboxValue[]): void {
    if (!Array.isArray(checkedList)) return;
    const mappedList = this.mapSelectionOnWrite(checkedList)
    this.checkBoxControl.setValue(mappedList)
  }

  registerOnChange(fn: Function): void {
    this.onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchFn = fn;
  }

  onChangeCheckbox(index: number) {
    this.provision[index].checked = !this.provision[index]?.checked
    if (!this.provision[index].checked) {
      this.provision[index].money = undefined
    }
    this.onChange();
  }

    detectChanges() {
        this.ref.detectChanges()
    }
}
