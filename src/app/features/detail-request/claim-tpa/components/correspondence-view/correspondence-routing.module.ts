import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {FunctionCode} from "@cores/utils/enums";
import {ClaimTpaViewComponent} from "@claim-detail/pages";
import {CorrespondenceViewComponent} from "@claim-detail/components/correspondence-view/correspondence-view.component";

const routes: Routes = [
  {
    path: '',
    component: ClaimTpaViewComponent,
  },
  {
    path: 'correspondence',
    component: CorrespondenceViewComponent,
    data: {
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.Correspondence}`,
    }
  },
  {path: '**', redirectTo: 'notfound', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorrespondenceRoutingModule {
}
