import {Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {BpmClaimService} from "@claim-detail/services";
import {createErrorMessage, exportFile} from "@cores/utils/functions";

@Component({
  selector: 'app-correspondence-view',
  templateUrl: './correspondence-view.component.html',
  styleUrls: ['./correspondence-view.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CorrespondenceViewComponent extends BaseActionComponent implements OnInit {

  stateUrl: any

  constructor(inject: Injector, private bpmClaimService: BpmClaimService) {
    super(inject, bpmClaimService);
    this.stateUrl = this.router.getCurrentNavigation()?.extras?.state;
  }

  ngOnInit(): void {
    if (!this.stateUrl) {
      this.location.back()
    }
  }

  exportAttachment(id: number) {
    this.loadingService.start()
    this.bpmClaimService.exportFile(id).subscribe({
      next: (res: any) => {
        exportFile(res?.data?.attachmentName, res?.data?.mainDocument, res?.data?.fileType)
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  override cancel() {
    this.location.back()
  }
}
