import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailRequestSharedModule} from '@detail-request/shared/detail-request-shared.module';
import {SERVICES} from '@claim-detail/services';
import {SharedModule} from '@shared/shared.module';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {CorrespondenceRoutingModule} from "@claim-detail/components/correspondence-view/correspondence-routing.module";

@NgModule({
  declarations: [],
  providers: [...SERVICES],
  imports: [CommonModule, DetailRequestSharedModule, SharedModule, CKEditorModule, CorrespondenceRoutingModule],
})
export class CorrespondenceModule {
}
