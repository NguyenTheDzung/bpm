import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren
} from "@angular/core";
import {MAX_FILE_SIZE} from "@cores/utils/constants";
import {NgModel} from "@angular/forms";
import {CommonModel} from "@common-category/models/common-category.model";
import {ClaimUnderWritingDecisionValue, FileUploadPreModel} from "@claim-detail/interfaces/claim-tpa.interface";
import {convertFileToBase64} from "@claim-detail/utils/functions";
import {ZCOL_PR} from "@create-requests/utils/constants";
import {FileUpload} from "primeng/fileupload";
import {BaseComponent} from "@shared/components";


@Component({
  selector: "app-claim-underwriting-decision",
  templateUrl: "./claim-underwriting-decision.component.html",
  styleUrls: ["./claim-underwriting-decision.component.scss"]
})
export class ClaimUnderwritingDecisionComponent extends BaseComponent implements OnChanges{

  @ViewChildren(NgModel) ngModels?: QueryList<NgModel>;
  @ViewChild(FileUpload) fileUploadComponent?: FileUpload;

  readonly MAX_FILE_SIZE = MAX_FILE_SIZE;
  readonly MAX_FILE_LIMIT = 10;

  @Output() decisionValueChange = new EventEmitter<ClaimUnderWritingDecisionValue>();
  @Input() decisions: CommonModel[] = [];

  @Input() decisionValue?: ClaimUnderWritingDecisionValue
  @Output() onClickForward = new EventEmitter<Event>()

  constructor(
    injector: Injector
  ) {
    super(injector);
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes['decisionValue'] && !!changes['decisionValue'].currentValue){
      const value = changes['decisionValue'].currentValue
      this.decision = value.decision;
      this.content = value.content;
      this.savedFiles = value.savedFiles;
      this.fileLimit = this.checkFileLimit();
      this.files = value.files;

      if (this.fileUploadComponent) {
        this.fileUploadComponent._files = value.files.map((item: any) => {
            return item.file;
          }
        );
      }

    }
  }


  fileLimit = this.MAX_FILE_LIMIT;

  decision?: ClaimUnderWritingDecisionValue["decision"];
  content: ClaimUnderWritingDecisionValue["content"] = "";
  files: ClaimUnderWritingDecisionValue["files"] = [];
  savedFiles: ClaimUnderWritingDecisionValue["savedFiles"] = [];

  async fileHandler($event: { files: File[] }) {
    this.files = await Promise.all($event?.files?.map(async (file, index) => {
      const data: FileUploadPreModel = {
        name: file.name,
        type: ZCOL_PR,
        extension: file.name.substring(file.name.lastIndexOf(".")).toLowerCase(),
        content: await convertFileToBase64(file),
        mimeType: file.type,
        size: file.size + "",
        file: file
      };
      return data;
    }));
    this.onChange();
  }

  onChange() {
    const newValue: ClaimUnderWritingDecisionValue = {
      files: this.files,
      content: this.content,
      decision: this.decision!,
      savedFiles: this.savedFiles
    };
    this.decisionValueChange.emit(newValue);
  }

  isValid() {
    this.ngModels?.forEach(item => item.control.markAllAsTouched());
    return !this.ngModels?.find(item => item.control.invalid);
  }

  removeFile($event: MouseEvent, i: number) {
    $event.preventDefault();
    this.savedFiles.splice(i, 1);
    this.onChange();
  }

  checkFileLimit() {
    const limit = this.MAX_FILE_LIMIT - this.savedFiles.length;
    if (limit > 0) return limit;
    return 0;
  }

  clean(){
      if(!this.fileUploadComponent) return
      this.fileUploadComponent.files = []
      this.fileUploadComponent._files = []
      this.fileUploadComponent.clear()
  }

  forward(event: any) {
    this.onClickForward.emit(event)
  }
}
