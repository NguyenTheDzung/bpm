import {Component, Injector, Input, OnInit} from '@angular/core';
import {DetailClaimService} from "@claim-detail/services";
import {BaseActionComponent} from "@shared/components";
import {createErrorMessage} from "@cores/utils/functions";
import {EMAIL_VIEW} from "@claim-detail/interfaces/send-mail.interface";

@Component({
  selector: 'app-claim-correspondence',
  templateUrl: './claim-correspondence.component.html',
  styleUrls: ['./claim-correspondence.component.scss']
})
export class ClaimCorrespondenceComponent extends BaseActionComponent implements OnInit {

  constructor(inject: Injector, private service: DetailClaimService) {
    super(inject, service);
  }

  @Input() claimRequestId: string = ''
  override data!: EMAIL_VIEW[]

  ngOnInit(): void {
    this.loadingService.start()
    this.service.getCorrespondence(this.claimRequestId || this.route.snapshot.paramMap.get('claimRequestId')!).subscribe({
      next: (res: any) => {
        this.data = res?.data
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }

  doDetail(item: EMAIL_VIEW) {
    this.router.navigate([`/bpm/requests/detail/claim/tpa/${this.route.snapshot.paramMap.get('claimRequestId')!}/correspondence`], {
      state: item
    })
      .then()
  }
}
