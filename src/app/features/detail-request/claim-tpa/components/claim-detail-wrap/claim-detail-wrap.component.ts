import {
  AfterViewInit,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import { DetailClaimService } from "@claim-detail/services/detail-claim.service";
import { IClaimDetailResponse } from "@claim-detail/interfaces/claim-tpa-response.interface";
import { debounceTime, tap } from "rxjs";
import { BaseComponent } from "@shared/components";
import { REQUEST_PARAMS } from "@detail-request/shared/utils/enums";
import {
  ClaimProcessRequestComponent
} from "@claim-detail/components/claim-process-request/claim-process-request.component";

@Component({
  selector: "app-claim-detail-wrap",
  templateUrl: "./claim-detail-wrap.component.html",
  styleUrls: ["./claim-detail-wrap.component.scss"]
})
export class ClaimDetailWrapComponent extends BaseComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChild(ClaimProcessRequestComponent)
  processReqComp?: ClaimProcessRequestComponent

  ObjectSLA: any;
  role?: any[]

  claimDetailResponse?: IClaimDetailResponse;

  /**
   * ===========================
   *   OUTPUT STAGE
   * ==========================
   */

  @Output() onSubmit = new EventEmitter<any>();
  @Output() activeTabChange = new EventEmitter<any>();
  @Output() onCancel = new EventEmitter<any>();
  @Output() onSave = new EventEmitter<any>();
  @Output() onInitDetail = new EventEmitter<any>();
  @Output() onLoadDetail = new EventEmitter<any>();

  /**
   * ===========================
   *   INPUT STAGE
   * ==========================
   */

  @Input() disableSubmit: boolean = false;
  @Input() disableSave: boolean = false;
  @Input() activeTab: number = 0;
  @Input() showCreateTaskButton: boolean = false
  /**
   * @description
   * Dùng để xác định thời điểm gọi api của detail là
   * khi khởi tạo component hay chờ khi có claimRequestId được truyền vào thì mới call api.
   * Chỉ sử dụng một lần khi tạo component
   * - true - Call API detail ngay khi khởi tạo component
   * - false - Call API sau khi thay đổi claimRequestId
   *
   * @default true
   */
  @Input() loadDetailOnInit: boolean = true

  /**
   * claimRequestID để lấy thông tin detail chỉ sử dụng được khi biến loadDetailOnInit được set là false
   */
  @Input() outsideClaimRequestID?: string

  planAmount = 0
  benefitsClaimsEntities: any = []

  constructor(
    inject: Injector,
    private detailClaimService: DetailClaimService
  ) {
    super(inject);

    /**
     * Để thực hiện handle loading cho tất cả các api được call một cách đồng bộ
     * Tránh loading bị ngắt quảng do các API call tuần tự
     */
    this.subDetectLoading();
  }

  /**
   * là ClaimRequestID tùy vào route thì nó sẽ là claimRequestID hoặc là taskId
   */
  get claimRequestID() {
    return this.route.snapshot.paramMap.get(REQUEST_PARAMS.CLAIM_REQUEST_ID)!;
  }

  ngOnInit(): void {
    this.role = this.authService.getUserRoles()
    if (this.loadDetailOnInit) {
      this.getClaimDetail();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    /**
     * Chỉ check on changes của claimRequestID khi có loadDetailOnInit bằng false
     */
    if (
      !this.loadDetailOnInit &&
      changes['outsideClaimRequestID'] &&
      !!changes['outsideClaimRequestID'].currentValue
    ) {
        this.getClaimDetail(this.outsideClaimRequestID)
    }
  }

  ngAfterViewInit() {
    this.processReqComp?.latestReferDecision$().subscribe(
      (lastReferInfo) => {
        if (Number.isFinite(lastReferInfo?.planAmount)) {
          this.planAmount = lastReferInfo?.planAmount!
          this.benefitsClaimsEntities = lastReferInfo?.benefitClaimHistoryEntities
        }
      }
    )
  }


  cancel() {
    this.onCancel.emit();
    this.returnRequestsScreen();
  }

  /**
   * Trở về màn requests
   */
  private returnRequestsScreen() {
    this.location.back()
  }


  save() {
    this.onSave.emit();
  }

  submit() {
    this.onSubmit.emit();
  }


  /**
   * Thực hiện subscribe theo loadingCount của service để hiển thị loading
   */
  subDetectLoading() {
    this.detailClaimService.loadingCount$.pipe(
      tap((isLoading) => !!isLoading && !this.loadingService.loading && this.loadingService.start()),
      debounceTime(2000),
      tap((isLoading) => !isLoading && this.loadingService.loading && this.loadingService.complete())
    ).subscribe();
  }

  getClaimDetail(claimRequestID = this.claimRequestID) {
    this.onInitDetail.emit();
    this.detailClaimService.getDetail(claimRequestID).subscribe(
      (value) => {
        this.onLoadDetail.emit(value);
        return this.claimDetailResponse = value;
      }
    );

  }

  checkAmount(amount: number) {
    if (amount > 50000000 && amount <= 75000000) {
      return 'request.detailRequest.claim-tpa.warningPlanAmountDeputyClaim'
    } else if (amount > 75000000 && amount <= 100000000) {
      return 'request.detailRequest.claim-tpa.warningPlanAmountClaimManager'
    } else if (amount > 100000000 && amount <= 250000000) {
      return 'request.detailRequest.claim-tpa.warningPlanAmountOPDirecter'
    } else {
      return 'request.detailRequest.claim-tpa.warningPlanAmountCommittee'
    }
  }

}
