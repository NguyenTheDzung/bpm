import { Component, Injector, Input, OnChanges, SimpleChanges } from "@angular/core";
import { DynamicDialogConfig } from "primeng/dynamicdialog";
import { CreateTaskDialogComponent } from "@claim-detail/dialogs/create-task-dialog/create-task-dialog.component";
import { BaseComponent } from "@shared/components";
import { MenuItem } from "primeng/api";
import { ICreateTaskFormValue } from "@claim-detail/interfaces/claim-tpa.interface";
import { CloseTaskComponent } from "@claim-detail/dialogs/close-task/close-task.component";
import { createErrorMessage } from "@cores/utils/functions";
import { BpmClaimService } from "@claim-detail/services/bpm-claim.service";


@Component({
  selector: 'app-claim-create-task',
  templateUrl: './claim-create-task.component.html',
  styleUrls: ['./claim-create-task.component.scss']
})
export class ClaimCreateTaskComponent extends BaseComponent implements OnChanges {

  @Input() policyNumber?: string
  @Input() caseId?: number
  @Input() showCreateTaskButton: boolean = false
  @Input() claimID: string = ''
  @Input() claimRequestId = ""
  @Input() icCode: string = ''
  @Input() icName: string = ''
  @Input() bpCode: string = ''
  @Input() customerName: string = ''
  @Input() effectiveDate: string = ''
  @Input() longText: string = ''

  tabs!: ICreateTaskFormValue[]
  item!: ICreateTaskFormValue

  DEFAULT_CONFIG_DIALOG: DynamicDialogConfig = {
    showHeader: false,
    closable: true,
    closeOnEscape: true,
    dismissableMask: true,
    modal: true,
    style: {width: 'max(60%, 400px)'}
  }

  MENU_MODEL: MenuItem[] = []


  initMenuModel() {
    this.MENU_MODEL = [
      {
        label: 'Chỉnh sửa',
        command: () => {
          this.openTaskDialog(this.item)
        },
        disabled: this.item?.status != "IN_PROCESS"
      },
      {
        label: 'Huỷ task',
        command: () => {
          this.destroyTask({...this.item, check: 0})
        },
        disabled: this.item?.status != "IN_PROCESS"
      },
      {
        label: 'Đóng task',
        command: () => {
          this.destroyTask({...this.item, check: 1})
        },
        disabled: this.item?.status != "IN_PROCESS" && this.item?.status != "COMPLETE"
      }
    ]
  }

  constructor(
    inject: Injector,
    private bpmClaimService: BpmClaimService,
  ) {
    super(inject)
    this.initMenuModel()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.claimRequestId) {
      this.getTask()
    }
  }


  openTaskDialog(item?: ICreateTaskFormValue) {
    this.dialogService?.open(CreateTaskDialogComponent, {
      ...this.DEFAULT_CONFIG_DIALOG,
      data: {
        ...item,
        policyNumber: this.policyNumber,
        requestId: this.claimRequestId,
        claimId: this.claimID,
        caseId: this.caseId,
        icCode: this.icCode,
        icName: this.icName,
        bpCode: this.bpCode,
        customerName: this.customerName,
        longText: this.longText
      }
    }).onClose.subscribe((result) => {
      if (result) {
        this.getTask()
      }
    })
  }

  destroyTask(action: ICreateTaskFormValue) {
    this.dialogService.open(
      CloseTaskComponent,
      {
        ...this.DEFAULT_CONFIG_DIALOG,
        style: {width: 'max(30%, 250px)'},
        data: action
      }
    ).onClose.subscribe((result) => {
      if (result) {
        this.getTask()
      }
    })
  }

  trackByTabs(index: number, item: any) {
    return item
  }


  getTask() {

    this.loadingService.start()
    this.bpmClaimService.getListTask(this.claimRequestId).subscribe({
      next: (res: any) => {
        this.tabs = res.data
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }


  setDataDialog(tab: ICreateTaskFormValue) {
    this.item = tab
    this.initMenuModel()
  }


  protected readonly menubar = menubar;
}
