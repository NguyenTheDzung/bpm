import { Component, Injector, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { ICreateTaskFormValue, UpdateHistory } from "@claim-detail/interfaces/claim-tpa.interface";
import { createErrorMessage } from "@cores/utils/functions";
import { BaseComponent } from "@shared/components";
import { DetailClaimService } from "@claim-detail/services";
import { CommonModel } from "@common-category/models/common-category.model";
import { HISTORY_TYPE, TASK_INFO_MODE } from "@claim-detail/utils/enums";
import { INeedToWorkClaimItem, INeedToWorkClaimResponse } from "@claim-detail/interfaces/claim-tpa-response.interface";



@Component( {
  selector: 'app-task-info-view',
  templateUrl: './task-info-view.component.html',
  styleUrls: [ './task-info-view.component.scss' ]
} )
export class TaskInfoViewComponent extends BaseComponent implements OnChanges, OnInit {

  readonly HISTORY_TYPE = HISTORY_TYPE;
  readonly MODE = TASK_INFO_MODE

  @Input() info!: ICreateTaskFormValue | INeedToWorkClaimItem | INeedToWorkClaimResponse

  /**
   * Dùng để kiểm soát hiển thị field request xử lý
   * - true -> Hiển thị
   * - false -> Không hiển thị
   * @default true - Hiển thị
   */
  @Input() displayCodeAfterSubmit: boolean = true

  @Input() mode: TASK_INFO_MODE = TASK_INFO_MODE.NORMAL

  createTask: UpdateHistory[] = []
  listUpdateHistory: UpdateHistory[] = []
  listSubmitHistory: UpdateHistory[] = []
  attachments: any
  department!: CommonModel[]
  reUnderwritingCommon: CommonModel[] = []

  constructor( inject: Injector,
               private claimTpaService: DetailClaimService ) {
    super( inject )
  }

  ngOnInit() {
    this.loadingService.start()
    this.claimTpaService.getTypeCreateTask( 'GROUP_USER_V2' ).subscribe( {
      next: ( res ) => {
        this.department = res
        this.loadingService.complete()
      },
      error: ( err ) => {
        this.messageService.error( createErrorMessage( err ) )
        this.loadingService.complete()
      }
    } )

    this.claimTpaService.reUnderwritingCommon$.subscribe(
      ( rs ) => {
        this.reUnderwritingCommon = rs
      }
    )

  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes['info'] && changes['info'].currentValue){
      this.createTask = this.info?.histories.filter((value: UpdateHistory) => value.historyType === 'CREATE_TASK')
      this.listUpdateHistory = this.info?.histories.filter((value: UpdateHistory) => value.historyType === 'UPDATE_TASK')
      this.listSubmitHistory = this.info?.histories.filter((value: UpdateHistory) => value.historyType === 'SUBMIT_TASK')
    }
  }

}
