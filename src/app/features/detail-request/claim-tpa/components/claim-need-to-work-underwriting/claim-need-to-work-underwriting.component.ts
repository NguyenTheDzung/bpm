import { Component, Injector, Input, OnChanges, SimpleChanges } from "@angular/core";
import { DATE_FORMAT } from "@claim-detail/utils/enums";
import { INeedToWorkClaimItem } from "@claim-detail/interfaces/claim-tpa-response.interface";
import { BaseComponent } from "@shared/components";
import { REQUEST_PARAMS } from "@detail-request/shared/utils/enums";

@Component({
  selector: "app-claim-need-to-work-underwriting",
  templateUrl: "./claim-need-to-work-underwriting.component.html",
  styleUrls: ["./claim-need-to-work-underwriting.component.scss"]
})
export class ClaimNeedToWorkUnderwritingComponent extends BaseComponent implements OnChanges {
  readonly DATE_FORMAT = DATE_FORMAT;
  activeIndex?: number;

  currentRequestId: string;

  @Input() taskNeedToProcess: INeedToWorkClaimItem[] = [];
  @Input() requestCode?: string;
  @Input() requestPic?: string;

  constructor(
    injector: Injector
  ) {
    super(injector);

    this.currentRequestId = this.route.snapshot.paramMap.get(REQUEST_PARAMS.CLAIM_REQUEST_ID)!;
  }

  trackByTabs() {
    return true;
  }

  ngOnChanges(changes: SimpleChanges) {
    const prop = ["code", "taskNeedToProcess"];
    console.log(changes);
    if (prop.find((key) => changes[key])) {
      this.constructNeedToWork();
    }
  }

  constructNeedToWork() {
    /**
     * Nếu code không tồn tại trong list task thì  bỏ qua
     */
    const index  = this.taskNeedToProcess.findIndex(value => value.code == this.requestCode)
    if (index < 0) {
      return;
    }
    this.activeIndex = index
  }
}
