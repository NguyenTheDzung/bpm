export enum PROCESSING_TASK {
  //Điều tra
  INSPECT = '12',
  // Tái thẩm định
  REAPPRAISAL = '13',
  // Tái bảo hiểm
  REINSURANCE = '14'
}

export const REINSURANCE_ACTION = {
  SEND_EMAIL: "SEND_EMAIL",
  RE_INSURANCE: "RE_INSURANCE"
} as const;
export type REINSURANCE_ACTION = keyof typeof REINSURANCE_ACTION



export const DATE_FORMAT = 'dd/MM/YYYY hh:mm:ss a'
export const CAN_APPROVE_REQUEST = "DECISION"

export enum HISTORY_TYPE  {
  UPDATE_TASK = 'UPDATE_TASK',
  SUBMIT_TASK = 'SUBMIT_TASK'
}

export const TASK_INFO_MODE = {
  RE_UNDERWRITING: 'RE_UNDERWRITING',
  NORMAL: 'NORMAL'
}as const
export type TASK_INFO_MODE =  keyof typeof TASK_INFO_MODE
