import {AbstractControl} from "@angular/forms";
import {Observable} from "rxjs";
import {
  AmountLevel
} from "@config-approval-level-amount/model/config-approval-level-amount.model";

export function listenControlTouched(
  control: AbstractControl,
): Observable<boolean> {
  return new Observable<boolean>(observer => {
    const originalMarkAsTouched = control.markAsTouched;
    const originalReset = control.reset;

    control.reset = (...args) => {
      observer.next(false);
      originalReset.call(control, ...args);
    };

    control.markAsTouched = (...args) => {
      observer.next(true);
      originalMarkAsTouched.call(control, ...args);
    };

    observer.next(control.touched);

    return () => {
      control.markAsTouched = originalMarkAsTouched;
      control.reset = originalReset;
    };
  });
}

export async function convertFileToBase64(file: File) {
  const check = await file.arrayBuffer();
  return btoa(Array.from(new Uint8Array(check)).map(b => String.fromCharCode(b)).join(""));
}

const regexRemoveZero = /^0+([1-9].+)$/g

export function removeZeroHeadOfPolicyNumber(input?: string) {
  if(!input) return
  return input.replace(regexRemoveZero, '$1')
}

export function toMillion(amount: number) {
  return amount / 1000000
}

export function isMatchAmountLevelConditions(amount: AmountLevel, planAmount: number) {
  const validateAmountFrom = () => {
    switch (amount.amountFromComparison as "<" | "<=") {
      case "<":
        return planAmount > amount.amountFrom!
      case "<=":
        return planAmount >= amount.amountFrom!
    }
  }
}


