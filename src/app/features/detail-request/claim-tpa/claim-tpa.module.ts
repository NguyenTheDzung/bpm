import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClaimTPARoutingModule } from './claim-tpa-routing.module';
import { DetailRequestSharedModule } from '@detail-request/shared/detail-request-shared.module';
import { COMPONENTS } from '@claim-detail/components';
import { PAGES } from '@claim-detail/pages';
import { SERVICES } from '@claim-detail/services';
import { DIALOGS } from '@claim-detail/dialogs';
import { SharedModule } from '@shared/shared.module';
import { PIPES } from '@claim-detail/pipes';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule( {
  declarations: [ ...COMPONENTS, ...PAGES, ...DIALOGS, ...PIPES ],
  providers: [ ...SERVICES ],
  imports: [ CommonModule, ClaimTPARoutingModule, DetailRequestSharedModule, SharedModule, CKEditorModule ],
} )
export class ClaimTPAModule {
}
