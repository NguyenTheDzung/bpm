import {SavedFileModel} from "@claim-detail/interfaces/claim-tpa.interface";



export interface IClaimHistoryItem {
  requestId: string
  claimId: string
  requestType: string
  policyNumber: string
  icCode: string
  icName: string
  policyHolder: string
  bp: string
  bpName: string
  bpLA: string
  bpLAName: string
  claimType: string
  status: string
  resolution: string
  claimStatus: string
  sla: number
  requestSource: string
  createdDate: string
  updatedDate: string
  eventDateFrom: string
  eventDateTo: string
  submitFirstRequest: string
  phone: string
  email: string
  sourceDoc: string
}

export interface IDiagnosisDetail {
  icdCode: string
  diagnosis: string
}

export interface IMsgSts {
  code: string
  message: string
}

export interface IClaimDetailResponse {
  allByRequestId: IAllByRequestId
  claimCaseBenefitEntities: IClaimCaseBenefitEntity[]
  claimCaseSicknessEntities: IClaimCaseSicknessEntity[]
  facilityEntities: IFacilityEntity[]
  policyInfoDto: IPolicyInfoDTO
  businessPartnerLA: IBusinessPartnerLa
  businessPartnerHolder: IBusinessPartnerHolder
  listAttachmentByRequestId: any[]
  icDto: IIcDTO,
  policyDate: IPolicyDate
  policyLapseResponse?: IPolicyLapseResponse
}

export interface IPolicyLapseResponse {
  laspeDateConverted: string
  lapseDate: string
  reinsDate: string
  coverageId: string
}

export interface IPoliciesResponse {
  coverages: IPolicyCoverage[]
  holder: IPolicyHolder
  policyInfo: IPolicyInfo
}

export interface IPolicyCoverage {
  coverageId: string
  coverageName: string
  status: string
  paidToDate: string
  laspeDate: string
  reistDate: string
  startDate: string
  endDate: string
  coverageNumber: string
  premiumCoverage: string
  lifeAssured: IPolicyLifeAssured
}

export interface IPolicyLifeAssured {
  partnerId: string
  partnerName: string
  bod: string
  gender: string
  address: string
}

export interface IPolicyHolder {
  partnerId: string
  partnerName: string
  dob: string
  gender: string
  type: string
  address: string
}

export interface IPolicyInfo {
  policyNumber: string
  applicationNumber: string
  pmId: string
  premiumId: string
  policyName: string
  policyNameVN: string
  policyStart: string
  recordDate: string
  ackDate: string
  casePolicy: string
  paidToDate: string
  flpDate: string
  premiumMain: string
  status: string
  distribution: string
  paymentFrequency: string
  topup: string
}


export interface IIcDTO {
  changeRequest: object,
  current: {
    // Mã CVTV
    agentCode: string
    // Tên CVTV :
    agentName: string
    // Kênh
    channel: string
    // Region code
    organizationUnitName: string
  }
}

export interface IAllByRequestId {
  accName: string
  accNumber: string
  adjustments: string
  amount: number
  bankCode: string
  bankName: string
  benefitInputs: string
  branchCode: string
  branchName: string
  caseId: number
  cashAdd: string
  cashDate: string
  cashNumber: string
  cashType: string
  claimId: string
  claimRequestId: string
  createDate: string
  createdBy: string
  decisionCodeHistory: string
  email: string
  eventDatefrom: string
  eventDateto: string
  fullSubmissionDate: string
  isSubmit: boolean
  laCode: string
  laName: string
  note: string
  partnerCode: string
  paymentException: boolean
  paymentMethod: string
  phone: string
  pic: string
  policy: string
  reason: string
  reasonNote: string
  reasonOther: string
  resolution: string
  sendMail: boolean
  status: string
  submitFirstRequest: string
  hasDraftMail: boolean
  isSentEmail: boolean
  mailType: string
  mailAttachmentId: string
}

export interface IClaimCaseBenefitEntity {
  benefitCode: string
}

export interface IClaimCaseSicknessEntity {
  sicknessCode: string,
  sicknessName: string
}

export interface IFacilityEntity {
  facilityCode: string
  facilityName: string
}

export interface IPolicyInfoDTO {
  coverages: IPolicyInfoDTOCoverage[]
  holder: IPolicyInfoDTOHolder
  policyInfo: IPolicyInfoDTOPolicyInfo,
  underWritings: IPolicyUnderwriting[]
}

type UnderWritingItem = {
  underWritingCase: string
}

export interface IPolicyUnderwriting {
  underWritingCase: string
  statusName: string
  reasonName: string
  createdBy: string
  statusDate: string
  statusTime: string
  statusId: string
  reasonId: string
}


export interface IBusinessPartnerLa {
  type: string
  idNumber: string
  issueDate: string
  name: string
  gender: string
  birthDate: string
  bpNumber: string
  mobilePhone: string
  email: string
  issuePlace?: string
}

export interface IBusinessPartnerHolder {
  type: string
  idNumber: string
  issueDate: string
  name: string
  gender: string
  birthDate: string
  bpNumber: string
  mobilePhone: string
  email: string
  issuePlace?: string
}

export interface INeedToWorkClaimResponse extends INeedToWorkClaimItem {
  tasks: INeedToWorkClaimItem[]

  claimId: string

  slaStepList: any[]
  slaPercent: number
  slaStepPercent: number
  slaStepMinutesRemaining: number
  step: number
  slaResolution: string
  slaStepMessage: string
  slaPercentWarning: number
  isSubmit: boolean

  // Sẽ có  sau khi cập nhật
  decision: string,
  decisionContent: string
  attachments: SavedFileModel[]
}

export interface INeedToWorkClaimItem {
  claimRequestId: string
  createdBy: string
  createdDate: string
  lastUpdatedBy: string
  lastUpdatedDate: string
  id: number
  code: string
  type: string
  requestId: string
  policyNumber: string
  department: string
  pic: string
  status: string
  resolution: string
  content: string
  slaStep: number
  slaStartDate: string
  slaEndDate: string
  slaMessage: string
  histories: INeedToWorkClaimHistoryResponse[]
}

export interface INeedToWorkClaimHistoryResponse {
  id: number
  code: string
  type: string
  department: string
  pic: string
  status: string
  content: string
  decision: string
  historyType: string
  createdBy: string
  createdDate: string
  lastUpdatedBy: string
  lastUpdatedDate: string
}

export type IDetailRequestProcessResponse = IBaseClaimProcessItemResponse[]
export interface IBenefitClaimHistoryEntity {
  createdBy: string
  createdDate: string
  lastUpdatedBy: string
  lastUpdatedDate: string
  id: number
  codeBenefit: string
  status: string
  amountBenefit: number
  decisionCode: string
  decisionId: number
  claimHistoryId: number
  statusClaim: string
}

export interface IBaseClaimProcessItemResponse {
  benefitClaimHistoryEntities: IBenefitClaimHistoryEntity[]
  fileCaseDTOS: IFileCaseDTO[]
  claimStatus: string
  content: string
  createdBy: string
  createdDate: string
  decisionCode?: string
  decisionName?: string
  planAmount?: number
  executor: string
  id: number
  lastUpdatedBy: string
  lastUpdatedDate: string
  note: string
  sourceProcess: string,
  sourceProcessData: string
  inforType?: string
}

export interface IFileCaseDTO {
  id: number
  fileType: string
  fileName: string
  content: string
  claimCaseHistoryId: number
  sourceDoc: string
  typeFile: string
  createDate: string
}

export type IClaimAttachmentsResponse = IClaimAttachmentItem[]

export interface IClaimAttachmentItem {
  id: number
  fileType: string
  fileName: string
  content: string
  sourceDoc?: string
  fileVersion: number
  typeFile: string
  createDate: string
  recipient?: string
}


export interface IPolicyInfoDTOCoverage {
  coverageId: string
  coverageName: string
  status: string
  paidToDate: string
  laspeDate: string
  reistDate: string
  startDate: string
  endDate: string
  coverageNumber: string
  premiumCoverage: string
  lifeAssured: LifeAssured
}

export interface LifeAssured {
  partnerId: string
  partnerName: string
  bod: string
  gender: string
  address: string
}

export interface IPolicyInfoDTOHolder {
  partnerId: string
  partnerName: string
  dob: string
  gender: string
  type: string
  address: string
}

export interface IPolicyInfoDTOPolicyInfo {
  policyNumber: string
  applicationNumber: string
  pmId: string
  premiumId: string
  policyName: string
  policyNameVN: string
  policyStart: string
  recordDate: string
  ackDate: string
  casePolicy: string
  paidToDate: string
  flpDate: string
  premiumMain: string
  status: string
  distribution: string
  paymentFrequency: string
  topup: string
}

export interface IPolicyDate {
  policyNumber: string;
  startDate: string;
  paidToDate: string;
  resetDate: string;
}

export interface ISapAttachmentDownload{
  result: string
  createDate: string
  createBy: string
  fileName: string
  type: string
  length: string
}

 interface ISapAttachment {
  policyNrTT: string
  pmId: string
  arcDocId: string
  arObject: string
  arDate: string
  reverse: string
}

export type ClaimSAPAttachments = ISapAttachment[]






