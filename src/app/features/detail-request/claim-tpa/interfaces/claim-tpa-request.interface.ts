import { SavedFileModel } from "@claim-detail/interfaces/claim-tpa.interface";

export interface ISubmitOrSaveUnderwritingDecisionRequest {
  code: string,
  decision: string,
  decisionContent: string,
  requestFileDTOs: FileUploadModel[],
  attachments: SavedFileModel[]
}



export type FileUploadModel = {
  name: string,
  type: string,
  extension: string,
  content: string,
  mimeType: string,
  size: string
}

export interface ISubmitOrSaveInvestigatorDecisionRequest {
  code: string,
  decisionContent: string,
  requestFileDTOs: FileUploadModel[],
  attachments: SavedFileModel[]
}
