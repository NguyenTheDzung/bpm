import {IFormGroupModel} from "@cores/models/form.model";

export interface SEND_MAIL {
    emailType: string | undefined
    policyType: string
    amount: string
    terms: string
    decision: string
    rating: string
    exclusion: string
    cancelRider: string
}

export interface ISendMailForm extends IFormGroupModel<SEND_MAIL> {

}

export interface EMAIL_VIEW {
  id: string
  channel: string
  type: string
  title: string
  from: string
  to: string
  cc: string
  bcc: string
  content: string
  attachment: any
  name: string
  phone: string | number
}
