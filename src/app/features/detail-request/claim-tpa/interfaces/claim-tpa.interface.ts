import { IFormGroupModel } from "@cores/models/form.model";
import { PROCESSING_TASK, REINSURANCE_ACTION } from "@claim-detail/utils/enums";
import { TemplateRef } from "@angular/core";
import { FileUploadModel } from "@claim-detail/interfaces/claim-tpa-request.interface";
import { CommonClaimCategory } from "@cores/models/common-claim.model";

export type ICreateTaskFormValue = {
  attachmentClaims: any
  attachments: any
  claimRequestId: string
  type: PROCESSING_TASK,
  department: any
  pic: any
  content: string
  fileUpload: File[]
  action: REINSURANCE_ACTION
  subject: string
  emailTo: string
  emailCc: string
  emailContent: string
  reInsuranceContent: string
  createdBy: string
  createdDate: string
  lastUpdatedBy: string
  lastUpdatedDate: string
  id: number
  code: string
  requestId: string
  policyNumber: string
  status: string
  resolution: string
  slaStep: number
  slaStartDate: string
  slaEndDate: string
  slaMessage: string
  check: number,
  histories: any[]
  caseId: number
  isSendEmail: boolean
  claimId: string
  icCode: string
  icName: string
  bpCode: string
  customerName: string
  longText: string
}

export type UpdateHistory = {
  id: number
  code: string
  type: string
  department: string
  pic: string
  status: string
  content: string
  decision: string
  historyType: string
  createdBy: string
  createdDate: string
  lastUpdatedBy: string
  lastUpdatedDate: string
  attachFile: any

  decisionContent?: string
  attachments?: any
}

export interface CreateTaskFormModel extends IFormGroupModel<ICreateTaskFormValue> {
}

export interface IBenefitValue extends CommonClaimCategory {
  money?: number
  checked?: boolean
  [p: string]: any
}

export interface IUpdatedTaskDialogData extends Partial<Omit<ICreateTaskFormValue, "type">> {
  processingTask: PROCESSING_TASK
}

export interface IProcessingTaskInformation {
  nameTask: string,
  items: IProcessingTaskInformationItem[]
}

export interface IProcessingTaskInformationItem {
  // Có thể sử dụng translate query string
  id: any,
  title: string,
  body: any,
  bodyStyleClass?: string | string[] | Set<string> | { [p: string]: any },
  routerLink?: string,
  onClickBody?: ($event: any, items: any) => void
  bodyTemplate?: TemplateRef<any>
}

interface IDecisionClaimFormValue {
  claimRequestId: string
  code: string,
  note: string,
  noteApprove: string,
  benefits: IBenefitSelectionItem[],
  provision: CheckProvisionCheckboxValue[],
  suspectedFraud: boolean
  checkSuspectedFraudItem: string[],
  hospital: boolean
  diagnosis: boolean
  amount: number
  policy: string
  other: boolean,
  detail: string,
  reason: string,
  reasonNote: string,
  reasonOther: string,
  sendMail: boolean,
  paymentException: boolean
  benefitInputs: any
  adjustments: any
}

export interface IDecisionClaimForm extends IFormGroupModel<IDecisionClaimFormValue> {

}

export interface IBenefitSelectionItem {
  code: string,
  money?: number,
  [p:string]: any
}

export interface CheckProvisionCheckboxValue {
  code: string,
  checked: boolean,
  money?: string | number
}

export interface ICloseTask {
  taskId: string
  type: string
  status: string
  reason: string
}

export type ClaimUnderWritingDecisionValue = {
  files: FileUploadPreModel[]
  content: string,
  decision: string,
  savedFiles: SavedFileModel[]
}

export type FileUploadPreModel = FileUploadModel & {
  content: any,
  file: File,
}

export type SavedFileModel = {
  fileName: string
  fileSize: number
  fileType: string
  fileNameS3: string
  extension: string
  mimeType: string
}

export type ClaimInvestigationDecisionValue = {
  files: FileUploadPreModel[]
  content: string,
  savedFiles: SavedFileModel[]
}

export interface Attachment {
  id: number
  fileType: string
  fileName: string
  content: string
  claimCaseHistoryId: number
  sourceDoc: string
  typeFile: string
  createDate: string
}

export interface DecisionValue {
  isSubmit: boolean
  code: string
  note: string
  benefits: any
  provision: any
  amount: number
  claimRequestId: string
  sendMail: boolean
  paymentException: boolean
  benefitInputs: any
  adjustments: any
  reason: string
  reasonNote: string
  reasonOther: string
  decisionCodeHistory: string
}

