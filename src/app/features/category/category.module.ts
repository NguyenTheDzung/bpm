import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedModule } from "@shared/shared.module";
import { CategoryRoutingModule } from "./category-routing.module";
import { Pages } from "./pages";
import { Dialogs } from "./dialogs";
import { PasswordModule } from "primeng/password";
import { Components } from "./components";
import { BackendService } from "./services/backend.service";
import { SplitterModule } from "primeng/splitter";
import { PanelModule } from "primeng/panel";
import { MenuModule } from "primeng/menu";
import { ListboxModule } from "primeng/listbox";
import { FrontEndService } from "./services/front-end.service";
import { NgxEchartsModule } from "ngx-echarts";


@NgModule({
  declarations: [...Pages, ...Dialogs, ...Components],
  imports: [
    SharedModule,
    CategoryRoutingModule,
    PasswordModule,
    SplitterModule,
    PanelModule,
    MenuModule,
    ListboxModule,
    NgxEchartsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [BackendService, FrontEndService]
})
export class CategoryModule {
}
