import { Injectable } from "@angular/core";
import { BaseService } from "@cores/services/base.service";
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { mapDataTableWithType, removeParamSearch } from "@cores/utils/functions";
import { CommonCategoryService } from "@cores/services/common-category.service";
import { IBackendModel, IConfigModel } from "../models/backend.model";
import { IPageableResponseModel, IResponseModel } from "@cores/models/response.model";
import { DataTable } from "@cores/models/data-table.model";
import { environment } from "@env";

@Injectable(
  {providedIn: 'root'}
)
export class BackendService extends BaseService {
  constructor(private httpClient: HttpClient, private common: CommonCategoryService,) {
    super(httpClient, `${environment.category_url}/adm/fund`)
  }

  getData(params: any): Observable<DataTable<IBackendModel>> {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<IResponseModel<IPageableResponseModel<IBackendModel>>>(`${this.baseUrl}?sort=functionCode,asc`, {
        params: {...newParam},
      })
      .pipe(map((response) => mapDataTableWithType(response.data, params)));
  }

  getFunctionType() {
    return this.common.getCommonCategory('FUNCTIONTYPE_BACKEND_CATEGORY');
  }

  getSystemCategory() {
    return this.common.getCommonCategory('SYSTEM_CATEGORY')
  }
  addQueryConfig(params: IConfigModel) {
    return this.http.put(`${this.baseUrl}/createAdmFuncQueryBO`, params)
  }

  getDataConfig(params: IBackendModel): Observable<any> {
    return this.http.put(`${this.baseUrl}/prepareConfigQuery`, params)
  }

  postItem(params: IBackendModel) {
    return this.http.post(`${this.baseUrl}/create`, params)
  }

  editItem(params: IBackendModel) {
    return this.http.post(`${this.baseUrl}/update`, params)
  }

  deleteItem(item: IBackendModel) {
    return this.http.delete(`${this.baseUrl}`, {
      body: item
    })
  }

  deleteItemConfig(item: IConfigModel) {
    return this.http.delete(`${this.baseUrl}/onDeleteQuery`, {
      body: item
    })
  }

  importBackEnd(data: any) {
    return this.http.post(`${this.baseUrl}/onImportXml`, data)
  }

  exportBackEnd(params: string) {
    return this.http.put(`${this.baseUrl}/onExportXml/${params}`, null)
  }

  testQueryConfig(item: IConfigModel) {
    return this.http.put(`${this.baseUrl}/onTestDirectQuery`, item)
  }

  setBaseCondition(functionCode: string, queryId: number) {
    return this.http.put(`${this.baseUrl}/copyConds?functionCode=${functionCode}&queryId=${queryId}`, {})
  }

  showConditionColumn(params: any, body: IConfigModel) {
    return this.http.put(`${this.baseUrl}/showTabChildOfQuery?tab=${params}`, body)
  }

  editCondition(body: any) {
    return this.http.post(`${this.baseUrl}/onSaveOrUpdateQueryCondition`, body)
  }

  editColumn(body: any) {
    return this.http.put(`${this.baseUrl}/onUpdateColumns`, body)
  }

  deleteCondition(body: any) {
    return this.http.put(`${this.baseUrl}/onDeleteQueryCond`, body)
  }
}
