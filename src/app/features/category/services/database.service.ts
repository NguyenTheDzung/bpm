import { Injectable } from "@angular/core";
import { map, Observable, of } from "rxjs";
import { BaseService } from "@cores/services/base.service";
import { HttpClient } from "@angular/common/http";
import { IDatabaseModel } from "../models/database.model";
import { mapDataTableEmployee, removeParamSearch } from "@cores/utils/functions";
import { DataTable } from "@cores/models/data-table.model";
import { environment } from "@env";

@Injectable({
  providedIn: 'root'
})
export class DatabaseService extends BaseService {

  constructor(http: HttpClient) {
    super(http, `${environment.category_url}/adm/database`);
  }

  getData(params: any): Observable<any> {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<DataTable>(`${this.baseUrl}?sort=dbId,asc`, {
        params: {...newParam},
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  postDatabase(param: IDatabaseModel) {
    return this.http.post(`${this.baseUrl}`, param)
  }

  deleteItem(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`)
  }

  testConnect(id: number) {
    return this.http.post(`${this.baseUrl}/connect/${id}`, null)
  }

  getSelect() {
    return of(
      [
        {
          code: 'Disable',
          value: 0
        },
        {
          code: 'Enable',
          value: 1
        }
      ]
    )
  }
}
