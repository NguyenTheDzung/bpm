import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, Observable, Observer, tap } from "rxjs";
import {
  IChangDBFrontEndSearchParams,
  IExportExcelFileModel,
  IOnViewFunctionResponse
} from "../models/front-end.model";
import { BaseService } from "@cores/services/base.service";
import { IPageableResponseModel, IResponseModel } from "@cores/models/response.model";
import { createErrorMessage, mapDataTableWithType } from "@cores/utils/functions";
import { DataTable } from "@cores/models/data-table.model";
import { NotificationMessageService } from "@cores/services/message.service";
import { LoadingService } from "@cores/services/loading.service";
import { environment } from "@env";

@Injectable()
export class FrontEndService extends BaseService {

  baseObserver: Partial<Observer<any>> = {
    next: () => {
      this.loadingService.complete()
    },
    error: (error) => {
      this.messageService.error(createErrorMessage(error))
      this.loadingService.complete()
    },
  };

  constructor(
    http: HttpClient,
    private messageService: NotificationMessageService,
    private loadingService: LoadingService
  ) {
    super(http, `${environment.category_url}/adm/view`);
  }

  callAPIOnViewFunction(functionData: any) {
    this.loadingService.start();
    const url = `${this.baseURL}/${functionData.functionCode}/onViewFunction`;
    return this.http.get<IResponseModel<IOnViewFunctionResponse[]>>(url).pipe(
      tap(this.baseObserver)
    );
  }

  postOnViewFunction(functionData: any): Observable<IOnViewFunctionResponse[]> {
    return this.callAPIOnViewFunction(functionData).pipe(
      map(val => val.data)
    );
  }

  exportXML(functionCode: string, conditions: object, type: string): Observable<IResponseModel<IExportExcelFileModel>> {
    this.loadingService.start();
    const url = `${this.baseURL}/${functionCode}/onExport`;
    return this.http.post<IResponseModel<IExportExcelFileModel>>(url, {conditions, type }).pipe(
      tap(this.baseObserver)
    );
  }

  execute(functionCode: string, conditions: object): Observable<IResponseModel> {
    this.loadingService.start();
    const url = `${this.baseURL}/${functionCode}/onExecute`;
    return this.http.post<IResponseModel>(url, {conditions}).pipe(
      tap(this.baseObserver)
    );
  }

  getSearchData(params: IChangDBFrontEndSearchParams, functionCode: string, loading = true): Observable<DataTable[]> {
    loading && this.loadingService.start();
    const url = `${this.baseURL}/${functionCode}/search`;
    return this.http.post<IResponseModel<IPageableResponseModel<any>[]>>(url, params).pipe(
      tap({
        next: () => {loading && this.loadingService.complete()},
        error: (error) =>  {
          this.messageService.error(createErrorMessage(error))
          loading && this.loadingService.complete()
        }
      }),
      map((response) => {
          return response.data.map(
            (data: any, index) => {
              return mapDataTableWithType(data, {...params.queries[index], size: params.queries[index].rows});
            });
        }
      )
    );
  }

  getChartData(params: IChangDBFrontEndSearchParams, functionCode: string, loading = true): Observable<DataTable[]> {
    loading && this.loadingService.start();
    const url = `${this.baseURL}/${functionCode}/chart`;
    return this.http.post<IResponseModel<IPageableResponseModel<any>[]>>(url, params).pipe(
      tap({
        next: () => {loading && this.loadingService.complete()},
        error: (error) =>  {
          this.messageService.error(createErrorMessage(error))
          loading && this.loadingService.complete()
        }
      }),
      map((response) => {
          return response.data.map(
            (data: any) => {
              return {
                content: data,
                currentPage: 0,
                size: 10,
                totalElements: 0,
                totalPages: 0,
                first: 0,
                numberOfElements: 0,
              }
            });
        }
      )
    );
  }

}
