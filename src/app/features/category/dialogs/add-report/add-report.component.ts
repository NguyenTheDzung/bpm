import {Component, Injector, OnInit} from '@angular/core';
import {FormGroup, Validators} from "@angular/forms";
import {BaseActionComponent} from "@shared/components";
import {BackendService} from "../../services/backend.service";
import {createErrorMessage, validateAllFormFields} from "@cores/utils/functions";
import {CommonModel} from "@common-category/models/common-category.model";
import {regexStartLetter} from "@cores/utils/constants";
import {IBackendModel} from "../../models/backend.model";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-add-report',
  templateUrl: './add-report.component.html',
  styleUrls: ['./add-report.component.scss']
})
export class AddReportComponent extends BaseActionComponent implements OnInit {

  item: IBackendModel;
  functionType!: CommonModel[]
  systemCategory!: CommonModel[]
  override form: FormGroup
  constructor(injector: Injector, private service: BackendService) {
    super(injector, service)
    this.item = this.configDialog?.data?.state;
    this.form = this.fb.group(
      {
        functionCode: [null, [Validators.required, Validators.pattern(regexStartLetter)]],
        functionName: [null, Validators.required],
        functionTitle: [null, Validators.required],
        functionType: [null, Validators.required],
        refreshInterval: [0, Validators.required],
        modules: [null, Validators.required],
        moduleName: [null, Validators.required]
      }
    )
  }
  ngOnInit(): void {
    this.loadingService.start();
    if (this.item) {
      this.form.patchValue(this.item);
    }
    const functionType = this.service.getFunctionType();
    const systemCategory = this.service.getSystemCategory();
    forkJoin([functionType, systemCategory]).subscribe({
      next: (res) => {
        this.functionType = res[0];
        this.systemCategory = res[1]
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }
  override cancel() {
    if (this.configDialog) {
      this.refDialog.close(false);
    } else {
      this.location.back()
    }
  }
  override create() {
    validateAllFormFields(this.form!);
    if (this.form?.status !== 'VALID') {
      return;
    }
    this.loadingService.start();
    if (this.item) {
      this.service.editItem(this.form.getRawValue()).subscribe({
        next: () => {
          this.messageService.success('MESSAGE.SUCCESS');
          this.loadingService.complete();
          this.refDialog.close(true)
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        }
      })
    } else {
      this.service.postItem(this.form.getRawValue()).subscribe({
        next: () => {
          this.messageService.success('MESSAGE.SUCCESS');
          this.loadingService.complete();
          this.refDialog.close(true)
        },
        error: (err) => {
          this.messageService.error(createErrorMessage(err));
          this.loadingService.complete();
        }
      })
    }
  }

  setValueModelName($event: any) {
    this.form.controls['moduleName'].setValue(this.systemCategory.find(value => {
      return value.value === $event.value
    })?.name)
  }
}
