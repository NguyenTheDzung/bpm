import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {DatabaseService} from "../../services/database.service";
import {createErrorMessage, updateValidity, validateAllFormFields} from "@cores/utils/functions";
import * as _ from "lodash";
import {BackendService} from "../../services/backend.service";
import {Validators} from "@angular/forms";
import {IConfigModel, IQueryModel} from "../../models/backend.model";
import {ChartType, Large} from "../../utils/enums";

@Component({
  selector: 'app-add-query',
  templateUrl: './add-query.component.html',
  styleUrls: ['./add-query.component.scss']
})
export class AddQueryComponent extends BaseActionComponent implements OnInit {
  dbName?: any
  item: IConfigModel
  type = [
    {
      value: 'direct',
      name: 'DIRECT'
    }]
  chartType = ChartType
  isLarge = Large

  constructor(inject: Injector, private service: DatabaseService, private beService: BackendService) {
    super(inject, service);
    this.item = this.configDialog?.data?.data?.state;
  }

  override form = this.fb.group({
    functionCode: [null, Validators.required],
    dbId: [null, Validators.required],
    queryName: [null, Validators.required],
    queryString: [null, Validators.required],
    gridName: [null],
    sheetName: [null],
    queryIndex: [null, Validators.required],
    queryType: ['direct', Validators.required],
    isLarge: [null],
    chartType: [null]
  })

  ngOnInit(): void {
    this.loadingService.start();
    if (this.item) {
      if (this.item.functionType === 'chart') {
        updateValidity(this.form.get('typeChart'), Validators.required);
        updateValidity(this.form.get('large'), Validators.required);
      }
      this.form.patchValue(this.item);
    }
    this.service.getData('').subscribe({
      next: (res) => {
        this.dbName = _.uniqBy(res.content.map((data: IQueryModel) => {
          return {
            value: data?.dbId,
            name: data?.dbName
          }
        }), 'name')
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  override save() {
    validateAllFormFields(this.form!);
    if (this.form?.status !== 'VALID') {
      return;
    }
    this.loadingService.start();
    const data = this.form.value
    const newData = {
      id: {
        functionCode: this.form.controls['functionCode'].value
      },
      ...data
    }
    delete newData.functionCode
    if (this.item?.id?.queryId) {
      newData.id.queryId = this.item.id.queryId
    }
    this.beService.addQueryConfig(newData).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
        this.refDialog.close({status: true, data: {...newData}})
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close(false);
    } else {
      this.location.back()
    }
  }
}
