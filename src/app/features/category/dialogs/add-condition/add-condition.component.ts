import {Component, Injector, OnInit} from '@angular/core';
import {createErrorMessage, validateAllFormFields} from "@cores/utils/functions";
import {BaseActionComponent} from "@shared/components";
import {Validators} from "@angular/forms";
import {BackendService} from "../../services/backend.service";
import {CompareType, CONTROL_TYPE_DATE, ControlType, DataType} from "../../utils/enums";
import {IEditConditionModel, IQueryModel} from "../../models/backend.model";
import * as _ from "lodash";
import {DatabaseService} from "../../services/database.service";

@Component({
  selector: 'app-add-condition',
  templateUrl: './add-condition.component.html',
  styleUrls: ['./add-condition.component.scss']
})
export class AddConditionComponent extends BaseActionComponent implements OnInit {

  dialogData: IEditConditionModel;
  compareData = CompareType;
  typeData = DataType;
  controlData = ControlType;
  controlDataDate = CONTROL_TYPE_DATE
  controlTypeValue?: string
  dbName: any

  constructor(inject: Injector, private service: BackendService, private dbService: DatabaseService) {
    super(inject, service);
    this.dialogData = this.configDialog?.data?.state
  }

  ngOnInit(): void {
    if (this.state) {
      this.state.isRequire = !!this.state.isRequire
      this.form.patchValue(this.dialogData)
      this.changValue({value: this.dialogData.dataType})
      this.changValueControlType({value: this.dialogData.controlType})
    }
    this.loadingService.start()
    this.dbService.getData('').subscribe({
      next: (res) => {
        this.dbName = _.uniqBy(res.content.map((item: IQueryModel) => {
          return {
            value: item?.dbId,
            name: item?.dbName
          }
        }), 'name')
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  override form = this.fb.group({
    columnCode: [null, Validators.required],
    compareType: [null, Validators.required],
    dataType: [null, Validators.required],
    controlType: [null, Validators.required],
    isRequire: [false],
    controlLabel: [null, Validators.required],
    dbId: [null],
    databind: [''],
    controlIndex: [null, Validators.required],
    defaultValue: [null]
  })

  override save() {
    validateAllFormFields(this.form!);
    if (this.form?.status !== 'VALID') {
      return;
    }
    const data = this.form.getRawValue()
    if (data.isRequire === true) {
      data.isRequire = 'isRequired'
    } else {
      data.isRequire = null
    }
    data.id = {
      functionCode: this.dialogData.functionCode,
      queryId: this.dialogData.queryId,
      controlId: this.dialogData?.id?.controlId ?? null
    }
    this.loadingService.start();
    this.service.editCondition(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS')
        this.loadingService.complete()
        this.refDialog.close(true)
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete();
      }
    })
  }

  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close();
    } else {
      this.location.back();
    }
  }

  changValue($event: any) {
    switch ($event?.value) {
      case 'Date':
        if (!this.controlDataDate.find(value => {
          return value.name === 'MonthPicker'
        })) {
          this.controlDataDate.push({
            value: "MonthPicker",
            name: "MonthPicker"
          })
        }
        this.addValidator()
        break;
      case 'roleLogin':
        this.removeValidator()
        break;
      case 'userLogin':
        this.removeValidator()
        break;
      default:
        this.addValidator()
        const index = this.controlDataDate.findIndex(value => {
          return value.name === 'MonthPicker'
        })
        if (index > -1) this.controlDataDate.splice(index, 1)
    }
  }


  // handle form Validator and action
  removeValidator() {
    this.form.controls['controlType'].removeValidators([Validators.required]);
    this.form.controls['controlLabel'].removeValidators([Validators.required]);
    this.form.controls['controlIndex'].removeValidators([Validators.required]);
    this.form.updateValueAndValidity();

    this.form.controls['controlType'].disable()
    this.form.controls['controlLabel'].disable()
    this.form.controls['dbId'].disable()
    this.form.controls['controlIndex'].disable()
    this.form.controls['databind'].disable()
    this.form.controls['isRequire'].disable()
    this.form.controls['defaultValue'].disable()

    this.form.reset({
      columnCode: this.dialogData.columnCode,
      compareType: this.dialogData.compareType,
      dataType: this.form.controls['dataType'].value
    })
  }

  addValidator() {
    this.form.controls['controlType'].enable()
    this.form.controls['controlLabel'].enable()
    this.form.controls['controlIndex'].enable()
    this.form.controls['databind'].enable()
    this.form.controls['isRequire'].enable()
    this.form.controls['dbId'].enable()
    this.form.controls['defaultValue'].enable()


    this.form.patchValue({
      isRequire: this.dialogData.isRequire,
      controlLabel: this.dialogData.controlLabel,
      databind: this.dialogData.databind,
      controlIndex: this.dialogData.controlIndex,
      defaultValue: this.dialogData.defaultValue
    })

    if (!this.form.controls['controlType'].hasValidator(Validators.required)) {
      this.form.controls['controlType'].addValidators(Validators.required)
      this.form.controls['controlLabel'].addValidators(Validators.required)
      this.form.controls['controlIndex'].addValidators(Validators.required)
    }
  }

  changValueControlType($event: any) {
    this.controlTypeValue = $event?.value
    if (this.controlTypeValue === 'MultiSelect' || this.controlTypeValue === 'SelectedBox') {
      this.form.controls['dbId'].setValue(this.dialogData?.dbId)
    } else {
      this.form.controls['dbId'].setValue(null)
    }
  }
}
