import {AddReportComponent} from "./add-report/add-report.component";
import {CreateDatabaseComponent} from "./create-database/create-database.component";
import {ConfigQueryComponent} from "./config-query/config-query.component";
import {AddQueryComponent} from "./add-query/add-query.component";
import {AddConditionComponent} from "./add-condition/add-condition.component";
import {AddColGroupComponent} from "./add-col-group/add-col-group.component";
import {ChildConditionsComponent} from "./child-conditions/child-conditions.component";
import {HandleExcelComponent} from "./handle-excel/handle-excel.component";

export const Dialogs = [
  AddReportComponent,
  CreateDatabaseComponent,
  ConfigQueryComponent,
  AddQueryComponent,
  AddConditionComponent,
  AddColGroupComponent,
  ChildConditionsComponent,
  HandleExcelComponent
]
