import {Component, Injector, ViewChild} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {createErrorMessage, exportFile} from "@cores/utils/functions";
import {FileUpload} from "primeng/fileupload";
import {BackendService} from "../../services/backend.service";
import {INTERNAL_ERROR_MESSAGE} from "@cores/utils/constants";

@Component({
  selector: 'app-handle-excel',
  templateUrl: './handle-excel.component.html',
  styleUrls: ['./handle-excel.component.scss']
})
export class HandleExcelComponent extends BaseActionComponent {
  @ViewChild('fileInput') fileInputs?: FileUpload
  check?: boolean
  functionCode?: string
  override data: any
  constructor(inject: Injector, private service: BackendService) {
    super(inject, service);
    this.check = this.configDialog?.data?.state
  }

  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close();
    } else {
      this.location.back();
    }
  }

  export() {
    if (!this.functionCode) {
      return this.messageService.warn('Vui lòng nhập mã chức năng')
    }
    this.loadingService.start()
    this.service.exportBackEnd(this.functionCode!).subscribe({
      next: (res: any) => {
        exportFile(this.functionCode!, res?.data?.content, 'application/xml')
        this.loadingService.complete()
        this.messageService.success('MESSAGE.SUCCESS')
        this.refDialog.close()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  import() {
    const file = (this.fileInputs?.files[0])!
    if (!file) {
      return this.messageService.warn('MESSAGE.FILE_NULL')
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onerror = () => {
      this.messageService.error('MESSAGE.' + INTERNAL_ERROR_MESSAGE)
    }
    reader.onload = () => {
      this.data = {
        name: file.name,
        type: 'ZCOL_PR',
        extension: file.name.substring(file.name.lastIndexOf('.')).toLowerCase(),
        content: String(reader.result).split(',')[1],
        mimeType: file.type,
      }
      this.loadingService.start();
      this.service.importBackEnd(this.data).subscribe({
        next: () => {
          this.loadingService.complete()
          this.refDialog.close(true)
          return this.messageService.success('MESSAGE.SUCCESS')
        },
        error: (err) => {
          this.loadingService.complete()
          return this.messageService.error(createErrorMessage(err));
        }
      })
    };

  }


}
