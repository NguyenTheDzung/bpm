import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {DatabaseService} from "../../services/database.service";
import {validateAllFormFields} from "@cores/utils/functions";

@Component({
  selector: 'app-add-col-group',
  templateUrl: './add-col-group.component.html',
  styleUrls: ['./add-col-group.component.scss']
})
export class AddColGroupComponent extends BaseActionComponent implements OnInit {

  constructor(inject: Injector, private service: DatabaseService) {
    super(inject, service);
  }

  ngOnInit(): void {
  }

  override save() {
    validateAllFormFields(this.form!);
    if (this.form?.status !== 'VALID') {
      return;
    }
    console.log('OK')
  }
  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close();
    } else {
      this.location.back();
    }
  }
}
