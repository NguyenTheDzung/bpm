import {Component, Injector, OnInit} from "@angular/core";
import {createErrorMessage, validateAllFormFields} from "@cores/utils/functions";
import {BaseActionComponent} from "@shared/components";
import {BackendService} from "../../services/backend.service";
import {MenuItem} from "primeng/api";
import {ScreenType} from "@cores/utils/enums";
import {DialogService} from "primeng/dynamicdialog";
import {AddQueryComponent} from "../add-query/add-query.component";
import {IBackendModel, IColumnModel, IConditionsModel, IConfigModel} from "../../models/backend.model";
import {ChartType, Large} from "../../utils/enums";

@Component({
  selector: 'app-config-query',
  templateUrl: './config-query.component.html',
  styleUrls: ['./config-query.component.scss'],
  providers: [DialogService],
})
export class ConfigQueryComponent extends BaseActionComponent implements OnInit {

  items!: MenuItem[];
  override state: IBackendModel;
  activeIndex!: number;
  override data!: IConfigModel[];
  listCondition!: IConfigModel;
  dataCondition!: IConditionsModel[]
  dataColumns!: IColumnModel[];
  dataGroupSum: any;
  currentItem!: IConfigModel
  currentTab?: string
  isLarge = Large
  chartType = ChartType

  constructor(injector: Injector, private service: BackendService, private dialogService: DialogService) {
    super(injector, service)
    this.state = this.configDialog?.data?.state;
  }

  ngOnInit(): void {
    this.callApiData();
  }

  callApiData() {
    this.loadingService.start();
    this.service.getDataConfig(this.state).subscribe({
      next: (res) => {
        this.data = res.data.content.sort((a: any, b: any) => {
          return a.queryIndex - b.queryIndex
        })
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  override cancel() {
    if (this.configDialog) {
      this.refDialog.close();
    } else {
      this.location.back()
    }
    this.loadingService.complete();
  }

  openDialogQuery(check: boolean, item?: IConfigModel) {
    const data = {
      ...item,
      functionCode: this.state.functionCode,
      functionType: this.state.functionType,
      check: check
    }
    this.dialogService.open(
      AddQueryComponent,
      {
        showHeader: false,
        data: {
          showHeader: false,
          width: '50%',
          data: {
            screenType: ScreenType.Create,
            state: check ? data : {functionCode: this.state.functionCode, functionType: this.state.functionType}
          }
        }
      }
    ).onClose.subscribe((result) => {
      if (result.status) {
        this.callApiData()
        this.currentItem = result.data
        this.show()
      }
    })
  }

  changeTab(tab: typeof this.currentTab) {
    this.currentTab = tab
  }

  changeItem(item: IConfigModel) {
    this.currentItem = item
  }

  change(tab: typeof this.currentTab, query: IConfigModel) {
    this.changeTab(tab);
    this.changeItem(query)
    this.show();
  }

  show(tab = this.currentTab, query = this.currentItem) {
    this.loadingService.start();
    this.service.showConditionColumn(tab, query).subscribe({
      next: (res: any) => {
        switch (tab) {
          case 'conditions':
            this.activeIndex = 1
            this.dataCondition = res?.data
            break;
          case 'columns':
            this.activeIndex = 0
            this.dataColumns = res?.data
            break;
          case 'groupSum':
            this.activeIndex = 2
            this.dataGroupSum = res?.data
            break;
        }
        this.loadingService.complete()
      }
    })
    this.listCondition = query
  }

  deleteItem(params: IConfigModel) {
    this.messageService.confirm().subscribe((confirm) => {
      if (confirm) {
        this.loadingService.start();
        this.service.deleteItemConfig(params).subscribe({
          next: () => {
            this.callApiData();
            this.loadingService.complete();
          },
          error: (err) => {
            this.messageService.error(createErrorMessage(err));
            this.loadingService.complete();
          }
        })
      }
    })
  }

  testQuery(item: IConfigModel) {
    this.loadingService.start()
    this.service.testQueryConfig(item).subscribe({
      next: () => {
        this.loadingService.complete();
        this.messageService.success('Test query thành công')
      },
      error: (err) => {
        this.loadingService.complete()
        this.messageService.error(createErrorMessage(err));
      }
    })
  }

  setBaseCondition(item: IConfigModel) {
    this.loadingService.start()
    this.service.setBaseCondition(item.id.functionCode, item.id.queryId!).subscribe({
      next: (res) => {
        this.messageService.success('MESSAGE.SUCCESS')
        this.loadingService.complete()
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err))
        this.loadingService.complete()
      }
    })
  }
}
