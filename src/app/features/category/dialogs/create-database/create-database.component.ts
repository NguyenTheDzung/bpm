import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {DatabaseService} from "../../services/database.service";
import {Validators} from "@angular/forms";
import {createErrorMessage, validateAllFormFields} from "@cores/utils/functions";
import {IDatabaseModel} from "../../models/database.model";
import {DATABASE_TYPE} from "../../utils/enums";

@Component({
  selector: 'app-create-database',
  templateUrl: './create-database.component.html',
  styleUrls: ['./create-database.component.scss']
})
export class CreateDatabaseComponent extends BaseActionComponent implements OnInit {
  edit: boolean = true;
  item: IDatabaseModel;
  option: { code: string, value: number }[] = []
  dbType = DATABASE_TYPE
  constructor(inject: Injector, private service: DatabaseService) {
    super(inject, service);
    this.item = this.configDialog.data.state;
  }
  ngOnInit(): void {
    this.loadingService.start();
    this.service.getSelect().subscribe((res) => {
      this.option = res;
    })
    if (this.item){
      this.form.patchValue(this.item)
    }
    this.loadingService.complete()
  }

  override form = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required],
    dbName: [null, Validators.required],
    driver: [null, Validators.required],
    url: [null, Validators.required],
    status: [1, Validators.required]
  })
  override save() {
    validateAllFormFields(this.form!);
    if (this.form?.status !== 'VALID') {
      return;
    }
    this.loadingService.start();
    const data = this.form.getRawValue();
    data.dbId = this.item?.dbId;
    if (this.item && this.edit) {
      data.password = null
    }
    this.service.postDatabase(data).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
        this.refDialog.close(true)
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }
  closeDialog() {
    if (this.configDialog) {
      this.refDialog.close(false);
    } else {
      this.location.back();
    }
  }

  editPassword() {
    this.edit = false
    this.form.get('password')?.enable()
    this.form.controls['password'].setValue(null)
  }
}
