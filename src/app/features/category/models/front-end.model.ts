import { SelectItem } from "primeng/api/selectitem";
import { CONTROL_TYPE, DATA_TYPE, EXPORT_CSV, EXPORT_EXCEL } from "../utils/enums";

export interface IOnViewFunctionResponse {
  query: IChangeDBQuery;
  columns: IChangeDBColumn[];
  conditions: IChangeDBCondition[];
  columnType: any[];
  colCals: any[];
  colGroups: any[];
  buttons: any[];
  hasCheckBox: boolean;
  rows: number;
  page: number;
  datas: any[];
  totalRows: number;
  totalPages: number;
}

export interface IChangeDBQuery {
  id: IChangDBID;
  chartType?: string,
  isLarge?: string
  queryName: string;
  queryString: string;
  queryIndex: number;
  queryType: string;
  widgetName: string;
  gridName?: string;
}


export interface IChangeDBColumn {
  id: IChangDBID;
  columnName: string;
  columnIndex: number;
  isDisplay: number;
  columnStyle: string;
  chartIndex: string;
  columnNameL1?: string;
  columnNameL2?: string;
  formula?: string;
  fieldInDataModel: string;
}

export interface IChangDBID {
  functionCode: string;
  queryId: number;
  columnCode?: string;
  controlId?: number;
}

export interface IChangeDBCondition {
  condition: IChangDBConditionItem;
  listValueSearch: any[];
}

export interface IChangDBConditionItem {
  id: IChangDBID;
  columnCode: string;
  compareType: string;
  dataType: DATA_TYPE;
  controlLabel: string;
  databind: string;
  controlIndex: number;
  controlType: CONTROL_TYPE;
  isRequire: string;
  isDisplay: number;
  lstSelect?: SelectItem[];
  defaultValue?: any;
}


export interface IChangDBFrontEndSearchParams {
  queries: IChangeDBFrontEndSearchQueryItem[];
  conditions: object;
}

export interface IChangeDBFrontEndSearchQueryItem {
  "page": number,
  "rows": number,
  "queryId": number
}

export interface IExportExcelFileModel {
  content: string,
  filename: string,
  type: string
}

export type TypeExport =  typeof EXPORT_EXCEL | typeof EXPORT_CSV


