export interface IBackendModel {
  functionCode: string
  functionName: string
  functionTitle: string
  functionType: string,
  refreshInterval: number,
  moduleName: string,
  modules: string
}

export interface IConfigModel {
  id: {
    queryId?: number,
    functionCode: string,
  }
  columnCode: string,
  queryName: string,
  queryString: string,
  queryIndex: number,
  queryType: string,
  chartType: string,
  gridName: string,
  isLarge: string,
  sheetName: string,
  buttonUpdate: string,
  functionType: string,
  buttonAction: string,
  timestenSource: string,
  check: boolean
}

export interface IColumnModel {
  id: {
    functionCode: string,
    queryId: number,
    columnCode: string,
  },
  columnName: string,
  columnIndex: number,
  isDisplay: number,
  columnStyle: string,
  chartIndex: string,
  columnNameL1: string,
  columnNameL2: string,
  formula: string,
  fieldInDataModel: string
}

export interface IConditionsModel {
  id: {
    functionCode: string,
    queryId: number,
    controlId: number,
  },
  columnCode: string,
  compareType: string,
  dataType: string,
  controlLabel: string,
  databind: string,
  controlIndex: number,
  controlType: string,
  isRequire: string,
  isDisplay: number
}

export interface IEditConditionModel {
  queryId: number,
  functionCode: string,
  id: {
    functionCode: string,
    queryId: number,
    controlId: number,
  },
  columnCode: string,
  compareType: string,
  dataType: string,
  controlLabel: string,
  databind: string,
  dbId: number,
  controlIndex: number,
  controlType: string,
  isDisplay: number,
  isRequire: boolean,
  defaultValue: string
}

export interface IQueryModel {
  dbId: number,
  dbName: string,
  license: string,
  url: string,
  username: string,
  password: string,
  status: number,
  driver: string
}
