import { EChartsOption } from "echarts";

export interface FrontEndChartModel {

  chartInit($config: EChartsOption): void
}
