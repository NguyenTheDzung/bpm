export interface IDatabaseModel {
  dbId: number,
  dbName: string,
  license: string,
  url: string,
  status: number,
  driver: string,
  username: string,
  password: string
}
