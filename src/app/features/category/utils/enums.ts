export enum FunctionType {
  REPORT = 'report',
  REPORT_ONLY = 'reportOnly',
  CHART = 'chart',
  CATEGORY = 'category',
  CSV = 'csv',
}

export const CompareType = [
  {
    value: ">",
    name: ">"
  },
  {
    value: ">=",
    name: ">="
  },
  {
    value: "=",
    name: "="
  },
  {
    value: "<",
    name: "<"
  },
  {
    value: "<=",
    name: "<="
  },
  {
    value: "like",
    name: "like"
  }
]

export const DataType = [
  {
    value: "Date",
    name: "Date"
  },
  {
    value: "Number",
    name: "Number"
  },
  {
    value: "Text",
    name: "Text"
  },
  {
    value: "userLogin",
    name: "User Login"
  },
  {
    value: "roleLogin",
    name: "Role Login"
  }
] as const


export const CONTROL_TYPE = {
  TextArea: 'TextArea',
  TextBox: 'TextBox',
  SelectedBox: 'SelectedBox',
  MultiSelect: 'MultiSelect',
  DateTimePicker: 'DateTimePicker',
  DatePicker: 'DatePicker',
  MonthPicker: 'MonthPicker'
}as const
export type CONTROL_TYPE = Readonly<keyof typeof CONTROL_TYPE>

export enum DATA_TYPE {
  Float = 'Float',
  Integer = 'Integer',
  Number = 'Number',
  Date = 'Date',
  DateTime = 'DateTime',
  Text = 'Text'
}
export const ControlType: {value: CONTROL_TYPE, name: CONTROL_TYPE}[] = [
  {
    value: "TextArea",
    name: "TextArea"
  },
  {
    value: "TextBox",
    name: "TextBox"
  },
  {
    value: "SelectedBox",
    name: "SelectedBox"
  },
  {
    value: "MultiSelect",
    name: "MultiSelect"
  }
]
export const CONTROL_TYPE_DATE = [
  {
    value: "DateTimePicker",
    name: "DateTimePicker"
  },
  {
    value: "DatePicker",
    name: "DatePicker"
  }
]


export const DisplayIndex = [
  {
    value: 0,
    name: '0'
  },
  {
    value: 1,
    name: '1'
  },
  {
    value: 2,
    name: '2'
  }
] as const
export const Formula = [
  {
    value: 'SUM',
    name: 'SUM'
  },
  {
    value: 'AVERAGE',
    name: 'AVERAGE'
  },
  {
    value: 'MIN',
    name: 'MIN'
  },
  {
    value: 'MAX',
    name: 'MAX'
  }
] as const
export const ChartIndex = [
  {
    value: 'time',
    name: 'time'
  },
  {
    value: 'group',
    name: 'group'
  },
  {
    value: 'label',
    name: 'label'
  },
  {
    value: 'group|label',
    name: 'group|label'
  },
  {
    value: 'line',
    name: 'line'
  },
  {
    value: 'bar',
    name: 'bar'
  }
]
export const ColumnStyle = [
  {
    value: 'Float',
    name: 'Float'
  },
  {
    value: 'Integer',
    name: 'Integer'
  },
  {
    value: 'Number',
    name: 'Number'
  },
  {
    value: 'Date',
    name: 'Date'
  },
  {
    value: 'DateTime',
    name: 'DateTime'
  }
]

export const Large = [
  {
    value: '20',
    name: '20%'
  },
  {
    value: '33',
    name: '33%'
  },
  {
    value: '50',
    name: '50%'
  },
  {
    value: '67',
    name: '67%'
  },
  {
    value: '100',
    name: '100%'
  }
]

export const ChartType = [
  {
    value: 'bar',
    name: 'Bar Chart'
  },
  {
    value: 'pie',
    name: 'Pie Chart'
  },
  {
    value: 'line',
    name: 'Line Chart'
  },
  {
    value: 'StackedHorizontalBar',
    name: 'Stacked Horizontal Bar'
  },
  {
    value: 'StackedColumnChart',
    name: 'Stacked Column Chart'
  },
  {
    value: 'Card',
    name: 'Card'
  },
  {
    value: 'Table',
    name: 'Table'
  },
  {
    value: 'Process',
    name: 'Process'
  }
]

export const DATABASE_TYPE = [
  {
    value: 'org.postgresql.Driver',
    name: 'POSTGRESQL'
  },
  {
    value: 'oracle.jdbc.driver.OracleDriver',
    name: 'ORACLE'
  },
  {
    value: 'com.mysql.cj.jdbc.Driver',
    name: 'MYSQL'
  },
  {
    value: 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
    name: 'SQL SERVER'
  },
  {
    value: 'com.sap.db.jdbc.Driver',
    name: 'HANA SAP'
  }
]

export const XAXIS_COLUMN_NAME = "xaxis";
export const YAXIS_COLUMN_NAME = "yaxis";
export const SERIES_COLUMN_NAME = "series"
export const EXPORT_CSV = 'csv'
export const EXPORT_EXCEL = 'excel'
export const regexDefaultValueDateMonthDateTimePicker =  /(today)(?:\s*?([+-]\s*?\d+)|)/g
