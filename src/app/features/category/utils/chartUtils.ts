import { EChartsOption, PieSeriesOption } from "echarts";
import { IPieChartSeriesData } from "../../dashboard/interfaces/pie-chart-series-data.interface";

export class ChartUtils {

  public static readonly chartTextStyle = {
    fontFamily:
      '-apple-system, BlinkMacSystemFont,' +
      ' "Segoe UI", Roboto, "Helvetica Neue",' +
      ' Arial, sans-serif, "Apple Color Emoji",' +
      ' "Segoe UI Emoji", "Segoe UI Symbol"',
    fontSize: 13,
  };


  public static readonly PIE_CHART_CONFIG: EChartsOption = {
    tooltip: {
      trigger: 'item',
      textStyle: this.chartTextStyle,
    },
    legend: {
      show: false,
      top: '0%',
      left: 'center',
      padding: [10,20],
      textStyle: this.chartTextStyle,
    },
    series: {
      name: '',
      type: 'pie',
      radius: '50%',
      data: [],
      label: {
        formatter: '{d}%',
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  };


  public static initPieChartConfig<T = any>(
    config?: {
      data?: IPieChartSeriesData<T>[]
      [a: string]: any
    }
  ) {

    let pipeChartConfig: EChartsOption = { ...this.PIE_CHART_CONFIG };
    (pipeChartConfig.series as PieSeriesOption).data =
      (config?.data as PieSeriesOption["data"] ) ?? ([] as  PieSeriesOption["data"])
    delete config?.data
    pipeChartConfig = {...pipeChartConfig, ...config}
    return pipeChartConfig
  }

  public static readonly LINE_CONFIG = {
    tooltip: {
      trigger: 'item',
      textStyle: this.chartTextStyle,
    },
    textStyle: this.chartTextStyle,
    grid: {
      top: '3%',
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      data: [],
      axisLine: { onZero: true },
      // splitLine: { show: false },
      // splitArea: { show: false },
    },
    yAxis: {
      type: 'value',
      nameTextStyle: {
        color: '#000',
      },
      splitNumber: 10,
      axisLine: { onZero: true },
      // splitLine: { show: false },
      // splitArea: { show: false },
    },
    series: [],
  } as EChartsOption

  public static readonly BAR_CONFIG = {
    tooltip: {
      trigger: 'item',
      textStyle: this.chartTextStyle,
    },
    textStyle: this.chartTextStyle,
    xAxis: {
      type: 'category',
      data: [],
    },
    yAxis: {
      type: 'value',
      nameTextStyle: {
        color: '#000',
      },
      splitNumber: 10,
    },
    series: [],
  } as EChartsOption;


  public static readonly BAR_SERIES_ITEM_CONFIG = {
    type: 'bar',
    stack: 'total',
    label: {
      show: true,
      fontSize: 8,
      lineHeight: 20,
      color: '#fff',
    },
    emphasis: {
      focus: 'series',
    },
  };

  public static readonly LINE_SERIES_ITEM_CONFIG = {
    type: 'line',
    // stack: 'total',
    label: {
      show: true,
      fontSize: 8,
      lineHeight: 20,
      color: '#fff',
    },
    emphasis: {
      focus: 'series',
    },
  };

  public static initBarChart(
    config?:BarAndLineCustomConfig
  ){
    return {...this.BAR_CONFIG, ...config}
  }

  public static initLineChart(
    config?: BarAndLineCustomConfig
  ) {
    return { ...this.LINE_CONFIG, ...config }
  }
}

type SeriesConfig = {
  id?: string,
  name: string,
  color?: string,
  query?: string,  // Dùng cho translate
  data: (number | undefined) [] // Nếu không có dữ liệu trong ngày thì sẽ là undefined
}

export type BarAndLineCustomConfig = Partial<{
  series: any[],
  xAxis: any
  yAxis: any
  legend: any
  [p: string]: any
}>
