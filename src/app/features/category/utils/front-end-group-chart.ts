import { Directive, Input, OnChanges, SimpleChanges } from "@angular/core";
import { ECharts, EChartsOption, Payload } from "echarts";
import { BehaviorSubject } from "rxjs";
import { IChangeDBColumn } from "../models/front-end.model";
import { DataTable } from "@cores/models/data-table.model";

@Directive()
export abstract class FrontEndGroupChart implements OnChanges{
  /**
   *  ============== Variable ==================
   */

  private echarts$ = new BehaviorSubject<ECharts | null>(null);
  options: EChartsOption = this.initChart();

  @Input() data?: DataTable<any>;
  @Input() column?: IChangeDBColumn[] = [];
  @Input() showLegend = true
  @Input() title?: string

  get echarts() {
    return this.echarts$.getValue();
  }

  /*   ==============================================  */

  /**
   *  ============== Function ==================
   */

  ngOnChanges(changes: SimpleChanges) {
    if (["data", "column"].find(val => changes[val])) {
      const preOptions = this.updateChart(this.mapChartData(this.data?.content ?? [], this.column!));
      // this.setTitle(preOptions, this.title)
      preOptions!.title! = this.setTileOptions();
      this.options = preOptions
      this.echarts$.getValue()?.setOption(preOptions)
    }
  }

  // setTitle(options: EChartsOption = this.options, title: string = this.title ?? "", ) {
  //   if(!options) throw new Error('Options is falsy!')
  //   const opTitle = options.title!
  //   !Array.isArray(opTitle) && (opTitle.text = title ?? "");
  //   return opTitle
  // }

  onMouseOver(index: number) {
    if (!this.chartIsInit()) return;
    let payload: Payload = {
      type: "highlight",
      seriesIndex: index
    };
    this.echarts!.dispatchAction(payload);
  }

  onMouseLeave() {
    if (!this.chartIsInit()) return;
    let payload: Payload = {
      type: "highlight"
    };
    this.echarts!.dispatchAction(payload);
  }

  onToggle(name: string, index: number) {
    if (!this.chartIsInit) return;
    let payload: Payload = {
      type: "legendToggleSelect",
      name: name
    };
    this.echarts!.dispatchAction(payload);
  }

  findColumnNameInDataTable(columns: IChangeDBColumn[], name: string) {
    return columns.find(value => {
      return value.columnName == name;
    })?.fieldInDataModel;
  }

  chartInit($event: any) {
    this.echarts$.next($event);
  }

  chartIsInit() {
    return !!this.echarts;
  }


  /*=====================================================*/


  /**
   *  ============== Abstract Function ==================
   */

  abstract updateChart(data: any): EChartsOption

  abstract initChart(): EChartsOption

  abstract mapChartData(dataContent: any[], columns: NonNullable<typeof this.column>): any

  abstract setTileOptions(): object

  /*=====================================================*/

  log(...args: any[]){
    console.log(`ChartUtils :: ${Date.now()} :: `, args)
  }

}
