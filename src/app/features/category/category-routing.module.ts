import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {FunctionCode} from "@cores/utils/enums";
import {DatabaseComponent} from "./pages/database/database.component";
import {BackendComponent} from "./pages/backend/backend.component";
import {FrontendComponent} from "./pages/frontend/frontend.component";

const routes: Routes = [
  {
    path: 'database',
    component: DatabaseComponent,
    data: {
      functionCode: FunctionCode.DbCategory,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.DbCategory}`,
    },
  },
  {
    path: 'backend',
    component: BackendComponent,
    data: {
      functionCode: FunctionCode.BackEndDB,
      breadcrumb: `LAYOUTS.MENU.${FunctionCode.BackEndDB}`,
    }
  },
  {
    path: 'frontend',
    component: FrontendComponent
  },
  {
    path: 'frontend/:functionCode',
    component: FrontendComponent,
  },
  { path: '**', redirectTo: 'notfound', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryRoutingModule {}
