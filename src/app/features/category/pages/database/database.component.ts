import {Component, Injector, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {BaseTableComponent} from '@shared/components';
import {DatabaseService} from '../../services/database.service';
import {CreateDatabaseComponent} from '../../dialogs/create-database/create-database.component';
import {ScreenType} from '@cores/utils/enums';
import {IDatabaseModel} from '../../models/database.model';
import {createErrorMessage} from '@cores/utils/functions';
import {DATABASE_TYPE} from "../../utils/enums";

@Component({
  selector: 'app-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.scss'],
})
export class DatabaseComponent extends BaseTableComponent<any> implements OnInit {
  items!: MenuItem[];
  option: { code: string; value: number }[] = [];
  dbType = DATABASE_TYPE
  override params = {
    dbName: '',
    url: '',
    username: '',
    status: undefined,
  };

  constructor(inject: Injector, private service: DatabaseService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.service.getSelect().subscribe((res) => {
      this.option = res;
    });
    this.search(true);
  }

  override search(firstPage: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params = {
      ...this.params,
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
    this.service.getData(params).subscribe({
      next: (res) => {
        this.dataTable = res;
        if (this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  openDialog(check: boolean, item?: IDatabaseModel) {
    this.dialogService
      ?.open(CreateDatabaseComponent, {
        showHeader: false,
        width: '50%',
        data: {
          screenType: ScreenType.Create,
          state: check ? item : false,
        },
      })
      .onClose.subscribe((result) => {
      if (result) {
        this.search(false);
      }
    });
  }

  handleDelete(id: number) {
    this.messageService.confirm().subscribe((confirm) => {
      if (confirm) {
        this.loadingService.start();
        this.service.deleteItem(id).subscribe({
          next: () => {
            this.search(false);
          },
          error: (err) => {
            this.messageService.error(createErrorMessage(err));
            this.loadingService.complete();
          },
        });
      }
    });
  }

  refresh() {
    this.params = {
      dbName: '',
      url: '',
      username: '',
      status: undefined,
    };
    this.search(true);
  }

  filterTableStatus($event: any) {
    this.params.status = $event.value;
    this.search(true);
  }

  testConnect(dbId: any) {
    this.loadingService.start();
    this.service.testConnect(dbId).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.TEST_CONNECT_SUCCESS');
        this.loadingService.complete()
      },
      error: (err) => {
        if (err.error.message === 'TEST_CONNECT_FAIL') {
          this.messageService.error('MESSAGE.TEST_CONNECT_FAIL');
        } else {
          this.messageService.error(createErrorMessage(err));
        }
        this.loadingService.complete();
      }
    })
  }
}
