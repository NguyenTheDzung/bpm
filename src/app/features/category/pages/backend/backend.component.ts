import {Component, Injector, OnInit} from '@angular/core';
import {BaseTableComponent} from '@shared/components';
import {AddReportComponent} from '../../dialogs/add-report/add-report.component';
import {BackendService} from '../../services/backend.service';
import {ScreenType} from '@cores/utils/enums';
import {ConfigQueryComponent} from '../../dialogs/config-query/config-query.component';
import {HandleExcelComponent} from '../../dialogs/handle-excel/handle-excel.component';
import {createErrorMessage, exportFile} from '@cores/utils/functions';
import {CommonModel} from '@common-category/models/common-category.model';
import {IBackendModel} from '../../models/backend.model';
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-backend',
  templateUrl: './backend.component.html',
  styleUrls: ['./backend.component.scss'],
})
export class BackendComponent extends BaseTableComponent<any> implements OnInit {
  filterOpts?: CommonModel[];
  systemCategory?: CommonModel[];

  override params = {
    functionCode: '',
    modules: '',
  };
  selectBackend?: IBackendModel;


  constructor(inject: Injector, private service: BackendService) {
    super(inject, service);
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.search(true);
    const functionType = this.service.getFunctionType();
    const systemCategory = this.service.getSystemCategory();
    forkJoin([functionType, systemCategory]).subscribe({
      next: (res) => {
        this.filterOpts = res[0];
        this.systemCategory = res[1]
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  handleDelete(item: IBackendModel) {
    this.messageService.confirm().subscribe((confirm) => {
      if (confirm) {
        this.loadingService.start();
        this.service.deleteItem(item).subscribe({
          next: () => {
            this.search(false);
          },
          error: (err) => {
            this.messageService.error(createErrorMessage(err));
            this.loadingService.complete();
          },
        });
      }
    });
  }

  override search(firstPage: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params = {
      ...this.params,
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
    };
    this.service.getData(params).subscribe({
      next: (res) => {
        this.dataTable = res;
        this.dataTable.content = this.dataTable.content.map(
          value => ({...value, modules: String(value.modules)})
        )
        if (this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  configQuery(state: IBackendModel) {
    this.dialogService?.open(ConfigQueryComponent, {
      showHeader: false,
      width: '70%',
      data: {
        screenType: ScreenType.Create,
        state: state,
      },
    });
  }

  openDialogCreate(check: boolean, item?: IBackendModel) {
    this.dialogService
      ?.open(AddReportComponent, {
        showHeader: false,
        width: '50%',
        data: {
          screenType: ScreenType.Create,
          state: check ? item : false,
        },
      })
      .onClose.subscribe((result) => {
        if (result) {
          this.search(false);
        }
      });
  }

  openHandleExcel(check: boolean) {
    this.dialogService
      .open(HandleExcelComponent, {
        showHeader: false,
        width: '50%',
        data: {
          screenType: ScreenType.Create,
          state: check,
        },
      })
      .onClose.subscribe((result) => {
        if (result) {
          this.search(true);
        }
      });
  }

  exportFile(functionCode: string) {
    this.loadingService.start()
    this.service.exportBackEnd(functionCode).subscribe({
      next: (res: any) => {
        exportFile(functionCode, res?.data?.content, 'application/xml')
        this.loadingService.complete()
        this.messageService.success('MESSAGE.SUCCESS')
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }

  refresh() {
    this.params = {
      functionCode: '',
      modules: '',
    };
    this.search(true);
  }

  doDetail(item: IBackendModel) {
    this.selectBackend = item
  }
}
