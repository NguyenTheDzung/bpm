import {BackendComponent} from "./backend/backend.component";
import {DatabaseComponent} from "./database/database.component";
import {FrontendComponent} from "./frontend/frontend.component";

export const Pages = [BackendComponent, DatabaseComponent, FrontendComponent]
