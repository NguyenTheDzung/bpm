import { Component, Injector } from "@angular/core";
import { BaseComponent } from "@shared/components";

@Component({
  selector: "app-frontend",
  template: `
    <app-frontend-group [functionCode]="functionCode"></app-frontend-group>`
})
export class FrontendComponent extends BaseComponent {
  functionCode =  this.route.snapshot.paramMap.get("functionCode");
  constructor(
    injector: Injector
  ) {
    super(injector);
    this.route.paramMap.subscribe(
      (params) => {
        this.functionCode = params.get("functionCode");
      }
    )
  }

}
