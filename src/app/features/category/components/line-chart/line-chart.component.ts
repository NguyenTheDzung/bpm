import { Component, OnChanges } from "@angular/core";
import { BarAndLineCustomConfig, ChartUtils } from "../../utils/chartUtils";
import { IChangeDBColumn } from "../../models/front-end.model";
import { uniq } from "lodash";
import { SERIES_COLUMN_NAME, XAXIS_COLUMN_NAME, YAXIS_COLUMN_NAME } from "../../utils/enums";
import { FrontEndGroupChart } from "../../utils/front-end-group-chart";
import { EChartsOption } from "echarts";

@Component({
  selector: "app-line-chart",
  templateUrl: '../../utils/chart-template.html',
  styles: []
})
export class LineChartComponent extends FrontEndGroupChart implements OnChanges {

  mapChartData(dataContent: any[], columns: IChangeDBColumn[]): BarAndLineCustomConfig {
    const nameField = this.findColumnNameInDataTable(columns, XAXIS_COLUMN_NAME);
    const dataField = this.findColumnNameInDataTable(columns, YAXIS_COLUMN_NAME);
    const seriesField = this.findColumnNameInDataTable(columns, SERIES_COLUMN_NAME);
    if (!nameField || !dataField || !seriesField) {
      throw new Error("lỗi không tìm thấy cột xaxis || yaxis || series");
    }
    const labels = this.getLabel(dataContent, nameField);
    const series: { name: string, data: number[] }[] =
      this.getSeries(dataContent, seriesField, dataField, nameField);
    const seriesMapped: any[] = series.map(
      item => ({...ChartUtils.LINE_SERIES_ITEM_CONFIG,...item})
    );
    const legendData = seriesMapped.map(series => series.name);
    let legendSelected: {[p:string]: boolean} = {}
    seriesMapped.forEach(series => legendSelected[series.name] =  true)
    return {
      series: seriesMapped! ,
      legend: {
        data: legendData,
        show: false,
        selected: legendSelected,
      },
      xAxis: {
        type: "category",
        data: labels
      }
    }
  }

  getLabel(dataContent: any[], labelField: string) {
    const listLabel: string[] = (dataContent ?? []).map(value => {
      return value[labelField];
    });
    return uniq(listLabel);
  }

  getSeries(dataContent: any[], fieldSeries: string, fieldValue: string, fieldLabel: string): { name: string, data: number[] }[] {

    const series = new Map<string, {name: string, data: number[]}>();
    const nameMap =  new Map<string, number[]>();
    (this.data?.content ?? []).forEach((item: any) => {

      const seriesName = item[fieldSeries]
      const label = item[fieldLabel]

      if(!series.has(seriesName) && !!seriesName){
        series.set(seriesName, {name: seriesName, data: []})
      }

      if(!nameMap.has(label)){
        nameMap.set(label, [])
      }
    })

    for (let [key, value] of series) {
      const data = this.getSingleSeriesData(
        value.name,
        Array.from(nameMap.keys()),
        dataContent,
        fieldLabel,
        fieldSeries,
        fieldValue
      )
      series.set(key, { name: value.name, data });
    }

    return Array.from(series.values())
  }

  getSingleSeriesData(
    seriesName: string,
    labels: string[],
    dataContent: any[] = this.data?.content?? [],
    fieldLabel: string,
    fieldNameSeries: string,
    fieldValue: string
  ): number[] {

    const filterData = dataContent.filter(
      data => data[fieldNameSeries] == seriesName
    )

    return labels.map((label) => {
      const data = filterData.filter(data => data[fieldLabel] == label)
      if (!data.length) {
        return 0
      }
      return data.reduce((previousValue, currentValue) => previousValue + +currentValue[fieldValue], 0)
    })
  }
  updateChart(data: any): EChartsOption {
    return ChartUtils.initLineChart(data)
  }

  initChart() {
    return ChartUtils.initLineChart()
  }

  override setTileOptions(): object {
    return {}
  }


}
