import {Component, Injector, Input, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {BackendService} from "../../services/backend.service";
import {DialogService} from "primeng/dynamicdialog";
import {createErrorMessage} from "@cores/utils/functions";
import {ChartIndex, ColumnStyle, DisplayIndex, Formula} from "../../utils/enums";
import {IColumnModel} from "../../models/backend.model";

@Component({
  selector: 'app-columns',
  templateUrl: './columns.component.html',
  styleUrls: ['./columns.component.scss']
})
export class ColumnsComponent extends BaseActionComponent implements OnInit {
  @Input() dataColumn!: IColumnModel[]
  displayIndex = DisplayIndex
  columnStyle = ColumnStyle
  chartIndex = ChartIndex
  formula = Formula

  constructor(injector: Injector,
              private service: BackendService,
              private dialogService: DialogService) {
    super(injector, service)
  }

  ngOnInit(): void {

  }

  saveData() {
    // Array.isArray(this.dataColumn) && this.dataColumn.forEach((item: any) => {
    //
    // })
    this.loadingService.start();
    this.service.editColumn(this.dataColumn).subscribe({
      next: (res: any) => {
        this.dataColumn = res?.data
        this.messageService.success('MESSAGE.SUCCESS');
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      }
    })
  }
}
