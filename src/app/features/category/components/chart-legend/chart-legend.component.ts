import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { ECharts, EChartsOption, SeriesOption } from "echarts";

@Component({
  selector: "app-chart-legend",
  template: `
    <div class="legend-container gap-2">
      <ng-container *ngFor="let name of names, let index=index, trackBy: trackByItem">
        <div class="cursor-pointer legend-item"
             [style.font-size]="fontSize + 'px'"
             (mouseover)="onOver(index)"
             (click)="toggle(index, name, $event)"
             (mouseleave)="onLeave()"
             [style.color]="'#333'"
             [style.font-family]=""
             [class.disabled]="!legendSelected[name]">
          <svg width="24" height="16" viewBox="0 0 24 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              [attr.fill]="colors[index]"
              d="M0 2C0 0.895431 0.948103 0 2.11765 0H21.8824C23.0519 0 24 0.895431 24 2V14C24 15.1046 23.0519 16 21.8824 16H2.11765C0.948104 16 0 15.1046 0 14V2Z" />
          </svg>
         <div class="ml-2">{{name}}</div>
        </div>
      </ng-container>
    </div>
  `,
  styleUrls: ["./chart-legend.component.scss"]
})
export class ChartLegendComponent implements OnChanges {
  @Input() debug: string = ""
  @Input() options: any
  @Input() echarts?: ECharts
  @Input() isPie = false

  series: SeriesOption[] = []
  names: string[] = []
  legendSelected: {[a: string]: any} = {}
  colors: string[] = []

  fontSize: string = "14";
  fontColor: string = "#333"
  textStyle = {}
  fontFamily = "san-serif"

  @Output() onMouseOver = new EventEmitter<number>();
  @Output() onToggle = new EventEmitter<{ name: any, index: number }>();
  @Output() onMouseLeave = new EventEmitter<void>();

  ngOnChanges(changes: SimpleChanges) {
    if(changes['options'] && !!changes['options'].currentValue){
      const options = changes['options'].currentValue as EChartsOption
      const legend  = ((options?.legend as any[])[0])
      this.names = legend.data
      this.legendSelected = legend['selected']
      // @ts-ignore
      this.colors = (options.color as string[])
      //@ts-ignore
      const selectorLabel  = (options.legend as EChartsOption["legend"])[0]?.selectorLabel
      this.fontSize = selectorLabel.fontSize
      this.fontFamily = selectorLabel.fontFamily
    }
  }

  protected readonly alert = alert;

  toggle(index: number, name: string,  $event: Event) {
    $event.preventDefault()
    this.onToggle.emit({name, index})
  }

  onOver(index: number) {
    this.onMouseOver.emit(index)
  }

  onLeave() {
    this.onMouseLeave.emit()
  }

  trackByItem(index: number, value: any) {
    return value
  }

  log(...args: any){
    console.log(`CHART_LEGEND :: ${this.debug} :: `, args)
  }
}
