import { Component, EventEmitter, Input, Output } from "@angular/core";
import { IChangeDBColumn, IChangeDBFrontEndSearchQueryItem, IChangeDBQuery } from "../../models/front-end.model";
import { DataTable } from "@cores/models/data-table.model";

@Component({
  selector: "app-panel-result",
  templateUrl: "./panel-result.component.html",
  styleUrls: ["./panel-result.component.scss"],
})
export class PanelResultComponent {
  dataTable: DataTable<object> = this.initDataTable()
  @Input() columns: IChangeDBColumn[] = [];
  @Input() query?: IChangeDBQuery;
  @Input() isHadRequireResult: boolean = true
  @Input() refreshInterval?: number
  @Output() lazyLoadParams = new EventEmitter<IChangeDBFrontEndSearchQueryItem>()

  @Input() set searchDataTable(value: DataTable<any> | null) {
    if(!value){
      this.dataTable = this.initDataTable()
      return
    }
    if(value. content.length) {
      if(!!value.currentPage){
        value.currentPage = value.currentPage - 1
        value.first = value.first - value.size
      }
      this.dataTable = value
    }else this.dataTable = this.initDataTable()
  }

  pageChange(paginator: any) {
    this.dataTable.currentPage = paginator.first! / paginator.rows!;
    this.dataTable.size = paginator.rows!;
    this.dataTable.first = paginator.first!;
    this.lazyLoadParams.emit(this.mapDataSearch())
  }

  mapDataSearch(): IChangeDBFrontEndSearchQueryItem {
    return {
      page: this.dataTable.currentPage,
      rows: this.dataTable.size,
      queryId: this.query?.id.queryId!
    };
  }

  initDataTable(){
    return <DataTable<any>>{
      content: [],
      currentPage: 0,
      size: 10,
      totalElements: 0,
      totalPages: 0,
      first: 0,
      numberOfElements: 0
    };
  }
}
