import {
  Component,
  ElementRef,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from "@angular/core";
import { MenuItem } from "primeng/api";
import { SelectItem } from "primeng/api/selectitem";
import { TPageableSearchParams } from "@cores/models/response.model";
import { BackendService } from "../../services/backend.service";
import { FrontEndService } from "../../services/front-end.service";
import {
  IChangDBFrontEndSearchParams,
  IChangeDBCondition,
  IChangeDBFrontEndSearchQueryItem,
  IOnViewFunctionResponse
} from "../../models/front-end.model";
import { BaseComponent } from "@shared/components";
import { finalize, interval, Observable, Observer, Subscription, tap } from "rxjs";
import { IBackendModel } from "../../models/backend.model";
import { FunctionType } from "../../utils/enums";
import { DataTable } from "@cores/models/data-table.model";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import * as moment from "moment/moment";

@Component({
  selector: "app-frontend-group",
  templateUrl: "./front-end-group.component.html",
  styleUrls: ["./front-end-group.component.scss"]
})
export class FrontEndGroupComponent extends BaseComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild("chartView") chardView?: ElementRef<HTMLDivElement>;

  readonly FUNCTION_TYPE = FunctionType;

  @Input() functionCode: string | null = null;

  items: MenuItem[] = [];
  selectedFunction?: IBackendModel;
  functions: SelectItem<IBackendModel>[] = [];
  conditions: IChangeDBCondition[] = [];
  functionDataList: IOnViewFunctionResponse[] = [];
  onlyDisplayResult = false;
  dataTables: DataTable[] = [];
  isHadRequireResult = false;
  searchParams: object = {};
  requestQueries: IChangeDBFrontEndSearchQueryItem[] = [];
  intervalSubscription?: Subscription;

  constructor(
    private backendService: BackendService,
    private frontEndService: FrontEndService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    if(!this.functionCode) this.getItemData();
  }

  ngOnChanges(changes: SimpleChanges) {
    const functionCode = changes["functionCode"];
    if (functionCode && functionCode.currentValue) {
      this.getItemData();
    }
  }

  getItemData() {
    const request: TPageableSearchParams<{}> = {
      page: 0,
      size: undefined!
    };
    this.backendService.getData(request).subscribe(
      {
        next: (response) => {
          this.functions = response.content.map(
            backendItem => {
              return {
                label: backendItem.functionName + " - " + backendItem.functionTitle,
                value: backendItem
              } as SelectItem;
            }
          );

          const functionExistInList = this.functions.find((val) => {
            return val.value.functionCode == this.functionCode;
          })!;
          if (this.functionCode && functionExistInList) {
            this.onlyDisplayResult = true;
            this.selectedFunction = functionExistInList.value;
          } else {
            this.selectedFunction = this.functions[0].value;
          }
          this.onChangeSelectFunction();

        }
      }
    );
  }

  onChangeSelectFunction() {
    /**
     * Reset dữ liệu các bảng
     */
    this.dataTables = [];
    this.functionDataList = [];

    this.unsubscribeOnSearch();
    this.getDetailFunction();
  }

  getDetailFunction() {
    this.loadingService.start();
    if (!this.selectedFunction) return;
    this.frontEndService.postOnViewFunction(this.selectedFunction!).pipe(
      finalize(() => this.loadingService.complete())
    )
      .subscribe(
        (response) => {
          this.conditions = response[0]?.conditions ?? [];
          this.functionDataList = response;
          this.requestQueries = response.map(functionData => {
            return <IChangeDBFrontEndSearchQueryItem>{
              page: 1,
              rows: 10,
              queryId: functionData.query.id.queryId
            };
          });
        }
      );
  }

  setRefreshInterval() {
    const timeInterval = this.selectedFunction?.refreshInterval;

    if (timeInterval == null || (timeInterval <= 0 || !Number.isFinite(timeInterval))) return;

    this.unsubscribeOnSearch();
    this.intervalSubscription = interval((timeInterval as number) * 1000).subscribe(
      () => {
        this.subscription = this.onCallAPIGetData(this.searchParams, false)
          .subscribe({
            next: (response) => {
              if (!response.length || response.every(val => !val.content.length)) {
                this.unsubscribeOnSearch();
              }
            },
            error: () => this.unsubscribeOnSearch()
          });
      }
    );
  }

  unsubscribeOnSearch() {
    this.intervalSubscription && this.intervalSubscription.unsubscribe();
    this.subscription && this.subscription.unsubscribe();
  }

  exportData(exportRequestData: object = {}) {
    // @ts-ignore
    const type = exportRequestData["type"]!;
    // @ts-ignore
    delete exportRequestData["type"];
    this.frontEndService.exportXML(this.selectedFunction?.functionCode!, exportRequestData, type)
      .subscribe(
        (response) => {
          const { filename, content } = response.data;
          const fileType = response.data.type.replace(".", "");
          const fileLink = document.createElement("a");
          const arrayBuffer = new Uint8Array([...window.atob(content)].map((char) => char.charCodeAt(0)));
          fileLink.href = window.URL.createObjectURL(new File([arrayBuffer], filename, { type: `application/${fileType}` }));
          fileLink.download = filename;
          fileLink.click();
          fileLink.remove();
        }
      );
  }

  executeFunction(searchParams: object) {
    this.frontEndService.execute(this.selectedFunction?.functionCode!, searchParams).subscribe();
  }

  onCallAPIGetData(paramsSearch: object, loading = true): Observable<DataTable[]> {
    const params = this.mapDataSearch(paramsSearch);
    this.searchParams = paramsSearch;
    let ob;
    if (this.selectedFunction?.functionType! === this.FUNCTION_TYPE.CHART) {
      ob = this.frontEndService.getChartData(params, this.selectedFunction?.functionCode!, loading);
    } else {
      ob = this.frontEndService.getSearchData(params, this.selectedFunction?.functionCode!, loading);
    }
    return ob.pipe(
      tap((value) => {
        this.dataTables = value;
      })
    );
  }

  refreshIntervalObserve(): Partial<Observer<any>> {
    return {
      next: () => this.setRefreshInterval(),
      error: () => this.unsubscribeOnSearch()
    };
  }

  mapDataSearch(searchParams?: object): IChangDBFrontEndSearchParams {
    return {
      conditions: searchParams ?? {},
      queries: this.requestQueries
    };
  }

  onSearch($event: object) {
    const observe = this.refreshIntervalObserve();
    this.subscription = this.onCallAPIGetData($event).subscribe(observe);
  }

  onLoadParams($event: IChangeDBFrontEndSearchQueryItem, index: number) {
    /**
     * Backend làm custom pageable chạy page bắt đầu từ đầu
     */
    this.requestQueries[index].page = $event.page + 1;
    this.requestQueries[index].rows = $event.rows;
    this.onSearch(this.searchParams);
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.unsubscribeOnSearch();
  }

  trackByResult(index: number, value: IOnViewFunctionResponse) {
    return value.query.id.queryId;
  }

  exportChart() {
    console.log(this.chardView);
    if (!this.chardView) throw new Error("chartView is invalid!");
    const el = document.querySelector("#chart_export_location")!;

    html2canvas(<HTMLElement>el, {
      scale: 1,
      scrollX: -window.scrollX,
      scrollY: -window.scrollY,
      windowWidth: document.documentElement.offsetWidth,
      windowHeight: document.documentElement.offsetHeight,
      onclone: (document, element) => {
        Array.from(<NodeListOf<HTMLElement>>element.querySelectorAll(".chart-card")).forEach(
          (value) => {
            value.style.boxShadow = "none";
          }
        );
        element.classList.remove("bg-white");
        element.style.backgroundColor = "#eceff4";
      }
    }).then((canvas) => {
      const image = canvas.toDataURL("image/png", 1.0);
      const doc = new jsPDF("p", "px", "a4");
      const pageWidth = doc.internal.pageSize.getWidth();
      const pageHeight = doc.internal.pageSize.getHeight();

      const widthRatio = pageWidth / canvas.width;
      const heightRatio = pageHeight / canvas.height;
      const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

      const canvasWidth = canvas.width * ratio;
      const canvasHeight = canvas.height * ratio;

      const marginX = (pageWidth - canvasWidth) / 2;
      // const marginY = (pageHeight - canvasHeight) / 2;

      doc.addImage(image, "PNG", marginX, 0, canvasWidth, canvasHeight);
      doc.save(`bieu-do-thong-ke-${moment().format("DDMMYYYYHHmmss")}.pdf`);

    });
  }

}
