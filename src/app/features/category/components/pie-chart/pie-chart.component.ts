import { Component } from "@angular/core";
import { ChartUtils } from "../../utils/chartUtils";
import { IPieChartSeriesData } from "../../../dashboard/interfaces/pie-chart-series-data.interface";
import { IChangeDBColumn } from "../../models/front-end.model";
import { XAXIS_COLUMN_NAME, YAXIS_COLUMN_NAME } from "../../utils/enums";
import { FrontEndGroupChart } from "../../utils/front-end-group-chart";

@Component({
  selector: "app-pie-chart",
  templateUrl: '../../utils/chart-template.html'
})
export class PieChartComponent extends FrontEndGroupChart {

  mapChartData(value: any[], columns: IChangeDBColumn[]) {
    const fieldName = this.findColumnNameInDataTable(columns, XAXIS_COLUMN_NAME);
    const fieldValue = this.findColumnNameInDataTable(columns, YAXIS_COLUMN_NAME);

    if (!fieldName || !fieldValue) {
      throw new Error("lỗi không tìm thấy 2 cột xaxis và yaxis");
    }
    const rs: IPieChartSeriesData<any>[] = [];

    value.forEach(
      (contentItem, index) => {
        if (rs.find(rsItem => rsItem.name == contentItem[fieldName])) {
          return;
        } else {
          const pieData: IPieChartSeriesData<any> = {
            id: index,
            value: value
              .filter(p => p[fieldName] == contentItem[fieldName])
              .reduce((previousValue, currentValue) => (previousValue + +currentValue[fieldValue]), 0),
            name: contentItem[fieldName],
            itemStyle: {}
          };
          rs.push(pieData);
        }
      }
    );
    const selectedLegend: {[a: string]: boolean} = {}
    const legendData =  rs.map(value => {
      selectedLegend[value.name] = true
      return value.name;
    })

    return {
      legend: {
        data: legendData,
        selected: selectedLegend,
        show: false
      },
      data: rs
    }
  }

  updateChart(data: any) {
    return ChartUtils.initPieChartConfig(data);
  }

  initChart() {
    return ChartUtils.initPieChartConfig();
  }

  setTileOptions(): object {
    return {}
  }
}
