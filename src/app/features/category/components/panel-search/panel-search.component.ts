import { Component, EventEmitter, Injector, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import {
  CONTROL_TYPE,
  DATA_TYPE,
  EXPORT_CSV,
  EXPORT_EXCEL,
  regexDefaultValueDateMonthDateTimePicker
} from "../../utils/enums";
import { IChangeDBCondition, TypeExport } from "../../models/front-end.model";
import { FormGroup, Validators } from "@angular/forms";
import { BaseComponent } from "@shared/components";
import { CalendarView, DateFormat } from "@cores/utils/enums";
import * as moment from "moment";
import { debounceTime, distinctUntilChanged, Subscription } from "rxjs";

@Component({
  selector: "app-panel-search",
  templateUrl: "./panel-search.component.html",
  styleUrls: ["./panel-search.component.scss"]
})
export class PanelSearchComponent extends BaseComponent implements OnChanges {

  @Input() model: IChangeDBCondition[] = [];
  @Output() onExport = new EventEmitter<object & TypeExport>();
  @Output() onExportChart = new EventEmitter<void>();
  @Output() onExecute = new EventEmitter<object>();
  @Output() onSearch = new EventEmitter<object>();

  @Input() isCategoryType: boolean = false;
  @Input() isChartType: boolean = false;
  @Input() loadOnInit: boolean = true
  @Input() loadOnChange: boolean = false

  onChangeSubscription?: Subscription

  readonly DateFormat = DateFormat;
  readonly CalendarView = CalendarView;
  readonly CONTROL_TYPE = CONTROL_TYPE;
  readonly COLUMN_STYLE = DATA_TYPE;

  form: FormGroup = this.fb.group({});
  readonly IGNORE_DATA_TYPE: string = "userLogin";

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!!changes["model"] && changes["model"].currentValue) {
      this.onChangeSubscription?.unsubscribe()
      const modelChanged = (changes["model"].currentValue ?? []) as typeof this.model;
      const controls: { [key: string]: any } = {};
      modelChanged.forEach(
        model => {
          controls[model.condition.columnCode] = [
            this.getDefaultValue(model),
            model.condition.isRequire ? [Validators.required] : []
          ];
        }
      );
      this.form = this.fb.group(controls);
      if(this.form.valid && this.loadOnInit) this.search()
      this.checkOnChange()
    }
    if(changes['loadOnChange']){
      const loadOnChange = changes['loadOnChange'].currentValue as boolean
      this.checkOnChange(loadOnChange)
    }
  }

  checkOnChange(loadOnChange = this.loadOnChange) {
    this.onChangeSubscription?.unsubscribe()
    if(loadOnChange) {
      this.form.valueChanges
        .pipe(
          debounceTime(500),
          distinctUntilChanged()
        )
        .subscribe(
          (value) => {
            this.reformingSearch()
          }
        )
    }
  }


  reformingSearch() {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    this.search()
  }

  search() {
    this.onSearch.emit(this.mapSearchValue());
  }

  mapSearchValue(typeExport?: TypeExport) {
    const result = this.form.value;
    this.model.forEach(
      val => {
        if (
          (<string><unknown>[
            CONTROL_TYPE.DateTimePicker,
            CONTROL_TYPE.DatePicker,
            CONTROL_TYPE.MonthPicker
          ]).includes(val.condition.controlType)
        ) {
          result[val.condition.columnCode] = this.getInputDateValue(
            val.condition.controlType,
            val.condition.dataType,
            result[val.condition.columnCode]
          );
        }
      }
    );

    if (typeExport) result["type"] = typeExport;
    return result;
  }

  getInputDateValue(ControlType: CONTROL_TYPE, dateType: DATA_TYPE, value: Date) {
    let mapValue = moment(value)
    let format = ""

    switch (ControlType) {
      case CONTROL_TYPE.MonthPicker:
        mapValue = moment(value).startOf('month')
        format = "MM/YYYY"
        break;
      case CONTROL_TYPE.DatePicker:
        mapValue = moment(value).startOf('day')
        format = "DD/MM/YYYY"
        break;
      case CONTROL_TYPE.DateTimePicker:
        mapValue = moment(value)
        format = "DD/MM/YYYY HH:mm:ss"
        break;
      default:
        throw new Error("Invalid Control type")
    }

    switch (dateType) {
      case DATA_TYPE.Date:
        return mapValue.toISOString()
      case DATA_TYPE.DateTime:
        return mapValue.toISOString()
      case DATA_TYPE.Text:
        return mapValue.format(format);
      case DATA_TYPE.Number:
        return value.getTime();
      case DATA_TYPE.Float:
        return value.getTime();
      case DATA_TYPE.Integer:
        return value.getTime();
    }
  }

  reset() {
    this.form.reset();
    if(this.form.valid) this.search()
  }

  exportExcel() {
    if (this.isValidForm()) this.onExport.emit(this.mapSearchValue(EXPORT_EXCEL));
  }

  exportCSV() {
    if (this.isValidForm()) this.onExport.emit(this.mapSearchValue(EXPORT_CSV));
  }

  isValidForm() {
    this.form.markAllAsTouched();
    return this.form.valid;
  }

  executeSearch() {
    this.form.markAllAsTouched();
    if (this.form.invalid) return;
    this.onExecute.emit(this.mapSearchValue());
  }

  exportChart() {
    this.onExportChart.emit();
  }

  private getDefaultValue(model: IChangeDBCondition): any | null {
    if(!model.condition.defaultValue) return null
    if(
      model.condition.controlType  === CONTROL_TYPE.MonthPicker ||
      model.condition.controlType  === CONTROL_TYPE.DateTimePicker ||
      model.condition.controlType  === CONTROL_TYPE.DatePicker
    ) {
      const arrValue : string[] =
        (model.condition.defaultValue as string)
          .replace(regexDefaultValueDateMonthDateTimePicker, '$1|$2')
          .split('|')

      if(!arrValue.length) throw new Error("Truyền default value cho date picker không đúng !")
      let result = moment()
      if(arrValue[1]) result?.add(+(arrValue[1].replace(/\s/g, '')),"day")
      return result.toDate()
    }
    return model?.condition?.defaultValue ?? null;
  }
}
