import {Component, Injector, OnInit} from '@angular/core';
import {BaseActionComponent} from "@shared/components";
import {BackendService} from "../../services/backend.service";
import {DialogService} from "primeng/dynamicdialog";
import {ScreenType} from "@cores/utils/enums";
import {AddColGroupComponent} from "../../dialogs/add-col-group/add-col-group.component";

@Component({
  selector: 'app-columns-group',
  templateUrl: './columns-group.component.html',
  styleUrls: ['./columns-group.component.scss']
})
export class ColumnsGroupComponent extends BaseActionComponent implements OnInit {

  constructor(injector: Injector, private service: BackendService, private dialogService: DialogService) {
    super(injector, service)
  }

  ngOnInit(): void {
  }

  openDialogAdd() {
    this.dialogService.open(
      AddColGroupComponent,
      {
        showHeader: false,
        width: '50%',
        data: {
          screenType: ScreenType.Create
        }
      }
    )
  }
}
