import { ColumnsComponent } from "./columns/columns.component";
import { ColumnsGroupComponent } from "./columns-group/columns-group.component";
import { ConditionsComponent } from "./conditions/conditions.component";
import { PanelSearchComponent } from "./panel-search/panel-search.component";
import { PanelResultComponent } from "./panel-result/panel-result.component";
import { FrontEndGroupComponent } from "./front-end-group/front-end-group.component";
import { PieChartComponent } from "./pie-chart/pie-chart.component";
import { LineChartComponent } from "./line-chart/line-chart.component";
import { BarChartComponent } from "./bar-chart/bar-chart.component";
import { ChartLegendComponent } from "./chart-legend/chart-legend.component";
import { FeCardsComponent } from "./fe-cards/fe-cards.component";
import { FeTableComponent } from "./fe-table/fe-table.component";

export const Components = [
  ColumnsComponent,
  ColumnsGroupComponent,
  ConditionsComponent,
  PanelSearchComponent,
  PanelResultComponent,
  FrontEndGroupComponent,
  PieChartComponent,
  LineChartComponent,
  BarChartComponent,
  ChartLegendComponent,
  FeCardsComponent,
  FeTableComponent
]
