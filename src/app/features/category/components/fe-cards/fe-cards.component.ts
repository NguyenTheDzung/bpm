import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";
import {DataTable} from "@cores/models/data-table.model";
import {IChangeDBColumn} from "../../models/front-end.model";
import {SERIES_COLUMN_NAME, XAXIS_COLUMN_NAME, YAXIS_COLUMN_NAME} from "../../utils/enums";

@Component({
  selector: "app-fe-cards",
  styleUrls: ["./fe-cards.component.scss"],
  template: `
    <div class="col-12 grid mx-0 p-0">
      <ng-container *ngFor="let itemCard of data?.content ?? [] let index = index trackBy: trackByTabs">
        <div [ngStyle]="{
                    'flex-basis': isLarge + '%' ,
                    'max-width': isLarge + '%',
                    'flex-grow': 1,
                    'flex-shink': 1
                  }"
             class="col"
        >
        <ng-container *ngTemplateOutlet="card; context: {$implicit: itemCard}"></ng-container>
        </div>
      </ng-container>
    </div>

    <ng-template #card let-value>
      <div class="card chart-card">
        <div class="overview-item_container cursor-pointer">
          <div class="overview-item_left flex justify-content-between flex-column">
            <div class="overview-item_title">
              {{labelField ? (value?.[labelField] ?? "- - -") : "- - -"}}
            </div>
            <div>
              <div class="overview-item_value">
                {{valueField ? (value?.[valueField] ?? "- - -") : "- - -"}}
              </div>
              <div class="overview-item_des" *ngIf="descriptionField">
                {{descriptionField ? (value?.[descriptionField] ?? "- - -") : "- - -"}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </ng-template>
  `
})
export class FeCardsComponent implements OnChanges {
  @Input() data?: DataTable<any>;
  @Input() columns: IChangeDBColumn[] = [];
  @Input() title?: string;
  @Input() isLarge?: string;

  descriptionField?: string
  labelField?: string;
  valueField?: string;

  ngOnChanges(changes: SimpleChanges) {
    if (["data", "column"].find(value => changes[value])) {
      const columns = changes?.["columns"]?.currentValue ?? this.columns;
      const nameFieldInDataTable = this.findColumnNameInDataTable(columns, XAXIS_COLUMN_NAME);
      const dataFieldInDataTable = this.findColumnNameInDataTable(columns, YAXIS_COLUMN_NAME);
      const seriesFieldInDataTable = this.findColumnNameInDataTable(columns, SERIES_COLUMN_NAME);

      this.valueField = dataFieldInDataTable;
      this.labelField = nameFieldInDataTable;
      this.descriptionField = seriesFieldInDataTable

    }
  }

  findColumnNameInDataTable(columns: IChangeDBColumn[], name: string) {
    return columns.find(value => {
      return value.columnName == name;
    })?.fieldInDataModel;
  }

  trackByTabs(index: number, value: any) {
    return index;
  }
}
