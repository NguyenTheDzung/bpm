import { Component, Input } from "@angular/core";
import { ChartUtils } from "../../utils/chartUtils";
import { IChangeDBColumn } from "../../models/front-end.model";
import { uniq } from "lodash";
import { SERIES_COLUMN_NAME, XAXIS_COLUMN_NAME, YAXIS_COLUMN_NAME } from "../../utils/enums";
import { FrontEndGroupChart } from "../../utils/front-end-group-chart";

@Component({
  selector: 'app-bar-chart',
  templateUrl: '../../utils/chart-template.html',
  styles: []
})
export class BarChartComponent extends FrontEndGroupChart {

  @Input() isHorizontalBar: boolean = false
  @Input() isStackBar: boolean = false

  mapChartData(dataContent: any[], columns: IChangeDBColumn[]) {
    const nameFieldInDataTable= this.findColumnNameInDataTable(columns, XAXIS_COLUMN_NAME)
    const dataFieldInDataTable = this.findColumnNameInDataTable(columns, YAXIS_COLUMN_NAME)
    const seriesFieldInDataTable = this.findColumnNameInDataTable(columns, SERIES_COLUMN_NAME)

    if (!nameFieldInDataTable || !dataFieldInDataTable) {
      throw new Error("lỗi không tìm thấy cột xaxis || yaxis");
    }

    const labels = this.getLabel(nameFieldInDataTable);

    let series: { name: string, data: number[] }[];
    let seriesMapped;
    let legendData: string[] = [];
    let legendSelected: {[a: string]: boolean} = {}

    if(this.isStackBar){
      series = this.getMultiSeries(seriesFieldInDataTable!, dataFieldInDataTable, nameFieldInDataTable)
      seriesMapped = series.map(
        item => {
          return {
            ...ChartUtils.BAR_SERIES_ITEM_CONFIG,
            ...item,
          }
        }
      )
      legendData = seriesMapped.map(series => {
        legendSelected[series.name] = true
        return series.name
      })
    }

    if(!this.isStackBar) {
      seriesMapped = this.getSingleSeriesValue(dataContent, dataFieldInDataTable)
    }

    const customConfig = this.getCustomConfig(labels)
    let config = {
      //@ts-ignore
      series: [
        //@ts-ignore
        ...seriesMapped
      ],
      legend: {
        data: legendData ?? [],
        selected: legendSelected ?? {},
        show: false
      },
      ...customConfig
    }

    return { ...config, ...customConfig };
  }

  getSingleSeriesValue(dataContent: any[], dataFieldInDataTable: string) {
    return [
      {
        data: dataContent.map(value => value[dataFieldInDataTable]),
        type: "bar",
        showBackground: true,
        backgroundStyle: {
          color: "rgba(180, 180, 180, 0.2)"
        }
      }
    ];
  }


  getCustomConfig(labels: string[]): object {
    let customConfig: any = {}

    if (this.isHorizontalBar) {
      customConfig = {
        xAxis:  {
          type: 'value',
        },
        yAxis: {
          type: "category",
          data: labels
        },
        grid:{
          left: "20%"
        },
      };
    }
    if (!this.isHorizontalBar) {
      customConfig = {
        xAxis: {
          type: "category",
          data: labels
        },
        yAxis: {
          type: 'value',
        }
      };
    }
    return customConfig
  }

  getLabel(labelField: string){
    const listLabel: string[] = (this.data?.content ?? []).map(value => {
      return value[labelField];
    });
    return uniq(listLabel)
  }

  getMultiSeries(fieldSeries: string, fieldValue: string, fieldLabel: string): {name: string, data: number[]}[]{

    const series = new Map<string, {name: string, data: number[]}>();
    const nameMap =  new Map<string, number[]>();
    (this.data?.content ?? []).forEach((item: any) => {

      const seriesName = item[fieldSeries]
      const label = item[fieldLabel]

      if(!series.has(seriesName) && !!seriesName){
        series.set(seriesName, {name: seriesName, data: []})
      }

      if(!nameMap.has(label)){
        nameMap.set(label, [])
      }
    })

    for (let [key, value] of series) {
      const data = this.getSingleSeriesData(
        value.name,
        Array.from(nameMap.keys()),
        (this.data?.content ?? []),
        fieldLabel,
        fieldSeries,
        fieldValue
      )
      series.set(key, { name: value.name, data });
    }

    return Array.from(series.values())
  }

  getSingleSeriesData(
    seriesName: string,
    labels: string[],
    dataContent: any[] = this.data?.content?? [],
    fieldLabel: string,
    fieldNameSeries: string,
    fieldValue: string
  ): number[] {

    const filterData = dataContent.filter(
      data => data[fieldNameSeries] == seriesName
    )

    return labels.map((label) => {
      const data = filterData.filter(data => data[fieldLabel] == label)
      if (!data.length) {
        return undefined
      }
      return data.reduce((previousValue, currentValue) => previousValue + +currentValue[fieldValue], 0)
    })
  }

  updateChart(data: any){
    return ChartUtils.initBarChart(data)
  }

  initChart() {
    return ChartUtils.initBarChart()
  }

  setTileOptions()  {
    return {}
  }


}
