import { Component, Input, OnInit } from "@angular/core";
import { DataTable } from "@cores/models/data-table.model";
import { IChangeDBColumn } from "../../models/front-end.model";

@Component({
  selector: "app-fe-table",
  styleUrls: ["./fe-table.component.scss"],
  templateUrl: "./fe-table.component.html"
})
export class FeTableComponent implements OnInit {

  @Input() data?: DataTable<any>;
  @Input() columns: IChangeDBColumn[] = [];
  @Input() title?: string

  constructor() { }

  ngOnInit(): void {
  }

}
