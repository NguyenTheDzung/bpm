import { Component, Injector, Input } from "@angular/core";
import { MenuItem } from "primeng/api";
import { BaseActionComponent } from "@shared/components";
import { BackendService } from "../../services/backend.service";
import { DialogService } from "primeng/dynamicdialog";
import { AddConditionComponent } from "../../dialogs/add-condition/add-condition.component";
import { ScreenType } from "@cores/utils/enums";
import { ChildConditionsComponent } from "../../dialogs/child-conditions/child-conditions.component";
import { IConditionsModel, IConfigModel } from "../../models/backend.model";
import { createErrorMessage } from "@cores/utils/functions";

@Component({
  selector: 'app-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.scss']
})
export class ConditionsComponent extends BaseActionComponent {

  items!: MenuItem[];
  @Input() dataQueryConfig!: IConfigModel
  dataCondition!: IConditionsModel[]

  @Input('dataCondition') set _dataCondition(value: IConditionsModel[]) {
    this.dataCondition = value
    this.compareColumnCode()
  }

  @Input() functionCode?: string
  listCondition?: IConditionsModel[]

  constructor(injector: Injector,
              private service: BackendService,
              private dialogService: DialogService) {
    super(injector, service)
  }


  compareColumnCode() {
    this.listCondition = []
    const raw = this.dataQueryConfig.queryString.match(/:(\w+)/g) || []
    for (let i = 0; i < raw.length; i++) {
      this.listCondition.push(<IConditionsModel>{columnCode: raw[i].split(':')[1]})
    }
    this.listCondition.forEach(
      (value) => {
        if (!this.dataCondition.find((item: IConditionsModel) => item.columnCode == value.columnCode)) {
          this.dataCondition.push(value)
        }
      }
    )
  }

  openChildCondition() {
    this.dialogService.open(
      ChildConditionsComponent,
      {
        showHeader: false,
        width: '70%',
        data: {
          screenType: ScreenType.Create
        }
      }
    )
  }

  getData() {
    this.service.showConditionColumn('conditions', this.dataQueryConfig).subscribe({
      next: (res: any) => {
        this.dataCondition = res?.data
        this.compareColumnCode()
      }
    })
  }

  openCreateCondition(item: any) {
    const data = {
      queryId: this.dataQueryConfig.id.queryId,
      functionCode: this.functionCode,
      ...item
    }
    this.dialogService.open(
      AddConditionComponent,
      {
        showHeader: false,
        width: '50%',
        data: {
          screenType: ScreenType.Create,
          state: data
        }
      }
    ).onClose.subscribe((result) => {
      if (result) {
        this.getData();
      }
    })
  }

  deleteItem(body: IConditionsModel) {
    this.messageService.confirm().subscribe((confirm) => {
      if (confirm) {
        this.loadingService.start()
        this.service.deleteCondition(body).subscribe({
          next: () => {
            this.getData();
            this.loadingService.complete();
          },
          error: (err) => {
            this.messageService.error(createErrorMessage(err));
            this.loadingService.complete()
          }
        })
      }
    })
  }
}
