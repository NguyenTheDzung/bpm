import { CommonModel, Pic } from '@common-category/models/common-category.model';
import { Nullable } from '@cores/models/ts-helpers';

export interface RequestModel {
  id?: string | number;
  code?: string;
  name?: string;
  value?: string;
  description?: string;
  price?: number;
  quantity?: number;
  inventoryStatus?: string;
  category?: string;
  image?: string;
  rating?: number;
  status?: null | string[];
  submitType?: number | string | null;
  requestId?: string | null;
  requestCode?: string | null;
  requestType?: string | null;
  channel?: string | null;
  startDate?: string | Date | null;
  endDate?: string | Date | null;
  policyNumber?: string | null;
  page?: number;
  size?: number;
  nb_user?: string;
  pics?: string[] | null;
  icCode?: string | null;
  icName?: string | null;
  resolution?: string | string[] | null;
  distributionChannel?: null | string;
  tittle?: any;
  show?: any;
  requestTypeCode?: string | null;
  createdDate?: string;
  customerName?: string | null;
  pic?: string | null;
  amount?: number | null;
  stepApprove?: string;
  createdBy?: string | string[] | null;
  slaStepMessage?: string;
  slaStepMinutesRemaining?: number;
  minutesSlaStep?: number;
  hoursSlaStep?: number;
  daySlaStep?: number;
  claimId?: Nullable<string>;
  claimStatus?: Nullable<string[]>;
  source?: Nullable<string[]>;
  departments?: Nullable<string[]>;
}

export interface StateRequest {
  listRequestType?: CommonModel[];
  listSubmitChannel?: CommonModel[];
  listPic?: Pic[];
  listGroup?: CommonModel[];
  listTitle?: CommonModel[];
  listDecision?: CommonModel[];
  listReason?: CommonModel[];
  listSubReasonApprove?: CommonModel[];
  listBank?: any[];
  listPendingReason?: CommonModel[];
  listRejectReason?: CommonModel[];
  listAdditionalAttachment?: CommonModel[];
  listApprovedReason?: CommonModel[];
  listResolution?: CommonModel[];
  listStatus?: CommonModel[];
  listDistributionChannel?: CommonModel[];
  listDecisionStepApprove?: CommonModel[];
  listPostText?: CommonModel[];
  listTypeCancel?: CommonModel[];
  listRefuseReason?: CommonModel[];
  listSubRefuseReason?: CommonModel[];
  listPaymentMethod?: CommonModel[];
  listIdentifyType?: CommonModel[];
  listCollectionDecisionContractStore?: CommonModel[];
  listGender?: CommonModel[];
  listFeeVoucherType?: CommonModel[];
  listPeriodicFeePayment?: CommonModel[];
  listCategory?: CommonModel[];
  listDepartment?: CommonModel[];
  listDailyNewAction?: CommonModel[];
  resolutionCollection?: CommonModel[];
  resolutionRefund?: CommonModel[];
  resolutionClaim?: CommonModel[];
  requestTypeRefund?: CommonModel[];
  sapIdentifyTypeCode?: CommonModel[];
  permissionForward?: ForwardTypeCode[];
  permissionPicForward?: ForwardTypeCode[];
  listClaimStatus?: CommonModel[];
  listResourceClaim?: CommonModel[];
}

export interface UserModel {
  name?: string | null;
  username?: string | null;
  value?: string;
  account?: string;
}

export interface GroupModel {
  code?: string;
  name?: string;
  value?: string | number;
}

export interface ApproveNonP2p {
  status: number;
  requests: string[];
  type: number;
  note?: string;
}

export interface Bank {
  citadCode: string;
  detailName: string;
  name: string;
}

export interface PermissionForward {
  forwardTypeCodes: ForwardTypeCode[];
  forwardTypeCodesOfRole: ForwardTypeCode[];
}

export interface ForwardTypeCode {
  role: string;
  types: string[];
}

export interface IBpmn {
  content: string;
  currentActivities: ICurrentTask[];
  taskMonitoring: IBpmnTask[];
  passedSequentFlows?: string[];
}

export interface ICurrentTask {
  defKey: string;
  name: string;
}

export interface IBpmnTask {
  duration?: number;
  endTime?: string;
  name: string;
  startTime: string;
  activityDefKey: string;
  type: 'startEvent' | 'endEvent' | 'serviceTask' | 'userTask' | 'exclusiveGateway';
}

export interface User {
  account: string;
  code: string;
  name: string;
  role: string;
  phoneNumber: string;
  email: string;
}
