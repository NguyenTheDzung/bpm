import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {DataTable} from '@cores/models/data-table.model';
import {BaseService} from '@cores/services/base.service';
import {mapCommonCategoryByCode, mapDataTable, mapDataTableEmployee, removeParamSearch} from '@cores/utils/functions';
import {environment} from '@env';
import {
  catchError,
  delay,
  forkJoin,
  map,
  Observable,
  of,
  OperatorFunction,
  repeatWhen,
  skipWhile,
  Subject,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs';
import {DetailRequestModel} from '@detail-request/shared/models/detail-request.model';
import {ApproveNonP2p, Bank, IBpmn, PermissionForward, StateRequest, User} from '../models/request.model';
import {CommonCategoryService} from '@cores/services/common-category.service';
import {
  APPROVEDREASON,
  ATTACHMENT_DROPLIST,
  CATEGORY_DROPLIST_NOPCHUNGTUPHI,
  CLAIM_SOURCE_REQUEST,
  ClaimCommonType,
  COLLECTION_DECISION_CONTRACT_STORE,
  DAILY_NEXT_ACTION,
  DECISION,
  DISTRIBUTION_CHANNEL,
  GENDER,
  GROUP_USER,
  IDENTIFY_TYPE,
  LOAI_CHUNGTU_PHI,
  NBU_APP_REFUSAL_REASON,
  NBU_APP_REFUSAL_SUB_REASON,
  NBU_APP_UW_REQ_PROCESS_OPTION,
  P_TO_P_DECICISON,
  PAYMENT_METHOD,
  PENDING_REASON,
  PERIODICFEE_PAYMENT,
  POST_TEXT,
  REJECT_REASON,
  REQUEST_TYPE,
  REQUEST_TYPE_REFUND,
  RESOLUTION,
  RESOLUTION_CLAIM,
  RESOLUTION_COLLECTION,
  RESOLUTION_REFUND,
  Roles,
  SAP_IDENTIFICATION_TYPE_CODE,
  STATUS,
  SUBREASONAPPROVE,
} from '@cores/utils/constants';
import {RequestType} from '@create-requests/utils/constants';
import {AttachmentFeeInfo} from '@detail-request/shared/models/collection-detail.model';
import {CommonModel} from '@common-category/models/common-category.model';
import {IResponseModel} from '@cores/models/response.model';
import {cloneDeep, isEmpty} from 'lodash';
import {ProcessSearch} from '@reports/models/report.model';
import {CommonClaimCategory} from '@cores/models/common-claim.model';

@Injectable({
  providedIn: 'root',
})
export class RequestService extends BaseService {
  override state: StateRequest = {};

  constructor(http: HttpClient, private commonService: CommonCategoryService) {
    super(http, `${environment.refund_url}/request`);
  }

  override getState(): Observable<StateRequest> {
    if (!isEmpty(this.state)) {
      return of(cloneDeep(this.state));
    } else {
      return forkJoin({
        state: this.getCommonState(),
        listPic: this.getPic({ roleName: [Roles.OP_NBU_USER, Roles.OP_COL_USER, Roles.OP_CLAIM_USER] }),
        listBank: this.getBankCode(),
        forwardConfig: this.getPermissionForwardRequest(),
        listClaimStatus: this.commonService
          .getCommonClaimByTypes(ClaimCommonType.STATUS_CLAIM)
          .pipe(catchError(() => of(<CommonClaimCategory[]>[]))),
        listResourceClaim: this.commonService.getCommonCategory(CLAIM_SOURCE_REQUEST),
      }).pipe(
        map((data) =>
          cloneDeep(
            (this.state = {
              ...this.state,
              ...data.state,
              listPic: data.listPic,
              listBank: data.listBank,
              permissionForward: data.forwardConfig.forwardTypeCodes,
              permissionPicForward: data.forwardConfig.forwardTypeCodesOfRole,
              listClaimStatus: data.listClaimStatus,
              listResourceClaim: data.listResourceClaim,
            })
          )
        )
      );
    }
  }

  getPermissionForwardRequest(): Observable<PermissionForward> {
    return this.http
      .get<IResponseModel<PermissionForward>>(`${environment.category_url}/request-type-config/requestType/permission`)
      .pipe(map((res) => res.data));
  }

  getCommonState(): Observable<StateRequest> {
    return this.commonService
      .getCommonByCodes([
        DECISION,
        SUBREASONAPPROVE,
        PENDING_REASON,
        REJECT_REASON,
        ATTACHMENT_DROPLIST,
        APPROVEDREASON,
        STATUS,
        REQUEST_TYPE,
        DISTRIBUTION_CHANNEL,
        P_TO_P_DECICISON,
        POST_TEXT,
        GROUP_USER,
        NBU_APP_UW_REQ_PROCESS_OPTION,
        NBU_APP_REFUSAL_REASON,
        NBU_APP_REFUSAL_SUB_REASON,
        PAYMENT_METHOD,
        IDENTIFY_TYPE,
        COLLECTION_DECISION_CONTRACT_STORE,
        GENDER,
        LOAI_CHUNGTU_PHI,
        PERIODICFEE_PAYMENT,
        CATEGORY_DROPLIST_NOPCHUNGTUPHI,
        DAILY_NEXT_ACTION,
        RESOLUTION_COLLECTION,
        RESOLUTION_REFUND,
        REQUEST_TYPE_REFUND,
        RESOLUTION_CLAIM,
        SAP_IDENTIFICATION_TYPE_CODE,
      ])
      .pipe(
        map((res: CommonModel[]) => {
          return <StateRequest>{
            listDecision: mapCommonCategoryByCode(res, DECISION),
            listSubReasonApprove: mapCommonCategoryByCode(res, SUBREASONAPPROVE),
            listPendingReason: mapCommonCategoryByCode(res, PENDING_REASON),
            listRejectReason: mapCommonCategoryByCode(res, REJECT_REASON),
            listAdditionalAttachment: mapCommonCategoryByCode(res, ATTACHMENT_DROPLIST),
            listApprovedReason: mapCommonCategoryByCode(res, APPROVEDREASON),
            listResolution: mapCommonCategoryByCode(res, RESOLUTION),
            listStatus: mapCommonCategoryByCode(res, STATUS),
            listRequestType: mapCommonCategoryByCode(res, REQUEST_TYPE),
            listDistributionChannel: mapCommonCategoryByCode(res, DISTRIBUTION_CHANNEL),
            listDecisionStepApprove: mapCommonCategoryByCode(res, P_TO_P_DECICISON),
            listPostText: mapCommonCategoryByCode(res, POST_TEXT),
            listGroup: mapCommonCategoryByCode(res, GROUP_USER),
            listTypeCancel: mapCommonCategoryByCode(res, NBU_APP_UW_REQ_PROCESS_OPTION),
            listRefuseReason: mapCommonCategoryByCode(res, NBU_APP_REFUSAL_REASON),
            listSubRefuseReason: mapCommonCategoryByCode(res, NBU_APP_REFUSAL_SUB_REASON),
            listPaymentMethod: mapCommonCategoryByCode(res, PAYMENT_METHOD),
            listIdentifyType: mapCommonCategoryByCode(res, IDENTIFY_TYPE),
            listCollectionDecisionContractStore: mapCommonCategoryByCode(res, COLLECTION_DECISION_CONTRACT_STORE),
            listGender: mapCommonCategoryByCode(res, GENDER),
            listFeeVoucherType: mapCommonCategoryByCode(res, LOAI_CHUNGTU_PHI),
            listPeriodicFeePayment: mapCommonCategoryByCode(res, PERIODICFEE_PAYMENT),
            listCategory: mapCommonCategoryByCode(res, CATEGORY_DROPLIST_NOPCHUNGTUPHI),
            listDailyNewAction: mapCommonCategoryByCode(res, DAILY_NEXT_ACTION),
            resolutionCollection: mapCommonCategoryByCode(res, RESOLUTION_COLLECTION),
            resolutionRefund: mapCommonCategoryByCode(res, RESOLUTION_REFUND),
            resolutionClaim: mapCommonCategoryByCode(res, RESOLUTION_CLAIM),
            requestTypeRefund: mapCommonCategoryByCode(res, REQUEST_TYPE_REFUND),
            sapIdentifyTypeCode: mapCommonCategoryByCode(res, SAP_IDENTIFICATION_TYPE_CODE),
          };
        })
      );
  }

  getPic(params?: { roleName?: string[]; activeOnly?: boolean }): Observable<User[]> {
    return this.http
      .get(`${environment.employee_url}/user/list-active-employee`, {
        params: params,
      })
      .pipe(map((res: any) => res.data));
  }

  getBankCode(): Observable<Bank[]> {
    return this.http.get(`${environment.category_url}/bank-code?regex=x`).pipe(
      map((data: any) =>
        data?.data
          ?.filter((item: any) => !!item.regex)
          .map((item: any) => {
            return {
              ...item,
              name: item.name ? `${item.detailName} - ${item.name}` : item.detailName,
            };
          })
      )
    );
  }

  override search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}/search`, { params: newParam })
        .pipe(map((data) => mapDataTable(data, params)));
    }
    return this.http
      .get<DataTable>(`${environment.task_url}/task/search`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTable(data, params)));
  }

  history(params?: any) {
    const newParam: any = removeParamSearch(params);
    return this.http
      .get<DataTable>(`${environment.task_url}/task/list-histories`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTable(data, params)));
  }

  //gọi api đay
  findByRequestId(id: string) {
    return this.http.get<any>(`${environment.task_url}/task/detail?code=${id}`).pipe(map((val: any) => val.data.data));
  }

  //get table paymentlot
  getChronological(requestId: string): Observable<any> {
    return this.http
      .get<AttachmentFeeInfo>(
        `${environment.collection_url}/collection/fee-attachment-request/get-chronological?requestId=` + requestId
      )
      .pipe(map((data) => data));
  }

  getLot(params: any): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);

    return this.http
      .get<DataTable>(`${environment.collection_url}/collection/fee-attachment-request/get-lot`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTableEmployee(data, params)));
  }

  asyncSubmit(endPointUrl: string): OperatorFunction<any, any> {
    return switchMap((res: any) => {
      let count = 200; // repeat 200 lần cách nhau 3 giây, sau 200 lần sẽ throw
      return this.http.get(`${endPointUrl}/process-management?id=${res.data}`).pipe(
        map((val: any) => {
          if (count === 0) {
            this._stop.next();
            throw { error: { status: 'ERROR' } };
          }
          if (['COMPLETE', 'ERROR'].includes(val.data?.status)) {
            this._stop.next();
            if ('ERROR' === val?.data?.status) {
              val.error = val.data;
              throw val;
            }
          }
          return val;
        }),
        skipWhile((resCheck: any) => !['COMPLETE', 'ERROR'].includes(resCheck.data?.status)),
        repeatWhen((ob) =>
          ob.pipe(
            delay(3000),
            takeUntil(this._stop),
            tap(() => {
              if (count == 0) {
                this._stop.next();
              }
              count--;
            })
          )
        )
      );
    });
  }

  //save or submit của luồng nộp chứng từ phí
  saveOrSubmitFeeAttachmentRequest(data: any, isSubmit?: boolean) {
    if (isSubmit) {
      return this.http
        .put(`${environment.collection_url}/collection/fee-attachment-request/submit`, data)
        .pipe(this.asyncSubmit(environment.collection_url));
    }
    return this.http.put(`${environment.collection_url}/collection/fee-attachment-request`, data);
  }

  //luồng col khôi phục hđ
  updateActionContractRestore(data: ApproveNonP2p): Observable<any> {
    return this.http.put(`${environment.task_url}/task/update`, data);
  }

  getAttachment(id: string): Observable<DetailRequestModel> {
    return this.http.get<DetailRequestModel>(`${this.baseUrl}/detail/getAttachment?id=${id}`);
  }

  getReport(id: string, type: string): Observable<any> {
    let url;
    if ([RequestType.TYPE07, RequestType.TYPE08, RequestType.TYPE09, RequestType.TYPE10].includes(type)) {
      url = `${environment.collection_url}/collection/report?requestId=${id}&type=${type}`;
    } else {
      url = `${this.baseUrl}/detail/report?code=${id}`;
    }
    return this.http.get(url);
  }

  downloadFileSAP(id: string): Observable<any> {
    return this.http.post(`${environment.refund_url}/sap/policy-attachment-file`, id);
  }

  forwardRequest(data: any): Observable<any> {
    return this.http.post(`${environment.task_url}/task/forward`, data);
  }

  addNewBankAccount(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/add-new-bank-account`, data);
  }

  addNewBankAccountCollection(data: any): Observable<any> {
    return this.http.post(`${environment.collection_url}/collection/add-bp-bank`, data);
  }

  saveDetail(data: any, requestType: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/${this.getPathByRequestType(requestType)}/detail/update-info`, data); //th là refunnd
  }

  saveProcessDetail(data: any, requestType: string): Observable<any> {
    return this.http.put(`${this.baseUrl}/${this.getPathByRequestType(requestType)}/update-process-item`, data);
  }

  private readonly _stop = new Subject<void>();

  stop(): void {
    this._stop.next();
  }

  submitInfo(data: any, requestType: string): Observable<any> {
    return this.http
      .put(`${this.baseUrl}/${this.getPathByRequestType(requestType)}/detail/submit-info`, data)
      .pipe(this.asyncSubmit(environment.refund_url));
  }

  getPaymentLot(params?: any): Observable<any> {
    const newParam: any = removeParamSearch(params);
    return this.http.get<any>(`${environment.refund_url}/sap/payment-lot`, {
      params: { ...newParam },
    });
  }

  refuse(data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}/app-underwriting/refusal`, data);
  }

  //Collection threads
  checkBp(requestId: string, requestType: string, data: any): Observable<any> {
    return this.http.post(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/check-bp`,
      data
    );
  }

  createBP(requestId: string, requestType: string, data: any): Observable<any> {
    return this.http.post(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/create-bp`,
      data
    );
  }

  createPR(requestId: string, requestType: string, data: any): Observable<any> {
    return this.http.post(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/create-payment-request`,
      data
    );
  }

  saveDetailRefundRequest(requestId: string, requestType: string, data: any): Observable<any> {
    return this.http.put(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/save`,
      data
    );
  }

  submitDetailRefundRequest(requestId: string, requestType: string, data: any): Observable<any> {
    return this.http
      .put(`${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/submit`, data)
      .pipe(this.asyncSubmit(environment.collection_url));
  }

  paymentLotProcessing(requestId: string, data: any, requestType: string): Observable<any> {
    return this.http.put(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/processing-payment-lot`,
      data
    );
  }

  getPaymentLotCollection(params?: any): Observable<any> {
    const newParam: any = removeParamSearch(params);
    return this.http.post<any>(`${environment.collection_url}/collection/refund-request/select-payment-lot`, newParam);
  }

  qmDecision(requestId: string, data: any, requestType: string): Observable<any> {
    return this.http.put(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/qm-decision`,
      data
    );
  }

  sendQmEmail(requestId: string, data: any, requestType: string): Observable<any> {
    return this.http.put(
      `${environment.collection_url}/${this.getPathByRequestType(requestType)}/${requestId}/send-qm-email`,
      data
    );
  }

  //collection
  //download attachment
  getAttachmentCollection(id: number) {
    return this.http.get(`${environment.collection_url}/collection/attachment-management?id=${id}`);
  }

  checkBpInfo(params: any) {
    return this.http.get<any>(`${this.baseUrl}/check-bp-info`, {
      params: params,
    });
  }

  addBpInfo(data: any) {
    return this.http.post(`${this.baseUrl}/add-bp`, data);
  }

  //processing cho luoofng nộp chứng từ
  updateProcess(data: any): Observable<any> {
    return this.http.put(`${environment.collection_url}/collection/fee-attachment-request/process-item`, data);
  }

  viewResetPolicy(requestId: string): Observable<any> {
    return this.http.get(`${environment.collection_url}/collection/reinstatement/workflow/`, { params: { requestId } });
  }

  // close request "Hủy HSYCBH theo quyết định của phòng thẩm định"
  closeRequest(requestCode: string) {
    return this.http.put(`${this.baseUrl}/cancel`, { requestCode });
  }

  // lưu ghi chú của người xử lý request
  noteRequest(requestCode: string, picNote: string) {
    return this.http.put(`${environment.task_url}/task/pic-note`, { requestCode, picNote });
  }

  private getPathByRequestType(requestType: string) {
    switch (requestType) {
      case RequestType.TYPE03:
        return 'refund';
      case RequestType.TYPE04:
        return 'transfer';
      case RequestType.TYPE02:
        return 'app-transfer';
      case RequestType.TYPE01:
        return 'app-refund';
      case RequestType.TYPE05:
        return 'app-underwriting';
      case RequestType.TYPE08:
        return 'collection/refund-request';
      default:
        return 'undefined';
    }
  }

  getFileBpmnByRequestId(requestId: string): Observable<IBpmn> {
    return this.http
      .get<IBpmn>(`${environment.task_url}/task/bpms-file?code=${requestId}`)
      .pipe(map((res: any) => res.data));
  }

  getFileBpmnByProcessTypeAndVersion(params: ProcessSearch) {
    return this.http
      .post<IBpmn>(`${environment.task_url}/sys-management/proc_monitoring`, removeParamSearch(params))
      .pipe(map((res: any) => res.data));
  }

  getVersionsBpmn(params: ProcessSearch) {
    return this.http
      .get<number[]>(`${environment.task_url}/sys-management/proc_versions`, { params: removeParamSearch(params) })
      .pipe(map((res: any) => res.data));
  }

  getBankInfo(data: { accountNumber: string, bankCode: string, accountType: string, transferType: string }) {
    return this.http.post(`${environment.payment_url}/payment/get-account-info`, data)
  }
}
