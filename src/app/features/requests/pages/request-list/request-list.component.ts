import { AfterViewChecked, Component, Injector, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { createErrorMessage, getValueDateTimeLocal } from '@cores/utils/functions';
import { FunctionCode, ScreenType, SessionKey } from '@cores/utils/enums';
import { BaseTableComponent } from '@shared/components';
import { ForwardUserComponent } from '../../components';
import { ForwardTypeCode, RequestModel, StateRequest } from '../../models/request.model';
import { RequestService } from '../../services/request.service';
import { Resolution, Roles, StatusValue } from '@cores/utils/constants';
import { filter, intersection, isDate, isNumber, uniq } from 'lodash';
import { Table } from 'primeng/table';
import { getUrlDetailRequest } from '@detail-request/shared/utils/functions';
import { TableCheckbox } from 'primeng/table/table';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss'],
})
export class RequestListComponent extends BaseTableComponent<RequestModel> implements OnInit, AfterViewChecked {
  username?: string;
  rolePic: boolean = false;
  showColumnClaim = false;
  showColumnNbuAndCol = false;
  hidden = false;
  selectedItem!: RequestModel[];
  screenType: ScreenType | undefined;
  override stateData: StateRequest = {};
  override params: RequestModel & { requestCodes?: string[] } = {
    requestId: null,
    requestCode: null,
    channel: null,
    status: null,
    submitType: null,
    startDate: null,
    endDate: null,
    policyNumber: '',
    pics: null,
    icCode: '',
    icName: '',
    requestType: null,
    resolution: [],
    distributionChannel: null,
    requestCodes: [],
    claimId: null,
    claimStatus: null,
    source: null,
    departments: null,
  };
  resolution = Resolution;
  requestTypeNbu = [
    this.requestType.TYPE08,
    this.requestType.TYPE01,
    this.requestType.TYPE02,
    this.requestType.TYPE03,
    this.requestType.TYPE04,
    this.requestType.TYPE05,
  ];
  listRequestTypeForward: string[] = [];
  today = new Date();
  @ViewChild(Table) table?: Table;
  @ViewChildren('checkboxRow') checkboxRows?: QueryList<TableCheckbox>;

  constructor(injector: Injector, private service: RequestService) {
    super(injector, service);
    this.username = this.currUser?.username;
  }

  show() {
    this.hidden = !this.hidden;
  }

  onReset() {
    this.params = {
      requestId: null,
      requestCode: null,
      channel: null,
      status: null,
      submitType: null,
      startDate: null,
      endDate: null,
      policyNumber: '',
      pics: null,
      icCode: '',
      icName: '',
      requestType: null,
      requestTypeCode: null,
      resolution: [],
    };
    this.search();
  }

  ngOnInit(): void {
    this.loadingService.start();
    this.stateData.listDepartment = [];
    this.stateData.listResolution = [];
    this.service.getState().subscribe({
      next: (state) => {
        state.permissionForward?.forEach((item) => {
          this.listRequestTypeForward = [...this.listRequestTypeForward, ...item.types];
        });
        /* Nếu user có vai trò OP_DIRECTOR hoặc SYSTEM_ADMIN thì hiển thị tất cả các phòng ban*/
        if (intersection([Roles.OP_DIRECTOR, Roles.SYSTEM_ADMIN], this.authService.getUserRoles()).length > 0) {
          this.showColumnClaim = true;
          this.showColumnNbuAndCol = true;
          this.stateData.listDepartment = [
            { code: 'NBU', name: 'NBU' },
            { code: 'COL', name: 'Collection' },
            { code: 'CLAIM', name: 'Claim' },
          ];
          this.stateData.listResolution = [
            {
              name: 'NBU',
              items: state.resolutionRefund || [],
            },
            {
              name: 'COL',
              items: state.resolutionCollection || [],
            },
            {
              name: 'CLAIM',
              items: state.resolutionClaim || [],
            },
          ];
        } else {
          /* kiểm tra user thuộc phòng ban nào để hiển thị các input filter và column tương ứng */
          if (
            intersection(
              [Roles.OP_NBU_ADMIN, Roles.OP_NBU_MANAGER, Roles.OP_NBU_USER, Roles.USER_SALE, Roles.FI_USER],
              this.authService.getUserRoles()
            ).length > 0
          ) {
            this.showColumnNbuAndCol = true;
            this.stateData.listDepartment?.push({ code: 'NBU', name: 'NBU' });
            this.stateData.listResolution?.push({
              name: 'NBU',
              items: state.resolutionRefund || [],
            });
          }
          if (
            intersection(
              [Roles.OP_COL_ADMIN, Roles.OP_COL_CSSS, Roles.OP_COL_MANAGER, Roles.OP_COL_USER],
              this.authService.getUserRoles()
            ).length > 0
          ) {
            this.showColumnNbuAndCol = true;
            this.stateData.listDepartment?.push({ code: 'COL', name: 'Collection' });
            this.stateData.listResolution?.push({
              name: 'COL',
              items: state.resolutionCollection || [],
            });
          }
          if (
            intersection(
              [
                Roles.OP_CLAIM_ADMIN,
                Roles.OP_CLAIM_ASSESSOR,
                Roles.OP_CLAIM_INVESTIGATOR,
                Roles.OP_CLAIM_ADMIN_INVESTIGATOR,
                Roles.OP_CLAIM_DEPUTY,
                Roles.CLAIM_COMMITTEE,
                Roles.OP_NBU_UNDERWRITTING,
              ],
              this.authService.getUserRoles()
            ).length > 0
          ) {
            this.showColumnClaim = true;
            this.stateData.listDepartment?.push({ code: 'CLAIM', name: 'Claim' });
            this.stateData.listResolution?.push({
              name: 'CLAIM',
              items: state.resolutionClaim || [],
            });
          }
        }
        this.params.departments = this.stateData.listDepartment?.map((item) => item.code!) ?? [];
        const roles = filter(
          this.authService.getUserRoles(),
          (role) => role !== Roles.MEMBER && role === role.toUpperCase()
        );
        // kiểm tra current user có được hiển thị cột PIC không
        this.rolePic =
          intersection(roles, [
            Roles.OP_NBU_ADMIN,
            Roles.OP_NBU_MANAGER,
            Roles.OP_COL_ADMIN,
            Roles.OP_COL_MANAGER,
            Roles.OP_CLAIM_ADMIN,
            Roles.OP_CLAIM_ADMIN_INVESTIGATOR,
            Roles.OP_CLAIM_DEPUTY,
            Roles.CLAIM_COMMITTEE,
          ]).length > 0;

        this.params.resolution = [];
        for (const role of roles) {
          switch (role) {
            case Roles.OP_NBU_USER:
            case Roles.OP_NBU_ADMIN:
              this.params.resolution = [...this.params.resolution, Resolution.NBU_INPROCESS];
              break;
            case Roles.USER_SALE:
              this.params.resolution = [...this.params.resolution, Resolution.NBU_SALE_PENDING];
              break;
            case Roles.FI_USER:
              this.params.resolution = [...this.params.resolution, Resolution.NBU_FI_INPROCESS];
              break;
            case Roles.OP_NBU_MANAGER:
              this.params.resolution = [...this.params.resolution, Resolution.NBU_P2P_INPROCESS];
              break;
            case Roles.SYSTEM_ADMIN:
              this.params.status = [StatusValue.In_process];
              break;
            case Roles.OP_COL_USER:
              this.params.resolution = [...this.params.resolution, Resolution.OP_COL_USER_INPROCESS];
              break;
            case Roles.OP_COL_ADMIN:
              this.params.resolution = [
                ...this.params.resolution,
                Resolution.OP_COL_USER_INPROCESS,
                Resolution.OP_COL_MANAGER_INPROCESS,
                Resolution.OP_DIRECTOR_INPROCESS,
              ];
              break;
            case Roles.OP_COL_MANAGER:
              this.params.resolution = [...this.params.resolution, Resolution.OP_COL_MANAGER_INPROCESS];
              break;
            case Roles.OP_CLAIM_INVESTIGATOR:
            case Roles.OP_CLAIM_INVESTIGATOR_ADMIN:
              this.params.resolution = [...this.params.resolution, Resolution.OP_CLAIM_INVESTIGATOR_INPROCESS];
              this.params.status = [StatusValue.In_process];
              break;
            default:
              break;
          }
        }
        // NBU nếu user có role OP_NBU_USER thì chỉ default resolution NBU_INPROCESS
        if (roles.includes(Roles.OP_NBU_USER)) {
          this.params.resolution = filter(
            this.params.resolution,
            (value) =>
              ![Resolution.NBU_SALE_PENDING, Resolution.NBU_FI_INPROCESS, Resolution.NBU_P2P_INPROCESS].includes(value)
          );
        }

        if (roles.includes(Roles.OP_COL_CSSS)) {
          this.params.resolution = [Resolution.COL_SALE_PENDING];
          this.params.status = [StatusValue.In_process];
        }
        // default value PIC nếu role là ADMIN
        if (
          (roles.includes(Roles.OP_NBU_ADMIN) ||
            roles.includes(Roles.OP_COL_ADMIN) ||
            roles.includes(Roles.OP_CLAIM_INVESTIGATOR)) &&
          state.listPic?.find((item: any) => item.account === this.currUser.username)
        ) {
          this.params.pics = [this.currUser.username];
        }

        const routeState = this.location.getState() as any;
        if (routeState['status'] || routeState['resolution'] || routeState['requestCodes']) {
          this.params.resolution = [];
          this.params.status = [];
        }

        //Lấy dữ liệu từ url xuống fill để tìm kiếm
        if (Array.isArray(routeState['status'])) {
          this.params.status = routeState['status'];
        } else if (routeState['status']) {
          this.params.status = [routeState['status']];
        }

        if (Array.isArray(routeState['resolution'])) {
          this.params.resolution = routeState['resolution'];
        } else if (routeState['resolution']) {
          this.params.resolution = [routeState['resolution']];
        }

        if (Array.isArray(routeState['pic'])) {
          let picList: string[] = routeState['pic'].filter((pic: string) =>
            Boolean(state.listPic?.find((item) => item.account == pic))
          );
          this.params.pics = [...picList];
        } else if (routeState['pic'] && state.listPic?.find((item) => routeState['pic'] == item.account)) {
          this.params.pics = [routeState['pic']];
        }

        if (routeState['startDate'] && routeState['endDate']) {
          const startDate = new Date(routeState['startDate']);
          const endDate = new Date(routeState['endDate']);
          if (isDate(startDate) && isDate(endDate)) {
            this.params.startDate = startDate;
            this.params.endDate = endDate;
          }
        }

        /*
         * Danh sách các mã yêu cầu được api trả về ở màn dashboard
         *
         * Dùng để hiển thị các mã yêu cầu không thể filter theo các trường của bên màn dashboard (SLA late ...)
         *
         * requestCodes này sẽ được sử dụng để call API sẽ trả về thông tin các mã yêu cầu và hiển thị lên ở table request
         *
         * Chỉ khi người dùng nhấn clear filter thì mới reset requestCodes này
         */
        if (routeState['requestCodes']) {
          this.params.requestCodes = routeState['requestCodes'];
          if (!this.params.requestCodes?.length) {
            /*
             * Nếu người dùng nhấn vào một card có giá trị là 0 thì khi pass request code qua bên
             * màn hình request thì sẽ thực hiện reject giá trị requestCodes nên sẽ lấy tất cả bản ghi
             *
             * Nên khi có requestCodes trên params sẽ check một lần nữa nếu mà requestCodes rỗng thì sẽ gán giá trị
             * [null] để request có thể nhận được params request code và không trả ra bất cứ giá trị nào
             */
            this.params.requestCodes = [null as any];
          }
        }

        //Xóa các params trên url
        this.router
          .navigate([], {
            queryParams: {
              resolution: null,
              status: null,
            },
            replaceUrl: true,
          })
          .then();

        this.params.status = uniq(this.params.status);
        this.params.resolution = uniq(this.params.resolution);
        this.stateData.listStatus = state.listStatus || [];
        this.stateData.listRequestType = state.listRequestType || [];
        this.stateData.listDistributionChannel = state.listDistributionChannel || [];
        this.stateData.listPic = state.listPic;
        this.stateData.listGroup = state.listGroup || [];
        this.stateData.permissionForward = state.permissionForward ?? [];
        this.stateData.permissionPicForward = state.permissionPicForward ?? [];
        this.stateData.listClaimStatus = state.listClaimStatus;
        this.stateData.listResourceClaim = state.listResourceClaim;
        this.search(true);
      },
    });
  }

  doDetail(item: RequestModel) {
    this.router
      .navigate([getUrlDetailRequest(this.router.url, item.requestTypeCode!, item.requestId!)], {
        state: item,
      })
      .then();
  }

  override search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }
    this.loadingService.start();
    const params = this.mapDataSearch();
    this.service.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (firstPage && this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.dataTable.content.forEach((item) => {
          if (item.slaStepMessage) {
            if (isNumber(item.slaStepMinutesRemaining)) {
              item.slaStepMinutesRemaining = Math.abs(item.slaStepMinutesRemaining);
              item.minutesSlaStep = item.slaStepMinutesRemaining % 60;
              item.hoursSlaStep = Math.floor(item.slaStepMinutesRemaining / 60);
              item.daySlaStep = Math.floor(item.hoursSlaStep / 24);
            }
            if (isNumber(item.daySlaStep) && item.daySlaStep > 0) {
              item.hoursSlaStep = item.hoursSlaStep! % 24;
              item.slaStepMessage = `request.detailRequest.underwrite.${item.slaStepMessage}_DAY`;
            } else {
              item.slaStepMessage = `request.detailRequest.underwrite.${item.slaStepMessage}`;
            }
          }
        });
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.loadingService.complete();
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  override mapDataSearch(): RequestModel {
    return {
      ...this.params,
      startDate: getValueDateTimeLocal(this.params.startDate),
      endDate: getValueDateTimeLocal(this.params.endDate),
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
      resolution: this.params.resolution,
    };
  }

  viewRequestData(item: RequestModel) {
    localStorage.setItem('requestId', item.requestId!.toString());
    const refundType = [
      this.requestType.TYPE01,
      this.requestType.TYPE02,
      this.requestType.TYPE03,
      this.requestType.TYPE04,
      this.requestType.TYPE05,
    ];
    if (refundType.includes(item.requestTypeCode!)) {
      this.router.navigateByUrl('/bpm/create-requests/refund', { state: { requestId: item.requestId } }).then();
    } else {
      this.loadingService.start();
      this.service.findByRequestId(item.requestId!).subscribe({
        next: (data) => {
          this.loadingService.complete();
          let uri = '/bpm/create-requests/collection';
          if (item.requestTypeCode! === this.requestType.TYPE08) {
            uri = '/bpm/create-requests/refund';
          }
          this.router
            .navigateByUrl(uri, {
              state: {
                request: data,
                type: this.requestType.TYPE06,
              },
            })
            .then();
        },
        error: () => {
          this.messageService.error(`MESSAGE.E_INTERNAL_SERVER`);
          this.loadingService.complete();
        },
      });
    }
  }

  override initConfigAction(): void {
    this.configAction = {
      title: 'forward user',
      component: ForwardUserComponent,
    };
  }

  override viewCreate() {
    if (!this.configAction?.component) {
      return;
    }
    let ids: string[] = [],
      types: string[] = [];
    if (this.selectedItem.length > 0) {
      for (let item of this.selectedItem) {
        ids.push(item.requestId!);
        types.push(item.requestTypeCode!);
      }
    }

    // kiểm tra các bản ghi forward có cùng nhóm không?
    types = uniq(types);
    const samePic = this.selectedItem.every(
      (item) => item.pic?.toLowerCase() === this.currUser.username?.toLowerCase()
    );
    const configForward = samePic ? this.stateData.permissionPicForward! : this.stateData.permissionForward!;
    let isSameGroup = false,
      itemPerForward: ForwardTypeCode | null = null;
    for (const itemPer of configForward) {
      if (types.every((o) => itemPer.types.includes(o!))) {
        isSameGroup = true;
        itemPerForward = itemPer;
        break;
      }
    }
    if (!isSameGroup && configForward.length > 0) {
      return this.messageService.warn('MESSAGE.RECORD_SELECTED_NOT_SAME_GROUP');
    }

    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toLowerCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '40%',
      data: {
        screenType: ScreenType.Create,
        state: this.stateData,
        model: ids,
        roleName: itemPerForward?.role,
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          this.selectedItem = [];
          this.table!.selectionKeys = {};
          this.search();
        }
      },
    });
  }

  onChangeValueDate(event: any) {
    this.params.startDate = event;
    const startDate = this.params.startDate ? new Date(this.params.startDate).valueOf() : 0;
    const endDate = this.params.endDate ? new Date(this.params.endDate).valueOf() : 0;
    if (startDate > endDate) {
      this.params.endDate = null;
    }
  }

  isDisabledCheckbox(item?: RequestModel) {
    return !(
      ((item && item?.pic === this.currUser?.username) ||
        this.listRequestTypeForward.includes(item?.requestTypeCode!)) &&
      [
        Resolution.NBU_SALE_PENDING,
        Resolution.NBU_INPROCESS,
        Resolution.COL_SALE_PENDING,
        Resolution.OP_COL_USER_INPROCESS,
        Resolution.OP_NBU_UNDERWRITING_INPROCESS,
        Resolution.OP_CLAIM_INVESTIGATOR_INPROCESS,
      ].includes(<string>item?.resolution)
    );
  }

  isDisabledCheckboxHeader() {
    return !!this.checkboxRows && this.checkboxRows!.filter((item) => item!.disabled)!.length > 0;
  }

  ngAfterViewChecked() {
    //your code to update the model
    this.ref.detectChanges();
  }

  isActionPending(item: RequestModel) {
    const isCreateNBU = !!this.sessionService
      .getSessionData(SessionKey.Menu)
      ?.menuOfUser?.find((item: any) => item.rsname === FunctionCode.RefundRequest);
    const isCreateCOL = !!this.sessionService
      .getSessionData(SessionKey.Menu)
      ?.menuOfUser?.find((item: any) => item.rsname === FunctionCode.CollectionRequest);

    return (
      this.currUser?.username === item?.createdBy &&
      [Resolution.NBU_SALE_PENDING, Resolution.COL_SALE_PENDING].includes(<string>item.resolution) &&
      ((this.requestTypeNbu.includes(item.requestTypeCode!) && isCreateNBU) ||
        (!this.requestTypeNbu.includes(item.requestTypeCode!) && isCreateCOL))
    );
  }

  onChangeDepartment() {
    const roleNames: string[] = [];
    if (this.params.departments?.includes('NBU')) {
      roleNames.push(Roles.OP_NBU_USER);
    }
    if (this.params.departments?.includes('COL')) {
      roleNames.push(Roles.OP_COL_USER);
    }
    if (this.params.departments?.includes('CLAIM')) {
      roleNames.push(Roles.OP_CLAIM_USER);
    }
    this.service.getPic({ roleName: roleNames }).subscribe({
      next: (data) => {
        this.stateData.listPic = data;
      },
    });
  }
}
