import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RequestListComponent } from "./pages";
import { FunctionCode } from "@cores/utils/enums";

const routes: Routes = [
  {
    path: '',
    data: {
      functionCode: FunctionCode.RequestManager,
    },
    children: [
      {
        path: '',
        component: RequestListComponent,
      },
      {
        path: 'detail/fee-refund/:requestId/:type',
        loadChildren: () => import('../detail-request/fee-refund/fee-refund.module').then((m) => m.FeeRefundModule),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/nbu/:requestId/:type',
        loadChildren: () => import('../detail-request/nbu/nbu.module').then((m) => m.NbuModule),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/lapse-reversal/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/lapse-reversal/lapse-reversal.module').then((m) => m.LapseReversalModule),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/submit-payment-receipt/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/submit-payment-receipt/submit-payment-receipt.module').then(
            (m) => m.SubmitPaymentReceiptModule
          ),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/premium-adjustment/:requestId/:type',
        loadChildren: () =>
          import('../detail-request/premium-adjustment/premium-adjustment.module').then(
            (m) => m.PremiumAdjustmentModule
          ),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        },
      },
      {
        path: 'detail/claim',
        loadChildren: () => import('../detail-request/claim-tpa/claim-tpa.module').then(m => m.ClaimTPAModule),
        data: {
          breadcrumb: 'LAYOUTS.BREADCRUMB.DETAIL_REQUEST',
        }
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestsRoutingModule {}
