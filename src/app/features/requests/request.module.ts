import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { pages } from './pages';
import { RequestsRoutingModule } from './request-routing.module';
import { components } from './components';

@NgModule({
  imports: [RequestsRoutingModule, SharedModule],
  declarations: [...pages, ...components],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class RequestsModule {}
