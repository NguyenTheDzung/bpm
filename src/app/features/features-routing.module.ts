import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeaturesComponent } from './features.component';
import { FunctionCode } from '@cores/utils/enums';
import { FunctionGuardService } from '@cores/services/function-guard.service';

const routes: Routes = [
  {
    path: '',
    component: FeaturesComponent,
    children: [
      {
        path: 'dashboard',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.Dashboard}`,
        },
        loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },

      {
        path: 'administration',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.CategoryManager}`,
          disabledUrl: true,
        },
        loadChildren: () => import('./administration/administration.module').then((m) => m.AdministrationModule),
      },
      {
        path: 'user-role-administration',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.UserRoleManager}`,
          disabledUrl: true,
        },
        loadChildren: () =>
          import('./user-role-administration/user-role-administration.module').then(
            (m) => m.UserRoleAdministrationModule
          ),
      },
      {
        path: 'requests',
        canActivate: [FunctionGuardService],
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.RequestManager}`,
          functionCode: FunctionCode.RequestManager,
        },
        loadChildren: () => import('./requests/request.module').then((m) => m.RequestsModule),
      },

      {
        path: 'configuration',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.Config}`,
          disabledUrl: true,
        },
        loadChildren: () => import('./configuration/configuration.module').then((m) => m.ConfigurationModule),
      },
      {
        path: 'create-requests',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.CreateRequest}`,
          disabledUrl: true,
        },
        loadChildren: () => import('./create-requests/create-requests.module').then((m) => m.CreateRequestsModule),
      },
      {
        path: 'history',
        loadChildren: () => import('./history/history.module').then((m) => m.HistoryModule),
      },
      {
        path: 'report',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.Report}`,
          disabledUrl: true,
        },
        loadChildren: () => import('./reports/reports.module').then((m) => m.ReportsModule),
      },
      {
        path: 'approves',
        data: {
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.Approves}`,
          disabledUrl: true,
        },
        loadChildren: () => import('./approves/approves.module').then((m) => m.ApprovesModule),
      },
      {
        path: 'category',
        canActivate: [FunctionGuardService],
        data: {
          functionCode: FunctionCode.ConfigDB,
          breadcrumb: `LAYOUTS.MENU.${FunctionCode.ConfigDB}`,
        },
        loadChildren: () => import('./category/category.module').then((m) => m.CategoryModule),
      },
      {
        path: 'claim',
        loadChildren: () => import('./claims/claims.module').then((m) => m.ClaimsModule),
      },
      { path: '**', redirectTo: 'notfound', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeaturesRoutingModule {}
