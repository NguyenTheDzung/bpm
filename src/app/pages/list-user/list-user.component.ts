import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../../user.service";
import {ConfirmationService, Message, PrimeNGConfig} from "primeng/api";
import {
  catchError,
  defer, delay,
  find,
  from,
  fromEvent,
  fromEventPattern,
  interval, map, merge,
  Observable,
  of, pluck, reduce,
  throwError,
  timer
} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
  providers: [ConfirmationService]
})


export class ListUserComponent implements OnInit {
  data: any[] = []
  positionData: any[] = [];
  statusData: any[] = [];
  classData: any[] = [];
  jobData: any[] = [];
  searchForm: any = FormGroup;
  dialog: boolean = false;
  edit: boolean = false;
  id: number = 0;
  totalRecord: number = 0
  msgs: Message[] = [];
  pageLinks = 2;
  pageNo: number = 0;
  setInit: boolean = false;


  @ViewChild('paginator') paginator: any;

  ngOnInit() {
    this.primengConfig.ripple = true;
    this.searchForm = new FormGroup({
      'searchData': new FormGroup({
        'username': new FormControl(''),
        'keyEmployee': new FormControl(''),
        'position': new FormControl(''),
        'status': new FormControl(''),
        'name': new FormControl(''),
      }),
    });
    this.dataSearch();
    this.loadData();
    this.loadDepartment();
    this.setInit = true;
  }

  constructor(public userService: UserService,
              private confirmationService: ConfirmationService,
              private router: Router,
              private primengConfig: PrimeNGConfig,
  ) {

  }

  dataSearch() {
    this.userService.getData(0, this.setInit == false ? 10 : this.paginator.rows, this.searchForm.value.searchData.username, this.searchForm.value.searchData.position == null ? '' : this.searchForm.value.searchData.position,
      this.searchForm.value.searchData.status == null ? '' : this.searchForm.value.searchData.status,
      this.searchForm.value.searchData.keyEmployee, this.searchForm.value.searchData.name).then(f => {
      this.data = f;
    })
    this.paginator?.changePage(0);
  }

  paginate(event: any) {
    this.pageNo = event.page;
    this.userService.getData(event.page + 1, event.rows, this.searchForm.value.searchData.username, this.searchForm.value.searchData.position == null ? '' : this.searchForm.value.searchData.position,
      this.searchForm.value.searchData.status == null ? '' : this.searchForm.value.searchData.status,
      this.searchForm.value.searchData.keyEmployee, this.searchForm.value.searchData.name).then(f => {
      this.data = f
    })
  }

  searchDataGetPaginator() {
    if ((!this.searchForm.value.searchData.username || this.searchForm.value.searchData.username == '')
      && (!this.searchForm.value.searchData.keyEmployee || this.searchForm.value.searchData.keyEmployee == '')
      && (!this.searchForm.value.searchData.position || this.searchForm.value.searchData.position == '')
      && (!this.searchForm.value.searchData.status || this.searchForm.value.searchData.status == '')
      && (!this.searchForm.value.searchData.name || this.searchForm.value.searchData.name == '')) {
      this.loadData();
      this.dataSearch()
    } else {
      this.userService.getData(0, 10000000, this.searchForm.value.searchData.username, this.searchForm.value.searchData.position == null ? '' : this.searchForm.value.searchData.position,
        this.searchForm.value.searchData.status == null ? '' : this.searchForm.value.searchData.status,
        this.searchForm.value.searchData.keyEmployee, this.searchForm.value.searchData.name).then(f => {
        this.data = f;
        this.totalRecord = this.data.length;
      })
    }
    this.paginator?.changePage(0);
  }

  loadDepartment() {
    this.userService.getListDepartment().subscribe({
      next: (res) => {
        this.positionData = res
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  loadData() {
    this.statusData = []
    this.classData = []
    this.jobData = []
    this.userService.getData(0, 100000, '', '', '', '', '').then(f => {
      if (f.length > 0 && f) {
        this.totalRecord = f.length;
      }
      f.forEach((f: any) => {

        this.statusData.push(f)
        this.classData.push(f)
        this.jobData.push(f)
      })
      this.statusData = [...new Set(this.statusData)].map((d: any) => {
          return {
            key: d.id,
            name: d.status
          }
        }
      )
      this.classData = [...new Set(this.classData)].map((d: any) => {
          return {
            key: d.id,
            name: d.class
          }
        }
      )
      this.jobData = [...new Set(this.jobData)].map((d: any) => {
          return {
            key: d.id,
            name: d.jobTitle
          }
        }
      )
    });
    setTimeout(() => {
      this.positiondropDown();
    }, 500)

  }

  positiondropDown() {
    let results2: any = []
    let results3: any = []
    let results4: any = []

    this.statusData.forEach(
      (value: any, index: number) => {
        if (results2.find((rsValue: any) => rsValue.name == value.name)) {
          this.statusData = this.statusData.splice(index, 1)
          return
        } else {
          results2.push(value)
          this.statusData = this.statusData.splice(index, 1)
        }
      }
    )
    this.statusData = results2

    this.classData.forEach(
      (value: any, index: number) => {
        if (results3.find((rsValue: any) => rsValue.name == value.name)) {
          this.classData = this.classData.splice(index, 1)
          return
        } else {
          results3.push(value)
          this.classData = this.classData.splice(index, 1)
        }
      }
    )
    this.classData = results3

    this.jobData.forEach(
      (value: any, index: number) => {
        if (results4.find((rsValue: any) => rsValue.name == value.name)) {
          this.jobData = this.jobData.splice(index, 1)
          return
        } else {
          results4.push(value)
          this.jobData = this.jobData.splice(index, 1)
        }
      }
    )
    this.jobData = results4
  }


  handleEdit(value: number) {
    this.edit = true;
    this.id = value
    this.dialog = true;
  }


  deleteData(id: number) {
    this.userService.removeData(id).subscribe({
      next: (res) => {
        this.loadData();
      },
      error: (err) => {
        console.log(err)
      }
    })
    this.dataSearch();
  }

  confirmDelete(id: number) {
    this.confirmationService.confirm({
      message: 'Bạn có chắc muốn xoá bản ghi này?',
      header: 'Lưu ý',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.deleteData(id)
        this.msgs = [{severity: 'info', summary: 'Thông báo', detail: 'Xoá thành công'}];
      },
      reject: () => {
      }
    });
  }

  onSubmit() {
  }

  showDialog() {
    // this.router.navigate(['/404']);
    this.dialog = true
  }

  changeEdit(newDisable: boolean) {
    this.edit = newDisable
  }

  changeDialog(newDialog: boolean) {
    this.dialog = newDialog
    this.dataSearch()
  }


}

const observable = new Observable(function (observer) {
  const id = setInterval(() => {
    observer.next('Hello RxJs');
    // observer.complete();
  }, 1000);

  return function () {
    observer.complete();
    clearInterval(id)
  }
})
const subscription = observable.subscribe(val => console.log(val), null, () => console.log('complete'));

setTimeout(() => {
  subscription.unsubscribe();
}, 5000)

const observer = {
  next: (val: any) => console.log(val),
  error: (err: any) => console.log(err),
  complete: () => console.log('completed')
}
//of //any value with of operator //emit all array
of(1, 2, 3, 'hello', 'world', [1, 2, 3], {foo: 'bar'}).subscribe(observer);

//from //input: observable, promise, array, iterale. map character: every char on a line // emit every element
from([1, 2, 3]).subscribe(observer)

//fromEvent // lister event one the display // lister in element have event
fromEvent(document, 'click').subscribe(observer)

//fromEventPattern // advanced fromEvent
fromEventPattern(
  (handler) => {
    document.addEventListener('click', handler);
  },
  (handler) => {
    document.removeEventListener('click', handler)
  }
).subscribe(observer)

//interval
interval(1000)

timer(1000)//setTimeout but you must don't clear timeout
timer(1000, 1000)

//defer
const randoms = defer(() => of(Math.random()));

//-----------------------------------------------------------------------------
//Transformation Operators
const users = [
  {
    id: 'dfe1111', username: 'dungnt', fisrtName: 'Nguyen The', lastName: 'Dzung', postCount: 5
  },
  {
    id: 'dfe2222', username: 'yang.h', fisrtName: 'Yang', lastName: 'Hunko', postCount: 22
  }
]

const usersVN = users.map(user => {
  return {
    ...user,
    fullName: `${user.fisrtName} ${user.lastName}`
  }
})
console.log(usersVN)

//parent observable
from(users).pipe(map(data => {
  console.log('inside map: ', data);
  return data;
} )).subscribe(observer)


//map
merge(
  of(users[0]).pipe(delay(2000)),
  of(users[1]).pipe(delay(4000))
).pipe(
  map(user =>({...user, fullName: `${user.fisrtName} ${user.lastName}`}) )
).subscribe(observer)

//pluck
const param$ = of({id: 123, foo: {bar: 'chart'}});

const id$ = param$.pipe(pluck( 'foo', 'bar')).subscribe(observer)

//reduce
const totalCount = merge(
  of(users[0]).pipe(delay(2000)),
  of(users[1]).pipe(delay(4000))
).pipe(reduce((acc, cur)=> acc + cur.postCount, 0)).subscribe(observer)
