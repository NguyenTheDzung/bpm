import {Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import {getErrorValidate} from '@cores/utils/functions';
import * as _ from 'lodash';
import {InputNumber} from 'primeng/inputnumber';

@Component({
  selector: 'mbal-input-number',
  templateUrl: './mbal-input-number.component.html',
  styleUrls: ['./mbal-input-number.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MBALInputNumberComponent),
      multi: true,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MBALInputNumberComponent),
      multi: true,
    },
  ],
})
export class MBALInputNumberComponent implements Validator, ControlValueAccessor {
  @Input() label: string = 'EMPTY';
  @Input() placeholder: string = '';
  @Input() showLabel: boolean = true;
  @Input() pattern: string = '';
  @Input() required?: boolean | string;
  @Input() readonly: boolean = false;
  @Input() disabled: boolean = false;
  @Input() border: boolean = true;
  @Input() useGrouping: boolean = true;
  @Input() suffix: string = '';
  @Input() prefix: string = '';
  @Input() maxLength: number = 1000;
  @Input() checkLength: boolean = false;
  @Input() fieldForm: boolean = true;
  @Input() max: number = Number.MAX_SAFE_INTEGER;
  @Input() min: number = Number.MIN_SAFE_INTEGER;
  @Input() locale: string = 'en-US';
  @Input() mode: 'decimal' | 'currency' = 'decimal';
  @Input() showButton: boolean = false;
  @Output() input = new EventEmitter();
  @Output() blur = new EventEmitter();
  @ViewChild('inputN') numberInput?: InputNumber;
  @Input() maxCommaLength: number = 0

  absControl!: AbstractControl;
  value: any;

  constructor(private el: ElementRef<HTMLElement>) {
  }

  _minFractionDigits: number = 0;

  get minFractionDigits() {
    return this._minFractionDigits;
  }

  @Input() set minFractionDigits(value: number) {
    this._minFractionDigits = value;
    if (this._maxFractionDigits < this._minFractionDigits) {
      this._maxFractionDigits = this._minFractionDigits;
    }
  }

  _maxFractionDigits: number = 0;

  get maxFractionDigits() {
    return this._maxFractionDigits;
  }

  @Input() set maxFractionDigits(value: number) {
    this._maxFractionDigits = value;
    if (this._maxFractionDigits < this._minFractionDigits) {
      this._maxFractionDigits = this._minFractionDigits;
    }
  }

  get errors() {
    return (
      (this.el.nativeElement.closest('.ng-submitted') || this.absControl?.touched || this.absControl?.dirty) &&
      this.absControl?.errors &&
      !this.readonly
    );
  }

  onInput(event: any) {
    if (this.maxLength) {
      const exactMaxLength = this.maxLength - this.maxCommaLength - this.prefix.length - this.suffix.length
      if(exactMaxLength >= String(event?.value ?? "").length) {
        this.onChange(event.value);
      }
    }else this.onChange(event.value)
    this.input.emit(event);
}

  onBlurInput() {
    this.onChange(this.value);
    this.blur.emit();
  }

  onChange = (_value: number) => {
    // This is intentional
  };

  onTouched = () => {
    // This is intentional
  };

  //Lấy ra message lỗi validate để hiển thị, nếu có nhiều lỗi -> hiển thị lỗi đầu tiên.
  getError() {
    const errorKey = Object.keys(this.absControl.errors as object)[0];
    const errorValue: any = this.absControl.errors![errorKey];
    return getErrorValidate(this.label, errorKey, errorValue);
  }

  //Dùng để check trường hiện tại có phải required hay không.
  checkRequire() {
    return (
      this.absControl?.hasValidator(Validators.required) ||
      _.isString(this.required) ||
      (_.isBoolean(this.required) && this.required)
    );
  }

  writeValue(value: any): void {
    value = !isNaN(parseFloat(value)) && isFinite(value) ? +value : null;
    this.value = value;
  }

  registerOnChange(fn: (value: number) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    this.absControl = control;
    let value = control.value;
    if (this.checkLength && value.toString().length > this.maxLength) {
      return {
        maxlength: {
          requiredLength: this.maxLength,
        },
      };
    }
    if (Number.isFinite(control.value)) {
      if (Number.isFinite(this.max) && this.max < control.value) {
        return {
          max: {
            max: this.max,
            actual: control.value,
          },
        };
      }
      if (Number.isFinite(this.min) && this.min > control.value) {
        return {
          min: {
            min: this.min,
            actual: control.value,
          },
        };
      }
    }
    return null;
  }
}
