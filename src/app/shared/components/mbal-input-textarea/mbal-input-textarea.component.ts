import { Component, ElementRef, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import { getErrorValidate } from '@cores/utils/functions';
import * as _ from 'lodash';

@Component({
  selector: 'mbal-input-textarea',
  templateUrl: './mbal-input-textarea.component.html',
  styleUrls: ['./mbal-input-textarea.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MBALInputTextAreaComponent),
      multi: true,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MBALInputTextAreaComponent),
      multi: true,
    },
  ],
})
export class MBALInputTextAreaComponent implements Validator, ControlValueAccessor {
  @Input() label: string = 'EMPTY';
  @Input() placeholder: string = 'EMPTY';
  @Input() showLabel: boolean = true;
  @Input() pattern: string = '';
  @Input() autoResize: boolean = true;
  @Input() required?: boolean | string;
  @Input() readonly: boolean = false;
  @Input() disabled: boolean = false;
  @Input() rows: number = 1;
  @Input() cols: number = 1;
  @Input() border: boolean = true;
  @Input() fieldForm: boolean = true;
  @Input() maxlength: number | null = null;
  @Output() blur = new EventEmitter();

  value: any;
  absControl!: AbstractControl;

  constructor(private el: ElementRef<HTMLElement>) {}

  get errors() {
    return (
      (this.el.nativeElement.closest('.ng-submitted') || this.absControl?.touched || this.absControl?.dirty) &&
      this.absControl?.errors &&
      !this.readonly
    );
  }

  onModelChange(event: any) {
    this.onChange(event);
  }

  onChange = (_value: string) => {
    // This is intentional
  };

  onTouched = () => {
    // This is intentional
  };

  //Lấy ra message lỗi validate để hiển thị, nếu có nhiều lỗi -> hiển thị lỗi đầu tiên.
  getError() {
    const errorKey = Object.keys(this.absControl.errors as object)[0];
    const errorValue: any = this.absControl.errors![errorKey];
    return getErrorValidate(this.label, errorKey, errorValue);
  }

  //Dùng để check trường hiện tại có phải required hay không.
  checkRequire() {
    return (
      this.absControl?.hasValidator(Validators.required) ||
      _.isString(this.required) ||
      (_.isBoolean(this.required) && this.required)
    );
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    this.absControl = control;
    if (this.maxlength && control.value?.length > this.maxlength) {
      return {
        maxlength: {
          requiredLength: this.maxlength,
        },
      };
    }
    return null;
  }

  onBlur(event: any) {
    this.blur.emit(event);
  }
}
