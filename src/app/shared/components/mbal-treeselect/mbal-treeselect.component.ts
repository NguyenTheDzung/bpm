import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import { TreeNode } from 'primeng/api';
import { getErrorValidate } from '@cores/utils/functions';
import * as _ from 'lodash';
import { TreeSelect } from 'primeng/treeselect';

@Component({
  selector: 'mbal-treeselect',
  templateUrl: './mbal-treeselect.component.html',
  styleUrls: ['./mbal-treeselect.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MBALTreeSelectComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MBALTreeSelectComponent),
      multi: true,
    },
  ],
})
export class MBALTreeSelectComponent implements ControlValueAccessor, Validator, OnChanges {
  @Input() label: string = 'EMPTY';
  @Input() className?: string;
  @Input() search: boolean = false;
  @Input() disabled: boolean = false;
  @Input() readonly: boolean = false;
  @Input() filter: boolean = false;
  @Input() showClear: boolean = false;
  @Input() showLabel: boolean = true;
  @Input() optionLabel: string = 'name';
  @Input() optionValue: string = 'code';
  @Input() multiple: boolean = false;
  @Input() border: boolean = false;
  @Input() propagateSelectionUp: boolean = true;
  @Input() propagateSelectionDown: boolean = true;
  @Input() options: Array<any> = [];
  @Input() scrollHeight = '250px';
  @Input() selectionMode: string = 'single';
  @Input() display: string = 'comma';
  @Input() emptyMessage: string = 'Không có kết quả nào được tìm thấy';
  @Input() treeFromRelation?: string;
  @Input() treeToRelation?: string;
  @Input() required?: boolean | string;
  @Input() fieldForm: boolean = true;
  @Output() itemSelected = new EventEmitter();

  absControl!: AbstractControl;
  value: any;
  nodes: TreeNode[] = [];

  constructor(private el: ElementRef<HTMLElement>) {}

  get errors() {
    return (
      (this.el.nativeElement.closest('.ng-submitted') || this.absControl?.touched || this.absControl?.dirty) &&
      this.absControl?.errors &&
      !this.readonly
    );
  }

  ngOnChanges(_changes: SimpleChanges): void {
    if (this.treeFromRelation && this.treeToRelation) {
      this.nodes = this.mapTreeView(this.options);
    }
  }

  mapTreeView(items: any[]) {
    if (items) {
      const list: TreeNode[] = [];
      items.forEach((item) => {
        list.push({
          label: item[this.optionLabel],
          key: item[this.optionValue],
          expanded: true,
          data: {
            ...item,
          },
          children: [],
        });
      });
      const root = list.filter((item: any) => !item.data[this.treeFromRelation!]);
      root.forEach((item: any) => {
        this.getChildrenNode(item, list);
      });
      return root;
    }
    return [];
  }

  getChildrenNode(currentItem: TreeNode, items: TreeNode[]) {
    items.forEach((item) => {
      if (item.data[this.treeFromRelation!] === currentItem.data[this.treeToRelation!]) {
        currentItem.children?.push(item);
        this.getChildrenNode(item, items);
      }
    });
    if (currentItem.children?.length === 0) {
      currentItem.expanded = false;
    }
  }

  getNodeByValue(tree: TreeNode[], value: any): any {
    let result = null;
    if (!tree || tree.length === 0) {
      return result;
    }
    for (const item of tree) {
      if (value === item.data[this.optionValue]) {
        return item;
      } else if (item.children) {
        result = this.getNodeByValue(item.children, value);
        if (result) {
          break;
        }
      }
    }
    return result;
  }

  writeValue(value: any): void {
    if (this.selectionMode === 'single') {
      value = this.getNodeByValue(this.nodes, value);
      this.value = value;
    } else {
      let newValue: TreeNode[] = [];
      value?.forEach((item: any) => {
        newValue.push(this.getNodeByValue(this.nodes, item));
      });
      newValue = newValue.filter((item) => item !== null);
      this.value = newValue;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onChangeValue() {
    if (this.selectionMode === 'single') {
      this.onChange(this.value?.data[this.optionValue] || null);
    } else {
      this.onChange(this.value?.map((item: TreeNode) => item?.data[this.optionValue]) || null);
    }
  }

  onChange = (_value: string) => {
    // This is intentional
  };

  onTouched = () => {
    // This is intentional
  };

  //Lấy ra message lỗi validate để hiển thị, nếu có nhiều lỗi -> hiển thị lỗi đầu tiên.
  getError() {
    const errorKey = Object.keys(this.absControl.errors as object)[0];
    const errorValue: any = this.absControl.errors![errorKey];
    return getErrorValidate(this.label, errorKey, errorValue);
  }

  //Dùng để check trường hiện tại có phải required hay không.
  checkRequire() {
    return (
      this.absControl?.hasValidator(Validators.required) ||
      _.isString(this.required) ||
      (_.isBoolean(this.required) && this.required)
    );
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    this.absControl = control;
    return null;
  }

  onFilterTree(treeSelect: TreeSelect) {
    treeSelect.filterValue = treeSelect.filterValue.trim();
    treeSelect.treeViewChild._filter(treeSelect.filterValue);
  }

  clearValue() {
    this.value = null;
    this.onChange(this.value);
  }
}
