import { AfterContentInit, Component, ElementRef, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
// @ts-ignore
import * as BpmnJS from 'bpmn-js/dist/bpmn-modeler.production.min.js';
import { RequestService } from '@requests/services/request.service';
import { ProcessSearch } from '@reports/models/report.model';
import { IBpmnTask } from '@requests/models/request.model';
import { createErrorMessage } from '@cores/utils/functions';
import { BaseComponent } from '@shared/components';
import { isNumber } from 'lodash';

@Component({
  selector: 'app-diagram-bpmn',
  templateUrl: './diagram-bpmn.component.html',
  styleUrls: ['./diagram-bpmn.component.scss'],
})
export class DiagramBpmnComponent extends BaseComponent implements OnInit, AfterContentInit, OnDestroy {
  private bpmnJS: BpmnJS;
  xml?: string;
  listTasks: IBpmnTask[] = [];
  listIdLine: string[] = [];
  currentTaskDefKey: string[] = [];

  @Input() set tabIndex(value: number) {
    this.hasTab = isNumber(value);
    if (this.hasTab && this.xml) {
      setTimeout(() => {
        this.bpmnJS.importXML(this.xml);
        // if (this.elHost.nativeElement.hasAttribute('ng-reflect-tab-index')) {
        const heightTabviewNav = document.getElementsByClassName('p-tabview-nav-container').item(0)!.clientHeight;
        const elLayoutContent = document.getElementsByClassName('layout-content').item(0)!;
        const heightLayoutContent = elLayoutContent.clientHeight;
        const styleDeclaration = getComputedStyle(elLayoutContent);
        const paddingTop = +styleDeclaration.paddingTop.replace('px', '');
        const paddingBottom = +styleDeclaration.paddingBottom.replace('px', '');
        this.elHost.nativeElement.style.height = `${
          heightLayoutContent - heightTabviewNav - paddingTop - paddingBottom
        }px`;
        // }
      }, 200);
    }
  }

  hasTab: boolean = false;

  @ViewChild('ref', { static: true }) private bpmnEl?: ElementRef;

  constructor(injector: Injector, private requestService: RequestService, private elHost: ElementRef<HTMLElement>) {
    super(injector);
    this.bpmnJS = new BpmnJS({
      additionalModules: [
        {
          // contextPad: ['value', {}],
          // contextPadProvider: ['value', {}],
          // palette: ['value', {}],
          // paletteProvider: ['value', {}],
          // dragging: ['value', {}],
          // move: ['value', {}],
          // create: ['value', {}],
        },
      ],
    });
    const requestId = this.route.snapshot.paramMap.get('requestId');
    if (requestId) {
      this.getFileBpmnByRequestId(requestId);
    }
  }

  ngOnInit(): void {
    this.bpmnJS.on('import.done', (result: any) => {
      if (!result.error) {
        const canvas = this.bpmnJS.get('canvas');
        const overlays = this.bpmnJS.get('overlays');
        const modeling = this.bpmnJS.get('modeling');
        const elementRegistry = this.bpmnJS.get('elementRegistry');
        if (this.hasTab) {
          this.currentTaskDefKey.forEach((defKey) => {
            canvas?.addMarker(defKey, 'highlight-green');
          });
          // highlight các serviceTask, userTask, exclusiveGateway đi qua
          this.listTasks?.forEach((item) => {
            if (
              !['startEvent', 'endEvent'].includes(item.type) &&
              !this.currentTaskDefKey.includes(item.activityDefKey)
            ) {
              canvas?.addMarker(item.activityDefKey, 'highlight-yellow');
            }
          });

          // highlight các đường đi qua
          const elementsToColor = this.listIdLine?.map((id) => elementRegistry.get(id));
          modeling.setColor(elementsToColor, {
            stroke: '#f4be5e',
            fill: '#f4be5e',
          });
        }

        // hiển thị thời gian xử lý task
        this.listTasks?.forEach((item) => {
          if (item.duration) {
            const time = this.calculatorTimeTask(item.duration);

            let html =
              '<div style="width:100px"><div style="margin:0 8px; text-align: center; color: red; font-weight: bold; background: #fff">';
            html += time.description;
            html += '</div></div>';
            overlays.add(item.activityDefKey, {
              html: html,

              position: {
                left: 0,
                bottom: 30,
              },
            });
          }
        });
        canvas?.zoom('fit-viewport');
        this.loadingService.complete();
      }
    });
  }

  getFileBpmnByRequestId(requestId: string, reload = false) {
    this.requestService.getFileBpmnByRequestId(requestId!).subscribe({
      next: (data) => {
        this.xml = data.content;
        this.currentTaskDefKey = data.currentActivities?.map((item) => item.defKey) ?? [];
        this.listTasks = data.taskMonitoring;
        this.listIdLine = data.passedSequentFlows ?? [];
        this.bpmnJS.importXML(this.xml);
      },
      error: () => {
        if (reload) {
          this.loadingService.complete();
        }
      },
    });
  }

  calculatorTimeTask(milli: number) {
    let seconds = Math.floor(milli / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    let days = Math.floor(hours / 24);

    return (
      (days && hours % 24 && { value: days, description: `${days} days ${hours % 24} hours` }) ||
      (days && { value: days, description: `${days} days` }) ||
      (hours && minutes % 60 && { value: hours, description: `${hours} hours ${minutes % 60} minutes` }) ||
      (hours && { value: hours, description: `${hours} hours` }) ||
      (minutes && seconds % 60 && { value: minutes, description: `${minutes} minutes ${seconds % 60} seconds` }) ||
      (minutes && { value: minutes, description: `${minutes} minutes` }) || {
        value: seconds,
        description: `${seconds} seconds`,
      }
    );
  }

  getFileBpmnByProcess(params: ProcessSearch) {
    this.loadingService.start();
    this.requestService.getFileBpmnByProcessTypeAndVersion(params).subscribe({
      next: (data) => {
        this.xml = data.content;
        this.listTasks = data.taskMonitoring;
        this.bpmnJS.importXML(this.xml);
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
      },
    });
  }

  ngAfterContentInit(): void {
    this.bpmnJS.attachTo(this.bpmnEl?.nativeElement);
  }

  override onDestroy(): void {
    this.bpmnJS.destroy();
  }

  resetZoom() {
    this.bpmnJS.get('canvas').zoom('fit-viewport');
  }

  zoomIn() {
    this.bpmnJS.get('zoomScroll').stepZoom(1);
  }

  zoomOut() {
    this.bpmnJS.get('zoomScroll').stepZoom(-1);
  }

  reloadData() {
    const requestId = this.route.snapshot.paramMap.get('requestId');
    if (requestId) {
      this.loadingService.start();
      this.getFileBpmnByRequestId(requestId, true);
    }
  }
}
