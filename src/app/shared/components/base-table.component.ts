import { Subscription } from 'rxjs';
import { ChangeDetectorRef, Component, Injector, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { UserProfileModel } from 'src/app/core/models/user-profile.model';
import { CommonCategoryService } from 'src/app/core/services/common-category.service';
import { SessionService } from 'src/app/core/services/session.service';
import { StreamDataService } from 'src/app/core/services/stream-data.service';
import { DialogService } from 'primeng/dynamicdialog';
import { NotificationMessageService } from 'src/app/core/services/message.service';
import { Actions, ScreenType, SessionKey } from 'src/app/core/utils/enums';
import { FunctionModel } from 'src/app/core/models/function.model';
import { DataTable } from 'src/app/core/models/data-table.model';
import { BaseService } from 'src/app/core/services/base.service';
import { PaginatorModel } from 'src/app/core/models/paginator.model';
import { ActionConfig } from 'src/app/core/models/action-config.model';
import * as lodash from 'lodash';
import { cloneDeep } from 'lodash';
import { LoadingService } from '@cores/services/loading.service';
import { cleanData, createErrorMessage } from '@cores/utils/functions';
import { TranslateService } from '@ngx-translate/core';
import { RequestType } from '@create-requests/utils/constants';
import { AuthService } from '@cores/services/auth.service';

@Component({
  template: ` <ng-content></ng-content>`,
  providers: [DialogService],
})
export class BaseTableComponent<M> implements OnDestroy {
  public objFunction: FunctionModel | undefined;
  public currUser: UserProfileModel;
  public action = Actions;
  public loadingService!: LoadingService;
  stateData: any;
  propData: any;
  dataTable: DataTable<M> = {
    content: [],
    currentPage: 0,
    size: 10,
    totalElements: 0,
    totalPages: 0,
    first: 0,
    numberOfElements: 0,
  };
  configAction: ActionConfig | undefined;
  prevParams: any;
  params: M | FormGroup | undefined;
  fileNameExcel = 'list-data.xlsx';
  subscription: Subscription | undefined;
  subscriptions: Subscription[] = [];
  isPopupShow = false;
  protected messageService!: NotificationMessageService;
  protected dialogService!: DialogService;
  protected router!: Router;
  protected route!: ActivatedRoute;
  protected location!: Location;
  protected streamDataService!: StreamDataService;
  protected sessionService!: SessionService;
  protected ref!: ChangeDetectorRef;
  protected commonService!: CommonCategoryService;
  protected fb!: FormBuilder;
  protected authService!: AuthService;
  translateService!: TranslateService;

  constructor(private injector: Injector, protected serviceBase: BaseService) {
    this.init();
    this.initConfigAction();
    this.currUser = this.sessionService?.getSessionData(SessionKey.UserProfile);
  }

  get requestType() {
    return RequestType;
  }

  init() {
    this.messageService = this.injector.get(NotificationMessageService);
    this.dialogService = this.injector.get(DialogService);
    this.fb = this.injector.get(FormBuilder);
    this.router = this.injector.get(Router);
    this.route = this.injector.get(ActivatedRoute);
    this.location = this.injector.get(Location);
    this.streamDataService = this.injector.get(StreamDataService);
    this.sessionService = this.injector.get(SessionService);
    this.ref = this.injector.get(ChangeDetectorRef);
    this.commonService = this.injector.get(CommonCategoryService);
    this.loadingService = this.injector.get(LoadingService);
    this.translateService = this.injector.get(TranslateService);
    this.authService = this.injector.get(AuthService);
    const functionCode = this.route.snapshot.data['functionCode'];
    if (functionCode) {
      this.objFunction = this.sessionService
        .getSessionData(SessionKey.Menu)
        ?.menuOfUser?.find((item: any) => item.rsname === functionCode);
    }
  }

  checkFunctionCreate() {
    return !!this.objFunction?.scopes?.includes(this.action.Create);
  }

  checkFunctionUpdate() {
    return !!this.objFunction?.scopes?.includes(this.action.Update);
  }

  checkFunctionDelete() {
    return !!this.objFunction?.scopes?.includes(this.action.Delete);
  }

  initConfigAction() {
    console.debug();
  }

  getState() {
    this.serviceBase.getState().subscribe({
      next: (state) => {
        this.propData = cloneDeep(state);
        this.stateData = cloneDeep(state);
        this.mapState();
        this.search();
      },
    });
  }

  mapState() {
    console.debug();
  }

  search(firstPage?: boolean) {
    if (firstPage) {
      this.dataTable.currentPage = 0;
    }

    this.loadingService.start();
    const params = this.mapDataSearch();

    this.serviceBase.search(params).subscribe({
      next: (data) => {
        this.dataTable = data;
        if (this.dataTable.content.length === 0) {
          this.messageService.warn('MESSAGE.notfoundrecord');
        }
        this.loadingService.complete();
        this.prevParams = params;
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  pageChange(paginator: PaginatorModel) {
    this.dataTable.currentPage = paginator.page;
    this.dataTable.size = paginator.rows;
    this.dataTable.first = paginator.first;
    this.search();
  }

  mapDataSearch() {
    const params = {
      page: this.dataTable.currentPage,
      size: this.dataTable.size,
      ...this.params,
    };
    return <M>params;
  }

  viewCreate() {
    if (!this.configAction?.component || this.isPopupShow) {
      return;
    }
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toUpperCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        screenType: ScreenType.Create,
        state: this.propData,
      },
    });
    this.isPopupShow = true;
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        this.isPopupShow = false;
        if (isSuccess) {
          this.search();
        }
      },
    });
  }

  viewEdit(code: string) {
    if (this.loadingService.loading || !this.configAction?.component || this.isPopupShow) {
      return;
    }
    this.loadingService.start();
    this.serviceBase.findByCode(code).subscribe({
      next: (data: M) => {
        const dialog = this.dialogService?.open(this.configAction!.component, {
          header: `Cập nhật ${this.configAction!.title.toLowerCase()}`,
          showHeader: false,
          width: this.configAction!.dialog?.width || '85%',
          data: {
            model: data,
            state: this.propData,
            screenType: ScreenType.Update,
          },
        });
        this.isPopupShow = true;

        dialog?.onClose.subscribe({
          next: (isSuccess) => {
            if (isSuccess) {
              this.search();
            }
            this.isPopupShow = false;
          },
        });
        this.loadingService.complete();
      },
      error: (e) => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  viewDetail(code: string) {
    if (this.loadingService.loading || !this.configAction?.component || this.isPopupShow) {
      return;
    }
    this.loadingService.start();
    this.serviceBase.findByCode(code).subscribe({
      next: (data: any) => {
        this.dialogService?.open(this.configAction!.component, {
          header: `${this.configAction!.title.toLowerCase()}`,
          showHeader: false,
          width: this.configAction!.dialog?.width || '85%',
          data: {
            model: data,
            state: this.stateData,
            screenType: ScreenType.Detail,
          },
        });
        this.loadingService.complete();
      },
      error: (e) => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  deleteItem(id: string | number) {
    if (this.loadingService.loading) {
      return;
    }
    this.messageService.confirm().subscribe((isConfirm) => {
      if (isConfirm) {
        this.loadingService.start();
        this.serviceBase.delete(id).subscribe({
          next: () => {
            this.messageService.success('MESSAGE.DELETE_SUCCESS');
            this.search();
          },
          error: (err) => {
            this.loadingService.complete();
            this.messageService.success(createErrorMessage(err));
          },
        });
      }
    });
  }

  exportExcel(isBase64?: boolean) {
    if (this.loadingService.loading || this.dataTable.totalElements === 0) {
      return;
    }
    this.loadingService.start();
    this.serviceBase.exportExcel(this.fileNameExcel, cleanData(this.prevParams) || {}, isBase64).subscribe({
      next: (res) => {
        this.loadingService.complete();
        console.log(res);
      },
      error: () => {
        this.loadingService.complete();
        this.messageService.error('MESSAGE.E_INTERNAL_SERVER');
      },
    });
  }

  getValue(obj: any, path: string) {
    return lodash.get(obj, path);
  }

  onDestroy() {
    console.debug();
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscriptions?.forEach((sub) => {
      sub.unsubscribe();
    });
    this.onDestroy();
  }
}
