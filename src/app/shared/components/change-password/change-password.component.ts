import { Component, Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ScreenType, SessionKey } from '@cores/utils/enums';
import { BaseActionComponent } from '@shared/components';
import { createErrorMessage, validateAllFormFields } from '@cores/utils/functions';
import { ChangePasswordService } from '@shared/service/change-password.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent extends BaseActionComponent implements OnInit {
  override form = this.fb.group({
    currentPassword: ['', Validators.required],
    newPassword: ['', Validators.required],
    confirmPassword: ['', Validators.required],
  });

  constructor(inject: Injector, private service: ChangePasswordService) {
    super(inject, service);
  }

  submit() {
    if (this.loadingService.loading || this.form.invalid) {
      validateAllFormFields(this.form);
      return;
    }
    if (this.form.get('newPassword')?.value.length < 8) {
      this.messageService.error(`Vui lòng nhập ít nhất 8 ký tự`);
      return;
    }
    if (this.form.get('newPassword')?.value != this.form.get('confirmPassword')?.value) {
      this.messageService.error(`Xác nhận mật khẩu không trùng khớp`);
      return;
    }

    const user = this.sessionService.getSessionData(SessionKey.UserProfile);

    const encodedData = {
      currentPassword: btoa(this.form.get('currentPassword')?.value),
      newPassword: btoa(this.form.get('newPassword')?.value),
      confirmPassword: btoa(this.form.get('confirmPassword')?.value),
      username: user.username,
    };

    this.loadingService.start();
    this.service.create(encodedData).subscribe({
      next: () => {
        this.messageService.success('MESSAGE.SUBMIT_SUCCESS');
        this.loadingService.complete();
        localStorage.clear();
        this.router.navigateByUrl('/login').then();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  ngOnInit(): void {
    if (this.screenType !== ScreenType.Create) {
      this.form.get('code')?.disable();
      this.form.patchValue(this.data);
    }
  }
}
