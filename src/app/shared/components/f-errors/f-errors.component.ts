import { Component, Input } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { isObject, map, replace } from 'lodash';
import { VALIDATORS_MESSAGE } from 'src/app/core/utils/constants';
import { getErrorValidate } from '@cores/utils/functions';

@Component({
  selector: 'f-errors',
  templateUrl: './f-errors.component.html',
  styleUrls: ['./f-errors.component.scss'],
})
export class ErrorComponent {
  @Input() control?: FormControl | AbstractControl | null;
  @Input() label?: string;

  getListErrors() {
    if (this.control?.touched) {
      return map(this.control?.errors, (value, key) => {
        if ((isObject(value) && key === 'maxlength') || key === 'minlength') {
          return replace(VALIDATORS_MESSAGE[key], '%d', value.requiredLength);
        }
        if ((isObject(value) && key === 'max') || key === 'min') {
          return replace(VALIDATORS_MESSAGE[key], '%d', value[key]);
        } else {
          return VALIDATORS_MESSAGE[key];
        }
      });
    }
    return null;
  }

  getError() {
    const errorKey = Object.keys(this.control!.errors as object)[0];
    const errorValue: any = this.control!.errors![errorKey];
    return getErrorValidate(this.label ?? '', errorKey, errorValue);
  }
}
