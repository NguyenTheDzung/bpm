import { Component, ElementRef, EventEmitter, forwardRef, Input, Output } from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from "@angular/forms";
import { getErrorValidate } from "@cores/utils/functions";
import * as moment from "moment";
import { CalendarView, DateFormat } from "@cores/utils/enums";


@Component({
  selector: 'mbal-calendar',
  templateUrl: './mbal-calendar.component.html',
  styleUrls: ['./mbal-calendar.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MBALCalendarComponent),
      multi: true,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MBALCalendarComponent),
      multi: true,
    },
  ],
})
export class MBALCalendarComponent implements Validator, ControlValueAccessor {
  @Input() label: string = 'EMPTY';
  @Input() placeholder: string = 'EMPTY';
  @Input() showLabel: boolean = true;
  @Input() selectionMode: 'single' | 'range' = 'single'; // single, range
  @Input() required?: boolean | string;
  @Input() readonly: boolean = false;
  @Input() maxDateLabel: string = '';
  @Input() minDateLabel: string = '';
  @Input() disabled: boolean = false;
  @Input() border: boolean = true;
  @Input() timeOnly: boolean = false;
  @Input() showOnFocus: boolean = false;
  @Input() fieldForm: boolean = true;
  @Input() multipleSeparator: string = '';
  @Input() showTime: boolean = false;
  @Input() calendarView: CalendarView = CalendarView.DatePicker
  @Input() listenOnInput: boolean = true
  @Input() listenOnSelect: boolean = true
  @Input() panelStyle?: any
  @Input() panelStyleClass: string = ""

  @Output() clickOutSide = new EventEmitter<any>();

  absControl!: AbstractControl;
  value: any;
  showSecond: boolean = false;

  dateFormat: 'dd/mm/yy' | 'mm/yy' = 'dd/mm/yy';
  markFormat: DateFormat = DateFormat.Date

  @Input() set format(value: DateFormat) {

    switch (value) {
      case DateFormat.Date:
        this.dateFormat = 'dd/mm/yy';
        this.markFormat = value
        break;

      case DateFormat.DateTime:
        this.dateFormat = 'dd/mm/yy';
        this.markFormat = value

        this.showSecond = true;
        this.showTime = true;
        break;
      case DateFormat.Month:
        this.markFormat = DateFormat.Month
        this.dateFormat = 'mm/yy'
        this.showSecond = false;
        this.showTime = false;
        break;
      default:
        this.showSecond = false;
        this.showTime = false;
        break;
    }
  }

  _maxDateToday = false;

  get maxDateToday() {
    return this._maxDateToday;
  }

  @Input() set maxDateToday(value: boolean) {
    this._maxDateToday = value;
    if (this._maxDateToday) {
      if (moment(this.maxDate).isValid() && moment(new Date()).isAfter(moment(this.maxDate), 'day')) {
        return;
      } else {
        this.maxDate = new Date();
      }
    }
  }

  _minDateToday = false;

  get minDateToday() {
    return this._minDateToday;
  }

  @Input() set minDateToday(value: boolean) {
    this._minDateToday = value;
    if (this._minDateToday) {
      this.minDate = new Date();
    }
  }

  _maxDate: any = null;

  get maxDate(): any {
    return this._maxDate;
  }

  @Input() set maxDate(value: string | Date | null) {
    if (typeof value === 'string') {
      this._maxDate = new Date(value);
    } else {
      this._maxDate = value;
    }
    if (this.maxDateToday) {
      if (moment(this.maxDate).isValid()) {
        if (moment(this.maxDate).isBefore(moment(new Date()))) {
          return;
        }
      }
      this._maxDate = new Date();
    }
  }

  _minDate: any = null;

  get minDate(): any {
    return this._minDate;
  }

  @Input() set minDate(value: string | Date | null) {
    if (typeof value === 'string') {
      this._minDate = new Date(value);
    } else {
      this._minDate = value;
    }
    if (this.minDateToday) {
      if (moment(this.minDate).isValid()) {
        if (moment(this.minDate).isAfter(moment(new Date()))) {
          return;
        }
      }
      this._minDate = new Date();
    }
  }

  constructor(private el: ElementRef<HTMLElement>) {
  }

  get errors() {
    return (
      (this.el.nativeElement.closest('.ng-submitted') || this.absControl?.touched || this.absControl?.dirty) &&
      this.absControl?.errors &&
      !this.readonly
    );
  }

  onModelChange($event: typeof this.value = this.value) {
    this.onChange($event);
  }

  onBlur() {}

  onInput(_event: any) {
    this.listenOnInput && this.onChange(this.value);
  }

  onChange = (_value: string) => {
    // This is intentional
  };

  onTouched = () => {
    // This is intentional
  };

  //Lấy ra message lỗi validate để hiển thị, nếu có nhiều lỗi -> hiển thị lỗi đầu tiên.
  getError() {
    const errorKey = Object.keys(this.absControl.errors as object)[0];
    const errorValue: any = this.absControl.errors![errorKey];
    return getErrorValidate(this.label, errorKey, errorValue);
  }

  //Dùng để check trường hiện tại có phải required hay không.
  checkRequire() {
    return this.absControl?.hasValidator(Validators.required);
  }

  writeValue(value: any): void {
    const timer = setTimeout(() => {
      if (this.selectionMode === 'single') {
        this.value = value ? new Date(value) : null;
      } else {
        this.value = Array.isArray(value) ? value?.map((date) => new Date(date)) : null;
      }
      clearTimeout(timer);
    }, 0);
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    this.absControl = control;
    return null;
  }

  onClickOutSide() {
    this.clickOutSide.emit();
  }
}
