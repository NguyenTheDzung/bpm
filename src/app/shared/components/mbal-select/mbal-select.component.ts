import {
  AfterContentInit, AfterViewInit, ChangeDetectorRef,
  Component, ContentChild, ContentChildren,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  Output, QueryList,
  SimpleChanges, TemplateRef,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import { getErrorValidate } from '@cores/utils/functions';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import { MbalTemplate } from "@shared/directives/mbal-template.directive";
import { template } from "lodash";

@Component({
  selector: 'mbal-select',
  templateUrl: './mbal-select.component.html',
  styleUrls: ['./mbal-select.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MBALSelectComponent),
      multi: true,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MBALSelectComponent),
      multi: true,
    },
  ],
})
export class MBALSelectComponent implements Validator, ControlValueAccessor, OnChanges, AfterContentInit {
  @Input() label: string = 'EMPTY';
  @Input() placeholder: string = '';
  @Input() showLabel: boolean = true;
  @Input() options: any = [];
  @Input() selectMode: 'single' | 'multiple' = 'single';
  @Input() display = 'comma';
  @Input() filter: boolean = false;
  @Input() optionValue: string = 'code';
  @Input() optionLabel: string = 'name';
  @Input() optionGroupLabel: string = 'label';
  @Input() optionGroupChildren: string = 'items';
  @Input() optionDisabled: string = 'disabled';
  @Input() scrollHeight: string = '200px';
  @Input() showClear: boolean = true;
  @Input() readonly: boolean = false;
  @Input() tooltip?: any;
  @Input() border: boolean = false;
  @Input() virtualScroll: boolean = false;
  @Input() required?: boolean | string;
  @Input() group: boolean = false;
  @Input() disabled: boolean = false;
  @Input() fieldForm: boolean = true;
  @Input() editable: boolean = false;
  @Input() showToggleAll: boolean = true;
  @Input() maxSelectedLabels: number = 3;
  @Input() selectedItemLabel: string = 'ellipsis';

  @Output() onChangeValue = new EventEmitter();
  @Output() onFilter = new EventEmitter();
  @Output() onClear = new EventEmitter();
  @Output() onPanelHide = new EventEmitter();

  @ContentChildren(MbalTemplate) templates?: QueryList<MbalTemplate>

  selectedItemsTemplate?: TemplateRef<any>
  itemTemplate?: TemplateRef<any>

  absControl!: AbstractControl;
  control = new FormControl(null);

  constructor(
    private el: ElementRef<HTMLElement>,
    private translate: TranslateService,
    private cb: ChangeDetectorRef
  ) {
    this.translate.onLangChange.subscribe(() => {
      this.options = _.cloneDeep(this.options);
      this.mapOptions();
    });
  }

  get errors() {
    return (
      (this.el.nativeElement.closest('.ng-submitted') || this.absControl?.touched || this.absControl?.dirty) &&
      this.absControl?.errors &&
      !this.readonly
    );
  }

  //Dùng để check trường hiện tại có phải required hay không.
  get checkRequire() {
    return (
      this.absControl?.hasValidator(Validators.required) ||
      _.isString(this.required) ||
      (_.isBoolean(this.required) && this.required)
    );
  }

  ngAfterContentInit() {
    this.initTemplate()
  }

  initTemplate() {
    this.templates?.forEach(
      (template) => {
        switch ( template.getName() ) {
          case "selectedItems":
            this.selectedItemsTemplate = template.getTemplate()
            break
          case "items":
            this.itemTemplate = template.getTemplate()
            break
        }
      }
    )
    this.cb.detectChanges()
  }

  mapOptions() {
    // this.listOptions = _.cloneDeep(this.options);
    this.options?.forEach((item: any) => {
      item[this.optionLabel] = !_.isEmpty(item.multiLanguage)
        ? item.multiLanguage[this.translate.currentLang]
        : item[this.optionLabel];
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.mapOptions();
    if (changes['disabled']) {
      this.setDisabledState(this.disabled);
    }
  }

  onChange = (_value: any) => {
    // This is intentional
  };

  onTouched = () => {
    // This is intentional
  };

  //Lấy ra message lỗi validate để hiển thị, nếu có nhiều lỗi -> hiển thị lỗi đầu tiên.
  getError() {
    const errorKey = Object.keys(this.absControl.errors as object)[0];
    const errorValue: any = this.absControl.errors![errorKey];
    return getErrorValidate(this.label, errorKey, errorValue);
  }

  writeValue(value: any): void {
    this.control.setValue(value !== null && value !== undefined ? value : null);
    this.mapOptions();
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  validate(control: AbstractControl): ValidationErrors | null {
    this.absControl = control;
    return null;
  }

  onChangeSelect(event: any) {
    this.onChange(event.value);
    this.onChangeValue.emit(event);
  }

  onFilterSelect(event: any, el: any) {
    el.filterValue = el.filterValue?.trim();
    el.activateFilter();
  }

  onClearInput(event: any) {
    this.onChange(null);
    this.onClear.emit(event);
  }

  onPanelHideEvent($event: Event) {
    this.onPanelHide.emit($event);
  }
}
