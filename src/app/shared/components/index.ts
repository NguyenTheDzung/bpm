import { BaseComponent } from './base.component';
import { BaseActionComponent } from './base-action.component';
import { BaseTableComponent } from './base-table.component';
import { LoadingComponent } from './loading/loading.component';
import { ConfirmDialogComponent } from './confirm/confirm.component';
import { ErrorComponent } from './f-errors/f-errors.component';
import { NotFoundComponent } from '@shared/components/not-found/not-found.component';
import { AccessDeniedComponent } from '@shared/components/access-denied/access-denied.component';
import { MBALInputTextComponent } from '@shared/components/mbal-input-text/mbal-input-text.component';
import { MBALInputTextAreaComponent } from '@shared/components/mbal-input-textarea/mbal-input-textarea.component';
import { MBALInputNumberComponent } from '@shared/components/mbal-input-number/mbal-input-number.component';
import { MBALSelectComponent } from '@shared/components/mbal-select/mbal-select.component';
import { MBALTreeSelectComponent } from '@shared/components/mbal-treeselect/mbal-treeselect.component';
import { MBALCalendarComponent } from '@shared/components/mbal-calendar/mbal-calendar.component';
import { MBALEditorComponent } from '@shared/components/mbal-editor/mbal-editor.component';
import { MaintenanceComponent } from '@shared/components/maintennace/maintenance.component';
import { MbalInputCheckboxComponent } from '@shared/components/mbal-input-checkbox/mbal-input-checkbox.component';
import { DiagramBpmnComponent } from '@shared/components/diagram-bpmn/diagram-bpmn.component';
import { ChangePasswordComponent } from '@shared/components/change-password/change-password.component';

export const components = [
  BaseComponent,
  LoadingComponent,
  ConfirmDialogComponent,
  BaseTableComponent,
  BaseActionComponent,
  ErrorComponent,
  NotFoundComponent,
  AccessDeniedComponent,
  MBALInputTextComponent,
  MBALInputTextAreaComponent,
  MBALInputNumberComponent,
  MBALSelectComponent,
  MBALTreeSelectComponent,
  MBALCalendarComponent,
  MBALEditorComponent,
  MaintenanceComponent,
  MbalInputCheckboxComponent,
  DiagramBpmnComponent,
  ChangePasswordComponent,
];

export * from './base.component';
export * from './loading/loading.component';
export * from './confirm/confirm.component';
export * from './base-table.component';
export * from './base-action.component';
export * from './f-errors/f-errors.component';
export * from './not-found/not-found.component';
export * from './access-denied/access-denied.component';
export * from './mbal-input-text/mbal-input-text.component';
export * from './mbal-input-textarea/mbal-input-textarea.component';
export * from './mbal-input-number/mbal-input-number.component';
export * from './mbal-select/mbal-select.component';
export * from './mbal-treeselect/mbal-treeselect.component';
export * from './mbal-calendar/mbal-calendar.component';
export * from './mbal-editor/mbal-editor.component';
export * from './maintennace/maintenance.component';
export * from './mbal-input-checkbox/mbal-input-checkbox.component';
export * from './diagram-bpmn/diagram-bpmn.component';
export * from './change-password/change-password.component';
