import { Component, ElementRef, forwardRef, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import { getErrorValidate } from '@cores/utils/functions';
import * as _ from 'lodash';

@Component({
  selector: 'mbal-editor',
  templateUrl: './mbal-editor.component.html',
  styleUrls: ['./mbal-editor.component.scss'],
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MBALEditorComponent),
      multi: true,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MBALEditorComponent),
      multi: true,
    },
  ],
})
export class MBALEditorComponent implements Validator, ControlValueAccessor {
  @Input() placeholder: string = 'EMPTY';
  @Input() required?: boolean | string;
  @Input() readonly: boolean = false;
  @Input() disabled: boolean = false;
  @Input() border: boolean = true;
  @Input() height: string = '250px';
  @Input() fieldForm: boolean = true;

  value: any;
  absControl!: AbstractControl;

  constructor(private el: ElementRef<HTMLElement>) {}

  get errors() {
    return (
      (this.el.nativeElement.closest('.ng-submitted') || this.absControl?.touched || this.absControl?.dirty) &&
      this.absControl?.errors &&
      !this.readonly
    );
  }

  onModelChange() {
    this.onChange(this.value);
  }

  onChange = (_value: string) => {
    // This is intentional
  };

  onTouched = () => {
    // This is intentional
  };

  //Lấy ra message lỗi validate để hiển thị, nếu có nhiều lỗi -> hiển thị lỗi đầu tiên.
  getError() {
    const errorKey = Object.keys(this.absControl.errors as object)[0];
    const errorValue: any = this.absControl.errors![errorKey];
    return getErrorValidate('EMPTY', errorKey, errorValue);
  }

  //Dùng để check trường hiện tại có phải required hay không.
  checkRequire() {
    return (
      this.absControl?.hasValidator(Validators.required) ||
      _.isString(this.required) ||
      (_.isBoolean(this.required) && this.required)
    );
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  validate(control: AbstractControl): ValidationErrors | null {
    this.absControl = control;
    return null;
  }

  onTextChange(event: any) {
    this.onChange(event.htmlValue);
  }
}
