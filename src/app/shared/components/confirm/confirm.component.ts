import { Component, OnDestroy } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import * as _ from 'lodash';

@Component({
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmDialogComponent implements OnDestroy {
  constructor(public ref: DynamicDialogRef, private configDialog: DynamicDialogConfig) {
    this.isNote = configDialog.data;
  }

  note: string = '';
  data = {
    confirm: false,
    note: '',
  };

  ngOnDestroy() {
    this.isNote = false;
  }

  isNote: boolean = false;
  isLength: boolean = false;

  confirm(isConfirm: boolean) {
    this.data.note = this.note;
    this.data.confirm = isConfirm;
    if ((this.note.length > 256 && isConfirm && this.isNote) || (isConfirm && _.isEmpty(this.note) && this.isNote)) {
      return;
    }
    this.ref.close(this.data);
  }

  // onResizeLength(event: any) {
  //   if (event.length > 256) {
  //     this.isLength = true;
  //   } else this.isLength = false;
  // }
}
