import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@cores/services/auth.service';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.scss'],
})
export class AccessDeniedComponent {
  constructor(private authService: AuthService, private router: Router) {}

  logout() {
    this.router.navigateByUrl('/').then(() => {
      return this.authService.logout().subscribe();
    });
  }
}
