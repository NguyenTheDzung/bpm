import {AppTopBarComponent} from './topbar/topbar.component';
import {AppMenuComponent} from './menu/menu.component';
import {AppBreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {LoginComponent} from "@shared/layouts/login/login.component";

export const layouts = [AppTopBarComponent, AppMenuComponent, AppBreadcrumbComponent, LoginComponent];

