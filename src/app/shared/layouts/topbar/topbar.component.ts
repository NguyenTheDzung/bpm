import { BaseComponent, ChangePasswordComponent } from '@shared/components';
import { Component, ElementRef, Injector, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { StreamDataService } from '@cores/services/stream-data.service';
import { NOTIFICATION, NotificationStatus, NotifyType, REFRESH_BREACRUMB, REQUEST_TYPE } from '@cores/utils/constants';
import { ScreenType, SessionKey } from '@cores/utils/enums';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { NotificationsService } from '@cores/services/notifications.service';
import { Notification } from '@cores/models/notification.model';
import { forkJoin } from 'rxjs';
import { CommonModel } from '@common-category/models/common-category.model';
import { OverlayPanel } from 'primeng/overlaypanel';
import * as moment from 'moment';
import { unitOfTime } from 'moment';
import { convertStringToJson, createErrorMessage, exportFile } from '@cores/utils/functions';
import { NotificationPostData } from '@notification-system/model';
import { DomSanitizer } from '@angular/platform-browser';
import { NotificationSystemService } from '@notification-system/services/notification-system.service';
import { getUrlDetailRequest } from '@detail-request/shared/utils/functions';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class AppTopBarComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChildren('itemNotifySystem') notifySystems?: QueryList<any>;
  length: number = 0;
  size: number = Number.MAX_SAFE_INTEGER;
  notificationContent: NotificationPostData[] = [];
  virtualNotifications: Notification[] = [];
  countNotifications: number = 0;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  configAction = {
    component: ChangePasswordComponent,
    title: 'title',
    dialog: {
      width: '30%',
    },
  };
  fullName?: string;
  userName?: string;
  interval: any;
  paramNotify = {
    page: 0,
    size: 10,
  };
  requestTypeDropList: CommonModel[] = [];
  notifyType: CommonModel[] = [];
  loadNotify = false;

  constructor(
    public translate: TranslateService,
    private streamData: StreamDataService,
    injector: Injector,
    private notifyService: NotificationsService,
    private notifySystemService: NotificationSystemService,
    private domSanitizer: DomSanitizer
  ) {
    super(injector);
    translate.use('vi');
  }

  ngOnInit() {
    this.fillInfoUser();
    setInterval(() => {
      this.fillInfoUser();
    }, 3000);
    this.getDataNotification();
    forkJoin([
      this.notifyService.countNotifications(),
      this.commonService.getCommonByCodes([REQUEST_TYPE, NOTIFICATION]),
    ]).subscribe({
      next: ([count, commons]) => {
        this.notifyType = commons
          .filter((item) => item.commonCategoryCode === NOTIFICATION)
          ?.map((item) => {
            return { ...item, multiLanguage: convertStringToJson(item.description) };
          });
        this.requestTypeDropList = commons
          .filter((item) => item.commonCategoryCode === REQUEST_TYPE)
          ?.map((item) => {
            return { ...item, multiLanguage: convertStringToJson(item.description) };
          });
        this.countNotifications = count || 0;
        _.delay(() => {
          this.interval = setInterval(() => {
            this.notifyService.countNotifications().subscribe({
              next: (countNotify) => {
                this.countNotifications = countNotify || 0;
              },
            });
          }, 120000);
        }, 12000);
      },
    });
  }

  filter(item: NotificationPostData) {
    return (
      item.notification.status &&
      item.notification.type === '1' &&
      moment(item.notification.dateStart).isBefore(moment()) &&
      moment(item.notification.dateEnd).isAfter(moment()) &&
      this.checkPermission(item.notification.departments)
    );
  }

  getDataNotification() {
    this.notifySystemService.getData(0, this.size).subscribe({
      next: (res) => {
        this.notificationContent = res.data?.notificationDetailDTOS?.filter((item: NotificationPostData) =>
          this.filter(item)
        );
        setTimeout(() => {
          this.getDataNotification();
        }, 120000);
      },
    });
  }

  // check điều kiện animation chạy chữ

  checkWidthContent(el: HTMLElement) {
    this.length = _.size(this.notificationContent);
    return (
      _.size(this.notificationContent) > 1 ||
      el.offsetWidth < this.getTextWidth((this.notifySystems?.get(0) as ElementRef)?.nativeElement.innerText)
    );
  }

  checkPermission(departments: string[]) {
    for (let role of this.authService.getUserRoles()) {
      for (let department of departments) {
        if (role == department) {
          return true;
        }
      }
    }
    return false;
  }

  innerCheck(content: string) {
    if (!content) return '';
    return this.domSanitizer.bypassSecurityTrustHtml(content);
  }

  switchLanguage() {
    this.translate.use(this.translate.currentLang === 'vi' ? 'en' : 'vi').subscribe({
      next: () => {
        localStorage.setItem(this.userName!, this.translate.currentLang);
        this.streamData.passData(REFRESH_BREACRUMB, true);
      },
    });
  }

  getTextWidth(str: string) {
    const text = document.createElement('span');
    document.body.appendChild(text);

    text.style.font = 'times new roman';
    text.style.fontSize = document.body.style.fontSize + 'px';
    text.style.height = 'auto';
    text.style.width = 'auto';
    text.style.position = 'absolute';
    text.style.whiteSpace = 'no-wrap';
    text.innerHTML = str;

    const width = Math.ceil(text.clientWidth);
    document.body.removeChild(text);
    return width;
  }

  fillInfoUser() {
    const user = this.sessionService.getSessionData(SessionKey.UserProfile);
    if (user) {
      this.fullName = user.username?.toUpperCase();
      this.userName = user.username?.toUpperCase();
    }
  }

  logout() {
    this.authService.logout().subscribe({
      next: () => {
        this.router.navigateByUrl('/login').then();
      },
    });
  }

  onScrollDown(_event: any) {
    this.paramNotify.page += 1;
    this.notifyService.fetchNotifications(this.paramNotify).subscribe({
      next: (notifications) => {
        if (notifications?.length > 0) {
          this.virtualNotifications = [...this.virtualNotifications, ...notifications];
        }
      },
    });
  }

  getTitleNotification(item: Notification) {
    const type = this.requestTypeDropList?.find((common) => common.value === item.requestType)?.multiLanguage[
      this.translate.currentLang
    ];
    return this.notifyType
      .find((common) => item.notificationCode === common.code)
      ?.multiLanguage[this.translate.currentLang]?.replace('{{ requestType }}', type)
      ?.replace('{{ requestCode }}', item.requestCode)
      ?.replace('{{ owner }}', item.owner);
  }

  override ngOnDestroy() {
    clearInterval(this.interval);
  }

  onHideNotify() {
    this.paramNotify.page = 0;
    this.loadNotify = false;
  }

  onShowNotify() {
    this.notifyService.fetchNotifications(this.paramNotify).subscribe({
      next: (notifications) => {
        this.loadNotify = true;
        this.virtualNotifications = notifications || [];
      },
    });
    const timer = setTimeout(() => {
      this.countNotifications = 0;
      const el = document.getElementsByClassName('op-notify').item(0) as HTMLElement;
      el!.style.top = '50px';
      el!.style.position = 'fixed';
      clearTimeout(timer);
    }, 10);
  }

  getIconNotify(item: Notification) {
    switch (item.status) {
      case NotificationStatus.Action:
        return 'icon-notify-checklist';
      case NotificationStatus.Approve:
        return 'icon-notify-approved';
      case NotificationStatus.Reject:
        return 'icon-notify-rejected';
      case NotificationStatus.Pending:
        return 'icon-notify-pending';
      case NotificationStatus.Forward:
        return 'icon-notify-forward';
      default:
        return '';
    }
  }

  getTime(item: Notification) {
    const moments = ['years', 'months', 'weeks', 'days', 'hours', 'minutes'];
    for (const time of moments) {
      const number = moment().diff(moment(item.createdDate), <unitOfTime.Diff>time);
      if (number > 0) {
        return this.translate.instant(`notification.${time}Ago`, { number });
      }
    }
    return '';
  }

  doDetailRequest(item: Notification, opNotify: OverlayPanel) {
    let path = '/bpm';
    // admin forward yêu cầu sang cho user B, nên user A k vào xem đc bản ghi này nữa
    if (item.notificationCode === NotifyType.case8_1) {
      this.router.navigateByUrl('/bpm/requests').then();
      return;
    }
    if (
      [this.requestType.TYPE10, this.requestType.TYPE07, this.requestType.TYPE09].includes(item.requestType!) &&
      item.notificationCode === NotifyType.case9
    ) {
      path += '/approves/non-p2p';
    } else {
      path += '/requests';
    }
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router
        .navigate([getUrlDetailRequest(path, item.requestType!, item.requestCode!)], {
          state: item,
        })
        .then(() => {
          this.notifyService.readNotification(item.id).subscribe();
        });
    });
    opNotify.hide();
  }

  exportFile(item: any) {
    this.loadingService.start();
    this.notifySystemService.exportFile(item.id).subscribe({
      next: (res: any) => {
        exportFile(res?.data?.attachmentName, res?.data?.mainDocument, res?.data?.fileType);
        this.loadingService.complete();
      },
      error: (err) => {
        this.messageService.error(createErrorMessage(err));
        this.loadingService.complete();
      },
    });
  }

  openChangePassPopup() {
    if (!this.configAction?.component) {
      return;
    }
    const dialog = this.dialogService?.open(this.configAction.component, {
      header: `${this.configAction.title.toUpperCase()}`,
      showHeader: false,
      width: this.configAction.dialog?.width || '85%',
      data: {
        screenType: ScreenType.Create,
      },
    });
    dialog?.onClose.subscribe({
      next: (isSuccess) => {
        if (isSuccess) {
          // this.search();
        }
      },
    });
  }
}
