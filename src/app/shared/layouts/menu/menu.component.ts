import { animate, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { getNodeMenuByUrl, getUrlPathName } from '@cores/utils/functions';
import { MenuModel } from '@cores/models/menu.model';
import { HttpCancelService } from '@cores/services/http-cancel.service';
import { SessionService } from '@cores/services/session.service';
import { SessionKey } from '@cores/utils/enums';
import { FunctionModel } from '@cores/models/function.model';
import { filter } from 'rxjs';
import { get, orderBy } from 'lodash';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    trigger('toggleMenu', [
      transition(':enter', [style({ visibility: 'visible', height: 0 }), animate(`${150}ms ease-in`)]),
      transition(':leave', [animate(`${150}ms ease-out`, style({ visibility: 'hidden', height: 0 }))]),
    ]),
  ],
})
export class AppMenuComponent implements OnInit {
  @Output() staticMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  classMenu = 'layout-sidebar';
  items: MenuModel[] = [];
  listMenuOfUser: FunctionModel[] = [];
  isLock = false;
  prevUrl = '';

  constructor(
    private router: Router,
    private httpCancelService: HttpCancelService,
    private sessionService: SessionService
  ) {
    this.prevUrl = getUrlPathName(this.router.url);
    this.router.events.pipe(filter((event) => event instanceof ActivationEnd)).subscribe(() => {
      const item = getNodeMenuByUrl({ children: this.items }, getUrlPathName(this.router.url));
      this.activeMenuItem(item);
      const currUrl = getUrlPathName(this.router.url);
      if (this.prevUrl !== '/login' && this.prevUrl !== currUrl) {
        this.httpCancelService.cancelPendingRequests();
      }
      this.prevUrl = currUrl;
    });
  }

  ngOnInit() {
    const dataMenu = this.sessionService.getSessionData(SessionKey.Menu);
    this.listMenuOfUser = dataMenu.menuOfUser;
    dataMenu.menu = dataMenu.menu?.map((itemM: any) => {
      return {
        id: itemM.id,
        label: itemM.code,
        icon: itemM.iconUri,
        order: itemM.order,
        routerLink: itemM.uri,
        active: false,
        parentId: itemM.parent,
        multiLanguage: itemM.multiLanguage,
      };
    });
    dataMenu.menu = orderBy(dataMenu.menu, 'order');
    this.items = dataMenu.menu
      .filter((o: MenuModel) => !o.parentId)
      .map((item: MenuModel, i: number) => {
        return { ...item, path: `[${i}]` };
      });
    this.mapTreeFunction(dataMenu.menu, this.items);

    const item: MenuModel = getNodeMenuByUrl({ children: this.items }, getUrlPathName(this.router.url));
    if (item) {
      const paths = item.path.split('.');
      let path = paths[0];
      for (let i = 0; i < paths.length; i++) {
        get(this.items, path).active = true;
        path = `${path}.${paths[i + 1]}`;
      }
    }
  }

  mapTreeFunction(list: MenuModel[], listParent: MenuModel[], path?: string) {
    listParent?.forEach((item, i) => {
      const listChildren = list?.filter((itemMenu) => itemMenu.parentId === item.id);
      item.path = path ? `${path}.children[${i}]` : `[${i}]`;
      if (listChildren.length > 0) {
        item.children = listChildren;
        item.expanded = false;
        this.mapTreeFunction(list, item.children, item.path);
      }
    });
  }

  mouseEnterMenu() {
    this.classMenu = 'layout-sidebar layout-sidebar-active';
  }

  mouseLeaveMenu() {
    this.classMenu = 'layout-sidebar';
  }

  activeMenuItem(item?: MenuModel, isClickMenu: boolean = false) {
    if (item?.routerLink && isClickMenu) {
      const objMenu = this.sessionService.getSessionData(SessionKey.Menu);
      const functionCode = objMenu?.menu?.find(
        (o: any) => !!o.routerLink && this.router.url.includes(o.routerLink)
      )?.label;
      if (objMenu && functionCode) {
        objMenu.menuOfUser.find((o: FunctionModel) => o.rsname === functionCode).prevParams = null;
        this.sessionService.setSessionData(SessionKey.Menu, objMenu);
      }
    }
    if (item) {
      item.active = !item.active;
      if (item.active) {
        const listSibling = item.path.includes('.')
          ? get(this.items, item.path.substring(0, item.path.lastIndexOf('.')))?.children
          : this.items;
        for (const itemS of listSibling) {
          if (item.id === itemS.id) {
            continue;
          }
          itemS.active = false;
          this.setActiveMenu(itemS.children || [], false);
        }
      } else {
        this.setActiveMenu(item.children || [], false);
      }
    }
  }

  setActiveMenu(data: MenuModel[], active: boolean) {
    for (const item of data) {
      item.active = active;
      if (item.children) {
        this.setActiveMenu(item.children, active);
      }
    }
  }

  onStaticMenu() {
    this.isLock = !this.isLock;
    this.staticMenu.emit(this.isLock);
  }

  checkRole(itemCheck: MenuModel) {
    return this.listMenuOfUser?.findIndex((item) => item.rsid === itemCheck.id) > -1;
  }
}
