import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MenuItem } from 'primeng/api';
import { buildBreadCrumb } from '@cores/utils/functions';
import { Title } from '@angular/platform-browser';
import { last } from 'lodash';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppBreadcrumbComponent implements OnInit {
  constructor(private router: Router, private trans: TranslateService, private title: Title) {
    this.router.events.subscribe((event) => {
      if (event instanceof ActivationEnd) {
        this.items = buildBreadCrumb(this.router.routerState.root, trans);
        this.title.setTitle(last(this.items)?.label ?? 'MBALBPM');
      }
    });
    this.trans.onLangChange.subscribe(() => {
      this.items = buildBreadCrumb(this.router.routerState.root, this.trans);
      this.title.setTitle(last(this.items)?.label ?? 'MBALBPM');
    });
  }

  items: MenuItem[] = [];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/' };

  ngOnInit() {
    const timer = setTimeout(() => {
      this.items = buildBreadCrumb(this.router.routerState.root, this.trans);
      this.title.setTitle(last(this.items)?.label ?? 'MBALBPM');
      clearTimeout(timer);
    }, 200);
  }
}
