import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "@cores/services/auth.service";
import { Router } from "@angular/router";
import { finalize, forkJoin, switchMap, take } from "rxjs";
import { Roles } from "@cores/utils/constants";
import { SessionKey } from "@cores/utils/enums";
import { SessionService } from "@cores/services/session.service";

interface IForm extends FormGroup {
  value: {
    username: string;
    password: string;
  };
  controls: {
    username: AbstractControl;
    password: AbstractControl;
  };
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loading = false;
  loginAccountError = false;
  inactiveUser = false;
  errorServer = false;
  showPass = true;
  form = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  }) as IForm;
  baseRedirectUrl = '/bpm';

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private sessionService: SessionService
  ) {
  }

  ngOnInit(): void {
    if (!this.authService.isAccessTokenExpired()) {
      this.redirectToScreenByRole();
    } else {
      this.sessionService.onStorageChange(SessionKey.UserProfile).subscribe((event) => {
        if (event.newValue && JSON.parse(event.oldValue!)?.id !== JSON.parse(event.newValue!)?.id) {
          this.redirectToScreenByRole();
        }
      });
    }
  }

  showPassword() {
    this.showPass = !this.showPass;
  }

  changeInvalid() {
    this.loginAccountError = false;
    this.inactiveUser = false;
    this.errorServer = false;
  }

  submitLogin() {
    this.loginAccountError = false;
    this.inactiveUser = false;
    this.errorServer = false;
    this.form.markAllAsTouched();
    if (this.form.invalid || this.loading) {
      return;
    }
    this.loading = true;
    const data = this.form.getRawValue();
    data.username = data.username.toLowerCase();
    this.authService
      .login(data)
      .pipe(
        switchMap(() => {
          return this.authService.getUserProfile$().pipe(take(1));
        }),
        finalize(() => this.loading = false)
      )
      .subscribe({
        next: () => {
          // Điều hướng tới bpm sẽ kích hoạt call API current user của Feature module
          // Từ đó sẽ có các roles
          this.redirectToScreenByRole();
        },
        error: (err)  => {
          if (err.error.messageKey === 'UNAUTHORIZED') {
            this.loginAccountError = true;
          } else if (err.error.messageKey === 'INACTIVE_USER') {
            this.inactiveUser = true;
          } else {
            this.errorServer = true;
          }
        }
      });
  }

  redirectToScreenByRole() {
    this.loading = true;
    const closeLoading = () => (this.loading = false);
    forkJoin([
      this.authService.getUserRoles$().pipe(take(1)),
      this.authService.getUserProfile$().pipe(take(1)),
    ]).subscribe({
      next: ([roles]) => {
        for (const role of roles) {
          switch (role) {
            //Role CSSS điều hướng về màn yêu cầu
            case Roles.OP_COL_CSSS: {
              this.router.navigateByUrl(this.baseRedirectUrl + '/requests', {replaceUrl: true}).then(closeLoading);
              return;
            }
            //Roles GUEST điều hướng về màn hình lịch sử
            case Roles.GUEST: {
              this.router.navigateByUrl(this.baseRedirectUrl + '/history', {replaceUrl: true}).then(closeLoading);
              return;
            }
          }
        }
        //Nếu không có các quyền đặc biệt thì chuyển hướng về dashboard
        this.router.navigateByUrl(this.baseRedirectUrl + '/dashboard', {replaceUrl: true}).then(closeLoading);
      },
    });
  }
}
