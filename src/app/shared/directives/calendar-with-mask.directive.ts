import { AfterViewInit, Directive, HostListener, Input } from '@angular/core';
import { Calendar } from 'primeng/calendar';
import { DateFormat } from '@cores/utils/enums';

interface cursorLastPosition {
  selectionStart: number;
  selectionEnd: number;
  code: 'Backspace' | 'Delete';
}

@Directive({
  selector: '[appCalendarWithMask]',
})
export class CalendarWithMaskDirective implements AfterViewInit {
  constructor(private calendar: Calendar) {
  }

  @Input() set format(value: DateFormat) {
    this._dateFormat = value;
  }

  cursorLastPosition: cursorLastPosition | null = null;

  _dateFormat: DateFormat = DateFormat.Date;

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event: KeyboardEvent) {
    this.setSelectionWhenDelete(event);
  }

  @HostListener('keydown.delete', ['$event'])
  keydownDelete(event: KeyboardEvent) {
    this.setSelectionWhenDelete(event);
  }

  ngAfterViewInit(): void {
    this.calendar.onInput.subscribe((event: InputEvent) => {
      const actualValue = this.getValue();
      // if (this.calendar.selectionMode === 'single') {
      //   if (this._dateFormat === DateFormat.Date && event.data === '/' && [3, 6].includes(actualValue.length)) {
      //     return;
      //   }
      //   if (this._dateFormat === DateFormat.Month && event.data === '/' && [3].includes(actualValue.length)) {
      //     return;
      //   }
      // } else {
      //   if (this._dateFormat === DateFormat.Date && event.data === '/' && [3, 6, 16, 19].includes(actualValue.length)) {
      //     return;
      //   }
      //   if (
      //     this._dateFormat === DateFormat.Date &&
      //     ((event.data === ' ' && [11, 13].includes(actualValue.length)) ||
      //       (event.data === '-' && [12].includes(actualValue.length)))
      //   ) {
      //     return;
      //   }
      //   if (this._dateFormat === DateFormat.Month && event.data === '/' && [3, 13].includes(actualValue.length)) {
      //     return;
      //   }
      //   if (
      //     this._dateFormat === DateFormat.Month &&
      //     ((event.data === ' ' && [8, 10].includes(actualValue.length)) ||
      //       (event.data === '-' && [9].includes(actualValue.length)))
      //   ) {
      //     return;
      //   }
      // }
      if (this.cursorLastPosition) {
        this.setValueToCalendar(actualValue);
        return;
      }

      this.formatAndSetValue(actualValue);
    });
  }

  private formatAndSetValue(actualValue: string) {
    // regex to replace everything except numbers, regex trim to 8 characters
    const trimmedValue = actualValue.replace(/[^0-9]/g, '');

    let formattedValue: string;
    if (this.calendar.selectionMode === 'single') {
      if (this._dateFormat === DateFormat.Date) {
        // regex to add points after 2 and 4 characters and trim to 10 characters
        formattedValue = trimmedValue
          .replace(/^(\d{2})(\d)/, '$1/$2')
          .replace(/^(\d{2})\/(\d{2})(\d)/, '$1/$2/$3')
          .substring(0, 10);
        this.setValueToCalendar([2, 5].includes(formattedValue.length) ? formattedValue + '/' : formattedValue);
      } else {
        // regex to add points after 2 characters and trim to 7 characters
        formattedValue = trimmedValue.replace(/^(\d{2})(\d)/, '$1/$2').substring(0, 7);
        this.setValueToCalendar(2 === formattedValue.length ? formattedValue + '/' : formattedValue);
      }
    } else {
      if (this._dateFormat === DateFormat.Date) {
        formattedValue = trimmedValue
          .replace(/^(\d{2})(\d)/, '$1/$2')
          .replace(/^(\d{2})\/(\d{2})(\d)/, '$1/$2/$3')
          .replace(/^(\d{2})\/(\d{2})\/(\d{4})(\d)/, '$1/$2/$3 - $4')
          .replace(/^(\d{2})\/(\d{2})\/(\d{4}) - (\d{2})(\d)/, '$1/$2/$3 - $4/$5')
          .replace(/^(\d{2})\/(\d{2})\/(\d{4}) - (\d{2})\/(\d{2})(\d)/, '$1/$2/$3 - $4/$5/$6')
          .substring(0, 23);
        formattedValue = [2, 5, 15, 18].includes(formattedValue.length) ? formattedValue + '/' : formattedValue;
        formattedValue = [10].includes(formattedValue.length) ? formattedValue + ' - ' : formattedValue;
        this.setValueToCalendar(formattedValue);
      } else {
        formattedValue = trimmedValue
          .replace(/^(\d{2})(\d)/, '$1/$2')
          .replace(/^(\d{2})\/(\d{4})(\d)/, '$1/$2 - $3')
          .replace(/^(\d{2})\/(\d{4}) - (\d{2})(\d)/, '$1/$2 - $3/$4')
          .substring(0, 17);
        formattedValue = [2, 12].includes(formattedValue.length) ? formattedValue + '/' : formattedValue;
        formattedValue = [7].includes(formattedValue.length) ? formattedValue + ' - ' : formattedValue;
        this.setValueToCalendar(formattedValue);
      }
    }
  }

  private getValue(): string {
    return this.calendar.inputfieldViewChild.nativeElement.value;
  }

  /**
   * Set value to the input field
   * @param val
   */
  private setValueToCalendar(val: any) {
    // if (this.calendar.inputfieldViewChild.nativeElement.value !== val) {
    this.calendar.inputfieldViewChild.nativeElement.value = val;
    try {
      let value = this.calendar.parseValueFromString(val);
      // valid date
      if (this.isValidSelection(value)) {
        this.calendar.updateModel(value);
        this.calendar.updateUI();
      } else {
        let value = this.calendar.keepInvalid ? val : null;
        this.calendar.updateModel(value);
      }
    } catch (err) {
      //invalid date
      let value = this.calendar.keepInvalid ? val : null;
      this.calendar.updateModel(value);
    }
    this.calendar.filled = val != null && val.length;
    // }
    if (this.cursorLastPosition && val!.length > this.cursorLastPosition.selectionEnd) {
      this.calendar.inputfieldViewChild.nativeElement.setSelectionRange(
        this.cursorLastPosition.code === 'Backspace'
          ? this.cursorLastPosition.selectionEnd - 1
          : this.cursorLastPosition.selectionEnd,
        this.cursorLastPosition.code === 'Backspace'
          ? this.cursorLastPosition.selectionEnd - 1
          : this.cursorLastPosition.selectionEnd
      );
    }
    this.cursorLastPosition = null;
  }

  private setSelectionWhenDelete(event: KeyboardEvent) {
    if (event?.target) {
      this.cursorLastPosition = {
        selectionStart: (event.target as any).selectionStart,
        selectionEnd: (event.target as any).selectionEnd,
        code: event.code as any,
      };
    }
  }

  private isValidSelection(value: any): boolean {
    let isValid = true;
    if (this.calendar.isSingleSelection()) {
      if (!this.calendar.isSelectable(value.getDate(), value.getMonth(), value.getFullYear(), false)) {
        isValid = false;
      }
    } else if (value[0] && !value[1]) {
      if (!this.calendar.isSelectable(value[0].getDate(), value[0].getMonth(), value[0].getFullYear(), false)) {
        isValid = false;
      }
    } else if (value.every((v: any) => this.calendar.isSelectable(v.getDate(), v.getMonth(), v.getFullYear(), false))) {
      if (this.calendar.isRangeSelection()) {
        isValid = value.length > 1 && value[1] >= value[0];
      }
    } else {
      isValid = false
    }
    return isValid;
  }
}
