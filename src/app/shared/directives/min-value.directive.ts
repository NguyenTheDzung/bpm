import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { isNumber } from 'lodash';

@Directive({
  selector: '[minValue]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinValueDirective, multi: true }],
})
export class MinValueDirective implements Validator {
  @Input() minValue: number | null = null;

  validate(control: AbstractControl): ValidationErrors | null {
    const invalid = isNumber(this.minValue) && isNumber(control.value) && this.minValue > control.value;
    return invalid ? { minValue: { value: control.value, min: this.minValue } } : null;
  }
}
