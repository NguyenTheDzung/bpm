import { Directive, HostBinding, HostListener, Input, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { NgControl } from "@angular/forms";

@Directive({
  selector: '[appTrim]',
})
export class TrimDirective implements OnInit, OnDestroy {
  @Input() TrimOnBlur = false;
  @Input() emitEvent = false

  valueSubscription: Subscription = new Subscription();
  textValue?: string ;

    @HostBinding('attr.maxlength') max: number = 500;

  @HostListener('blur', ['$event'])
  onBlur() {
    if (!this.TrimOnBlur) {
      return;
    }
    this.textValue = this.transform(this.textValue);
    this.ngControl.control?.setValue(this.textValue, { emitEvent: this.emitEvent });
  }

  @HostListener('input', ['$event'])
  onInput() {
    if (this.TrimOnBlur) {
      return;
    }
    this.textValue = this.transform(this.textValue)!;
    this.ngControl.control?.setValue(this.textValue, { emitEvent: this.emitEvent });
  }

  constructor(private ngControl: NgControl) {}

  ngOnInit(): void {
    this.valueSubscription = this.ngControl?.control!.valueChanges.subscribe((value) => {
      this.textValue = value;
    });
    this.textValue = this.ngControl?.control?.value ?? ""
  }

  transform(value?: string) {
    let text = value;
    if (!text) {
      return undefined;
    }
    text = text.replace(/^\s+|\s+$/g, '');
    return text;
  }

  ngOnDestroy(): void {
    this.valueSubscription.unsubscribe();
  }
}
