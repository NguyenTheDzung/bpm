import {Directive, Input, TemplateRef} from "@angular/core";

@Directive({
  selector: '[mbalTempate]'
})
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export class MbalTemplate {
  @Input('mbalTempate') name?: string

  constructor(private template: TemplateRef<any>) {
  }

  getName() {
    return this.name
  }

  getTemplate(){
    return this.template
  }
}
