import { InputNoSpaceValidatorDirective } from './input-nospace.directive';
import { TrimDirective } from '@shared/directives/trim-white-space.directive';
import { CalendarWithMaskDirective } from '@shared/directives/calendar-with-mask.directive';
import { PlusNumberOnlyDirective } from '@shared/directives/plus-number-only.directive';
import { MbalTemplate } from '@shared/directives/mbal-template.directive';
import { MinValueDirective } from '@shared/directives/min-value.directive';
import { MaxValueDirective } from '@shared/directives/max-value.directive';

export const directives = [
  InputNoSpaceValidatorDirective,
  TrimDirective,
  CalendarWithMaskDirective,
  PlusNumberOnlyDirective,
  MbalTemplate,
  MinValueDirective,
  MaxValueDirective,
];

export * from './input-nospace.directive';
export * from './trim-white-space.directive';
export * from './calendar-with-mask.directive';
export * from './plus-number-only.directive';
