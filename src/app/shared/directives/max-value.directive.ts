import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { isNumber } from 'lodash';

@Directive({
  selector: '[maxValue]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MaxValueDirective, multi: true }],
})
export class MaxValueDirective implements Validator {
  @Input() maxValue: number | null = null;

  validate(control: AbstractControl): ValidationErrors | null {
    const invalid = isNumber(this.maxValue) && isNumber(control.value) && this.maxValue < control.value;
    return invalid ? { maxValue: { value: control.value, max: this.maxValue } } : null;
  }
}
