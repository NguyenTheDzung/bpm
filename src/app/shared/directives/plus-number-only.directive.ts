import {AfterViewInit, Directive, ElementRef, HostListener, Input, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {NgControl} from "@angular/forms";
import {DecimalPipe} from "@angular/common";

@Directive({
  selector: '[appPlusNumber]',
  providers: [DecimalPipe]
})
export class PlusNumberOnlyDirective implements OnInit, OnDestroy, AfterViewInit {
  character: any
  @Input() TrimOnBlur = false;
  @Input() emitEvent = false

  valueSubscription: Subscription = new Subscription();
  textValue: any;

  @HostListener('blur', ['$event'])
  onBlur() {
    if (!this.TrimOnBlur) {
      return;
    }
    this.textValue = this.transform(this.textValue);
    this.ngControl.control?.setValue(this.textValue, {emitEvent: this.emitEvent});
    const len = this.character?.length - 4
    this.el?.setSelectionRange(len, len)
  }

  @HostListener('input', ['$event'])
  onInput() {
    if (this.TrimOnBlur) {
      return;
    }
    this.textValue = this.transform(this.textValue)!;
    this.ngControl.control?.setValue(this.textValue, {emitEvent: this.emitEvent});
    const len = this.character?.length - 4
    this.el?.setSelectionRange(len, len)
  }

  el?: HTMLInputElement

  constructor(
    private ngControl: NgControl,
    private decimalPipe: DecimalPipe,
    private element: ElementRef
  ) {
  }

  ngOnInit(): void {
    this.valueSubscription = this.ngControl?.control!.valueChanges.subscribe((value) => {
      this.textValue = this.transform(value);
      this.ngControl.control?.setValue(this.textValue, {emitEvent: false})
    });
  }

  transform(value?: string) {
    let text = value;
    if (!text) {
      return undefined;
    }
    text = text?.replace(/\s+VND/g, '')
    if (text.length > 0 && (text.startsWith("+") || text.startsWith("-"))) {
      text = text.replace(/[^-+\d]/g, '');
      const char = text.slice(0, 1)
      let number: any
      if (text.slice(1).length > 0) {
        let cutting = text.replace(/[^\d]/g, '');
        number = this.decimalPipe.transform(cutting, '1.0', 'en-US')
        this.character = char + number + ' VND'
      } else {
        this.character = char + ' VND'
      }
    } else if (text.length > 0) {
      text = text.replace(/[^\d]+/g, '');
      if (text.length > 0) {
        this.character = this.decimalPipe.transform(text, '1.0', 'en-US') + ' VND'
      }
    } else {
      this.character = ''
    }

    return this.character
  }

  ngOnDestroy(): void {
    this.valueSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    const el = this.element.nativeElement as HTMLElement
    this.el = el as HTMLInputElement
  }
}
