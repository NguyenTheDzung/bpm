import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'apply',
})
export class ApplyPipe implements PipeTransform {
  transform<T, U extends any[]>(fn: (...params: U) => T , ...params: U): T {
    return fn(...params)
  }
}