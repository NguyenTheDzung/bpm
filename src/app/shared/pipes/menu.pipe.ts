import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MenuModel } from '@cores/models/menu.model';
import { isEmpty } from 'lodash';

@Pipe({ name: 'menuTranslate', pure: false })
export class MenuPipe implements PipeTransform {
  result: string = '';

  constructor(private translate: TranslateService) {}

  transform(item: MenuModel): string {
    this.updateValue(item);
    return this.result;
  }

  updateValue(item: MenuModel) {
    if (!item || !this.translate.currentLang || isEmpty(item.multiLanguage)) {
      this.result = this.translate.instant(`LAYOUTS.MENU.${item.label}`);
    } else {
      this.result = item.multiLanguage[this.translate.currentLang];
    }

    return this.result;
  }
}
