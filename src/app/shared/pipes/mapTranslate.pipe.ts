import { Pipe, PipeTransform } from '@angular/core';
import { ListContainValue, MapTranslateConfig } from '@cores/models/map-translate-config.model';
import { TranslateService } from '@ngx-translate/core';
import { DEFAULT_MAP_TRANSLATE_CONFIG } from '@cores/utils/constants';

@Pipe({ name: 'MapTranslate', pure: false })
export class MapTranslatePipe implements PipeTransform {
  result!: any;

  constructor(private translate: TranslateService) {}

  transform(value: any, listContainValue: ListContainValue[], config?: Partial<MapTranslateConfig>): string {
    const configMapped: MapTranslateConfig = {
      ...DEFAULT_MAP_TRANSLATE_CONFIG,
      ...config,
    };
    this.updateValue(value, listContainValue, configMapped);
    return this.result;
  }

  updateValue(value: any, listContainValue: ListContainValue[], config: MapTranslateConfig) {
    this.result = config.emptyTemplate ?? '---';

    if (!value || !config.matchFieldName || !this.translate.currentLang) return this.result;
    this.result =
      listContainValue?.find((item) => value == item[config.matchFieldName])?.[config?.fieldContainTranslate]?.[
        this.translate.currentLang
      ] ?? this.result;

    return this.result;
  }
}
