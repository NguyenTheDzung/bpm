import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'MatchPipe',
})
export class MatchPipe implements PipeTransform {
  transform(value: any, list: any[], matchField: string = "code", displayField: string = "name"): any {
    return list?.find((item) => item[matchField] == value)?.[displayField] ?? '- - -';
  }
}
