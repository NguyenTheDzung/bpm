import { DateCustomPipe } from "@shared/pipes/date.pipe";
import { MatchPipe } from "@shared/pipes/match.pipe";
import { MapTranslatePipe } from "@shared/pipes/mapTranslate.pipe";
import { ApplyPipe } from "@shared/pipes/apply.pipe";
import { MenuPipe } from "@shared/pipes/menu.pipe";

export const pipes = [DateCustomPipe, ApplyPipe, MatchPipe, MapTranslatePipe, MenuPipe];
