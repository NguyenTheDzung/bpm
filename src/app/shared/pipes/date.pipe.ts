import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateCus',
})
export class DateCustomPipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(value: any, arg?: string): any {
    const date = moment(value);
    return value && date.isValid() ? date.format(arg || 'DD/MM/YYYY') : '';
  }
}
