import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../user.service";

@Component({
  selector: 'app-dialog-user',
  templateUrl: './dialog-user.component.html',
  styleUrls: ['./dialog-user.component.scss']
})
export class DialogUserComponent  implements OnInit{
  @ViewChild('errorInput') errorInput: any = ElementRef;
  constructor(private userService: UserService) {
  }
  @Input() disable: boolean = false;
  @Input() idEdit: number = 0;
  @Input() dialog: boolean = false;
  @Input() classData: any [] = [];
  @Input() statusData: any [] = [];
  @Input() jobData: any[] = [];
  @Output() newDialog = new EventEmitter<boolean>();
  @Output() newDisable = new EventEmitter<boolean>();
  insertForm: FormGroup = new FormGroup({
    account:  new FormControl(null, [Validators.required,
    Validators.minLength(6), Validators.maxLength(50),
      Validators.pattern('[a-zA-Z0-9_]*')]),
    employeeName: new FormControl(null, [Validators.required, Validators.maxLength(200),
    Validators.pattern('[a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềếểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý ]*')]),
    jobTitle:new FormControl(null, Validators.required),
    position: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]),
    phoneNumber: new FormControl(null, [Validators.required, Validators.pattern('^(((\\+)84)|0)(3|5|7|8|9)+([0-9]{8})$')]),
    class: new FormControl(null, Validators.required),
    keyEmployee: new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(10),
    Validators.pattern('^(L[P])[a-zA-Z0-9]{8}$')]),
    status: new FormControl(null, Validators.required),
  })
  get account(){
    return this.insertForm.get('account');
  }
  get employeeName(){
    return this.insertForm.get('employeeName');
  }
  get jobTitle(){
    return this.insertForm.get('jobTitle');
  }
  get position(){
    return this.insertForm.get('position');
  }
  get email(){
    return this.insertForm.get('email');
  }
  get phoneNumber(){
    return this.insertForm.get('phoneNumber');
  }
  get class(){
    return this.insertForm.get('class');
  }
  get status(){
    return this.insertForm.get('status');
  }
  get keyEmployee(){
    return this.insertForm.get('keyEmployee');
  }

  ngOnInit() {
    if (this.disable){
      this.insertForm.get('account')?.disable();
      this.insertForm.get('keyEmployee')?.disable();
      this.userService.getDataDetails(this.idEdit).subscribe({
        next: (res) => {
          this.insertForm.setValue({
            account : res.account,
            employeeName : res.employeeName,
            jobTitle : res.jobTitle,
            position : res.position,
            email : res.email,
            phoneNumber : res.phoneNumber,
            class : res.class,
            keyEmployee : res.keyEmployee,
            status : res.status,
          })
        },
        error: (err) => {
          console.log(err);
        }
      })
    }
  }
  onInsert()
  {
    this.insertForm.markAllAsTouched()

  if (this.insertForm.invalid){
    return
  }
  if (this.disable == false){
    this.userService.postData(this.insertForm.value).subscribe({
      next: (response) =>  {
      },
      error: (err) => {
        console.log(err)
      }
    });
  }
  else {
    this.userService.patchData(this.insertForm.value, this.idEdit).subscribe({
      next: (response) =>  {
      },
      error: (err) => {
        console.log(err)
      }
    })
  }



  }
  onCancel(value: boolean, value2: boolean) {
    this.disable = false;
    this.idEdit = 0;
    this.newDialog.emit(value);
    this.newDisable.emit(value2);
  }
}
