import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {ListUserComponent} from "../../pages/list-user/list-user.component";
import {RouterModule, Routes} from "@angular/router";
import {ChipsModule} from "primeng/chips";
import {ReactiveFormsModule} from "@angular/forms";
import {DropdownModule} from "primeng/dropdown";
import {TableModule} from "primeng/table";
import {PaginatorModule} from "primeng/paginator";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {MessagesModule} from "primeng/messages";
import {PipeModule} from "../Pipe/pipe.module";
import {DirectiveModule} from "../Directives/driective.module";
import {DialogModule} from "primeng/dialog";
import {DialogUserComponent} from "../Dialog/dialog-user/dialog-user.component";
import {PageChildComponent} from "./page-child.component";
import {SideBarComponent} from "../side-bar/side-bar.component";
import {HeaderComponent} from "../header/header.component";
import {AccordionModule} from "primeng/accordion";


const routers: Routes = [
  {
    path: '',
    component: ListUserComponent
  }
]

@NgModule({
  imports: [CommonModule,
    RouterModule.forChild(routers),
    ChipsModule,
    ReactiveFormsModule,
    DropdownModule,
    TableModule,
    PaginatorModule,
    ConfirmDialogModule,
    PipeModule,
    DirectiveModule,
    MessagesModule, DialogModule, AccordionModule],
  exports: [RouterModule],
  declarations: [
    DialogUserComponent,
    ListUserComponent,
    PageChildComponent,
    SideBarComponent,
    HeaderComponent

  ]
})

export class ListuserRoutingModule {
}
