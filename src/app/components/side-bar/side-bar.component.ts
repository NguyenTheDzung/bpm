import { Component } from '@angular/core';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent {
  idxActive: number | undefined;
  listCustomer: any[] = [
    {
      id: 1,
      name: 'Danh mục người dùng',
      url: ''
    },
    {
      id: 1,
      name: 'Đăng nhập',
      url: '/login'
    },
    {
      id: 1,
      name: 'Danh mục thao tác',
      url: '/list-control'
    },
    {
      id: 1,
      name: 'Danh mục vai trò',
      url: '/list-role'
    },
    {
      id: 1,
      name: 'Danh mục quyền',
      url: '/list-pow'
    }
  ];

  active(idx: any) {
    this.idxActive = idx;
  }
}
