import {AbstractControl} from '@angular/forms';
import {Observable} from "rxjs";

export function ValidateAccount(control: AbstractControl) {
  if (control.value != "[a-zA-Z0-9_]*") {
    return {invalidUrl: true};
  }
  return null;
}

const ob = new Observable(function (ob) {
  const id = setTimeout(() => {
    ob.next('RxJS');
    ob.complete();
  }, 1000);

  return function () {
    clearTimeout(id);
  }
})

ob.subscribe(console.log)
ob.subscribe({
  next: val => console.log(val),
  error: err => console.log(err),
  complete: () => console.log('complete')
})
