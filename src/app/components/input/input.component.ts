import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
  @Input() label: string = ""
  @Input() text: string = ""

  constructor(private router: Router) {
  }
  back(){
    this.router.navigate(['']);
  }
}
