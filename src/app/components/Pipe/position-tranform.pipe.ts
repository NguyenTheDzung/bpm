import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'positionTranform'
})
export class PositionTranformPipe implements PipeTransform {

  transform(key: any, list: any): any {
    return list.find((value: any) => {
      return key == value?.id
    })?.name
  }
}
