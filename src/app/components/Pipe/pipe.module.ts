import {NgModule} from "@angular/core";
import {PositionTranformPipe} from "./position-tranform.pipe";

@NgModule({
  declarations: [
    PositionTranformPipe,
  ],
  exports: [
    PositionTranformPipe
  ]
})

export class PipeModule{}
