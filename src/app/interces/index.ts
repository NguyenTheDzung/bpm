import {Injectable} from "@angular/core";
import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpStatusCode
} from "@angular/common/http";
import {Router} from "@angular/router";
import {finalize, Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {TokenService} from "../services/token.service"; //handleLocalStorage
import {SpinnerService} from "../utils/spinner/spinner.service";
import {Constant} from "../utils/constant";
import {SessionStorageService} from "../services/session-storage.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  loadingArr = Constant.LOADING_URL //

  constructor(private router: Router,
              private spinnerService: SpinnerService,
              private tokenService: TokenService,
              private sessionStorage: SessionStorageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req  !req.url) return next.handle(req)
    if (!req.url.includes("/login")
      && !req.url.includes("/login/social")
      && !req.url.includes('www.googleapis.com')
      && !req.url.includes('/forgot-password')) {

      const token = this.tokenService._getToken()
      if (token) {
        req = req.clone({
            headers: req.headers.set('authorization', 'Bearer ' + this.tokenService._getToken()?.access_token)
          }
        )
      }

      const admin_token = this.sessionStorage._getData(Constant.SESSION_ADMIN_TOKEN)
      if(admin_token) {
        req = req.clone({
            headers: req.headers.set('authorization', 'Bearer ' + admin_token)
          }
        )
      }
    }
    if(this.findLoadingUrl(req)){
      this.spinnerService.show()
      return next.handle(req).pipe(catchError(x => this.handleAuthError(x)), finalize(() => {this.spinnerService.hide()}))
    }else{
      return next.handle(req).pipe(catchError(x => this.handleAuthError(x)))
    }

  }

  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    this.spinnerService.hide()
    if (err.status === HttpStatusCode.Unauthorized) {
    }
    if (err.status === HttpStatusCode.Forbidden) {
    }
    if (err.status === 500) {
    }
    if (err.status === 0) {
    }
    return throwError(err);
  }

  private findLoadingUrl(req: HttpRequest<any>){
    if( req.method == "POST"  req.method == "DELETE" || req.method == "PUT"){
      return true
    }
    return Boolean(this.loadingArr.find(value =>  req.url.includes(value)))
  }
}

export const AuthInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true,
}
