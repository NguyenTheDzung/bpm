import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppGuardService } from "@cores/services/app-guard.service";
import { AccessDeniedComponent, MaintenanceComponent, NotFoundComponent } from "@shared/components";
import { LoginComponent } from "@shared/layouts/login/login.component";

const routes: Routes = [
  {
    path: 'bpm',
    loadChildren: () => import('./features/features.module').then((m) => m.FeaturesModule),
    canActivate: [AppGuardService],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'notfound',
    component: NotFoundComponent,
  },
  {
    path: 'under-maintenance',
    component: MaintenanceComponent,
  },
  {
    path: 'access-denied',
    component: AccessDeniedComponent,
  },
  { path: '', redirectTo: '/bpm/dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'notfound' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
