import { AbstractControl, FormArray, FormControl, FormGroup } from "@angular/forms";
import { Observable } from "rxjs";

export interface IFormGroupModel<FormGroupValue = any> extends FormGroup {
  value: FormGroupValue;
  valueChanges: Observable<FormGroupValue>;
  controls: ExtractFormControl<FormGroupValue>;
  setValue(value: FormGroupValue, options?: Parameters<FormGroup["setValue"]>[1]): void;
  patchValue(value: Partial<FormGroupValue>, options?: Parameters<FormGroup["patchValue"]>[1]): void;

  reset(value?: Partial<FormGroupValue>, options?: Parameters<FormGroup["reset"]>[1]): void;
}

export interface IFormArray<FormArrayValue = any> extends FormArray {
  value: FormArrayValue[];
  valueChanges: Observable<FormArrayValue[]>;

  at(index: number): IAbstractControl<FormArrayValue>;

  push(control: IAbstractControl<FormArrayValue>, options?: Parameters<FormArray["push"]>[1]): void;
  insert(
    index: number,
    control: IAbstractControl<FormArrayValue>,
    options?: Parameters<FormArray["insert"]>[2]
  ): void;

  setControl(
    index: number,
    control: IAbstractControl<FormArrayValue>,
    options?: Parameters<FormArray["setControl"]>[2]
  ): void;

  setValue(
    value: Flatten<FormArrayValue>[],
    options?: Parameters<FormArray["setValue"]>[1]
  ): void;

  patchValue(
    value: Flatten<FormArrayValue>[],
    options?: Parameters<FormArray["patchValue"]>[1]
  ): void;

  reset(
    value?: FormArrayValue[],
    options?: Parameters<FormArray["reset"]>[1]
  ): void;

  getRawValue(): FormArrayValue[];

  controls: IAbstractControl<FormArrayValue>[];
}

export interface IFormControlModel<FromControlValue = any> extends FormControl {
  value: FromControlValue;
  valueChanges: Observable<FromControlValue>;

  setValue(
    value: FromControlValue,
    options?: Parameters<FormControl["setValue"]>[1]
  ): void;
}

export interface IAbstractControl<T> extends AbstractControl {
  readonly value: T;

  setValue(value: T, options?: Parameters<AbstractControl["setValue"]>[1]): void;

  patchValue(value: T, options?: Parameters<AbstractControl["patchValue"]>[1]): void;

  reset(value?: T, options?: Parameters<AbstractControl["reset"]>[1]): void;
}

export type ExtractFormControl<T> = {
  [K in keyof T]: Flatten<T[K]> extends string | number | boolean | null
    ? IAbstractControl<T[K] | null>
    : IFormGroupModel<T[K]>;
};

type Flatten<T> = T extends any[] ? T[number] : T;
