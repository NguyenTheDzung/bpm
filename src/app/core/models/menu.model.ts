import { MenuItem } from 'primeng/api';

export interface MenuModel extends MenuItem {
  path: string;
  active: boolean;
  children?: MenuModel[];
  data?: any;
  parentId?: string;
  multiLanguage: any;
}
