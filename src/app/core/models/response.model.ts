import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';

export interface IResponseModel<DataModel = any> {
  timestamp: string;
  path: string;
  code: number;
  message: string;
  status: number;
  data: DataModel;
}

export interface IPageableResponseModel<ContentType = any[]> {
  content: ContentType[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: IPageableInSide;
  size: number;
  sort: TPageableSort;
  total: number;
  totalElements: number;
  totalPages: number;
}

export type TPageableSort = {
  order: string[];
  empty: boolean;
  unsorted: boolean;
  sorted: boolean;
};

export type TPageableSearchParams<ParamsType = any> = ParamsType & { page: number; size: number };

export interface IPageableInSide {
  page: number;
  size: number;
  sort: {
    orders: string[];
    empty: boolean;
    unsorted: boolean;
    sorted: boolean;
  };
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  unpaged: boolean;
}

export interface IErrorResponse extends HttpErrorResponse {
  error: ISimpleResponseModel;
}

export interface ISimpleResponseModel {
  code: string;
  messageKey?: string;
  message?: string;
  status: HttpStatusCode;
}
