export interface CommonClaimCategory {
  name: string;
  code: string;
  id: number;
}

export type CommonClaimServiceOpts = {
  timeSpan: number,
  params: {[p:string]: any}
}
