export interface FunctionModel {
  id?: string;
  rsid?: string;
  rsname?: string;
  code?: string;
  name?: string;
  scopes?: string[];
  data?: any;
  parent?: string;
  search?: string;
  prevParams?: any;
}
