export type MapTranslateConfig  = {
  matchFieldName: string,
  fieldContainTranslate: string,
  emptyTemplate?: string
}

export type ListContainValue = { [p: string]: { en: string; vi: string } | any };
