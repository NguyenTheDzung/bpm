export interface Notification {
  id: number;
  notificationCode: string;
  isRead: boolean;
  assignee: string;
  resolution: string;
  requestType: string;
  requestCode: string;
  createdDate: string;
  status: string;
  owner?: string;
}
