export interface ResponsiveLogin<T> {
  timestamp: string;
  path: string;
  code: number;
  message: string;
  status: number;
  data: T;
}

export interface AuthModel {
  accessToken: string;
  expiresIn: number;
  refreshExpiresIn: number;
  refreshToken: string;
  scope: string;
}

export interface RequestBody {
  username: string;
  password: string;
}

export interface Profile {
  permissionResponseDTOS: any[];
  userDTO: {
    email: string;
    emailVerified: boolean;
    firstName: string;
    id: string;
    lastName: string;
    roles: string[];
    username: string;
  };
}
