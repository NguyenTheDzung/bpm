export interface DataTable<ContentType = any> {
  totalPages: number;
  totalElements: number;
  currentPage: number;
  size: number;
  first: number;
  numberOfElements: number;
  content: ContentType[];
}
