export interface UserProfileModel {
  id: string;
  code: string;
  username: string;
  fullName: string;
  age: number;
  gender: string;
  phone: number;
  name: string;
  roles: string[];

  [key: string]: any;
}
