export interface ChartConfig {
  /**
   * Danh sách series hiển trên biểu đồ
   */
  series: SeriesOption[];
  /**
   * Tên biểu đồ
   */
  title: string;
}

export interface SeriesOption {
  /*
   * Tên series để hiển thị tooltip hoặc chú thích
   * */
  name: string;
  /**
   * List data hiển thị trên biểu đồ
   */
  data: ItemData[];
  /**
   * Kiểu biểu đồ
   */
  type: 'bar' | 'line' | 'pie';
  /* xếp chồng giá trị có cùng stack */
  stack?: string;
  /**
   * config màu cho series
   */
  color?: string;
  /**
   * Cấu hình hiển thị giá trị của từng item, dùng cho chart Bar, Line
   * @show: on/off giá trị
   * @position: vị trí hiển thị
   */
  label?: {
    show: boolean;
    position: 'top' | 'left' | 'right' | 'bottom';
  };
}

export interface ItemData {
  // Tên item thường dùng để hiển thị tooltip của chart Pie
  name?: string;
  // giá trị item hiển thị
  value: string | number | Date;
  // config màu cho giá trị item (dùng cho chart bar, char pie)
  color?: string;
}
