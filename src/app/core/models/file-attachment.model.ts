export interface FileAttachment {
  name: string;
  type: string;
  content: string;
  extension: string;
  mimeType: string;
  size: number;
  lastModified: number;
}
