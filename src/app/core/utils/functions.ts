import { AbstractControl, FormArray, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import * as moment from 'moment';
import { MenuItem } from 'primeng/api';
import { DataTable } from '../models/data-table.model';
import { INTERNAL_ERROR_MESSAGE, VALIDATORS_KEY } from '@cores/utils/constants';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonModel } from '@common-category/models/common-category.model';
import { IErrorResponse, IPageableResponseModel, TPageableSearchParams } from '@cores/models/response.model';
import { FileAttachment } from '@cores/models/file-attachment.model';

export function HttpLoaderFactoryTranslate(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

export function removeParamSearch(params: any) {
  const newParams: any = {};
  _.forEach(params, (value, key) => {
    if (_.isNumber(value) || (!_.isNull(value) && !_.isUndefined(value) && !_.isEmpty(value)) || _.isDate(value)) {
      newParams[key] = value;
    }
  });
  return newParams;
}

export function cleanDataForm(formGroup: FormGroup) {
  const form = formGroup;
  Object.keys(form.controls).forEach((field) => {
    const control = form.get(field);
    if (control instanceof FormControl && typeof control.value === 'string') {
      control.setValue(control?.value?.trim(), { emitEvent: false });
    } else if (control instanceof FormGroup) {
      cleanDataForm(control);
    } else if (control instanceof FormArray) {
      for (const item of control.controls) {
        if (item instanceof FormControl) {
          item.setValue(item?.value?.trim(), { emitEvent: false });
        } else if (item instanceof FormGroup) {
          cleanDataForm(item);
        }
      }
    }
  });
  return form.getRawValue();
}

export function validateAllFormFields(formGroup?: FormGroup) {
  if (!formGroup) {
    return;
  }
  Object.keys(formGroup.controls).forEach((field) => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
      control.markAsDirty({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      validateAllFormFields(control);
    } else if (control instanceof FormArray) {
      for (const form of control.controls) {
        validateAllFormFields(form as FormGroup);
      }
    }
  });
}

export function mapDataTable(data: any, params: any): DataTable {
  return {
    content: data?.data?.data?.content || [],
    currentPage: params?.page || 0,
    size: params?.size || 10,
    totalElements: data?.data?.data?.totalElements || 0,
    totalPages: data?.data?.data?.totalPages || 0,
    numberOfElements: data?.data?.data?.numberOfElements || 0,
    first: (params?.size || 0) * (params?.page || 0),
  };
}

/**
 * sử dụng để map data từ response pagable -> dữ liệu DataTable dùng cho các table
 * cải thiện typing
 * @param data dữ liệu pageable được api trả ra
 * @param params dữ liệu request params
 */
export function mapDataTableWithType<DataType = any[], ParamType = any>(
  data: IPageableResponseModel<DataType>,
  params: TPageableSearchParams<ParamType>
): DataTable<DataType> {
  return {
    content: data.content,
    currentPage: params.page,
    size: params.size,
    totalElements: data.totalElements,
    totalPages: data.totalPages,
    numberOfElements: data.numberOfElements,
    first: params.size * params.page,
  };
}

//map data employee
export function mapDataTableEmployee(data: any, params: any): DataTable {
  return {
    content: data?.data?.content || [],
    currentPage: params?.page || 0,
    size: params?.size || 10,
    totalElements: data?.data?.totalElements || 0,
    totalPages: data?.data?.totalPages || 0,
    numberOfElements: data?.data?.numberOfElements || 0,
    first: (params?.size || 0) * (params?.page || 0),
  };
}

export function getNodeMenuByUrl(tree: any, url: string): any {
  let result = null;
  if (url === tree.routerLink) {
    return tree;
  } else if (tree.children) {
    for (const item of tree.children) {
      result = getNodeMenuByUrl(item, url);
      if (result) {
        break;
      }
    }
  }
  return result;
}

export function getUrlPathName(url: string) {
  return url.substring(0, url.indexOf('?') > -1 ? url.indexOf('?') : url.length);
}

export function getUrlByFunctionCode(code: string, stateMenu: any) {
  return stateMenu.menu?.find((item: any) => item.label === code)?.routerLink;
}

export function buildBreadCrumb(
  route: ActivatedRoute,
  translate: TranslateService,
  url: string = '',
  breadcrumbs: MenuItem[] = []
): MenuItem[] {
  const label = _.get(route, 'routeConfig.data.breadcrumb', '');
  const path = _.get(route, 'routeConfig.path', '');
  const hasUrl = _.get(route, 'routeConfig.data.disabledUrl');

  const nextUrl = path ? `${url}/${path}` : url;

  if (!_.isEmpty(label)) {
    const breadcrumb: MenuItem = {
      label: translate.instant(label),
      routerLink: !hasUrl ? nextUrl : undefined,
    };
    breadcrumbs.push(breadcrumb);
  }

  if (route.firstChild) {
    return buildBreadCrumb(route.firstChild, translate, nextUrl, breadcrumbs);
  }

  return breadcrumbs;
}

// clear data
export function cleanData(data: any) {
  Object.keys(data).forEach((key) => {
    if (_.isString(data[key])) {
      data[key] = _.trim(data[key]);
    } else if (
      _.isNull(data[key]) ||
      _.isUndefined(data[key]) ||
      ((_.isArray(data[key]) || _.isObject(data[key])) && _.isEmpty(data[key]))
    ) {
      delete data[key];
    } else if (_.isArray(data[key])) {
      const array = data[key];
      for (let index = 0; index < array?.length; index++) {
        if (_.isString(array[index])) {
          array[index] = _.trim(array[index]);
        } else if (_.isObject(array[index])) {
          cleanData(array[index]);
        }
      }
    } else if (_.isObject(data[key]) && !(data[key] instanceof File)) {
      cleanData(data[key]);
    }
  });
  return data;
}

export function convertStringToJson(value?: string) {
  try {
    return JSON.parse(value!);
  } catch (e) {
    return {};
  }
}

// Convert from Tree Data to Flat Data
export function flattenTreeData(data?: any[]): any[] {
  return _.reduce(
    data,
    (result: any[], current) => {
      return [...result, current, ...flattenTreeData(current.children)];
    },
    []
  );
}

export function dataURItoBlob(dataURI: any, type?: string) {
  const byteString = window.atob(dataURI);
  const arrayBuffer = new ArrayBuffer(byteString.length);
  const int8Array = new Uint8Array(arrayBuffer);
  for (let i = 0; i < byteString.length; i++) {
    int8Array[i] = byteString.charCodeAt(i);
  }
  return new Blob([int8Array], { type: type || 'application/octet-stream' });
}

//format về kiểu string datetime local
export function getValueDateTimeLocal(value: any, format?: string): string | null {
  if (_.isDate(value) || moment(value, format ?? moment.HTML5_FMT.DATETIME_LOCAL).isValid()) {
    return moment(value).format(format ?? moment.HTML5_FMT.DATETIME_LOCAL);
  }
  return null;
}

export function updateValidity(control: AbstractControl | null, validators: ValidatorFn | ValidatorFn[] | null) {
  const value = control?.value;
  control?.setValue(undefined, { emitEvent: false });
  control?.setValidators(validators);
  if (!validators) {
    control?.setErrors(null);
  }
  control?.updateValueAndValidity();
  control?.setValue(value, { emitEvent: false });
}

export function getErrorValidate(label: string, errorKey: string, errorValue: any) {
  switch (errorKey) {
    case 'file':
      return {
        key: VALIDATORS_KEY[Object.keys(errorValue as object)[0]],
        required: errorValue,
      };
    case 'required':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'required' : 'requiredNoLabel'],
        required: {
          label: label,
        },
      };
    case 'pattern':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'pattern' : 'patternNoLabel'],
        required: {
          label: label,
        },
      };
    case 'phonePattern':
      return {
        key: VALIDATORS_KEY['phonePattern'],
        required: {},
      };
    case 'maxlength':
      return {
        key: VALIDATORS_KEY['maxlength'],
        required: {
          requiredLength: errorValue['requiredLength'],
        },
      };
    case 'minlength':
      return {
        key: VALIDATORS_KEY['minlength'],
        required: {
          requiredLength: errorValue['requiredLength'],
        },
      };
    case 'min':
      return {
        key: VALIDATORS_KEY['min'],
        required: {
          min: errorValue['min'],
        },
      };
    case 'max':
      return {
        key: VALIDATORS_KEY['max'],
        required: {
          max: errorValue['max'],
        },
      };
    case 'email':
      return {
        key: VALIDATORS_KEY['email'],
        required: {},
      };
    case 'datePattern':
      return {
        key: VALIDATORS_KEY['datePattern'],
        required: {
          datePattern: errorValue['requiredPattern'],
        },
      };
    case 'minDateAndMaxDate':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'minDateAndMaxDate' : 'minDateAndMaxDateNoLabel'],
        required: {
          label: label,
          minDate: errorValue['minDateLabel'],
          maxDate: errorValue['maxDateLabel'],
        },
      };
    case 'minDate':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'minDate' : 'minDateNoLabel'],
        required: {
          label: label,
          minDateLabel: errorValue['minDateLabel'],
        },
      };
    case 'maxDate':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'maxDate' : 'maxDateNoLabel'],
        required: {
          label: label,
          maxDateLabel: errorValue['maxDateLabel'],
        },
      };
    case 'maxDateToday':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'maxDateToday' : 'maxDateTodayNoLabel'],
        required: {
          label: label,
        },
      };
    case 'minDateToday':
      return {
        key: VALIDATORS_KEY[label !== 'EMPTY' ? 'minDateToday' : 'minDateTodayNoLabel'],
        required: {
          label: label,
        },
      };
    default:
      return {
        key: 'EMPTY',
        required: {},
      };
  }
}

export function removeAscent(str: string) {
  if (str === null || str === undefined) return str;
  str = str.toLowerCase();
  str = str.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a');
  str = str.replace(/[èéẹẻẽêềếệểễ]/g, 'e');
  str = str.replace(/[ìíịỉĩ]/g, 'i');
  str = str.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, 'o');
  str = str.replace(/[ùúụủũưừứựửữ]/g, 'u');
  str = str.replace(/[ỳýỵỷỹ]/g, 'y');
  str = str.replace(/đ/g, 'd');
  return str;
}

export function mapCommonCategoryByCode(data: CommonModel[], code: string) {
  return data
    ?.filter((o) => o.commonCategoryCode === code)
    ?.map((o) => {
      return { ...o, multiLanguage: convertStringToJson(o.description) };
    });
}

export function createErrorMessage(error: IErrorResponse) {
  if (_.isEmpty(error?.error?.messageKey)) {
    return `MESSAGE.${INTERNAL_ERROR_MESSAGE}`;
  } else if (error.error.messageKey === 'SAP_ERROR') {
    return error.error.message!;
  } else {
    return `MESSAGE.${error.error.messageKey}`;
  }
}

// function check file type or max size
export function isFileTypeValid(file: FileAttachment, accept: string): boolean {
  let acceptableTypes = accept.split(',').map((type) => type.trim());
  for (let type of acceptableTypes!) {
    let acceptable = isWildcard(type)
      ? getTypeClass(file.mimeType) === getTypeClass(type)
      : file.extension?.toLowerCase() === type?.toLowerCase();

    if (acceptable) {
      return true;
    }
  }

  return false;
}

export function getTypeClass(fileType: string): string {
  return fileType.substring(0, fileType.indexOf('/'));
}

export function isWildcard(fileType: string): boolean {
  return fileType.indexOf('*') !== -1;
}

export function exportFile(fileName: string, base64: string, fileType: string) {
  const name = fileName;
  const link = document.createElement('a');
  const arrayBuffer = new Uint8Array([...window.atob(base64)].map((char) => char.charCodeAt(0)));
  link.href = window.URL.createObjectURL(new File([arrayBuffer], fileName, { type: fileType }));
  link.setAttribute('download', name);
  document.body.appendChild(link);
  link.click();
  link.remove();
}

export function openFile(content: string, isPDF: boolean) {
  if (isPDF) {
    const url = URL.createObjectURL(dataURItoBlob(content, 'application/pdf'));
    window.open(url, '_blank')?.focus();
  } else {
    let image = new Image();
    image.src = `data:image/png;base64,${content}`;
    let wd = window.open('', '_blank');
    wd!.document.write(image.outerHTML);
  }
}

// end function check file type
