import {AuthService} from '@cores/services/auth.service';
import {Router} from '@angular/router';

export function initializer(authService: AuthService, router: Router): () => Promise<boolean> {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (!authService.getToken() && authService.isAccessTokenExpired()) {
        router.navigateByUrl('/login').then();
      }
      resolve(true);
    });
  };
}
