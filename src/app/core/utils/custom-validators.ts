import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {FileAttachment} from '@cores/models/file-attachment.model';
import {isFileTypeValid} from '@cores/utils/functions';
import {isEmpty, isString} from 'lodash';

const regexEmail = new RegExp(/^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,6},?)+$/);
const blockSpecialCharacter = /^[^~`'!@#$%^&()_={}[\]:;,.<>+\/?-]*$/;
const emailInvalid = ['yopmail'];

interface FileValidator {
  acceptType?: string;
  maxSize?: number;
}

export class CustomValidators {
  static email(control: AbstractControl): ValidationErrors | null {
    regexEmail.lastIndex = 0;

    if (!control.value || !isString(control.value)) {
      return null
    }

    if (!regexEmail.test(control.value.toString().replace(/\s/g, ','))) {
      return { email: true };
    }

    if (emailInvalid.includes(
        control.value.substring(control.value.lastIndexOf('@') + 1,
            control.value.lastIndexOf('.')
        ))
    ) {
      return {emailInvalid: true};
    }

    return null;
  }

  static emailArray(control: AbstractControl): ValidationErrors | null {
    if (control.value?.length) {
      for (let index in control.value) {
        let email = control.value[index]
        if (!regexEmail.test(email)) {
          return {email: true, position: index}
        }
      }
      return null;
    } else {
      return null
    }

  }

  static file(arg: FileValidator): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let files: FileAttachment[] = [];
      if (!arg.acceptType || !arg.maxSize || isEmpty(control.value)) {
        return null;
      } else {
        files = control.value instanceof Array ? control.value : [control.value];
        let error = null;
        for (const file of files) {
          if (arg.acceptType && !isFileTypeValid(file, arg.acceptType)) {
            error = { file: { acceptType: arg.acceptType } };
            break;
          }
          if (arg.maxSize && file.size > arg.maxSize) {
            error = { file: { maxSize: `${arg.maxSize / Math.pow(1000, 2)}Mb` } };
            break;
          }
        }
        return error;
      }
    };
  }

  static blockSpecialCharacter(control: AbstractControl): ValidationErrors | null {
    if (!isEmpty(control.value)) {
      if (!control.value.match(blockSpecialCharacter)) {
        return null;
      } else {
        return { blockSpecialCharacter: true };
      }
    } else {
      return null;
    }
  }
}
