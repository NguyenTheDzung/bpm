import { MapTranslateConfig } from '@cores/models/map-translate-config.model';

export const APP_LOADING = 'APP_LOADING';
export const REFRESH_BREACRUMB = 'REFRESH_BREACRUMB';

export const VALIDATORS_MESSAGE: any = {
  required: 'Trường bắt buộc',
  minlength: 'Độ dài tối thiểu %d ký tự',
  maxlength: 'Độ dài tối đa %d ký tự',
  min: 'Giá trị tối thiểu là %d',
  max: 'Giá trị tối đa là %d',
  email: 'Sai định dạng email',
  emailInvalid: 'Email không hợp lệ',
  phone: 'Sai định dạng SĐT',
  noSpace: 'Không nhập ký tự khoảng trắng',
  blockSpecialCharacter: 'Không được chứa ký tự đặc biệt',
};

// mã danh mục
export const DECISION = 'DECISION';
export const REASON = 'REASON';
export const SUBREASONAPPROVE = 'SUBREASONAPPROVE';
export const PENDING_REASON = 'PENDING_REASON';
export const REJECT_REASON = 'REJECT_REASON';
export const ATTACHMENT_DROPLIST = 'ATTACHMENT_DROPLIST';
export const APPROVEDREASON = 'APPROVEDREASON';
export const RESOLUTION = 'RESOLUTION';
export const STATUS = 'STATUS';
export const REQUEST_TYPE = 'REQUEST_TYPE';
export const DISTRIBUTION_CHANNEL = 'DISTRIBUTION_CHANNEL';
export const P_TO_P_DECICISON = 'P_TO_P_DECICISON'; // danh mục step approve luồng refund
export const POST_TEXT = 'POST_TEX';
export const GROUP_USER = 'GROUP_USER';
export const SAP_FILE_TYPE_NBU = 'SAP_FILE_TYPE_NBU';
export const ACCOUNT_STATUS = 'ACCOUNT_STATUS';
export const ACCOUNT_IS_LOCK = 'ACCOUNT_IS_LOCK';
export const NBU_APP_UW_REQ_PROCESS_OPTION = 'NBU_APP_UW_REQ_PROCESS_OPTION';
export const NBU_APP_REFUSAL_REASON = 'NBU_APP_REFUSAL_REASON';
export const NBU_APP_REFUSAL_SUB_REASON = 'NBU_APP_REFUSAL_SUB_REASON';
export const PAYMENT_METHOD = 'PAYMENT_METHOD';
export const IDENTIFY_TYPE = 'IDENTIFY_TYPE';
export const COLLECTION_DECISION_CONTRACT_STORE = 'COLLECTION_DECISION_CONTRACT_STORE';
export const GENDER = 'GENDER';
export const LOAI_CHUNGTU_PHI = 'LOAI_CHUNGTU_PHI';
export const PERIODICFEE_PAYMENT = 'PERIODICFEE_PAYMENT';
export const CATEGORY_DROPLIST_NOPCHUNGTUPHI = 'CATEGORY_DROPLIST_NOPCHUNGTUPHI';
export const DAILY_NEXT_ACTION = 'DAILY_NEXT_ACTION';
export const RESOLUTION_COLLECTION = 'RESOLUTION_COLLECTION';
export const RESOLUTION_REFUND = 'RESOLUTION_REFUND';
export const REQUEST_TYPE_REFUND = 'REQUEST_TYPE_REFUND';
export const SAP_IDENTIFICATION_TYPE_CODE = 'SAP_IDENTIFICATION_TYPE_CODE';
export const PAYFRQ_CD = 'PAYFRQ_CD';
export const PREMIUM_ADJUSTMENT_DECISION = 'PREMIUM_ADJUSTMENT_DECISION';
export const NOTIFICATION = 'NOTIFICATION';
export const CLAIM_SOURCE_REQUEST = 'CLAIM_SOURCE_REQUEST';
export const FILE_TYPE_CLAIM_01 = 'FILE_TYPE_CLAIM_01';
export const CLAIM_STATUS = 'CLAIM_STATUS';
export const CLAIM_SUBMISSION_CHANNEL = 'CLAIM_SUBMISSION_CHANNEL';
//Nguồn chứng từ Claim
export const CLAIM_RESOURCE_ATTACHMENT = 'CLAIM_RESOURCE_ATTACHMENT';
export const INTERNAL_ERROR_MESSAGE = 'E_INTERNAL_SERVER';
//Chi tiết claim  ||  Quyết định tát thẩm định
export const CLAIM_UNDERWRITING_DECISION = 'CLAIM_UNDERWRITING_DECISION';

export const RESOLUTION_CLAIM = 'RESOLUTION_CLAIM';

export const MAX_FILE_SIZE = 10000000;

// Role code
export const Roles = {
  // COL
  OP_COL_ADMIN: 'OP_COL_ADMIN',
  OP_COL_MANAGER: 'OP_COL_MANAGER',
  OP_COL_REVIEW: 'OP_COL_REVIEW',
  OP_COL_USER: 'OP_COL_USER',
  OP_COL_CSSS: 'OP_COL_CSSS',
  // NBU
  OP_NBU_ADMIN: 'OP_NBU_ADMIN',
  OP_NBU_MANAGER: 'OP_NBU_MANAGER',
  OP_NBU_REVIEW: 'OP_NBU_REVIEW',
  OP_NBU_USER: 'OP_NBU_USER',
  OP_NBU_USER_COLLECTION: 'OP_NBU_USER_COLLECTION',
  OP_NBU_UNDERWRITTING: 'OP_NBU_UNDERWRITTING',

  //Claim
  OP_CLAIM_USER: 'OP_CLAIM_USER',
  // Chuyên viên giải quyết quyền lợi bảo hiểm Claim
  OP_CLAIM_ASSESSOR: 'OP_CLAIM_ASSESSOR',
  //  Phó phòng giải quyết bảo hiểm
  OP_CLAIM_DEPUTY: 'OP_CLAIM_ASSESSOR',
  // Trưởng phòng giải quyết quyền lợi bảo hiểm
  OP_CLAIM_MANAGER: 'OP_CLAIM_MANAGER',
  // Hội đồng giải quyết quyền lợi bảo hiểm
  CLAIM_COMMITTEE: 'CLAIM_COMMITTEE',
  OP_CLAIM_ADMIN: 'OP_CLAIM_ADMIN',
  OP_CLAIM_INVESTIGATOR: 'OP_CLAIM_INVESTIGATOR',
  OP_CLAIM_INVESTIGATOR_ADMIN: 'OP_CLAIM_INVESTIGATOR_ADMIN',
  OP_CLAIM_ADMIN_INVESTIGATOR: 'OP_CLAIM_ADMIN_INVESTIGATOR',

  // OTHER
  OP_DIRECTOR: 'OP_DIRECTOR',
  FI_USER: 'FI_USER',
  SYSTEM_ADMIN: 'SYSTEM_ADMIN',
  USER_SALE: 'USER_SALE',
  MEMBER: 'MEMBER',
  VIEW_DASHBOARD: 'VIEW_DASHBOARD',
  GUEST: 'GUEST',
};

// resolution bpm
export const Resolution = {
  //NBU
  NBU_INPROCESS: 'NBU in-process',
  NBU_REJECT: 'NBU reject',
  NBU_APPROVE: 'NBU approve',
  NBU_FI_DONE: 'FI_done',
  NBU_FI_INPROCESS: 'FI_inprocess',
  NBU_P2P_INPROCESS: 'P2P in-process',
  NBU_SALE_PENDING: 'SALE pending',
  NBU_SLA_SALE_PENDING_END: 'Sla sale pending end',
  NBU_SALE_INPROCESS: 'SALE In-process',

  // COL
  COL_NEW_SUBMIT: 'NEW_SUBMIT',
  // COL_INPROCESS: 'COL_INPROCESS',
  // COL_REJECT: 'COL_REJECT',
  // COL_APPROVED: 'COL_APPROVED',
  // COL_PENDING: 'COL_PENDING',
  COL_SALE_PENDING: 'SALE_PENDING',
  COL_P2P_INPROCESS: 'P2P_INPROCESS',
  COL_P2P_REJECT: 'P2P_REJECT',
  COL_FI_INPROCESS: 'FI_INPROCESS',
  COL_FI_DONE: 'FI_DONE',
  // COL_APPROVE_COL_INPROCESS: 'APPROVE_COL_INPROCESS',
  // COL_APPROVE_OP_INPROCESS: 'APPROVE_OP_INPROCESS',
  // COL_PA_INPROCESS: 'PA_INPROCESS',
  // COL_PA_DONE: 'PA_DONE',
  OP_COL_USER_INPROCESS: 'OP_COL_USER_INPROCESS',
  OP_COL_USER_REJECT: 'OP_COL_USER_REJECT',
  OP_COL_USER_APPROVED: 'OP_COL_USER_APPROVED',
  OP_COL_MANAGER_INPROCESS: 'OP_COL_MANAGER_INPROCESS', //APPROVE_COL_INPROCESS
  OP_COL_MANAGER_REJECT: 'OP_COL_MANAGER_REJECT',
  OP_DIRECTOR_INPROCESS: 'OP_DIRECTOR_INPROCESS', //APPROVE_OP_INPROCESS
  OP_DIRECTOR_REJECT: 'OP_DIRECTOR_REJECT',
  PA_USER_INPROCESS: 'PA_USER_INPROCESS',
  PA_USER_DONE: 'PA_USER_DONE',

  //CLAIM
  OP_CLAIM_INVESTIGATOR_INPROCESS: 'OP_CLAIM_INVESTIGATOR_INPROCESS',
  OP_NBU_UNDERWRITING_INPROCESS: 'OP_NBU_UNDERWRITING_INPROCESS',
};

//status request
export const StatusValue = {
  In_process: 'In process',
  Reject: 'Reject',
  Complete: 'Complete',
  Close: 'Close',
  Overdue: 'OVERDUE',
};
//phương thức thanh toán
export const PaymentMethod = {
  Banking: 'Banking',
  Cash: 'Cash',
};
export const StatusItemLot = {
  Completed: 'Completed',
  Incomplete: 'Incomplete',
};
//định kì đóng phí
export const Frequency = {
  Monthly: '12',
  Annually: '1',
  SinglePerium: '0',
  SemiAnnually: '2',
  Quartelly: '4',
  Biweekly: '26',
  Weeklus: '52',
};
//Dùng cho multi language validate input
export const VALIDATORS_KEY: any = {
  required: 'VALIDATE_ERROR.REQUIRED',
  requiredNoLabel: 'VALIDATE_ERROR.REQUIRED_NO_LABEL',
  pattern: 'VALIDATE_ERROR.PATTERN',
  patternNoLabel: 'VALIDATE_ERROR.PATTERN_NO_LABEL',
  minlength: 'VALIDATE_ERROR.MINLENGTH',
  maxlength: 'VALIDATE_ERROR.MAXLENGTH',
  max: 'VALIDATE_ERROR.MAX',
  min: 'VALIDATE_ERROR.MIN',
  email: 'VALIDATE_ERROR.EMAIL',
  datePattern: 'VALIDATE_ERROR.DATEPATTERN',
  minDateAndMaxDate: 'VALIDATE_ERROR.MINDATE_MAXDATE',
  minDateAndMaxDateNoLabel: 'VALIDATE_ERROR.MINDATE_MAXDATE_NO_LABEL',
  minDate: 'VALIDATE_ERROR.MINDATE',
  minDateNoLabel: 'VALIDATE_ERROR.MINDATE_NO_LABEL',
  maxDate: 'VALIDATE_ERROR.MAXDATE',
  phonePattern: 'VALIDATE_ERROR.PHONEPATTERN',
  maxDateToday: 'VALIDATE_ERROR.MAXDATE-TODAY',
  maxDateTodayNoLabel: 'VALIDATE_ERROR.MAXDATE_TODAY_NO_LABEL',
  minDateToday: 'VALIDATE_ERROR.MINDATE-TODAY',
  minDateTodayNoLabel: 'VALIDATE_ERROR.MINDATE_TODAY_NO_LABEL',
  maxSize: 'VALIDATE_ERROR.MAX_SIZE_FILE',
  acceptType: 'VALIDATE_ERROR.FILE_TYPE',
};

export const Pattern = {
  NAME: '^([^~!@#$%^&*"()_+={}\\[\\]|\\\\:;“’<,>.๐฿\\/?]+ [^~!@#$%^&*"()_+={}\\[\\]|\\\\:;“’<,>.๐฿\\/?]+)$',
  IDENTIFY_CARD: '^([0-9]{9}|[0-9]{12})$',
  PASSPORT: '^[A-Za-z][0-9]{7}$',
  DRIVING_LICENSE: '^[A-Za-z0-9]{3}[0-9]{4,8}$|^[0-9]{12}$',
};

//LOẠI GIẤY TỜ TÙY THÂN
export const IdentificationType = {
  CCCD: 'CCCD',
  CMND: 'CMND',
  GPLX: 'GPLX',
  PASSPORT: 'HC',
};

// loại giấy tờ tuỳ thân từ SAP
export const IdentificationTypeSAP = {
  Type01: 'ZMBL01', //Giấy khai sinh
  Type02: 'ZMBL02', //Chứng minh nhân dân
  Type03: 'ZMBL03', //Căn cước công dân
  Type04: 'FS0002', //Hộ chiếu
  Type05: 'ZMBL13', //Mã số thuế cá nhân
  Type06: 'ZMBL18', //Thẻ căn cước quân sự
  Type07: 'ZMBL04', //Giấy phép lái xe
};
//status của payment lot
export const StatusPaymentLot = {
  FREE: 'Free',
  ALLOCATED: 'ALLOCATED',
  REVERSED: 'REVERSED',
  REPAYMENT: 'REPAYMENT',
  IN_PROCESS: 'In Process',
};

export const NotificationStatus = {
  Action: 'ACTION',
  Pending: 'PENDING',
  Reject: 'REJECT',
  Approve: 'APPROVE',
  Forward: 'FORWARD',
};

export const NotifyType = {
  case1: 'TH1',
  case2: 'TH2',
  case3: 'TH3',
  case4: 'TH4',
  case5: 'TH5',
  case6_1: 'TH6_1',
  case6_2: 'TH6_2',
  case7: 'TH7',
  case8_1: 'TH8_1',
  case8_2: 'TH8_2',
  case9: 'TH9',
  case10: 'TH10',
  case11: 'TH11',
  case12: 'TH12',
  case13: 'TH13',
  case14: 'TH14',
  case15: 'TH15',
  case16: 'TH16',
};
export const regexNameFormat = '^([^\\d!@#\\$%\\^\\&*\\)\\(,?+=._-]+\\s{1}[^\\d!@#\\$%\\^\\&*\\)\\(,?+=._-]+)';
export const regexStartLetter = '^[A-Za-z]+[A-Za-z0-9]*[A-Za-z0-9]';

export const REFRESH_TOKEN_STATE_KEY = 'REFRESH_TOKEN_STATE_KEY';
export const MAX_TIME_DIFF_TO_GET_REFRESH_TOKEN = 3;

export const DEFAULT_MAP_TRANSLATE_CONFIG: MapTranslateConfig = {
  matchFieldName: 'value',
  fieldContainTranslate: 'multiLanguage',
  emptyTemplate: '- - -',
};

export const ClaimCommonType =  {
  STATUS_CLAIM: "STATUS_CLAIM",
  SICKNESS: "SICKNESS",
  FACILITY: "FACILITY",
  BENEFIT:"BENEFIT",
  RESOURCE_DOC: "RESOURCE_DOC",
  STATUS_REQUEST:"STATUS_REQUEST",
  RESOLUTION_REQUEST:"RESOLUTION_REQUEST"
} as const

/**
 *  Cấu hình hết hạn một common claim trong session storage
 */
export const DEFAULT_TIME_SPAN_SESSION_CLAIM_COMMON = 180000 //ms
export const CACHE_COMMON_CATEGORY_KEY = "BPM_CACHE_COMMON_CONFIG"
