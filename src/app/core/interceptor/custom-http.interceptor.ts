import {Router} from '@angular/router';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent,
} from '@angular/common/http';
import {Inject, Injectable, InjectionToken} from '@angular/core';
import {filter, finalize, Observable, switchMap, take, takeUntil, tap, throwError, timeout} from 'rxjs';
import {HttpCancelService} from '@cores/services/http-cancel.service';
import {environment} from '@env';
import {AuthService} from '@cores/services/auth.service';
import * as moment from 'moment';
import {v4 as uuIdv4} from 'uuid';
import {MAX_TIME_DIFF_TO_GET_REFRESH_TOKEN, REFRESH_TOKEN_STATE_KEY, Roles} from '@cores/utils/constants';
import {SessionService} from '@cores/services/session.service';
import {REFRESH_TOKEN_STATE} from '@cores/utils/enums';

export const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
  refresh$ = this.sessionService.onStorageChange(REFRESH_TOKEN_STATE_KEY);

  get refreshTokenInProgress(): REFRESH_TOKEN_STATE {
    return localStorage.getItem(REFRESH_TOKEN_STATE_KEY) as REFRESH_TOKEN_STATE;
  }

  set refreshTokenInProgress(value: REFRESH_TOKEN_STATE) {
    localStorage.setItem(REFRESH_TOKEN_STATE_KEY, value);
  }

  constructor(
    private router: Router,
    private authService: AuthService,
    private httpCancelService: HttpCancelService,
    private sessionService: SessionService,
    @Inject(DEFAULT_TIMEOUT) protected defaultTimeout: number
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    const accessExpired = this.authService.isAccessTokenExpired();
    const refreshExpired = this.authService.isRefreshTokenExpired();

    if (
      req.url.startsWith(`${environment.employee_url}/login`) ||
      req.url.startsWith(`${environment.employee_url}/refresh`)
    ) {
      return next.handle(req).pipe(finalize(() => (this.refreshTokenInProgress = REFRESH_TOKEN_STATE.COMPLETE)));
    }

    if (
      (accessExpired || this.authService.getDiffTimerTokenExpired() <= MAX_TIME_DIFF_TO_GET_REFRESH_TOKEN) &&
      !refreshExpired
    ) {
      if (this.refreshTokenInProgress == REFRESH_TOKEN_STATE.COMPLETE) {
        this.refreshTokenInProgress = REFRESH_TOKEN_STATE.PENDING;

        return this.authService.requestAccessToken().pipe(
          tap({
            error: () => {
              this.router.navigateByUrl('/login').then();
            },
          }),
          switchMap((authResponse) => {
            this.authService.saveToken(authResponse.data);
            return this.handleRequest(req, next);
          })
        );
      } else {
        return this.refresh$.pipe(
          filter((event) => !!event.newValue && event.newValue === REFRESH_TOKEN_STATE.COMPLETE),
          take(1),
          switchMap(() => {
            return this.handleRequest(req, next);
          })
        );
      }
    } else if (!accessExpired) {
      return this.handleRequest(req, next).pipe(
        tap({
          error: (error: HttpErrorResponse) => {
            if (error.status === 401) {
              localStorage.clear();
              this.router.navigateByUrl('/login').then();
            }
          },
        })
      );
    } else {
      this.router.navigateByUrl('/login').then();
      return throwError(() => 'authentication');
    }
  }

  handleRequest(req: HttpRequest<any>, next: HttpHandler) {
    const timeoutValue = req.headers.get('timeout') || this.defaultTimeout;
    const timeoutValueNumeric = Number(timeoutValue);
    return next
      .handle(this.injectToken(req))
      .pipe(timeout(timeoutValueNumeric), takeUntil(this.httpCancelService.onCancelPendingRequests()));
  }

  injectToken(request: HttpRequest<any>) {
    const token = this.authService.getToken();
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
        msgId: uuIdv4(),
        frSystm: this.authService.getUserRoles().includes(Roles.OP_COL_CSSS) ? 'CS/SS' : 'BPM',
        creDtTm: moment(new Date()).format(),
      },
    });
  }
}
