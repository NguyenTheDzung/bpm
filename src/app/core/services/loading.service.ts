import {Injectable} from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  private _loading$ = new Subject<boolean>();
  loading = false;
  start() {
    this._loading$.next(true);
    this.loading = true;
  }

  complete() {
    this._loading$.next(false);
    this.loading = false;
  }

  get loading$(){
    return this._loading$.asObservable()
  }
}
