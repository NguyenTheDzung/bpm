import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable, take } from 'rxjs';
import { APP_LOADING } from '../utils/constants';
import { SessionService } from './session.service';
import { StreamDataService } from './stream-data.service';
import { AuthService } from '@cores/services/auth.service';
import { SessionKey } from '@cores/utils/enums';
import { NotificationSystemService } from '@notification-system/services/notification-system.service';
import { convertStringToJson } from '@cores/utils/functions';

@Injectable({
  providedIn: 'root',
})
export class AppGuardService implements CanActivate {
  constructor(
    private streamData: StreamDataService,
    private sessionService: SessionService,
    private authService: AuthService,
    private router: Router,
    private notifySystemService: NotificationSystemService
  ) {}

  canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>((_observable) => {
      forkJoin([this.authService.getMenu(), this.authService.getUserProfile$().pipe(take(1))]).subscribe({
        next: ([dataMenu, infoUser]) => {
          const listMenu: any[] = [];
          dataMenu.data.content?.forEach((menu: any) => {
            if (!menu.disabled) {
              listMenu.push({
                ...menu,
                multiLanguage: convertStringToJson(menu.description),
              });
            }
          });
          this.sessionService.setSessionData(SessionKey.Menu, {
            menu: listMenu,
            menuOfUser: infoUser?.permissionResponseDTOS,
          });
          this.sessionService.setSessionData(SessionKey.UserProfile, infoUser?.userDTO);
          this.notifySystemService.getData(0, Number.MAX_SAFE_INTEGER).subscribe({
            next: (_notify) => {
              this.streamData.passData(APP_LOADING, true);
              return _observable.next(true);
            },
            error: () => {
              this.streamData.passData(APP_LOADING, true);
              return _observable.next(true);
            },
          });
        },
        error: () => {
          this.streamData.passData(APP_LOADING, true);
        },
      });
    });
  }
}
