import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, filter, map, Observable, tap} from 'rxjs';
import {environment} from 'src/environments/environment';
import {SessionKey} from '@cores/utils/enums';
import * as moment from 'moment';
import {AuthModel, Profile, ResponsiveLogin} from '@cores/models/auth.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _roles$ = new BehaviorSubject<string[]>([]);
  private _userProfile$ = new BehaviorSubject<Profile | null>(null);

  constructor(private http: HttpClient) {}

  login(data: any) {
    return this.http.post<ResponsiveLogin<AuthModel>>(`${environment.employee_url}/login`, data).pipe(
      map((res: any) => {
        const authModel: AuthModel = {
          ...res.data,
        };
        this.saveToken(authModel);
        return authModel;
      })
    );
  }

  logout() {
    return this.http.post(`${environment.employee_url}/logout`, {}).pipe(
      tap({
        next: () => {
          localStorage.removeItem(SessionKey.TOKEN);
          localStorage.removeItem(SessionKey.REFRESH_TOKEN);
          localStorage.removeItem(SessionKey.EXP_TOKEN);
          localStorage.removeItem(SessionKey.EXP_REFRESH_TOKEN);
          localStorage.removeItem(SessionKey.UserProfile);
          this.userProfile = null;
          this.setUserRoles([]);
        },
      })
    );
  }

  requestAccessToken() {
    return this.http.post<any>(`${environment.employee_url}/refresh`, {
      refreshToken: localStorage.getItem(SessionKey.REFRESH_TOKEN),
    });
  }

  saveToken(auth: AuthModel) {
    localStorage.setItem(SessionKey.TOKEN, auth.accessToken);
    localStorage.setItem(SessionKey.REFRESH_TOKEN, auth.refreshToken);
    const tokenExpired = moment().add(auth.expiresIn, 'seconds').valueOf();
    const refreshExpired = moment().add(auth.refreshExpiresIn, 'seconds').valueOf();
    localStorage.setItem(SessionKey.EXP_TOKEN, tokenExpired?.toString());
    localStorage.setItem(SessionKey.EXP_REFRESH_TOKEN, refreshExpired.toString());
  }

  getToken() {
    return localStorage.getItem(SessionKey.TOKEN)!;
  }

  getDiffTimerTokenExpired() {
    return moment(+localStorage.getItem(SessionKey.EXP_TOKEN)!).diff(moment(), 'minutes');
  }

  isAccessTokenExpired() {
    return +localStorage.getItem(SessionKey.EXP_TOKEN)! < new Date().valueOf();
  }

  isRefreshTokenExpired() {
    return +localStorage.getItem(SessionKey.EXP_REFRESH_TOKEN)! < new Date().valueOf();
  }

  getInfoCurrentUser(): Observable<Profile> {
    return this.http
      .post<Profile>(`${environment.employee_url}/keycloak/user/current-user`, {
        audience: environment.app.relationShip,
        response_mode: 'permissions',
        grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
      })
      .pipe(
        tap((data) => {
          this.userProfile = data;
          this.setUserRoles(data.userDTO.roles);
        })
      );
  }

  get userProfile() {
    return this._userProfile$.getValue();
  }

  set userProfile(value) {
    this._userProfile$.next(value);
  }

  getUserProfile$() {
    if (!this.userProfile) {
      this.getInfoCurrentUser().subscribe();
    }
    return this._userProfile$.asObservable().pipe(filter((value) => value != null));
  }

  getUserRoles() {
    return this._roles$.getValue();
  }

  getUserRoles$() {
    return this._roles$.pipe(filter((roles) => roles.length > 0));
  }

  setUserRoles(value: string[]) {
    this._roles$.next(value);
  }

  getMenu(): Observable<any> {
    return this.http.get<any>(`${environment.employee_url}/resource`);
  }
}
