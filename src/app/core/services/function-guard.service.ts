import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { SessionKey } from '@cores/utils/enums';
import { Observable } from 'rxjs';
import { SessionService } from './session.service';
import { StreamDataService } from './stream-data.service';

@Injectable({
  providedIn: 'root',
})
export class FunctionGuardService implements CanActivate {
  constructor(private sessionService: SessionService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>((observable) => {
      const listFunction = this.sessionService.getSessionData(SessionKey.Menu)?.menuOfUser;
      const objFunction = listFunction?.find((item: any) => item.rsname === route.data['functionCode']);
      if (route.data['functionCode'] && !objFunction) {
        this.router.navigateByUrl('/bpm/dashboard');
        return observable.next(false);
      } else {
        return observable.next(true);
      }
    });
  }
}
