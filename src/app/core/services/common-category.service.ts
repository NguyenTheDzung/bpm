import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SessionService } from './session.service';
import { CommonModel } from '@common-category/models/common-category.model';
import { convertStringToJson } from '@cores/utils/functions';
import { CommonClaimCategory, CommonClaimServiceOpts } from '@cores/models/common-claim.model';
import { CacheService } from '@cores/services/cache.service';
import { IResponseModel } from '@cores/models/response.model';

@Injectable({
  providedIn: 'root',
})
export class CommonCategoryService {
  constructor(private http: HttpClient, private session: SessionService, private cacheService: CacheService) {}

  getCommonCategory(code: string): Observable<CommonModel[]> {
    const key = `STATE_CATEGORY_${code}`
    const state = this.session.getSessionData(key);
    if (state) {
      return of(state);
    }
    return this.http.get(`${environment.category_url}/common?common-category-code=${code}`).pipe(
      map((val: any) =>
        val.data.content?.map((item: CommonModel) => {
          return { ...item, multiLanguage: convertStringToJson(item.description) };
        })
      ),
      tap((rs) => {
        this.session.setSessionData(key, rs)
      })
    );
  }

  getCommonByCodes(codes: string[]): Observable<CommonModel[]> {
    return this.http.get(`${environment.category_url}/common/categories`, { params: { listCategoryCode: codes } }).pipe(
      map((val: any) =>
        val.data?.map((item: CommonModel) => {
          return { ...item, multiLanguage: convertStringToJson(item.description) };
        })
      ),
      catchError(() => of([]))
    );
  }

  getCommonClaimByTypes(type: string, options?: Partial<CommonClaimServiceOpts>): Observable<CommonClaimCategory[]> {
    const url = `${environment.claim_url}/claim/common/search`;
    const key = `CLAIM_COMMON_${type}`;
    const httpRequest$ = this.http.get<IResponseModel<CommonClaimCategory[]>>(url, {
      params: {
        ...options?.params,
        type,
      },
    });
    return this.cacheService.cachingWithHttp(key, httpRequest$);
  }

  getCommonClaimByCode(code: string, options?: Partial<CommonClaimServiceOpts>): Observable<CommonClaimCategory[]> {
    const url = `${environment.claim_url}/claim/common`;
    const key = `CLAIM_COMMON_${code}`;
    const httpRequest$ = this.http.get<IResponseModel<CommonClaimCategory[]>>(url, {
      params: {
        ...options?.params,
        code,
      },
    });
    return this.cacheService.cachingWithHttp(key, httpRequest$);
  }
}
