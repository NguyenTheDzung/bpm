import { Injectable } from '@angular/core';
import { SessionKey } from '@cores/utils/enums';
import { filter, fromEvent, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private sessionVariables: Map<string, any> = new Map<string, any>();

  public setSessionData(key: string, value: any): void {
    if (key === SessionKey.UserProfile && !!value) {
      localStorage.setItem(SessionKey.UserProfile, JSON.stringify(value));
    } else {
      this.sessionVariables.set(key, value);
    }
  }

  public getSessionData(key: string): any {
    if (!!key) {
      return key === SessionKey.UserProfile
        ? JSON.parse(localStorage.getItem(SessionKey.UserProfile)!)
        : this.sessionVariables.get(key);
    }

    return undefined;
  }

  /**
   * Method lắng nghe sự kiện thay đổi của storage sử dụng `fromEvent` của rxjs.
   * @param key - Là một key trong storage mà muốn lắng nghe thay đổi
   * @return {Observable<StorageEvent>} Nếu có truyền key vào thì dữ liệu trả ra sẽ chỉ lắng nghe theo sự thay đổi **duy nhất key đó**. Nếu không thì sẽ lắng nghe theo **Tất cả sự thay đổi của storage**
   */
  public onStorageChange(key?: string): Observable<StorageEvent> {
    return fromEvent(window, 'storage').pipe(
      map((event) => <StorageEvent>event),
      filter((event) => (!!key && event.key == key) || !key)
    );
  }

  public clearSessionData(key: string): void {
    if (key !== undefined && key !== null) {
      this.sessionVariables.delete(key);
    }
  }

  public clearSession(): void {
    this.sessionVariables.clear();
  }
}
