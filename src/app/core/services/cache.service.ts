import { Injectable } from '@angular/core';
import { isNumber } from "lodash";
import { map, Observable, of, tap } from "rxjs";
import { IResponseModel } from "@cores/models/response.model";
import { CACHE_COMMON_CATEGORY_KEY, DEFAULT_TIME_SPAN_SESSION_CLAIM_COMMON } from "@cores/utils/constants";
import { environment } from "@env";
import { CommonModel } from "@common-category/models/common-category.model";
import { HttpClient } from '@angular/common/http';


@Injectable( { providedIn: 'root' } )
export class CacheService {
  /**
   * Represents a cache storing key-value pairs with associated expiration timestamps.
   * Each cached item is stored as an object with 'value' and 'expiration' properties.
   */
  private cache: Map<string, { value: any; expiration: number }> = new Map();

  /**
   * A unique channel name used for synchronization with other components or windows.
   * This channel is used to inform other parts of the application about cache changes.
   */
  private readonly channelName = 'cache-sync-channel';

  /**
   * A BroadcastChannel instance for broadcasting cache-related messages to other components
   * or windows within the application. Used for cache synchronization.
   */
  private broadcastChannel!: BroadcastChannel;


  // This is a hash every key in cache must have to manage cache separate with another key
  private readonly KEY_CONTAIN = "BPM_CACHE"

  // Property to hold the list of keys to ignore
  private cachingKeys: Map<string, boolean> = new Map();

  constructor(
    private http: HttpClient
  ) {


    console.log( "INIT_CACHE" )
    // Initialize the service by loading data from session storage and setting up broadcast communication
    this.loadFromLocalStorage();
    this.setupBroadcastChannel();

    // Fetch the ignore keys list from an API (assuming you have a method for this)
    this.fetchCachingKey().subscribe();
  }


  /**
   * Fetches a list of keys from an API to be ignored when caching data.
   * These keys determine which cache items should not be stored or retrieved.
   * The fetched keys are stored in the 'cachingKeys' property for reference.
   *
   * @returns void
   */
  private fetchCachingKey() {
    const url = `${ environment.category_url }/common?common-category-code=${ CACHE_COMMON_CATEGORY_KEY }`
    return this.http.get( url ).pipe(
      map( ( val: any ) => val.data.content as CommonModel[] ),
      tap( ( rs ) => {
        this.cachingKeys = new Map<string, boolean>(
          rs
            .map( commonModel => [commonModel.code!, commonModel.value !== "0"] )
        )
        console.log(this.cachingKeys)
      } )
    )
  }


  /**
   * Cache a key-value pair with an optional expiration time.
   * @param key The key to store in the cache.
   * @param value The value to cache.
   * @param expirationMs Optional expiration time in milliseconds.
   */
  set( key: string, value: any, expirationMs: number = DEFAULT_TIME_SPAN_SESSION_CLAIM_COMMON ) {
    // Check if the key is in the ignore list
    if (
      !this.cachingKeys.has( key ) ||
      !this.cachingKeys.get( key ) ) {
      return
    }

    // Store the data in the in-memory cache
    this.cache.set(
      // Re-generate key
      this.generateKey( key ),
      {
        value,
        expiration: Date.now() + expirationMs,
      }
    );

    // Synchronize the in-memory cache with session storage and notify other tabs
    this.syncWithLocalStorage();
    this.broadcastDataChange( key );
  }

  /**
   * Retrieve a cached value by its key, checking for expiration.
   * Only retrieve key contained the default key contain
   * @param key The key to retrieve from the cache.
   * @returns The cached value or null if the key is not found or has expired.
   */
  get<T>( key: string ): T | null {
    //In development will not cache
    // if ( !environment.production ) {
    //   return null
    // }

    // Ignore key are not cache generate
    if ( !key.startsWith( this.KEY_CONTAIN ) ) {
      return null
    }

    // Ignore key have before config
    if (
      !this.cachingKeys.has( key ) ||
      !this.cachingKeys.get( key )
    ) {
      return null;
    }

    //If not store in local cache try to look at localstorage
    return this.getInCache( key ) as T || this.getInLocalStorage( key ) as T

  }

  /**
   * Clear a specific key from the cache and notify other tabs.
   * @param key The key to clear from the cache.
   */
  clear( key: string ) {
    // Remove the key from the in-memory cache and notify other tabs
    this.cache.delete( key );
    this.syncWithLocalStorage();
    this.broadcastDataChange( key );
  }

  /**
   * Clear the entire cache and session storage, and notify other tabs.
   * This method removes all cached data from both the in-memory cache and session storage,
   * and then broadcasts a message to notify other tabs about the cache clearance.
   */
  clearAll() {
    this.cache.clear();
    for ( let i = 0; i < localStorage.length; i++ ) {
      const key = localStorage.key( i )!;

      if ( !key.startsWith( this.KEY_CONTAIN ) ) {
        continue;
      }
      localStorage.removeItem( key );
    }
    this.broadcastDataChange( 'clearAll' );
  }

  /**
   * Caches and retrieves data using a key and an HTTP Observable.
   * This method first attempts to retrieve data from the cache based on the provided key.
   * If the data is not found in the cache, it tries an alternative key generated from the original key.
   * If both cache retrieval attempts fail or an error occurs, the method logs the error and clears the cache.
   *
   * If cached data is found, it is returned as an observable with the cached data.
   * If no cached data is found, the method makes an HTTP request using the provided `HttpObservable`.
   * The response from the HTTP request is then stored in the cache with an optional expiration time.
   *
   * @param key - The key used to access and store data in the cache.
   * @param HttpObservable - An HTTP Observable representing the data retrieval process.
   * @param timeSpan - Optional. The duration in seconds for which the data should be cached.
   * @returns An observable that emits the cached or newly fetched data.
   */
  cachingWithHttp<T>( key: string, HttpObservable: Observable<IResponseModel<T>>, timeSpan?: number ): Observable<T> {
    let state: T | null = null;

    try {
      // Attempt to retrieve data from the cache based on the provided key.
      state = this.get<T>( key ) as T | null;
      if ( !state ) {
        // If data is not found, try an alternative key generated from the original key.
        state = this.get<T>( this.generateKey( key ) );
      }
    } catch ( e ) {
      // Handle any errors that occur during cache retrieval.
      console.error( e );
      this.clear( key );
    }

    if ( !!state ) {
      // Return the cached data as an observable.
      return of( state );
    }

    // If no cached data is found, make an HTTP request and cache the response.
    return HttpObservable.pipe(
      map( ( res: IResponseModel<T> ) => {
        // Store the response data in the cache.
        timeSpan ? this.set( key, res.data, timeSpan ) : this.set( key, res.data );
        return res.data;
      } )
    );
  }


  /**
   * Load cached data from session storage into the in-memory cache and remove expired items.
   * This method reads data from session storage, validates expiration, and populates the cache
   * with valid items. Expired items are removed from session storage.
   */
  private loadFromLocalStorage() {
    this.cache.clear();
    for ( let i = 0; i < localStorage.length; i++ ) {
      const key = localStorage.key( i )!;
      if ( !key.startsWith( this.KEY_CONTAIN ) ) {
        continue;
      }
      const itemString = localStorage.getItem( key );
      if ( itemString ) {
        try {
          const item = JSON.parse( itemString );
          if (
            item &&
            typeof item === 'object' &&
            'expiration' in item &&
            'value' in item &&
            isNumber( item.expiration )
          ) {
            if ( item.expiration > Date.now() ) {
              this.cache.set( key, item );
            } else {
              localStorage.removeItem( key );
            }
          }
        } catch ( error ) {
          console.error( `Error loading data for key ${ key }:`, error );
        }
      }
    }
  }

  /**
   * Set up a Broadcast Channel for cache synchronization with other tabs.
   * This method creates a Broadcast Channel and sets up an event handler to receive
   * and process messages for cache synchronization.
   */
  private setupBroadcastChannel() {
    this.broadcastChannel = new BroadcastChannel( this.channelName );
    this.broadcastChannel.onmessage = ( event ) => {
      const { key, value } = event.data;
      if ( key === 'clearAll' ) {
        // Handle "clearAll" event by clearing the cache and session storage
        this.clearAll();
      } else {
        // Handle other events by updating the cache
        // this.set(key, value);
        this.loadFromLocalStorage()
      }
    }
  }

  /**
   * Synchronize the in-memory cache with session storage.
   * This method updates session storage with the data in the in-memory cache.
   */
  private syncWithLocalStorage() {
    this.cache.forEach( ( value, key ) => {
      localStorage.setItem( key, JSON.stringify( value ) );
    } )
  }

  /**
   * Broadcast data changes to other tabs through the Broadcast Channel.
   * This method sends messages to notify other tabs about data changes.
   *
   * @param key - The key associated with the data change.
   */
  private broadcastDataChange( key: string ) {
    this.broadcastChannel.postMessage( { key } );
  }

  /**
   * Generate a key to be used in session storage.
   * This method appends a prefix to the provided key to ensure uniqueness in session storage.
   *
   * @param key - The original key for the data.
   * @returns A modified key for use in session storage.
   */
  private generateKey( key: string ) {
    return `${ this.KEY_CONTAIN }_${ key }`;
  }

  /**
   * Retrieves an item from the cache using the specified key and validates it.
   *
   * @param key - The key used to access the item in the cache.
   * @returns The item from the cache, or null if the item is invalid, expired, or not found.
   */
  private getInCache( key: string ): any {
    // Retrieve the item from the cache.
    let item = this.cache.get( key );

    // Validate the retrieved item and check for expiration.
    return this.validateItem( item, key );
  }

  /**
   * Validates an item, checks for expiration, and ensures it has the expected fields.
   *
   * @param item - The item to be validated.
   * @param key - The key associated with the item.
   * @returns The validated item, or null if it is invalid, expired, or lacks expected fields.
   */
  private validateItem( item: any, key: string ): any {
    if ( item && typeof item === 'object' ) {
      // Check if the value is an object and has the expected fields.
      // Also, check if it has expired or not.
      if ( !( "expiration" in item ) || !isNumber( item.expiration ) || item.expiration <= new Date().getTime() ) {
        // Clear the invalid or expired item from the cache.
        this.clear( key );
        return null;
      }
      if ( !( "value" in item ) ) {
        // Clear the item without the 'value' field from the cache.
        this.clear( key );
        return null;
      }
      return item?.value ?? null;
    }
    return null;
  }

  /**
   * Retrieves an item from local storage using the specified key and validates it.
   *
   * @param key - The key used to access the item in local storage.
   * @returns The item from local storage, or null if the item is invalid, expired, or not found.
   */
  private getInLocalStorage( key: string ): any {
    let item: any;

    try {
      // Retrieve the item from local storage and parse it as JSON.
      item = JSON.parse( localStorage.getItem( key ) ?? "null" )
    } catch ( e ) {
      // Handle any potential errors during retrieval and parsing.
      console.error( e );
      item = null;
    }

    // Validate the retrieved item.
    return this.validateItem( item, key ) ?? null;
  }


}
