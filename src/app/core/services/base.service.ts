import { saveAs } from 'file-saver';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { BaseData } from '../abstract/base-data';
import { DataTable } from '../models/data-table.model';
import { dataURItoBlob, mapDataTable, removeParamSearch } from '../utils/functions';

@Injectable()
export class BaseService implements BaseData {
  baseUrl!: string;
  state: any;

  constructor(public http: HttpClient, @Inject(String) public baseURL: string) {
    this.baseUrl = baseURL;
  }

  getState(): Observable<any> {
    return of(this.state);
  }

  search(params?: any, isPost?: boolean): Observable<DataTable> {
    const newParam: any = removeParamSearch(params);
    if (isPost) {
      return this.http
        .post<DataTable>(`${this.baseUrl}`, { params: newParam })
        .pipe(map((data) => mapDataTable(data, params)));
    }
    return this.http
      .get<DataTable>(`${this.baseUrl}`, {
        params: { ...newParam },
      })
      .pipe(map((data) => mapDataTable(data, params)));
  }

  findByUsername(username: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${username}`);
  }

  findByCode(code?: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${code}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(`${this.baseUrl}`, data);
  }

  update(data: any): Observable<any> {
    return this.http.put(`${this.baseUrl}`, data);
  }

  delete(id: string | number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }

  exportExcel(fileName: string, params: any, isBase64?: boolean): Observable<boolean> {
    const responseType = isBase64 ? 'json' : 'arraybuffer';
    const option: any = { params, responseType };

    return this.http.get(`${this.baseUrl}/export`, option).pipe(
      map((res: any) => {
        if (isBase64) {
          res = dataURItoBlob(res?.data?.data);
        }
        saveAs(
          new Blob([res], {
            type: 'application/octet-stream',
          }),
          fileName
        );
        return true;
      })
    );
  }
}
