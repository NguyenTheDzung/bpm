import { Injectable } from '@angular/core';
import { environment } from '@env';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Notification } from '@cores/models/notification.model';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  baseUrl = `${environment.task_url}/notification`;

  constructor(private http: HttpClient) {}

  fetchNotifications(params: any): Observable<Notification[]> {
    return this.http.get(this.baseUrl, { params }).pipe(map((res: any) => res.data?.data?.content));
  }

  readNotification(id: number) {
    return this.http.put(`${this.baseUrl}/${id}`, {});
  }

  countNotifications(): Observable<number> {
    return this.http.get(`${this.baseUrl}/count`).pipe(map((res: any) => res.data));
  }
}
