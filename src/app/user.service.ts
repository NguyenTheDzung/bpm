import { Injectable, Input } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpHeaders} from "@angular/common/http";
import {catchError, Observable} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
}
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {

  }
  getData(page: number, limit: number, account: string, position: string, status: string, keyEmployee: string, employeeName: string) {
    if (account && keyEmployee){
      return this.http.get<any>(`http://localhost:3004/listUser?q&_page=${page}&_limit=${limit}&account=${account}&position_like=${position}&status_like=${status}&keyEmployee=${keyEmployee}&employeeName_like=${employeeName}`).toPromise().then(res => res);
    }
    else if (account){
      return this.http.get<any>(`http://localhost:3004/listUser?q&_page=${page}&_limit=${limit}&account=${account}&position_like=${position}&status_like=${status}&keyEmployee_like=${keyEmployee}&employeeName_like=${employeeName}`).toPromise().then(res => res);
    }
    else if (keyEmployee){
      return this.http.get<any>(`http://localhost:3004/listUser?q&_page=${page}&_limit=${limit}&account_like=${account}&position_like=${position}&status_like=${status}&keyEmployee=${keyEmployee}&employeeName_like=${employeeName}`).toPromise().then(res => res);
    }
    else {
      return this.http.get<any>(`http://localhost:3004/listUser?q&_page=${page}&_limit=${limit}&account_like=${account}&position_like=${position}&status_like=${status}&keyEmployee_like=${keyEmployee}&employeeName_like=${employeeName}`).toPromise().then(res => res);
    }
  }

  getListDepartment (){
    return this.http.get<any>('http://localhost:3004/listDepartment')
  }

  postData(body: any): Observable<any> {
    return this.http.post<any>('http://localhost:3004/listUser', body, httpOptions);
  }
  patchData(body: any, id: number): Observable<any>{
    return  this.http.patch<any>('http://localhost:3004/listUser/' + id, body, httpOptions)
  }

  getDataDetails(id: number){
    return this.http.get<any>('http://localhost:3004/listUser/' + id);
  }

  removeData(id: number){
    return this.http.delete<any>('http://localhost:3004/listUser/' + id)
  }
}
