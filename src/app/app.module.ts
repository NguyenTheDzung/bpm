import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from '@shared/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactoryTranslate } from '@cores/utils/functions';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxEchartsModule.forRoot({
      /**
       * Những tính năng đã được compress lại trong file:
       * - Chart: Bar | Line | Pie
       * - Coordinate Systems: Grid
       * - Coordinate Systems: Title | Legend | Tooltip
       * - Other: Utilities | Code Compression
       */
      // @ts-ignore
      echarts: () => import('../assets/echart-bundle/echarts.min.js'),
    }),
    TranslateModule.forRoot({
      defaultLanguage: 'vi',
      useDefaultLang: true,
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactoryTranslate,
        deps: [HttpClient],
      },
    }),
    CoreModule.forRoot(),
    SharedModule.forRoot(),
  ],
  providers: [
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: initializer,
    //   deps: [KeycloakService],
    //   multi: true,
    // },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
